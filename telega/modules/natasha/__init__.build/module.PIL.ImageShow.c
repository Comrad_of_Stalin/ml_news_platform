/* Generated code for Python module 'PIL.ImageShow'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_PIL$ImageShow" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_PIL$ImageShow;
PyDictObject *moduledict_PIL$ImageShow;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_f8c87d375a84d8cdcf7d6314f8a47ada;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_str_digest_c3032212395a0fbc92ff111d65a8410a;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_b7452332cb96748e6cd13493307ccf8e;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_digest_082cef2a0b23aebc75dc4d2069eabf0b;
static PyObject *const_str_digest_1cdf033b8c960667a939c36353e86b65;
static PyObject *const_str_digest_6b7b90b3e1f49b978a3fb0fe064aece8;
static PyObject *const_tuple_str_plain_xv_tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_str_plain_show_image;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___exit__;
extern PyObject *const_tuple_str_plain_display_tuple;
extern PyObject *const_str_plain_tempfile;
static PyObject *const_str_plain_get_command_ex;
static PyObject *const_str_digest_0656acedc222e54acce2b17f7460767e;
extern PyObject *const_str_plain_mode;
extern PyObject *const_str_plain_which;
static PyObject *const_dict_f340ab83a67ad58f315753e268cd4d44;
extern PyObject *const_str_plain_image;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_f2893365b8649be4fd37f1e45f30ea7e;
static PyObject *const_str_plain_EogViewer;
extern PyObject *const_str_plain___enter__;
extern PyObject *const_str_plain_isfile;
static PyObject *const_str_digest_0d9b4a629a2a7746be32401c3593b928;
extern PyObject *const_str_plain_command;
extern PyObject *const_str_plain_pipes;
extern PyObject *const_str_plain_join;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_platform;
static PyObject *const_str_digest_f033dfdf46b40ce829d63ef28b2f10f7;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_XVViewer;
extern PyObject *const_str_plain_Popen;
extern PyObject *const_str_plain_getmodebase;
extern PyObject *const_str_plain___debug__;
static PyObject *const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple;
extern PyObject *const_str_plain_L;
static PyObject *const_str_digest_38ffab005772e67b8b735166edbf1513;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_PIL;
static PyObject *const_tuple_cad7af596d55db69499f14366fc3184f_tuple;
extern PyObject *const_str_plain_quote;
extern PyObject *const_str_plain_options;
extern PyObject *const_str_plain_environ;
static PyObject *const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple;
static PyObject *const_str_plain_eog;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_dbaa2ae3e0fa649b7a8fa121daf2a7e8;
static PyObject *const_str_plain_get_format;
static PyObject *const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple;
static PyObject *const_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_plain_path;
static PyObject *const_str_plain__viewers;
static PyObject *const_str_digest_63c0ab1ab33a4c5210275e0c2fcbd817;
extern PyObject *const_str_plain_fdopen;
extern PyObject *const_str_plain_insert;
extern PyObject *const_str_plain_X_OK;
static PyObject *const_str_plain_show_file;
extern PyObject *const_str_plain_subprocess;
extern PyObject *const_str_plain_display;
static PyObject *const_str_digest_8dd48c9a9324b0d8c2ba699985151b49;
extern PyObject *const_str_plain_convert;
static PyObject *const_str_digest_38f70f269aa451981712abba905303d0;
extern PyObject *const_str_plain_fd;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_viewer;
static PyObject *const_str_digest_7acd952a9f67b5e9ea004b6ea77e6331;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_issubclass;
extern PyObject *const_str_plain_title;
static PyObject *const_str_digest_6a0da6d3c6b355e8363dcc08366ad154;
extern PyObject *const_str_plain_major;
static PyObject *const_tuple_str_plain_self_str_plain_image_str_plain_options_tuple;
extern PyObject *const_str_plain_r;
static PyObject *const_str_digest_e3456b1263890745563ca15add33fa1e;
static PyObject *const_tuple_str_plain_eog_str_plain_eog_tuple;
static PyObject *const_str_digest_6216e4740b26b17fa88e334290e7fc51;
extern PyObject *const_str_digest_0fde4dc5ce330fdd8e2eb5c008ed370c;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_remove;
static PyObject *const_str_digest_c3436a9fbd3881d406bd8ad9020c89b0;
static PyObject *const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple;
static PyObject *const_str_plain_xv;
extern PyObject *const_str_plain_show;
extern PyObject *const_tuple_str_plain_Image_tuple;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple;
extern PyObject *const_str_plain_f;
extern PyObject *const_int_0;
static PyObject *const_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list;
extern PyObject *const_str_plain_register;
static PyObject *const_tuple_str_plain_display_str_plain_display_tuple;
static PyObject *const_tuple_str_plain_viewer_str_plain_order_tuple;
static PyObject *const_str_digest_435ac3f107734ce2e0e8e4f835e273fa;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_abcbeb7ba7ac3747a1ee20c6dc91ef42;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_plain_get_command;
extern PyObject *const_slice_none_int_pos_4_none;
extern PyObject *const_str_plain_base;
static PyObject *const_str_plain_DisplayViewer;
extern PyObject *const_int_pos_4;
extern PyObject *const_tuple_str_plain_quote_tuple;
static PyObject *const_tuple_str_plain_eog_tuple;
extern PyObject *const_str_plain_PATH;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_dirname;
extern PyObject *const_str_plain_system;
static PyObject *const_str_digest_417b20c428ec11e5abc58f5b33589964;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_7e2f4256bad75e8a09424ead68261b92;
extern PyObject *const_str_plain_print_function;
static PyObject *const_str_digest_f0c978d81af299905560b512e18af135;
extern PyObject *const_str_plain___class__;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_str_plain_MacViewer;
extern PyObject *const_tuple_type_object_tuple;
static PyObject *const_str_digest_0c9499ab8fc32c33c6e95a447660ccfc;
extern PyObject *const_str_plain___module__;
static PyObject *const_tuple_str_plain_PATH_tuple;
static PyObject *const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain__dump;
extern PyObject *const_str_plain_filename;
static PyObject *const_str_plain_UnixViewer;
extern PyObject *const_str_plain_file;
static PyObject *const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_shell;
extern PyObject *const_str_plain_1;
extern PyObject *const_str_plain_BMP;
extern PyObject *const_str_plain_access;
static PyObject *const_str_digest_6c9971e8ae6af8026f9683ca60f2364b;
static PyObject *const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_shlex;
static PyObject *const_str_plain_save_image;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_WindowsViewer;
extern PyObject *const_str_plain_stdin;
extern PyObject *const_str_plain_RGBA;
extern PyObject *const_str_plain_write;
extern PyObject *const_str_plain_pathsep;
extern PyObject *const_str_plain_version_info;
static PyObject *const_tuple_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list_tuple;
extern PyObject *const_str_plain_PNG;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_darwin;
static PyObject *const_str_digest_7a4dc08fb4b4a7d54d583752f8679aac;
extern PyObject *const_str_plain_Image;
extern PyObject *const_str_plain_format;
extern PyObject *const_tuple_str_plain_self_str_plain_image_tuple;
extern PyObject *const_str_plain_mkstemp;
static PyObject *const_str_digest_6903407ee41e83e6ca72d5d2af95bb42;
extern PyObject *const_str_plain_executable;
extern PyObject *const_str_plain_win32;
static PyObject *const_str_plain_Viewer;
extern PyObject *const_str_plain_compress_level;
static PyObject *const_str_digest_270fcf9c829b97bd437d7f5bd4e89845;
extern PyObject *const_str_plain_order;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_f8c87d375a84d8cdcf7d6314f8a47ada = UNSTREAM_STRING_ASCII( &constant_bin[ 76018 ], 46, 0 );
    const_str_digest_c3032212395a0fbc92ff111d65a8410a = UNSTREAM_STRING_ASCII( &constant_bin[ 76064 ], 25, 0 );
    const_str_digest_b7452332cb96748e6cd13493307ccf8e = UNSTREAM_STRING_ASCII( &constant_bin[ 76089 ], 9, 0 );
    const_str_digest_082cef2a0b23aebc75dc4d2069eabf0b = UNSTREAM_STRING_ASCII( &constant_bin[ 76098 ], 260, 0 );
    const_str_digest_1cdf033b8c960667a939c36353e86b65 = UNSTREAM_STRING_ASCII( &constant_bin[ 76358 ], 18, 0 );
    const_str_digest_6b7b90b3e1f49b978a3fb0fe064aece8 = UNSTREAM_STRING_ASCII( &constant_bin[ 76376 ], 22, 0 );
    const_tuple_str_plain_xv_tuple = PyTuple_New( 1 );
    const_str_plain_xv = UNSTREAM_STRING_ASCII( &constant_bin[ 34801 ], 2, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_xv_tuple, 0, const_str_plain_xv ); Py_INCREF( const_str_plain_xv );
    const_str_plain_show_image = UNSTREAM_STRING_ASCII( &constant_bin[ 76398 ], 10, 1 );
    const_str_plain_get_command_ex = UNSTREAM_STRING_ASCII( &constant_bin[ 76408 ], 14, 1 );
    const_str_digest_0656acedc222e54acce2b17f7460767e = UNSTREAM_STRING_ASCII( &constant_bin[ 76422 ], 19, 0 );
    const_dict_f340ab83a67ad58f315753e268cd4d44 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_f340ab83a67ad58f315753e268cd4d44, const_str_plain_compress_level, const_int_pos_1 );
    assert( PyDict_Size( const_dict_f340ab83a67ad58f315753e268cd4d44 ) == 1 );
    const_str_digest_f2893365b8649be4fd37f1e45f30ea7e = UNSTREAM_STRING_ASCII( &constant_bin[ 76441 ], 43, 0 );
    const_str_plain_EogViewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76484 ], 9, 1 );
    const_str_digest_0d9b4a629a2a7746be32401c3593b928 = UNSTREAM_STRING_ASCII( &constant_bin[ 76493 ], 18, 0 );
    const_str_digest_f033dfdf46b40ce829d63ef28b2f10f7 = UNSTREAM_STRING_ASCII( &constant_bin[ 76425 ], 11, 0 );
    const_str_plain_XVViewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76511 ], 8, 1 );
    const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple, 0, const_str_plain_executable ); Py_INCREF( const_str_plain_executable );
    PyTuple_SET_ITEM( const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple, 1, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple, 2, const_str_plain_dirname ); Py_INCREF( const_str_plain_dirname );
    PyTuple_SET_ITEM( const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple, 3, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    const_str_digest_38ffab005772e67b8b735166edbf1513 = UNSTREAM_STRING_ASCII( &constant_bin[ 76071 ], 18, 0 );
    const_tuple_cad7af596d55db69499f14366fc3184f_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 1, const_str_plain_file ); Py_INCREF( const_str_plain_file );
    PyTuple_SET_ITEM( const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 3, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 4, const_str_plain_executable ); Py_INCREF( const_str_plain_executable );
    const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple, 1, const_str_plain_image ); Py_INCREF( const_str_plain_image );
    PyTuple_SET_ITEM( const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple, 3, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    const_str_plain_eog = UNSTREAM_STRING_ASCII( &constant_bin[ 35608 ], 3, 1 );
    const_str_digest_dbaa2ae3e0fa649b7a8fa121daf2a7e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 76519 ], 10, 0 );
    const_str_plain_get_format = UNSTREAM_STRING_ASCII( &constant_bin[ 60198 ], 10, 1 );
    const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple, 0, const_str_plain_image ); Py_INCREF( const_str_plain_image );
    PyTuple_SET_ITEM( const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple, 1, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    const_str_plain_viewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76205 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple, 3, const_str_plain_viewer ); Py_INCREF( const_str_plain_viewer );
    const_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e = UNSTREAM_STRING_ASCII( &constant_bin[ 76529 ], 66, 0 );
    const_str_plain__viewers = UNSTREAM_STRING_ASCII( &constant_bin[ 76595 ], 8, 1 );
    const_str_digest_63c0ab1ab33a4c5210275e0c2fcbd817 = UNSTREAM_STRING_ASCII( &constant_bin[ 76603 ], 22, 0 );
    const_str_plain_show_file = UNSTREAM_STRING_ASCII( &constant_bin[ 76432 ], 9, 1 );
    const_str_digest_8dd48c9a9324b0d8c2ba699985151b49 = UNSTREAM_STRING_ASCII( &constant_bin[ 76625 ], 23, 0 );
    const_str_digest_38f70f269aa451981712abba905303d0 = UNSTREAM_STRING_ASCII( &constant_bin[ 76648 ], 17, 0 );
    const_str_digest_7acd952a9f67b5e9ea004b6ea77e6331 = UNSTREAM_STRING_ASCII( &constant_bin[ 76665 ], 28, 0 );
    const_str_digest_6a0da6d3c6b355e8363dcc08366ad154 = UNSTREAM_STRING_ASCII( &constant_bin[ 76693 ], 14, 0 );
    const_tuple_str_plain_self_str_plain_image_str_plain_options_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_image_str_plain_options_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_image_str_plain_options_tuple, 1, const_str_plain_image ); Py_INCREF( const_str_plain_image );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_image_str_plain_options_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    const_str_digest_e3456b1263890745563ca15add33fa1e = UNSTREAM_STRING_ASCII( &constant_bin[ 76539 ], 33, 0 );
    const_tuple_str_plain_eog_str_plain_eog_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_eog_str_plain_eog_tuple, 0, const_str_plain_eog ); Py_INCREF( const_str_plain_eog );
    PyTuple_SET_ITEM( const_tuple_str_plain_eog_str_plain_eog_tuple, 1, const_str_plain_eog ); Py_INCREF( const_str_plain_eog );
    const_str_digest_6216e4740b26b17fa88e334290e7fc51 = UNSTREAM_STRING_ASCII( &constant_bin[ 76707 ], 21, 0 );
    const_str_digest_c3436a9fbd3881d406bd8ad9020c89b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 76728 ], 16, 0 );
    const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple, 1, const_str_plain_file ); Py_INCREF( const_str_plain_file );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 1, const_str_plain_file ); Py_INCREF( const_str_plain_file );
    PyTuple_SET_ITEM( const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 3, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyTuple_SET_ITEM( const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 4, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 5, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    const_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list, 0, const_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e ); Py_INCREF( const_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e );
    const_tuple_str_plain_display_str_plain_display_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_display_str_plain_display_tuple, 0, const_str_plain_display ); Py_INCREF( const_str_plain_display );
    PyTuple_SET_ITEM( const_tuple_str_plain_display_str_plain_display_tuple, 1, const_str_plain_display ); Py_INCREF( const_str_plain_display );
    const_tuple_str_plain_viewer_str_plain_order_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_viewer_str_plain_order_tuple, 0, const_str_plain_viewer ); Py_INCREF( const_str_plain_viewer );
    PyTuple_SET_ITEM( const_tuple_str_plain_viewer_str_plain_order_tuple, 1, const_str_plain_order ); Py_INCREF( const_str_plain_order );
    const_str_digest_435ac3f107734ce2e0e8e4f835e273fa = UNSTREAM_STRING_ASCII( &constant_bin[ 76744 ], 23, 0 );
    const_str_digest_abcbeb7ba7ac3747a1ee20c6dc91ef42 = UNSTREAM_STRING_ASCII( &constant_bin[ 76425 ], 16, 0 );
    const_str_plain_get_command = UNSTREAM_STRING_ASCII( &constant_bin[ 76078 ], 11, 1 );
    const_str_plain_DisplayViewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76767 ], 13, 1 );
    const_tuple_str_plain_eog_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_eog_tuple, 0, const_str_plain_eog ); Py_INCREF( const_str_plain_eog );
    const_str_digest_417b20c428ec11e5abc58f5b33589964 = UNSTREAM_STRING_ASCII( &constant_bin[ 76384 ], 13, 0 );
    const_str_digest_7e2f4256bad75e8a09424ead68261b92 = UNSTREAM_STRING_ASCII( &constant_bin[ 76780 ], 17, 0 );
    const_str_digest_f0c978d81af299905560b512e18af135 = UNSTREAM_STRING_ASCII( &constant_bin[ 76797 ], 20, 0 );
    const_str_plain_MacViewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76422 ], 9, 1 );
    const_str_digest_0c9499ab8fc32c33c6e95a447660ccfc = UNSTREAM_STRING_ASCII( &constant_bin[ 76817 ], 19, 0 );
    const_tuple_str_plain_PATH_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PATH_tuple, 0, const_str_plain_PATH ); Py_INCREF( const_str_plain_PATH );
    const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple, 1, const_str_plain_file ); Py_INCREF( const_str_plain_file );
    PyTuple_SET_ITEM( const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple, 3, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    const_str_plain_UnixViewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76603 ], 10, 1 );
    const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 1, const_str_plain_file ); Py_INCREF( const_str_plain_file );
    PyTuple_SET_ITEM( const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 2, const_str_plain_title ); Py_INCREF( const_str_plain_title );
    PyTuple_SET_ITEM( const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 3, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 4, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 5, const_str_plain_executable ); Py_INCREF( const_str_plain_executable );
    const_str_digest_6c9971e8ae6af8026f9683ca60f2364b = UNSTREAM_STRING_ASCII( &constant_bin[ 76836 ], 24, 0 );
    const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 1, const_str_plain_file ); Py_INCREF( const_str_plain_file );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 3, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 4, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 5, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 6, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    const_str_plain_save_image = UNSTREAM_STRING_ASCII( &constant_bin[ 76787 ], 10, 1 );
    const_str_plain_WindowsViewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76064 ], 13, 1 );
    const_tuple_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list_tuple, 0, const_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list ); Py_INCREF( const_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list );
    const_str_digest_7a4dc08fb4b4a7d54d583752f8679aac = UNSTREAM_STRING_ASCII( &constant_bin[ 76860 ], 28, 0 );
    const_str_digest_6903407ee41e83e6ca72d5d2af95bb42 = UNSTREAM_STRING_ASCII( &constant_bin[ 76888 ], 17, 0 );
    const_str_plain_Viewer = UNSTREAM_STRING_ASCII( &constant_bin[ 76071 ], 6, 1 );
    const_str_digest_270fcf9c829b97bd437d7f5bd4e89845 = UNSTREAM_STRING_ASCII( &constant_bin[ 76905 ], 68, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_PIL$ImageShow( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_f8eb43222464beea8566d39e2743ee92;
static PyCodeObject *codeobj_621adb69f1a7443e006c5aeb8b9b0b88;
static PyCodeObject *codeobj_8d5a3a6c708045300f009fb324038521;
static PyCodeObject *codeobj_dc73a6dbd4552d1ae48c358e48b98604;
static PyCodeObject *codeobj_86e12a72b9b504adcb71f61e7d4a4a09;
static PyCodeObject *codeobj_e1dcd8f1e1caf4be2e71097cbe9d7729;
static PyCodeObject *codeobj_3bcdf4be5ba30a7e1c42daff022047c0;
static PyCodeObject *codeobj_dde79275ab8f4680c1f745554f06161e;
static PyCodeObject *codeobj_b91a0df666fb7462de6ece8dbe0acfaa;
static PyCodeObject *codeobj_900997e66616405bb67e8e18e3401df0;
static PyCodeObject *codeobj_9562bfe5aabee73f406826ce2558b031;
static PyCodeObject *codeobj_4fed9cd01f7a7d6bebe26a0c71b200df;
static PyCodeObject *codeobj_4feee8de771cab4b7f65e5b6826feaa2;
static PyCodeObject *codeobj_24f67ce2a7d6b1b4e483b18cfb0665a7;
static PyCodeObject *codeobj_293b13abf68fb62da82aed03bd26e861;
static PyCodeObject *codeobj_f309a2bd401499e67876b8f2bd30a70d;
static PyCodeObject *codeobj_64e98e70326e906274a0a1f3b595209e;
static PyCodeObject *codeobj_c3c436a7550ca9f95df58c4ea3455b83;
static PyCodeObject *codeobj_98caddbffe284a15e4482c8d4eaba007;
static PyCodeObject *codeobj_7b8b283b22ea14de67fd17cc47f1d83d;
static PyCodeObject *codeobj_dee868658f2b95885999e169bc5e41c3;
static PyCodeObject *codeobj_e9430fb60134b32275873c29099f45d1;
static PyCodeObject *codeobj_e62a848cc795bd9659efa4c2ed88fed5;
static PyCodeObject *codeobj_cab2371a5f8deb3b9df99981852a3993;
static PyCodeObject *codeobj_65a6ef7c15d54ec2fdf30736098ea855;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_c3436a9fbd3881d406bd8ad9020c89b0 );
    codeobj_f8eb43222464beea8566d39e2743ee92 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_6b7b90b3e1f49b978a3fb0fe064aece8, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_621adb69f1a7443e006c5aeb8b9b0b88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_DisplayViewer, 189, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8d5a3a6c708045300f009fb324038521 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_EogViewer, 197, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_dc73a6dbd4552d1ae48c358e48b98604 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MacViewer, 121, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_86e12a72b9b504adcb71f61e7d4a4a09 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_UnixViewer, 164, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_e1dcd8f1e1caf4be2e71097cbe9d7729 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Viewer, 58, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_3bcdf4be5ba30a7e1c42daff022047c0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_WindowsViewer, 109, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_dde79275ab8f4680c1f745554f06161e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_XVViewer, 205, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_b91a0df666fb7462de6ece8dbe0acfaa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command, 88, const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_900997e66616405bb67e8e18e3401df0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command, 112, const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_9562bfe5aabee73f406826ce2558b031 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command, 125, const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_4fed9cd01f7a7d6bebe26a0c71b200df = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command, 168, const_tuple_7201030e0c4e8c6637a8f9eb379238a1_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_4feee8de771cab4b7f65e5b6826feaa2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command_ex, 190, const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_24f67ce2a7d6b1b4e483b18cfb0665a7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command_ex, 198, const_tuple_cad7af596d55db69499f14366fc3184f_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_293b13abf68fb62da82aed03bd26e861 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_command_ex, 206, const_tuple_21d19d0833a60e7bad95623c45da7f05_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_f309a2bd401499e67876b8f2bd30a70d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_format, 84, const_tuple_str_plain_self_str_plain_image_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_64e98e70326e906274a0a1f3b595209e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_register, 31, const_tuple_str_plain_viewer_str_plain_order_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c3c436a7550ca9f95df58c4ea3455b83 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_save_image, 91, const_tuple_str_plain_self_str_plain_image_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_98caddbffe284a15e4482c8d4eaba007 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show, 43, const_tuple_6171ce2561d90c9a7e650fcd3eb7a827_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_7b8b283b22ea14de67fd17cc47f1d83d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show, 63, const_tuple_2523d89fd3c9198a086a43ba7dea6ab6_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_dee868658f2b95885999e169bc5e41c3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_file, 99, const_tuple_str_plain_self_str_plain_file_str_plain_options_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_e9430fb60134b32275873c29099f45d1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_file, 133, const_tuple_1d913518bf4e92eb88644711cc1a1f6e_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_e62a848cc795bd9659efa4c2ed88fed5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_file, 172, const_tuple_123bac57f9d5a7bab8fb4bdd10a533c2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_cab2371a5f8deb3b9df99981852a3993 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_image, 95, const_tuple_str_plain_self_str_plain_image_str_plain_options_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_65a6ef7c15d54ec2fdf30736098ea855 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_which, 154, const_tuple_3a4da288cd338c82fb9643cc7750f4d9_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_11_complex_call_helper_pos_keywords_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_10_get_command(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_11_show_file(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_12_which(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_13_get_command(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_14_show_file(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_15_get_command_ex(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_16_get_command_ex(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_17_get_command_ex( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_1_register( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_2_show( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_3_show(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_4_get_format(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_5_get_command(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_6_save_image(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_7_show_image(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_8_show_file(  );


static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_9_get_command(  );


// The module function definitions.
static PyObject *impl_PIL$ImageShow$$$function_1_register( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_viewer = python_pars[ 0 ];
    PyObject *par_order = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_64e98e70326e906274a0a1f3b595209e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_64e98e70326e906274a0a1f3b595209e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_64e98e70326e906274a0a1f3b595209e, codeobj_64e98e70326e906274a0a1f3b595209e, module_PIL$ImageShow, sizeof(void *)+sizeof(void *) );
    frame_64e98e70326e906274a0a1f3b595209e = cache_frame_64e98e70326e906274a0a1f3b595209e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_64e98e70326e906274a0a1f3b595209e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_64e98e70326e906274a0a1f3b595209e ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_1;
        int tmp_truth_name_1;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_issubclass );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( par_viewer );
        tmp_args_element_name_1 = par_viewer;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Viewer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Viewer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Viewer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_2 = tmp_mvar_value_1;
        frame_64e98e70326e906274a0a1f3b595209e->m_frame.f_lineno = 33;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 33;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_2;
            CHECK_OBJECT( par_viewer );
            tmp_called_name_2 = par_viewer;
            frame_64e98e70326e906274a0a1f3b595209e->m_frame.f_lineno = 34;
            tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = par_viewer;
                assert( old != NULL );
                par_viewer = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_64e98e70326e906274a0a1f3b595209e, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_64e98e70326e906274a0a1f3b595209e, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_TypeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 32;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_64e98e70326e906274a0a1f3b595209e->m_frame) frame_64e98e70326e906274a0a1f3b595209e->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_3;
        branch_no_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_1_register );
    return NULL;
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_order );
        tmp_compexpr_left_2 = par_order;
        tmp_compexpr_right_2 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain__viewers );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__viewers );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_viewers" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 38;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            if ( par_viewer == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "viewer" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 38;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_3 = par_viewer;
            frame_64e98e70326e906274a0a1f3b595209e->m_frame.f_lineno = 38;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_order );
            tmp_compexpr_left_3 = par_order;
            tmp_compexpr_right_3 = const_int_0;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_2;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain__viewers );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__viewers );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_viewers" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 40;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_2 = tmp_mvar_value_3;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_insert );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_4 = const_int_0;
                if ( par_viewer == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "viewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 40;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }

                tmp_args_element_name_5 = par_viewer;
                frame_64e98e70326e906274a0a1f3b595209e->m_frame.f_lineno = 40;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                    tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_4:;
        }
        branch_end_3:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64e98e70326e906274a0a1f3b595209e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64e98e70326e906274a0a1f3b595209e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_64e98e70326e906274a0a1f3b595209e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_64e98e70326e906274a0a1f3b595209e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_64e98e70326e906274a0a1f3b595209e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_64e98e70326e906274a0a1f3b595209e,
        type_description_1,
        par_viewer,
        par_order
    );


    // Release cached frame.
    if ( frame_64e98e70326e906274a0a1f3b595209e == cache_frame_64e98e70326e906274a0a1f3b595209e )
    {
        Py_DECREF( frame_64e98e70326e906274a0a1f3b595209e );
    }
    cache_frame_64e98e70326e906274a0a1f3b595209e = NULL;

    assertFrameObject( frame_64e98e70326e906274a0a1f3b595209e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_1_register );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_viewer );
    par_viewer = NULL;

    CHECK_OBJECT( (PyObject *)par_order );
    Py_DECREF( par_order );
    par_order = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_viewer );
    par_viewer = NULL;

    CHECK_OBJECT( (PyObject *)par_order );
    Py_DECREF( par_order );
    par_order = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_1_register );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_2_show( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image = python_pars[ 0 ];
    PyObject *par_title = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *var_viewer = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_98caddbffe284a15e4482c8d4eaba007;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_98caddbffe284a15e4482c8d4eaba007 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_98caddbffe284a15e4482c8d4eaba007, codeobj_98caddbffe284a15e4482c8d4eaba007, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_98caddbffe284a15e4482c8d4eaba007 = cache_frame_98caddbffe284a15e4482c8d4eaba007;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_98caddbffe284a15e4482c8d4eaba007 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_98caddbffe284a15e4482c8d4eaba007 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain__viewers );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__viewers );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_viewers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = tmp_mvar_value_1;
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 52;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_viewer;
            var_viewer = tmp_assign_source_3;
            Py_INCREF( var_viewer );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dircall_arg4_1;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_viewer );
        tmp_source_name_1 = var_viewer;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_show );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_image );
        tmp_tuple_element_1 = par_image;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_title;
        CHECK_OBJECT( par_title );
        tmp_dict_value_1 = par_title;
        tmp_dircall_arg3_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        CHECK_OBJECT( par_options );
        tmp_dircall_arg4_1 = par_options;
        Py_INCREF( tmp_dircall_arg4_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1, tmp_dircall_arg4_1};
            tmp_call_result_1 = impl___internal__$$$function_11_complex_call_helper_pos_keywords_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 53;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_int_pos_1;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_2;
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_98caddbffe284a15e4482c8d4eaba007 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_98caddbffe284a15e4482c8d4eaba007 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_98caddbffe284a15e4482c8d4eaba007 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_98caddbffe284a15e4482c8d4eaba007, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_98caddbffe284a15e4482c8d4eaba007->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_98caddbffe284a15e4482c8d4eaba007, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_98caddbffe284a15e4482c8d4eaba007,
        type_description_1,
        par_image,
        par_title,
        par_options,
        var_viewer
    );


    // Release cached frame.
    if ( frame_98caddbffe284a15e4482c8d4eaba007 == cache_frame_98caddbffe284a15e4482c8d4eaba007 )
    {
        Py_DECREF( frame_98caddbffe284a15e4482c8d4eaba007 );
    }
    cache_frame_98caddbffe284a15e4482c8d4eaba007 = NULL;

    assertFrameObject( frame_98caddbffe284a15e4482c8d4eaba007 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = const_int_0;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_2_show );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    Py_XDECREF( var_viewer );
    var_viewer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    Py_XDECREF( var_viewer );
    var_viewer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_2_show );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_3_show( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_image = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *var_base = NULL;
    struct Nuitka_FrameObject *frame_7b8b283b22ea14de67fd17cc47f1d83d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7b8b283b22ea14de67fd17cc47f1d83d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7b8b283b22ea14de67fd17cc47f1d83d, codeobj_7b8b283b22ea14de67fd17cc47f1d83d, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7b8b283b22ea14de67fd17cc47f1d83d = cache_frame_7b8b283b22ea14de67fd17cc47f1d83d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7b8b283b22ea14de67fd17cc47f1d83d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7b8b283b22ea14de67fd17cc47f1d83d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_image );
        tmp_source_name_1 = par_image;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_mode );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_slice_none_int_pos_4_none;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_digest_0fde4dc5ce330fdd8e2eb5c008ed370c;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = const_str_plain_L;
            assert( var_base == NULL );
            Py_INCREF( tmp_assign_source_1 );
            var_base = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Image );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 73;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_getmodebase );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_image );
            tmp_source_name_3 = par_image;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_mode );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 73;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_7b8b283b22ea14de67fd17cc47f1d83d->m_frame.f_lineno = 73;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_base == NULL );
            var_base = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_4;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( var_base );
        tmp_compexpr_left_2 = var_base;
        CHECK_OBJECT( par_image );
        tmp_source_name_4 = par_image;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_mode );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_image );
        tmp_source_name_5 = par_image;
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_mode );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_str_plain_1;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        CHECK_OBJECT( par_image );
        tmp_source_name_6 = par_image;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_mode );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_str_plain_RGBA;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_right_value_1 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_and_right_value_1 = tmp_and_left_value_2;
        and_end_2:;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_image );
            tmp_called_instance_1 = par_image;
            CHECK_OBJECT( var_base );
            tmp_args_element_name_2 = var_base;
            frame_7b8b283b22ea14de67fd17cc47f1d83d->m_frame.f_lineno = 75;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_convert, call_args );
            }

            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_image;
                assert( old != NULL );
                par_image = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_7;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_show_image );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image );
        tmp_tuple_element_1 = par_image;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_options );
        tmp_dircall_arg3_1 = par_options;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b8b283b22ea14de67fd17cc47f1d83d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b8b283b22ea14de67fd17cc47f1d83d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b8b283b22ea14de67fd17cc47f1d83d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7b8b283b22ea14de67fd17cc47f1d83d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7b8b283b22ea14de67fd17cc47f1d83d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7b8b283b22ea14de67fd17cc47f1d83d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7b8b283b22ea14de67fd17cc47f1d83d,
        type_description_1,
        par_self,
        par_image,
        par_options,
        var_base
    );


    // Release cached frame.
    if ( frame_7b8b283b22ea14de67fd17cc47f1d83d == cache_frame_7b8b283b22ea14de67fd17cc47f1d83d )
    {
        Py_DECREF( frame_7b8b283b22ea14de67fd17cc47f1d83d );
    }
    cache_frame_7b8b283b22ea14de67fd17cc47f1d83d = NULL;

    assertFrameObject( frame_7b8b283b22ea14de67fd17cc47f1d83d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_3_show );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_base );
    Py_DECREF( var_base );
    var_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_3_show );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_4_get_format( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_image = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_f309a2bd401499e67876b8f2bd30a70d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f309a2bd401499e67876b8f2bd30a70d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f309a2bd401499e67876b8f2bd30a70d, codeobj_f309a2bd401499e67876b8f2bd30a70d, module_PIL$ImageShow, sizeof(void *)+sizeof(void *) );
    frame_f309a2bd401499e67876b8f2bd30a70d = cache_frame_f309a2bd401499e67876b8f2bd30a70d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f309a2bd401499e67876b8f2bd30a70d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f309a2bd401499e67876b8f2bd30a70d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f309a2bd401499e67876b8f2bd30a70d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f309a2bd401499e67876b8f2bd30a70d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f309a2bd401499e67876b8f2bd30a70d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f309a2bd401499e67876b8f2bd30a70d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f309a2bd401499e67876b8f2bd30a70d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f309a2bd401499e67876b8f2bd30a70d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f309a2bd401499e67876b8f2bd30a70d,
        type_description_1,
        par_self,
        par_image
    );


    // Release cached frame.
    if ( frame_f309a2bd401499e67876b8f2bd30a70d == cache_frame_f309a2bd401499e67876b8f2bd30a70d )
    {
        Py_DECREF( frame_f309a2bd401499e67876b8f2bd30a70d );
    }
    cache_frame_f309a2bd401499e67876b8f2bd30a70d = NULL;

    assertFrameObject( frame_f309a2bd401499e67876b8f2bd30a70d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_4_get_format );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_4_get_format );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_5_get_command( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_b91a0df666fb7462de6ece8dbe0acfaa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b91a0df666fb7462de6ece8dbe0acfaa = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b91a0df666fb7462de6ece8dbe0acfaa, codeobj_b91a0df666fb7462de6ece8dbe0acfaa, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b91a0df666fb7462de6ece8dbe0acfaa = cache_frame_b91a0df666fb7462de6ece8dbe0acfaa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b91a0df666fb7462de6ece8dbe0acfaa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b91a0df666fb7462de6ece8dbe0acfaa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        tmp_raise_type_1 = PyExc_NotImplementedError;
        exception_type = tmp_raise_type_1;
        Py_INCREF( tmp_raise_type_1 );
        exception_lineno = 89;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b91a0df666fb7462de6ece8dbe0acfaa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b91a0df666fb7462de6ece8dbe0acfaa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b91a0df666fb7462de6ece8dbe0acfaa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b91a0df666fb7462de6ece8dbe0acfaa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b91a0df666fb7462de6ece8dbe0acfaa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b91a0df666fb7462de6ece8dbe0acfaa,
        type_description_1,
        par_self,
        par_file,
        par_options
    );


    // Release cached frame.
    if ( frame_b91a0df666fb7462de6ece8dbe0acfaa == cache_frame_b91a0df666fb7462de6ece8dbe0acfaa )
    {
        Py_DECREF( frame_b91a0df666fb7462de6ece8dbe0acfaa );
    }
    cache_frame_b91a0df666fb7462de6ece8dbe0acfaa = NULL;

    assertFrameObject( frame_b91a0df666fb7462de6ece8dbe0acfaa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_5_get_command );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_5_get_command );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_PIL$ImageShow$$$function_6_save_image( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_image = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_c3c436a7550ca9f95df58c4ea3455b83;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_c3c436a7550ca9f95df58c4ea3455b83 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c3c436a7550ca9f95df58c4ea3455b83, codeobj_c3c436a7550ca9f95df58c4ea3455b83, module_PIL$ImageShow, sizeof(void *)+sizeof(void *) );
    frame_c3c436a7550ca9f95df58c4ea3455b83 = cache_frame_c3c436a7550ca9f95df58c4ea3455b83;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c3c436a7550ca9f95df58c4ea3455b83 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c3c436a7550ca9f95df58c4ea3455b83 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_image );
        tmp_source_name_1 = par_image;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__dump );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_format;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_image );
        tmp_args_element_name_1 = par_image;
        frame_c3c436a7550ca9f95df58c4ea3455b83->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get_format, call_args );
        }

        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 93;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg2_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_dircall_arg3_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_options );
        if ( tmp_dircall_arg3_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );
            Py_DECREF( tmp_dircall_arg2_1 );

            exception_lineno = 93;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c3c436a7550ca9f95df58c4ea3455b83 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c3c436a7550ca9f95df58c4ea3455b83 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c3c436a7550ca9f95df58c4ea3455b83 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c3c436a7550ca9f95df58c4ea3455b83, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c3c436a7550ca9f95df58c4ea3455b83->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c3c436a7550ca9f95df58c4ea3455b83, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c3c436a7550ca9f95df58c4ea3455b83,
        type_description_1,
        par_self,
        par_image
    );


    // Release cached frame.
    if ( frame_c3c436a7550ca9f95df58c4ea3455b83 == cache_frame_c3c436a7550ca9f95df58c4ea3455b83 )
    {
        Py_DECREF( frame_c3c436a7550ca9f95df58c4ea3455b83 );
    }
    cache_frame_c3c436a7550ca9f95df58c4ea3455b83 = NULL;

    assertFrameObject( frame_c3c436a7550ca9f95df58c4ea3455b83 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_6_save_image );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_6_save_image );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_7_show_image( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_image = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_cab2371a5f8deb3b9df99981852a3993;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_cab2371a5f8deb3b9df99981852a3993 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cab2371a5f8deb3b9df99981852a3993, codeobj_cab2371a5f8deb3b9df99981852a3993, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cab2371a5f8deb3b9df99981852a3993 = cache_frame_cab2371a5f8deb3b9df99981852a3993;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cab2371a5f8deb3b9df99981852a3993 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cab2371a5f8deb3b9df99981852a3993 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_show_file );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_image );
        tmp_args_element_name_1 = par_image;
        frame_cab2371a5f8deb3b9df99981852a3993->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_save_image, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 97;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_options );
        tmp_dircall_arg3_1 = par_options;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cab2371a5f8deb3b9df99981852a3993 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cab2371a5f8deb3b9df99981852a3993 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cab2371a5f8deb3b9df99981852a3993 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cab2371a5f8deb3b9df99981852a3993, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cab2371a5f8deb3b9df99981852a3993->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cab2371a5f8deb3b9df99981852a3993, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cab2371a5f8deb3b9df99981852a3993,
        type_description_1,
        par_self,
        par_image,
        par_options
    );


    // Release cached frame.
    if ( frame_cab2371a5f8deb3b9df99981852a3993 == cache_frame_cab2371a5f8deb3b9df99981852a3993 )
    {
        Py_DECREF( frame_cab2371a5f8deb3b9df99981852a3993 );
    }
    cache_frame_cab2371a5f8deb3b9df99981852a3993 = NULL;

    assertFrameObject( frame_cab2371a5f8deb3b9df99981852a3993 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_7_show_image );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_7_show_image );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_8_show_file( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_dee868658f2b95885999e169bc5e41c3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dee868658f2b95885999e169bc5e41c3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dee868658f2b95885999e169bc5e41c3, codeobj_dee868658f2b95885999e169bc5e41c3, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_dee868658f2b95885999e169bc5e41c3 = cache_frame_dee868658f2b95885999e169bc5e41c3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dee868658f2b95885999e169bc5e41c3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dee868658f2b95885999e169bc5e41c3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_system );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get_command );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 101;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_file );
        tmp_tuple_element_1 = par_file;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_options );
        tmp_dircall_arg3_1 = par_options;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_args_element_name_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 101;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_dee868658f2b95885999e169bc5e41c3->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dee868658f2b95885999e169bc5e41c3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dee868658f2b95885999e169bc5e41c3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dee868658f2b95885999e169bc5e41c3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dee868658f2b95885999e169bc5e41c3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dee868658f2b95885999e169bc5e41c3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dee868658f2b95885999e169bc5e41c3,
        type_description_1,
        par_self,
        par_file,
        par_options
    );


    // Release cached frame.
    if ( frame_dee868658f2b95885999e169bc5e41c3 == cache_frame_dee868658f2b95885999e169bc5e41c3 )
    {
        Py_DECREF( frame_dee868658f2b95885999e169bc5e41c3 );
    }
    cache_frame_dee868658f2b95885999e169bc5e41c3 = NULL;

    assertFrameObject( frame_dee868658f2b95885999e169bc5e41c3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = const_int_pos_1;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_8_show_file );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_8_show_file );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_9_get_command( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_900997e66616405bb67e8e18e3401df0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_900997e66616405bb67e8e18e3401df0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_900997e66616405bb67e8e18e3401df0, codeobj_900997e66616405bb67e8e18e3401df0, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_900997e66616405bb67e8e18e3401df0 = cache_frame_900997e66616405bb67e8e18e3401df0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_900997e66616405bb67e8e18e3401df0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_900997e66616405bb67e8e18e3401df0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        tmp_left_name_1 = const_str_digest_270fcf9c829b97bd437d7f5bd4e89845;
        CHECK_OBJECT( par_file );
        tmp_tuple_element_1 = par_file;
        tmp_right_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_file );
        tmp_tuple_element_1 = par_file;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_900997e66616405bb67e8e18e3401df0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_900997e66616405bb67e8e18e3401df0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_900997e66616405bb67e8e18e3401df0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_900997e66616405bb67e8e18e3401df0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_900997e66616405bb67e8e18e3401df0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_900997e66616405bb67e8e18e3401df0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_900997e66616405bb67e8e18e3401df0,
        type_description_1,
        par_self,
        par_file,
        par_options
    );


    // Release cached frame.
    if ( frame_900997e66616405bb67e8e18e3401df0 == cache_frame_900997e66616405bb67e8e18e3401df0 )
    {
        Py_DECREF( frame_900997e66616405bb67e8e18e3401df0 );
    }
    cache_frame_900997e66616405bb67e8e18e3401df0 = NULL;

    assertFrameObject( frame_900997e66616405bb67e8e18e3401df0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_9_get_command );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_9_get_command );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_10_get_command( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *var_command = NULL;
    struct Nuitka_FrameObject *frame_9562bfe5aabee73f406826ce2558b031;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9562bfe5aabee73f406826ce2558b031 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9562bfe5aabee73f406826ce2558b031, codeobj_9562bfe5aabee73f406826ce2558b031, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9562bfe5aabee73f406826ce2558b031 = cache_frame_9562bfe5aabee73f406826ce2558b031;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9562bfe5aabee73f406826ce2558b031 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9562bfe5aabee73f406826ce2558b031 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_left_name_1 = const_str_digest_7acd952a9f67b5e9ea004b6ea77e6331;
        tmp_tuple_element_1 = const_str_digest_e3456b1263890745563ca15add33fa1e;
        tmp_right_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_quote );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "quote" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_file );
        tmp_args_element_name_1 = par_file;
        frame_9562bfe5aabee73f406826ce2558b031->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 129;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_quote );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "quote" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_file );
        tmp_args_element_name_2 = par_file;
        frame_9562bfe5aabee73f406826ce2558b031->m_frame.f_lineno = 130;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 130;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        tmp_assign_source_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_command == NULL );
        var_command = tmp_assign_source_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9562bfe5aabee73f406826ce2558b031 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9562bfe5aabee73f406826ce2558b031 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9562bfe5aabee73f406826ce2558b031, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9562bfe5aabee73f406826ce2558b031->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9562bfe5aabee73f406826ce2558b031, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9562bfe5aabee73f406826ce2558b031,
        type_description_1,
        par_self,
        par_file,
        par_options,
        var_command
    );


    // Release cached frame.
    if ( frame_9562bfe5aabee73f406826ce2558b031 == cache_frame_9562bfe5aabee73f406826ce2558b031 )
    {
        Py_DECREF( frame_9562bfe5aabee73f406826ce2558b031 );
    }
    cache_frame_9562bfe5aabee73f406826ce2558b031 = NULL;

    assertFrameObject( frame_9562bfe5aabee73f406826ce2558b031 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_command );
    tmp_return_value = var_command;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_10_get_command );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_command );
    Py_DECREF( var_command );
    var_command = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_10_get_command );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_11_show_file( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *var_fd = NULL;
    PyObject *var_path = NULL;
    PyObject *var_f = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    PyObject *tmp_with_2__enter = NULL;
    PyObject *tmp_with_2__exit = NULL;
    nuitka_bool tmp_with_2__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_2__source = NULL;
    struct Nuitka_FrameObject *frame_e9430fb60134b32275873c29099f45d1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    static struct Nuitka_FrameObject *cache_frame_e9430fb60134b32275873c29099f45d1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e9430fb60134b32275873c29099f45d1, codeobj_e9430fb60134b32275873c29099f45d1, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e9430fb60134b32275873c29099f45d1 = cache_frame_e9430fb60134b32275873c29099f45d1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e9430fb60134b32275873c29099f45d1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e9430fb60134b32275873c29099f45d1 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_tempfile );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tempfile );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tempfile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 135;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_mkstemp );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 135;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 135;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 135;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 135;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_fd == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_fd = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_path == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_path = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_fd );
        tmp_args_element_name_1 = var_fd;
        tmp_args_element_name_2 = const_str_plain_w;
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 136;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_fdopen, call_args );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_1 = tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 136;
        tmp_assign_source_7 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_2 = tmp_with_1__source;
        tmp_assign_source_8 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_8;
    }
    {
        nuitka_bool tmp_assign_source_9;
        tmp_assign_source_9 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_10 = tmp_with_1__enter;
        assert( var_f == NULL );
        Py_INCREF( tmp_assign_source_10 );
        var_f = tmp_assign_source_10;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( var_f );
        tmp_called_instance_3 = var_f;
        CHECK_OBJECT( par_file );
        tmp_args_element_name_3 = par_file;
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_write, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "oooooo";
            goto try_except_handler_6;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_e9430fb60134b32275873c29099f45d1, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_e9430fb60134b32275873c29099f45d1, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "oooooo";
            goto try_except_handler_7;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_11;
            tmp_assign_source_11 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_11;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_2 = tmp_with_1__exit;
            tmp_args_element_name_4 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_5 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_6 = EXC_TRACEBACK(PyThreadState_GET());
            frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 137;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "oooooo";
                goto try_except_handler_7;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "oooooo";
                goto try_except_handler_7;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 137;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_e9430fb60134b32275873c29099f45d1->m_frame) frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooo";
            goto try_except_handler_7;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 136;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_e9430fb60134b32275873c29099f45d1->m_frame) frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooo";
        goto try_except_handler_7;
        branch_end_1:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_5;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_11_show_file );
    return NULL;
    // End of try:
    try_end_3:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_3 = tmp_with_1__exit;
            frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 137;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_5 );
                Py_XDECREF( exception_keeper_value_5 );
                Py_XDECREF( exception_keeper_tb_5 );

                exception_lineno = 137;
                type_description_1 = "oooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_4;
    // End of try:
    try_end_5:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 137;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "oooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_open_filename_1;
        PyObject *tmp_open_mode_1;
        CHECK_OBJECT( var_path );
        tmp_open_filename_1 = var_path;
        tmp_open_mode_1 = const_str_plain_r;
        tmp_assign_source_12 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooooo";
            goto try_except_handler_8;
        }
        assert( tmp_with_2__source == NULL );
        tmp_with_2__source = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_with_2__source );
        tmp_source_name_3 = tmp_with_2__source;
        tmp_called_name_5 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___enter__ );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooooo";
            goto try_except_handler_8;
        }
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 138;
        tmp_assign_source_13 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
        Py_DECREF( tmp_called_name_5 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooooo";
            goto try_except_handler_8;
        }
        assert( tmp_with_2__enter == NULL );
        tmp_with_2__enter = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( tmp_with_2__source );
        tmp_source_name_4 = tmp_with_2__source;
        tmp_assign_source_14 = LOOKUP_SPECIAL( tmp_source_name_4, const_str_plain___exit__ );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooooo";
            goto try_except_handler_8;
        }
        assert( tmp_with_2__exit == NULL );
        tmp_with_2__exit = tmp_assign_source_14;
    }
    {
        nuitka_bool tmp_assign_source_15;
        tmp_assign_source_15 = NUITKA_BOOL_TRUE;
        tmp_with_2__indicator = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        CHECK_OBJECT( tmp_with_2__enter );
        tmp_assign_source_16 = tmp_with_2__enter;
        {
            PyObject *old = var_f;
            assert( old != NULL );
            var_f = tmp_assign_source_16;
            Py_INCREF( var_f );
            Py_DECREF( old );
        }

    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;
            type_description_1 = "oooooo";
            goto try_except_handler_10;
        }

        tmp_source_name_5 = tmp_mvar_value_3;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Popen );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oooooo";
            goto try_except_handler_10;
        }
        tmp_args_name_1 = DEEP_COPY( const_tuple_list_str_digest_ca0a3cf3a1e53a881cfc0cf62abe8b3e_list_tuple );
        tmp_dict_key_1 = const_str_plain_shell;
        tmp_dict_value_1 = Py_True;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_stdin;
        CHECK_OBJECT( var_f );
        tmp_dict_value_2 = var_f;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 139;
        tmp_call_result_4 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oooooo";
            goto try_except_handler_10;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_e9430fb60134b32275873c29099f45d1, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_e9430fb60134b32275873c29099f45d1, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_4 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooooo";
            goto try_except_handler_11;
        }
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            nuitka_bool tmp_assign_source_17;
            tmp_assign_source_17 = NUITKA_BOOL_FALSE;
            tmp_with_2__indicator = tmp_assign_source_17;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_7 = tmp_with_2__exit;
            tmp_args_element_name_7 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_8 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_9 = EXC_TRACEBACK(PyThreadState_GET());
            frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 144;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
            }

            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooooo";
                goto try_except_handler_11;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooooo";
                goto try_except_handler_11;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 144;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_e9430fb60134b32275873c29099f45d1->m_frame) frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooo";
            goto try_except_handler_11;
            branch_no_6:;
        }
        goto branch_end_5;
        branch_no_5:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 138;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_e9430fb60134b32275873c29099f45d1->m_frame) frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooo";
        goto try_except_handler_11;
        branch_end_5:;
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_9;
    // End of try:
    try_end_8:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_7;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_11_show_file );
    return NULL;
    // End of try:
    try_end_7:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_7;
        nuitka_bool tmp_compexpr_left_5;
        nuitka_bool tmp_compexpr_right_5;
        assert( tmp_with_2__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_5 = tmp_with_2__indicator;
        tmp_compexpr_right_5 = NUITKA_BOOL_TRUE;
        tmp_condition_result_7 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_8 = tmp_with_2__exit;
            frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 144;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_9 );
                Py_XDECREF( exception_keeper_value_9 );
                Py_XDECREF( exception_keeper_tb_9 );

                exception_lineno = 144;
                type_description_1 = "oooooo";
                goto try_except_handler_8;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_7:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto try_except_handler_8;
    // End of try:
    try_end_9:;
    {
        nuitka_bool tmp_condition_result_8;
        nuitka_bool tmp_compexpr_left_6;
        nuitka_bool tmp_compexpr_right_6;
        assert( tmp_with_2__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_6 = tmp_with_2__indicator;
        tmp_compexpr_right_6 = NUITKA_BOOL_TRUE;
        tmp_condition_result_8 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_called_name_9;
            PyObject *tmp_call_result_6;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_9 = tmp_with_2__exit;
            frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 144;
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 144;
                type_description_1 = "oooooo";
                goto try_except_handler_8;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        branch_no_8:;
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    Py_XDECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    Py_XDECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_with_2__source );
    Py_DECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__enter );
    Py_DECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__exit );
    Py_DECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_4;
        CHECK_OBJECT( var_path );
        tmp_args_element_name_10 = var_path;
        frame_e9430fb60134b32275873c29099f45d1->m_frame.f_lineno = 145;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_remove, call_args );
        }

        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9430fb60134b32275873c29099f45d1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9430fb60134b32275873c29099f45d1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e9430fb60134b32275873c29099f45d1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e9430fb60134b32275873c29099f45d1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e9430fb60134b32275873c29099f45d1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e9430fb60134b32275873c29099f45d1,
        type_description_1,
        par_self,
        par_file,
        par_options,
        var_fd,
        var_path,
        var_f
    );


    // Release cached frame.
    if ( frame_e9430fb60134b32275873c29099f45d1 == cache_frame_e9430fb60134b32275873c29099f45d1 )
    {
        Py_DECREF( frame_e9430fb60134b32275873c29099f45d1 );
    }
    cache_frame_e9430fb60134b32275873c29099f45d1 = NULL;

    assertFrameObject( frame_e9430fb60134b32275873c29099f45d1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = const_int_pos_1;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_11_show_file );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_fd );
    Py_DECREF( var_fd );
    var_fd = NULL;

    CHECK_OBJECT( (PyObject *)var_path );
    Py_DECREF( var_path );
    var_path = NULL;

    CHECK_OBJECT( (PyObject *)var_f );
    Py_DECREF( var_f );
    var_f = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_11_show_file );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_12_which( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_executable = python_pars[ 0 ];
    PyObject *var_path = NULL;
    PyObject *var_dirname = NULL;
    PyObject *var_filename = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_65a6ef7c15d54ec2fdf30736098ea855;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_65a6ef7c15d54ec2fdf30736098ea855 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_65a6ef7c15d54ec2fdf30736098ea855, codeobj_65a6ef7c15d54ec2fdf30736098ea855, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_65a6ef7c15d54ec2fdf30736098ea855 = cache_frame_65a6ef7c15d54ec2fdf30736098ea855;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_65a6ef7c15d54ec2fdf30736098ea855 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_65a6ef7c15d54ec2fdf30736098ea855 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_environ );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_65a6ef7c15d54ec2fdf30736098ea855->m_frame.f_lineno = 155;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_PATH_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_path == NULL );
        var_path = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_path );
        tmp_operand_name_1 = var_path;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_path );
        tmp_source_name_2 = var_path;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_split );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_2;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pathsep );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 158;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_65a6ef7c15d54ec2fdf30736098ea855->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 158;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_dirname;
            var_dirname = tmp_assign_source_4;
            Py_INCREF( var_dirname );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_source_name_4 = tmp_mvar_value_3;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_path );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_dirname );
        tmp_args_element_name_2 = var_dirname;
        CHECK_OBJECT( par_executable );
        tmp_args_element_name_3 = par_executable;
        frame_65a6ef7c15d54ec2fdf30736098ea855->m_frame.f_lineno = 159;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_5 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_join, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_filename;
            var_filename = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_4;
        int tmp_truth_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_6;
        int tmp_truth_name_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_source_name_5 = tmp_mvar_value_4;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_path );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_filename );
        tmp_args_element_name_4 = var_filename;
        frame_65a6ef7c15d54ec2fdf30736098ea855->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_isfile, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_source_name_6 = tmp_mvar_value_5;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_access );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_filename );
        tmp_args_element_name_5 = var_filename;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_source_name_7 = tmp_mvar_value_6;
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_X_OK );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_65a6ef7c15d54ec2fdf30736098ea855->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( var_filename );
        tmp_return_value = var_filename;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_2;
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 158;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_65a6ef7c15d54ec2fdf30736098ea855 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_65a6ef7c15d54ec2fdf30736098ea855 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_65a6ef7c15d54ec2fdf30736098ea855 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_65a6ef7c15d54ec2fdf30736098ea855, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_65a6ef7c15d54ec2fdf30736098ea855->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_65a6ef7c15d54ec2fdf30736098ea855, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_65a6ef7c15d54ec2fdf30736098ea855,
        type_description_1,
        par_executable,
        var_path,
        var_dirname,
        var_filename
    );


    // Release cached frame.
    if ( frame_65a6ef7c15d54ec2fdf30736098ea855 == cache_frame_65a6ef7c15d54ec2fdf30736098ea855 )
    {
        Py_DECREF( frame_65a6ef7c15d54ec2fdf30736098ea855 );
    }
    cache_frame_65a6ef7c15d54ec2fdf30736098ea855 = NULL;

    assertFrameObject( frame_65a6ef7c15d54ec2fdf30736098ea855 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_12_which );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_executable );
    Py_DECREF( par_executable );
    par_executable = NULL;

    CHECK_OBJECT( (PyObject *)var_path );
    Py_DECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_dirname );
    var_dirname = NULL;

    Py_XDECREF( var_filename );
    var_filename = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_executable );
    Py_DECREF( par_executable );
    par_executable = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_dirname );
    var_dirname = NULL;

    Py_XDECREF( var_filename );
    var_filename = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_12_which );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_13_get_command( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *var_command = NULL;
    struct Nuitka_FrameObject *frame_4fed9cd01f7a7d6bebe26a0c71b200df;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_4fed9cd01f7a7d6bebe26a0c71b200df = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4fed9cd01f7a7d6bebe26a0c71b200df, codeobj_4fed9cd01f7a7d6bebe26a0c71b200df, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4fed9cd01f7a7d6bebe26a0c71b200df = cache_frame_4fed9cd01f7a7d6bebe26a0c71b200df;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4fed9cd01f7a7d6bebe26a0c71b200df );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4fed9cd01f7a7d6bebe26a0c71b200df ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_command_ex );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_file );
        tmp_tuple_element_1 = par_file;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_options );
        tmp_dircall_arg3_1 = par_options;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_subscribed_name_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_command == NULL );
        var_command = tmp_assign_source_1;
    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_left_name_1 = const_str_digest_0d9b4a629a2a7746be32401c3593b928;
        CHECK_OBJECT( var_command );
        tmp_tuple_element_2 = var_command;
        tmp_right_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_quote );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "quote" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_file );
        tmp_args_element_name_1 = par_file;
        frame_4fed9cd01f7a7d6bebe26a0c71b200df->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 170;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_quote );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "quote" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_file );
        tmp_args_element_name_2 = par_file;
        frame_4fed9cd01f7a7d6bebe26a0c71b200df->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_tuple_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 170;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_2 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fed9cd01f7a7d6bebe26a0c71b200df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fed9cd01f7a7d6bebe26a0c71b200df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fed9cd01f7a7d6bebe26a0c71b200df );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4fed9cd01f7a7d6bebe26a0c71b200df, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4fed9cd01f7a7d6bebe26a0c71b200df->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4fed9cd01f7a7d6bebe26a0c71b200df, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4fed9cd01f7a7d6bebe26a0c71b200df,
        type_description_1,
        par_self,
        par_file,
        par_options,
        var_command
    );


    // Release cached frame.
    if ( frame_4fed9cd01f7a7d6bebe26a0c71b200df == cache_frame_4fed9cd01f7a7d6bebe26a0c71b200df )
    {
        Py_DECREF( frame_4fed9cd01f7a7d6bebe26a0c71b200df );
    }
    cache_frame_4fed9cd01f7a7d6bebe26a0c71b200df = NULL;

    assertFrameObject( frame_4fed9cd01f7a7d6bebe26a0c71b200df );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_13_get_command );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_command );
    Py_DECREF( var_command );
    var_command = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    Py_XDECREF( var_command );
    var_command = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_13_get_command );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_14_show_file( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *var_fd = NULL;
    PyObject *var_path = NULL;
    PyObject *var_f = NULL;
    PyObject *var_command = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    PyObject *tmp_with_2__enter = NULL;
    PyObject *tmp_with_2__exit = NULL;
    nuitka_bool tmp_with_2__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_2__source = NULL;
    struct Nuitka_FrameObject *frame_e62a848cc795bd9659efa4c2ed88fed5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    static struct Nuitka_FrameObject *cache_frame_e62a848cc795bd9659efa4c2ed88fed5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e62a848cc795bd9659efa4c2ed88fed5, codeobj_e62a848cc795bd9659efa4c2ed88fed5, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e62a848cc795bd9659efa4c2ed88fed5 = cache_frame_e62a848cc795bd9659efa4c2ed88fed5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e62a848cc795bd9659efa4c2ed88fed5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e62a848cc795bd9659efa4c2ed88fed5 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_tempfile );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tempfile );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tempfile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 174;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_mkstemp );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 174;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 174;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooo";
                    exception_lineno = 174;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooo";
            exception_lineno = 174;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_fd == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_fd = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_path == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_path = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        CHECK_OBJECT( var_fd );
        tmp_args_element_name_1 = var_fd;
        tmp_args_element_name_2 = const_str_plain_w;
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_fdopen, call_args );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_1 = tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 175;
        tmp_assign_source_7 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_2 = tmp_with_1__source;
        tmp_assign_source_8 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooooo";
            goto try_except_handler_4;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_8;
    }
    {
        nuitka_bool tmp_assign_source_9;
        tmp_assign_source_9 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_10 = tmp_with_1__enter;
        assert( var_f == NULL );
        Py_INCREF( tmp_assign_source_10 );
        var_f = tmp_assign_source_10;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( var_f );
        tmp_called_instance_3 = var_f;
        CHECK_OBJECT( par_file );
        tmp_args_element_name_3 = par_file;
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 176;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_write, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_e62a848cc795bd9659efa4c2ed88fed5, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_e62a848cc795bd9659efa4c2ed88fed5, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "ooooooo";
            goto try_except_handler_7;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_11;
            tmp_assign_source_11 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_11;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_2 = tmp_with_1__exit;
            tmp_args_element_name_4 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_5 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_6 = EXC_TRACEBACK(PyThreadState_GET());
            frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 176;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooooooo";
                goto try_except_handler_7;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooooooo";
                goto try_except_handler_7;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 176;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame) frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooo";
            goto try_except_handler_7;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 175;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame) frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooo";
        goto try_except_handler_7;
        branch_end_1:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_5;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_14_show_file );
    return NULL;
    // End of try:
    try_end_3:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_3 = tmp_with_1__exit;
            frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 176;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_5 );
                Py_XDECREF( exception_keeper_value_5 );
                Py_XDECREF( exception_keeper_tb_5 );

                exception_lineno = 176;
                type_description_1 = "ooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_4;
    // End of try:
    try_end_5:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 176;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_open_filename_1;
        PyObject *tmp_open_mode_1;
        CHECK_OBJECT( var_path );
        tmp_open_filename_1 = var_path;
        tmp_open_mode_1 = const_str_plain_r;
        tmp_assign_source_12 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooooooo";
            goto try_except_handler_8;
        }
        assert( tmp_with_2__source == NULL );
        tmp_with_2__source = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_with_2__source );
        tmp_source_name_3 = tmp_with_2__source;
        tmp_called_name_5 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___enter__ );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooooooo";
            goto try_except_handler_8;
        }
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 177;
        tmp_assign_source_13 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
        Py_DECREF( tmp_called_name_5 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooooooo";
            goto try_except_handler_8;
        }
        assert( tmp_with_2__enter == NULL );
        tmp_with_2__enter = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( tmp_with_2__source );
        tmp_source_name_4 = tmp_with_2__source;
        tmp_assign_source_14 = LOOKUP_SPECIAL( tmp_source_name_4, const_str_plain___exit__ );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooooooo";
            goto try_except_handler_8;
        }
        assert( tmp_with_2__exit == NULL );
        tmp_with_2__exit = tmp_assign_source_14;
    }
    {
        nuitka_bool tmp_assign_source_15;
        tmp_assign_source_15 = NUITKA_BOOL_TRUE;
        tmp_with_2__indicator = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        CHECK_OBJECT( tmp_with_2__enter );
        tmp_assign_source_16 = tmp_with_2__enter;
        {
            PyObject *old = var_f;
            assert( old != NULL );
            var_f = tmp_assign_source_16;
            Py_INCREF( var_f );
            Py_DECREF( old );
        }

    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_get_command_ex );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        CHECK_OBJECT( par_file );
        tmp_tuple_element_1 = par_file;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_options );
        tmp_dircall_arg3_1 = par_options;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_subscribed_name_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_assign_source_17 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        assert( var_command == NULL );
        var_command = tmp_assign_source_17;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }

        tmp_source_name_6 = tmp_mvar_value_3;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_Popen );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        tmp_left_name_2 = const_str_digest_dbaa2ae3e0fa649b7a8fa121daf2a7e8;
        CHECK_OBJECT( var_command );
        tmp_right_name_1 = var_command;
        tmp_left_name_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 180;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        tmp_right_name_2 = const_str_digest_6a0da6d3c6b355e8363dcc08366ad154;
        tmp_list_element_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 181;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        tmp_tuple_element_2 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_tuple_element_2, 0, tmp_list_element_1 );
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
        tmp_dict_key_1 = const_str_plain_shell;
        tmp_dict_value_1 = Py_True;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_stdin;
        CHECK_OBJECT( var_f );
        tmp_dict_value_2 = var_f;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 179;
        tmp_call_result_4 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooooooo";
            goto try_except_handler_10;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_e62a848cc795bd9659efa4c2ed88fed5, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_e62a848cc795bd9659efa4c2ed88fed5, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_4 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooooooo";
            goto try_except_handler_11;
        }
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            nuitka_bool tmp_assign_source_18;
            tmp_assign_source_18 = NUITKA_BOOL_FALSE;
            tmp_with_2__indicator = tmp_assign_source_18;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_7 = tmp_with_2__exit;
            tmp_args_element_name_7 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_8 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_9 = EXC_TRACEBACK(PyThreadState_GET());
            frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 183;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
            }

            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_1 = "ooooooo";
                goto try_except_handler_11;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_1 = "ooooooo";
                goto try_except_handler_11;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 183;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame) frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooo";
            goto try_except_handler_11;
            branch_no_6:;
        }
        goto branch_end_5;
        branch_no_5:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 177;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame) frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooo";
        goto try_except_handler_11;
        branch_end_5:;
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_9;
    // End of try:
    try_end_8:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_7;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_14_show_file );
    return NULL;
    // End of try:
    try_end_7:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_7;
        nuitka_bool tmp_compexpr_left_5;
        nuitka_bool tmp_compexpr_right_5;
        assert( tmp_with_2__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_5 = tmp_with_2__indicator;
        tmp_compexpr_right_5 = NUITKA_BOOL_TRUE;
        tmp_condition_result_7 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_8 = tmp_with_2__exit;
            frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 183;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_9 );
                Py_XDECREF( exception_keeper_value_9 );
                Py_XDECREF( exception_keeper_tb_9 );

                exception_lineno = 183;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_7:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto try_except_handler_8;
    // End of try:
    try_end_9:;
    {
        nuitka_bool tmp_condition_result_8;
        nuitka_bool tmp_compexpr_left_6;
        nuitka_bool tmp_compexpr_right_6;
        assert( tmp_with_2__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_6 = tmp_with_2__indicator;
        tmp_compexpr_right_6 = NUITKA_BOOL_TRUE;
        tmp_condition_result_8 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_called_name_9;
            PyObject *tmp_call_result_6;
            CHECK_OBJECT( tmp_with_2__exit );
            tmp_called_name_9 = tmp_with_2__exit;
            frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 183;
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        branch_no_8:;
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    Py_XDECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    Py_XDECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_with_2__source );
    Py_DECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__enter );
    Py_DECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__exit );
    Py_DECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_4;
        CHECK_OBJECT( var_path );
        tmp_args_element_name_10 = var_path;
        frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame.f_lineno = 184;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_remove, call_args );
        }

        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 184;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e62a848cc795bd9659efa4c2ed88fed5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e62a848cc795bd9659efa4c2ed88fed5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e62a848cc795bd9659efa4c2ed88fed5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e62a848cc795bd9659efa4c2ed88fed5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e62a848cc795bd9659efa4c2ed88fed5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e62a848cc795bd9659efa4c2ed88fed5,
        type_description_1,
        par_self,
        par_file,
        par_options,
        var_fd,
        var_path,
        var_f,
        var_command
    );


    // Release cached frame.
    if ( frame_e62a848cc795bd9659efa4c2ed88fed5 == cache_frame_e62a848cc795bd9659efa4c2ed88fed5 )
    {
        Py_DECREF( frame_e62a848cc795bd9659efa4c2ed88fed5 );
    }
    cache_frame_e62a848cc795bd9659efa4c2ed88fed5 = NULL;

    assertFrameObject( frame_e62a848cc795bd9659efa4c2ed88fed5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = const_int_pos_1;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_14_show_file );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_fd );
    Py_DECREF( var_fd );
    var_fd = NULL;

    CHECK_OBJECT( (PyObject *)var_path );
    Py_DECREF( var_path );
    var_path = NULL;

    CHECK_OBJECT( (PyObject *)var_f );
    Py_DECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_command );
    var_command = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    Py_XDECREF( var_path );
    var_path = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_command );
    var_command = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_14_show_file );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_15_get_command_ex( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_tuple_str_plain_display_str_plain_display_tuple;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_15_get_command_ex );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_15_get_command_ex );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_16_get_command_ex( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_options = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_tuple_str_plain_eog_str_plain_eog_tuple;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_16_get_command_ex );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_16_get_command_ex );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageShow$$$function_17_get_command_ex( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_file = python_pars[ 1 ];
    PyObject *par_title = python_pars[ 2 ];
    PyObject *par_options = python_pars[ 3 ];
    PyObject *var_command = NULL;
    struct Nuitka_FrameObject *frame_293b13abf68fb62da82aed03bd26e861;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_293b13abf68fb62da82aed03bd26e861 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_plain_xv;
        assert( var_command == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_command = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_293b13abf68fb62da82aed03bd26e861, codeobj_293b13abf68fb62da82aed03bd26e861, module_PIL$ImageShow, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_293b13abf68fb62da82aed03bd26e861 = cache_frame_293b13abf68fb62da82aed03bd26e861;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_293b13abf68fb62da82aed03bd26e861 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_293b13abf68fb62da82aed03bd26e861 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_title );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_title );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_1 = "oooooN";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            tmp_left_name_1 = const_str_plain_xv;
            tmp_left_name_2 = const_str_digest_b7452332cb96748e6cd13493307ccf8e;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_quote );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "quote" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_title );
            tmp_args_element_name_1 = par_title;
            frame_293b13abf68fb62da82aed03bd26e861->m_frame.f_lineno = 211;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_right_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "oooooN";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_command;
                assert( old != NULL );
                var_command = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_293b13abf68fb62da82aed03bd26e861 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_293b13abf68fb62da82aed03bd26e861 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_293b13abf68fb62da82aed03bd26e861, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_293b13abf68fb62da82aed03bd26e861->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_293b13abf68fb62da82aed03bd26e861, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_293b13abf68fb62da82aed03bd26e861,
        type_description_1,
        par_self,
        par_file,
        par_title,
        par_options,
        var_command,
        NULL
    );


    // Release cached frame.
    if ( frame_293b13abf68fb62da82aed03bd26e861 == cache_frame_293b13abf68fb62da82aed03bd26e861 )
    {
        Py_DECREF( frame_293b13abf68fb62da82aed03bd26e861 );
    }
    cache_frame_293b13abf68fb62da82aed03bd26e861 = NULL;

    assertFrameObject( frame_293b13abf68fb62da82aed03bd26e861 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_command );
        tmp_tuple_element_1 = var_command;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_str_plain_xv;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_17_get_command_ex );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_command );
    Py_DECREF( var_command );
    var_command = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_file );
    Py_DECREF( par_file );
    par_file = NULL;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    CHECK_OBJECT( (PyObject *)par_options );
    Py_DECREF( par_options );
    par_options = NULL;

    CHECK_OBJECT( (PyObject *)var_command );
    Py_DECREF( var_command );
    var_command = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageShow$$$function_17_get_command_ex );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_10_get_command(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_10_get_command,
        const_str_plain_get_command,
#if PYTHON_VERSION >= 300
        const_str_digest_6216e4740b26b17fa88e334290e7fc51,
#endif
        codeobj_9562bfe5aabee73f406826ce2558b031,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_11_show_file(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_11_show_file,
        const_str_plain_show_file,
#if PYTHON_VERSION >= 300
        const_str_digest_0656acedc222e54acce2b17f7460767e,
#endif
        codeobj_e9430fb60134b32275873c29099f45d1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_1cdf033b8c960667a939c36353e86b65,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_12_which(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_12_which,
        const_str_plain_which,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_65a6ef7c15d54ec2fdf30736098ea855,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_13_get_command(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_13_get_command,
        const_str_plain_get_command,
#if PYTHON_VERSION >= 300
        const_str_digest_63c0ab1ab33a4c5210275e0c2fcbd817,
#endif
        codeobj_4fed9cd01f7a7d6bebe26a0c71b200df,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_14_show_file(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_14_show_file,
        const_str_plain_show_file,
#if PYTHON_VERSION >= 300
        const_str_digest_f0c978d81af299905560b512e18af135,
#endif
        codeobj_e62a848cc795bd9659efa4c2ed88fed5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_1cdf033b8c960667a939c36353e86b65,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_15_get_command_ex(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_15_get_command_ex,
        const_str_plain_get_command_ex,
#if PYTHON_VERSION >= 300
        const_str_digest_7a4dc08fb4b4a7d54d583752f8679aac,
#endif
        codeobj_4feee8de771cab4b7f65e5b6826feaa2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_16_get_command_ex(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_16_get_command_ex,
        const_str_plain_get_command_ex,
#if PYTHON_VERSION >= 300
        const_str_digest_6c9971e8ae6af8026f9683ca60f2364b,
#endif
        codeobj_24f67ce2a7d6b1b4e483b18cfb0665a7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_17_get_command_ex( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_17_get_command_ex,
        const_str_plain_get_command_ex,
#if PYTHON_VERSION >= 300
        const_str_digest_435ac3f107734ce2e0e8e4f835e273fa,
#endif
        codeobj_293b13abf68fb62da82aed03bd26e861,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_1_register( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_1_register,
        const_str_plain_register,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_64e98e70326e906274a0a1f3b595209e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_2_show( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_2_show,
        const_str_plain_show,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_98caddbffe284a15e4482c8d4eaba007,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_082cef2a0b23aebc75dc4d2069eabf0b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_3_show(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_3_show,
        const_str_plain_show,
#if PYTHON_VERSION >= 300
        const_str_digest_f033dfdf46b40ce829d63ef28b2f10f7,
#endif
        codeobj_7b8b283b22ea14de67fd17cc47f1d83d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_4_get_format(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_4_get_format,
        const_str_plain_get_format,
#if PYTHON_VERSION >= 300
        const_str_digest_6903407ee41e83e6ca72d5d2af95bb42,
#endif
        codeobj_f309a2bd401499e67876b8f2bd30a70d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_f8c87d375a84d8cdcf7d6314f8a47ada,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_5_get_command(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_5_get_command,
        const_str_plain_get_command,
#if PYTHON_VERSION >= 300
        const_str_digest_38ffab005772e67b8b735166edbf1513,
#endif
        codeobj_b91a0df666fb7462de6ece8dbe0acfaa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_6_save_image(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_6_save_image,
        const_str_plain_save_image,
#if PYTHON_VERSION >= 300
        const_str_digest_7e2f4256bad75e8a09424ead68261b92,
#endif
        codeobj_c3c436a7550ca9f95df58c4ea3455b83,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_f2893365b8649be4fd37f1e45f30ea7e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_7_show_image(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_7_show_image,
        const_str_plain_show_image,
#if PYTHON_VERSION >= 300
        const_str_digest_38f70f269aa451981712abba905303d0,
#endif
        codeobj_cab2371a5f8deb3b9df99981852a3993,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_0c9499ab8fc32c33c6e95a447660ccfc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_8_show_file(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_8_show_file,
        const_str_plain_show_file,
#if PYTHON_VERSION >= 300
        const_str_digest_abcbeb7ba7ac3747a1ee20c6dc91ef42,
#endif
        codeobj_dee868658f2b95885999e169bc5e41c3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        const_str_digest_1cdf033b8c960667a939c36353e86b65,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageShow$$$function_9_get_command(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageShow$$$function_9_get_command,
        const_str_plain_get_command,
#if PYTHON_VERSION >= 300
        const_str_digest_c3032212395a0fbc92ff111d65a8410a,
#endif
        codeobj_900997e66616405bb67e8e18e3401df0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageShow,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_PIL$ImageShow =
{
    PyModuleDef_HEAD_INIT,
    "PIL.ImageShow",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(PIL$ImageShow)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(PIL$ImageShow)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_PIL$ImageShow );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("PIL.ImageShow: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.ImageShow: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.ImageShow: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initPIL$ImageShow" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_PIL$ImageShow = Py_InitModule4(
        "PIL.ImageShow",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_PIL$ImageShow = PyModule_Create( &mdef_PIL$ImageShow );
#endif

    moduledict_PIL$ImageShow = MODULE_DICT( module_PIL$ImageShow );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_PIL$ImageShow,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_PIL$ImageShow,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$ImageShow,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$ImageShow,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_PIL$ImageShow );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_417b20c428ec11e5abc58f5b33589964, module_PIL$ImageShow );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *outline_5_var___class__ = NULL;
    PyObject *outline_6_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__bases_orig = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__bases_orig = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    PyObject *tmp_class_creation_6__bases = NULL;
    PyObject *tmp_class_creation_6__bases_orig = NULL;
    PyObject *tmp_class_creation_6__class_decl_dict = NULL;
    PyObject *tmp_class_creation_6__metaclass = NULL;
    PyObject *tmp_class_creation_6__prepared = NULL;
    PyObject *tmp_class_creation_7__bases = NULL;
    PyObject *tmp_class_creation_7__bases_orig = NULL;
    PyObject *tmp_class_creation_7__class_decl_dict = NULL;
    PyObject *tmp_class_creation_7__metaclass = NULL;
    PyObject *tmp_class_creation_7__prepared = NULL;
    struct Nuitka_FrameObject *frame_f8eb43222464beea8566d39e2743ee92;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_PIL$ImageShow_58 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *locals_PIL$ImageShow_109 = NULL;
    struct Nuitka_FrameObject *frame_3bcdf4be5ba30a7e1c42daff022047c0_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3bcdf4be5ba30a7e1c42daff022047c0_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *locals_PIL$ImageShow_121 = NULL;
    struct Nuitka_FrameObject *frame_dc73a6dbd4552d1ae48c358e48b98604_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_dc73a6dbd4552d1ae48c358e48b98604_4 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *locals_PIL$ImageShow_164 = NULL;
    struct Nuitka_FrameObject *frame_86e12a72b9b504adcb71f61e7d4a4a09_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_86e12a72b9b504adcb71f61e7d4a4a09_5 = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *locals_PIL$ImageShow_189 = NULL;
    struct Nuitka_FrameObject *frame_621adb69f1a7443e006c5aeb8b9b0b88_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_621adb69f1a7443e006c5aeb8b9b0b88_6 = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *locals_PIL$ImageShow_197 = NULL;
    struct Nuitka_FrameObject *frame_8d5a3a6c708045300f009fb324038521_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8d5a3a6c708045300f009fb324038521_7 = NULL;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *locals_PIL$ImageShow_205 = NULL;
    struct Nuitka_FrameObject *frame_dde79275ab8f4680c1f745554f06161e_8;
    NUITKA_MAY_BE_UNUSED char const *type_description_8 = NULL;
    static struct Nuitka_FrameObject *cache_frame_dde79275ab8f4680c1f745554f06161e_8 = NULL;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_f8eb43222464beea8566d39e2743ee92 = MAKE_MODULE_FRAME( codeobj_f8eb43222464beea8566d39e2743ee92, module_PIL$ImageShow );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_f8eb43222464beea8566d39e2743ee92 );
    assert( Py_REFCNT( frame_f8eb43222464beea8566d39e2743ee92 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 15;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_print_function );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_PIL;
        tmp_globals_name_1 = (PyObject *)moduledict_PIL$ImageShow;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Image_tuple;
        tmp_level_name_1 = const_int_0;
        frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 17;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Image );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Image, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_os;
        tmp_globals_name_2 = (PyObject *)moduledict_PIL$ImageShow;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 18;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_sys;
        tmp_globals_name_3 = (PyObject *)moduledict_PIL$ImageShow;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 19;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        assert( !(tmp_assign_source_7 == NULL) );
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_subprocess;
        tmp_globals_name_4 = (PyObject *)moduledict_PIL$ImageShow;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 20;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_subprocess, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_tempfile;
        tmp_globals_name_5 = (PyObject *)moduledict_PIL$ImageShow;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 21;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_tempfile, tmp_assign_source_9 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_2 = tmp_mvar_value_3;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version_info );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_major );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_3;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_import_name_from_3;
            PyObject *tmp_name_name_6;
            PyObject *tmp_globals_name_6;
            PyObject *tmp_locals_name_6;
            PyObject *tmp_fromlist_name_6;
            PyObject *tmp_level_name_6;
            tmp_name_name_6 = const_str_plain_shlex;
            tmp_globals_name_6 = (PyObject *)moduledict_PIL$ImageShow;
            tmp_locals_name_6 = Py_None;
            tmp_fromlist_name_6 = const_tuple_str_plain_quote_tuple;
            tmp_level_name_6 = const_int_0;
            frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 24;
            tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
            if ( tmp_import_name_from_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_quote );
            Py_DECREF( tmp_import_name_from_3 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote, tmp_assign_source_10 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_import_name_from_4;
            PyObject *tmp_name_name_7;
            PyObject *tmp_globals_name_7;
            PyObject *tmp_locals_name_7;
            PyObject *tmp_fromlist_name_7;
            PyObject *tmp_level_name_7;
            tmp_name_name_7 = const_str_plain_pipes;
            tmp_globals_name_7 = (PyObject *)moduledict_PIL$ImageShow;
            tmp_locals_name_7 = Py_None;
            tmp_fromlist_name_7 = const_tuple_str_plain_quote_tuple;
            tmp_level_name_7 = const_int_0;
            frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 26;
            tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
            if ( tmp_import_name_from_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 26;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_quote );
            Py_DECREF( tmp_import_name_from_4 );
            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 26;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_quote, tmp_assign_source_11 );
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = PyList_New( 0 );
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain__viewers, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_int_pos_1_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_13 = MAKE_FUNCTION_PIL$ImageShow$$$function_1_register( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_register, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_14 = MAKE_FUNCTION_PIL$ImageShow$$$function_2_show( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_show, tmp_assign_source_14 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_15 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_17 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_17;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_1;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_3 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_4 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;

                goto try_except_handler_1;
            }
            tmp_tuple_element_1 = const_str_plain_Viewer;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 58;
            tmp_assign_source_18 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;

                goto try_except_handler_1;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_18;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_5 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;

                goto try_except_handler_1;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_6;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;

                    goto try_except_handler_1;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_6 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_6 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_6 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 58;

                    goto try_except_handler_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;

                    goto try_except_handler_1;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 58;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_1;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_19;
            tmp_assign_source_19 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_19;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_20;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_PIL$ImageShow_58 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_digest_8dd48c9a9324b0d8c2ba699985151b49;
        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_plain_Viewer;
        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;

            goto try_except_handler_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2, codeobj_e1dcd8f1e1caf4be2e71097cbe9d7729, module_PIL$ImageShow, sizeof(void *) );
        frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 = cache_frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_3_show(  );



        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_show, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_format, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = PyDict_New();
        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_options, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_4_get_format(  );



        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_get_format, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_5_get_command(  );



        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_get_command, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_6_save_image(  );



        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_save_image, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_7_show_image(  );



        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_show_image, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_8_show_file(  );



        tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain_show_file, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 == cache_frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 )
        {
            Py_DECREF( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 );
        }
        cache_frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 = NULL;

        assertFrameObject( frame_e1dcd8f1e1caf4be2e71097cbe9d7729_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_3;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_2 = tmp_class_creation_1__bases;
            tmp_compexpr_right_2 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;

                goto try_except_handler_3;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_PIL$ImageShow_58, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;

                goto try_except_handler_3;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_Viewer;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_PIL$ImageShow_58;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 58;
            tmp_assign_source_21 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 58;

                goto try_except_handler_3;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_21;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_20 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_20 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        Py_DECREF( locals_PIL$ImageShow_58 );
        locals_PIL$ImageShow_58 = NULL;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_PIL$ImageShow_58 );
        locals_PIL$ImageShow_58 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 58;
        goto try_except_handler_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Viewer, tmp_assign_source_20 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_4;
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_platform );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_str_plain_win32;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        // Tried code:
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Viewer );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Viewer );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Viewer" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 109;

                goto try_except_handler_4;
            }

            tmp_tuple_element_4 = tmp_mvar_value_5;
            tmp_assign_source_22 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_assign_source_22, 0, tmp_tuple_element_4 );
            assert( tmp_class_creation_2__bases_orig == NULL );
            tmp_class_creation_2__bases_orig = tmp_assign_source_22;
        }
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_dircall_arg1_2;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
            Py_INCREF( tmp_dircall_arg1_2 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
                tmp_assign_source_23 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_2__bases == NULL );
            tmp_class_creation_2__bases = tmp_assign_source_23;
        }
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = PyDict_New();
            assert( tmp_class_creation_2__class_decl_dict == NULL );
            tmp_class_creation_2__class_decl_dict = tmp_assign_source_24;
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_metaclass_name_2;
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_key_name_4;
            PyObject *tmp_dict_name_4;
            PyObject *tmp_dict_name_5;
            PyObject *tmp_key_name_5;
            nuitka_bool tmp_condition_result_10;
            int tmp_truth_name_2;
            PyObject *tmp_type_arg_3;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_bases_name_2;
            tmp_key_name_4 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
            tmp_key_name_5 = const_str_plain_metaclass;
            tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
            if ( tmp_metaclass_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            tmp_condition_result_10 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_subscribed_name_2 = tmp_class_creation_2__bases;
            tmp_subscript_name_2 = const_int_0;
            tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
            if ( tmp_type_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
            Py_DECREF( tmp_type_arg_3 );
            if ( tmp_metaclass_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_2 );
            condexpr_end_4:;
            condexpr_end_3:;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_bases_name_2 = tmp_class_creation_2__bases;
            tmp_assign_source_25 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
            Py_DECREF( tmp_metaclass_name_2 );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_2__metaclass == NULL );
            tmp_class_creation_2__metaclass = tmp_assign_source_25;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_key_name_6;
            PyObject *tmp_dict_name_6;
            tmp_key_name_6 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_4;
            }
            branch_no_7:;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_8 = tmp_class_creation_2__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_8, const_str_plain___prepare__ );
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_26;
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_9;
                PyObject *tmp_args_name_3;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_kw_name_3;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_source_name_9 = tmp_class_creation_2__metaclass;
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___prepare__ );
                if ( tmp_called_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_4;
                }
                tmp_tuple_element_5 = const_str_plain_WindowsViewer;
                tmp_args_name_3 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_tuple_element_5 = tmp_class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
                CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
                tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
                frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 109;
                tmp_assign_source_26 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_name_3 );
                if ( tmp_assign_source_26 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_4;
                }
                assert( tmp_class_creation_2__prepared == NULL );
                tmp_class_creation_2__prepared = tmp_assign_source_26;
            }
            {
                nuitka_bool tmp_condition_result_13;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_source_name_10;
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_source_name_10 = tmp_class_creation_2__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_10, const_str_plain___getitem__ );
                tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_4;
                }
                tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                {
                    PyObject *tmp_raise_type_2;
                    PyObject *tmp_raise_value_2;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_right_name_2;
                    PyObject *tmp_tuple_element_6;
                    PyObject *tmp_getattr_target_2;
                    PyObject *tmp_getattr_attr_2;
                    PyObject *tmp_getattr_default_2;
                    PyObject *tmp_source_name_11;
                    PyObject *tmp_type_arg_4;
                    tmp_raise_type_2 = PyExc_TypeError;
                    tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_class_creation_2__metaclass );
                    tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                    tmp_getattr_attr_2 = const_str_plain___name__;
                    tmp_getattr_default_2 = const_str_angle_metaclass;
                    tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                    if ( tmp_tuple_element_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;

                        goto try_except_handler_4;
                    }
                    tmp_right_name_2 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                    CHECK_OBJECT( tmp_class_creation_2__prepared );
                    tmp_type_arg_4 = tmp_class_creation_2__prepared;
                    tmp_source_name_11 = BUILTIN_TYPE1( tmp_type_arg_4 );
                    assert( !(tmp_source_name_11 == NULL) );
                    tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_11 );
                    if ( tmp_tuple_element_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_2 );

                        exception_lineno = 109;

                        goto try_except_handler_4;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                    tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                    Py_DECREF( tmp_right_name_2 );
                    if ( tmp_raise_value_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;

                        goto try_except_handler_4;
                    }
                    exception_type = tmp_raise_type_2;
                    Py_INCREF( tmp_raise_type_2 );
                    exception_value = tmp_raise_value_2;
                    exception_lineno = 109;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                    goto try_except_handler_4;
                }
                branch_no_9:;
            }
            goto branch_end_8;
            branch_no_8:;
            {
                PyObject *tmp_assign_source_27;
                tmp_assign_source_27 = PyDict_New();
                assert( tmp_class_creation_2__prepared == NULL );
                tmp_class_creation_2__prepared = tmp_assign_source_27;
            }
            branch_end_8:;
        }
        {
            PyObject *tmp_assign_source_28;
            {
                PyObject *tmp_set_locals_2;
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_set_locals_2 = tmp_class_creation_2__prepared;
                locals_PIL$ImageShow_109 = tmp_set_locals_2;
                Py_INCREF( tmp_set_locals_2 );
            }
            // Tried code:
            // Tried code:
            tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
            tmp_res = PyObject_SetItem( locals_PIL$ImageShow_109, const_str_plain___module__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_6;
            }
            tmp_dictset_value = const_str_plain_WindowsViewer;
            tmp_res = PyObject_SetItem( locals_PIL$ImageShow_109, const_str_plain___qualname__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;

                goto try_except_handler_6;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_3bcdf4be5ba30a7e1c42daff022047c0_3, codeobj_3bcdf4be5ba30a7e1c42daff022047c0, module_PIL$ImageShow, sizeof(void *) );
            frame_3bcdf4be5ba30a7e1c42daff022047c0_3 = cache_frame_3bcdf4be5ba30a7e1c42daff022047c0_3;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 ) == 2 ); // Frame stack

            // Framed code:
            tmp_dictset_value = const_str_plain_BMP;
            tmp_res = PyObject_SetItem( locals_PIL$ImageShow_109, const_str_plain_format, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 110;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_9_get_command(  );



            tmp_res = PyObject_SetItem( locals_PIL$ImageShow_109, const_str_plain_get_command, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 112;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

#if 0
            RESTORE_FRAME_EXCEPTION( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_2;

            frame_exception_exit_3:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_3bcdf4be5ba30a7e1c42daff022047c0_3, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_3bcdf4be5ba30a7e1c42daff022047c0_3->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_3bcdf4be5ba30a7e1c42daff022047c0_3, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_3bcdf4be5ba30a7e1c42daff022047c0_3,
                type_description_2,
                outline_1_var___class__
            );


            // Release cached frame.
            if ( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 == cache_frame_3bcdf4be5ba30a7e1c42daff022047c0_3 )
            {
                Py_DECREF( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 );
            }
            cache_frame_3bcdf4be5ba30a7e1c42daff022047c0_3 = NULL;

            assertFrameObject( frame_3bcdf4be5ba30a7e1c42daff022047c0_3 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_2;

            frame_no_exception_2:;
            goto skip_nested_handling_2;
            nested_frame_exit_2:;

            goto try_except_handler_6;
            skip_nested_handling_2:;
            {
                nuitka_bool tmp_condition_result_14;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_compexpr_left_4 = tmp_class_creation_2__bases;
                CHECK_OBJECT( tmp_class_creation_2__bases_orig );
                tmp_compexpr_right_4 = tmp_class_creation_2__bases_orig;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_6;
                }
                tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                CHECK_OBJECT( tmp_class_creation_2__bases_orig );
                tmp_dictset_value = tmp_class_creation_2__bases_orig;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_109, const_str_plain___orig_bases__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_6;
                }
                branch_no_10:;
            }
            {
                PyObject *tmp_assign_source_29;
                PyObject *tmp_called_name_4;
                PyObject *tmp_args_name_4;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_kw_name_4;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_called_name_4 = tmp_class_creation_2__metaclass;
                tmp_tuple_element_7 = const_str_plain_WindowsViewer;
                tmp_args_name_4 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_7 );
                PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__bases );
                tmp_tuple_element_7 = tmp_class_creation_2__bases;
                Py_INCREF( tmp_tuple_element_7 );
                PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
                tmp_tuple_element_7 = locals_PIL$ImageShow_109;
                Py_INCREF( tmp_tuple_element_7 );
                PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
                tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
                frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 109;
                tmp_assign_source_29 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
                Py_DECREF( tmp_args_name_4 );
                if ( tmp_assign_source_29 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;

                    goto try_except_handler_6;
                }
                assert( outline_1_var___class__ == NULL );
                outline_1_var___class__ = tmp_assign_source_29;
            }
            CHECK_OBJECT( outline_1_var___class__ );
            tmp_assign_source_28 = outline_1_var___class__;
            Py_INCREF( tmp_assign_source_28 );
            goto try_return_handler_6;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_6:;
            Py_DECREF( locals_PIL$ImageShow_109 );
            locals_PIL$ImageShow_109 = NULL;
            goto try_return_handler_5;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_DECREF( locals_PIL$ImageShow_109 );
            locals_PIL$ImageShow_109 = NULL;
            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto try_except_handler_5;
            // End of try:
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_5:;
            CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
            Py_DECREF( outline_1_var___class__ );
            outline_1_var___class__ = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto outline_exception_2;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
            return MOD_RETURN_VALUE( NULL );
            outline_exception_2:;
            exception_lineno = 109;
            goto try_except_handler_4;
            outline_result_2:;
            UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_WindowsViewer, tmp_assign_source_28 );
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_class_creation_2__bases_orig );
        tmp_class_creation_2__bases_orig = NULL;

        Py_XDECREF( tmp_class_creation_2__bases );
        tmp_class_creation_2__bases = NULL;

        Py_XDECREF( tmp_class_creation_2__class_decl_dict );
        tmp_class_creation_2__class_decl_dict = NULL;

        Py_XDECREF( tmp_class_creation_2__metaclass );
        tmp_class_creation_2__metaclass = NULL;

        Py_XDECREF( tmp_class_creation_2__prepared );
        tmp_class_creation_2__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
        Py_DECREF( tmp_class_creation_2__bases_orig );
        tmp_class_creation_2__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
        Py_DECREF( tmp_class_creation_2__bases );
        tmp_class_creation_2__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
        Py_DECREF( tmp_class_creation_2__class_decl_dict );
        tmp_class_creation_2__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
        Py_DECREF( tmp_class_creation_2__metaclass );
        tmp_class_creation_2__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
        Py_DECREF( tmp_class_creation_2__prepared );
        tmp_class_creation_2__prepared = NULL;

        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_mvar_value_7;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_register );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 117;

                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_6;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_WindowsViewer );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_WindowsViewer );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "WindowsViewer" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 117;

                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = tmp_mvar_value_7;
            frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 117;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 117;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_15;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_8;
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_8 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 119;

                goto frame_exception_exit_1;
            }

            tmp_source_name_12 = tmp_mvar_value_8;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_platform );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 119;

                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_5 = const_str_plain_darwin;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 119;

                goto frame_exception_exit_1;
            }
            tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            // Tried code:
            {
                PyObject *tmp_assign_source_30;
                PyObject *tmp_tuple_element_8;
                PyObject *tmp_mvar_value_9;
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Viewer );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Viewer );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Viewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 121;

                    goto try_except_handler_7;
                }

                tmp_tuple_element_8 = tmp_mvar_value_9;
                tmp_assign_source_30 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_assign_source_30, 0, tmp_tuple_element_8 );
                assert( tmp_class_creation_3__bases_orig == NULL );
                tmp_class_creation_3__bases_orig = tmp_assign_source_30;
            }
            {
                PyObject *tmp_assign_source_31;
                PyObject *tmp_dircall_arg1_3;
                CHECK_OBJECT( tmp_class_creation_3__bases_orig );
                tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
                Py_INCREF( tmp_dircall_arg1_3 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
                    tmp_assign_source_31 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                assert( tmp_class_creation_3__bases == NULL );
                tmp_class_creation_3__bases = tmp_assign_source_31;
            }
            {
                PyObject *tmp_assign_source_32;
                tmp_assign_source_32 = PyDict_New();
                assert( tmp_class_creation_3__class_decl_dict == NULL );
                tmp_class_creation_3__class_decl_dict = tmp_assign_source_32;
            }
            {
                PyObject *tmp_assign_source_33;
                PyObject *tmp_metaclass_name_3;
                nuitka_bool tmp_condition_result_16;
                PyObject *tmp_key_name_7;
                PyObject *tmp_dict_name_7;
                PyObject *tmp_dict_name_8;
                PyObject *tmp_key_name_8;
                nuitka_bool tmp_condition_result_17;
                int tmp_truth_name_3;
                PyObject *tmp_type_arg_5;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_bases_name_3;
                tmp_key_name_7 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
                tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_5;
                }
                else
                {
                    goto condexpr_false_5;
                }
                condexpr_true_5:;
                CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
                tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
                tmp_key_name_8 = const_str_plain_metaclass;
                tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
                if ( tmp_metaclass_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                goto condexpr_end_5;
                condexpr_false_5:;
                CHECK_OBJECT( tmp_class_creation_3__bases );
                tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                tmp_condition_result_17 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_6;
                }
                else
                {
                    goto condexpr_false_6;
                }
                condexpr_true_6:;
                CHECK_OBJECT( tmp_class_creation_3__bases );
                tmp_subscribed_name_3 = tmp_class_creation_3__bases;
                tmp_subscript_name_3 = const_int_0;
                tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
                if ( tmp_type_arg_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
                Py_DECREF( tmp_type_arg_5 );
                if ( tmp_metaclass_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                goto condexpr_end_6;
                condexpr_false_6:;
                tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_3 );
                condexpr_end_6:;
                condexpr_end_5:;
                CHECK_OBJECT( tmp_class_creation_3__bases );
                tmp_bases_name_3 = tmp_class_creation_3__bases;
                tmp_assign_source_33 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
                Py_DECREF( tmp_metaclass_name_3 );
                if ( tmp_assign_source_33 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                assert( tmp_class_creation_3__metaclass == NULL );
                tmp_class_creation_3__metaclass = tmp_assign_source_33;
            }
            {
                nuitka_bool tmp_condition_result_18;
                PyObject *tmp_key_name_9;
                PyObject *tmp_dict_name_9;
                tmp_key_name_9 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
                tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_7;
                }
                branch_no_12:;
            }
            {
                nuitka_bool tmp_condition_result_19;
                PyObject *tmp_source_name_13;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_source_name_13 = tmp_class_creation_3__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___prepare__ );
                tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_13;
                }
                else
                {
                    goto branch_no_13;
                }
                branch_yes_13:;
                {
                    PyObject *tmp_assign_source_34;
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_source_name_14;
                    PyObject *tmp_args_name_5;
                    PyObject *tmp_tuple_element_9;
                    PyObject *tmp_kw_name_5;
                    CHECK_OBJECT( tmp_class_creation_3__metaclass );
                    tmp_source_name_14 = tmp_class_creation_3__metaclass;
                    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___prepare__ );
                    if ( tmp_called_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 121;

                        goto try_except_handler_7;
                    }
                    tmp_tuple_element_9 = const_str_plain_MacViewer;
                    tmp_args_name_5 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_9 );
                    PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_9 );
                    CHECK_OBJECT( tmp_class_creation_3__bases );
                    tmp_tuple_element_9 = tmp_class_creation_3__bases;
                    Py_INCREF( tmp_tuple_element_9 );
                    PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_9 );
                    CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
                    tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 121;
                    tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_5, tmp_kw_name_5 );
                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_args_name_5 );
                    if ( tmp_assign_source_34 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 121;

                        goto try_except_handler_7;
                    }
                    assert( tmp_class_creation_3__prepared == NULL );
                    tmp_class_creation_3__prepared = tmp_assign_source_34;
                }
                {
                    nuitka_bool tmp_condition_result_20;
                    PyObject *tmp_operand_name_3;
                    PyObject *tmp_source_name_15;
                    CHECK_OBJECT( tmp_class_creation_3__prepared );
                    tmp_source_name_15 = tmp_class_creation_3__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___getitem__ );
                    tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 121;

                        goto try_except_handler_7;
                    }
                    tmp_condition_result_20 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_14;
                    }
                    else
                    {
                        goto branch_no_14;
                    }
                    branch_yes_14:;
                    {
                        PyObject *tmp_raise_type_3;
                        PyObject *tmp_raise_value_3;
                        PyObject *tmp_left_name_3;
                        PyObject *tmp_right_name_3;
                        PyObject *tmp_tuple_element_10;
                        PyObject *tmp_getattr_target_3;
                        PyObject *tmp_getattr_attr_3;
                        PyObject *tmp_getattr_default_3;
                        PyObject *tmp_source_name_16;
                        PyObject *tmp_type_arg_6;
                        tmp_raise_type_3 = PyExc_TypeError;
                        tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_3__metaclass );
                        tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                        tmp_getattr_attr_3 = const_str_plain___name__;
                        tmp_getattr_default_3 = const_str_angle_metaclass;
                        tmp_tuple_element_10 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                        if ( tmp_tuple_element_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 121;

                            goto try_except_handler_7;
                        }
                        tmp_right_name_3 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_10 );
                        CHECK_OBJECT( tmp_class_creation_3__prepared );
                        tmp_type_arg_6 = tmp_class_creation_3__prepared;
                        tmp_source_name_16 = BUILTIN_TYPE1( tmp_type_arg_6 );
                        assert( !(tmp_source_name_16 == NULL) );
                        tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_16 );
                        if ( tmp_tuple_element_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_3 );

                            exception_lineno = 121;

                            goto try_except_handler_7;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_10 );
                        tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                        Py_DECREF( tmp_right_name_3 );
                        if ( tmp_raise_value_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 121;

                            goto try_except_handler_7;
                        }
                        exception_type = tmp_raise_type_3;
                        Py_INCREF( tmp_raise_type_3 );
                        exception_value = tmp_raise_value_3;
                        exception_lineno = 121;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_7;
                    }
                    branch_no_14:;
                }
                goto branch_end_13;
                branch_no_13:;
                {
                    PyObject *tmp_assign_source_35;
                    tmp_assign_source_35 = PyDict_New();
                    assert( tmp_class_creation_3__prepared == NULL );
                    tmp_class_creation_3__prepared = tmp_assign_source_35;
                }
                branch_end_13:;
            }
            {
                PyObject *tmp_assign_source_36;
                {
                    PyObject *tmp_set_locals_3;
                    CHECK_OBJECT( tmp_class_creation_3__prepared );
                    tmp_set_locals_3 = tmp_class_creation_3__prepared;
                    locals_PIL$ImageShow_121 = tmp_set_locals_3;
                    Py_INCREF( tmp_set_locals_3 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_9;
                }
                tmp_dictset_value = const_str_plain_MacViewer;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 121;

                    goto try_except_handler_9;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_dc73a6dbd4552d1ae48c358e48b98604_4, codeobj_dc73a6dbd4552d1ae48c358e48b98604, module_PIL$ImageShow, sizeof(void *) );
                frame_dc73a6dbd4552d1ae48c358e48b98604_4 = cache_frame_dc73a6dbd4552d1ae48c358e48b98604_4;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_dc73a6dbd4552d1ae48c358e48b98604_4 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_dc73a6dbd4552d1ae48c358e48b98604_4 ) == 2 ); // Frame stack

                // Framed code:
                tmp_dictset_value = const_str_plain_PNG;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain_format, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 122;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }
                tmp_dictset_value = PyDict_Copy( const_dict_f340ab83a67ad58f315753e268cd4d44 );
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain_options, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 123;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }
                tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_10_get_command(  );



                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain_get_command, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 125;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }
                tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_11_show_file(  );



                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain_show_file, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 133;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_dc73a6dbd4552d1ae48c358e48b98604_4 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_3;

                frame_exception_exit_4:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_dc73a6dbd4552d1ae48c358e48b98604_4 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_dc73a6dbd4552d1ae48c358e48b98604_4, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_dc73a6dbd4552d1ae48c358e48b98604_4->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_dc73a6dbd4552d1ae48c358e48b98604_4, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_dc73a6dbd4552d1ae48c358e48b98604_4,
                    type_description_2,
                    outline_2_var___class__
                );


                // Release cached frame.
                if ( frame_dc73a6dbd4552d1ae48c358e48b98604_4 == cache_frame_dc73a6dbd4552d1ae48c358e48b98604_4 )
                {
                    Py_DECREF( frame_dc73a6dbd4552d1ae48c358e48b98604_4 );
                }
                cache_frame_dc73a6dbd4552d1ae48c358e48b98604_4 = NULL;

                assertFrameObject( frame_dc73a6dbd4552d1ae48c358e48b98604_4 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_3;

                frame_no_exception_3:;
                goto skip_nested_handling_3;
                nested_frame_exit_3:;

                goto try_except_handler_9;
                skip_nested_handling_3:;
                {
                    nuitka_bool tmp_condition_result_21;
                    PyObject *tmp_compexpr_left_6;
                    PyObject *tmp_compexpr_right_6;
                    CHECK_OBJECT( tmp_class_creation_3__bases );
                    tmp_compexpr_left_6 = tmp_class_creation_3__bases;
                    CHECK_OBJECT( tmp_class_creation_3__bases_orig );
                    tmp_compexpr_right_6 = tmp_class_creation_3__bases_orig;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 121;

                        goto try_except_handler_9;
                    }
                    tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_15;
                    }
                    else
                    {
                        goto branch_no_15;
                    }
                    branch_yes_15:;
                    CHECK_OBJECT( tmp_class_creation_3__bases_orig );
                    tmp_dictset_value = tmp_class_creation_3__bases_orig;
                    tmp_res = PyObject_SetItem( locals_PIL$ImageShow_121, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 121;

                        goto try_except_handler_9;
                    }
                    branch_no_15:;
                }
                {
                    PyObject *tmp_assign_source_37;
                    PyObject *tmp_called_name_7;
                    PyObject *tmp_args_name_6;
                    PyObject *tmp_tuple_element_11;
                    PyObject *tmp_kw_name_6;
                    CHECK_OBJECT( tmp_class_creation_3__metaclass );
                    tmp_called_name_7 = tmp_class_creation_3__metaclass;
                    tmp_tuple_element_11 = const_str_plain_MacViewer;
                    tmp_args_name_6 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_11 );
                    PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_11 );
                    CHECK_OBJECT( tmp_class_creation_3__bases );
                    tmp_tuple_element_11 = tmp_class_creation_3__bases;
                    Py_INCREF( tmp_tuple_element_11 );
                    PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_11 );
                    tmp_tuple_element_11 = locals_PIL$ImageShow_121;
                    Py_INCREF( tmp_tuple_element_11 );
                    PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_11 );
                    CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
                    tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 121;
                    tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_6, tmp_kw_name_6 );
                    Py_DECREF( tmp_args_name_6 );
                    if ( tmp_assign_source_37 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 121;

                        goto try_except_handler_9;
                    }
                    assert( outline_2_var___class__ == NULL );
                    outline_2_var___class__ = tmp_assign_source_37;
                }
                CHECK_OBJECT( outline_2_var___class__ );
                tmp_assign_source_36 = outline_2_var___class__;
                Py_INCREF( tmp_assign_source_36 );
                goto try_return_handler_9;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_9:;
                Py_DECREF( locals_PIL$ImageShow_121 );
                locals_PIL$ImageShow_121 = NULL;
                goto try_return_handler_8;
                // Exception handler code:
                try_except_handler_9:;
                exception_keeper_type_7 = exception_type;
                exception_keeper_value_7 = exception_value;
                exception_keeper_tb_7 = exception_tb;
                exception_keeper_lineno_7 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_PIL$ImageShow_121 );
                locals_PIL$ImageShow_121 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_7;
                exception_value = exception_keeper_value_7;
                exception_tb = exception_keeper_tb_7;
                exception_lineno = exception_keeper_lineno_7;

                goto try_except_handler_8;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_8:;
                CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
                Py_DECREF( outline_2_var___class__ );
                outline_2_var___class__ = NULL;

                goto outline_result_3;
                // Exception handler code:
                try_except_handler_8:;
                exception_keeper_type_8 = exception_type;
                exception_keeper_value_8 = exception_value;
                exception_keeper_tb_8 = exception_tb;
                exception_keeper_lineno_8 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_8;
                exception_value = exception_keeper_value_8;
                exception_tb = exception_keeper_tb_8;
                exception_lineno = exception_keeper_lineno_8;

                goto outline_exception_3;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_3:;
                exception_lineno = 121;
                goto try_except_handler_7;
                outline_result_3:;
                UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_MacViewer, tmp_assign_source_36 );
            }
            goto try_end_3;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_9 = exception_type;
            exception_keeper_value_9 = exception_value;
            exception_keeper_tb_9 = exception_tb;
            exception_keeper_lineno_9 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_3__bases_orig );
            tmp_class_creation_3__bases_orig = NULL;

            Py_XDECREF( tmp_class_creation_3__bases );
            tmp_class_creation_3__bases = NULL;

            Py_XDECREF( tmp_class_creation_3__class_decl_dict );
            tmp_class_creation_3__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_3__metaclass );
            tmp_class_creation_3__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_3__prepared );
            tmp_class_creation_3__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_9;
            exception_value = exception_keeper_value_9;
            exception_tb = exception_keeper_tb_9;
            exception_lineno = exception_keeper_lineno_9;

            goto frame_exception_exit_1;
            // End of try:
            try_end_3:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
            Py_DECREF( tmp_class_creation_3__bases_orig );
            tmp_class_creation_3__bases_orig = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
            Py_DECREF( tmp_class_creation_3__bases );
            tmp_class_creation_3__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
            Py_DECREF( tmp_class_creation_3__class_decl_dict );
            tmp_class_creation_3__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
            Py_DECREF( tmp_class_creation_3__metaclass );
            tmp_class_creation_3__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
            Py_DECREF( tmp_class_creation_3__prepared );
            tmp_class_creation_3__prepared = NULL;

            {
                PyObject *tmp_called_name_8;
                PyObject *tmp_mvar_value_10;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_mvar_value_11;
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_register );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 148;

                    goto frame_exception_exit_1;
                }

                tmp_called_name_8 = tmp_mvar_value_10;
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_MacViewer );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MacViewer );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MacViewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 148;

                    goto frame_exception_exit_1;
                }

                tmp_args_element_name_2 = tmp_mvar_value_11;
                frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 148;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
                }

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 148;

                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto branch_end_11;
            branch_no_11:;
            {
                PyObject *tmp_assign_source_38;
                tmp_assign_source_38 = MAKE_FUNCTION_PIL$ImageShow$$$function_12_which(  );



                UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_which, tmp_assign_source_38 );
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_39;
                PyObject *tmp_tuple_element_12;
                PyObject *tmp_mvar_value_12;
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_Viewer );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Viewer );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Viewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 164;

                    goto try_except_handler_10;
                }

                tmp_tuple_element_12 = tmp_mvar_value_12;
                tmp_assign_source_39 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_12 );
                PyTuple_SET_ITEM( tmp_assign_source_39, 0, tmp_tuple_element_12 );
                assert( tmp_class_creation_4__bases_orig == NULL );
                tmp_class_creation_4__bases_orig = tmp_assign_source_39;
            }
            {
                PyObject *tmp_assign_source_40;
                PyObject *tmp_dircall_arg1_4;
                CHECK_OBJECT( tmp_class_creation_4__bases_orig );
                tmp_dircall_arg1_4 = tmp_class_creation_4__bases_orig;
                Py_INCREF( tmp_dircall_arg1_4 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
                    tmp_assign_source_40 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_40 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                assert( tmp_class_creation_4__bases == NULL );
                tmp_class_creation_4__bases = tmp_assign_source_40;
            }
            {
                PyObject *tmp_assign_source_41;
                tmp_assign_source_41 = PyDict_New();
                assert( tmp_class_creation_4__class_decl_dict == NULL );
                tmp_class_creation_4__class_decl_dict = tmp_assign_source_41;
            }
            {
                PyObject *tmp_assign_source_42;
                PyObject *tmp_metaclass_name_4;
                nuitka_bool tmp_condition_result_22;
                PyObject *tmp_key_name_10;
                PyObject *tmp_dict_name_10;
                PyObject *tmp_dict_name_11;
                PyObject *tmp_key_name_11;
                nuitka_bool tmp_condition_result_23;
                int tmp_truth_name_4;
                PyObject *tmp_type_arg_7;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_subscript_name_4;
                PyObject *tmp_bases_name_4;
                tmp_key_name_10 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
                tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_7;
                }
                else
                {
                    goto condexpr_false_7;
                }
                condexpr_true_7:;
                CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
                tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
                tmp_key_name_11 = const_str_plain_metaclass;
                tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
                if ( tmp_metaclass_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                goto condexpr_end_7;
                condexpr_false_7:;
                CHECK_OBJECT( tmp_class_creation_4__bases );
                tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
                if ( tmp_truth_name_4 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                tmp_condition_result_23 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_8;
                }
                else
                {
                    goto condexpr_false_8;
                }
                condexpr_true_8:;
                CHECK_OBJECT( tmp_class_creation_4__bases );
                tmp_subscribed_name_4 = tmp_class_creation_4__bases;
                tmp_subscript_name_4 = const_int_0;
                tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
                if ( tmp_type_arg_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
                Py_DECREF( tmp_type_arg_7 );
                if ( tmp_metaclass_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                goto condexpr_end_8;
                condexpr_false_8:;
                tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_4 );
                condexpr_end_8:;
                condexpr_end_7:;
                CHECK_OBJECT( tmp_class_creation_4__bases );
                tmp_bases_name_4 = tmp_class_creation_4__bases;
                tmp_assign_source_42 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
                Py_DECREF( tmp_metaclass_name_4 );
                if ( tmp_assign_source_42 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                assert( tmp_class_creation_4__metaclass == NULL );
                tmp_class_creation_4__metaclass = tmp_assign_source_42;
            }
            {
                nuitka_bool tmp_condition_result_24;
                PyObject *tmp_key_name_12;
                PyObject *tmp_dict_name_12;
                tmp_key_name_12 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
                tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_16;
                }
                else
                {
                    goto branch_no_16;
                }
                branch_yes_16:;
                CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_10;
                }
                branch_no_16:;
            }
            {
                nuitka_bool tmp_condition_result_25;
                PyObject *tmp_source_name_17;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_source_name_17 = tmp_class_creation_4__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_17, const_str_plain___prepare__ );
                tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_17;
                }
                else
                {
                    goto branch_no_17;
                }
                branch_yes_17:;
                {
                    PyObject *tmp_assign_source_43;
                    PyObject *tmp_called_name_9;
                    PyObject *tmp_source_name_18;
                    PyObject *tmp_args_name_7;
                    PyObject *tmp_tuple_element_13;
                    PyObject *tmp_kw_name_7;
                    CHECK_OBJECT( tmp_class_creation_4__metaclass );
                    tmp_source_name_18 = tmp_class_creation_4__metaclass;
                    tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___prepare__ );
                    if ( tmp_called_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 164;

                        goto try_except_handler_10;
                    }
                    tmp_tuple_element_13 = const_str_plain_UnixViewer;
                    tmp_args_name_7 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_13 );
                    PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_13 );
                    CHECK_OBJECT( tmp_class_creation_4__bases );
                    tmp_tuple_element_13 = tmp_class_creation_4__bases;
                    Py_INCREF( tmp_tuple_element_13 );
                    PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_13 );
                    CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
                    tmp_kw_name_7 = tmp_class_creation_4__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 164;
                    tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_7, tmp_kw_name_7 );
                    Py_DECREF( tmp_called_name_9 );
                    Py_DECREF( tmp_args_name_7 );
                    if ( tmp_assign_source_43 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 164;

                        goto try_except_handler_10;
                    }
                    assert( tmp_class_creation_4__prepared == NULL );
                    tmp_class_creation_4__prepared = tmp_assign_source_43;
                }
                {
                    nuitka_bool tmp_condition_result_26;
                    PyObject *tmp_operand_name_4;
                    PyObject *tmp_source_name_19;
                    CHECK_OBJECT( tmp_class_creation_4__prepared );
                    tmp_source_name_19 = tmp_class_creation_4__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_19, const_str_plain___getitem__ );
                    tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 164;

                        goto try_except_handler_10;
                    }
                    tmp_condition_result_26 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_18;
                    }
                    else
                    {
                        goto branch_no_18;
                    }
                    branch_yes_18:;
                    {
                        PyObject *tmp_raise_type_4;
                        PyObject *tmp_raise_value_4;
                        PyObject *tmp_left_name_4;
                        PyObject *tmp_right_name_4;
                        PyObject *tmp_tuple_element_14;
                        PyObject *tmp_getattr_target_4;
                        PyObject *tmp_getattr_attr_4;
                        PyObject *tmp_getattr_default_4;
                        PyObject *tmp_source_name_20;
                        PyObject *tmp_type_arg_8;
                        tmp_raise_type_4 = PyExc_TypeError;
                        tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_4__metaclass );
                        tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                        tmp_getattr_attr_4 = const_str_plain___name__;
                        tmp_getattr_default_4 = const_str_angle_metaclass;
                        tmp_tuple_element_14 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                        if ( tmp_tuple_element_14 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 164;

                            goto try_except_handler_10;
                        }
                        tmp_right_name_4 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_14 );
                        CHECK_OBJECT( tmp_class_creation_4__prepared );
                        tmp_type_arg_8 = tmp_class_creation_4__prepared;
                        tmp_source_name_20 = BUILTIN_TYPE1( tmp_type_arg_8 );
                        assert( !(tmp_source_name_20 == NULL) );
                        tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_20 );
                        if ( tmp_tuple_element_14 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_4 );

                            exception_lineno = 164;

                            goto try_except_handler_10;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_14 );
                        tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                        Py_DECREF( tmp_right_name_4 );
                        if ( tmp_raise_value_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 164;

                            goto try_except_handler_10;
                        }
                        exception_type = tmp_raise_type_4;
                        Py_INCREF( tmp_raise_type_4 );
                        exception_value = tmp_raise_value_4;
                        exception_lineno = 164;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_10;
                    }
                    branch_no_18:;
                }
                goto branch_end_17;
                branch_no_17:;
                {
                    PyObject *tmp_assign_source_44;
                    tmp_assign_source_44 = PyDict_New();
                    assert( tmp_class_creation_4__prepared == NULL );
                    tmp_class_creation_4__prepared = tmp_assign_source_44;
                }
                branch_end_17:;
            }
            {
                PyObject *tmp_assign_source_45;
                {
                    PyObject *tmp_set_locals_4;
                    CHECK_OBJECT( tmp_class_creation_4__prepared );
                    tmp_set_locals_4 = tmp_class_creation_4__prepared;
                    locals_PIL$ImageShow_164 = tmp_set_locals_4;
                    Py_INCREF( tmp_set_locals_4 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_12;
                }
                tmp_dictset_value = const_str_plain_UnixViewer;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;

                    goto try_except_handler_12;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_86e12a72b9b504adcb71f61e7d4a4a09_5, codeobj_86e12a72b9b504adcb71f61e7d4a4a09, module_PIL$ImageShow, sizeof(void *) );
                frame_86e12a72b9b504adcb71f61e7d4a4a09_5 = cache_frame_86e12a72b9b504adcb71f61e7d4a4a09_5;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 ) == 2 ); // Frame stack

                // Framed code:
                tmp_dictset_value = const_str_plain_PNG;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain_format, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 165;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }
                tmp_dictset_value = PyDict_Copy( const_dict_f340ab83a67ad58f315753e268cd4d44 );
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain_options, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 166;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }
                tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_13_get_command(  );



                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain_get_command, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }
                tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_14_show_file(  );



                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain_show_file, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 172;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_4;

                frame_exception_exit_5:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_86e12a72b9b504adcb71f61e7d4a4a09_5, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_86e12a72b9b504adcb71f61e7d4a4a09_5->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_86e12a72b9b504adcb71f61e7d4a4a09_5, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_86e12a72b9b504adcb71f61e7d4a4a09_5,
                    type_description_2,
                    outline_3_var___class__
                );


                // Release cached frame.
                if ( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 == cache_frame_86e12a72b9b504adcb71f61e7d4a4a09_5 )
                {
                    Py_DECREF( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 );
                }
                cache_frame_86e12a72b9b504adcb71f61e7d4a4a09_5 = NULL;

                assertFrameObject( frame_86e12a72b9b504adcb71f61e7d4a4a09_5 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_4;

                frame_no_exception_4:;
                goto skip_nested_handling_4;
                nested_frame_exit_4:;

                goto try_except_handler_12;
                skip_nested_handling_4:;
                {
                    nuitka_bool tmp_condition_result_27;
                    PyObject *tmp_compexpr_left_7;
                    PyObject *tmp_compexpr_right_7;
                    CHECK_OBJECT( tmp_class_creation_4__bases );
                    tmp_compexpr_left_7 = tmp_class_creation_4__bases;
                    CHECK_OBJECT( tmp_class_creation_4__bases_orig );
                    tmp_compexpr_right_7 = tmp_class_creation_4__bases_orig;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 164;

                        goto try_except_handler_12;
                    }
                    tmp_condition_result_27 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_19;
                    }
                    else
                    {
                        goto branch_no_19;
                    }
                    branch_yes_19:;
                    CHECK_OBJECT( tmp_class_creation_4__bases_orig );
                    tmp_dictset_value = tmp_class_creation_4__bases_orig;
                    tmp_res = PyObject_SetItem( locals_PIL$ImageShow_164, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 164;

                        goto try_except_handler_12;
                    }
                    branch_no_19:;
                }
                {
                    PyObject *tmp_assign_source_46;
                    PyObject *tmp_called_name_10;
                    PyObject *tmp_args_name_8;
                    PyObject *tmp_tuple_element_15;
                    PyObject *tmp_kw_name_8;
                    CHECK_OBJECT( tmp_class_creation_4__metaclass );
                    tmp_called_name_10 = tmp_class_creation_4__metaclass;
                    tmp_tuple_element_15 = const_str_plain_UnixViewer;
                    tmp_args_name_8 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_15 );
                    PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_15 );
                    CHECK_OBJECT( tmp_class_creation_4__bases );
                    tmp_tuple_element_15 = tmp_class_creation_4__bases;
                    Py_INCREF( tmp_tuple_element_15 );
                    PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_15 );
                    tmp_tuple_element_15 = locals_PIL$ImageShow_164;
                    Py_INCREF( tmp_tuple_element_15 );
                    PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_15 );
                    CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
                    tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 164;
                    tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_8, tmp_kw_name_8 );
                    Py_DECREF( tmp_args_name_8 );
                    if ( tmp_assign_source_46 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 164;

                        goto try_except_handler_12;
                    }
                    assert( outline_3_var___class__ == NULL );
                    outline_3_var___class__ = tmp_assign_source_46;
                }
                CHECK_OBJECT( outline_3_var___class__ );
                tmp_assign_source_45 = outline_3_var___class__;
                Py_INCREF( tmp_assign_source_45 );
                goto try_return_handler_12;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_12:;
                Py_DECREF( locals_PIL$ImageShow_164 );
                locals_PIL$ImageShow_164 = NULL;
                goto try_return_handler_11;
                // Exception handler code:
                try_except_handler_12:;
                exception_keeper_type_10 = exception_type;
                exception_keeper_value_10 = exception_value;
                exception_keeper_tb_10 = exception_tb;
                exception_keeper_lineno_10 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_PIL$ImageShow_164 );
                locals_PIL$ImageShow_164 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_10;
                exception_value = exception_keeper_value_10;
                exception_tb = exception_keeper_tb_10;
                exception_lineno = exception_keeper_lineno_10;

                goto try_except_handler_11;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_11:;
                CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
                Py_DECREF( outline_3_var___class__ );
                outline_3_var___class__ = NULL;

                goto outline_result_4;
                // Exception handler code:
                try_except_handler_11:;
                exception_keeper_type_11 = exception_type;
                exception_keeper_value_11 = exception_value;
                exception_keeper_tb_11 = exception_tb;
                exception_keeper_lineno_11 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_11;
                exception_value = exception_keeper_value_11;
                exception_tb = exception_keeper_tb_11;
                exception_lineno = exception_keeper_lineno_11;

                goto outline_exception_4;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_4:;
                exception_lineno = 164;
                goto try_except_handler_10;
                outline_result_4:;
                UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_UnixViewer, tmp_assign_source_45 );
            }
            goto try_end_4;
            // Exception handler code:
            try_except_handler_10:;
            exception_keeper_type_12 = exception_type;
            exception_keeper_value_12 = exception_value;
            exception_keeper_tb_12 = exception_tb;
            exception_keeper_lineno_12 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_4__bases_orig );
            tmp_class_creation_4__bases_orig = NULL;

            Py_XDECREF( tmp_class_creation_4__bases );
            tmp_class_creation_4__bases = NULL;

            Py_XDECREF( tmp_class_creation_4__class_decl_dict );
            tmp_class_creation_4__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_4__metaclass );
            tmp_class_creation_4__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_4__prepared );
            tmp_class_creation_4__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_12;
            exception_value = exception_keeper_value_12;
            exception_tb = exception_keeper_tb_12;
            exception_lineno = exception_keeper_lineno_12;

            goto frame_exception_exit_1;
            // End of try:
            try_end_4:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases_orig );
            Py_DECREF( tmp_class_creation_4__bases_orig );
            tmp_class_creation_4__bases_orig = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
            Py_DECREF( tmp_class_creation_4__bases );
            tmp_class_creation_4__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
            Py_DECREF( tmp_class_creation_4__class_decl_dict );
            tmp_class_creation_4__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
            Py_DECREF( tmp_class_creation_4__metaclass );
            tmp_class_creation_4__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
            Py_DECREF( tmp_class_creation_4__prepared );
            tmp_class_creation_4__prepared = NULL;

            // Tried code:
            {
                PyObject *tmp_assign_source_47;
                PyObject *tmp_tuple_element_16;
                PyObject *tmp_mvar_value_13;
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_UnixViewer );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnixViewer );
                }

                if ( tmp_mvar_value_13 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnixViewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 189;

                    goto try_except_handler_13;
                }

                tmp_tuple_element_16 = tmp_mvar_value_13;
                tmp_assign_source_47 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_16 );
                PyTuple_SET_ITEM( tmp_assign_source_47, 0, tmp_tuple_element_16 );
                assert( tmp_class_creation_5__bases_orig == NULL );
                tmp_class_creation_5__bases_orig = tmp_assign_source_47;
            }
            {
                PyObject *tmp_assign_source_48;
                PyObject *tmp_dircall_arg1_5;
                CHECK_OBJECT( tmp_class_creation_5__bases_orig );
                tmp_dircall_arg1_5 = tmp_class_creation_5__bases_orig;
                Py_INCREF( tmp_dircall_arg1_5 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
                    tmp_assign_source_48 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_48 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                assert( tmp_class_creation_5__bases == NULL );
                tmp_class_creation_5__bases = tmp_assign_source_48;
            }
            {
                PyObject *tmp_assign_source_49;
                tmp_assign_source_49 = PyDict_New();
                assert( tmp_class_creation_5__class_decl_dict == NULL );
                tmp_class_creation_5__class_decl_dict = tmp_assign_source_49;
            }
            {
                PyObject *tmp_assign_source_50;
                PyObject *tmp_metaclass_name_5;
                nuitka_bool tmp_condition_result_28;
                PyObject *tmp_key_name_13;
                PyObject *tmp_dict_name_13;
                PyObject *tmp_dict_name_14;
                PyObject *tmp_key_name_14;
                nuitka_bool tmp_condition_result_29;
                int tmp_truth_name_5;
                PyObject *tmp_type_arg_9;
                PyObject *tmp_subscribed_name_5;
                PyObject *tmp_subscript_name_5;
                PyObject *tmp_bases_name_5;
                tmp_key_name_13 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
                tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_9;
                }
                else
                {
                    goto condexpr_false_9;
                }
                condexpr_true_9:;
                CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
                tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
                tmp_key_name_14 = const_str_plain_metaclass;
                tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
                if ( tmp_metaclass_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                goto condexpr_end_9;
                condexpr_false_9:;
                CHECK_OBJECT( tmp_class_creation_5__bases );
                tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
                if ( tmp_truth_name_5 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                tmp_condition_result_29 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_10;
                }
                else
                {
                    goto condexpr_false_10;
                }
                condexpr_true_10:;
                CHECK_OBJECT( tmp_class_creation_5__bases );
                tmp_subscribed_name_5 = tmp_class_creation_5__bases;
                tmp_subscript_name_5 = const_int_0;
                tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
                if ( tmp_type_arg_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
                Py_DECREF( tmp_type_arg_9 );
                if ( tmp_metaclass_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                goto condexpr_end_10;
                condexpr_false_10:;
                tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_5 );
                condexpr_end_10:;
                condexpr_end_9:;
                CHECK_OBJECT( tmp_class_creation_5__bases );
                tmp_bases_name_5 = tmp_class_creation_5__bases;
                tmp_assign_source_50 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
                Py_DECREF( tmp_metaclass_name_5 );
                if ( tmp_assign_source_50 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                assert( tmp_class_creation_5__metaclass == NULL );
                tmp_class_creation_5__metaclass = tmp_assign_source_50;
            }
            {
                nuitka_bool tmp_condition_result_30;
                PyObject *tmp_key_name_15;
                PyObject *tmp_dict_name_15;
                tmp_key_name_15 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
                tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                tmp_condition_result_30 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_20;
                }
                else
                {
                    goto branch_no_20;
                }
                branch_yes_20:;
                CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_13;
                }
                branch_no_20:;
            }
            {
                nuitka_bool tmp_condition_result_31;
                PyObject *tmp_source_name_21;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_source_name_21 = tmp_class_creation_5__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_21, const_str_plain___prepare__ );
                tmp_condition_result_31 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_21;
                }
                else
                {
                    goto branch_no_21;
                }
                branch_yes_21:;
                {
                    PyObject *tmp_assign_source_51;
                    PyObject *tmp_called_name_11;
                    PyObject *tmp_source_name_22;
                    PyObject *tmp_args_name_9;
                    PyObject *tmp_tuple_element_17;
                    PyObject *tmp_kw_name_9;
                    CHECK_OBJECT( tmp_class_creation_5__metaclass );
                    tmp_source_name_22 = tmp_class_creation_5__metaclass;
                    tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain___prepare__ );
                    if ( tmp_called_name_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 189;

                        goto try_except_handler_13;
                    }
                    tmp_tuple_element_17 = const_str_plain_DisplayViewer;
                    tmp_args_name_9 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_17 );
                    PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_17 );
                    CHECK_OBJECT( tmp_class_creation_5__bases );
                    tmp_tuple_element_17 = tmp_class_creation_5__bases;
                    Py_INCREF( tmp_tuple_element_17 );
                    PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_17 );
                    CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
                    tmp_kw_name_9 = tmp_class_creation_5__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 189;
                    tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_9, tmp_kw_name_9 );
                    Py_DECREF( tmp_called_name_11 );
                    Py_DECREF( tmp_args_name_9 );
                    if ( tmp_assign_source_51 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 189;

                        goto try_except_handler_13;
                    }
                    assert( tmp_class_creation_5__prepared == NULL );
                    tmp_class_creation_5__prepared = tmp_assign_source_51;
                }
                {
                    nuitka_bool tmp_condition_result_32;
                    PyObject *tmp_operand_name_5;
                    PyObject *tmp_source_name_23;
                    CHECK_OBJECT( tmp_class_creation_5__prepared );
                    tmp_source_name_23 = tmp_class_creation_5__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_23, const_str_plain___getitem__ );
                    tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 189;

                        goto try_except_handler_13;
                    }
                    tmp_condition_result_32 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_22;
                    }
                    else
                    {
                        goto branch_no_22;
                    }
                    branch_yes_22:;
                    {
                        PyObject *tmp_raise_type_5;
                        PyObject *tmp_raise_value_5;
                        PyObject *tmp_left_name_5;
                        PyObject *tmp_right_name_5;
                        PyObject *tmp_tuple_element_18;
                        PyObject *tmp_getattr_target_5;
                        PyObject *tmp_getattr_attr_5;
                        PyObject *tmp_getattr_default_5;
                        PyObject *tmp_source_name_24;
                        PyObject *tmp_type_arg_10;
                        tmp_raise_type_5 = PyExc_TypeError;
                        tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_5__metaclass );
                        tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                        tmp_getattr_attr_5 = const_str_plain___name__;
                        tmp_getattr_default_5 = const_str_angle_metaclass;
                        tmp_tuple_element_18 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                        if ( tmp_tuple_element_18 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 189;

                            goto try_except_handler_13;
                        }
                        tmp_right_name_5 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_18 );
                        CHECK_OBJECT( tmp_class_creation_5__prepared );
                        tmp_type_arg_10 = tmp_class_creation_5__prepared;
                        tmp_source_name_24 = BUILTIN_TYPE1( tmp_type_arg_10 );
                        assert( !(tmp_source_name_24 == NULL) );
                        tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_24 );
                        if ( tmp_tuple_element_18 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_5 );

                            exception_lineno = 189;

                            goto try_except_handler_13;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_18 );
                        tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                        Py_DECREF( tmp_right_name_5 );
                        if ( tmp_raise_value_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 189;

                            goto try_except_handler_13;
                        }
                        exception_type = tmp_raise_type_5;
                        Py_INCREF( tmp_raise_type_5 );
                        exception_value = tmp_raise_value_5;
                        exception_lineno = 189;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_13;
                    }
                    branch_no_22:;
                }
                goto branch_end_21;
                branch_no_21:;
                {
                    PyObject *tmp_assign_source_52;
                    tmp_assign_source_52 = PyDict_New();
                    assert( tmp_class_creation_5__prepared == NULL );
                    tmp_class_creation_5__prepared = tmp_assign_source_52;
                }
                branch_end_21:;
            }
            {
                PyObject *tmp_assign_source_53;
                {
                    PyObject *tmp_set_locals_5;
                    CHECK_OBJECT( tmp_class_creation_5__prepared );
                    tmp_set_locals_5 = tmp_class_creation_5__prepared;
                    locals_PIL$ImageShow_189 = tmp_set_locals_5;
                    Py_INCREF( tmp_set_locals_5 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_189, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_15;
                }
                tmp_dictset_value = const_str_plain_DisplayViewer;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_189, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_15;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_621adb69f1a7443e006c5aeb8b9b0b88_6, codeobj_621adb69f1a7443e006c5aeb8b9b0b88, module_PIL$ImageShow, sizeof(void *) );
                frame_621adb69f1a7443e006c5aeb8b9b0b88_6 = cache_frame_621adb69f1a7443e006c5aeb8b9b0b88_6;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 ) == 2 ); // Frame stack

                // Framed code:
                tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_15_get_command_ex(  );



                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_189, const_str_plain_get_command_ex, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 190;
                    type_description_2 = "o";
                    goto frame_exception_exit_6;
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_5;

                frame_exception_exit_6:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_621adb69f1a7443e006c5aeb8b9b0b88_6, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_621adb69f1a7443e006c5aeb8b9b0b88_6->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_621adb69f1a7443e006c5aeb8b9b0b88_6, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_621adb69f1a7443e006c5aeb8b9b0b88_6,
                    type_description_2,
                    outline_4_var___class__
                );


                // Release cached frame.
                if ( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 == cache_frame_621adb69f1a7443e006c5aeb8b9b0b88_6 )
                {
                    Py_DECREF( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 );
                }
                cache_frame_621adb69f1a7443e006c5aeb8b9b0b88_6 = NULL;

                assertFrameObject( frame_621adb69f1a7443e006c5aeb8b9b0b88_6 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_5;

                frame_no_exception_5:;
                goto skip_nested_handling_5;
                nested_frame_exit_5:;

                goto try_except_handler_15;
                skip_nested_handling_5:;
                {
                    nuitka_bool tmp_condition_result_33;
                    PyObject *tmp_compexpr_left_8;
                    PyObject *tmp_compexpr_right_8;
                    CHECK_OBJECT( tmp_class_creation_5__bases );
                    tmp_compexpr_left_8 = tmp_class_creation_5__bases;
                    CHECK_OBJECT( tmp_class_creation_5__bases_orig );
                    tmp_compexpr_right_8 = tmp_class_creation_5__bases_orig;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 189;

                        goto try_except_handler_15;
                    }
                    tmp_condition_result_33 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_23;
                    }
                    else
                    {
                        goto branch_no_23;
                    }
                    branch_yes_23:;
                    CHECK_OBJECT( tmp_class_creation_5__bases_orig );
                    tmp_dictset_value = tmp_class_creation_5__bases_orig;
                    tmp_res = PyObject_SetItem( locals_PIL$ImageShow_189, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 189;

                        goto try_except_handler_15;
                    }
                    branch_no_23:;
                }
                {
                    PyObject *tmp_assign_source_54;
                    PyObject *tmp_called_name_12;
                    PyObject *tmp_args_name_10;
                    PyObject *tmp_tuple_element_19;
                    PyObject *tmp_kw_name_10;
                    CHECK_OBJECT( tmp_class_creation_5__metaclass );
                    tmp_called_name_12 = tmp_class_creation_5__metaclass;
                    tmp_tuple_element_19 = const_str_plain_DisplayViewer;
                    tmp_args_name_10 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_19 );
                    PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_19 );
                    CHECK_OBJECT( tmp_class_creation_5__bases );
                    tmp_tuple_element_19 = tmp_class_creation_5__bases;
                    Py_INCREF( tmp_tuple_element_19 );
                    PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_19 );
                    tmp_tuple_element_19 = locals_PIL$ImageShow_189;
                    Py_INCREF( tmp_tuple_element_19 );
                    PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_19 );
                    CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
                    tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 189;
                    tmp_assign_source_54 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_10, tmp_kw_name_10 );
                    Py_DECREF( tmp_args_name_10 );
                    if ( tmp_assign_source_54 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 189;

                        goto try_except_handler_15;
                    }
                    assert( outline_4_var___class__ == NULL );
                    outline_4_var___class__ = tmp_assign_source_54;
                }
                CHECK_OBJECT( outline_4_var___class__ );
                tmp_assign_source_53 = outline_4_var___class__;
                Py_INCREF( tmp_assign_source_53 );
                goto try_return_handler_15;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_15:;
                Py_DECREF( locals_PIL$ImageShow_189 );
                locals_PIL$ImageShow_189 = NULL;
                goto try_return_handler_14;
                // Exception handler code:
                try_except_handler_15:;
                exception_keeper_type_13 = exception_type;
                exception_keeper_value_13 = exception_value;
                exception_keeper_tb_13 = exception_tb;
                exception_keeper_lineno_13 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_PIL$ImageShow_189 );
                locals_PIL$ImageShow_189 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_13;
                exception_value = exception_keeper_value_13;
                exception_tb = exception_keeper_tb_13;
                exception_lineno = exception_keeper_lineno_13;

                goto try_except_handler_14;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_14:;
                CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
                Py_DECREF( outline_4_var___class__ );
                outline_4_var___class__ = NULL;

                goto outline_result_5;
                // Exception handler code:
                try_except_handler_14:;
                exception_keeper_type_14 = exception_type;
                exception_keeper_value_14 = exception_value;
                exception_keeper_tb_14 = exception_tb;
                exception_keeper_lineno_14 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_14;
                exception_value = exception_keeper_value_14;
                exception_tb = exception_keeper_tb_14;
                exception_lineno = exception_keeper_lineno_14;

                goto outline_exception_5;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_5:;
                exception_lineno = 189;
                goto try_except_handler_13;
                outline_result_5:;
                UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_DisplayViewer, tmp_assign_source_53 );
            }
            goto try_end_5;
            // Exception handler code:
            try_except_handler_13:;
            exception_keeper_type_15 = exception_type;
            exception_keeper_value_15 = exception_value;
            exception_keeper_tb_15 = exception_tb;
            exception_keeper_lineno_15 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_5__bases_orig );
            tmp_class_creation_5__bases_orig = NULL;

            Py_XDECREF( tmp_class_creation_5__bases );
            tmp_class_creation_5__bases = NULL;

            Py_XDECREF( tmp_class_creation_5__class_decl_dict );
            tmp_class_creation_5__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_5__metaclass );
            tmp_class_creation_5__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_5__prepared );
            tmp_class_creation_5__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_15;
            exception_value = exception_keeper_value_15;
            exception_tb = exception_keeper_tb_15;
            exception_lineno = exception_keeper_lineno_15;

            goto frame_exception_exit_1;
            // End of try:
            try_end_5:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases_orig );
            Py_DECREF( tmp_class_creation_5__bases_orig );
            tmp_class_creation_5__bases_orig = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
            Py_DECREF( tmp_class_creation_5__bases );
            tmp_class_creation_5__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
            Py_DECREF( tmp_class_creation_5__class_decl_dict );
            tmp_class_creation_5__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
            Py_DECREF( tmp_class_creation_5__metaclass );
            tmp_class_creation_5__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
            Py_DECREF( tmp_class_creation_5__prepared );
            tmp_class_creation_5__prepared = NULL;

            {
                nuitka_bool tmp_condition_result_34;
                PyObject *tmp_called_name_13;
                PyObject *tmp_mvar_value_14;
                PyObject *tmp_call_result_3;
                int tmp_truth_name_6;
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_which );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_which );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "which" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 194;

                    goto frame_exception_exit_1;
                }

                tmp_called_name_13 = tmp_mvar_value_14;
                frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 194;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_plain_display_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 194;

                    goto frame_exception_exit_1;
                }
                tmp_truth_name_6 = CHECK_IF_TRUE( tmp_call_result_3 );
                if ( tmp_truth_name_6 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_3 );

                    exception_lineno = 194;

                    goto frame_exception_exit_1;
                }
                tmp_condition_result_34 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_3 );
                if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_24;
                }
                else
                {
                    goto branch_no_24;
                }
                branch_yes_24:;
                {
                    PyObject *tmp_called_name_14;
                    PyObject *tmp_mvar_value_15;
                    PyObject *tmp_call_result_4;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_mvar_value_16;
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_register );

                    if (unlikely( tmp_mvar_value_15 == NULL ))
                    {
                        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
                    }

                    if ( tmp_mvar_value_15 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 195;

                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_14 = tmp_mvar_value_15;
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_DisplayViewer );

                    if (unlikely( tmp_mvar_value_16 == NULL ))
                    {
                        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DisplayViewer );
                    }

                    if ( tmp_mvar_value_16 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DisplayViewer" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 195;

                        goto frame_exception_exit_1;
                    }

                    tmp_args_element_name_3 = tmp_mvar_value_16;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 195;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3 };
                        tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
                    }

                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 195;

                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                branch_no_24:;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_55;
                PyObject *tmp_tuple_element_20;
                PyObject *tmp_mvar_value_17;
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_UnixViewer );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnixViewer );
                }

                if ( tmp_mvar_value_17 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnixViewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 197;

                    goto try_except_handler_16;
                }

                tmp_tuple_element_20 = tmp_mvar_value_17;
                tmp_assign_source_55 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_20 );
                PyTuple_SET_ITEM( tmp_assign_source_55, 0, tmp_tuple_element_20 );
                assert( tmp_class_creation_6__bases_orig == NULL );
                tmp_class_creation_6__bases_orig = tmp_assign_source_55;
            }
            {
                PyObject *tmp_assign_source_56;
                PyObject *tmp_dircall_arg1_6;
                CHECK_OBJECT( tmp_class_creation_6__bases_orig );
                tmp_dircall_arg1_6 = tmp_class_creation_6__bases_orig;
                Py_INCREF( tmp_dircall_arg1_6 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_6};
                    tmp_assign_source_56 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_56 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                assert( tmp_class_creation_6__bases == NULL );
                tmp_class_creation_6__bases = tmp_assign_source_56;
            }
            {
                PyObject *tmp_assign_source_57;
                tmp_assign_source_57 = PyDict_New();
                assert( tmp_class_creation_6__class_decl_dict == NULL );
                tmp_class_creation_6__class_decl_dict = tmp_assign_source_57;
            }
            {
                PyObject *tmp_assign_source_58;
                PyObject *tmp_metaclass_name_6;
                nuitka_bool tmp_condition_result_35;
                PyObject *tmp_key_name_16;
                PyObject *tmp_dict_name_16;
                PyObject *tmp_dict_name_17;
                PyObject *tmp_key_name_17;
                nuitka_bool tmp_condition_result_36;
                int tmp_truth_name_7;
                PyObject *tmp_type_arg_11;
                PyObject *tmp_subscribed_name_6;
                PyObject *tmp_subscript_name_6;
                PyObject *tmp_bases_name_6;
                tmp_key_name_16 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
                tmp_dict_name_16 = tmp_class_creation_6__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_16, tmp_key_name_16 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                tmp_condition_result_35 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_11;
                }
                else
                {
                    goto condexpr_false_11;
                }
                condexpr_true_11:;
                CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
                tmp_dict_name_17 = tmp_class_creation_6__class_decl_dict;
                tmp_key_name_17 = const_str_plain_metaclass;
                tmp_metaclass_name_6 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
                if ( tmp_metaclass_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                goto condexpr_end_11;
                condexpr_false_11:;
                CHECK_OBJECT( tmp_class_creation_6__bases );
                tmp_truth_name_7 = CHECK_IF_TRUE( tmp_class_creation_6__bases );
                if ( tmp_truth_name_7 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                tmp_condition_result_36 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_12;
                }
                else
                {
                    goto condexpr_false_12;
                }
                condexpr_true_12:;
                CHECK_OBJECT( tmp_class_creation_6__bases );
                tmp_subscribed_name_6 = tmp_class_creation_6__bases;
                tmp_subscript_name_6 = const_int_0;
                tmp_type_arg_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
                if ( tmp_type_arg_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                tmp_metaclass_name_6 = BUILTIN_TYPE1( tmp_type_arg_11 );
                Py_DECREF( tmp_type_arg_11 );
                if ( tmp_metaclass_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                goto condexpr_end_12;
                condexpr_false_12:;
                tmp_metaclass_name_6 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_6 );
                condexpr_end_12:;
                condexpr_end_11:;
                CHECK_OBJECT( tmp_class_creation_6__bases );
                tmp_bases_name_6 = tmp_class_creation_6__bases;
                tmp_assign_source_58 = SELECT_METACLASS( tmp_metaclass_name_6, tmp_bases_name_6 );
                Py_DECREF( tmp_metaclass_name_6 );
                if ( tmp_assign_source_58 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                assert( tmp_class_creation_6__metaclass == NULL );
                tmp_class_creation_6__metaclass = tmp_assign_source_58;
            }
            {
                nuitka_bool tmp_condition_result_37;
                PyObject *tmp_key_name_18;
                PyObject *tmp_dict_name_18;
                tmp_key_name_18 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
                tmp_dict_name_18 = tmp_class_creation_6__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_18, tmp_key_name_18 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                tmp_condition_result_37 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_25;
                }
                else
                {
                    goto branch_no_25;
                }
                branch_yes_25:;
                CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_6__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_16;
                }
                branch_no_25:;
            }
            {
                nuitka_bool tmp_condition_result_38;
                PyObject *tmp_source_name_25;
                CHECK_OBJECT( tmp_class_creation_6__metaclass );
                tmp_source_name_25 = tmp_class_creation_6__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_25, const_str_plain___prepare__ );
                tmp_condition_result_38 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_38 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_26;
                }
                else
                {
                    goto branch_no_26;
                }
                branch_yes_26:;
                {
                    PyObject *tmp_assign_source_59;
                    PyObject *tmp_called_name_15;
                    PyObject *tmp_source_name_26;
                    PyObject *tmp_args_name_11;
                    PyObject *tmp_tuple_element_21;
                    PyObject *tmp_kw_name_11;
                    CHECK_OBJECT( tmp_class_creation_6__metaclass );
                    tmp_source_name_26 = tmp_class_creation_6__metaclass;
                    tmp_called_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain___prepare__ );
                    if ( tmp_called_name_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 197;

                        goto try_except_handler_16;
                    }
                    tmp_tuple_element_21 = const_str_plain_EogViewer;
                    tmp_args_name_11 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_21 );
                    PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_21 );
                    CHECK_OBJECT( tmp_class_creation_6__bases );
                    tmp_tuple_element_21 = tmp_class_creation_6__bases;
                    Py_INCREF( tmp_tuple_element_21 );
                    PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_21 );
                    CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
                    tmp_kw_name_11 = tmp_class_creation_6__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 197;
                    tmp_assign_source_59 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_11, tmp_kw_name_11 );
                    Py_DECREF( tmp_called_name_15 );
                    Py_DECREF( tmp_args_name_11 );
                    if ( tmp_assign_source_59 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 197;

                        goto try_except_handler_16;
                    }
                    assert( tmp_class_creation_6__prepared == NULL );
                    tmp_class_creation_6__prepared = tmp_assign_source_59;
                }
                {
                    nuitka_bool tmp_condition_result_39;
                    PyObject *tmp_operand_name_6;
                    PyObject *tmp_source_name_27;
                    CHECK_OBJECT( tmp_class_creation_6__prepared );
                    tmp_source_name_27 = tmp_class_creation_6__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_27, const_str_plain___getitem__ );
                    tmp_operand_name_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 197;

                        goto try_except_handler_16;
                    }
                    tmp_condition_result_39 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_39 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_27;
                    }
                    else
                    {
                        goto branch_no_27;
                    }
                    branch_yes_27:;
                    {
                        PyObject *tmp_raise_type_6;
                        PyObject *tmp_raise_value_6;
                        PyObject *tmp_left_name_6;
                        PyObject *tmp_right_name_6;
                        PyObject *tmp_tuple_element_22;
                        PyObject *tmp_getattr_target_6;
                        PyObject *tmp_getattr_attr_6;
                        PyObject *tmp_getattr_default_6;
                        PyObject *tmp_source_name_28;
                        PyObject *tmp_type_arg_12;
                        tmp_raise_type_6 = PyExc_TypeError;
                        tmp_left_name_6 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_6__metaclass );
                        tmp_getattr_target_6 = tmp_class_creation_6__metaclass;
                        tmp_getattr_attr_6 = const_str_plain___name__;
                        tmp_getattr_default_6 = const_str_angle_metaclass;
                        tmp_tuple_element_22 = BUILTIN_GETATTR( tmp_getattr_target_6, tmp_getattr_attr_6, tmp_getattr_default_6 );
                        if ( tmp_tuple_element_22 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 197;

                            goto try_except_handler_16;
                        }
                        tmp_right_name_6 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_6, 0, tmp_tuple_element_22 );
                        CHECK_OBJECT( tmp_class_creation_6__prepared );
                        tmp_type_arg_12 = tmp_class_creation_6__prepared;
                        tmp_source_name_28 = BUILTIN_TYPE1( tmp_type_arg_12 );
                        assert( !(tmp_source_name_28 == NULL) );
                        tmp_tuple_element_22 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_28 );
                        if ( tmp_tuple_element_22 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_6 );

                            exception_lineno = 197;

                            goto try_except_handler_16;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_6, 1, tmp_tuple_element_22 );
                        tmp_raise_value_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                        Py_DECREF( tmp_right_name_6 );
                        if ( tmp_raise_value_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 197;

                            goto try_except_handler_16;
                        }
                        exception_type = tmp_raise_type_6;
                        Py_INCREF( tmp_raise_type_6 );
                        exception_value = tmp_raise_value_6;
                        exception_lineno = 197;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_16;
                    }
                    branch_no_27:;
                }
                goto branch_end_26;
                branch_no_26:;
                {
                    PyObject *tmp_assign_source_60;
                    tmp_assign_source_60 = PyDict_New();
                    assert( tmp_class_creation_6__prepared == NULL );
                    tmp_class_creation_6__prepared = tmp_assign_source_60;
                }
                branch_end_26:;
            }
            {
                PyObject *tmp_assign_source_61;
                {
                    PyObject *tmp_set_locals_6;
                    CHECK_OBJECT( tmp_class_creation_6__prepared );
                    tmp_set_locals_6 = tmp_class_creation_6__prepared;
                    locals_PIL$ImageShow_197 = tmp_set_locals_6;
                    Py_INCREF( tmp_set_locals_6 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_197, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_18;
                }
                tmp_dictset_value = const_str_plain_EogViewer;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_197, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;

                    goto try_except_handler_18;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_8d5a3a6c708045300f009fb324038521_7, codeobj_8d5a3a6c708045300f009fb324038521, module_PIL$ImageShow, sizeof(void *) );
                frame_8d5a3a6c708045300f009fb324038521_7 = cache_frame_8d5a3a6c708045300f009fb324038521_7;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_8d5a3a6c708045300f009fb324038521_7 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_8d5a3a6c708045300f009fb324038521_7 ) == 2 ); // Frame stack

                // Framed code:
                tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_16_get_command_ex(  );



                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_197, const_str_plain_get_command_ex, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_2 = "o";
                    goto frame_exception_exit_7;
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_8d5a3a6c708045300f009fb324038521_7 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_6;

                frame_exception_exit_7:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_8d5a3a6c708045300f009fb324038521_7 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_8d5a3a6c708045300f009fb324038521_7, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_8d5a3a6c708045300f009fb324038521_7->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_8d5a3a6c708045300f009fb324038521_7, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_8d5a3a6c708045300f009fb324038521_7,
                    type_description_2,
                    outline_5_var___class__
                );


                // Release cached frame.
                if ( frame_8d5a3a6c708045300f009fb324038521_7 == cache_frame_8d5a3a6c708045300f009fb324038521_7 )
                {
                    Py_DECREF( frame_8d5a3a6c708045300f009fb324038521_7 );
                }
                cache_frame_8d5a3a6c708045300f009fb324038521_7 = NULL;

                assertFrameObject( frame_8d5a3a6c708045300f009fb324038521_7 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_6;

                frame_no_exception_6:;
                goto skip_nested_handling_6;
                nested_frame_exit_6:;

                goto try_except_handler_18;
                skip_nested_handling_6:;
                {
                    nuitka_bool tmp_condition_result_40;
                    PyObject *tmp_compexpr_left_9;
                    PyObject *tmp_compexpr_right_9;
                    CHECK_OBJECT( tmp_class_creation_6__bases );
                    tmp_compexpr_left_9 = tmp_class_creation_6__bases;
                    CHECK_OBJECT( tmp_class_creation_6__bases_orig );
                    tmp_compexpr_right_9 = tmp_class_creation_6__bases_orig;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 197;

                        goto try_except_handler_18;
                    }
                    tmp_condition_result_40 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_40 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_28;
                    }
                    else
                    {
                        goto branch_no_28;
                    }
                    branch_yes_28:;
                    CHECK_OBJECT( tmp_class_creation_6__bases_orig );
                    tmp_dictset_value = tmp_class_creation_6__bases_orig;
                    tmp_res = PyObject_SetItem( locals_PIL$ImageShow_197, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 197;

                        goto try_except_handler_18;
                    }
                    branch_no_28:;
                }
                {
                    PyObject *tmp_assign_source_62;
                    PyObject *tmp_called_name_16;
                    PyObject *tmp_args_name_12;
                    PyObject *tmp_tuple_element_23;
                    PyObject *tmp_kw_name_12;
                    CHECK_OBJECT( tmp_class_creation_6__metaclass );
                    tmp_called_name_16 = tmp_class_creation_6__metaclass;
                    tmp_tuple_element_23 = const_str_plain_EogViewer;
                    tmp_args_name_12 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_23 );
                    PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_23 );
                    CHECK_OBJECT( tmp_class_creation_6__bases );
                    tmp_tuple_element_23 = tmp_class_creation_6__bases;
                    Py_INCREF( tmp_tuple_element_23 );
                    PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_23 );
                    tmp_tuple_element_23 = locals_PIL$ImageShow_197;
                    Py_INCREF( tmp_tuple_element_23 );
                    PyTuple_SET_ITEM( tmp_args_name_12, 2, tmp_tuple_element_23 );
                    CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
                    tmp_kw_name_12 = tmp_class_creation_6__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 197;
                    tmp_assign_source_62 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_12, tmp_kw_name_12 );
                    Py_DECREF( tmp_args_name_12 );
                    if ( tmp_assign_source_62 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 197;

                        goto try_except_handler_18;
                    }
                    assert( outline_5_var___class__ == NULL );
                    outline_5_var___class__ = tmp_assign_source_62;
                }
                CHECK_OBJECT( outline_5_var___class__ );
                tmp_assign_source_61 = outline_5_var___class__;
                Py_INCREF( tmp_assign_source_61 );
                goto try_return_handler_18;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_18:;
                Py_DECREF( locals_PIL$ImageShow_197 );
                locals_PIL$ImageShow_197 = NULL;
                goto try_return_handler_17;
                // Exception handler code:
                try_except_handler_18:;
                exception_keeper_type_16 = exception_type;
                exception_keeper_value_16 = exception_value;
                exception_keeper_tb_16 = exception_tb;
                exception_keeper_lineno_16 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_PIL$ImageShow_197 );
                locals_PIL$ImageShow_197 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_16;
                exception_value = exception_keeper_value_16;
                exception_tb = exception_keeper_tb_16;
                exception_lineno = exception_keeper_lineno_16;

                goto try_except_handler_17;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_17:;
                CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
                Py_DECREF( outline_5_var___class__ );
                outline_5_var___class__ = NULL;

                goto outline_result_6;
                // Exception handler code:
                try_except_handler_17:;
                exception_keeper_type_17 = exception_type;
                exception_keeper_value_17 = exception_value;
                exception_keeper_tb_17 = exception_tb;
                exception_keeper_lineno_17 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_17;
                exception_value = exception_keeper_value_17;
                exception_tb = exception_keeper_tb_17;
                exception_lineno = exception_keeper_lineno_17;

                goto outline_exception_6;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_6:;
                exception_lineno = 197;
                goto try_except_handler_16;
                outline_result_6:;
                UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_EogViewer, tmp_assign_source_61 );
            }
            goto try_end_6;
            // Exception handler code:
            try_except_handler_16:;
            exception_keeper_type_18 = exception_type;
            exception_keeper_value_18 = exception_value;
            exception_keeper_tb_18 = exception_tb;
            exception_keeper_lineno_18 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_6__bases_orig );
            tmp_class_creation_6__bases_orig = NULL;

            Py_XDECREF( tmp_class_creation_6__bases );
            tmp_class_creation_6__bases = NULL;

            Py_XDECREF( tmp_class_creation_6__class_decl_dict );
            tmp_class_creation_6__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_6__metaclass );
            tmp_class_creation_6__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_6__prepared );
            tmp_class_creation_6__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_18;
            exception_value = exception_keeper_value_18;
            exception_tb = exception_keeper_tb_18;
            exception_lineno = exception_keeper_lineno_18;

            goto frame_exception_exit_1;
            // End of try:
            try_end_6:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases_orig );
            Py_DECREF( tmp_class_creation_6__bases_orig );
            tmp_class_creation_6__bases_orig = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases );
            Py_DECREF( tmp_class_creation_6__bases );
            tmp_class_creation_6__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_6__class_decl_dict );
            Py_DECREF( tmp_class_creation_6__class_decl_dict );
            tmp_class_creation_6__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_6__metaclass );
            Py_DECREF( tmp_class_creation_6__metaclass );
            tmp_class_creation_6__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_6__prepared );
            Py_DECREF( tmp_class_creation_6__prepared );
            tmp_class_creation_6__prepared = NULL;

            {
                nuitka_bool tmp_condition_result_41;
                PyObject *tmp_called_name_17;
                PyObject *tmp_mvar_value_18;
                PyObject *tmp_call_result_5;
                int tmp_truth_name_8;
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_which );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_which );
                }

                if ( tmp_mvar_value_18 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "which" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 202;

                    goto frame_exception_exit_1;
                }

                tmp_called_name_17 = tmp_mvar_value_18;
                frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 202;
                tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_eog_tuple, 0 ) );

                if ( tmp_call_result_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 202;

                    goto frame_exception_exit_1;
                }
                tmp_truth_name_8 = CHECK_IF_TRUE( tmp_call_result_5 );
                if ( tmp_truth_name_8 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_5 );

                    exception_lineno = 202;

                    goto frame_exception_exit_1;
                }
                tmp_condition_result_41 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_5 );
                if ( tmp_condition_result_41 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_29;
                }
                else
                {
                    goto branch_no_29;
                }
                branch_yes_29:;
                {
                    PyObject *tmp_called_name_18;
                    PyObject *tmp_mvar_value_19;
                    PyObject *tmp_call_result_6;
                    PyObject *tmp_args_element_name_4;
                    PyObject *tmp_mvar_value_20;
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_register );

                    if (unlikely( tmp_mvar_value_19 == NULL ))
                    {
                        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
                    }

                    if ( tmp_mvar_value_19 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 203;

                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_18 = tmp_mvar_value_19;
                    tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_EogViewer );

                    if (unlikely( tmp_mvar_value_20 == NULL ))
                    {
                        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EogViewer );
                    }

                    if ( tmp_mvar_value_20 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EogViewer" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 203;

                        goto frame_exception_exit_1;
                    }

                    tmp_args_element_name_4 = tmp_mvar_value_20;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 203;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_4 };
                        tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
                    }

                    if ( tmp_call_result_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 203;

                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_6 );
                }
                branch_no_29:;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_63;
                PyObject *tmp_tuple_element_24;
                PyObject *tmp_mvar_value_21;
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_UnixViewer );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnixViewer );
                }

                if ( tmp_mvar_value_21 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnixViewer" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 205;

                    goto try_except_handler_19;
                }

                tmp_tuple_element_24 = tmp_mvar_value_21;
                tmp_assign_source_63 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_24 );
                PyTuple_SET_ITEM( tmp_assign_source_63, 0, tmp_tuple_element_24 );
                assert( tmp_class_creation_7__bases_orig == NULL );
                tmp_class_creation_7__bases_orig = tmp_assign_source_63;
            }
            {
                PyObject *tmp_assign_source_64;
                PyObject *tmp_dircall_arg1_7;
                CHECK_OBJECT( tmp_class_creation_7__bases_orig );
                tmp_dircall_arg1_7 = tmp_class_creation_7__bases_orig;
                Py_INCREF( tmp_dircall_arg1_7 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_7};
                    tmp_assign_source_64 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_64 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                assert( tmp_class_creation_7__bases == NULL );
                tmp_class_creation_7__bases = tmp_assign_source_64;
            }
            {
                PyObject *tmp_assign_source_65;
                tmp_assign_source_65 = PyDict_New();
                assert( tmp_class_creation_7__class_decl_dict == NULL );
                tmp_class_creation_7__class_decl_dict = tmp_assign_source_65;
            }
            {
                PyObject *tmp_assign_source_66;
                PyObject *tmp_metaclass_name_7;
                nuitka_bool tmp_condition_result_42;
                PyObject *tmp_key_name_19;
                PyObject *tmp_dict_name_19;
                PyObject *tmp_dict_name_20;
                PyObject *tmp_key_name_20;
                nuitka_bool tmp_condition_result_43;
                int tmp_truth_name_9;
                PyObject *tmp_type_arg_13;
                PyObject *tmp_subscribed_name_7;
                PyObject *tmp_subscript_name_7;
                PyObject *tmp_bases_name_7;
                tmp_key_name_19 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
                tmp_dict_name_19 = tmp_class_creation_7__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_19, tmp_key_name_19 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                tmp_condition_result_42 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_42 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_13;
                }
                else
                {
                    goto condexpr_false_13;
                }
                condexpr_true_13:;
                CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
                tmp_dict_name_20 = tmp_class_creation_7__class_decl_dict;
                tmp_key_name_20 = const_str_plain_metaclass;
                tmp_metaclass_name_7 = DICT_GET_ITEM( tmp_dict_name_20, tmp_key_name_20 );
                if ( tmp_metaclass_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                goto condexpr_end_13;
                condexpr_false_13:;
                CHECK_OBJECT( tmp_class_creation_7__bases );
                tmp_truth_name_9 = CHECK_IF_TRUE( tmp_class_creation_7__bases );
                if ( tmp_truth_name_9 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                tmp_condition_result_43 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_43 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_14;
                }
                else
                {
                    goto condexpr_false_14;
                }
                condexpr_true_14:;
                CHECK_OBJECT( tmp_class_creation_7__bases );
                tmp_subscribed_name_7 = tmp_class_creation_7__bases;
                tmp_subscript_name_7 = const_int_0;
                tmp_type_arg_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 0 );
                if ( tmp_type_arg_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                tmp_metaclass_name_7 = BUILTIN_TYPE1( tmp_type_arg_13 );
                Py_DECREF( tmp_type_arg_13 );
                if ( tmp_metaclass_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                goto condexpr_end_14;
                condexpr_false_14:;
                tmp_metaclass_name_7 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_7 );
                condexpr_end_14:;
                condexpr_end_13:;
                CHECK_OBJECT( tmp_class_creation_7__bases );
                tmp_bases_name_7 = tmp_class_creation_7__bases;
                tmp_assign_source_66 = SELECT_METACLASS( tmp_metaclass_name_7, tmp_bases_name_7 );
                Py_DECREF( tmp_metaclass_name_7 );
                if ( tmp_assign_source_66 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                assert( tmp_class_creation_7__metaclass == NULL );
                tmp_class_creation_7__metaclass = tmp_assign_source_66;
            }
            {
                nuitka_bool tmp_condition_result_44;
                PyObject *tmp_key_name_21;
                PyObject *tmp_dict_name_21;
                tmp_key_name_21 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
                tmp_dict_name_21 = tmp_class_creation_7__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_21, tmp_key_name_21 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                tmp_condition_result_44 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_44 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_30;
                }
                else
                {
                    goto branch_no_30;
                }
                branch_yes_30:;
                CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_7__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_19;
                }
                branch_no_30:;
            }
            {
                nuitka_bool tmp_condition_result_45;
                PyObject *tmp_source_name_29;
                CHECK_OBJECT( tmp_class_creation_7__metaclass );
                tmp_source_name_29 = tmp_class_creation_7__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_29, const_str_plain___prepare__ );
                tmp_condition_result_45 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_45 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_31;
                }
                else
                {
                    goto branch_no_31;
                }
                branch_yes_31:;
                {
                    PyObject *tmp_assign_source_67;
                    PyObject *tmp_called_name_19;
                    PyObject *tmp_source_name_30;
                    PyObject *tmp_args_name_13;
                    PyObject *tmp_tuple_element_25;
                    PyObject *tmp_kw_name_13;
                    CHECK_OBJECT( tmp_class_creation_7__metaclass );
                    tmp_source_name_30 = tmp_class_creation_7__metaclass;
                    tmp_called_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain___prepare__ );
                    if ( tmp_called_name_19 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 205;

                        goto try_except_handler_19;
                    }
                    tmp_tuple_element_25 = const_str_plain_XVViewer;
                    tmp_args_name_13 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_25 );
                    PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_25 );
                    CHECK_OBJECT( tmp_class_creation_7__bases );
                    tmp_tuple_element_25 = tmp_class_creation_7__bases;
                    Py_INCREF( tmp_tuple_element_25 );
                    PyTuple_SET_ITEM( tmp_args_name_13, 1, tmp_tuple_element_25 );
                    CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
                    tmp_kw_name_13 = tmp_class_creation_7__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 205;
                    tmp_assign_source_67 = CALL_FUNCTION( tmp_called_name_19, tmp_args_name_13, tmp_kw_name_13 );
                    Py_DECREF( tmp_called_name_19 );
                    Py_DECREF( tmp_args_name_13 );
                    if ( tmp_assign_source_67 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 205;

                        goto try_except_handler_19;
                    }
                    assert( tmp_class_creation_7__prepared == NULL );
                    tmp_class_creation_7__prepared = tmp_assign_source_67;
                }
                {
                    nuitka_bool tmp_condition_result_46;
                    PyObject *tmp_operand_name_7;
                    PyObject *tmp_source_name_31;
                    CHECK_OBJECT( tmp_class_creation_7__prepared );
                    tmp_source_name_31 = tmp_class_creation_7__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_31, const_str_plain___getitem__ );
                    tmp_operand_name_7 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_7 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 205;

                        goto try_except_handler_19;
                    }
                    tmp_condition_result_46 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_46 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_32;
                    }
                    else
                    {
                        goto branch_no_32;
                    }
                    branch_yes_32:;
                    {
                        PyObject *tmp_raise_type_7;
                        PyObject *tmp_raise_value_7;
                        PyObject *tmp_left_name_7;
                        PyObject *tmp_right_name_7;
                        PyObject *tmp_tuple_element_26;
                        PyObject *tmp_getattr_target_7;
                        PyObject *tmp_getattr_attr_7;
                        PyObject *tmp_getattr_default_7;
                        PyObject *tmp_source_name_32;
                        PyObject *tmp_type_arg_14;
                        tmp_raise_type_7 = PyExc_TypeError;
                        tmp_left_name_7 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_7__metaclass );
                        tmp_getattr_target_7 = tmp_class_creation_7__metaclass;
                        tmp_getattr_attr_7 = const_str_plain___name__;
                        tmp_getattr_default_7 = const_str_angle_metaclass;
                        tmp_tuple_element_26 = BUILTIN_GETATTR( tmp_getattr_target_7, tmp_getattr_attr_7, tmp_getattr_default_7 );
                        if ( tmp_tuple_element_26 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 205;

                            goto try_except_handler_19;
                        }
                        tmp_right_name_7 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_7, 0, tmp_tuple_element_26 );
                        CHECK_OBJECT( tmp_class_creation_7__prepared );
                        tmp_type_arg_14 = tmp_class_creation_7__prepared;
                        tmp_source_name_32 = BUILTIN_TYPE1( tmp_type_arg_14 );
                        assert( !(tmp_source_name_32 == NULL) );
                        tmp_tuple_element_26 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_32 );
                        if ( tmp_tuple_element_26 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_7 );

                            exception_lineno = 205;

                            goto try_except_handler_19;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_7, 1, tmp_tuple_element_26 );
                        tmp_raise_value_7 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                        Py_DECREF( tmp_right_name_7 );
                        if ( tmp_raise_value_7 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 205;

                            goto try_except_handler_19;
                        }
                        exception_type = tmp_raise_type_7;
                        Py_INCREF( tmp_raise_type_7 );
                        exception_value = tmp_raise_value_7;
                        exception_lineno = 205;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                        goto try_except_handler_19;
                    }
                    branch_no_32:;
                }
                goto branch_end_31;
                branch_no_31:;
                {
                    PyObject *tmp_assign_source_68;
                    tmp_assign_source_68 = PyDict_New();
                    assert( tmp_class_creation_7__prepared == NULL );
                    tmp_class_creation_7__prepared = tmp_assign_source_68;
                }
                branch_end_31:;
            }
            {
                PyObject *tmp_assign_source_69;
                {
                    PyObject *tmp_set_locals_7;
                    CHECK_OBJECT( tmp_class_creation_7__prepared );
                    tmp_set_locals_7 = tmp_class_creation_7__prepared;
                    locals_PIL$ImageShow_205 = tmp_set_locals_7;
                    Py_INCREF( tmp_set_locals_7 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_417b20c428ec11e5abc58f5b33589964;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_205, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_21;
                }
                tmp_dictset_value = const_str_plain_XVViewer;
                tmp_res = PyObject_SetItem( locals_PIL$ImageShow_205, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;

                    goto try_except_handler_21;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_dde79275ab8f4680c1f745554f06161e_8, codeobj_dde79275ab8f4680c1f745554f06161e, module_PIL$ImageShow, sizeof(void *) );
                frame_dde79275ab8f4680c1f745554f06161e_8 = cache_frame_dde79275ab8f4680c1f745554f06161e_8;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_dde79275ab8f4680c1f745554f06161e_8 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_dde79275ab8f4680c1f745554f06161e_8 ) == 2 ); // Frame stack

                // Framed code:
                {
                    PyObject *tmp_defaults_3;
                    tmp_defaults_3 = const_tuple_none_tuple;
                    Py_INCREF( tmp_defaults_3 );
                    tmp_dictset_value = MAKE_FUNCTION_PIL$ImageShow$$$function_17_get_command_ex( tmp_defaults_3 );



                    tmp_res = PyObject_SetItem( locals_PIL$ImageShow_205, const_str_plain_get_command_ex, tmp_dictset_value );
                    Py_DECREF( tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 206;
                        type_description_2 = "o";
                        goto frame_exception_exit_8;
                    }
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_dde79275ab8f4680c1f745554f06161e_8 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_7;

                frame_exception_exit_8:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_dde79275ab8f4680c1f745554f06161e_8 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_dde79275ab8f4680c1f745554f06161e_8, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_dde79275ab8f4680c1f745554f06161e_8->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_dde79275ab8f4680c1f745554f06161e_8, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_dde79275ab8f4680c1f745554f06161e_8,
                    type_description_2,
                    outline_6_var___class__
                );


                // Release cached frame.
                if ( frame_dde79275ab8f4680c1f745554f06161e_8 == cache_frame_dde79275ab8f4680c1f745554f06161e_8 )
                {
                    Py_DECREF( frame_dde79275ab8f4680c1f745554f06161e_8 );
                }
                cache_frame_dde79275ab8f4680c1f745554f06161e_8 = NULL;

                assertFrameObject( frame_dde79275ab8f4680c1f745554f06161e_8 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_7;

                frame_no_exception_7:;
                goto skip_nested_handling_7;
                nested_frame_exit_7:;

                goto try_except_handler_21;
                skip_nested_handling_7:;
                {
                    nuitka_bool tmp_condition_result_47;
                    PyObject *tmp_compexpr_left_10;
                    PyObject *tmp_compexpr_right_10;
                    CHECK_OBJECT( tmp_class_creation_7__bases );
                    tmp_compexpr_left_10 = tmp_class_creation_7__bases;
                    CHECK_OBJECT( tmp_class_creation_7__bases_orig );
                    tmp_compexpr_right_10 = tmp_class_creation_7__bases_orig;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 205;

                        goto try_except_handler_21;
                    }
                    tmp_condition_result_47 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_47 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_33;
                    }
                    else
                    {
                        goto branch_no_33;
                    }
                    branch_yes_33:;
                    CHECK_OBJECT( tmp_class_creation_7__bases_orig );
                    tmp_dictset_value = tmp_class_creation_7__bases_orig;
                    tmp_res = PyObject_SetItem( locals_PIL$ImageShow_205, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 205;

                        goto try_except_handler_21;
                    }
                    branch_no_33:;
                }
                {
                    PyObject *tmp_assign_source_70;
                    PyObject *tmp_called_name_20;
                    PyObject *tmp_args_name_14;
                    PyObject *tmp_tuple_element_27;
                    PyObject *tmp_kw_name_14;
                    CHECK_OBJECT( tmp_class_creation_7__metaclass );
                    tmp_called_name_20 = tmp_class_creation_7__metaclass;
                    tmp_tuple_element_27 = const_str_plain_XVViewer;
                    tmp_args_name_14 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_27 );
                    PyTuple_SET_ITEM( tmp_args_name_14, 0, tmp_tuple_element_27 );
                    CHECK_OBJECT( tmp_class_creation_7__bases );
                    tmp_tuple_element_27 = tmp_class_creation_7__bases;
                    Py_INCREF( tmp_tuple_element_27 );
                    PyTuple_SET_ITEM( tmp_args_name_14, 1, tmp_tuple_element_27 );
                    tmp_tuple_element_27 = locals_PIL$ImageShow_205;
                    Py_INCREF( tmp_tuple_element_27 );
                    PyTuple_SET_ITEM( tmp_args_name_14, 2, tmp_tuple_element_27 );
                    CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
                    tmp_kw_name_14 = tmp_class_creation_7__class_decl_dict;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 205;
                    tmp_assign_source_70 = CALL_FUNCTION( tmp_called_name_20, tmp_args_name_14, tmp_kw_name_14 );
                    Py_DECREF( tmp_args_name_14 );
                    if ( tmp_assign_source_70 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 205;

                        goto try_except_handler_21;
                    }
                    assert( outline_6_var___class__ == NULL );
                    outline_6_var___class__ = tmp_assign_source_70;
                }
                CHECK_OBJECT( outline_6_var___class__ );
                tmp_assign_source_69 = outline_6_var___class__;
                Py_INCREF( tmp_assign_source_69 );
                goto try_return_handler_21;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_21:;
                Py_DECREF( locals_PIL$ImageShow_205 );
                locals_PIL$ImageShow_205 = NULL;
                goto try_return_handler_20;
                // Exception handler code:
                try_except_handler_21:;
                exception_keeper_type_19 = exception_type;
                exception_keeper_value_19 = exception_value;
                exception_keeper_tb_19 = exception_tb;
                exception_keeper_lineno_19 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_PIL$ImageShow_205 );
                locals_PIL$ImageShow_205 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_19;
                exception_value = exception_keeper_value_19;
                exception_tb = exception_keeper_tb_19;
                exception_lineno = exception_keeper_lineno_19;

                goto try_except_handler_20;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                // Return handler code:
                try_return_handler_20:;
                CHECK_OBJECT( (PyObject *)outline_6_var___class__ );
                Py_DECREF( outline_6_var___class__ );
                outline_6_var___class__ = NULL;

                goto outline_result_7;
                // Exception handler code:
                try_except_handler_20:;
                exception_keeper_type_20 = exception_type;
                exception_keeper_value_20 = exception_value;
                exception_keeper_tb_20 = exception_tb;
                exception_keeper_lineno_20 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_20;
                exception_value = exception_keeper_value_20;
                exception_tb = exception_keeper_tb_20;
                exception_lineno = exception_keeper_lineno_20;

                goto outline_exception_7;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( PIL$ImageShow );
                return MOD_RETURN_VALUE( NULL );
                outline_exception_7:;
                exception_lineno = 205;
                goto try_except_handler_19;
                outline_result_7:;
                UPDATE_STRING_DICT1( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_XVViewer, tmp_assign_source_69 );
            }
            goto try_end_7;
            // Exception handler code:
            try_except_handler_19:;
            exception_keeper_type_21 = exception_type;
            exception_keeper_value_21 = exception_value;
            exception_keeper_tb_21 = exception_tb;
            exception_keeper_lineno_21 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_7__bases_orig );
            tmp_class_creation_7__bases_orig = NULL;

            Py_XDECREF( tmp_class_creation_7__bases );
            tmp_class_creation_7__bases = NULL;

            Py_XDECREF( tmp_class_creation_7__class_decl_dict );
            tmp_class_creation_7__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_7__metaclass );
            tmp_class_creation_7__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_7__prepared );
            tmp_class_creation_7__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_21;
            exception_value = exception_keeper_value_21;
            exception_tb = exception_keeper_tb_21;
            exception_lineno = exception_keeper_lineno_21;

            goto frame_exception_exit_1;
            // End of try:
            try_end_7:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_7__bases_orig );
            Py_DECREF( tmp_class_creation_7__bases_orig );
            tmp_class_creation_7__bases_orig = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_7__bases );
            Py_DECREF( tmp_class_creation_7__bases );
            tmp_class_creation_7__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_7__class_decl_dict );
            Py_DECREF( tmp_class_creation_7__class_decl_dict );
            tmp_class_creation_7__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_7__metaclass );
            Py_DECREF( tmp_class_creation_7__metaclass );
            tmp_class_creation_7__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_7__prepared );
            Py_DECREF( tmp_class_creation_7__prepared );
            tmp_class_creation_7__prepared = NULL;

            {
                nuitka_bool tmp_condition_result_48;
                PyObject *tmp_called_name_21;
                PyObject *tmp_mvar_value_22;
                PyObject *tmp_call_result_7;
                int tmp_truth_name_10;
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_which );

                if (unlikely( tmp_mvar_value_22 == NULL ))
                {
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_which );
                }

                if ( tmp_mvar_value_22 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "which" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 214;

                    goto frame_exception_exit_1;
                }

                tmp_called_name_21 = tmp_mvar_value_22;
                frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 214;
                tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_str_plain_xv_tuple, 0 ) );

                if ( tmp_call_result_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 214;

                    goto frame_exception_exit_1;
                }
                tmp_truth_name_10 = CHECK_IF_TRUE( tmp_call_result_7 );
                if ( tmp_truth_name_10 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_7 );

                    exception_lineno = 214;

                    goto frame_exception_exit_1;
                }
                tmp_condition_result_48 = tmp_truth_name_10 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_7 );
                if ( tmp_condition_result_48 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_34;
                }
                else
                {
                    goto branch_no_34;
                }
                branch_yes_34:;
                {
                    PyObject *tmp_called_name_22;
                    PyObject *tmp_mvar_value_23;
                    PyObject *tmp_call_result_8;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_mvar_value_24;
                    tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_register );

                    if (unlikely( tmp_mvar_value_23 == NULL ))
                    {
                        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
                    }

                    if ( tmp_mvar_value_23 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 215;

                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_22 = tmp_mvar_value_23;
                    tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageShow, (Nuitka_StringObject *)const_str_plain_XVViewer );

                    if (unlikely( tmp_mvar_value_24 == NULL ))
                    {
                        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XVViewer );
                    }

                    if ( tmp_mvar_value_24 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "XVViewer" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 215;

                        goto frame_exception_exit_1;
                    }

                    tmp_args_element_name_5 = tmp_mvar_value_24;
                    frame_f8eb43222464beea8566d39e2743ee92->m_frame.f_lineno = 215;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_5 };
                        tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
                    }

                    if ( tmp_call_result_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 215;

                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_8 );
                }
                branch_no_34:;
            }
            branch_end_11:;
        }
        branch_end_6:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f8eb43222464beea8566d39e2743ee92 );
#endif
    popFrameStack();

    assertFrameObject( frame_f8eb43222464beea8566d39e2743ee92 );

    goto frame_no_exception_8;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f8eb43222464beea8566d39e2743ee92 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f8eb43222464beea8566d39e2743ee92, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f8eb43222464beea8566d39e2743ee92->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f8eb43222464beea8566d39e2743ee92, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_8:;

    return MOD_RETURN_VALUE( module_PIL$ImageShow );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
