/* Generated code for Python module 'nbconvert.filters.ansi'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbconvert$filters$ansi" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbconvert$filters$ansi;
PyDictObject *moduledict_nbconvert$filters$ansi;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain__get_extended_color;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_int_pos_24;
extern PyObject *const_int_pos_22;
static PyObject *const_str_digest_1ff171db268cd23eca2e5ac6c10ad8a6;
extern PyObject *const_str_plain___file__;
extern PyObject *const_int_pos_55;
extern PyObject *const_str_plain_group;
static PyObject *const_str_plain__ANSI_COLORS;
extern PyObject *const_int_pos_39;
static PyObject *const_str_digest_f393d2d23d9d200ea835839ef9db9e5b;
static PyObject *const_str_digest_ace9eab81453cf75424fea0f41abbdc3;
extern PyObject *const_str_plain_bold;
static PyObject *const_str_digest_0d1dca1b0178235f9579d05489903b00;
extern PyObject *const_str_plain_m;
static PyObject *const_str_plain__latexconverter;
static PyObject *const_str_digest_4b175065a725220958033ad5a6ae95a0;
extern PyObject *const_str_plain_ansi2html;
extern PyObject *const_str_plain_sub;
extern PyObject *const_str_plain_end;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_bg;
extern PyObject *const_str_plain_classes;
extern PyObject *const_int_pos_27;
extern PyObject *const_int_pos_5;
extern PyObject *const_tuple_str_chr_59_tuple;
extern PyObject *const_int_pos_97;
static PyObject *const_tuple_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b_tuple;
static PyObject *const_str_digest_58a264413831ca8cd55685774282d190;
extern PyObject *const_str_digest_41e98c7cc1ef944f96b5cfc5c1252f70;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_utils;
extern PyObject *const_str_plain_start;
static PyObject *const_xrange_0_8;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_int_pos_16;
static PyObject *const_str_digest_4d635eb4049f62e0b56339038045f93a;
extern PyObject *const_int_pos_30;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_01272028dee0e900b7dfb28b25ca6d94;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_chr_62;
static PyObject *const_str_digest_f675ffcaef98887e5fa932e5d8c01d01;
static PyObject *const_tuple_str_digest_234471512520ae48c7f14090928fae2f_tuple;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_str_digest_19d865f6380bc81b1d77e285bcd35c1b;
extern PyObject *const_str_plain_n;
static PyObject *const_str_digest_730f109b6863a24160a9deec18cc5573;
static PyObject *const_str_digest_d2839e524c16fe287454716b62e1c528;
static PyObject *const_str_digest_992e9240907bcf9687bf2ad96f1bdf0d;
static PyObject *const_tuple_int_pos_21_int_pos_22_tuple;
extern PyObject *const_int_pos_38;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_plain_endtag;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_g;
static PyObject *const_str_digest_d523d6a7d4fe66258bc0ea49419c76af;
static PyObject *const_str_digest_006e40fe17078cfc334c6899486a5797;
static PyObject *const_str_digest_c10e631569b91b4a6ef7765bebb21a71;
static PyObject *const_str_digest_cf46ea5ecce2c3b908f1b0197fcbd6b0;
extern PyObject *const_str_plain_all;
extern PyObject *const_int_pos_8;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_c_tuple;
static PyObject *const_str_digest_8ec66f772ebd70941d98927629962cd9;
static PyObject *const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple;
static PyObject *const_str_digest_eb47a2a30205b2518e68f374a96456d9;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_234471512520ae48c7f14090928fae2f;
extern PyObject *const_str_space;
static PyObject *const_str_digest_5decc50f6b60a72cfc7a91db021a7536;
extern PyObject *const_str_plain_numbers;
extern PyObject *const_str_plain_out;
extern PyObject *const_str_plain_append;
extern PyObject *const_int_pos_48;
extern PyObject *const_str_chr_125;
static PyObject *const_str_digest_2bcd36a837a6bcd3dc5707391e5d72eb;
static PyObject *const_str_digest_ee5629dc23fa4d27f356b50b0aaf4fa4;
extern PyObject *const_str_plain_r;
extern PyObject *const_int_pos_10;
static PyObject *const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple;
extern PyObject *const_str_plain_fg;
extern PyObject *const_str_plain_styles;
extern PyObject *const_str_plain_compile;
static PyObject *const_str_plain__htmlconverter;
static PyObject *const_str_digest_f0a700a760d19879aaaba7b39e379bd5;
extern PyObject *const_str_plain_b;
static PyObject *const_str_digest_e3e20b1250f8607c43e98a3559e4d9b9;
extern PyObject *const_str_plain_split;
static PyObject *const_str_digest_0565824281459b13c58c134fdef74c54;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_ansi2latex;
extern PyObject *const_str_plain_underline;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_inverse;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_digest_37c5b6975ec1413a79aad25d886c5902;
static PyObject *const_str_digest_fa51db684424c16c00745202341fc215;
static PyObject *const_str_digest_dc8bc559f19bb84aaf2a155c00a1bfc9;
extern PyObject *const_str_plain_pop;
static PyObject *const_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b;
extern PyObject *const_int_0;
static PyObject *const_str_digest_0dabe95c13acefb112e7541387c7b612;
static PyObject *const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple;
extern PyObject *const_int_pos_21;
extern PyObject *const_tuple_int_pos_2_tuple;
extern PyObject *const_str_plain_text;
extern PyObject *const_int_pos_37;
extern PyObject *const_int_pos_107;
static PyObject *const_str_digest_2ce2c996889a20c510a171e4e8bf83ba;
extern PyObject *const_str_plain_search;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain__ansi2anything;
static PyObject *const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple;
static PyObject *const_str_digest_85da1f2a5bedec973a2c706c289411cf;
static PyObject *const_str_digest_96f5dde4ffd3af721ecf069eb3e013af;
extern PyObject *const_tuple_str_plain_text_tuple;
static PyObject *const_str_digest_11b18af4c2a33e057af45f86db9324e8;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_c;
static PyObject *const_str_digest_36af21b46d7fcc42c935be869c70d384;
static PyObject *const_tuple_none_none_false_false_false_tuple;
extern PyObject *const_str_plain_converter;
static PyObject *const_str_digest_09daaa4311e53c88fafcd6bc4ada19c8;
static PyObject *const_str_digest_ad78f42d023c3d1e12b7024ca8601061;
extern PyObject *const_int_pos_36;
extern PyObject *const_int_pos_4;
static PyObject *const_str_plain_starttag;
extern PyObject *const_tuple_str_empty_str_empty_tuple;
extern PyObject *const_tuple_str_plain_n_tuple;
static PyObject *const_tuple_str_digest_f0bb28bf95577296167e47cbfbce5457_tuple;
extern PyObject *const_int_pos_90;
static PyObject *const_str_digest_dfa7d79eeee95e06a7ce71756f59428f;
extern PyObject *const_int_pos_47;
static PyObject *const_tuple_str_digest_8ec66f772ebd70941d98927629962cd9_tuple;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_49539f82574722b3b3918405db0e61b8;
extern PyObject *const_str_plain_idx;
extern PyObject *const_str_plain_source;
static PyObject *const_tuple_str_digest_e3e20b1250f8607c43e98a3559e4d9b9_tuple;
extern PyObject *const_str_plain_chunk;
extern PyObject *const_tuple_str_plain_source_tuple;
static PyObject *const_str_digest_bd410b583fe6c40b5bae1d2c8cdc8986;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_cde2b2c9c0e13f4d6e9f7dcaa1d69c3d;
extern PyObject *const_int_pos_256;
static PyObject *const_str_plain__ANSI_RE;
extern PyObject *const_str_plain_jinja2;
static PyObject *const_str_digest_dab519039f2cf6f9b13dec763c93ec88;
static PyObject *const_str_digest_421daf503d9a0e29da1789304b9388ec;
static PyObject *const_str_digest_17fa4c93bb815db2006cadc7c8613890;
extern PyObject *const_str_chr_34;
extern PyObject *const_int_pos_3;
static PyObject *const_str_digest_8dba28c58ea0449dab447d1fca87088a;
extern PyObject *const_str_digest_b26d3cd4cb167eb4277cbd83e0c608a6;
static PyObject *const_str_digest_f3f58f6c13bc1ed32570513d30f6fa49;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_plain_clear;
extern PyObject *const_str_plain_strip_ansi;
extern PyObject *const_int_pos_40;
extern PyObject *const_int_pos_7;
static PyObject *const_str_digest_06447dfc18f52089335ac0dd87e70cb3;
static PyObject *const_str_digest_96837971d0ba13cc170ea27eba8e62d6;
static PyObject *const_list_597d25cf901b12bc3e0fbe1d16fe702d_list;
extern PyObject *const_str_chr_59;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_fdc9678f371cf698e2fa760aab40d69f;
static PyObject *const_str_digest_05ebc7feaf1a25f4f304812f8163497d;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_empty;
extern PyObject *const_int_pos_49;
static PyObject *const_int_pos_232;
static PyObject *const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_str_plain_escape;
extern PyObject *const_int_pos_100;
static PyObject *const_str_digest_f0bb28bf95577296167e47cbfbce5457;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain__get_extended_color = UNSTREAM_STRING_ASCII( &constant_bin[ 2745070 ], 19, 1 );
    const_str_digest_1ff171db268cd23eca2e5ac6c10ad8a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1092754 ], 8, 0 );
    const_str_plain__ANSI_COLORS = UNSTREAM_STRING_ASCII( &constant_bin[ 2745089 ], 12, 1 );
    const_str_digest_f393d2d23d9d200ea835839ef9db9e5b = UNSTREAM_STRING_ASCII( &constant_bin[ 2745101 ], 20, 0 );
    const_str_digest_ace9eab81453cf75424fea0f41abbdc3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745121 ], 7, 0 );
    const_str_digest_0d1dca1b0178235f9579d05489903b00 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745128 ], 9, 0 );
    const_str_plain__latexconverter = UNSTREAM_STRING_ASCII( &constant_bin[ 2745137 ], 15, 1 );
    const_str_digest_4b175065a725220958033ad5a6ae95a0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745152 ], 42, 0 );
    const_tuple_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b_tuple = PyTuple_New( 1 );
    const_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b = UNSTREAM_STRING_ASCII( &constant_bin[ 2745194 ], 15, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b_tuple, 0, const_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b ); Py_INCREF( const_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b );
    const_str_digest_58a264413831ca8cd55685774282d190 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745209 ], 36, 0 );
    const_xrange_0_8 = BUILTIN_XRANGE3( const_int_0, const_int_pos_8, const_int_pos_1 );
    const_str_digest_4d635eb4049f62e0b56339038045f93a = UNSTREAM_STRING_ASCII( &constant_bin[ 2745245 ], 25, 0 );
    const_str_digest_01272028dee0e900b7dfb28b25ca6d94 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745270 ], 84, 0 );
    const_str_digest_f675ffcaef98887e5fa932e5d8c01d01 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745354 ], 38, 0 );
    const_tuple_str_digest_234471512520ae48c7f14090928fae2f_tuple = PyTuple_New( 1 );
    const_str_digest_234471512520ae48c7f14090928fae2f = UNSTREAM_STRING_ASCII( &constant_bin[ 2745392 ], 14, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_234471512520ae48c7f14090928fae2f_tuple, 0, const_str_digest_234471512520ae48c7f14090928fae2f ); Py_INCREF( const_str_digest_234471512520ae48c7f14090928fae2f );
    const_str_digest_19d865f6380bc81b1d77e285bcd35c1b = UNSTREAM_STRING_ASCII( &constant_bin[ 2745406 ], 31, 0 );
    const_str_digest_730f109b6863a24160a9deec18cc5573 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745437 ], 10, 0 );
    const_str_digest_d2839e524c16fe287454716b62e1c528 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745447 ], 5, 0 );
    const_str_digest_992e9240907bcf9687bf2ad96f1bdf0d = UNSTREAM_STRING_ASCII( &constant_bin[ 2745452 ], 58, 0 );
    const_tuple_int_pos_21_int_pos_22_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_21_int_pos_22_tuple, 0, const_int_pos_21 ); Py_INCREF( const_int_pos_21 );
    PyTuple_SET_ITEM( const_tuple_int_pos_21_int_pos_22_tuple, 1, const_int_pos_22 ); Py_INCREF( const_int_pos_22 );
    const_str_plain_endtag = UNSTREAM_STRING_ASCII( &constant_bin[ 2745510 ], 6, 1 );
    const_str_digest_d523d6a7d4fe66258bc0ea49419c76af = UNSTREAM_STRING_ASCII( &constant_bin[ 2745516 ], 20, 0 );
    const_str_digest_006e40fe17078cfc334c6899486a5797 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745536 ], 10, 0 );
    const_str_digest_c10e631569b91b4a6ef7765bebb21a71 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745546 ], 375, 0 );
    const_str_digest_cf46ea5ecce2c3b908f1b0197fcbd6b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745921 ], 38, 0 );
    const_str_digest_8ec66f772ebd70941d98927629962cd9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745959 ], 9, 0 );
    const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 0, const_str_plain_numbers ); Py_INCREF( const_str_plain_numbers );
    PyTuple_SET_ITEM( const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 1, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 2, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 3, const_str_plain_g ); Py_INCREF( const_str_plain_g );
    PyTuple_SET_ITEM( const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 4, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 5, const_str_plain_idx ); Py_INCREF( const_str_plain_idx );
    const_str_digest_eb47a2a30205b2518e68f374a96456d9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745968 ], 8, 0 );
    const_str_digest_5decc50f6b60a72cfc7a91db021a7536 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745976 ], 10, 0 );
    const_str_digest_2bcd36a837a6bcd3dc5707391e5d72eb = UNSTREAM_STRING_ASCII( &constant_bin[ 2745986 ], 25, 0 );
    const_str_digest_ee5629dc23fa4d27f356b50b0aaf4fa4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746011 ], 35, 0 );
    const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple = PyTuple_New( 14 );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 1, const_str_plain_converter ); Py_INCREF( const_str_plain_converter );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 2, const_str_plain_fg ); Py_INCREF( const_str_plain_fg );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 3, const_str_plain_bg ); Py_INCREF( const_str_plain_bg );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 4, const_str_plain_bold ); Py_INCREF( const_str_plain_bold );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 5, const_str_plain_underline ); Py_INCREF( const_str_plain_underline );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 6, const_str_plain_inverse ); Py_INCREF( const_str_plain_inverse );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 7, const_str_plain_numbers ); Py_INCREF( const_str_plain_numbers );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 8, const_str_plain_out ); Py_INCREF( const_str_plain_out );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 9, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 10, const_str_plain_chunk ); Py_INCREF( const_str_plain_chunk );
    const_str_plain_starttag = UNSTREAM_STRING_ASCII( &constant_bin[ 2746046 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 11, const_str_plain_starttag ); Py_INCREF( const_str_plain_starttag );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 12, const_str_plain_endtag ); Py_INCREF( const_str_plain_endtag );
    PyTuple_SET_ITEM( const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 13, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    const_str_plain__htmlconverter = UNSTREAM_STRING_ASCII( &constant_bin[ 2746054 ], 14, 1 );
    const_str_digest_f0a700a760d19879aaaba7b39e379bd5 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746068 ], 31, 0 );
    const_str_digest_e3e20b1250f8607c43e98a3559e4d9b9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746021 ], 23, 0 );
    const_str_digest_0565824281459b13c58c134fdef74c54 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746099 ], 39, 0 );
    const_str_digest_37c5b6975ec1413a79aad25d886c5902 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746138 ], 16, 0 );
    const_str_digest_fa51db684424c16c00745202341fc215 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746154 ], 82, 0 );
    const_str_digest_dc8bc559f19bb84aaf2a155c00a1bfc9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746236 ], 153, 0 );
    const_str_digest_0dabe95c13acefb112e7541387c7b612 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745209 ], 11, 0 );
    const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 0, const_str_plain_fg ); Py_INCREF( const_str_plain_fg );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 1, const_str_plain_bg ); Py_INCREF( const_str_plain_bg );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 2, const_str_plain_bold ); Py_INCREF( const_str_plain_bold );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 3, const_str_plain_underline ); Py_INCREF( const_str_plain_underline );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 4, const_str_plain_inverse ); Py_INCREF( const_str_plain_inverse );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 5, const_str_plain_starttag ); Py_INCREF( const_str_plain_starttag );
    PyTuple_SET_ITEM( const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 6, const_str_plain_endtag ); Py_INCREF( const_str_plain_endtag );
    const_str_digest_2ce2c996889a20c510a171e4e8bf83ba = UNSTREAM_STRING_ASCII( &constant_bin[ 2538547 ], 8, 0 );
    const_str_plain__ansi2anything = UNSTREAM_STRING_ASCII( &constant_bin[ 2746389 ], 14, 1 );
    const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 0, const_str_plain_fg ); Py_INCREF( const_str_plain_fg );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 1, const_str_plain_bg ); Py_INCREF( const_str_plain_bg );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 2, const_str_plain_bold ); Py_INCREF( const_str_plain_bold );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 3, const_str_plain_underline ); Py_INCREF( const_str_plain_underline );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 4, const_str_plain_inverse ); Py_INCREF( const_str_plain_inverse );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 5, const_str_plain_classes ); Py_INCREF( const_str_plain_classes );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 6, const_str_plain_styles ); Py_INCREF( const_str_plain_styles );
    PyTuple_SET_ITEM( const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 7, const_str_plain_starttag ); Py_INCREF( const_str_plain_starttag );
    const_str_digest_85da1f2a5bedec973a2c706c289411cf = UNSTREAM_STRING_ASCII( &constant_bin[ 2746403 ], 151, 0 );
    const_str_digest_96f5dde4ffd3af721ecf069eb3e013af = UNSTREAM_STRING_ASCII( &constant_bin[ 2745240 ], 3, 0 );
    const_str_digest_11b18af4c2a33e057af45f86db9324e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746138 ], 8, 0 );
    const_str_digest_36af21b46d7fcc42c935be869c70d384 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745516 ], 12, 0 );
    const_tuple_none_none_false_false_false_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_none_none_false_false_false_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_false_false_false_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_false_false_false_tuple, 2, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_none_none_false_false_false_tuple, 3, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_none_none_false_false_false_tuple, 4, Py_False ); Py_INCREF( Py_False );
    const_str_digest_09daaa4311e53c88fafcd6bc4ada19c8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746554 ], 9, 0 );
    const_str_digest_ad78f42d023c3d1e12b7024ca8601061 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746076 ], 22, 0 );
    const_tuple_str_digest_f0bb28bf95577296167e47cbfbce5457_tuple = PyTuple_New( 1 );
    const_str_digest_f0bb28bf95577296167e47cbfbce5457 = UNSTREAM_STRING_ASCII( &constant_bin[ 2745220 ], 23, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_f0bb28bf95577296167e47cbfbce5457_tuple, 0, const_str_digest_f0bb28bf95577296167e47cbfbce5457 ); Py_INCREF( const_str_digest_f0bb28bf95577296167e47cbfbce5457 );
    const_str_digest_dfa7d79eeee95e06a7ce71756f59428f = UNSTREAM_STRING_ASCII( &constant_bin[ 104684 ], 2, 0 );
    const_tuple_str_digest_8ec66f772ebd70941d98927629962cd9_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_8ec66f772ebd70941d98927629962cd9_tuple, 0, const_str_digest_8ec66f772ebd70941d98927629962cd9 ); Py_INCREF( const_str_digest_8ec66f772ebd70941d98927629962cd9 );
    const_str_digest_49539f82574722b3b3918405db0e61b8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746563 ], 17, 0 );
    const_tuple_str_digest_e3e20b1250f8607c43e98a3559e4d9b9_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e3e20b1250f8607c43e98a3559e4d9b9_tuple, 0, const_str_digest_e3e20b1250f8607c43e98a3559e4d9b9 ); Py_INCREF( const_str_digest_e3e20b1250f8607c43e98a3559e4d9b9 );
    const_str_digest_bd410b583fe6c40b5bae1d2c8cdc8986 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746580 ], 19, 0 );
    const_str_digest_cde2b2c9c0e13f4d6e9f7dcaa1d69c3d = UNSTREAM_STRING_ASCII( &constant_bin[ 2746599 ], 17, 0 );
    const_str_plain__ANSI_RE = UNSTREAM_STRING_ASCII( &constant_bin[ 2746616 ], 8, 1 );
    const_str_digest_dab519039f2cf6f9b13dec763c93ec88 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746624 ], 7, 0 );
    const_str_digest_421daf503d9a0e29da1789304b9388ec = UNSTREAM_STRING_ASCII( &constant_bin[ 2746631 ], 18, 0 );
    const_str_digest_17fa4c93bb815db2006cadc7c8613890 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746649 ], 133, 0 );
    const_str_digest_8dba28c58ea0449dab447d1fca87088a = UNSTREAM_STRING_ASCII( &constant_bin[ 2746580 ], 11, 0 );
    const_str_digest_f3f58f6c13bc1ed32570513d30f6fa49 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746782 ], 11, 0 );
    const_str_digest_06447dfc18f52089335ac0dd87e70cb3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746793 ], 18, 0 );
    const_str_digest_96837971d0ba13cc170ea27eba8e62d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 2746811 ], 18, 0 );
    const_list_597d25cf901b12bc3e0fbe1d16fe702d_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_597d25cf901b12bc3e0fbe1d16fe702d_list, 0, const_str_plain_strip_ansi ); Py_INCREF( const_str_plain_strip_ansi );
    PyList_SET_ITEM( const_list_597d25cf901b12bc3e0fbe1d16fe702d_list, 1, const_str_plain_ansi2html ); Py_INCREF( const_str_plain_ansi2html );
    PyList_SET_ITEM( const_list_597d25cf901b12bc3e0fbe1d16fe702d_list, 2, const_str_plain_ansi2latex ); Py_INCREF( const_str_plain_ansi2latex );
    const_str_digest_fdc9678f371cf698e2fa760aab40d69f = UNSTREAM_STRING_ASCII( &constant_bin[ 2746829 ], 42, 0 );
    const_str_digest_05ebc7feaf1a25f4f304812f8163497d = UNSTREAM_STRING_ASCII( &constant_bin[ 2746631 ], 10, 0 );
    const_int_pos_232 = PyLong_FromUnsignedLong( 232ul );
    const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple = PyTuple_New( 16 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 0, const_str_digest_006e40fe17078cfc334c6899486a5797 ); Py_INCREF( const_str_digest_006e40fe17078cfc334c6899486a5797 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 1, const_str_digest_11b18af4c2a33e057af45f86db9324e8 ); Py_INCREF( const_str_digest_11b18af4c2a33e057af45f86db9324e8 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 2, const_str_digest_730f109b6863a24160a9deec18cc5573 ); Py_INCREF( const_str_digest_730f109b6863a24160a9deec18cc5573 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 3, const_str_digest_8dba28c58ea0449dab447d1fca87088a ); Py_INCREF( const_str_digest_8dba28c58ea0449dab447d1fca87088a );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 4, const_str_digest_0d1dca1b0178235f9579d05489903b00 ); Py_INCREF( const_str_digest_0d1dca1b0178235f9579d05489903b00 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 5, const_str_digest_36af21b46d7fcc42c935be869c70d384 ); Py_INCREF( const_str_digest_36af21b46d7fcc42c935be869c70d384 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 6, const_str_digest_09daaa4311e53c88fafcd6bc4ada19c8 ); Py_INCREF( const_str_digest_09daaa4311e53c88fafcd6bc4ada19c8 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 7, const_str_digest_05ebc7feaf1a25f4f304812f8163497d ); Py_INCREF( const_str_digest_05ebc7feaf1a25f4f304812f8163497d );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 8, const_str_digest_06447dfc18f52089335ac0dd87e70cb3 ); Py_INCREF( const_str_digest_06447dfc18f52089335ac0dd87e70cb3 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 9, const_str_digest_37c5b6975ec1413a79aad25d886c5902 ); Py_INCREF( const_str_digest_37c5b6975ec1413a79aad25d886c5902 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 10, const_str_digest_96837971d0ba13cc170ea27eba8e62d6 ); Py_INCREF( const_str_digest_96837971d0ba13cc170ea27eba8e62d6 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 11, const_str_digest_bd410b583fe6c40b5bae1d2c8cdc8986 ); Py_INCREF( const_str_digest_bd410b583fe6c40b5bae1d2c8cdc8986 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 12, const_str_digest_cde2b2c9c0e13f4d6e9f7dcaa1d69c3d ); Py_INCREF( const_str_digest_cde2b2c9c0e13f4d6e9f7dcaa1d69c3d );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 13, const_str_digest_d523d6a7d4fe66258bc0ea49419c76af ); Py_INCREF( const_str_digest_d523d6a7d4fe66258bc0ea49419c76af );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 14, const_str_digest_49539f82574722b3b3918405db0e61b8 ); Py_INCREF( const_str_digest_49539f82574722b3b3918405db0e61b8 );
    PyTuple_SET_ITEM( const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple, 15, const_str_digest_421daf503d9a0e29da1789304b9388ec ); Py_INCREF( const_str_digest_421daf503d9a0e29da1789304b9388ec );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbconvert$filters$ansi( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_7809e82659fc448aa0ab0dbe65c8d484;
static PyCodeObject *codeobj_ffac657803e54ac28ddd892a14318711;
static PyCodeObject *codeobj_acfc9bc0751f324dc3a67f294912c38c;
static PyCodeObject *codeobj_751ac65bcf26755606db7ed0c99281e9;
static PyCodeObject *codeobj_8f09e924ecaf0438920b75bcf0f1041a;
static PyCodeObject *codeobj_68d17afc28a356befa5597ef06df715b;
static PyCodeObject *codeobj_968be856d81cfb420acbf0a59e382a6d;
static PyCodeObject *codeobj_5e4a6582b5daa4b2ae997bb2a2a0a328;
static PyCodeObject *codeobj_56c953ba45592034f684ef34ecd6d8b1;
static PyCodeObject *codeobj_e4ff26e45c720f09fbbb16c2ca85f3ac;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_4d635eb4049f62e0b56339038045f93a );
    codeobj_7809e82659fc448aa0ab0dbe65c8d484 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 271, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_c_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ffac657803e54ac28ddd892a14318711 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 198, const_tuple_str_plain_n_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_acfc9bc0751f324dc3a67f294912c38c = MAKE_CODEOBJ( module_filename_obj, const_str_digest_f0a700a760d19879aaaba7b39e379bd5, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_751ac65bcf26755606db7ed0c99281e9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__ansi2anything, 171, const_tuple_dd1670bd2593a46112ccdc08b269e8db_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8f09e924ecaf0438920b75bcf0f1041a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_extended_color, 264, const_tuple_4e19a5f8b79f7d67bd84871e2fbbecca_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_68d17afc28a356befa5597ef06df715b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__htmlconverter, 77, const_tuple_fdb9f00980fa21e0b66b789af6e6846b_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_968be856d81cfb420acbf0a59e382a6d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__latexconverter, 120, const_tuple_8566f34e21d4eda96b8a2c6a2dc6910e_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5e4a6582b5daa4b2ae997bb2a2a0a328 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ansi2html, 50, const_tuple_str_plain_text_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_56c953ba45592034f684ef34ecd6d8b1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ansi2latex, 64, const_tuple_str_plain_text_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e4ff26e45c720f09fbbb16c2ca85f3ac = MAKE_CODEOBJ( module_filename_obj, const_str_plain_strip_ansi, 37, const_tuple_str_plain_source_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_1_strip_ansi(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_2_ansi2html(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_3_ansi2latex(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_4__htmlconverter(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_5__latexconverter(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_6__ansi2anything(  );


static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_7__get_extended_color(  );


// The module function definitions.
static PyObject *impl_nbconvert$filters$ansi$$$function_1_strip_ansi( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_source = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e4ff26e45c720f09fbbb16c2ca85f3ac;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e4ff26e45c720f09fbbb16c2ca85f3ac = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e4ff26e45c720f09fbbb16c2ca85f3ac, codeobj_e4ff26e45c720f09fbbb16c2ca85f3ac, module_nbconvert$filters$ansi, sizeof(void *) );
    frame_e4ff26e45c720f09fbbb16c2ca85f3ac = cache_frame_e4ff26e45c720f09fbbb16c2ca85f3ac;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e4ff26e45c720f09fbbb16c2ca85f3ac );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e4ff26e45c720f09fbbb16c2ca85f3ac ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_RE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ANSI_RE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ANSI_RE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        tmp_args_element_name_1 = const_str_empty;
        CHECK_OBJECT( par_source );
        tmp_args_element_name_2 = par_source;
        frame_e4ff26e45c720f09fbbb16c2ca85f3ac->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_sub, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4ff26e45c720f09fbbb16c2ca85f3ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4ff26e45c720f09fbbb16c2ca85f3ac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4ff26e45c720f09fbbb16c2ca85f3ac );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e4ff26e45c720f09fbbb16c2ca85f3ac, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e4ff26e45c720f09fbbb16c2ca85f3ac->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e4ff26e45c720f09fbbb16c2ca85f3ac, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e4ff26e45c720f09fbbb16c2ca85f3ac,
        type_description_1,
        par_source
    );


    // Release cached frame.
    if ( frame_e4ff26e45c720f09fbbb16c2ca85f3ac == cache_frame_e4ff26e45c720f09fbbb16c2ca85f3ac )
    {
        Py_DECREF( frame_e4ff26e45c720f09fbbb16c2ca85f3ac );
    }
    cache_frame_e4ff26e45c720f09fbbb16c2ca85f3ac = NULL;

    assertFrameObject( frame_e4ff26e45c720f09fbbb16c2ca85f3ac );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_1_strip_ansi );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_1_strip_ansi );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$ansi$$$function_2_ansi2html( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5e4a6582b5daa4b2ae997bb2a2a0a328;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5e4a6582b5daa4b2ae997bb2a2a0a328 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5e4a6582b5daa4b2ae997bb2a2a0a328, codeobj_5e4a6582b5daa4b2ae997bb2a2a0a328, module_nbconvert$filters$ansi, sizeof(void *) );
    frame_5e4a6582b5daa4b2ae997bb2a2a0a328 = cache_frame_5e4a6582b5daa4b2ae997bb2a2a0a328;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_jinja2 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_jinja2 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "jinja2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_utils );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_5e4a6582b5daa4b2ae997bb2a2a0a328->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_escape, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_text;
            assert( old != NULL );
            par_text = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ansi2anything );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ansi2anything );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ansi2anything" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_2 = par_text;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__htmlconverter );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__htmlconverter );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_htmlconverter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_3;
        frame_5e4a6582b5daa4b2ae997bb2a2a0a328->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5e4a6582b5daa4b2ae997bb2a2a0a328, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5e4a6582b5daa4b2ae997bb2a2a0a328->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5e4a6582b5daa4b2ae997bb2a2a0a328, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5e4a6582b5daa4b2ae997bb2a2a0a328,
        type_description_1,
        par_text
    );


    // Release cached frame.
    if ( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 == cache_frame_5e4a6582b5daa4b2ae997bb2a2a0a328 )
    {
        Py_DECREF( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 );
    }
    cache_frame_5e4a6582b5daa4b2ae997bb2a2a0a328 = NULL;

    assertFrameObject( frame_5e4a6582b5daa4b2ae997bb2a2a0a328 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_2_ansi2html );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_2_ansi2html );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$ansi$$$function_3_ansi2latex( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_56c953ba45592034f684ef34ecd6d8b1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_56c953ba45592034f684ef34ecd6d8b1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_56c953ba45592034f684ef34ecd6d8b1, codeobj_56c953ba45592034f684ef34ecd6d8b1, module_nbconvert$filters$ansi, sizeof(void *) );
    frame_56c953ba45592034f684ef34ecd6d8b1 = cache_frame_56c953ba45592034f684ef34ecd6d8b1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_56c953ba45592034f684ef34ecd6d8b1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_56c953ba45592034f684ef34ecd6d8b1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ansi2anything );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ansi2anything );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ansi2anything" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__latexconverter );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__latexconverter );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_latexconverter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_2;
        frame_56c953ba45592034f684ef34ecd6d8b1->m_frame.f_lineno = 74;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_56c953ba45592034f684ef34ecd6d8b1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_56c953ba45592034f684ef34ecd6d8b1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_56c953ba45592034f684ef34ecd6d8b1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_56c953ba45592034f684ef34ecd6d8b1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_56c953ba45592034f684ef34ecd6d8b1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_56c953ba45592034f684ef34ecd6d8b1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_56c953ba45592034f684ef34ecd6d8b1,
        type_description_1,
        par_text
    );


    // Release cached frame.
    if ( frame_56c953ba45592034f684ef34ecd6d8b1 == cache_frame_56c953ba45592034f684ef34ecd6d8b1 )
    {
        Py_DECREF( frame_56c953ba45592034f684ef34ecd6d8b1 );
    }
    cache_frame_56c953ba45592034f684ef34ecd6d8b1 = NULL;

    assertFrameObject( frame_56c953ba45592034f684ef34ecd6d8b1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_3_ansi2latex );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_3_ansi2latex );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$ansi$$$function_4__htmlconverter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fg = python_pars[ 0 ];
    PyObject *par_bg = python_pars[ 1 ];
    PyObject *par_bold = python_pars[ 2 ];
    PyObject *par_underline = python_pars[ 3 ];
    PyObject *par_inverse = python_pars[ 4 ];
    PyObject *var_classes = NULL;
    PyObject *var_styles = NULL;
    PyObject *var_starttag = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_68d17afc28a356befa5597ef06df715b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_68d17afc28a356befa5597ef06df715b = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_68d17afc28a356befa5597ef06df715b, codeobj_68d17afc28a356befa5597ef06df715b, module_nbconvert$filters$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_68d17afc28a356befa5597ef06df715b = cache_frame_68d17afc28a356befa5597ef06df715b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_68d17afc28a356befa5597ef06df715b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_68d17afc28a356befa5597ef06df715b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_fg );
        tmp_tuple_element_1 = par_fg;
        tmp_compexpr_left_1 = PyTuple_New( 5 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_bg );
        tmp_tuple_element_1 = par_bg;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_bold );
        tmp_tuple_element_1 = par_bold;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_underline );
        tmp_tuple_element_1 = par_underline;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_inverse );
        tmp_tuple_element_1 = par_inverse;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 4, tmp_tuple_element_1 );
        tmp_compexpr_right_1 = const_tuple_none_none_false_false_false_tuple;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_tuple_str_empty_str_empty_tuple;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_classes == NULL );
        var_classes = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_styles == NULL );
        var_styles = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_inverse );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_inverse );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_tuple_element_2;
            CHECK_OBJECT( par_bg );
            tmp_tuple_element_2 = par_bg;
            tmp_iter_arg_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_iter_arg_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( par_fg );
            tmp_tuple_element_2 = par_fg;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_iter_arg_1, 1, tmp_tuple_element_2 );
            tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            assert( !(tmp_assign_source_3 == NULL) );
            assert( tmp_tuple_unpack_1__source_iter == NULL );
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 89;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_1 == NULL );
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
            if ( tmp_assign_source_5 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 89;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_2 == NULL );
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
            {
                PyObject *old = par_fg;
                assert( old != NULL );
                par_fg = tmp_assign_source_6;
                Py_INCREF( par_fg );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
            {
                PyObject *old = par_bg;
                assert( old != NULL );
                par_bg = tmp_assign_source_7;
                Py_INCREF( par_bg );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_fg );
        tmp_isinstance_inst_1 = par_fg;
        tmp_isinstance_cls_1 = (PyObject *)&PyLong_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( var_classes );
            tmp_source_name_1 = var_classes;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ANSI_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 92;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_fg );
            tmp_subscript_name_1 = par_fg;
            tmp_left_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 92;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = const_str_digest_96f5dde4ffd3af721ecf069eb3e013af;
            tmp_args_element_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 92;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 92;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            int tmp_truth_name_2;
            CHECK_OBJECT( par_fg );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_fg );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_2;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_dircall_arg1_1;
                PyObject *tmp_source_name_3;
                PyObject *tmp_dircall_arg2_1;
                CHECK_OBJECT( var_styles );
                tmp_source_name_2 = var_styles;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_source_name_3 = const_str_digest_f393d2d23d9d200ea835839ef9db9e5b;
                tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_format );
                assert( !(tmp_dircall_arg1_1 == NULL) );
                CHECK_OBJECT( par_fg );
                tmp_dircall_arg2_1 = par_fg;
                Py_INCREF( tmp_dircall_arg2_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
                    tmp_args_element_name_2 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
                }
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    exception_lineno = 94;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 94;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                int tmp_truth_name_3;
                CHECK_OBJECT( par_inverse );
                tmp_truth_name_3 = CHECK_IF_TRUE( par_inverse );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 95;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_called_instance_1;
                    PyObject *tmp_call_result_3;
                    CHECK_OBJECT( var_classes );
                    tmp_called_instance_1 = var_classes;
                    frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 96;
                    tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_digest_f0bb28bf95577296167e47cbfbce5457_tuple, 0 ) );

                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 96;
                        type_description_1 = "oooooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                branch_no_5:;
            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_isinstance_inst_2;
        PyObject *tmp_isinstance_cls_2;
        CHECK_OBJECT( par_bg );
        tmp_isinstance_inst_2 = par_bg;
        tmp_isinstance_cls_2 = (PyObject *)&PyLong_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_left_name_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_classes );
            tmp_source_name_4 = var_classes;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ANSI_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( par_bg );
            tmp_subscript_name_2 = par_bg;
            tmp_left_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 99;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = const_str_digest_b26d3cd4cb167eb4277cbd83e0c608a6;
            tmp_args_element_name_3 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 99;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 99;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_7;
            int tmp_truth_name_4;
            CHECK_OBJECT( par_bg );
            tmp_truth_name_4 = CHECK_IF_TRUE( par_bg );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_5;
                PyObject *tmp_call_result_5;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_dircall_arg1_2;
                PyObject *tmp_source_name_6;
                PyObject *tmp_dircall_arg2_2;
                CHECK_OBJECT( var_styles );
                tmp_source_name_5 = var_styles;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_append );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_source_name_6 = const_str_digest_19d865f6380bc81b1d77e285bcd35c1b;
                tmp_dircall_arg1_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_format );
                assert( !(tmp_dircall_arg1_2 == NULL) );
                CHECK_OBJECT( par_bg );
                tmp_dircall_arg2_2 = par_bg;
                Py_INCREF( tmp_dircall_arg2_2 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2};
                    tmp_args_element_name_4 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
                }
                if ( tmp_args_element_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 101;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 101;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4 };
                    tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_4 );
                if ( tmp_call_result_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 101;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_5 );
            }
            goto branch_end_7;
            branch_no_7:;
            {
                nuitka_bool tmp_condition_result_8;
                int tmp_truth_name_5;
                CHECK_OBJECT( par_inverse );
                tmp_truth_name_5 = CHECK_IF_TRUE( par_inverse );
                if ( tmp_truth_name_5 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 102;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_8 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_called_instance_2;
                    PyObject *tmp_call_result_6;
                    CHECK_OBJECT( var_classes );
                    tmp_called_instance_2 = var_classes;
                    frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 103;
                    tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_digest_e3e20b1250f8607c43e98a3559e4d9b9_tuple, 0 ) );

                    if ( tmp_call_result_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 103;
                        type_description_1 = "oooooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_6 );
                }
                branch_no_8:;
            }
            branch_end_7:;
        }
        branch_end_6:;
    }
    {
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_6;
        CHECK_OBJECT( par_bold );
        tmp_truth_name_6 = CHECK_IF_TRUE( par_bold );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_7;
            CHECK_OBJECT( var_classes );
            tmp_called_instance_3 = var_classes;
            frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 106;
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_digest_8ec66f772ebd70941d98927629962cd9_tuple, 0 ) );

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        int tmp_truth_name_7;
        CHECK_OBJECT( par_underline );
        tmp_truth_name_7 = CHECK_IF_TRUE( par_underline );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_10 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_call_result_8;
            CHECK_OBJECT( var_classes );
            tmp_called_instance_4 = var_classes;
            frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 109;
            tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_digest_234471512520ae48c7f14090928fae2f_tuple, 0 ) );

            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_no_10:;
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_str_digest_d2839e524c16fe287454716b62e1c528;
        assert( var_starttag == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_starttag = tmp_assign_source_8;
    }
    {
        nuitka_bool tmp_condition_result_11;
        int tmp_truth_name_8;
        CHECK_OBJECT( var_classes );
        tmp_truth_name_8 = CHECK_IF_TRUE( var_classes );
        assert( !(tmp_truth_name_8 == -1) );
        tmp_condition_result_11 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_left_name_4;
            PyObject *tmp_left_name_5;
            PyObject *tmp_right_name_4;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_right_name_5;
            tmp_left_name_3 = const_str_digest_d2839e524c16fe287454716b62e1c528;
            tmp_left_name_5 = const_str_digest_1ff171db268cd23eca2e5ac6c10ad8a6;
            tmp_called_instance_5 = const_str_space;
            CHECK_OBJECT( var_classes );
            tmp_args_element_name_5 = var_classes;
            frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 113;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_right_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_join, call_args );
            }

            if ( tmp_right_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_5, tmp_right_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_left_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_5 = const_str_chr_34;
            tmp_right_name_3 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_4, tmp_right_name_5 );
            Py_DECREF( tmp_left_name_4 );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_9 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_starttag;
                assert( old != NULL );
                var_starttag = tmp_assign_source_9;
                Py_DECREF( old );
            }

        }
        branch_no_11:;
    }
    {
        nuitka_bool tmp_condition_result_12;
        int tmp_truth_name_9;
        CHECK_OBJECT( var_styles );
        tmp_truth_name_9 = CHECK_IF_TRUE( var_styles );
        assert( !(tmp_truth_name_9 == -1) );
        tmp_condition_result_12 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_12;
        }
        else
        {
            goto branch_no_12;
        }
        branch_yes_12:;
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_left_name_6;
            PyObject *tmp_right_name_6;
            PyObject *tmp_left_name_7;
            PyObject *tmp_left_name_8;
            PyObject *tmp_right_name_7;
            PyObject *tmp_called_instance_6;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_right_name_8;
            CHECK_OBJECT( var_starttag );
            tmp_left_name_6 = var_starttag;
            tmp_left_name_8 = const_str_digest_2ce2c996889a20c510a171e4e8bf83ba;
            tmp_called_instance_6 = const_str_digest_41e98c7cc1ef944f96b5cfc5c1252f70;
            CHECK_OBJECT( var_styles );
            tmp_args_element_name_6 = var_styles;
            frame_68d17afc28a356befa5597ef06df715b->m_frame.f_lineno = 115;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_right_name_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_join, call_args );
            }

            if ( tmp_right_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_7 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_8, tmp_right_name_7 );
            Py_DECREF( tmp_right_name_7 );
            if ( tmp_left_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_8 = const_str_chr_34;
            tmp_right_name_6 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_7, tmp_right_name_8 );
            Py_DECREF( tmp_left_name_7 );
            if ( tmp_right_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_6, tmp_right_name_6 );
            Py_DECREF( tmp_right_name_6 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_10 = tmp_left_name_6;
            var_starttag = tmp_assign_source_10;

        }
        branch_no_12:;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_left_name_9;
        PyObject *tmp_right_name_9;
        if ( var_starttag == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 116;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_9 = var_starttag;
        tmp_right_name_9 = const_str_chr_62;
        tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_9, tmp_right_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = tmp_left_name_9;
        var_starttag = tmp_assign_source_11;

    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_68d17afc28a356befa5597ef06df715b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_68d17afc28a356befa5597ef06df715b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_68d17afc28a356befa5597ef06df715b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_68d17afc28a356befa5597ef06df715b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_68d17afc28a356befa5597ef06df715b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_68d17afc28a356befa5597ef06df715b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_68d17afc28a356befa5597ef06df715b,
        type_description_1,
        par_fg,
        par_bg,
        par_bold,
        par_underline,
        par_inverse,
        var_classes,
        var_styles,
        var_starttag
    );


    // Release cached frame.
    if ( frame_68d17afc28a356befa5597ef06df715b == cache_frame_68d17afc28a356befa5597ef06df715b )
    {
        Py_DECREF( frame_68d17afc28a356befa5597ef06df715b );
    }
    cache_frame_68d17afc28a356befa5597ef06df715b = NULL;

    assertFrameObject( frame_68d17afc28a356befa5597ef06df715b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_tuple_element_3;
        CHECK_OBJECT( var_starttag );
        tmp_tuple_element_3 = var_starttag;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_3 );
        tmp_tuple_element_3 = const_str_digest_dab519039f2cf6f9b13dec763c93ec88;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_3 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_4__htmlconverter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_fg );
    par_fg = NULL;

    Py_XDECREF( par_bg );
    par_bg = NULL;

    CHECK_OBJECT( (PyObject *)par_bold );
    Py_DECREF( par_bold );
    par_bold = NULL;

    CHECK_OBJECT( (PyObject *)par_underline );
    Py_DECREF( par_underline );
    par_underline = NULL;

    CHECK_OBJECT( (PyObject *)par_inverse );
    Py_DECREF( par_inverse );
    par_inverse = NULL;

    Py_XDECREF( var_classes );
    var_classes = NULL;

    Py_XDECREF( var_styles );
    var_styles = NULL;

    Py_XDECREF( var_starttag );
    var_starttag = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_fg );
    par_fg = NULL;

    Py_XDECREF( par_bg );
    par_bg = NULL;

    CHECK_OBJECT( (PyObject *)par_bold );
    Py_DECREF( par_bold );
    par_bold = NULL;

    CHECK_OBJECT( (PyObject *)par_underline );
    Py_DECREF( par_underline );
    par_underline = NULL;

    CHECK_OBJECT( (PyObject *)par_inverse );
    Py_DECREF( par_inverse );
    par_inverse = NULL;

    Py_XDECREF( var_classes );
    var_classes = NULL;

    Py_XDECREF( var_styles );
    var_styles = NULL;

    Py_XDECREF( var_starttag );
    var_starttag = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_4__htmlconverter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$ansi$$$function_5__latexconverter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fg = python_pars[ 0 ];
    PyObject *par_bg = python_pars[ 1 ];
    PyObject *par_bold = python_pars[ 2 ];
    PyObject *par_underline = python_pars[ 3 ];
    PyObject *par_inverse = python_pars[ 4 ];
    PyObject *var_starttag = NULL;
    PyObject *var_endtag = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_968be856d81cfb420acbf0a59e382a6d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_968be856d81cfb420acbf0a59e382a6d = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_968be856d81cfb420acbf0a59e382a6d, codeobj_968be856d81cfb420acbf0a59e382a6d, module_nbconvert$filters$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_968be856d81cfb420acbf0a59e382a6d = cache_frame_968be856d81cfb420acbf0a59e382a6d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_968be856d81cfb420acbf0a59e382a6d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_968be856d81cfb420acbf0a59e382a6d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_fg );
        tmp_tuple_element_1 = par_fg;
        tmp_compexpr_left_1 = PyTuple_New( 5 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_bg );
        tmp_tuple_element_1 = par_bg;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_bold );
        tmp_tuple_element_1 = par_bold;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_underline );
        tmp_tuple_element_1 = par_underline;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_inverse );
        tmp_tuple_element_1 = par_inverse;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_compexpr_left_1, 4, tmp_tuple_element_1 );
        tmp_compexpr_right_1 = const_tuple_none_none_false_false_false_tuple;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_tuple_str_empty_str_empty_tuple;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_tuple_str_empty_str_empty_tuple;
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_1 == NULL) );
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 128;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooo";
            exception_lineno = 128;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_starttag == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_starttag = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_endtag == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_endtag = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_inverse );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_inverse );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_2;
            PyObject *tmp_tuple_element_2;
            CHECK_OBJECT( par_bg );
            tmp_tuple_element_2 = par_bg;
            tmp_iter_arg_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_iter_arg_2, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( par_fg );
            tmp_tuple_element_2 = par_fg;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_iter_arg_2, 1, tmp_tuple_element_2 );
            tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
            Py_DECREF( tmp_iter_arg_2 );
            assert( !(tmp_assign_source_6 == NULL) );
            assert( tmp_tuple_unpack_2__source_iter == NULL );
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_6;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooo";
                exception_lineno = 131;
                goto try_except_handler_5;
            }
            assert( tmp_tuple_unpack_2__element_1 == NULL );
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_7;
        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooo";
                exception_lineno = 131;
                goto try_except_handler_5;
            }
            assert( tmp_tuple_unpack_2__element_2 == NULL );
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_8;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_9 = tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = par_fg;
                assert( old != NULL );
                par_fg = tmp_assign_source_9;
                Py_INCREF( par_fg );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_10 = tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = par_bg;
                assert( old != NULL );
                par_bg = tmp_assign_source_10;
                Py_INCREF( par_bg );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_fg );
        tmp_isinstance_inst_1 = par_fg;
        tmp_isinstance_cls_1 = (PyObject *)&PyLong_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_right_name_3;
            CHECK_OBJECT( var_starttag );
            tmp_left_name_1 = var_starttag;
            tmp_left_name_3 = const_str_digest_0dabe95c13acefb112e7541387c7b612;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ANSI_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_fg );
            tmp_subscript_name_1 = par_fg;
            tmp_right_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_2 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_3, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_3 = const_str_digest_dfa7d79eeee95e06a7ce71756f59428f;
            tmp_right_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_11 = tmp_left_name_1;
            var_starttag = tmp_assign_source_11;

        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            tmp_left_name_4 = const_str_chr_125;
            CHECK_OBJECT( var_endtag );
            tmp_right_name_4 = var_endtag;
            tmp_assign_source_12 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_4, tmp_right_name_4 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_endtag;
                assert( old != NULL );
                var_endtag = tmp_assign_source_12;
                Py_DECREF( old );
            }

        }
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            int tmp_truth_name_2;
            CHECK_OBJECT( par_fg );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_fg );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                CHECK_OBJECT( var_starttag );
                tmp_left_name_5 = var_starttag;
                tmp_right_name_5 = const_str_digest_0565824281459b13c58c134fdef74c54;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_5, tmp_right_name_5 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_13 = tmp_left_name_5;
                var_starttag = tmp_assign_source_13;

            }
            {
                PyObject *tmp_assign_source_14;
                PyObject *tmp_left_name_6;
                PyObject *tmp_right_name_6;
                PyObject *tmp_left_name_7;
                PyObject *tmp_right_name_7;
                CHECK_OBJECT( var_starttag );
                tmp_left_name_6 = var_starttag;
                tmp_left_name_7 = const_str_digest_fdc9678f371cf698e2fa760aab40d69f;
                CHECK_OBJECT( par_fg );
                tmp_right_name_7 = par_fg;
                tmp_right_name_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                if ( tmp_right_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 139;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_6, tmp_right_name_6 );
                Py_DECREF( tmp_right_name_6 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 139;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_14 = tmp_left_name_6;
                var_starttag = tmp_assign_source_14;

            }
            {
                PyObject *tmp_assign_source_15;
                PyObject *tmp_left_name_8;
                PyObject *tmp_right_name_8;
                tmp_left_name_8 = const_str_chr_125;
                CHECK_OBJECT( var_endtag );
                tmp_right_name_8 = var_endtag;
                tmp_assign_source_15 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_8, tmp_right_name_8 );
                if ( tmp_assign_source_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_endtag;
                    assert( old != NULL );
                    var_endtag = tmp_assign_source_15;
                    Py_DECREF( old );
                }

            }
            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                int tmp_truth_name_3;
                CHECK_OBJECT( par_inverse );
                tmp_truth_name_3 = CHECK_IF_TRUE( par_inverse );
                if ( tmp_truth_name_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 141;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_16;
                    PyObject *tmp_left_name_9;
                    PyObject *tmp_right_name_9;
                    CHECK_OBJECT( var_starttag );
                    tmp_left_name_9 = var_starttag;
                    tmp_right_name_9 = const_str_digest_58a264413831ca8cd55685774282d190;
                    tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_9, tmp_right_name_9 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 142;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_16 = tmp_left_name_9;
                    var_starttag = tmp_assign_source_16;

                }
                {
                    PyObject *tmp_assign_source_17;
                    PyObject *tmp_left_name_10;
                    PyObject *tmp_right_name_10;
                    tmp_left_name_10 = const_str_chr_125;
                    CHECK_OBJECT( var_endtag );
                    tmp_right_name_10 = var_endtag;
                    tmp_assign_source_17 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_10, tmp_right_name_10 );
                    if ( tmp_assign_source_17 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 143;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_endtag;
                        assert( old != NULL );
                        var_endtag = tmp_assign_source_17;
                        Py_DECREF( old );
                    }

                }
                branch_no_5:;
            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_isinstance_inst_2;
        PyObject *tmp_isinstance_cls_2;
        CHECK_OBJECT( par_bg );
        tmp_isinstance_inst_2 = par_bg;
        tmp_isinstance_cls_2 = (PyObject *)&PyLong_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_left_name_11;
            PyObject *tmp_right_name_11;
            if ( var_starttag == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 146;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_left_name_11 = var_starttag;
            tmp_right_name_11 = const_str_digest_2bcd36a837a6bcd3dc5707391e5d72eb;
            tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_11, tmp_right_name_11 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 146;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_18 = tmp_left_name_11;
            var_starttag = tmp_assign_source_18;

        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_left_name_12;
            PyObject *tmp_right_name_12;
            PyObject *tmp_left_name_13;
            PyObject *tmp_left_name_14;
            PyObject *tmp_right_name_13;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_right_name_14;
            CHECK_OBJECT( var_starttag );
            tmp_left_name_12 = var_starttag;
            tmp_left_name_14 = const_str_digest_5decc50f6b60a72cfc7a91db021a7536;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ANSI_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 147;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( par_bg );
            tmp_subscript_name_2 = par_bg;
            tmp_right_name_13 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_right_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_13 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_14, tmp_right_name_13 );
            Py_DECREF( tmp_right_name_13 );
            if ( tmp_left_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_14 = const_str_digest_dfa7d79eeee95e06a7ce71756f59428f;
            tmp_right_name_12 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_13, tmp_right_name_14 );
            Py_DECREF( tmp_left_name_13 );
            if ( tmp_right_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_12, tmp_right_name_12 );
            Py_DECREF( tmp_right_name_12 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_19 = tmp_left_name_12;
            var_starttag = tmp_assign_source_19;

        }
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_left_name_15;
            PyObject *tmp_right_name_15;
            tmp_left_name_15 = const_str_digest_ace9eab81453cf75424fea0f41abbdc3;
            if ( var_endtag == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "endtag" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 148;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_15 = var_endtag;
            tmp_assign_source_20 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_15, tmp_right_name_15 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_endtag;
                var_endtag = tmp_assign_source_20;
                Py_XDECREF( old );
            }

        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_7;
            int tmp_truth_name_4;
            CHECK_OBJECT( par_bg );
            tmp_truth_name_4 = CHECK_IF_TRUE( par_bg );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_assign_source_21;
                PyObject *tmp_left_name_16;
                PyObject *tmp_right_name_16;
                if ( var_starttag == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 150;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_16 = var_starttag;
                tmp_right_name_16 = const_str_digest_2bcd36a837a6bcd3dc5707391e5d72eb;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_16, tmp_right_name_16 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 150;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_21 = tmp_left_name_16;
                var_starttag = tmp_assign_source_21;

            }
            {
                PyObject *tmp_assign_source_22;
                PyObject *tmp_left_name_17;
                PyObject *tmp_right_name_17;
                CHECK_OBJECT( var_starttag );
                tmp_left_name_17 = var_starttag;
                tmp_right_name_17 = const_str_digest_f675ffcaef98887e5fa932e5d8c01d01;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_17, tmp_right_name_17 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 152;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_22 = tmp_left_name_17;
                var_starttag = tmp_assign_source_22;

            }
            {
                PyObject *tmp_assign_source_23;
                PyObject *tmp_left_name_18;
                PyObject *tmp_right_name_18;
                PyObject *tmp_left_name_19;
                PyObject *tmp_right_name_19;
                CHECK_OBJECT( var_starttag );
                tmp_left_name_18 = var_starttag;
                tmp_left_name_19 = const_str_digest_4b175065a725220958033ad5a6ae95a0;
                CHECK_OBJECT( par_bg );
                tmp_right_name_19 = par_bg;
                tmp_right_name_18 = BINARY_OPERATION_REMAINDER( tmp_left_name_19, tmp_right_name_19 );
                if ( tmp_right_name_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_18, tmp_right_name_18 );
                Py_DECREF( tmp_right_name_18 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_23 = tmp_left_name_18;
                var_starttag = tmp_assign_source_23;

            }
            {
                PyObject *tmp_assign_source_24;
                PyObject *tmp_left_name_20;
                PyObject *tmp_right_name_20;
                tmp_left_name_20 = const_str_digest_ace9eab81453cf75424fea0f41abbdc3;
                if ( var_endtag == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "endtag" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 154;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_20 = var_endtag;
                tmp_assign_source_24 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_20, tmp_right_name_20 );
                if ( tmp_assign_source_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_endtag;
                    var_endtag = tmp_assign_source_24;
                    Py_XDECREF( old );
                }

            }
            goto branch_end_7;
            branch_no_7:;
            {
                nuitka_bool tmp_condition_result_8;
                int tmp_truth_name_5;
                CHECK_OBJECT( par_inverse );
                tmp_truth_name_5 = CHECK_IF_TRUE( par_inverse );
                if ( tmp_truth_name_5 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 155;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_8 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_assign_source_25;
                    PyObject *tmp_left_name_21;
                    PyObject *tmp_right_name_21;
                    if ( var_starttag == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 156;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_left_name_21 = var_starttag;
                    tmp_right_name_21 = const_str_digest_2bcd36a837a6bcd3dc5707391e5d72eb;
                    tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_21, tmp_right_name_21 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 156;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_25 = tmp_left_name_21;
                    var_starttag = tmp_assign_source_25;

                }
                {
                    PyObject *tmp_assign_source_26;
                    PyObject *tmp_left_name_22;
                    PyObject *tmp_right_name_22;
                    CHECK_OBJECT( var_starttag );
                    tmp_left_name_22 = var_starttag;
                    tmp_right_name_22 = const_str_digest_ee5629dc23fa4d27f356b50b0aaf4fa4;
                    tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_22, tmp_right_name_22 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 157;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_26 = tmp_left_name_22;
                    var_starttag = tmp_assign_source_26;

                }
                {
                    PyObject *tmp_assign_source_27;
                    PyObject *tmp_left_name_23;
                    PyObject *tmp_right_name_23;
                    tmp_left_name_23 = const_str_digest_ace9eab81453cf75424fea0f41abbdc3;
                    if ( var_endtag == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "endtag" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 158;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_right_name_23 = var_endtag;
                    tmp_assign_source_27 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_23, tmp_right_name_23 );
                    if ( tmp_assign_source_27 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 158;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_endtag;
                        var_endtag = tmp_assign_source_27;
                        Py_XDECREF( old );
                    }

                }
                branch_no_8:;
            }
            branch_end_7:;
        }
        branch_end_6:;
    }
    {
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_6;
        CHECK_OBJECT( par_bold );
        tmp_truth_name_6 = CHECK_IF_TRUE( par_bold );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_left_name_24;
            PyObject *tmp_right_name_24;
            if ( var_starttag == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 161;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_left_name_24 = var_starttag;
            tmp_right_name_24 = const_str_digest_eb47a2a30205b2518e68f374a96456d9;
            tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_24, tmp_right_name_24 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_28 = tmp_left_name_24;
            var_starttag = tmp_assign_source_28;

        }
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_left_name_25;
            PyObject *tmp_right_name_25;
            tmp_left_name_25 = const_str_chr_125;
            if ( var_endtag == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "endtag" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 162;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_25 = var_endtag;
            tmp_assign_source_29 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_25, tmp_right_name_25 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_endtag;
                var_endtag = tmp_assign_source_29;
                Py_XDECREF( old );
            }

        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        int tmp_truth_name_7;
        CHECK_OBJECT( par_underline );
        tmp_truth_name_7 = CHECK_IF_TRUE( par_underline );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_10 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_left_name_26;
            PyObject *tmp_right_name_26;
            if ( var_starttag == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 165;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_left_name_26 = var_starttag;
            tmp_right_name_26 = const_str_digest_f3f58f6c13bc1ed32570513d30f6fa49;
            tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_26, tmp_right_name_26 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 165;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_30 = tmp_left_name_26;
            var_starttag = tmp_assign_source_30;

        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_left_name_27;
            PyObject *tmp_right_name_27;
            tmp_left_name_27 = const_str_chr_125;
            if ( var_endtag == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "endtag" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 166;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_right_name_27 = var_endtag;
            tmp_assign_source_31 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_27, tmp_right_name_27 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_endtag;
                var_endtag = tmp_assign_source_31;
                Py_XDECREF( old );
            }

        }
        branch_no_10:;
    }
    {
        PyObject *tmp_tuple_element_3;
        if ( var_starttag == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "starttag" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_3 = var_starttag;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_3 );
        if ( var_endtag == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "endtag" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_3 = var_endtag;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_3 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_968be856d81cfb420acbf0a59e382a6d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_968be856d81cfb420acbf0a59e382a6d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_968be856d81cfb420acbf0a59e382a6d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_968be856d81cfb420acbf0a59e382a6d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_968be856d81cfb420acbf0a59e382a6d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_968be856d81cfb420acbf0a59e382a6d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_968be856d81cfb420acbf0a59e382a6d,
        type_description_1,
        par_fg,
        par_bg,
        par_bold,
        par_underline,
        par_inverse,
        var_starttag,
        var_endtag
    );


    // Release cached frame.
    if ( frame_968be856d81cfb420acbf0a59e382a6d == cache_frame_968be856d81cfb420acbf0a59e382a6d )
    {
        Py_DECREF( frame_968be856d81cfb420acbf0a59e382a6d );
    }
    cache_frame_968be856d81cfb420acbf0a59e382a6d = NULL;

    assertFrameObject( frame_968be856d81cfb420acbf0a59e382a6d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_5__latexconverter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_fg );
    par_fg = NULL;

    Py_XDECREF( par_bg );
    par_bg = NULL;

    CHECK_OBJECT( (PyObject *)par_bold );
    Py_DECREF( par_bold );
    par_bold = NULL;

    CHECK_OBJECT( (PyObject *)par_underline );
    Py_DECREF( par_underline );
    par_underline = NULL;

    CHECK_OBJECT( (PyObject *)par_inverse );
    Py_DECREF( par_inverse );
    par_inverse = NULL;

    Py_XDECREF( var_starttag );
    var_starttag = NULL;

    Py_XDECREF( var_endtag );
    var_endtag = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_fg );
    par_fg = NULL;

    Py_XDECREF( par_bg );
    par_bg = NULL;

    CHECK_OBJECT( (PyObject *)par_bold );
    Py_DECREF( par_bold );
    par_bold = NULL;

    CHECK_OBJECT( (PyObject *)par_underline );
    Py_DECREF( par_underline );
    par_underline = NULL;

    CHECK_OBJECT( (PyObject *)par_inverse );
    Py_DECREF( par_inverse );
    par_inverse = NULL;

    Py_XDECREF( var_starttag );
    var_starttag = NULL;

    Py_XDECREF( var_endtag );
    var_endtag = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_5__latexconverter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$ansi$$$function_6__ansi2anything( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *par_converter = python_pars[ 1 ];
    PyObject *var_fg = NULL;
    PyObject *var_bg = NULL;
    PyObject *var_bold = NULL;
    PyObject *var_underline = NULL;
    PyObject *var_inverse = NULL;
    PyObject *var_numbers = NULL;
    PyObject *var_out = NULL;
    PyObject *var_m = NULL;
    PyObject *var_chunk = NULL;
    PyObject *var_starttag = NULL;
    PyObject *var_endtag = NULL;
    PyObject *var_n = NULL;
    PyObject *outline_0_var_n = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    PyObject *tmp_assign_unpack_2__assign_source = NULL;
    PyObject *tmp_comparison_chain_1__comparison_result = NULL;
    PyObject *tmp_comparison_chain_1__operand_2 = NULL;
    PyObject *tmp_comparison_chain_2__comparison_result = NULL;
    PyObject *tmp_comparison_chain_2__operand_2 = NULL;
    PyObject *tmp_comparison_chain_3__comparison_result = NULL;
    PyObject *tmp_comparison_chain_3__operand_2 = NULL;
    PyObject *tmp_comparison_chain_4__comparison_result = NULL;
    PyObject *tmp_comparison_chain_4__operand_2 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    struct Nuitka_FrameObject *frame_751ac65bcf26755606db7ed0c99281e9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    struct Nuitka_FrameObject *frame_ffac657803e54ac28ddd892a14318711_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_ffac657803e54ac28ddd892a14318711_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_751ac65bcf26755606db7ed0c99281e9 = NULL;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        tmp_iter_arg_1 = const_tuple_none_none_tuple;
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        assert( !(tmp_assign_source_1 == NULL) );
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_751ac65bcf26755606db7ed0c99281e9, codeobj_751ac65bcf26755606db7ed0c99281e9, module_nbconvert$filters$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_751ac65bcf26755606db7ed0c99281e9 = cache_frame_751ac65bcf26755606db7ed0c99281e9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_751ac65bcf26755606db7ed0c99281e9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_751ac65bcf26755606db7ed0c99281e9 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 185;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooo";
            exception_lineno = 185;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_fg == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_fg = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_bg == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_bg = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = Py_False;
        assert( var_bold == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_bold = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = Py_False;
        assert( var_underline == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_underline = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = Py_False;
        assert( var_inverse == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_inverse = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = PyList_New( 0 );
        assert( var_numbers == NULL );
        var_numbers = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = PyList_New( 0 );
        assert( var_out == NULL );
        var_out = tmp_assign_source_10;
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_text );
        tmp_operand_name_1 = par_text;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_RE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ANSI_RE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ANSI_RE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_text );
        tmp_args_element_name_1 = par_text;
        frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_search, call_args );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_m;
            var_m = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_m );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_m );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_m );
            tmp_called_instance_2 = var_m;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 195;
            tmp_compexpr_left_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "oooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_1 = const_str_plain_m;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "oooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            // Tried code:
            {
                PyObject *tmp_assign_source_12;
                // Tried code:
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_iter_arg_2;
                    PyObject *tmp_called_instance_3;
                    PyObject *tmp_called_instance_4;
                    CHECK_OBJECT( var_m );
                    tmp_called_instance_4 = var_m;
                    frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 199;
                    tmp_called_instance_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

                    if ( tmp_called_instance_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 199;
                        type_description_1 = "oooooooooooooo";
                        goto try_except_handler_5;
                    }
                    frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 199;
                    tmp_iter_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_59_tuple, 0 ) );

                    Py_DECREF( tmp_called_instance_3 );
                    if ( tmp_iter_arg_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 199;
                        type_description_1 = "oooooooooooooo";
                        goto try_except_handler_5;
                    }
                    tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_2 );
                    Py_DECREF( tmp_iter_arg_2 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 198;
                        type_description_1 = "oooooooooooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = tmp_listcomp_1__$0;
                        tmp_listcomp_1__$0 = tmp_assign_source_13;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_14;
                    tmp_assign_source_14 = PyList_New( 0 );
                    {
                        PyObject *old = tmp_listcomp_1__contraction;
                        tmp_listcomp_1__contraction = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                MAKE_OR_REUSE_FRAME( cache_frame_ffac657803e54ac28ddd892a14318711_2, codeobj_ffac657803e54ac28ddd892a14318711, module_nbconvert$filters$ansi, sizeof(void *) );
                frame_ffac657803e54ac28ddd892a14318711_2 = cache_frame_ffac657803e54ac28ddd892a14318711_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_ffac657803e54ac28ddd892a14318711_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_ffac657803e54ac28ddd892a14318711_2 ) == 2 ); // Frame stack

                // Framed code:
                // Tried code:
                loop_start_2:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_15;
                    CHECK_OBJECT( tmp_listcomp_1__$0 );
                    tmp_next_source_1 = tmp_listcomp_1__$0;
                    tmp_assign_source_15 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_15 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_2;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "o";
                            exception_lineno = 198;
                            goto try_except_handler_6;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_0;
                        tmp_listcomp_1__iter_value_0 = tmp_assign_source_15;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_16;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                    tmp_assign_source_16 = tmp_listcomp_1__iter_value_0;
                    {
                        PyObject *old = outline_0_var_n;
                        outline_0_var_n = tmp_assign_source_16;
                        Py_INCREF( outline_0_var_n );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_append_list_1;
                    PyObject *tmp_append_value_1;
                    nuitka_bool tmp_condition_result_4;
                    int tmp_truth_name_2;
                    PyObject *tmp_int_arg_1;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_append_list_1 = tmp_listcomp_1__contraction;
                    CHECK_OBJECT( outline_0_var_n );
                    tmp_truth_name_2 = CHECK_IF_TRUE( outline_0_var_n );
                    if ( tmp_truth_name_2 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 198;
                        type_description_2 = "o";
                        goto try_except_handler_6;
                    }
                    tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_1;
                    }
                    else
                    {
                        goto condexpr_false_1;
                    }
                    condexpr_true_1:;
                    CHECK_OBJECT( outline_0_var_n );
                    tmp_int_arg_1 = outline_0_var_n;
                    tmp_append_value_1 = PyNumber_Int( tmp_int_arg_1 );
                    if ( tmp_append_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 198;
                        type_description_2 = "o";
                        goto try_except_handler_6;
                    }
                    goto condexpr_end_1;
                    condexpr_false_1:;
                    tmp_append_value_1 = const_int_0;
                    Py_INCREF( tmp_append_value_1 );
                    condexpr_end_1:;
                    assert( PyList_Check( tmp_append_list_1 ) );
                    tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                    Py_DECREF( tmp_append_value_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 198;
                        type_description_2 = "o";
                        goto try_except_handler_6;
                    }
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_2 = "o";
                    goto try_except_handler_6;
                }
                goto loop_start_2;
                loop_end_2:;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_assign_source_12 = tmp_listcomp_1__contraction;
                Py_INCREF( tmp_assign_source_12 );
                goto try_return_handler_6;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                return NULL;
                // Return handler code:
                try_return_handler_6:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                goto frame_return_exit_2;
                // Exception handler code:
                try_except_handler_6:;
                exception_keeper_type_3 = exception_type;
                exception_keeper_value_3 = exception_value;
                exception_keeper_tb_3 = exception_tb;
                exception_keeper_lineno_3 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_3;
                exception_value = exception_keeper_value_3;
                exception_tb = exception_keeper_tb_3;
                exception_lineno = exception_keeper_lineno_3;

                goto frame_exception_exit_2;
                // End of try:

#if 0
                RESTORE_FRAME_EXCEPTION( frame_ffac657803e54ac28ddd892a14318711_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_return_exit_2:;
#if 0
                RESTORE_FRAME_EXCEPTION( frame_ffac657803e54ac28ddd892a14318711_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto try_return_handler_5;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_ffac657803e54ac28ddd892a14318711_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_ffac657803e54ac28ddd892a14318711_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_ffac657803e54ac28ddd892a14318711_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_ffac657803e54ac28ddd892a14318711_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_ffac657803e54ac28ddd892a14318711_2,
                    type_description_2,
                    outline_0_var_n
                );


                // Release cached frame.
                if ( frame_ffac657803e54ac28ddd892a14318711_2 == cache_frame_ffac657803e54ac28ddd892a14318711_2 )
                {
                    Py_DECREF( frame_ffac657803e54ac28ddd892a14318711_2 );
                }
                cache_frame_ffac657803e54ac28ddd892a14318711_2 = NULL;

                assertFrameObject( frame_ffac657803e54ac28ddd892a14318711_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_5;
                skip_nested_handling_1:;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                return NULL;
                // Return handler code:
                try_return_handler_5:;
                Py_XDECREF( outline_0_var_n );
                outline_0_var_n = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_5:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( outline_0_var_n );
                outline_0_var_n = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                return NULL;
                outline_exception_1:;
                exception_lineno = 198;
                goto try_except_handler_4;
                outline_result_1:;
                {
                    PyObject *old = var_numbers;
                    var_numbers = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            goto try_end_3;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_5 == NULL )
            {
                exception_keeper_tb_5 = MAKE_TRACEBACK( frame_751ac65bcf26755606db7ed0c99281e9, exception_keeper_lineno_5 );
            }
            else if ( exception_keeper_lineno_5 != 0 )
            {
                exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_751ac65bcf26755606db7ed0c99281e9, exception_keeper_lineno_5 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
            PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
            PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_2 = PyExc_ValueError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 200;
                    type_description_1 = "oooooooooooooo";
                    goto try_except_handler_7;
                }
                tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 200;
                    type_description_1 = "oooooooooooooo";
                    goto try_except_handler_7;
                }
                tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 196;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_751ac65bcf26755606db7ed0c99281e9->m_frame) frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_7;
                branch_no_4:;
            }
            goto try_end_4;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto frame_exception_exit_1;
            // End of try:
            try_end_4:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_3;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
            return NULL;
            // End of try:
            try_end_3:;
            branch_no_3:;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_start_name_1;
            PyObject *tmp_stop_name_1;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_step_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_start_name_2;
            PyObject *tmp_called_instance_6;
            PyObject *tmp_stop_name_2;
            PyObject *tmp_step_name_2;
            CHECK_OBJECT( par_text );
            tmp_subscribed_name_1 = par_text;
            tmp_start_name_1 = Py_None;
            CHECK_OBJECT( var_m );
            tmp_called_instance_5 = var_m;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 204;
            tmp_stop_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_start );
            if ( tmp_stop_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 204;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_step_name_1 = Py_None;
            tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
            Py_DECREF( tmp_stop_name_1 );
            assert( !(tmp_subscript_name_1 == NULL) );
            tmp_tuple_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscript_name_1 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 204;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_iter_arg_3 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_iter_arg_3, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( par_text );
            tmp_subscribed_name_2 = par_text;
            CHECK_OBJECT( var_m );
            tmp_called_instance_6 = var_m;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 204;
            tmp_start_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_end );
            if ( tmp_start_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_iter_arg_3 );

                exception_lineno = 204;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_stop_name_2 = Py_None;
            tmp_step_name_2 = Py_None;
            tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
            Py_DECREF( tmp_start_name_2 );
            assert( !(tmp_subscript_name_2 == NULL) );
            tmp_tuple_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_iter_arg_3 );

                exception_lineno = 204;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
            }
            PyTuple_SET_ITEM( tmp_iter_arg_3, 1, tmp_tuple_element_1 );
            tmp_assign_source_17 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 204;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__source_iter;
                tmp_tuple_unpack_2__source_iter = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
            if ( tmp_assign_source_18 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooo";
                exception_lineno = 204;
                goto try_except_handler_9;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_1;
                tmp_tuple_unpack_2__element_1 = tmp_assign_source_18;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
            if ( tmp_assign_source_19 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooo";
                exception_lineno = 204;
                goto try_except_handler_9;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_2;
                tmp_tuple_unpack_2__element_2 = tmp_assign_source_19;
                Py_XDECREF( old );
            }

        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        try_end_5:;
        goto try_end_6;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto frame_exception_exit_1;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_20;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_20 = tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = var_chunk;
                var_chunk = tmp_assign_source_20;
                Py_INCREF( var_chunk );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_21;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_21 = tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = par_text;
                assert( old != NULL );
                par_text = tmp_assign_source_21;
                Py_INCREF( par_text );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        goto branch_end_2;
        branch_no_2:;
        // Tried code:
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_iter_arg_4;
            PyObject *tmp_tuple_element_2;
            CHECK_OBJECT( par_text );
            tmp_tuple_element_2 = par_text;
            tmp_iter_arg_4 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_iter_arg_4, 0, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_empty;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_iter_arg_4, 1, tmp_tuple_element_2 );
            tmp_assign_source_22 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
            Py_DECREF( tmp_iter_arg_4 );
            assert( !(tmp_assign_source_22 == NULL) );
            {
                PyObject *old = tmp_tuple_unpack_3__source_iter;
                tmp_tuple_unpack_3__source_iter = tmp_assign_source_22;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_unpack_5;
            CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
            tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
            tmp_assign_source_23 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
            if ( tmp_assign_source_23 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooo";
                exception_lineno = 206;
                goto try_except_handler_11;
            }
            {
                PyObject *old = tmp_tuple_unpack_3__element_1;
                tmp_tuple_unpack_3__element_1 = tmp_assign_source_23;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_unpack_6;
            CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
            tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
            tmp_assign_source_24 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
            if ( tmp_assign_source_24 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooo";
                exception_lineno = 206;
                goto try_except_handler_11;
            }
            {
                PyObject *old = tmp_tuple_unpack_3__element_2;
                tmp_tuple_unpack_3__element_2 = tmp_assign_source_24;
                Py_XDECREF( old );
            }

        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
        Py_DECREF( tmp_tuple_unpack_3__source_iter );
        tmp_tuple_unpack_3__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_10;
        // End of try:
        try_end_7:;
        goto try_end_8;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_3__element_1 );
        tmp_tuple_unpack_3__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_3__element_2 );
        tmp_tuple_unpack_3__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto frame_exception_exit_1;
        // End of try:
        try_end_8:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
        Py_DECREF( tmp_tuple_unpack_3__source_iter );
        tmp_tuple_unpack_3__source_iter = NULL;

        {
            PyObject *tmp_assign_source_25;
            CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
            tmp_assign_source_25 = tmp_tuple_unpack_3__element_1;
            {
                PyObject *old = var_chunk;
                var_chunk = tmp_assign_source_25;
                Py_INCREF( var_chunk );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_3__element_1 );
        tmp_tuple_unpack_3__element_1 = NULL;

        {
            PyObject *tmp_assign_source_26;
            CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
            tmp_assign_source_26 = tmp_tuple_unpack_3__element_2;
            {
                PyObject *old = par_text;
                assert( old != NULL );
                par_text = tmp_assign_source_26;
                Py_INCREF( par_text );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_3__element_2 );
        tmp_tuple_unpack_3__element_2 = NULL;

        branch_end_2:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_chunk );
        tmp_truth_name_3 = CHECK_IF_TRUE( var_chunk );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        // Tried code:
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_iter_arg_5;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_2;
            nuitka_bool tmp_condition_result_7;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            int tmp_truth_name_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( par_converter );
            tmp_called_name_1 = par_converter;
            if ( var_bold == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "bold" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_truth_name_4 = CHECK_IF_TRUE( var_bold );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            tmp_and_left_value_1 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            if ( var_fg == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_compexpr_left_3 = var_fg;
            tmp_compexpr_right_3 = const_xrange_0_8;
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            tmp_and_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_7 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_7 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            if ( var_fg == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_left_name_1 = var_fg;
            tmp_right_name_1 = const_int_pos_8;
            tmp_args_element_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            goto condexpr_end_2;
            condexpr_false_2:;
            if ( var_fg == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 210;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_args_element_name_2 = var_fg;
            Py_INCREF( tmp_args_element_name_2 );
            condexpr_end_2:;
            if ( var_bg == NULL )
            {
                Py_DECREF( tmp_args_element_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "bg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_args_element_name_3 = var_bg;
            if ( var_bold == NULL )
            {
                Py_DECREF( tmp_args_element_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "bold" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_args_element_name_4 = var_bold;
            if ( var_underline == NULL )
            {
                Py_DECREF( tmp_args_element_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "underline" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_args_element_name_5 = var_underline;
            if ( var_inverse == NULL )
            {
                Py_DECREF( tmp_args_element_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "inverse" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_args_element_name_6 = var_inverse;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 209;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_iter_arg_5 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_iter_arg_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            tmp_assign_source_27 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_5 );
            Py_DECREF( tmp_iter_arg_5 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_1 = "oooooooooooooo";
                goto try_except_handler_12;
            }
            {
                PyObject *old = tmp_tuple_unpack_4__source_iter;
                tmp_tuple_unpack_4__source_iter = tmp_assign_source_27;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_unpack_7;
            CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
            tmp_unpack_7 = tmp_tuple_unpack_4__source_iter;
            tmp_assign_source_28 = UNPACK_NEXT( tmp_unpack_7, 0, 2 );
            if ( tmp_assign_source_28 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooo";
                exception_lineno = 209;
                goto try_except_handler_13;
            }
            {
                PyObject *old = tmp_tuple_unpack_4__element_1;
                tmp_tuple_unpack_4__element_1 = tmp_assign_source_28;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_unpack_8;
            CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
            tmp_unpack_8 = tmp_tuple_unpack_4__source_iter;
            tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_8, 1, 2 );
            if ( tmp_assign_source_29 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooo";
                exception_lineno = 209;
                goto try_except_handler_13;
            }
            {
                PyObject *old = tmp_tuple_unpack_4__element_2;
                tmp_tuple_unpack_4__element_2 = tmp_assign_source_29;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_1;
            CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
            tmp_iterator_name_1 = tmp_tuple_unpack_4__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooooooooo";
                        exception_lineno = 209;
                        goto try_except_handler_13;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooooooooo";
                exception_lineno = 209;
                goto try_except_handler_13;
            }
        }
        goto try_end_9;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
        Py_DECREF( tmp_tuple_unpack_4__source_iter );
        tmp_tuple_unpack_4__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto try_except_handler_12;
        // End of try:
        try_end_9:;
        goto try_end_10;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_4__element_1 );
        tmp_tuple_unpack_4__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_4__element_2 );
        tmp_tuple_unpack_4__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto frame_exception_exit_1;
        // End of try:
        try_end_10:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
        Py_DECREF( tmp_tuple_unpack_4__source_iter );
        tmp_tuple_unpack_4__source_iter = NULL;

        {
            PyObject *tmp_assign_source_30;
            CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
            tmp_assign_source_30 = tmp_tuple_unpack_4__element_1;
            {
                PyObject *old = var_starttag;
                var_starttag = tmp_assign_source_30;
                Py_INCREF( var_starttag );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_4__element_1 );
        tmp_tuple_unpack_4__element_1 = NULL;

        {
            PyObject *tmp_assign_source_31;
            CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
            tmp_assign_source_31 = tmp_tuple_unpack_4__element_2;
            {
                PyObject *old = var_endtag;
                var_endtag = tmp_assign_source_31;
                Py_INCREF( var_endtag );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_4__element_2 );
        tmp_tuple_unpack_4__element_2 = NULL;

        {
            PyObject *tmp_called_instance_7;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( var_out );
            tmp_called_instance_7 = var_out;
            CHECK_OBJECT( var_starttag );
            tmp_args_element_name_7 = var_starttag;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 212;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "oooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_instance_8;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_8;
            CHECK_OBJECT( var_out );
            tmp_called_instance_8 = var_out;
            CHECK_OBJECT( var_chunk );
            tmp_args_element_name_8 = var_chunk;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 213;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 213;
                type_description_1 = "oooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( var_out );
            tmp_called_instance_9 = var_out;
            CHECK_OBJECT( var_endtag );
            tmp_args_element_name_9 = var_endtag;
            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 214;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_1 = "oooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_5:;
    }
    loop_start_3:;
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_operand_name_3;
        if ( var_numbers == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numbers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_3 = var_numbers;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        goto loop_end_3;
        branch_no_6:;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_instance_10;
        if ( var_numbers == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numbers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 217;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_10 = var_numbers;
        frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 217;
        tmp_assign_source_32 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_n;
            var_n = tmp_assign_source_32;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        CHECK_OBJECT( var_n );
        tmp_compexpr_left_4 = var_n;
        tmp_compexpr_right_4 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_33;
            tmp_assign_source_33 = Py_None;
            {
                PyObject *old = tmp_assign_unpack_1__assign_source;
                tmp_assign_unpack_1__assign_source = tmp_assign_source_33;
                Py_INCREF( tmp_assign_unpack_1__assign_source );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_34;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_34 = tmp_assign_unpack_1__assign_source;
            {
                PyObject *old = var_fg;
                var_fg = tmp_assign_source_34;
                Py_INCREF( var_fg );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_35;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_35 = tmp_assign_unpack_1__assign_source;
            {
                PyObject *old = var_bg;
                var_bg = tmp_assign_source_35;
                Py_INCREF( var_bg );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
        Py_DECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = Py_False;
            {
                PyObject *old = tmp_assign_unpack_2__assign_source;
                tmp_assign_unpack_2__assign_source = tmp_assign_source_36;
                Py_INCREF( tmp_assign_unpack_2__assign_source );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_37;
            CHECK_OBJECT( tmp_assign_unpack_2__assign_source );
            tmp_assign_source_37 = tmp_assign_unpack_2__assign_source;
            {
                PyObject *old = var_bold;
                var_bold = tmp_assign_source_37;
                Py_INCREF( var_bold );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_38;
            CHECK_OBJECT( tmp_assign_unpack_2__assign_source );
            tmp_assign_source_38 = tmp_assign_unpack_2__assign_source;
            {
                PyObject *old = var_underline;
                var_underline = tmp_assign_source_38;
                Py_INCREF( var_underline );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_39;
            CHECK_OBJECT( tmp_assign_unpack_2__assign_source );
            tmp_assign_source_39 = tmp_assign_unpack_2__assign_source;
            {
                PyObject *old = var_inverse;
                var_inverse = tmp_assign_source_39;
                Py_INCREF( var_inverse );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_2__assign_source );
        Py_DECREF( tmp_assign_unpack_2__assign_source );
        tmp_assign_unpack_2__assign_source = NULL;

        goto branch_end_7;
        branch_no_7:;
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( var_n );
            tmp_compexpr_left_5 = var_n;
            tmp_compexpr_right_5 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_1 = "oooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_40;
                tmp_assign_source_40 = Py_True;
                {
                    PyObject *old = var_bold;
                    var_bold = tmp_assign_source_40;
                    Py_INCREF( var_bold );
                    Py_XDECREF( old );
                }

            }
            goto branch_end_8;
            branch_no_8:;
            {
                nuitka_bool tmp_condition_result_11;
                PyObject *tmp_compexpr_left_6;
                PyObject *tmp_compexpr_right_6;
                CHECK_OBJECT( var_n );
                tmp_compexpr_left_6 = var_n;
                tmp_compexpr_right_6 = const_int_pos_4;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 224;
                    type_description_1 = "oooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                {
                    PyObject *tmp_assign_source_41;
                    tmp_assign_source_41 = Py_True;
                    {
                        PyObject *old = var_underline;
                        var_underline = tmp_assign_source_41;
                        Py_INCREF( var_underline );
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_9;
                branch_no_9:;
                {
                    nuitka_bool tmp_condition_result_12;
                    PyObject *tmp_compexpr_left_7;
                    PyObject *tmp_compexpr_right_7;
                    CHECK_OBJECT( var_n );
                    tmp_compexpr_left_7 = var_n;
                    tmp_compexpr_right_7 = const_int_pos_5;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 226;
                        type_description_1 = "oooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_10;
                    }
                    else
                    {
                        goto branch_no_10;
                    }
                    branch_yes_10:;
                    {
                        PyObject *tmp_assign_source_42;
                        tmp_assign_source_42 = Py_True;
                        {
                            PyObject *old = var_bold;
                            var_bold = tmp_assign_source_42;
                            Py_INCREF( var_bold );
                            Py_XDECREF( old );
                        }

                    }
                    goto branch_end_10;
                    branch_no_10:;
                    {
                        nuitka_bool tmp_condition_result_13;
                        PyObject *tmp_compexpr_left_8;
                        PyObject *tmp_compexpr_right_8;
                        CHECK_OBJECT( var_n );
                        tmp_compexpr_left_8 = var_n;
                        tmp_compexpr_right_8 = const_int_pos_7;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 229;
                            type_description_1 = "oooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_11;
                        }
                        else
                        {
                            goto branch_no_11;
                        }
                        branch_yes_11:;
                        {
                            PyObject *tmp_assign_source_43;
                            tmp_assign_source_43 = Py_True;
                            {
                                PyObject *old = var_inverse;
                                var_inverse = tmp_assign_source_43;
                                Py_INCREF( var_inverse );
                                Py_XDECREF( old );
                            }

                        }
                        goto branch_end_11;
                        branch_no_11:;
                        {
                            nuitka_bool tmp_condition_result_14;
                            PyObject *tmp_compexpr_left_9;
                            PyObject *tmp_compexpr_right_9;
                            CHECK_OBJECT( var_n );
                            tmp_compexpr_left_9 = var_n;
                            tmp_compexpr_right_9 = const_tuple_int_pos_21_int_pos_22_tuple;
                            tmp_res = PySequence_Contains( tmp_compexpr_right_9, tmp_compexpr_left_9 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 231;
                                type_description_1 = "oooooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_14 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_12;
                            }
                            else
                            {
                                goto branch_no_12;
                            }
                            branch_yes_12:;
                            {
                                PyObject *tmp_assign_source_44;
                                tmp_assign_source_44 = Py_False;
                                {
                                    PyObject *old = var_bold;
                                    var_bold = tmp_assign_source_44;
                                    Py_INCREF( var_bold );
                                    Py_XDECREF( old );
                                }

                            }
                            goto branch_end_12;
                            branch_no_12:;
                            {
                                nuitka_bool tmp_condition_result_15;
                                PyObject *tmp_compexpr_left_10;
                                PyObject *tmp_compexpr_right_10;
                                CHECK_OBJECT( var_n );
                                tmp_compexpr_left_10 = var_n;
                                tmp_compexpr_right_10 = const_int_pos_24;
                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                                if ( tmp_res == -1 )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 233;
                                    type_description_1 = "oooooooooooooo";
                                    goto frame_exception_exit_1;
                                }
                                tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                                {
                                    goto branch_yes_13;
                                }
                                else
                                {
                                    goto branch_no_13;
                                }
                                branch_yes_13:;
                                {
                                    PyObject *tmp_assign_source_45;
                                    tmp_assign_source_45 = Py_False;
                                    {
                                        PyObject *old = var_underline;
                                        var_underline = tmp_assign_source_45;
                                        Py_INCREF( var_underline );
                                        Py_XDECREF( old );
                                    }

                                }
                                goto branch_end_13;
                                branch_no_13:;
                                {
                                    nuitka_bool tmp_condition_result_16;
                                    PyObject *tmp_compexpr_left_11;
                                    PyObject *tmp_compexpr_right_11;
                                    CHECK_OBJECT( var_n );
                                    tmp_compexpr_left_11 = var_n;
                                    tmp_compexpr_right_11 = const_int_pos_27;
                                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                                    if ( tmp_res == -1 )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 235;
                                        type_description_1 = "oooooooooooooo";
                                        goto frame_exception_exit_1;
                                    }
                                    tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                    if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                                    {
                                        goto branch_yes_14;
                                    }
                                    else
                                    {
                                        goto branch_no_14;
                                    }
                                    branch_yes_14:;
                                    {
                                        PyObject *tmp_assign_source_46;
                                        tmp_assign_source_46 = Py_False;
                                        {
                                            PyObject *old = var_inverse;
                                            var_inverse = tmp_assign_source_46;
                                            Py_INCREF( var_inverse );
                                            Py_XDECREF( old );
                                        }

                                    }
                                    goto branch_end_14;
                                    branch_no_14:;
                                    {
                                        nuitka_bool tmp_condition_result_17;
                                        PyObject *tmp_outline_return_value_1;
                                        int tmp_truth_name_5;
                                        {
                                            PyObject *tmp_assign_source_47;
                                            CHECK_OBJECT( var_n );
                                            tmp_assign_source_47 = var_n;
                                            {
                                                PyObject *old = tmp_comparison_chain_1__operand_2;
                                                tmp_comparison_chain_1__operand_2 = tmp_assign_source_47;
                                                Py_INCREF( tmp_comparison_chain_1__operand_2 );
                                                Py_XDECREF( old );
                                            }

                                        }
                                        // Tried code:
                                        {
                                            PyObject *tmp_assign_source_48;
                                            PyObject *tmp_compexpr_left_12;
                                            PyObject *tmp_compexpr_right_12;
                                            tmp_compexpr_left_12 = const_int_pos_30;
                                            CHECK_OBJECT( tmp_comparison_chain_1__operand_2 );
                                            tmp_compexpr_right_12 = tmp_comparison_chain_1__operand_2;
                                            tmp_assign_source_48 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                                            if ( tmp_assign_source_48 == NULL )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 237;
                                                type_description_1 = "oooooooooooooo";
                                                goto try_except_handler_14;
                                            }
                                            {
                                                PyObject *old = tmp_comparison_chain_1__comparison_result;
                                                tmp_comparison_chain_1__comparison_result = tmp_assign_source_48;
                                                Py_XDECREF( old );
                                            }

                                        }
                                        {
                                            nuitka_bool tmp_condition_result_18;
                                            PyObject *tmp_operand_name_4;
                                            CHECK_OBJECT( tmp_comparison_chain_1__comparison_result );
                                            tmp_operand_name_4 = tmp_comparison_chain_1__comparison_result;
                                            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
                                            if ( tmp_res == -1 )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 237;
                                                type_description_1 = "oooooooooooooo";
                                                goto try_except_handler_14;
                                            }
                                            tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
                                            {
                                                goto branch_yes_16;
                                            }
                                            else
                                            {
                                                goto branch_no_16;
                                            }
                                            branch_yes_16:;
                                            CHECK_OBJECT( tmp_comparison_chain_1__comparison_result );
                                            tmp_outline_return_value_1 = tmp_comparison_chain_1__comparison_result;
                                            Py_INCREF( tmp_outline_return_value_1 );
                                            goto try_return_handler_14;
                                            branch_no_16:;
                                        }
                                        {
                                            PyObject *tmp_compexpr_left_13;
                                            PyObject *tmp_compexpr_right_13;
                                            CHECK_OBJECT( tmp_comparison_chain_1__operand_2 );
                                            tmp_compexpr_left_13 = tmp_comparison_chain_1__operand_2;
                                            tmp_compexpr_right_13 = const_int_pos_37;
                                            tmp_outline_return_value_1 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
                                            if ( tmp_outline_return_value_1 == NULL )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 237;
                                                type_description_1 = "oooooooooooooo";
                                                goto try_except_handler_14;
                                            }
                                            goto try_return_handler_14;
                                        }
                                        // tried codes exits in all cases
                                        NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                        return NULL;
                                        // Return handler code:
                                        try_return_handler_14:;
                                        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__operand_2 );
                                        Py_DECREF( tmp_comparison_chain_1__operand_2 );
                                        tmp_comparison_chain_1__operand_2 = NULL;

                                        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__comparison_result );
                                        Py_DECREF( tmp_comparison_chain_1__comparison_result );
                                        tmp_comparison_chain_1__comparison_result = NULL;

                                        goto outline_result_2;
                                        // Exception handler code:
                                        try_except_handler_14:;
                                        exception_keeper_type_13 = exception_type;
                                        exception_keeper_value_13 = exception_value;
                                        exception_keeper_tb_13 = exception_tb;
                                        exception_keeper_lineno_13 = exception_lineno;
                                        exception_type = NULL;
                                        exception_value = NULL;
                                        exception_tb = NULL;
                                        exception_lineno = 0;

                                        CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__operand_2 );
                                        Py_DECREF( tmp_comparison_chain_1__operand_2 );
                                        tmp_comparison_chain_1__operand_2 = NULL;

                                        Py_XDECREF( tmp_comparison_chain_1__comparison_result );
                                        tmp_comparison_chain_1__comparison_result = NULL;

                                        // Re-raise.
                                        exception_type = exception_keeper_type_13;
                                        exception_value = exception_keeper_value_13;
                                        exception_tb = exception_keeper_tb_13;
                                        exception_lineno = exception_keeper_lineno_13;

                                        goto frame_exception_exit_1;
                                        // End of try:
                                        // Return statement must have exited already.
                                        NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                        return NULL;
                                        outline_result_2:;
                                        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_outline_return_value_1 );
                                        if ( tmp_truth_name_5 == -1 )
                                        {
                                            assert( ERROR_OCCURRED() );

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                            Py_DECREF( tmp_outline_return_value_1 );

                                            exception_lineno = 237;
                                            type_description_1 = "oooooooooooooo";
                                            goto frame_exception_exit_1;
                                        }
                                        tmp_condition_result_17 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                        Py_DECREF( tmp_outline_return_value_1 );
                                        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                                        {
                                            goto branch_yes_15;
                                        }
                                        else
                                        {
                                            goto branch_no_15;
                                        }
                                        branch_yes_15:;
                                        {
                                            PyObject *tmp_assign_source_49;
                                            PyObject *tmp_left_name_2;
                                            PyObject *tmp_right_name_2;
                                            CHECK_OBJECT( var_n );
                                            tmp_left_name_2 = var_n;
                                            tmp_right_name_2 = const_int_pos_30;
                                            tmp_assign_source_49 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
                                            if ( tmp_assign_source_49 == NULL )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 238;
                                                type_description_1 = "oooooooooooooo";
                                                goto frame_exception_exit_1;
                                            }
                                            {
                                                PyObject *old = var_fg;
                                                var_fg = tmp_assign_source_49;
                                                Py_XDECREF( old );
                                            }

                                        }
                                        goto branch_end_15;
                                        branch_no_15:;
                                        {
                                            nuitka_bool tmp_condition_result_19;
                                            PyObject *tmp_compexpr_left_14;
                                            PyObject *tmp_compexpr_right_14;
                                            CHECK_OBJECT( var_n );
                                            tmp_compexpr_left_14 = var_n;
                                            tmp_compexpr_right_14 = const_int_pos_38;
                                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
                                            if ( tmp_res == -1 )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 239;
                                                type_description_1 = "oooooooooooooo";
                                                goto frame_exception_exit_1;
                                            }
                                            tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
                                            {
                                                goto branch_yes_17;
                                            }
                                            else
                                            {
                                                goto branch_no_17;
                                            }
                                            branch_yes_17:;
                                            // Tried code:
                                            {
                                                PyObject *tmp_assign_source_50;
                                                PyObject *tmp_called_name_2;
                                                PyObject *tmp_mvar_value_2;
                                                PyObject *tmp_args_element_name_10;
                                                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__get_extended_color );

                                                if (unlikely( tmp_mvar_value_2 == NULL ))
                                                {
                                                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_extended_color );
                                                }

                                                if ( tmp_mvar_value_2 == NULL )
                                                {

                                                    exception_type = PyExc_NameError;
                                                    Py_INCREF( exception_type );
                                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_extended_color" );
                                                    exception_tb = NULL;
                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                    CHAIN_EXCEPTION( exception_value );

                                                    exception_lineno = 241;
                                                    type_description_1 = "oooooooooooooo";
                                                    goto try_except_handler_15;
                                                }

                                                tmp_called_name_2 = tmp_mvar_value_2;
                                                if ( var_numbers == NULL )
                                                {

                                                    exception_type = PyExc_UnboundLocalError;
                                                    Py_INCREF( exception_type );
                                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numbers" );
                                                    exception_tb = NULL;
                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                    CHAIN_EXCEPTION( exception_value );

                                                    exception_lineno = 241;
                                                    type_description_1 = "oooooooooooooo";
                                                    goto try_except_handler_15;
                                                }

                                                tmp_args_element_name_10 = var_numbers;
                                                frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 241;
                                                {
                                                    PyObject *call_args[] = { tmp_args_element_name_10 };
                                                    tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                                                }

                                                if ( tmp_assign_source_50 == NULL )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 241;
                                                    type_description_1 = "oooooooooooooo";
                                                    goto try_except_handler_15;
                                                }
                                                {
                                                    PyObject *old = var_fg;
                                                    var_fg = tmp_assign_source_50;
                                                    Py_XDECREF( old );
                                                }

                                            }
                                            goto try_end_11;
                                            // Exception handler code:
                                            try_except_handler_15:;
                                            exception_keeper_type_14 = exception_type;
                                            exception_keeper_value_14 = exception_value;
                                            exception_keeper_tb_14 = exception_tb;
                                            exception_keeper_lineno_14 = exception_lineno;
                                            exception_type = NULL;
                                            exception_value = NULL;
                                            exception_tb = NULL;
                                            exception_lineno = 0;

                                            // Preserve existing published exception.
                                            exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
                                            Py_XINCREF( exception_preserved_type_2 );
                                            exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
                                            Py_XINCREF( exception_preserved_value_2 );
                                            exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                                            Py_XINCREF( exception_preserved_tb_2 );

                                            if ( exception_keeper_tb_14 == NULL )
                                            {
                                                exception_keeper_tb_14 = MAKE_TRACEBACK( frame_751ac65bcf26755606db7ed0c99281e9, exception_keeper_lineno_14 );
                                            }
                                            else if ( exception_keeper_lineno_14 != 0 )
                                            {
                                                exception_keeper_tb_14 = ADD_TRACEBACK( exception_keeper_tb_14, frame_751ac65bcf26755606db7ed0c99281e9, exception_keeper_lineno_14 );
                                            }

                                            NORMALIZE_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
                                            PyException_SetTraceback( exception_keeper_value_14, (PyObject *)exception_keeper_tb_14 );
                                            PUBLISH_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
                                            // Tried code:
                                            {
                                                nuitka_bool tmp_condition_result_20;
                                                PyObject *tmp_compexpr_left_15;
                                                PyObject *tmp_compexpr_right_15;
                                                tmp_compexpr_left_15 = EXC_TYPE(PyThreadState_GET());
                                                tmp_compexpr_right_15 = PyExc_ValueError;
                                                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_15, tmp_compexpr_right_15 );
                                                if ( tmp_res == -1 )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 242;
                                                    type_description_1 = "oooooooooooooo";
                                                    goto try_except_handler_16;
                                                }
                                                tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                                                {
                                                    goto branch_yes_18;
                                                }
                                                else
                                                {
                                                    goto branch_no_18;
                                                }
                                                branch_yes_18:;
                                                {
                                                    PyObject *tmp_called_instance_11;
                                                    PyObject *tmp_call_result_4;
                                                    if ( var_numbers == NULL )
                                                    {

                                                        exception_type = PyExc_UnboundLocalError;
                                                        Py_INCREF( exception_type );
                                                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numbers" );
                                                        exception_tb = NULL;
                                                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                        CHAIN_EXCEPTION( exception_value );

                                                        exception_lineno = 243;
                                                        type_description_1 = "oooooooooooooo";
                                                        goto try_except_handler_16;
                                                    }

                                                    tmp_called_instance_11 = var_numbers;
                                                    frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 243;
                                                    tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_clear );
                                                    if ( tmp_call_result_4 == NULL )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                        exception_lineno = 243;
                                                        type_description_1 = "oooooooooooooo";
                                                        goto try_except_handler_16;
                                                    }
                                                    Py_DECREF( tmp_call_result_4 );
                                                }
                                                goto branch_end_18;
                                                branch_no_18:;
                                                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                if (unlikely( tmp_result == false ))
                                                {
                                                    exception_lineno = 240;
                                                }

                                                if (exception_tb && exception_tb->tb_frame == &frame_751ac65bcf26755606db7ed0c99281e9->m_frame) frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = exception_tb->tb_lineno;
                                                type_description_1 = "oooooooooooooo";
                                                goto try_except_handler_16;
                                                branch_end_18:;
                                            }
                                            goto try_end_12;
                                            // Exception handler code:
                                            try_except_handler_16:;
                                            exception_keeper_type_15 = exception_type;
                                            exception_keeper_value_15 = exception_value;
                                            exception_keeper_tb_15 = exception_tb;
                                            exception_keeper_lineno_15 = exception_lineno;
                                            exception_type = NULL;
                                            exception_value = NULL;
                                            exception_tb = NULL;
                                            exception_lineno = 0;

                                            // Restore previous exception.
                                            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
                                            // Re-raise.
                                            exception_type = exception_keeper_type_15;
                                            exception_value = exception_keeper_value_15;
                                            exception_tb = exception_keeper_tb_15;
                                            exception_lineno = exception_keeper_lineno_15;

                                            goto frame_exception_exit_1;
                                            // End of try:
                                            try_end_12:;
                                            // Restore previous exception.
                                            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
                                            goto try_end_11;
                                            // exception handler codes exits in all cases
                                            NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                            return NULL;
                                            // End of try:
                                            try_end_11:;
                                            goto branch_end_17;
                                            branch_no_17:;
                                            {
                                                nuitka_bool tmp_condition_result_21;
                                                PyObject *tmp_compexpr_left_16;
                                                PyObject *tmp_compexpr_right_16;
                                                CHECK_OBJECT( var_n );
                                                tmp_compexpr_left_16 = var_n;
                                                tmp_compexpr_right_16 = const_int_pos_39;
                                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_16, tmp_compexpr_right_16 );
                                                if ( tmp_res == -1 )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 244;
                                                    type_description_1 = "oooooooooooooo";
                                                    goto frame_exception_exit_1;
                                                }
                                                tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
                                                {
                                                    goto branch_yes_19;
                                                }
                                                else
                                                {
                                                    goto branch_no_19;
                                                }
                                                branch_yes_19:;
                                                {
                                                    PyObject *tmp_assign_source_51;
                                                    tmp_assign_source_51 = Py_None;
                                                    {
                                                        PyObject *old = var_fg;
                                                        var_fg = tmp_assign_source_51;
                                                        Py_INCREF( var_fg );
                                                        Py_XDECREF( old );
                                                    }

                                                }
                                                goto branch_end_19;
                                                branch_no_19:;
                                                {
                                                    nuitka_bool tmp_condition_result_22;
                                                    PyObject *tmp_outline_return_value_2;
                                                    int tmp_truth_name_6;
                                                    {
                                                        PyObject *tmp_assign_source_52;
                                                        CHECK_OBJECT( var_n );
                                                        tmp_assign_source_52 = var_n;
                                                        {
                                                            PyObject *old = tmp_comparison_chain_2__operand_2;
                                                            tmp_comparison_chain_2__operand_2 = tmp_assign_source_52;
                                                            Py_INCREF( tmp_comparison_chain_2__operand_2 );
                                                            Py_XDECREF( old );
                                                        }

                                                    }
                                                    // Tried code:
                                                    {
                                                        PyObject *tmp_assign_source_53;
                                                        PyObject *tmp_compexpr_left_17;
                                                        PyObject *tmp_compexpr_right_17;
                                                        tmp_compexpr_left_17 = const_int_pos_40;
                                                        CHECK_OBJECT( tmp_comparison_chain_2__operand_2 );
                                                        tmp_compexpr_right_17 = tmp_comparison_chain_2__operand_2;
                                                        tmp_assign_source_53 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_17, tmp_compexpr_right_17 );
                                                        if ( tmp_assign_source_53 == NULL )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 246;
                                                            type_description_1 = "oooooooooooooo";
                                                            goto try_except_handler_17;
                                                        }
                                                        {
                                                            PyObject *old = tmp_comparison_chain_2__comparison_result;
                                                            tmp_comparison_chain_2__comparison_result = tmp_assign_source_53;
                                                            Py_XDECREF( old );
                                                        }

                                                    }
                                                    {
                                                        nuitka_bool tmp_condition_result_23;
                                                        PyObject *tmp_operand_name_5;
                                                        CHECK_OBJECT( tmp_comparison_chain_2__comparison_result );
                                                        tmp_operand_name_5 = tmp_comparison_chain_2__comparison_result;
                                                        tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
                                                        if ( tmp_res == -1 )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 246;
                                                            type_description_1 = "oooooooooooooo";
                                                            goto try_except_handler_17;
                                                        }
                                                        tmp_condition_result_23 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                        if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
                                                        {
                                                            goto branch_yes_21;
                                                        }
                                                        else
                                                        {
                                                            goto branch_no_21;
                                                        }
                                                        branch_yes_21:;
                                                        CHECK_OBJECT( tmp_comparison_chain_2__comparison_result );
                                                        tmp_outline_return_value_2 = tmp_comparison_chain_2__comparison_result;
                                                        Py_INCREF( tmp_outline_return_value_2 );
                                                        goto try_return_handler_17;
                                                        branch_no_21:;
                                                    }
                                                    {
                                                        PyObject *tmp_compexpr_left_18;
                                                        PyObject *tmp_compexpr_right_18;
                                                        CHECK_OBJECT( tmp_comparison_chain_2__operand_2 );
                                                        tmp_compexpr_left_18 = tmp_comparison_chain_2__operand_2;
                                                        tmp_compexpr_right_18 = const_int_pos_47;
                                                        tmp_outline_return_value_2 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_18, tmp_compexpr_right_18 );
                                                        if ( tmp_outline_return_value_2 == NULL )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 246;
                                                            type_description_1 = "oooooooooooooo";
                                                            goto try_except_handler_17;
                                                        }
                                                        goto try_return_handler_17;
                                                    }
                                                    // tried codes exits in all cases
                                                    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                    return NULL;
                                                    // Return handler code:
                                                    try_return_handler_17:;
                                                    CHECK_OBJECT( (PyObject *)tmp_comparison_chain_2__operand_2 );
                                                    Py_DECREF( tmp_comparison_chain_2__operand_2 );
                                                    tmp_comparison_chain_2__operand_2 = NULL;

                                                    CHECK_OBJECT( (PyObject *)tmp_comparison_chain_2__comparison_result );
                                                    Py_DECREF( tmp_comparison_chain_2__comparison_result );
                                                    tmp_comparison_chain_2__comparison_result = NULL;

                                                    goto outline_result_3;
                                                    // Exception handler code:
                                                    try_except_handler_17:;
                                                    exception_keeper_type_16 = exception_type;
                                                    exception_keeper_value_16 = exception_value;
                                                    exception_keeper_tb_16 = exception_tb;
                                                    exception_keeper_lineno_16 = exception_lineno;
                                                    exception_type = NULL;
                                                    exception_value = NULL;
                                                    exception_tb = NULL;
                                                    exception_lineno = 0;

                                                    CHECK_OBJECT( (PyObject *)tmp_comparison_chain_2__operand_2 );
                                                    Py_DECREF( tmp_comparison_chain_2__operand_2 );
                                                    tmp_comparison_chain_2__operand_2 = NULL;

                                                    Py_XDECREF( tmp_comparison_chain_2__comparison_result );
                                                    tmp_comparison_chain_2__comparison_result = NULL;

                                                    // Re-raise.
                                                    exception_type = exception_keeper_type_16;
                                                    exception_value = exception_keeper_value_16;
                                                    exception_tb = exception_keeper_tb_16;
                                                    exception_lineno = exception_keeper_lineno_16;

                                                    goto frame_exception_exit_1;
                                                    // End of try:
                                                    // Return statement must have exited already.
                                                    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                    return NULL;
                                                    outline_result_3:;
                                                    tmp_truth_name_6 = CHECK_IF_TRUE( tmp_outline_return_value_2 );
                                                    if ( tmp_truth_name_6 == -1 )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                        Py_DECREF( tmp_outline_return_value_2 );

                                                        exception_lineno = 246;
                                                        type_description_1 = "oooooooooooooo";
                                                        goto frame_exception_exit_1;
                                                    }
                                                    tmp_condition_result_22 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                    Py_DECREF( tmp_outline_return_value_2 );
                                                    if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
                                                    {
                                                        goto branch_yes_20;
                                                    }
                                                    else
                                                    {
                                                        goto branch_no_20;
                                                    }
                                                    branch_yes_20:;
                                                    {
                                                        PyObject *tmp_assign_source_54;
                                                        PyObject *tmp_left_name_3;
                                                        PyObject *tmp_right_name_3;
                                                        CHECK_OBJECT( var_n );
                                                        tmp_left_name_3 = var_n;
                                                        tmp_right_name_3 = const_int_pos_40;
                                                        tmp_assign_source_54 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_3, tmp_right_name_3 );
                                                        if ( tmp_assign_source_54 == NULL )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 247;
                                                            type_description_1 = "oooooooooooooo";
                                                            goto frame_exception_exit_1;
                                                        }
                                                        {
                                                            PyObject *old = var_bg;
                                                            var_bg = tmp_assign_source_54;
                                                            Py_XDECREF( old );
                                                        }

                                                    }
                                                    goto branch_end_20;
                                                    branch_no_20:;
                                                    {
                                                        nuitka_bool tmp_condition_result_24;
                                                        PyObject *tmp_compexpr_left_19;
                                                        PyObject *tmp_compexpr_right_19;
                                                        CHECK_OBJECT( var_n );
                                                        tmp_compexpr_left_19 = var_n;
                                                        tmp_compexpr_right_19 = const_int_pos_48;
                                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_19, tmp_compexpr_right_19 );
                                                        if ( tmp_res == -1 )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 248;
                                                            type_description_1 = "oooooooooooooo";
                                                            goto frame_exception_exit_1;
                                                        }
                                                        tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                        if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
                                                        {
                                                            goto branch_yes_22;
                                                        }
                                                        else
                                                        {
                                                            goto branch_no_22;
                                                        }
                                                        branch_yes_22:;
                                                        // Tried code:
                                                        {
                                                            PyObject *tmp_assign_source_55;
                                                            PyObject *tmp_called_name_3;
                                                            PyObject *tmp_mvar_value_3;
                                                            PyObject *tmp_args_element_name_11;
                                                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__get_extended_color );

                                                            if (unlikely( tmp_mvar_value_3 == NULL ))
                                                            {
                                                                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_extended_color );
                                                            }

                                                            if ( tmp_mvar_value_3 == NULL )
                                                            {

                                                                exception_type = PyExc_NameError;
                                                                Py_INCREF( exception_type );
                                                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_extended_color" );
                                                                exception_tb = NULL;
                                                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                CHAIN_EXCEPTION( exception_value );

                                                                exception_lineno = 250;
                                                                type_description_1 = "oooooooooooooo";
                                                                goto try_except_handler_18;
                                                            }

                                                            tmp_called_name_3 = tmp_mvar_value_3;
                                                            if ( var_numbers == NULL )
                                                            {

                                                                exception_type = PyExc_UnboundLocalError;
                                                                Py_INCREF( exception_type );
                                                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numbers" );
                                                                exception_tb = NULL;
                                                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                CHAIN_EXCEPTION( exception_value );

                                                                exception_lineno = 250;
                                                                type_description_1 = "oooooooooooooo";
                                                                goto try_except_handler_18;
                                                            }

                                                            tmp_args_element_name_11 = var_numbers;
                                                            frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 250;
                                                            {
                                                                PyObject *call_args[] = { tmp_args_element_name_11 };
                                                                tmp_assign_source_55 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                                                            }

                                                            if ( tmp_assign_source_55 == NULL )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 250;
                                                                type_description_1 = "oooooooooooooo";
                                                                goto try_except_handler_18;
                                                            }
                                                            {
                                                                PyObject *old = var_bg;
                                                                var_bg = tmp_assign_source_55;
                                                                Py_XDECREF( old );
                                                            }

                                                        }
                                                        goto try_end_13;
                                                        // Exception handler code:
                                                        try_except_handler_18:;
                                                        exception_keeper_type_17 = exception_type;
                                                        exception_keeper_value_17 = exception_value;
                                                        exception_keeper_tb_17 = exception_tb;
                                                        exception_keeper_lineno_17 = exception_lineno;
                                                        exception_type = NULL;
                                                        exception_value = NULL;
                                                        exception_tb = NULL;
                                                        exception_lineno = 0;

                                                        // Preserve existing published exception.
                                                        exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
                                                        Py_XINCREF( exception_preserved_type_3 );
                                                        exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
                                                        Py_XINCREF( exception_preserved_value_3 );
                                                        exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                                                        Py_XINCREF( exception_preserved_tb_3 );

                                                        if ( exception_keeper_tb_17 == NULL )
                                                        {
                                                            exception_keeper_tb_17 = MAKE_TRACEBACK( frame_751ac65bcf26755606db7ed0c99281e9, exception_keeper_lineno_17 );
                                                        }
                                                        else if ( exception_keeper_lineno_17 != 0 )
                                                        {
                                                            exception_keeper_tb_17 = ADD_TRACEBACK( exception_keeper_tb_17, frame_751ac65bcf26755606db7ed0c99281e9, exception_keeper_lineno_17 );
                                                        }

                                                        NORMALIZE_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
                                                        PyException_SetTraceback( exception_keeper_value_17, (PyObject *)exception_keeper_tb_17 );
                                                        PUBLISH_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
                                                        // Tried code:
                                                        {
                                                            nuitka_bool tmp_condition_result_25;
                                                            PyObject *tmp_compexpr_left_20;
                                                            PyObject *tmp_compexpr_right_20;
                                                            tmp_compexpr_left_20 = EXC_TYPE(PyThreadState_GET());
                                                            tmp_compexpr_right_20 = PyExc_ValueError;
                                                            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_20, tmp_compexpr_right_20 );
                                                            if ( tmp_res == -1 )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 251;
                                                                type_description_1 = "oooooooooooooo";
                                                                goto try_except_handler_19;
                                                            }
                                                            tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
                                                            {
                                                                goto branch_yes_23;
                                                            }
                                                            else
                                                            {
                                                                goto branch_no_23;
                                                            }
                                                            branch_yes_23:;
                                                            {
                                                                PyObject *tmp_called_instance_12;
                                                                PyObject *tmp_call_result_5;
                                                                if ( var_numbers == NULL )
                                                                {

                                                                    exception_type = PyExc_UnboundLocalError;
                                                                    Py_INCREF( exception_type );
                                                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numbers" );
                                                                    exception_tb = NULL;
                                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                    CHAIN_EXCEPTION( exception_value );

                                                                    exception_lineno = 252;
                                                                    type_description_1 = "oooooooooooooo";
                                                                    goto try_except_handler_19;
                                                                }

                                                                tmp_called_instance_12 = var_numbers;
                                                                frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 252;
                                                                tmp_call_result_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_clear );
                                                                if ( tmp_call_result_5 == NULL )
                                                                {
                                                                    assert( ERROR_OCCURRED() );

                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                    exception_lineno = 252;
                                                                    type_description_1 = "oooooooooooooo";
                                                                    goto try_except_handler_19;
                                                                }
                                                                Py_DECREF( tmp_call_result_5 );
                                                            }
                                                            goto branch_end_23;
                                                            branch_no_23:;
                                                            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                            if (unlikely( tmp_result == false ))
                                                            {
                                                                exception_lineno = 249;
                                                            }

                                                            if (exception_tb && exception_tb->tb_frame == &frame_751ac65bcf26755606db7ed0c99281e9->m_frame) frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = exception_tb->tb_lineno;
                                                            type_description_1 = "oooooooooooooo";
                                                            goto try_except_handler_19;
                                                            branch_end_23:;
                                                        }
                                                        goto try_end_14;
                                                        // Exception handler code:
                                                        try_except_handler_19:;
                                                        exception_keeper_type_18 = exception_type;
                                                        exception_keeper_value_18 = exception_value;
                                                        exception_keeper_tb_18 = exception_tb;
                                                        exception_keeper_lineno_18 = exception_lineno;
                                                        exception_type = NULL;
                                                        exception_value = NULL;
                                                        exception_tb = NULL;
                                                        exception_lineno = 0;

                                                        // Restore previous exception.
                                                        SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
                                                        // Re-raise.
                                                        exception_type = exception_keeper_type_18;
                                                        exception_value = exception_keeper_value_18;
                                                        exception_tb = exception_keeper_tb_18;
                                                        exception_lineno = exception_keeper_lineno_18;

                                                        goto frame_exception_exit_1;
                                                        // End of try:
                                                        try_end_14:;
                                                        // Restore previous exception.
                                                        SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
                                                        goto try_end_13;
                                                        // exception handler codes exits in all cases
                                                        NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                        return NULL;
                                                        // End of try:
                                                        try_end_13:;
                                                        goto branch_end_22;
                                                        branch_no_22:;
                                                        {
                                                            nuitka_bool tmp_condition_result_26;
                                                            PyObject *tmp_compexpr_left_21;
                                                            PyObject *tmp_compexpr_right_21;
                                                            CHECK_OBJECT( var_n );
                                                            tmp_compexpr_left_21 = var_n;
                                                            tmp_compexpr_right_21 = const_int_pos_49;
                                                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_21, tmp_compexpr_right_21 );
                                                            if ( tmp_res == -1 )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 253;
                                                                type_description_1 = "oooooooooooooo";
                                                                goto frame_exception_exit_1;
                                                            }
                                                            tmp_condition_result_26 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                            if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
                                                            {
                                                                goto branch_yes_24;
                                                            }
                                                            else
                                                            {
                                                                goto branch_no_24;
                                                            }
                                                            branch_yes_24:;
                                                            {
                                                                PyObject *tmp_assign_source_56;
                                                                tmp_assign_source_56 = Py_None;
                                                                {
                                                                    PyObject *old = var_bg;
                                                                    var_bg = tmp_assign_source_56;
                                                                    Py_INCREF( var_bg );
                                                                    Py_XDECREF( old );
                                                                }

                                                            }
                                                            goto branch_end_24;
                                                            branch_no_24:;
                                                            {
                                                                nuitka_bool tmp_condition_result_27;
                                                                PyObject *tmp_outline_return_value_3;
                                                                int tmp_truth_name_7;
                                                                {
                                                                    PyObject *tmp_assign_source_57;
                                                                    CHECK_OBJECT( var_n );
                                                                    tmp_assign_source_57 = var_n;
                                                                    {
                                                                        PyObject *old = tmp_comparison_chain_3__operand_2;
                                                                        tmp_comparison_chain_3__operand_2 = tmp_assign_source_57;
                                                                        Py_INCREF( tmp_comparison_chain_3__operand_2 );
                                                                        Py_XDECREF( old );
                                                                    }

                                                                }
                                                                // Tried code:
                                                                {
                                                                    PyObject *tmp_assign_source_58;
                                                                    PyObject *tmp_compexpr_left_22;
                                                                    PyObject *tmp_compexpr_right_22;
                                                                    tmp_compexpr_left_22 = const_int_pos_90;
                                                                    CHECK_OBJECT( tmp_comparison_chain_3__operand_2 );
                                                                    tmp_compexpr_right_22 = tmp_comparison_chain_3__operand_2;
                                                                    tmp_assign_source_58 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_22, tmp_compexpr_right_22 );
                                                                    if ( tmp_assign_source_58 == NULL )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 255;
                                                                        type_description_1 = "oooooooooooooo";
                                                                        goto try_except_handler_20;
                                                                    }
                                                                    {
                                                                        PyObject *old = tmp_comparison_chain_3__comparison_result;
                                                                        tmp_comparison_chain_3__comparison_result = tmp_assign_source_58;
                                                                        Py_XDECREF( old );
                                                                    }

                                                                }
                                                                {
                                                                    nuitka_bool tmp_condition_result_28;
                                                                    PyObject *tmp_operand_name_6;
                                                                    CHECK_OBJECT( tmp_comparison_chain_3__comparison_result );
                                                                    tmp_operand_name_6 = tmp_comparison_chain_3__comparison_result;
                                                                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
                                                                    if ( tmp_res == -1 )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 255;
                                                                        type_description_1 = "oooooooooooooo";
                                                                        goto try_except_handler_20;
                                                                    }
                                                                    tmp_condition_result_28 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                    if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
                                                                    {
                                                                        goto branch_yes_26;
                                                                    }
                                                                    else
                                                                    {
                                                                        goto branch_no_26;
                                                                    }
                                                                    branch_yes_26:;
                                                                    CHECK_OBJECT( tmp_comparison_chain_3__comparison_result );
                                                                    tmp_outline_return_value_3 = tmp_comparison_chain_3__comparison_result;
                                                                    Py_INCREF( tmp_outline_return_value_3 );
                                                                    goto try_return_handler_20;
                                                                    branch_no_26:;
                                                                }
                                                                {
                                                                    PyObject *tmp_compexpr_left_23;
                                                                    PyObject *tmp_compexpr_right_23;
                                                                    CHECK_OBJECT( tmp_comparison_chain_3__operand_2 );
                                                                    tmp_compexpr_left_23 = tmp_comparison_chain_3__operand_2;
                                                                    tmp_compexpr_right_23 = const_int_pos_97;
                                                                    tmp_outline_return_value_3 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_23, tmp_compexpr_right_23 );
                                                                    if ( tmp_outline_return_value_3 == NULL )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 255;
                                                                        type_description_1 = "oooooooooooooo";
                                                                        goto try_except_handler_20;
                                                                    }
                                                                    goto try_return_handler_20;
                                                                }
                                                                // tried codes exits in all cases
                                                                NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                                return NULL;
                                                                // Return handler code:
                                                                try_return_handler_20:;
                                                                CHECK_OBJECT( (PyObject *)tmp_comparison_chain_3__operand_2 );
                                                                Py_DECREF( tmp_comparison_chain_3__operand_2 );
                                                                tmp_comparison_chain_3__operand_2 = NULL;

                                                                CHECK_OBJECT( (PyObject *)tmp_comparison_chain_3__comparison_result );
                                                                Py_DECREF( tmp_comparison_chain_3__comparison_result );
                                                                tmp_comparison_chain_3__comparison_result = NULL;

                                                                goto outline_result_4;
                                                                // Exception handler code:
                                                                try_except_handler_20:;
                                                                exception_keeper_type_19 = exception_type;
                                                                exception_keeper_value_19 = exception_value;
                                                                exception_keeper_tb_19 = exception_tb;
                                                                exception_keeper_lineno_19 = exception_lineno;
                                                                exception_type = NULL;
                                                                exception_value = NULL;
                                                                exception_tb = NULL;
                                                                exception_lineno = 0;

                                                                CHECK_OBJECT( (PyObject *)tmp_comparison_chain_3__operand_2 );
                                                                Py_DECREF( tmp_comparison_chain_3__operand_2 );
                                                                tmp_comparison_chain_3__operand_2 = NULL;

                                                                Py_XDECREF( tmp_comparison_chain_3__comparison_result );
                                                                tmp_comparison_chain_3__comparison_result = NULL;

                                                                // Re-raise.
                                                                exception_type = exception_keeper_type_19;
                                                                exception_value = exception_keeper_value_19;
                                                                exception_tb = exception_keeper_tb_19;
                                                                exception_lineno = exception_keeper_lineno_19;

                                                                goto frame_exception_exit_1;
                                                                // End of try:
                                                                // Return statement must have exited already.
                                                                NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                                return NULL;
                                                                outline_result_4:;
                                                                tmp_truth_name_7 = CHECK_IF_TRUE( tmp_outline_return_value_3 );
                                                                if ( tmp_truth_name_7 == -1 )
                                                                {
                                                                    assert( ERROR_OCCURRED() );

                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                    Py_DECREF( tmp_outline_return_value_3 );

                                                                    exception_lineno = 255;
                                                                    type_description_1 = "oooooooooooooo";
                                                                    goto frame_exception_exit_1;
                                                                }
                                                                tmp_condition_result_27 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                Py_DECREF( tmp_outline_return_value_3 );
                                                                if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
                                                                {
                                                                    goto branch_yes_25;
                                                                }
                                                                else
                                                                {
                                                                    goto branch_no_25;
                                                                }
                                                                branch_yes_25:;
                                                                {
                                                                    PyObject *tmp_assign_source_59;
                                                                    PyObject *tmp_left_name_4;
                                                                    PyObject *tmp_left_name_5;
                                                                    PyObject *tmp_right_name_4;
                                                                    PyObject *tmp_right_name_5;
                                                                    CHECK_OBJECT( var_n );
                                                                    tmp_left_name_5 = var_n;
                                                                    tmp_right_name_4 = const_int_pos_90;
                                                                    tmp_left_name_4 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_5, tmp_right_name_4 );
                                                                    if ( tmp_left_name_4 == NULL )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 256;
                                                                        type_description_1 = "oooooooooooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                    tmp_right_name_5 = const_int_pos_8;
                                                                    tmp_assign_source_59 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_4, tmp_right_name_5 );
                                                                    Py_DECREF( tmp_left_name_4 );
                                                                    if ( tmp_assign_source_59 == NULL )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 256;
                                                                        type_description_1 = "oooooooooooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                    {
                                                                        PyObject *old = var_fg;
                                                                        var_fg = tmp_assign_source_59;
                                                                        Py_XDECREF( old );
                                                                    }

                                                                }
                                                                goto branch_end_25;
                                                                branch_no_25:;
                                                                {
                                                                    nuitka_bool tmp_condition_result_29;
                                                                    PyObject *tmp_outline_return_value_4;
                                                                    int tmp_truth_name_8;
                                                                    {
                                                                        PyObject *tmp_assign_source_60;
                                                                        CHECK_OBJECT( var_n );
                                                                        tmp_assign_source_60 = var_n;
                                                                        {
                                                                            PyObject *old = tmp_comparison_chain_4__operand_2;
                                                                            tmp_comparison_chain_4__operand_2 = tmp_assign_source_60;
                                                                            Py_INCREF( tmp_comparison_chain_4__operand_2 );
                                                                            Py_XDECREF( old );
                                                                        }

                                                                    }
                                                                    // Tried code:
                                                                    {
                                                                        PyObject *tmp_assign_source_61;
                                                                        PyObject *tmp_compexpr_left_24;
                                                                        PyObject *tmp_compexpr_right_24;
                                                                        tmp_compexpr_left_24 = const_int_pos_100;
                                                                        CHECK_OBJECT( tmp_comparison_chain_4__operand_2 );
                                                                        tmp_compexpr_right_24 = tmp_comparison_chain_4__operand_2;
                                                                        tmp_assign_source_61 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_24, tmp_compexpr_right_24 );
                                                                        if ( tmp_assign_source_61 == NULL )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 257;
                                                                            type_description_1 = "oooooooooooooo";
                                                                            goto try_except_handler_21;
                                                                        }
                                                                        {
                                                                            PyObject *old = tmp_comparison_chain_4__comparison_result;
                                                                            tmp_comparison_chain_4__comparison_result = tmp_assign_source_61;
                                                                            Py_XDECREF( old );
                                                                        }

                                                                    }
                                                                    {
                                                                        nuitka_bool tmp_condition_result_30;
                                                                        PyObject *tmp_operand_name_7;
                                                                        CHECK_OBJECT( tmp_comparison_chain_4__comparison_result );
                                                                        tmp_operand_name_7 = tmp_comparison_chain_4__comparison_result;
                                                                        tmp_res = CHECK_IF_TRUE( tmp_operand_name_7 );
                                                                        if ( tmp_res == -1 )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 257;
                                                                            type_description_1 = "oooooooooooooo";
                                                                            goto try_except_handler_21;
                                                                        }
                                                                        tmp_condition_result_30 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                        if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
                                                                        {
                                                                            goto branch_yes_28;
                                                                        }
                                                                        else
                                                                        {
                                                                            goto branch_no_28;
                                                                        }
                                                                        branch_yes_28:;
                                                                        CHECK_OBJECT( tmp_comparison_chain_4__comparison_result );
                                                                        tmp_outline_return_value_4 = tmp_comparison_chain_4__comparison_result;
                                                                        Py_INCREF( tmp_outline_return_value_4 );
                                                                        goto try_return_handler_21;
                                                                        branch_no_28:;
                                                                    }
                                                                    {
                                                                        PyObject *tmp_compexpr_left_25;
                                                                        PyObject *tmp_compexpr_right_25;
                                                                        CHECK_OBJECT( tmp_comparison_chain_4__operand_2 );
                                                                        tmp_compexpr_left_25 = tmp_comparison_chain_4__operand_2;
                                                                        tmp_compexpr_right_25 = const_int_pos_107;
                                                                        tmp_outline_return_value_4 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_25, tmp_compexpr_right_25 );
                                                                        if ( tmp_outline_return_value_4 == NULL )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 257;
                                                                            type_description_1 = "oooooooooooooo";
                                                                            goto try_except_handler_21;
                                                                        }
                                                                        goto try_return_handler_21;
                                                                    }
                                                                    // tried codes exits in all cases
                                                                    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                                    return NULL;
                                                                    // Return handler code:
                                                                    try_return_handler_21:;
                                                                    CHECK_OBJECT( (PyObject *)tmp_comparison_chain_4__operand_2 );
                                                                    Py_DECREF( tmp_comparison_chain_4__operand_2 );
                                                                    tmp_comparison_chain_4__operand_2 = NULL;

                                                                    CHECK_OBJECT( (PyObject *)tmp_comparison_chain_4__comparison_result );
                                                                    Py_DECREF( tmp_comparison_chain_4__comparison_result );
                                                                    tmp_comparison_chain_4__comparison_result = NULL;

                                                                    goto outline_result_5;
                                                                    // Exception handler code:
                                                                    try_except_handler_21:;
                                                                    exception_keeper_type_20 = exception_type;
                                                                    exception_keeper_value_20 = exception_value;
                                                                    exception_keeper_tb_20 = exception_tb;
                                                                    exception_keeper_lineno_20 = exception_lineno;
                                                                    exception_type = NULL;
                                                                    exception_value = NULL;
                                                                    exception_tb = NULL;
                                                                    exception_lineno = 0;

                                                                    CHECK_OBJECT( (PyObject *)tmp_comparison_chain_4__operand_2 );
                                                                    Py_DECREF( tmp_comparison_chain_4__operand_2 );
                                                                    tmp_comparison_chain_4__operand_2 = NULL;

                                                                    Py_XDECREF( tmp_comparison_chain_4__comparison_result );
                                                                    tmp_comparison_chain_4__comparison_result = NULL;

                                                                    // Re-raise.
                                                                    exception_type = exception_keeper_type_20;
                                                                    exception_value = exception_keeper_value_20;
                                                                    exception_tb = exception_keeper_tb_20;
                                                                    exception_lineno = exception_keeper_lineno_20;

                                                                    goto frame_exception_exit_1;
                                                                    // End of try:
                                                                    // Return statement must have exited already.
                                                                    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
                                                                    return NULL;
                                                                    outline_result_5:;
                                                                    tmp_truth_name_8 = CHECK_IF_TRUE( tmp_outline_return_value_4 );
                                                                    if ( tmp_truth_name_8 == -1 )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                        Py_DECREF( tmp_outline_return_value_4 );

                                                                        exception_lineno = 257;
                                                                        type_description_1 = "oooooooooooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                    tmp_condition_result_29 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                    Py_DECREF( tmp_outline_return_value_4 );
                                                                    if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
                                                                    {
                                                                        goto branch_yes_27;
                                                                    }
                                                                    else
                                                                    {
                                                                        goto branch_no_27;
                                                                    }
                                                                    branch_yes_27:;
                                                                    {
                                                                        PyObject *tmp_assign_source_62;
                                                                        PyObject *tmp_left_name_6;
                                                                        PyObject *tmp_left_name_7;
                                                                        PyObject *tmp_right_name_6;
                                                                        PyObject *tmp_right_name_7;
                                                                        CHECK_OBJECT( var_n );
                                                                        tmp_left_name_7 = var_n;
                                                                        tmp_right_name_6 = const_int_pos_100;
                                                                        tmp_left_name_6 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_7, tmp_right_name_6 );
                                                                        if ( tmp_left_name_6 == NULL )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 258;
                                                                            type_description_1 = "oooooooooooooo";
                                                                            goto frame_exception_exit_1;
                                                                        }
                                                                        tmp_right_name_7 = const_int_pos_8;
                                                                        tmp_assign_source_62 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_6, tmp_right_name_7 );
                                                                        Py_DECREF( tmp_left_name_6 );
                                                                        if ( tmp_assign_source_62 == NULL )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 258;
                                                                            type_description_1 = "oooooooooooooo";
                                                                            goto frame_exception_exit_1;
                                                                        }
                                                                        {
                                                                            PyObject *old = var_bg;
                                                                            var_bg = tmp_assign_source_62;
                                                                            Py_XDECREF( old );
                                                                        }

                                                                    }
                                                                    branch_no_27:;
                                                                }
                                                                branch_end_25:;
                                                            }
                                                            branch_end_24:;
                                                        }
                                                        branch_end_22:;
                                                    }
                                                    branch_end_20:;
                                                }
                                                branch_end_19:;
                                            }
                                            branch_end_17:;
                                        }
                                        branch_end_15:;
                                    }
                                    branch_end_14:;
                                }
                                branch_end_13:;
                            }
                            branch_end_12:;
                        }
                        branch_end_11:;
                    }
                    branch_end_10:;
                }
                branch_end_9:;
            }
            branch_end_8:;
        }
        branch_end_7:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 216;
        type_description_1 = "oooooooooooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_3;
    loop_end_3:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 192;
        type_description_1 = "oooooooooooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    {
        PyObject *tmp_called_instance_13;
        PyObject *tmp_args_element_name_12;
        tmp_called_instance_13 = const_str_empty;
        CHECK_OBJECT( var_out );
        tmp_args_element_name_12 = var_out;
        frame_751ac65bcf26755606db7ed0c99281e9->m_frame.f_lineno = 261;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_13, const_str_plain_join, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "oooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_751ac65bcf26755606db7ed0c99281e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_751ac65bcf26755606db7ed0c99281e9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_751ac65bcf26755606db7ed0c99281e9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_751ac65bcf26755606db7ed0c99281e9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_751ac65bcf26755606db7ed0c99281e9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_751ac65bcf26755606db7ed0c99281e9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_751ac65bcf26755606db7ed0c99281e9,
        type_description_1,
        par_text,
        par_converter,
        var_fg,
        var_bg,
        var_bold,
        var_underline,
        var_inverse,
        var_numbers,
        var_out,
        var_m,
        var_chunk,
        var_starttag,
        var_endtag,
        var_n
    );


    // Release cached frame.
    if ( frame_751ac65bcf26755606db7ed0c99281e9 == cache_frame_751ac65bcf26755606db7ed0c99281e9 )
    {
        Py_DECREF( frame_751ac65bcf26755606db7ed0c99281e9 );
    }
    cache_frame_751ac65bcf26755606db7ed0c99281e9 = NULL;

    assertFrameObject( frame_751ac65bcf26755606db7ed0c99281e9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_converter );
    Py_DECREF( par_converter );
    par_converter = NULL;

    Py_XDECREF( var_fg );
    var_fg = NULL;

    Py_XDECREF( var_bg );
    var_bg = NULL;

    Py_XDECREF( var_bold );
    var_bold = NULL;

    Py_XDECREF( var_underline );
    var_underline = NULL;

    Py_XDECREF( var_inverse );
    var_inverse = NULL;

    Py_XDECREF( var_numbers );
    var_numbers = NULL;

    CHECK_OBJECT( (PyObject *)var_out );
    Py_DECREF( var_out );
    var_out = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_chunk );
    var_chunk = NULL;

    Py_XDECREF( var_starttag );
    var_starttag = NULL;

    Py_XDECREF( var_endtag );
    var_endtag = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    CHECK_OBJECT( (PyObject *)par_converter );
    Py_DECREF( par_converter );
    par_converter = NULL;

    Py_XDECREF( var_fg );
    var_fg = NULL;

    Py_XDECREF( var_bg );
    var_bg = NULL;

    Py_XDECREF( var_bold );
    var_bold = NULL;

    Py_XDECREF( var_underline );
    var_underline = NULL;

    Py_XDECREF( var_inverse );
    var_inverse = NULL;

    Py_XDECREF( var_numbers );
    var_numbers = NULL;

    Py_XDECREF( var_out );
    var_out = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_chunk );
    var_chunk = NULL;

    Py_XDECREF( var_starttag );
    var_starttag = NULL;

    Py_XDECREF( var_endtag );
    var_endtag = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_21;
    exception_value = exception_keeper_value_21;
    exception_tb = exception_keeper_tb_21;
    exception_lineno = exception_keeper_lineno_21;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_6__ansi2anything );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$filters$ansi$$$function_7__get_extended_color( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_numbers = python_pars[ 0 ];
    PyObject *var_n = NULL;
    PyObject *var_r = NULL;
    PyObject *var_g = NULL;
    PyObject *var_b = NULL;
    PyObject *var_idx = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_8f09e924ecaf0438920b75bcf0f1041a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8f09e924ecaf0438920b75bcf0f1041a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8f09e924ecaf0438920b75bcf0f1041a, codeobj_8f09e924ecaf0438920b75bcf0f1041a, module_nbconvert$filters$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8f09e924ecaf0438920b75bcf0f1041a = cache_frame_8f09e924ecaf0438920b75bcf0f1041a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8f09e924ecaf0438920b75bcf0f1041a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8f09e924ecaf0438920b75bcf0f1041a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_numbers );
        tmp_called_instance_1 = par_numbers;
        frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 265;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_n == NULL );
        var_n = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_n );
        tmp_compexpr_left_1 = var_n;
        tmp_compexpr_right_1 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_numbers );
        tmp_len_arg_1 = par_numbers;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_3;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( par_numbers );
            tmp_called_instance_2 = par_numbers;
            frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 268;
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 268;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_r == NULL );
            var_r = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_3;
            CHECK_OBJECT( par_numbers );
            tmp_called_instance_3 = par_numbers;
            frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 269;
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 269;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_g == NULL );
            var_g = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( par_numbers );
            tmp_called_instance_4 = par_numbers;
            frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 270;
            tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 270;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_b == NULL );
            var_b = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_all );
            assert( tmp_called_name_1 != NULL );
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_tuple_element_1;
                CHECK_OBJECT( var_r );
                tmp_tuple_element_1 = var_r;
                tmp_iter_arg_1 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_iter_arg_1, 0, tmp_tuple_element_1 );
                CHECK_OBJECT( var_g );
                tmp_tuple_element_1 = var_g;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_iter_arg_1, 1, tmp_tuple_element_1 );
                CHECK_OBJECT( var_b );
                tmp_tuple_element_1 = var_b;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_iter_arg_1, 2, tmp_tuple_element_1 );
                tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                assert( !(tmp_assign_source_5 == NULL) );
                assert( tmp_genexpr_1__$0 == NULL );
                tmp_genexpr_1__$0 = tmp_assign_source_5;
            }
            // Tried code:
            tmp_args_element_name_1 = nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


            goto try_return_handler_2;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_7__get_extended_color );
            return NULL;
            // Return handler code:
            try_return_handler_2:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            goto outline_result_1;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_7__get_extended_color );
            return NULL;
            outline_result_1:;
            frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 271;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 271;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 271;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 272;
                tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_ValueError );
                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 272;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_and_left_truth_2;
            nuitka_bool tmp_and_left_value_2;
            nuitka_bool tmp_and_right_value_2;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_len_arg_2;
            CHECK_OBJECT( var_n );
            tmp_compexpr_left_3 = var_n;
            tmp_compexpr_right_3 = const_int_pos_5;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            CHECK_OBJECT( par_numbers );
            tmp_len_arg_2 = par_numbers;
            tmp_compexpr_left_4 = BUILTIN_LEN( tmp_len_arg_2 );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_4 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            assert( !(tmp_res == -1) );
            tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_3 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_condition_result_3 = tmp_and_left_value_2;
            and_end_2:;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_called_instance_5;
                CHECK_OBJECT( par_numbers );
                tmp_called_instance_5 = par_numbers;
                frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 275;
                tmp_assign_source_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_idx == NULL );
                var_idx = tmp_assign_source_6;
            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_5;
                PyObject *tmp_compexpr_right_5;
                CHECK_OBJECT( var_idx );
                tmp_compexpr_left_5 = var_idx;
                tmp_compexpr_right_5 = const_int_0;
                tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 276;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_raise_type_2;
                    frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 277;
                    tmp_raise_type_2 = CALL_FUNCTION_NO_ARGS( PyExc_ValueError );
                    assert( !(tmp_raise_type_2 == NULL) );
                    exception_type = tmp_raise_type_2;
                    exception_lineno = 277;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_6;
                    PyObject *tmp_compexpr_right_6;
                    CHECK_OBJECT( var_idx );
                    tmp_compexpr_left_6 = var_idx;
                    tmp_compexpr_right_6 = const_int_pos_16;
                    tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 278;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    CHECK_OBJECT( var_idx );
                    tmp_return_value = var_idx;
                    Py_INCREF( tmp_return_value );
                    goto frame_return_exit_1;
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        nuitka_bool tmp_condition_result_6;
                        PyObject *tmp_compexpr_left_7;
                        PyObject *tmp_compexpr_right_7;
                        CHECK_OBJECT( var_idx );
                        tmp_compexpr_left_7 = var_idx;
                        tmp_compexpr_right_7 = const_int_pos_232;
                        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 281;
                            type_description_1 = "oooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_6;
                        }
                        else
                        {
                            goto branch_no_6;
                        }
                        branch_yes_6:;
                        {
                            PyObject *tmp_assign_source_7;
                            PyObject *tmp_left_name_1;
                            PyObject *tmp_left_name_2;
                            PyObject *tmp_right_name_1;
                            PyObject *tmp_right_name_2;
                            CHECK_OBJECT( var_idx );
                            tmp_left_name_2 = var_idx;
                            tmp_right_name_1 = const_int_pos_16;
                            tmp_left_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_1 );
                            if ( tmp_left_name_1 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 283;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_2 = const_int_pos_36;
                            tmp_assign_source_7 = BINARY_OPERATION_FLOORDIV_OBJECT_LONG( tmp_left_name_1, tmp_right_name_2 );
                            Py_DECREF( tmp_left_name_1 );
                            if ( tmp_assign_source_7 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 283;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_r == NULL );
                            var_r = tmp_assign_source_7;
                        }
                        {
                            PyObject *tmp_assign_source_8;
                            nuitka_bool tmp_condition_result_7;
                            PyObject *tmp_compexpr_left_8;
                            PyObject *tmp_compexpr_right_8;
                            PyObject *tmp_left_name_3;
                            PyObject *tmp_right_name_3;
                            PyObject *tmp_left_name_4;
                            PyObject *tmp_right_name_4;
                            CHECK_OBJECT( var_r );
                            tmp_compexpr_left_8 = var_r;
                            tmp_compexpr_right_8 = const_int_0;
                            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 284;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                            {
                                goto condexpr_true_1;
                            }
                            else
                            {
                                goto condexpr_false_1;
                            }
                            condexpr_true_1:;
                            tmp_left_name_3 = const_int_pos_55;
                            CHECK_OBJECT( var_r );
                            tmp_left_name_4 = var_r;
                            tmp_right_name_4 = const_int_pos_40;
                            tmp_right_name_3 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_4, tmp_right_name_4 );
                            if ( tmp_right_name_3 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 284;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_assign_source_8 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_3, tmp_right_name_3 );
                            Py_DECREF( tmp_right_name_3 );
                            if ( tmp_assign_source_8 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 284;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            goto condexpr_end_1;
                            condexpr_false_1:;
                            tmp_assign_source_8 = const_int_0;
                            Py_INCREF( tmp_assign_source_8 );
                            condexpr_end_1:;
                            {
                                PyObject *old = var_r;
                                assert( old != NULL );
                                var_r = tmp_assign_source_8;
                                Py_DECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_9;
                            PyObject *tmp_left_name_5;
                            PyObject *tmp_left_name_6;
                            PyObject *tmp_left_name_7;
                            PyObject *tmp_right_name_5;
                            PyObject *tmp_right_name_6;
                            PyObject *tmp_right_name_7;
                            CHECK_OBJECT( var_idx );
                            tmp_left_name_7 = var_idx;
                            tmp_right_name_5 = const_int_pos_16;
                            tmp_left_name_6 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_7, tmp_right_name_5 );
                            if ( tmp_left_name_6 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 285;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_6 = const_int_pos_36;
                            tmp_left_name_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                            Py_DECREF( tmp_left_name_6 );
                            if ( tmp_left_name_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 285;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_7 = const_int_pos_6;
                            tmp_assign_source_9 = BINARY_OPERATION_FLOORDIV_OBJECT_LONG( tmp_left_name_5, tmp_right_name_7 );
                            Py_DECREF( tmp_left_name_5 );
                            if ( tmp_assign_source_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 285;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_g == NULL );
                            var_g = tmp_assign_source_9;
                        }
                        {
                            PyObject *tmp_assign_source_10;
                            nuitka_bool tmp_condition_result_8;
                            PyObject *tmp_compexpr_left_9;
                            PyObject *tmp_compexpr_right_9;
                            PyObject *tmp_left_name_8;
                            PyObject *tmp_right_name_8;
                            PyObject *tmp_left_name_9;
                            PyObject *tmp_right_name_9;
                            CHECK_OBJECT( var_g );
                            tmp_compexpr_left_9 = var_g;
                            tmp_compexpr_right_9 = const_int_0;
                            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 286;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                            {
                                goto condexpr_true_2;
                            }
                            else
                            {
                                goto condexpr_false_2;
                            }
                            condexpr_true_2:;
                            tmp_left_name_8 = const_int_pos_55;
                            CHECK_OBJECT( var_g );
                            tmp_left_name_9 = var_g;
                            tmp_right_name_9 = const_int_pos_40;
                            tmp_right_name_8 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_9, tmp_right_name_9 );
                            if ( tmp_right_name_8 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 286;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_assign_source_10 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_8, tmp_right_name_8 );
                            Py_DECREF( tmp_right_name_8 );
                            if ( tmp_assign_source_10 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 286;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            goto condexpr_end_2;
                            condexpr_false_2:;
                            tmp_assign_source_10 = const_int_0;
                            Py_INCREF( tmp_assign_source_10 );
                            condexpr_end_2:;
                            {
                                PyObject *old = var_g;
                                assert( old != NULL );
                                var_g = tmp_assign_source_10;
                                Py_DECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_11;
                            PyObject *tmp_left_name_10;
                            PyObject *tmp_left_name_11;
                            PyObject *tmp_right_name_10;
                            PyObject *tmp_right_name_11;
                            CHECK_OBJECT( var_idx );
                            tmp_left_name_11 = var_idx;
                            tmp_right_name_10 = const_int_pos_16;
                            tmp_left_name_10 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_11, tmp_right_name_10 );
                            if ( tmp_left_name_10 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 287;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_11 = const_int_pos_6;
                            tmp_assign_source_11 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_11 );
                            Py_DECREF( tmp_left_name_10 );
                            if ( tmp_assign_source_11 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 287;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_b == NULL );
                            var_b = tmp_assign_source_11;
                        }
                        {
                            PyObject *tmp_assign_source_12;
                            nuitka_bool tmp_condition_result_9;
                            PyObject *tmp_compexpr_left_10;
                            PyObject *tmp_compexpr_right_10;
                            PyObject *tmp_left_name_12;
                            PyObject *tmp_right_name_12;
                            PyObject *tmp_left_name_13;
                            PyObject *tmp_right_name_13;
                            CHECK_OBJECT( var_b );
                            tmp_compexpr_left_10 = var_b;
                            tmp_compexpr_right_10 = const_int_0;
                            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 288;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                            {
                                goto condexpr_true_3;
                            }
                            else
                            {
                                goto condexpr_false_3;
                            }
                            condexpr_true_3:;
                            tmp_left_name_12 = const_int_pos_55;
                            CHECK_OBJECT( var_b );
                            tmp_left_name_13 = var_b;
                            tmp_right_name_13 = const_int_pos_40;
                            tmp_right_name_12 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_13, tmp_right_name_13 );
                            if ( tmp_right_name_12 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 288;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_assign_source_12 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_12, tmp_right_name_12 );
                            Py_DECREF( tmp_right_name_12 );
                            if ( tmp_assign_source_12 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 288;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            goto condexpr_end_3;
                            condexpr_false_3:;
                            tmp_assign_source_12 = const_int_0;
                            Py_INCREF( tmp_assign_source_12 );
                            condexpr_end_3:;
                            {
                                PyObject *old = var_b;
                                assert( old != NULL );
                                var_b = tmp_assign_source_12;
                                Py_DECREF( old );
                            }

                        }
                        goto branch_end_6;
                        branch_no_6:;
                        {
                            nuitka_bool tmp_condition_result_10;
                            PyObject *tmp_compexpr_left_11;
                            PyObject *tmp_compexpr_right_11;
                            CHECK_OBJECT( var_idx );
                            tmp_compexpr_left_11 = var_idx;
                            tmp_compexpr_right_11 = const_int_pos_256;
                            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 289;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_7;
                            }
                            else
                            {
                                goto branch_no_7;
                            }
                            branch_yes_7:;
                            {
                                PyObject *tmp_assign_source_13;
                                PyObject *tmp_left_name_14;
                                PyObject *tmp_left_name_15;
                                PyObject *tmp_left_name_16;
                                PyObject *tmp_right_name_14;
                                PyObject *tmp_right_name_15;
                                PyObject *tmp_right_name_16;
                                CHECK_OBJECT( var_idx );
                                tmp_left_name_16 = var_idx;
                                tmp_right_name_14 = const_int_pos_232;
                                tmp_left_name_15 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_16, tmp_right_name_14 );
                                if ( tmp_left_name_15 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 291;
                                    type_description_1 = "oooooo";
                                    goto frame_exception_exit_1;
                                }
                                tmp_right_name_15 = const_int_pos_10;
                                tmp_left_name_14 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_15, tmp_right_name_15 );
                                Py_DECREF( tmp_left_name_15 );
                                if ( tmp_left_name_14 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 291;
                                    type_description_1 = "oooooo";
                                    goto frame_exception_exit_1;
                                }
                                tmp_right_name_16 = const_int_pos_8;
                                tmp_assign_source_13 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_14, tmp_right_name_16 );
                                Py_DECREF( tmp_left_name_14 );
                                if ( tmp_assign_source_13 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 291;
                                    type_description_1 = "oooooo";
                                    goto frame_exception_exit_1;
                                }
                                assert( tmp_assign_unpack_1__assign_source == NULL );
                                tmp_assign_unpack_1__assign_source = tmp_assign_source_13;
                            }
                            {
                                PyObject *tmp_assign_source_14;
                                CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
                                tmp_assign_source_14 = tmp_assign_unpack_1__assign_source;
                                assert( var_r == NULL );
                                Py_INCREF( tmp_assign_source_14 );
                                var_r = tmp_assign_source_14;
                            }
                            {
                                PyObject *tmp_assign_source_15;
                                CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
                                tmp_assign_source_15 = tmp_assign_unpack_1__assign_source;
                                assert( var_g == NULL );
                                Py_INCREF( tmp_assign_source_15 );
                                var_g = tmp_assign_source_15;
                            }
                            {
                                PyObject *tmp_assign_source_16;
                                CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
                                tmp_assign_source_16 = tmp_assign_unpack_1__assign_source;
                                assert( var_b == NULL );
                                Py_INCREF( tmp_assign_source_16 );
                                var_b = tmp_assign_source_16;
                            }
                            CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
                            Py_DECREF( tmp_assign_unpack_1__assign_source );
                            tmp_assign_unpack_1__assign_source = NULL;

                            goto branch_end_7;
                            branch_no_7:;
                            {
                                PyObject *tmp_raise_type_3;
                                frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 293;
                                tmp_raise_type_3 = CALL_FUNCTION_NO_ARGS( PyExc_ValueError );
                                assert( !(tmp_raise_type_3 == NULL) );
                                exception_type = tmp_raise_type_3;
                                exception_lineno = 293;
                                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            branch_end_7:;
                        }
                        branch_end_6:;
                    }
                    branch_end_5:;
                }
                branch_end_4:;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_raise_type_4;
                frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame.f_lineno = 295;
                tmp_raise_type_4 = CALL_FUNCTION_NO_ARGS( PyExc_ValueError );
                assert( !(tmp_raise_type_4 == NULL) );
                exception_type = tmp_raise_type_4;
                exception_lineno = 295;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            branch_end_3:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_tuple_element_2;
        if ( var_r == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "r" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_2 = var_r;
        tmp_return_value = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
        if ( var_g == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "g" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_2 = var_g;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
        if ( var_b == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "b" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_2 = var_b;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_2 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f09e924ecaf0438920b75bcf0f1041a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f09e924ecaf0438920b75bcf0f1041a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f09e924ecaf0438920b75bcf0f1041a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8f09e924ecaf0438920b75bcf0f1041a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8f09e924ecaf0438920b75bcf0f1041a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8f09e924ecaf0438920b75bcf0f1041a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8f09e924ecaf0438920b75bcf0f1041a,
        type_description_1,
        par_numbers,
        var_n,
        var_r,
        var_g,
        var_b,
        var_idx
    );


    // Release cached frame.
    if ( frame_8f09e924ecaf0438920b75bcf0f1041a == cache_frame_8f09e924ecaf0438920b75bcf0f1041a )
    {
        Py_DECREF( frame_8f09e924ecaf0438920b75bcf0f1041a );
    }
    cache_frame_8f09e924ecaf0438920b75bcf0f1041a = NULL;

    assertFrameObject( frame_8f09e924ecaf0438920b75bcf0f1041a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_7__get_extended_color );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_numbers );
    Py_DECREF( par_numbers );
    par_numbers = NULL;

    CHECK_OBJECT( (PyObject *)var_n );
    Py_DECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_g );
    var_g = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_idx );
    var_idx = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_numbers );
    Py_DECREF( par_numbers );
    par_numbers = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_g );
    var_g = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_idx );
    var_idx = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_7__get_extended_color );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_locals {
    PyObject *var_c;
    PyObject *tmp_comparison_chain_1__comparison_result;
    PyObject *tmp_comparison_chain_1__operand_2;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
};

static PyObject *nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_locals *generator_heap = (struct nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_c = NULL;
    generator_heap->tmp_comparison_chain_1__comparison_result = NULL;
    generator_heap->tmp_comparison_chain_1__operand_2 = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_7809e82659fc448aa0ab0dbe65c8d484, module_nbconvert$filters$ansi, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 271;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_c;
            generator_heap->var_c = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_c );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        {
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( generator_heap->var_c );
            tmp_assign_source_3 = generator_heap->var_c;
            {
                PyObject *old = generator_heap->tmp_comparison_chain_1__operand_2;
                generator_heap->tmp_comparison_chain_1__operand_2 = tmp_assign_source_3;
                Py_INCREF( generator_heap->tmp_comparison_chain_1__operand_2 );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = const_int_0;
            CHECK_OBJECT( generator_heap->tmp_comparison_chain_1__operand_2 );
            tmp_compexpr_right_1 = generator_heap->tmp_comparison_chain_1__operand_2;
            tmp_assign_source_4 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 271;
                generator_heap->type_description_1 = "No";
                goto try_except_handler_3;
            }
            {
                PyObject *old = generator_heap->tmp_comparison_chain_1__comparison_result;
                generator_heap->tmp_comparison_chain_1__comparison_result = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_1;
            PyObject *tmp_operand_name_1;
            CHECK_OBJECT( generator_heap->tmp_comparison_chain_1__comparison_result );
            tmp_operand_name_1 = generator_heap->tmp_comparison_chain_1__comparison_result;
            generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 271;
                generator_heap->type_description_1 = "No";
                goto try_except_handler_3;
            }
            tmp_condition_result_1 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_1;
            }
            else
            {
                goto branch_no_1;
            }
            branch_yes_1:;
            CHECK_OBJECT( generator_heap->tmp_comparison_chain_1__comparison_result );
            tmp_expression_name_1 = generator_heap->tmp_comparison_chain_1__comparison_result;
            Py_INCREF( tmp_expression_name_1 );
            goto try_return_handler_3;
            branch_no_1:;
        }
        {
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( generator_heap->tmp_comparison_chain_1__operand_2 );
            tmp_compexpr_left_2 = generator_heap->tmp_comparison_chain_1__operand_2;
            tmp_compexpr_right_2 = const_int_pos_255;
            tmp_expression_name_1 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 271;
                generator_heap->type_description_1 = "No";
                goto try_except_handler_3;
            }
            goto try_return_handler_3;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)generator_heap->tmp_comparison_chain_1__operand_2 );
        Py_DECREF( generator_heap->tmp_comparison_chain_1__operand_2 );
        generator_heap->tmp_comparison_chain_1__operand_2 = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_comparison_chain_1__comparison_result );
        Py_DECREF( generator_heap->tmp_comparison_chain_1__comparison_result );
        generator_heap->tmp_comparison_chain_1__comparison_result = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_comparison_chain_1__operand_2 );
        Py_DECREF( generator_heap->tmp_comparison_chain_1__operand_2 );
        generator_heap->tmp_comparison_chain_1__operand_2 = NULL;

        Py_XDECREF( generator_heap->tmp_comparison_chain_1__comparison_result );
        generator_heap->tmp_comparison_chain_1__comparison_result = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_1;
        generator_heap->exception_value = generator_heap->exception_keeper_value_1;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr );
        return NULL;
        outline_result_1:;
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 271;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 271;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_c
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_context,
        module_nbconvert$filters$ansi,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_cf46ea5ecce2c3b908f1b0197fcbd6b0,
#endif
        codeobj_7809e82659fc448aa0ab0dbe65c8d484,
        1,
        sizeof(struct nbconvert$filters$ansi$$$function_7__get_extended_color$$$genexpr_1_genexpr_locals)
    );
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_1_strip_ansi(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_1_strip_ansi,
        const_str_plain_strip_ansi,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e4ff26e45c720f09fbbb16c2ca85f3ac,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        const_str_digest_17fa4c93bb815db2006cadc7c8613890,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_2_ansi2html(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_2_ansi2html,
        const_str_plain_ansi2html,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5e4a6582b5daa4b2ae997bb2a2a0a328,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        const_str_digest_85da1f2a5bedec973a2c706c289411cf,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_3_ansi2latex(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_3_ansi2latex,
        const_str_plain_ansi2latex,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_56c953ba45592034f684ef34ecd6d8b1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        const_str_digest_dc8bc559f19bb84aaf2a155c00a1bfc9,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_4__htmlconverter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_4__htmlconverter,
        const_str_plain__htmlconverter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_68d17afc28a356befa5597ef06df715b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        const_str_digest_01272028dee0e900b7dfb28b25ca6d94,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_5__latexconverter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_5__latexconverter,
        const_str_plain__latexconverter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_968be856d81cfb420acbf0a59e382a6d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        const_str_digest_fa51db684424c16c00745202341fc215,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_6__ansi2anything(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_6__ansi2anything,
        const_str_plain__ansi2anything,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_751ac65bcf26755606db7ed0c99281e9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        const_str_digest_c10e631569b91b4a6ef7765bebb21a71,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$filters$ansi$$$function_7__get_extended_color(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$filters$ansi$$$function_7__get_extended_color,
        const_str_plain__get_extended_color,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8f09e924ecaf0438920b75bcf0f1041a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$filters$ansi,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbconvert$filters$ansi =
{
    PyModuleDef_HEAD_INIT,
    "nbconvert.filters.ansi",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbconvert$filters$ansi)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbconvert$filters$ansi)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbconvert$filters$ansi );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.ansi: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.ansi: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert.filters.ansi: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbconvert$filters$ansi" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbconvert$filters$ansi = Py_InitModule4(
        "nbconvert.filters.ansi",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbconvert$filters$ansi = PyModule_Create( &mdef_nbconvert$filters$ansi );
#endif

    moduledict_nbconvert$filters$ansi = MODULE_DICT( module_nbconvert$filters$ansi );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbconvert$filters$ansi,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbconvert$filters$ansi,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$filters$ansi,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$filters$ansi,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbconvert$filters$ansi );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_ad78f42d023c3d1e12b7024ca8601061, module_nbconvert$filters$ansi );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_acfc9bc0751f324dc3a67f294912c38c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_992e9240907bcf9687bf2ad96f1bdf0d;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_acfc9bc0751f324dc3a67f294912c38c = MAKE_MODULE_FRAME( codeobj_acfc9bc0751f324dc3a67f294912c38c, module_nbconvert$filters$ansi );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_acfc9bc0751f324dc3a67f294912c38c );
    assert( Py_REFCNT( frame_acfc9bc0751f324dc3a67f294912c38c ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_nbconvert$filters$ansi;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_acfc9bc0751f324dc3a67f294912c38c->m_frame.f_lineno = 6;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_jinja2;
        tmp_globals_name_2 = (PyObject *)moduledict_nbconvert$filters$ansi;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_acfc9bc0751f324dc3a67f294912c38c->m_frame.f_lineno = 7;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_jinja2, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = LIST_COPY( const_list_597d25cf901b12bc3e0fbe1d16fe702d_list );
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_acfc9bc0751f324dc3a67f294912c38c->m_frame.f_lineno = 15;
        tmp_assign_source_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_1706b8c60f579e7b4ae3dc31f09fc67b_tuple, 0 ) );

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_RE, tmp_assign_source_7 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_acfc9bc0751f324dc3a67f294912c38c );
#endif
    popFrameStack();

    assertFrameObject( frame_acfc9bc0751f324dc3a67f294912c38c );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_acfc9bc0751f324dc3a67f294912c38c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_acfc9bc0751f324dc3a67f294912c38c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_acfc9bc0751f324dc3a67f294912c38c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_acfc9bc0751f324dc3a67f294912c38c, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_tuple_afa2bede85b9840dbd4b659d0da9ce0f_tuple;
        UPDATE_STRING_DICT0( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ANSI_COLORS, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_1_strip_ansi(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_strip_ansi, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_2_ansi2html(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_ansi2html, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_3_ansi2latex(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain_ansi2latex, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_4__htmlconverter(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__htmlconverter, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_5__latexconverter(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__latexconverter, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_6__ansi2anything(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__ansi2anything, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_nbconvert$filters$ansi$$$function_7__get_extended_color(  );



        UPDATE_STRING_DICT1( moduledict_nbconvert$filters$ansi, (Nuitka_StringObject *)const_str_plain__get_extended_color, tmp_assign_source_15 );
    }

    return MOD_RETURN_VALUE( module_nbconvert$filters$ansi );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
