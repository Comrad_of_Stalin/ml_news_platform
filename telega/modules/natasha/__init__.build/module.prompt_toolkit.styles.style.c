/* Generated code for Python module 'prompt_toolkit.styles.style'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$styles$style" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$styles$style;
PyDictObject *moduledict_prompt_toolkit$styles$style;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_priority;
extern PyObject *const_str_plain_itertools;
static PyObject *const_str_plain_nohidden;
static PyObject *const_dict_60744952d45ce2f5f4698116992f779e;
static PyObject *const_str_digest_2a05c7d88909eec54fdd707adc7a3ca7;
static PyObject *const_dict_7da069136e18beec48fdb07c47426a46;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_87d99b725539b32c70b78c33a71d1e6b;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_str_plain__named_colors_lowercase;
static PyObject *const_str_digest_7f01f8a02212dab8d7b62920b65eb392;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_chr_93_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_digest_48a933953d719432d3a3e25030df4190;
extern PyObject *const_tuple_int_pos_3_int_pos_6_tuple;
extern PyObject *const_str_plain_i;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain__style;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_NAMED_COLORS_tuple;
static PyObject *const_tuple_str_digest_414e7c1a37ecdf47d805591c54eebf9e_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_styles_tuple;
static PyObject *const_str_digest_1f86dfbfc872b260c34f936393749341;
extern PyObject *const_str_plain_lstrip;
static PyObject *const_str_digest_1939094dcac36ef643a087fcbcf3f0f6;
extern PyObject *const_str_plain_default;
extern PyObject *const_str_plain_a;
static PyObject *const_str_digest_2a099de932416571369332a839857661;
extern PyObject *const_str_plain_items;
extern PyObject *const_int_neg_1;
extern PyObject *const_tuple_str_plain_s_tuple;
extern PyObject *const_str_plain_bold;
extern PyObject *const_tuple_str_chr_44_tuple;
static PyObject *const_str_digest_880d7c26405f7490c45b0304c064c26c;
extern PyObject *const_tuple_str_chr_35_tuple;
extern PyObject *const_str_plain_noitalic;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_97ef8aab1125d13cd336fe8e99d6c41c;
extern PyObject *const_slice_int_pos_3_none_none;
extern PyObject *const_str_plain_classmethod;
extern PyObject *const_str_plain_cls;
static PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_style_tuple;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_names;
static PyObject *const_str_digest_1407764f6b6711f9b3bc2282002e3660;
extern PyObject *const_tuple_str_plain_self_str_plain_style_str_str_plain_default_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain_reverse;
extern PyObject *const_str_plain_lower;
extern PyObject *const_str_plain_extend;
static PyObject *const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_new_name;
extern PyObject *const_str_plain_attrs;
extern PyObject *const_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8;
extern PyObject *const_str_plain_k;
static PyObject *const_dict_d0b57c53f671eb73f8d61fc118c758f9;
static PyObject *const_str_digest_c6b48af0f15c2ae1b16cff1d896cd5e2;
extern PyObject *const_str_plain_s;
extern PyObject *const_slice_int_0_int_pos_1_none;
extern PyObject *const_str_plain_add;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_dict_e6cb51b08e7677b8e331675852d6476e;
static PyObject *const_str_digest_fb78513695615235f1a6d5cf86686c09;
static PyObject *const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple;
extern PyObject *const_str_plain_hidden;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_tuple_str_plain_style_str_str_plain_attrs_str_plain_part_tuple;
extern PyObject *const_str_chr_44;
extern PyObject *const_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3;
static PyObject *const_str_digest_97d512a018c5d28ce3f196729552a02b;
static PyObject *const_str_plain__expand_classname;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_item;
extern PyObject *const_str_plain_c2;
extern PyObject *const_str_plain_from_dict;
extern PyObject *const_str_plain_p;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_style;
extern PyObject *const_str_chr_35;
static PyObject *const_tuple_str_plain_list_of_attrs_str_plain__or_tuple;
extern PyObject *const_str_plain_all;
static PyObject *const_str_digest_9e071caae90aa3b849c0b822a5a20520;
extern PyObject *const_str_plain_sans;
extern PyObject *const_str_chr_91;
extern PyObject *const_str_plain_nobold;
static PyObject *const_str_digest_39c92ff1ce0df0e177ad7ebcfc00940b;
extern PyObject *const_str_plain_parts;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_plain_get_attrs_for_style_str;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_203d51646015f8d807838193fada14f3;
static PyObject *const_str_plain_default_priority;
extern PyObject *const_str_plain_SimpleCache;
extern PyObject *const_tuple_str_plain_item_tuple;
extern PyObject *const_str_plain_append;
static PyObject *const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple;
static PyObject *const_tuple_str_digest_880d7c26405f7490c45b0304c064c26c_tuple;
static PyObject *const_str_digest_7303bc0c82644766e24f5f5df5b5795e;
static PyObject *const_str_plain_list_of_attrs;
extern PyObject *const_str_plain_Attrs;
extern PyObject *const_str_plain_Style;
extern PyObject *const_str_plain_style_rules;
extern PyObject *const_str_plain_endswith;
static PyObject *const_str_plain_inline_attrs;
extern PyObject *const_str_plain_attr;
static PyObject *const_str_digest_d11448a99d1fef69467026fbe80d70eb;
extern PyObject *const_str_plain_styles;
extern PyObject *const_str_plain_compile;
extern PyObject *const_str_plain_parse_color;
static PyObject *const_tuple_str_plain_Priority_tuple_empty_tuple;
static PyObject *const_tuple_str_plain_styles_tuple;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_match;
static PyObject *const_str_digest_c7bcb3bb1990002fb3110ff9c8be69ea;
extern PyObject *const_str_plain__replace;
extern PyObject *const_str_plain_underline;
extern PyObject *const_str_plain_False;
static PyObject *const_str_plain_noblink;
extern PyObject *const_tuple_str_chr_91_tuple;
extern PyObject *const_tuple_str_empty_tuple;
extern PyObject *const_str_plain_BaseStyle;
static PyObject *const_str_digest_887fd040eff1b3854508a5fdf7741541;
static PyObject *const_str_plain_noreverse;
extern PyObject *const_tuple_str_plain_SimpleCache_tuple;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_digest_d77b3a90020c5798da9795100e0e0473;
extern PyObject *const_str_digest_af79046d82e88e002b260ea4f0738b6b;
static PyObject *const_dict_ac127549b4c984c0ed1fb132945dcabf;
static PyObject *const_dict_fd78d4f01deb8812307e791ecafb5d29;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_bgcolor;
extern PyObject *const_str_plain_classname;
static PyObject *const_str_digest_0383abc0192e69ed29885314af8deab7;
extern PyObject *const_dict_9cebe5e8e339af1b92c19ff6455c60db;
extern PyObject *const_str_plain_count;
extern PyObject *const_str_plain_col;
static PyObject *const_str_digest_30f57ccd20ef5453da9844c1efba42ea;
extern PyObject *const_int_0;
static PyObject *const_str_digest_dd962ba54e036373b889cc52e17b49e8;
static PyObject *const_str_plain__merge_attrs;
extern PyObject *const_str_plain_Priority;
extern PyObject *const_str_plain_maxsize;
extern PyObject *const_str_plain_text;
static PyObject *const_list_9b4875dc865751f38c5731f75997792e_list;
static PyObject *const_tuple_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8_tuple;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain__MergedStyle;
extern PyObject *const_str_plain_merge_styles;
static PyObject *const_str_plain_new_class_names;
extern PyObject *const_str_plain_part;
extern PyObject *const_str_plain_mono;
extern PyObject *const_str_plain_nounderline;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_tuple_str_plain_self_str_plain_get_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain__merged_style;
static PyObject *const_dict_83d97a7aa9a123f1f0800dc984e479e1;
extern PyObject *const_str_plain_DEFAULT_ATTRS;
static PyObject *const_str_digest_a7c604da0f3a8583a8bd8e40bef640e0;
extern PyObject *const_str_plain_base;
static PyObject *const_str_plain_KEY_ORDER;
static PyObject *const_str_plain_MOST_PRECISE;
static PyObject *const_str_plain_CLASS_NAMES_RE;
static PyObject *const_str_plain_style_dict;
extern PyObject *const_str_plain_italic;
extern PyObject *const_tuple_b0c00933e83a0151953f3a00c2178204_tuple;
static PyObject *const_dict_60a5a3572ac01fb72bdec3862d78bf39;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_combinations;
extern PyObject *const_str_plain__ALL;
static PyObject *const_str_plain__EMPTY_ATTRS;
extern PyObject *const_str_plain_invalidation_hash;
static PyObject *const_str_digest_03c6720c104c9f7b2ed5075c12663074;
static PyObject *const_dict_77f39e8969719f41e72e5ef06d7a7af8;
extern PyObject *const_str_chr_93;
extern PyObject *const_str_plain_color;
static PyObject *const_str_digest_36489a3e172a2dc3f6a601c884daff7e;
extern PyObject *const_slice_none_none_int_neg_1;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_ANSI_COLOR_NAMES;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_digest_414e7c1a37ecdf47d805591c54eebf9e;
extern PyObject *const_str_plain_noinherit;
static PyObject *const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple;
static PyObject *const_str_plain__or;
extern PyObject *const_tuple_str_empty_str_plain_default_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_digest_fe2155e54b379def61c84e8893a851a7;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_tuple_str_plain_values_str_plain_v_tuple;
static PyObject *const_str_plain_DICT_KEY_ORDER;
extern PyObject *const_str_plain_style_str;
static PyObject *const_tuple_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3_tuple;
static PyObject *const_tuple_str_plain_roman_str_plain_sans_str_plain_mono_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_values;
extern PyObject *const_str_digest_7b2e486efb24f89284503a5b81c960c1;
extern PyObject *const_dict_20f16b30e1951d468c3080dfc46b8748;
extern PyObject *const_str_plain_named_colors;
static PyObject *const_str_plain__parse_style_str;
static PyObject *const_str_plain_class_names;
extern PyObject *const_str_plain_key;
extern PyObject *const_slice_int_pos_6_none_none;
static PyObject *const_str_plain__style_rules;
static PyObject *const_dict_8ce99e3cf979f423b4467cc99c456ab9;
extern PyObject *const_tuple_str_plain_text_str_plain_col_tuple;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_NAMED_COLORS;
static PyObject *const_dict_998421227eff0edc6c9497483c1109a1;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
static PyObject *const_dict_cd71fca483cf1c9e989c58412c8cfbab;
static PyObject *const_str_digest_0dbb553e8fbc140d7df3be1eb49afac9;
static PyObject *const_tuple_str_plain_self_str_plain_style_rules_str_plain_s_tuple;
static PyObject *const_str_plain_class_names_and_attrs;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_e8819e97da12c4a29728cc28ea343534;
static PyObject *const_str_digest_249676a808dd31d5f4bc74f3fab6177b;
static PyObject *const_tuple_str_digest_7b2e486efb24f89284503a5b81c960c1_tuple;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_tuple_str_plain_a_tuple;
extern PyObject *const_str_plain_ANSI_COLOR_NAMES_ALIASES;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_roman;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_combos;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_12a998220967f141b9b43b9a76571d53;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_empty;
static PyObject *const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple;
extern PyObject *const_str_plain_blink;
static PyObject *const_str_digest_93bf0b002207c69e0706ac364311f835;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_s_tuple;
static PyObject *const_str_digest_932a3e82dba3b9bafdbe544436b2bd07;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_nohidden = UNSTREAM_STRING_ASCII( &constant_bin[ 4864694 ], 8, 1 );
    const_dict_60744952d45ce2f5f4698116992f779e = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_60744952d45ce2f5f4698116992f779e, const_str_plain_italic, Py_False );
    assert( PyDict_Size( const_dict_60744952d45ce2f5f4698116992f779e ) == 1 );
    const_str_digest_2a05c7d88909eec54fdd707adc7a3ca7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4864702 ], 39, 0 );
    const_dict_7da069136e18beec48fdb07c47426a46 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_7da069136e18beec48fdb07c47426a46, const_str_plain_blink, Py_True );
    assert( PyDict_Size( const_dict_7da069136e18beec48fdb07c47426a46 ) == 1 );
    const_str_digest_87d99b725539b32c70b78c33a71d1e6b = UNSTREAM_STRING_ASCII( &constant_bin[ 4864741 ], 49, 0 );
    const_str_plain__named_colors_lowercase = UNSTREAM_STRING_ASCII( &constant_bin[ 4864790 ], 23, 1 );
    const_str_digest_7f01f8a02212dab8d7b62920b65eb392 = UNSTREAM_STRING_ASCII( &constant_bin[ 4864813 ], 27, 0 );
    const_str_digest_48a933953d719432d3a3e25030df4190 = UNSTREAM_STRING_ASCII( &constant_bin[ 4853917 ], 23, 0 );
    const_tuple_str_digest_414e7c1a37ecdf47d805591c54eebf9e_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_414e7c1a37ecdf47d805591c54eebf9e_tuple, 0, const_str_digest_414e7c1a37ecdf47d805591c54eebf9e ); Py_INCREF( const_str_digest_414e7c1a37ecdf47d805591c54eebf9e );
    const_tuple_str_plain_self_str_plain_styles_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_styles_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_styles_tuple, 1, const_str_plain_styles ); Py_INCREF( const_str_plain_styles );
    const_str_digest_1f86dfbfc872b260c34f936393749341 = UNSTREAM_STRING_ASCII( &constant_bin[ 4864840 ], 49, 0 );
    const_str_digest_1939094dcac36ef643a087fcbcf3f0f6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4864889 ], 36, 0 );
    const_str_digest_2a099de932416571369332a839857661 = UNSTREAM_STRING_ASCII( &constant_bin[ 4864925 ], 28, 0 );
    const_str_digest_880d7c26405f7490c45b0304c064c26c = UNSTREAM_STRING_ASCII( &constant_bin[ 4864953 ], 16, 0 );
    const_str_digest_97ef8aab1125d13cd336fe8e99d6c41c = UNSTREAM_STRING_ASCII( &constant_bin[ 4864969 ], 41, 0 );
    const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_style_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_style_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_style_tuple, 1, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    const_str_digest_1407764f6b6711f9b3bc2282002e3660 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865010 ], 100, 0 );
    const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple, 0, const_str_plain_BaseStyle ); Py_INCREF( const_str_plain_BaseStyle );
    PyTuple_SET_ITEM( const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple, 1, const_str_plain_DEFAULT_ATTRS ); Py_INCREF( const_str_plain_DEFAULT_ATTRS );
    PyTuple_SET_ITEM( const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple, 2, const_str_plain_ANSI_COLOR_NAMES ); Py_INCREF( const_str_plain_ANSI_COLOR_NAMES );
    PyTuple_SET_ITEM( const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple, 3, const_str_plain_ANSI_COLOR_NAMES_ALIASES ); Py_INCREF( const_str_plain_ANSI_COLOR_NAMES_ALIASES );
    PyTuple_SET_ITEM( const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple, 4, const_str_plain_Attrs ); Py_INCREF( const_str_plain_Attrs );
    const_dict_d0b57c53f671eb73f8d61fc118c758f9 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_d0b57c53f671eb73f8d61fc118c758f9, const_str_plain_reverse, Py_False );
    assert( PyDict_Size( const_dict_d0b57c53f671eb73f8d61fc118c758f9 ) == 1 );
    const_str_digest_c6b48af0f15c2ae1b16cff1d896cd5e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2195725 ], 14, 0 );
    const_dict_e6cb51b08e7677b8e331675852d6476e = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_e6cb51b08e7677b8e331675852d6476e, const_str_plain_blink, Py_False );
    assert( PyDict_Size( const_dict_e6cb51b08e7677b8e331675852d6476e ) == 1 );
    const_str_digest_fb78513695615235f1a6d5cf86686c09 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865110 ], 97, 0 );
    const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 1, const_str_plain_style_str ); Py_INCREF( const_str_plain_style_str );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 2, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_str_plain_list_of_attrs = UNSTREAM_STRING_ASCII( &constant_bin[ 4865207 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 3, const_str_plain_list_of_attrs ); Py_INCREF( const_str_plain_list_of_attrs );
    const_str_plain_class_names = UNSTREAM_STRING_ASCII( &constant_bin[ 4865220 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 4, const_str_plain_class_names ); Py_INCREF( const_str_plain_class_names );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 5, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 6, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 7, const_str_plain_part ); Py_INCREF( const_str_plain_part );
    const_str_plain_new_class_names = UNSTREAM_STRING_ASCII( &constant_bin[ 4865231 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 8, const_str_plain_new_class_names ); Py_INCREF( const_str_plain_new_class_names );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 9, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 10, const_str_plain_new_name ); Py_INCREF( const_str_plain_new_name );
    const_str_plain_combos = UNSTREAM_STRING_ASCII( &constant_bin[ 4865246 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 11, const_str_plain_combos ); Py_INCREF( const_str_plain_combos );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 12, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 13, const_str_plain_c2 ); Py_INCREF( const_str_plain_c2 );
    const_str_plain_inline_attrs = UNSTREAM_STRING_ASCII( &constant_bin[ 4865252 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 14, const_str_plain_inline_attrs ); Py_INCREF( const_str_plain_inline_attrs );
    const_tuple_str_plain_style_str_str_plain_attrs_str_plain_part_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_style_str_str_plain_attrs_str_plain_part_tuple, 0, const_str_plain_style_str ); Py_INCREF( const_str_plain_style_str );
    PyTuple_SET_ITEM( const_tuple_str_plain_style_str_str_plain_attrs_str_plain_part_tuple, 1, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_str_plain_style_str_str_plain_attrs_str_plain_part_tuple, 2, const_str_plain_part ); Py_INCREF( const_str_plain_part );
    const_str_digest_97d512a018c5d28ce3f196729552a02b = UNSTREAM_STRING_ASCII( &constant_bin[ 4865264 ], 40, 0 );
    const_str_plain__expand_classname = UNSTREAM_STRING_ASCII( &constant_bin[ 4865304 ], 17, 1 );
    const_tuple_str_plain_list_of_attrs_str_plain__or_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_list_of_attrs_str_plain__or_tuple, 0, const_str_plain_list_of_attrs ); Py_INCREF( const_str_plain_list_of_attrs );
    const_str_plain__or = UNSTREAM_STRING_ASCII( &constant_bin[ 55248 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_list_of_attrs_str_plain__or_tuple, 1, const_str_plain__or ); Py_INCREF( const_str_plain__or );
    const_str_digest_9e071caae90aa3b849c0b822a5a20520 = UNSTREAM_STRING_ASCII( &constant_bin[ 4853893 ], 17, 0 );
    const_str_digest_39c92ff1ce0df0e177ad7ebcfc00940b = UNSTREAM_STRING_ASCII( &constant_bin[ 4865264 ], 21, 0 );
    const_str_digest_203d51646015f8d807838193fada14f3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865321 ], 24, 0 );
    const_str_plain_default_priority = UNSTREAM_STRING_ASCII( &constant_bin[ 4865345 ], 16, 1 );
    const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple, 0, const_str_plain_classname ); Py_INCREF( const_str_plain_classname );
    PyTuple_SET_ITEM( const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple, 1, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple, 2, const_str_plain_parts ); Py_INCREF( const_str_plain_parts );
    PyTuple_SET_ITEM( const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple, 3, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_tuple_str_digest_880d7c26405f7490c45b0304c064c26c_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_880d7c26405f7490c45b0304c064c26c_tuple, 0, const_str_digest_880d7c26405f7490c45b0304c064c26c ); Py_INCREF( const_str_digest_880d7c26405f7490c45b0304c064c26c );
    const_str_digest_7303bc0c82644766e24f5f5df5b5795e = UNSTREAM_STRING_ASCII( &constant_bin[ 4851943 ], 15, 0 );
    const_str_digest_d11448a99d1fef69467026fbe80d70eb = UNSTREAM_STRING_ASCII( &constant_bin[ 4865361 ], 47, 0 );
    const_tuple_str_plain_Priority_tuple_empty_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Priority_tuple_empty_tuple, 0, const_str_plain_Priority ); Py_INCREF( const_str_plain_Priority );
    PyTuple_SET_ITEM( const_tuple_str_plain_Priority_tuple_empty_tuple, 1, const_tuple_empty ); Py_INCREF( const_tuple_empty );
    const_tuple_str_plain_styles_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_styles_tuple, 0, const_str_plain_styles ); Py_INCREF( const_str_plain_styles );
    const_str_digest_c7bcb3bb1990002fb3110ff9c8be69ea = UNSTREAM_STRING_ASCII( &constant_bin[ 4864702 ], 26, 0 );
    const_str_plain_noblink = UNSTREAM_STRING_ASCII( &constant_bin[ 4857590 ], 7, 1 );
    const_str_digest_887fd040eff1b3854508a5fdf7741541 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865408 ], 63, 0 );
    const_str_plain_noreverse = UNSTREAM_STRING_ASCII( &constant_bin[ 4857559 ], 9, 1 );
    const_str_digest_d77b3a90020c5798da9795100e0e0473 = UNSTREAM_STRING_ASCII( &constant_bin[ 4864840 ], 30, 0 );
    const_dict_ac127549b4c984c0ed1fb132945dcabf = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_ac127549b4c984c0ed1fb132945dcabf, const_str_plain_bold, Py_True );
    assert( PyDict_Size( const_dict_ac127549b4c984c0ed1fb132945dcabf ) == 1 );
    const_dict_fd78d4f01deb8812307e791ecafb5d29 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_fd78d4f01deb8812307e791ecafb5d29, const_str_plain_italic, Py_True );
    assert( PyDict_Size( const_dict_fd78d4f01deb8812307e791ecafb5d29 ) == 1 );
    const_str_digest_0383abc0192e69ed29885314af8deab7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865471 ], 57, 0 );
    const_str_digest_30f57ccd20ef5453da9844c1efba42ea = UNSTREAM_STRING_ASCII( &constant_bin[ 4865528 ], 193, 0 );
    const_str_digest_dd962ba54e036373b889cc52e17b49e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865721 ], 133, 0 );
    const_str_plain__merge_attrs = UNSTREAM_STRING_ASCII( &constant_bin[ 4865854 ], 12, 1 );
    const_list_9b4875dc865751f38c5731f75997792e_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_9b4875dc865751f38c5731f75997792e_list, 0, const_str_plain_Style ); Py_INCREF( const_str_plain_Style );
    PyList_SET_ITEM( const_list_9b4875dc865751f38c5731f75997792e_list, 1, const_str_plain_parse_color ); Py_INCREF( const_str_plain_parse_color );
    PyList_SET_ITEM( const_list_9b4875dc865751f38c5731f75997792e_list, 2, const_str_plain_Priority ); Py_INCREF( const_str_plain_Priority );
    PyList_SET_ITEM( const_list_9b4875dc865751f38c5731f75997792e_list, 3, const_str_plain_merge_styles ); Py_INCREF( const_str_plain_merge_styles );
    const_tuple_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8_tuple, 0, const_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8 ); Py_INCREF( const_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8 );
    const_str_plain__MergedStyle = UNSTREAM_STRING_ASCII( &constant_bin[ 4854671 ], 12, 1 );
    const_dict_83d97a7aa9a123f1f0800dc984e479e1 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_83d97a7aa9a123f1f0800dc984e479e1, const_str_plain_underline, Py_False );
    assert( PyDict_Size( const_dict_83d97a7aa9a123f1f0800dc984e479e1 ) == 1 );
    const_str_digest_a7c604da0f3a8583a8bd8e40bef640e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865866 ], 30, 0 );
    const_str_plain_KEY_ORDER = UNSTREAM_STRING_ASCII( &constant_bin[ 4865896 ], 9, 1 );
    const_str_plain_MOST_PRECISE = UNSTREAM_STRING_ASCII( &constant_bin[ 4865905 ], 12, 1 );
    const_str_plain_CLASS_NAMES_RE = UNSTREAM_STRING_ASCII( &constant_bin[ 4865917 ], 14, 1 );
    const_str_plain_style_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 2210967 ], 10, 1 );
    const_dict_60a5a3572ac01fb72bdec3862d78bf39 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_60a5a3572ac01fb72bdec3862d78bf39, const_str_plain_underline, Py_True );
    assert( PyDict_Size( const_dict_60a5a3572ac01fb72bdec3862d78bf39 ) == 1 );
    const_str_plain__EMPTY_ATTRS = UNSTREAM_STRING_ASCII( &constant_bin[ 4865931 ], 12, 1 );
    const_str_digest_03c6720c104c9f7b2ed5075c12663074 = UNSTREAM_STRING_ASCII( &constant_bin[ 4865943 ], 712, 0 );
    const_dict_77f39e8969719f41e72e5ef06d7a7af8 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_77f39e8969719f41e72e5ef06d7a7af8, const_str_plain_bold, Py_False );
    assert( PyDict_Size( const_dict_77f39e8969719f41e72e5ef06d7a7af8 ) == 1 );
    const_str_digest_36489a3e172a2dc3f6a601c884daff7e = UNSTREAM_STRING_ASCII( &constant_bin[ 4866655 ], 21, 0 );
    const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 1, const_str_plain_style_rules ); Py_INCREF( const_str_plain_style_rules );
    const_str_plain_class_names_and_attrs = UNSTREAM_STRING_ASCII( &constant_bin[ 4866676 ], 21, 1 );
    PyTuple_SET_ITEM( const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 2, const_str_plain_class_names_and_attrs ); Py_INCREF( const_str_plain_class_names_and_attrs );
    PyTuple_SET_ITEM( const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 3, const_str_plain_class_names ); Py_INCREF( const_str_plain_class_names );
    PyTuple_SET_ITEM( const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 4, const_str_plain_style_str ); Py_INCREF( const_str_plain_style_str );
    PyTuple_SET_ITEM( const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 5, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    const_str_digest_fe2155e54b379def61c84e8893a851a7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4854264 ], 29, 0 );
    const_tuple_str_plain_values_str_plain_v_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_values_str_plain_v_tuple, 0, const_str_plain_values ); Py_INCREF( const_str_plain_values );
    PyTuple_SET_ITEM( const_tuple_str_plain_values_str_plain_v_tuple, 1, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    const_str_plain_DICT_KEY_ORDER = UNSTREAM_STRING_ASCII( &constant_bin[ 4866289 ], 14, 1 );
    const_tuple_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3_tuple, 0, const_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3 ); Py_INCREF( const_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3 );
    const_tuple_str_plain_roman_str_plain_sans_str_plain_mono_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_roman_str_plain_sans_str_plain_mono_tuple, 0, const_str_plain_roman ); Py_INCREF( const_str_plain_roman );
    PyTuple_SET_ITEM( const_tuple_str_plain_roman_str_plain_sans_str_plain_mono_tuple, 1, const_str_plain_sans ); Py_INCREF( const_str_plain_sans );
    PyTuple_SET_ITEM( const_tuple_str_plain_roman_str_plain_sans_str_plain_mono_tuple, 2, const_str_plain_mono ); Py_INCREF( const_str_plain_mono );
    const_str_plain__parse_style_str = UNSTREAM_STRING_ASCII( &constant_bin[ 4866697 ], 16, 1 );
    const_str_plain__style_rules = UNSTREAM_STRING_ASCII( &constant_bin[ 4866713 ], 12, 1 );
    const_dict_8ce99e3cf979f423b4467cc99c456ab9 = _PyDict_NewPresized( 8 );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_color, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_bgcolor, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_bold, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_underline, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_italic, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_blink, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_reverse, Py_None );
    PyDict_SetItem( const_dict_8ce99e3cf979f423b4467cc99c456ab9, const_str_plain_hidden, Py_None );
    assert( PyDict_Size( const_dict_8ce99e3cf979f423b4467cc99c456ab9 ) == 8 );
    const_dict_998421227eff0edc6c9497483c1109a1 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_998421227eff0edc6c9497483c1109a1, const_str_plain_hidden, Py_False );
    assert( PyDict_Size( const_dict_998421227eff0edc6c9497483c1109a1 ) == 1 );
    const_dict_cd71fca483cf1c9e989c58412c8cfbab = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_cd71fca483cf1c9e989c58412c8cfbab, const_str_plain_hidden, Py_True );
    assert( PyDict_Size( const_dict_cd71fca483cf1c9e989c58412c8cfbab ) == 1 );
    const_str_digest_0dbb553e8fbc140d7df3be1eb49afac9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4866725 ], 45, 0 );
    const_tuple_str_plain_self_str_plain_style_rules_str_plain_s_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_style_rules_str_plain_s_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_style_rules_str_plain_s_tuple, 1, const_str_plain_style_rules ); Py_INCREF( const_str_plain_style_rules );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_style_rules_str_plain_s_tuple, 2, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    const_str_digest_e8819e97da12c4a29728cc28ea343534 = UNSTREAM_STRING_ASCII( &constant_bin[ 4866770 ], 652, 0 );
    const_str_digest_249676a808dd31d5f4bc74f3fab6177b = UNSTREAM_STRING_ASCII( &constant_bin[ 4867422 ], 25, 0 );
    const_tuple_str_digest_7b2e486efb24f89284503a5b81c960c1_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_7b2e486efb24f89284503a5b81c960c1_tuple, 0, const_str_digest_7b2e486efb24f89284503a5b81c960c1 ); Py_INCREF( const_str_digest_7b2e486efb24f89284503a5b81c960c1 );
    const_str_digest_12a998220967f141b9b43b9a76571d53 = UNSTREAM_STRING_ASCII( &constant_bin[ 4867447 ], 36, 0 );
    const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple, 1, const_str_plain_style_dict ); Py_INCREF( const_str_plain_style_dict );
    PyTuple_SET_ITEM( const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple, 2, const_str_plain_priority ); Py_INCREF( const_str_plain_priority );
    PyTuple_SET_ITEM( const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple, 3, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    const_str_digest_93bf0b002207c69e0706ac364311f835 = UNSTREAM_STRING_ASCII( &constant_bin[ 4867483 ], 167, 0 );
    const_str_digest_932a3e82dba3b9bafdbe544436b2bd07 = UNSTREAM_STRING_ASCII( &constant_bin[ 4867650 ], 160, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$styles$style( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_53470a7a2592edfac0b1898a0f096b92;
static PyCodeObject *codeobj_34861fad7ab14ff6965da1bb7b704d3a;
static PyCodeObject *codeobj_5e1fa19d313cfe381aa6dba097b1d284;
static PyCodeObject *codeobj_13075177ec6ed53f119134c2dd701cef;
static PyCodeObject *codeobj_e97f894cee234ee49bd77eda907bc49f;
static PyCodeObject *codeobj_21282692aeb5fcf488820c1ff22e5765;
static PyCodeObject *codeobj_b4b001164f03a3033d420cdf09338ea9;
static PyCodeObject *codeobj_baf9490416e1e63c9e5694e9c0b9c318;
static PyCodeObject *codeobj_81191b2c28d26781ff9f891146f1b5e8;
static PyCodeObject *codeobj_634b8a7a2963086040aad95fc83f076b;
static PyCodeObject *codeobj_1cc926fb2d7eec06c400f6f61016c5ac;
static PyCodeObject *codeobj_6c860282339a3115d86b5bb5c4691800;
static PyCodeObject *codeobj_fadc48548147d75e6d5a8d3f7f9ae15a;
static PyCodeObject *codeobj_a15d48e1ef886f9277cadf9c3fb2034e;
static PyCodeObject *codeobj_0a71b46f9cd9d9d57f0972b1b7f13fda;
static PyCodeObject *codeobj_ced792830c6b3aa30a9736212caeb0ed;
static PyCodeObject *codeobj_3307939780071f890ee385f4804005da;
static PyCodeObject *codeobj_297839a72227d66dabefa647f05bd6a6;
static PyCodeObject *codeobj_f3562bd77c1aec8c9508f6fabcf4263c;
static PyCodeObject *codeobj_cfc5159abc4115925f0d1731e506f4a0;
static PyCodeObject *codeobj_6a637ac17fcf2384c9063cb586fde7d3;
static PyCodeObject *codeobj_e9dd8c8eab0cd0a25c2926d6e167cbbc;
static PyCodeObject *codeobj_efe509c90dd5bac6257972e3587c609f;
static PyCodeObject *codeobj_d4f69ab891d763c49c51182a5edf512b;
static PyCodeObject *codeobj_bf9ec9bffa6966be9cdb21ecb7bba484;
static PyCodeObject *codeobj_50093a0c2c2673fa118f6ebc5d4d4282;
static PyCodeObject *codeobj_1a6ee3024f37feb13a005e47a16e621b;
static PyCodeObject *codeobj_46343e520769db6726bd3f118076dfab;
static PyCodeObject *codeobj_c59078e4979f48e64e8e3b91755632f0;
static PyCodeObject *codeobj_c8dec6dabe0b6ae8ddc8c1fbbcad346d;
static PyCodeObject *codeobj_480159fc265e16f5844c0552e15bd18f;
static PyCodeObject *codeobj_ff1ef7f2fa661a5079acef48c017cd49;
static PyCodeObject *codeobj_1cbf91231e6347669238443f05030aa3;
static PyCodeObject *codeobj_499daadb8d1c0bc8c11425a0b9d1d69a;
static PyCodeObject *codeobj_dac7c133e5149ac106fa2011c3316895;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_a7c604da0f3a8583a8bd8e40bef640e0 );
    codeobj_53470a7a2592edfac0b1898a0f096b92 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 245, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_34861fad7ab14ff6965da1bb7b704d3a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 20, const_tuple_b0c00933e83a0151953f3a00c2178204_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5e1fa19d313cfe381aa6dba097b1d284 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 372, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_s_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_13075177ec6ed53f119134c2dd701cef = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 349, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_style_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e97f894cee234ee49bd77eda907bc49f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 315, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_21282692aeb5fcf488820c1ff22e5765 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 316, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b4b001164f03a3033d420cdf09338ea9 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 317, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_baf9490416e1e63c9e5694e9c0b9c318 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 318, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_81191b2c28d26781ff9f891146f1b5e8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 319, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_634b8a7a2963086040aad95fc83f076b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 320, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1cc926fb2d7eec06c400f6f61016c5ac = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 321, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6c860282339a3115d86b5bb5c4691800 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 322, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_fadc48548147d75e6d5a8d3f7f9ae15a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 329, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a15d48e1ef886f9277cadf9c3fb2034e = MAKE_CODEOBJ( module_filename_obj, const_str_digest_1939094dcac36ef643a087fcbcf3f0f6, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0a71b46f9cd9d9d57f0972b1b7f13fda = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Priority, 157, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ced792830c6b3aa30a9736212caeb0ed = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Style, 189, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3307939780071f890ee385f4804005da = MAKE_CODEOBJ( module_filename_obj, const_str_plain__MergedStyle, 333, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_297839a72227d66dabefa647f05bd6a6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 210, const_tuple_ff4ffd8ccc39994c3343eaac41f3c359_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f3562bd77c1aec8c9508f6fabcf4263c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 348, const_tuple_str_plain_self_str_plain_styles_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_cfc5159abc4115925f0d1731e506f4a0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__expand_classname, 75, const_tuple_012cff8275b254c6513f2eb7dc91b9a3_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6a637ac17fcf2384c9063cb586fde7d3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__merge_attrs, 302, const_tuple_str_plain_list_of_attrs_str_plain__or_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e9dd8c8eab0cd0a25c2926d6e167cbbc = MAKE_CODEOBJ( module_filename_obj, const_str_plain__merged_style, 354, const_tuple_str_plain_self_str_plain_get_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_efe509c90dd5bac6257972e3587c609f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__or, 308, const_tuple_str_plain_values_str_plain_v_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d4f69ab891d763c49c51182a5edf512b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__parse_style_str, 90, const_tuple_str_plain_style_str_str_plain_attrs_str_plain_part_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bf9ec9bffa6966be9cdb21ecb7bba484 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_from_dict, 234, const_tuple_78c63e83115b175de7d6a05f1e5c2bea_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_50093a0c2c2673fa118f6ebc5d4d4282 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get, 357, const_tuple_str_plain_self_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1a6ee3024f37feb13a005e47a16e621b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_attrs_for_style_str, 368, const_tuple_str_plain_self_str_plain_style_str_str_plain_default_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_46343e520769db6726bd3f118076dfab = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_attrs_for_style_str, 251, const_tuple_8dd03d1c62c167e48abd4c88a8fba963_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c59078e4979f48e64e8e3b91755632f0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_invalidation_hash, 298, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c8dec6dabe0b6ae8ddc8c1fbbcad346d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_invalidation_hash, 371, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_480159fc265e16f5844c0552e15bd18f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_key, 243, const_tuple_str_plain_item_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ff1ef7f2fa661a5079acef48c017cd49 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_merge_styles, 325, const_tuple_str_plain_styles_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1cbf91231e6347669238443f05030aa3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse_color, 23, const_tuple_str_plain_text_str_plain_col_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_499daadb8d1c0bc8c11425a0b9d1d69a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_style_rules, 230, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dac7c133e5149ac106fa2011c3316895 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_style_rules, 361, const_tuple_str_plain_self_str_plain_style_rules_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *prompt_toolkit$styles$style$$$genexpr_1_genexpr_maker( void );


static PyObject *prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_maker( void );


static PyObject *prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_maker( void );


static PyObject *prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_5_complex_call_helper_pos_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_10_merge_styles(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_11___init__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style$$$function_1_get(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_13_style_rules(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_15_invalidation_hash(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_1_parse_color(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_2__expand_classname(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_3__parse_style_str(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_4___init__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_5_style_rules(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_8_invalidation_hash(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_9__merge_attrs(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or(  );


// The module function definitions.

struct prompt_toolkit$styles$style$$$genexpr_1_genexpr_locals {
    PyObject *var_k;
    PyObject *var_v;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *prompt_toolkit$styles$style$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$styles$style$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$styles$style$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_k = NULL;
    generator_heap->var_v = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_34861fad7ab14ff6965da1bb7b704d3a, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noo";
                generator_heap->exception_lineno = 20;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 20;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 20;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 20;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Noo";
                    generator_heap->exception_lineno = 20;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 20;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_k;
            generator_heap->var_k = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_v;
            generator_heap->var_v = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_k );
        tmp_called_instance_1 = generator_heap->var_k;
        generator->m_frame->m_frame.f_lineno = 20;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_lower );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 20;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_v );
        tmp_called_instance_2 = generator_heap->var_v;
        generator->m_frame->m_frame.f_lineno = 20;
        tmp_tuple_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_lstrip, &PyTuple_GET_ITEM( const_tuple_str_chr_35_tuple, 0 ) );

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 20;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_called_instance_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 20;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 20;
        generator_heap->type_description_1 = "Noo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_k,
            generator_heap->var_v
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$styles$style$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$styles$style$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$styles$style,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_34861fad7ab14ff6965da1bb7b704d3a,
        1,
        sizeof(struct prompt_toolkit$styles$style$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_1_parse_color( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *var_col = NULL;
    struct Nuitka_FrameObject *frame_1cbf91231e6347669238443f05030aa3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_1cbf91231e6347669238443f05030aa3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1cbf91231e6347669238443f05030aa3, codeobj_1cbf91231e6347669238443f05030aa3, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    frame_1cbf91231e6347669238443f05030aa3 = cache_frame_1cbf91231e6347669238443f05030aa3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1cbf91231e6347669238443f05030aa3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1cbf91231e6347669238443f05030aa3 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_text );
        tmp_compexpr_left_1 = par_text;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_COLOR_NAMES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_text );
        tmp_return_value = par_text;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_text );
        tmp_compexpr_left_2 = par_text;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_COLOR_NAMES_ALIASES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_2 = tmp_mvar_value_2;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_subscript_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_COLOR_NAMES_ALIASES" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 34;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_3;
            CHECK_OBJECT( par_text );
            tmp_subscript_name_1 = par_text;
            tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    // Tried code:
    {
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_called_instance_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__named_colors_lowercase );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__named_colors_lowercase );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_named_colors_lowercase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_4;
        CHECK_OBJECT( par_text );
        tmp_called_instance_1 = par_text;
        frame_1cbf91231e6347669238443f05030aa3->m_frame.f_lineno = 39;
        tmp_subscript_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_lower );
        if ( tmp_subscript_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscript_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_1_parse_color );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_1cbf91231e6347669238443f05030aa3, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_1cbf91231e6347669238443f05030aa3, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_3 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 37;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_1cbf91231e6347669238443f05030aa3->m_frame) frame_1cbf91231e6347669238443f05030aa3->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_3;
        branch_no_3:;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_1_parse_color );
    return NULL;
    // End of try:
    try_end_2:;
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( par_text );
        tmp_subscribed_name_3 = par_text;
        tmp_subscript_name_3 = const_slice_int_0_int_pos_1_none;
        tmp_compexpr_left_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_str_chr_35;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            CHECK_OBJECT( par_text );
            tmp_subscribed_name_4 = par_text;
            tmp_subscript_name_4 = const_slice_int_pos_1_none_none;
            tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_col == NULL );
            var_col = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_mvar_value_5;
            CHECK_OBJECT( var_col );
            tmp_compexpr_left_5 = var_col;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_COLOR_NAMES" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 49;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_right_5 = tmp_mvar_value_5;
            tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            CHECK_OBJECT( var_col );
            tmp_return_value = var_col;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_5;
            branch_no_5:;
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_6;
                PyObject *tmp_compexpr_right_6;
                PyObject *tmp_mvar_value_6;
                CHECK_OBJECT( var_col );
                tmp_compexpr_left_6 = var_col;
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_COLOR_NAMES_ALIASES" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 51;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }

                tmp_compexpr_right_6 = tmp_mvar_value_6;
                tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 51;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_subscribed_name_5;
                    PyObject *tmp_mvar_value_7;
                    PyObject *tmp_subscript_name_5;
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );

                    if (unlikely( tmp_mvar_value_7 == NULL ))
                    {
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES );
                    }

                    if ( tmp_mvar_value_7 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI_COLOR_NAMES_ALIASES" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 52;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }

                    tmp_subscribed_name_5 = tmp_mvar_value_7;
                    CHECK_OBJECT( var_col );
                    tmp_subscript_name_5 = var_col;
                    tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 52;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                goto branch_end_6;
                branch_no_6:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_7;
                    PyObject *tmp_compexpr_right_7;
                    PyObject *tmp_len_arg_1;
                    CHECK_OBJECT( var_col );
                    tmp_len_arg_1 = var_col;
                    tmp_compexpr_left_7 = BUILTIN_LEN( tmp_len_arg_1 );
                    if ( tmp_compexpr_left_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 55;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_7 = const_int_pos_6;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                    Py_DECREF( tmp_compexpr_left_7 );
                    assert( !(tmp_res == -1) );
                    tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    CHECK_OBJECT( var_col );
                    tmp_return_value = var_col;
                    Py_INCREF( tmp_return_value );
                    goto frame_return_exit_1;
                    goto branch_end_7;
                    branch_no_7:;
                    {
                        nuitka_bool tmp_condition_result_8;
                        PyObject *tmp_compexpr_left_8;
                        PyObject *tmp_compexpr_right_8;
                        PyObject *tmp_len_arg_2;
                        CHECK_OBJECT( var_col );
                        tmp_len_arg_2 = var_col;
                        tmp_compexpr_left_8 = BUILTIN_LEN( tmp_len_arg_2 );
                        if ( tmp_compexpr_left_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 59;
                            type_description_1 = "oo";
                            goto frame_exception_exit_1;
                        }
                        tmp_compexpr_right_8 = const_int_pos_3;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                        Py_DECREF( tmp_compexpr_left_8 );
                        assert( !(tmp_res == -1) );
                        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_8;
                        }
                        else
                        {
                            goto branch_no_8;
                        }
                        branch_yes_8:;
                        {
                            PyObject *tmp_left_name_1;
                            PyObject *tmp_left_name_2;
                            PyObject *tmp_left_name_3;
                            PyObject *tmp_subscribed_name_6;
                            PyObject *tmp_subscript_name_6;
                            PyObject *tmp_right_name_1;
                            PyObject *tmp_right_name_2;
                            PyObject *tmp_left_name_4;
                            PyObject *tmp_subscribed_name_7;
                            PyObject *tmp_subscript_name_7;
                            PyObject *tmp_right_name_3;
                            PyObject *tmp_right_name_4;
                            PyObject *tmp_left_name_5;
                            PyObject *tmp_subscribed_name_8;
                            PyObject *tmp_subscript_name_8;
                            PyObject *tmp_right_name_5;
                            CHECK_OBJECT( var_col );
                            tmp_subscribed_name_6 = var_col;
                            tmp_subscript_name_6 = const_int_0;
                            tmp_left_name_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
                            if ( tmp_left_name_3 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_1 = const_int_pos_2;
                            tmp_left_name_2 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_3, tmp_right_name_1 );
                            Py_DECREF( tmp_left_name_3 );
                            if ( tmp_left_name_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            CHECK_OBJECT( var_col );
                            tmp_subscribed_name_7 = var_col;
                            tmp_subscript_name_7 = const_int_pos_1;
                            tmp_left_name_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 1 );
                            if ( tmp_left_name_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_left_name_2 );

                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_3 = const_int_pos_2;
                            tmp_right_name_2 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_4, tmp_right_name_3 );
                            Py_DECREF( tmp_left_name_4 );
                            if ( tmp_right_name_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_left_name_2 );

                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
                            Py_DECREF( tmp_left_name_2 );
                            Py_DECREF( tmp_right_name_2 );
                            if ( tmp_left_name_1 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            CHECK_OBJECT( var_col );
                            tmp_subscribed_name_8 = var_col;
                            tmp_subscript_name_8 = const_int_pos_2;
                            tmp_left_name_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 2 );
                            if ( tmp_left_name_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_left_name_1 );

                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            tmp_right_name_5 = const_int_pos_2;
                            tmp_right_name_4 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_5, tmp_right_name_5 );
                            Py_DECREF( tmp_left_name_5 );
                            if ( tmp_right_name_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_left_name_1 );

                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_4 );
                            Py_DECREF( tmp_left_name_1 );
                            Py_DECREF( tmp_right_name_4 );
                            if ( tmp_return_value == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 60;
                                type_description_1 = "oo";
                                goto frame_exception_exit_1;
                            }
                            goto frame_return_exit_1;
                        }
                        branch_no_8:;
                    }
                    branch_end_7:;
                }
                branch_end_6:;
            }
            branch_end_5:;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            CHECK_OBJECT( par_text );
            tmp_compexpr_left_9 = par_text;
            tmp_compexpr_right_9 = const_tuple_str_empty_str_plain_default_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_9, tmp_compexpr_left_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_9 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            CHECK_OBJECT( par_text );
            tmp_return_value = par_text;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_no_9:;
        }
        branch_end_4:;
    }
    {
        PyObject *tmp_raise_type_1;
        PyObject *tmp_make_exception_arg_1;
        PyObject *tmp_left_name_6;
        PyObject *tmp_right_name_6;
        tmp_left_name_6 = const_str_digest_36489a3e172a2dc3f6a601c884daff7e;
        CHECK_OBJECT( par_text );
        tmp_right_name_6 = par_text;
        tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
        if ( tmp_make_exception_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_1cbf91231e6347669238443f05030aa3->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_make_exception_arg_1 };
            tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
        }

        Py_DECREF( tmp_make_exception_arg_1 );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 66;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cbf91231e6347669238443f05030aa3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cbf91231e6347669238443f05030aa3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cbf91231e6347669238443f05030aa3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1cbf91231e6347669238443f05030aa3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1cbf91231e6347669238443f05030aa3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1cbf91231e6347669238443f05030aa3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1cbf91231e6347669238443f05030aa3,
        type_description_1,
        par_text,
        var_col
    );


    // Release cached frame.
    if ( frame_1cbf91231e6347669238443f05030aa3 == cache_frame_1cbf91231e6347669238443f05030aa3 )
    {
        Py_DECREF( frame_1cbf91231e6347669238443f05030aa3 );
    }
    cache_frame_1cbf91231e6347669238443f05030aa3 = NULL;

    assertFrameObject( frame_1cbf91231e6347669238443f05030aa3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_1_parse_color );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_col );
    var_col = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_col );
    var_col = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_1_parse_color );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_2__expand_classname( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_classname = python_pars[ 0 ];
    PyObject *var_result = NULL;
    PyObject *var_parts = NULL;
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_cfc5159abc4115925f0d1731e506f4a0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_cfc5159abc4115925f0d1731e506f4a0 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_result == NULL );
        var_result = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cfc5159abc4115925f0d1731e506f4a0, codeobj_cfc5159abc4115925f0d1731e506f4a0, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cfc5159abc4115925f0d1731e506f4a0 = cache_frame_cfc5159abc4115925f0d1731e506f4a0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cfc5159abc4115925f0d1731e506f4a0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cfc5159abc4115925f0d1731e506f4a0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_classname );
        tmp_called_instance_1 = par_classname;
        frame_cfc5159abc4115925f0d1731e506f4a0->m_frame.f_lineno = 82;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_parts == NULL );
        var_parts = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_xrange_high_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_right_name_1;
        tmp_xrange_low_1 = const_int_pos_1;
        CHECK_OBJECT( var_parts );
        tmp_len_arg_1 = var_parts;
        tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_1;
        tmp_xrange_high_1 = BINARY_OPERATION_ADD_LONG_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        assert( !(tmp_xrange_high_1 == NULL) );
        tmp_iter_arg_1 = BUILTIN_XRANGE2( tmp_xrange_low_1, tmp_xrange_high_1 );
        Py_DECREF( tmp_xrange_high_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 84;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_5;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        CHECK_OBJECT( var_result );
        tmp_source_name_1 = var_result;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_source_name_2 = const_str_dot;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_join );
        assert( !(tmp_called_name_2 == NULL) );
        CHECK_OBJECT( var_parts );
        tmp_subscribed_name_1 = var_parts;
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( var_i );
        tmp_stop_name_1 = var_i;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 85;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_cfc5159abc4115925f0d1731e506f4a0->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_called_instance_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 85;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_cfc5159abc4115925f0d1731e506f4a0->m_frame.f_lineno = 85;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_lower );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 85;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_cfc5159abc4115925f0d1731e506f4a0->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cfc5159abc4115925f0d1731e506f4a0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cfc5159abc4115925f0d1731e506f4a0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cfc5159abc4115925f0d1731e506f4a0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cfc5159abc4115925f0d1731e506f4a0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cfc5159abc4115925f0d1731e506f4a0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cfc5159abc4115925f0d1731e506f4a0,
        type_description_1,
        par_classname,
        var_result,
        var_parts,
        var_i
    );


    // Release cached frame.
    if ( frame_cfc5159abc4115925f0d1731e506f4a0 == cache_frame_cfc5159abc4115925f0d1731e506f4a0 )
    {
        Py_DECREF( frame_cfc5159abc4115925f0d1731e506f4a0 );
    }
    cache_frame_cfc5159abc4115925f0d1731e506f4a0 = NULL;

    assertFrameObject( frame_cfc5159abc4115925f0d1731e506f4a0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_2__expand_classname );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_classname );
    Py_DECREF( par_classname );
    par_classname = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    CHECK_OBJECT( (PyObject *)var_parts );
    Py_DECREF( var_parts );
    var_parts = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_classname );
    Py_DECREF( par_classname );
    par_classname = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_parts );
    var_parts = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_2__expand_classname );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_3__parse_style_str( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_style_str = python_pars[ 0 ];
    PyObject *var_attrs = NULL;
    PyObject *var_part = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_d4f69ab891d763c49c51182a5edf512b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d4f69ab891d763c49c51182a5edf512b = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d4f69ab891d763c49c51182a5edf512b, codeobj_d4f69ab891d763c49c51182a5edf512b, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d4f69ab891d763c49c51182a5edf512b = cache_frame_d4f69ab891d763c49c51182a5edf512b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d4f69ab891d763c49c51182a5edf512b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d4f69ab891d763c49c51182a5edf512b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = const_str_plain_noinherit;
        CHECK_OBJECT( par_style_str );
        tmp_compexpr_right_1 = par_style_str;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DEFAULT_ATTRS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 97;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_1 = tmp_mvar_value_1;
            assert( var_attrs == NULL );
            Py_INCREF( tmp_assign_source_1 );
            var_attrs = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__EMPTY_ATTRS );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__EMPTY_ATTRS );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_EMPTY_ATTRS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_2 = tmp_mvar_value_2;
            assert( var_attrs == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_attrs = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_style_str );
        tmp_called_instance_1 = par_style_str;
        frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 102;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 102;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_part;
            var_part = tmp_assign_source_5;
            Py_INCREF( var_part );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_part );
        tmp_compexpr_left_2 = var_part;
        tmp_compexpr_right_2 = const_str_plain_noinherit;
        tmp_operand_name_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( var_part );
            tmp_compexpr_left_3 = var_part;
            tmp_compexpr_right_3 = const_str_plain_bold;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_1;
                PyObject *tmp_kw_name_1;
                if ( var_attrs == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 106;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }

                tmp_source_name_1 = var_attrs;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__replace );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 106;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                tmp_kw_name_1 = PyDict_Copy( const_dict_ac127549b4c984c0ed1fb132945dcabf );
                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 106;
                tmp_assign_source_6 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 106;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_attrs;
                    var_attrs = tmp_assign_source_6;
                    Py_XDECREF( old );
                }

            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( var_part );
                tmp_compexpr_left_4 = var_part;
                tmp_compexpr_right_4 = const_str_plain_nobold;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 107;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_7;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_2;
                    PyObject *tmp_kw_name_2;
                    if ( var_attrs == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 108;
                        type_description_1 = "ooo";
                        goto try_except_handler_2;
                    }

                    tmp_source_name_2 = var_attrs;
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__replace );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 108;
                        type_description_1 = "ooo";
                        goto try_except_handler_2;
                    }
                    tmp_kw_name_2 = PyDict_Copy( const_dict_77f39e8969719f41e72e5ef06d7a7af8 );
                    frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 108;
                    tmp_assign_source_7 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_kw_name_2 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 108;
                        type_description_1 = "ooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = var_attrs;
                        var_attrs = tmp_assign_source_7;
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    CHECK_OBJECT( var_part );
                    tmp_compexpr_left_5 = var_part;
                    tmp_compexpr_right_5 = const_str_plain_italic;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;
                        type_description_1 = "ooo";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_8;
                        PyObject *tmp_called_name_3;
                        PyObject *tmp_source_name_3;
                        PyObject *tmp_kw_name_3;
                        if ( var_attrs == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 110;
                            type_description_1 = "ooo";
                            goto try_except_handler_2;
                        }

                        tmp_source_name_3 = var_attrs;
                        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__replace );
                        if ( tmp_called_name_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "ooo";
                            goto try_except_handler_2;
                        }
                        tmp_kw_name_3 = PyDict_Copy( const_dict_fd78d4f01deb8812307e791ecafb5d29 );
                        frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 110;
                        tmp_assign_source_8 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_3 );
                        Py_DECREF( tmp_called_name_3 );
                        Py_DECREF( tmp_kw_name_3 );
                        if ( tmp_assign_source_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "ooo";
                            goto try_except_handler_2;
                        }
                        {
                            PyObject *old = var_attrs;
                            var_attrs = tmp_assign_source_8;
                            Py_XDECREF( old );
                        }

                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        nuitka_bool tmp_condition_result_6;
                        PyObject *tmp_compexpr_left_6;
                        PyObject *tmp_compexpr_right_6;
                        CHECK_OBJECT( var_part );
                        tmp_compexpr_left_6 = var_part;
                        tmp_compexpr_right_6 = const_str_plain_noitalic;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 111;
                            type_description_1 = "ooo";
                            goto try_except_handler_2;
                        }
                        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_6;
                        }
                        else
                        {
                            goto branch_no_6;
                        }
                        branch_yes_6:;
                        {
                            PyObject *tmp_assign_source_9;
                            PyObject *tmp_called_name_4;
                            PyObject *tmp_source_name_4;
                            PyObject *tmp_kw_name_4;
                            if ( var_attrs == NULL )
                            {

                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 112;
                                type_description_1 = "ooo";
                                goto try_except_handler_2;
                            }

                            tmp_source_name_4 = var_attrs;
                            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__replace );
                            if ( tmp_called_name_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 112;
                                type_description_1 = "ooo";
                                goto try_except_handler_2;
                            }
                            tmp_kw_name_4 = PyDict_Copy( const_dict_60744952d45ce2f5f4698116992f779e );
                            frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 112;
                            tmp_assign_source_9 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_4 );
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_kw_name_4 );
                            if ( tmp_assign_source_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 112;
                                type_description_1 = "ooo";
                                goto try_except_handler_2;
                            }
                            {
                                PyObject *old = var_attrs;
                                var_attrs = tmp_assign_source_9;
                                Py_XDECREF( old );
                            }

                        }
                        goto branch_end_6;
                        branch_no_6:;
                        {
                            nuitka_bool tmp_condition_result_7;
                            PyObject *tmp_compexpr_left_7;
                            PyObject *tmp_compexpr_right_7;
                            CHECK_OBJECT( var_part );
                            tmp_compexpr_left_7 = var_part;
                            tmp_compexpr_right_7 = const_str_plain_underline;
                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 113;
                                type_description_1 = "ooo";
                                goto try_except_handler_2;
                            }
                            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_7;
                            }
                            else
                            {
                                goto branch_no_7;
                            }
                            branch_yes_7:;
                            {
                                PyObject *tmp_assign_source_10;
                                PyObject *tmp_called_name_5;
                                PyObject *tmp_source_name_5;
                                PyObject *tmp_kw_name_5;
                                if ( var_attrs == NULL )
                                {

                                    exception_type = PyExc_UnboundLocalError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 114;
                                    type_description_1 = "ooo";
                                    goto try_except_handler_2;
                                }

                                tmp_source_name_5 = var_attrs;
                                tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__replace );
                                if ( tmp_called_name_5 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 114;
                                    type_description_1 = "ooo";
                                    goto try_except_handler_2;
                                }
                                tmp_kw_name_5 = PyDict_Copy( const_dict_60a5a3572ac01fb72bdec3862d78bf39 );
                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 114;
                                tmp_assign_source_10 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_5 );
                                Py_DECREF( tmp_called_name_5 );
                                Py_DECREF( tmp_kw_name_5 );
                                if ( tmp_assign_source_10 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 114;
                                    type_description_1 = "ooo";
                                    goto try_except_handler_2;
                                }
                                {
                                    PyObject *old = var_attrs;
                                    var_attrs = tmp_assign_source_10;
                                    Py_XDECREF( old );
                                }

                            }
                            goto branch_end_7;
                            branch_no_7:;
                            {
                                nuitka_bool tmp_condition_result_8;
                                PyObject *tmp_compexpr_left_8;
                                PyObject *tmp_compexpr_right_8;
                                CHECK_OBJECT( var_part );
                                tmp_compexpr_left_8 = var_part;
                                tmp_compexpr_right_8 = const_str_plain_nounderline;
                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                                if ( tmp_res == -1 )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 115;
                                    type_description_1 = "ooo";
                                    goto try_except_handler_2;
                                }
                                tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                                {
                                    goto branch_yes_8;
                                }
                                else
                                {
                                    goto branch_no_8;
                                }
                                branch_yes_8:;
                                {
                                    PyObject *tmp_assign_source_11;
                                    PyObject *tmp_called_name_6;
                                    PyObject *tmp_source_name_6;
                                    PyObject *tmp_kw_name_6;
                                    if ( var_attrs == NULL )
                                    {

                                        exception_type = PyExc_UnboundLocalError;
                                        Py_INCREF( exception_type );
                                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                        exception_tb = NULL;
                                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                        CHAIN_EXCEPTION( exception_value );

                                        exception_lineno = 116;
                                        type_description_1 = "ooo";
                                        goto try_except_handler_2;
                                    }

                                    tmp_source_name_6 = var_attrs;
                                    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__replace );
                                    if ( tmp_called_name_6 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 116;
                                        type_description_1 = "ooo";
                                        goto try_except_handler_2;
                                    }
                                    tmp_kw_name_6 = PyDict_Copy( const_dict_83d97a7aa9a123f1f0800dc984e479e1 );
                                    frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 116;
                                    tmp_assign_source_11 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_6, tmp_kw_name_6 );
                                    Py_DECREF( tmp_called_name_6 );
                                    Py_DECREF( tmp_kw_name_6 );
                                    if ( tmp_assign_source_11 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 116;
                                        type_description_1 = "ooo";
                                        goto try_except_handler_2;
                                    }
                                    {
                                        PyObject *old = var_attrs;
                                        var_attrs = tmp_assign_source_11;
                                        Py_XDECREF( old );
                                    }

                                }
                                goto branch_end_8;
                                branch_no_8:;
                                {
                                    nuitka_bool tmp_condition_result_9;
                                    PyObject *tmp_compexpr_left_9;
                                    PyObject *tmp_compexpr_right_9;
                                    CHECK_OBJECT( var_part );
                                    tmp_compexpr_left_9 = var_part;
                                    tmp_compexpr_right_9 = const_str_plain_blink;
                                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                                    if ( tmp_res == -1 )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 119;
                                        type_description_1 = "ooo";
                                        goto try_except_handler_2;
                                    }
                                    tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                    if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                                    {
                                        goto branch_yes_9;
                                    }
                                    else
                                    {
                                        goto branch_no_9;
                                    }
                                    branch_yes_9:;
                                    {
                                        PyObject *tmp_assign_source_12;
                                        PyObject *tmp_called_name_7;
                                        PyObject *tmp_source_name_7;
                                        PyObject *tmp_kw_name_7;
                                        if ( var_attrs == NULL )
                                        {

                                            exception_type = PyExc_UnboundLocalError;
                                            Py_INCREF( exception_type );
                                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                            exception_tb = NULL;
                                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                            CHAIN_EXCEPTION( exception_value );

                                            exception_lineno = 120;
                                            type_description_1 = "ooo";
                                            goto try_except_handler_2;
                                        }

                                        tmp_source_name_7 = var_attrs;
                                        tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__replace );
                                        if ( tmp_called_name_7 == NULL )
                                        {
                                            assert( ERROR_OCCURRED() );

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                            exception_lineno = 120;
                                            type_description_1 = "ooo";
                                            goto try_except_handler_2;
                                        }
                                        tmp_kw_name_7 = PyDict_Copy( const_dict_7da069136e18beec48fdb07c47426a46 );
                                        frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 120;
                                        tmp_assign_source_12 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_7, tmp_kw_name_7 );
                                        Py_DECREF( tmp_called_name_7 );
                                        Py_DECREF( tmp_kw_name_7 );
                                        if ( tmp_assign_source_12 == NULL )
                                        {
                                            assert( ERROR_OCCURRED() );

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                            exception_lineno = 120;
                                            type_description_1 = "ooo";
                                            goto try_except_handler_2;
                                        }
                                        {
                                            PyObject *old = var_attrs;
                                            var_attrs = tmp_assign_source_12;
                                            Py_XDECREF( old );
                                        }

                                    }
                                    goto branch_end_9;
                                    branch_no_9:;
                                    {
                                        nuitka_bool tmp_condition_result_10;
                                        PyObject *tmp_compexpr_left_10;
                                        PyObject *tmp_compexpr_right_10;
                                        CHECK_OBJECT( var_part );
                                        tmp_compexpr_left_10 = var_part;
                                        tmp_compexpr_right_10 = const_str_plain_noblink;
                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                                        if ( tmp_res == -1 )
                                        {
                                            assert( ERROR_OCCURRED() );

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                            exception_lineno = 121;
                                            type_description_1 = "ooo";
                                            goto try_except_handler_2;
                                        }
                                        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                                        {
                                            goto branch_yes_10;
                                        }
                                        else
                                        {
                                            goto branch_no_10;
                                        }
                                        branch_yes_10:;
                                        {
                                            PyObject *tmp_assign_source_13;
                                            PyObject *tmp_called_name_8;
                                            PyObject *tmp_source_name_8;
                                            PyObject *tmp_kw_name_8;
                                            if ( var_attrs == NULL )
                                            {

                                                exception_type = PyExc_UnboundLocalError;
                                                Py_INCREF( exception_type );
                                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                exception_tb = NULL;
                                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                CHAIN_EXCEPTION( exception_value );

                                                exception_lineno = 122;
                                                type_description_1 = "ooo";
                                                goto try_except_handler_2;
                                            }

                                            tmp_source_name_8 = var_attrs;
                                            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain__replace );
                                            if ( tmp_called_name_8 == NULL )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 122;
                                                type_description_1 = "ooo";
                                                goto try_except_handler_2;
                                            }
                                            tmp_kw_name_8 = PyDict_Copy( const_dict_e6cb51b08e7677b8e331675852d6476e );
                                            frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 122;
                                            tmp_assign_source_13 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_8, tmp_kw_name_8 );
                                            Py_DECREF( tmp_called_name_8 );
                                            Py_DECREF( tmp_kw_name_8 );
                                            if ( tmp_assign_source_13 == NULL )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 122;
                                                type_description_1 = "ooo";
                                                goto try_except_handler_2;
                                            }
                                            {
                                                PyObject *old = var_attrs;
                                                var_attrs = tmp_assign_source_13;
                                                Py_XDECREF( old );
                                            }

                                        }
                                        goto branch_end_10;
                                        branch_no_10:;
                                        {
                                            nuitka_bool tmp_condition_result_11;
                                            PyObject *tmp_compexpr_left_11;
                                            PyObject *tmp_compexpr_right_11;
                                            CHECK_OBJECT( var_part );
                                            tmp_compexpr_left_11 = var_part;
                                            tmp_compexpr_right_11 = const_str_plain_reverse;
                                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                                            if ( tmp_res == -1 )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 123;
                                                type_description_1 = "ooo";
                                                goto try_except_handler_2;
                                            }
                                            tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                                            {
                                                goto branch_yes_11;
                                            }
                                            else
                                            {
                                                goto branch_no_11;
                                            }
                                            branch_yes_11:;
                                            {
                                                PyObject *tmp_assign_source_14;
                                                PyObject *tmp_called_name_9;
                                                PyObject *tmp_source_name_9;
                                                PyObject *tmp_kw_name_9;
                                                if ( var_attrs == NULL )
                                                {

                                                    exception_type = PyExc_UnboundLocalError;
                                                    Py_INCREF( exception_type );
                                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                    exception_tb = NULL;
                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                    CHAIN_EXCEPTION( exception_value );

                                                    exception_lineno = 124;
                                                    type_description_1 = "ooo";
                                                    goto try_except_handler_2;
                                                }

                                                tmp_source_name_9 = var_attrs;
                                                tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__replace );
                                                if ( tmp_called_name_9 == NULL )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 124;
                                                    type_description_1 = "ooo";
                                                    goto try_except_handler_2;
                                                }
                                                tmp_kw_name_9 = PyDict_Copy( const_dict_20f16b30e1951d468c3080dfc46b8748 );
                                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 124;
                                                tmp_assign_source_14 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_9, tmp_kw_name_9 );
                                                Py_DECREF( tmp_called_name_9 );
                                                Py_DECREF( tmp_kw_name_9 );
                                                if ( tmp_assign_source_14 == NULL )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 124;
                                                    type_description_1 = "ooo";
                                                    goto try_except_handler_2;
                                                }
                                                {
                                                    PyObject *old = var_attrs;
                                                    var_attrs = tmp_assign_source_14;
                                                    Py_XDECREF( old );
                                                }

                                            }
                                            goto branch_end_11;
                                            branch_no_11:;
                                            {
                                                nuitka_bool tmp_condition_result_12;
                                                PyObject *tmp_compexpr_left_12;
                                                PyObject *tmp_compexpr_right_12;
                                                CHECK_OBJECT( var_part );
                                                tmp_compexpr_left_12 = var_part;
                                                tmp_compexpr_right_12 = const_str_plain_noreverse;
                                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                                                if ( tmp_res == -1 )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 125;
                                                    type_description_1 = "ooo";
                                                    goto try_except_handler_2;
                                                }
                                                tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                                                {
                                                    goto branch_yes_12;
                                                }
                                                else
                                                {
                                                    goto branch_no_12;
                                                }
                                                branch_yes_12:;
                                                {
                                                    PyObject *tmp_assign_source_15;
                                                    PyObject *tmp_called_name_10;
                                                    PyObject *tmp_source_name_10;
                                                    PyObject *tmp_kw_name_10;
                                                    if ( var_attrs == NULL )
                                                    {

                                                        exception_type = PyExc_UnboundLocalError;
                                                        Py_INCREF( exception_type );
                                                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                        exception_tb = NULL;
                                                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                        CHAIN_EXCEPTION( exception_value );

                                                        exception_lineno = 126;
                                                        type_description_1 = "ooo";
                                                        goto try_except_handler_2;
                                                    }

                                                    tmp_source_name_10 = var_attrs;
                                                    tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain__replace );
                                                    if ( tmp_called_name_10 == NULL )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                        exception_lineno = 126;
                                                        type_description_1 = "ooo";
                                                        goto try_except_handler_2;
                                                    }
                                                    tmp_kw_name_10 = PyDict_Copy( const_dict_d0b57c53f671eb73f8d61fc118c758f9 );
                                                    frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 126;
                                                    tmp_assign_source_15 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_10, tmp_kw_name_10 );
                                                    Py_DECREF( tmp_called_name_10 );
                                                    Py_DECREF( tmp_kw_name_10 );
                                                    if ( tmp_assign_source_15 == NULL )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                        exception_lineno = 126;
                                                        type_description_1 = "ooo";
                                                        goto try_except_handler_2;
                                                    }
                                                    {
                                                        PyObject *old = var_attrs;
                                                        var_attrs = tmp_assign_source_15;
                                                        Py_XDECREF( old );
                                                    }

                                                }
                                                goto branch_end_12;
                                                branch_no_12:;
                                                {
                                                    nuitka_bool tmp_condition_result_13;
                                                    PyObject *tmp_compexpr_left_13;
                                                    PyObject *tmp_compexpr_right_13;
                                                    CHECK_OBJECT( var_part );
                                                    tmp_compexpr_left_13 = var_part;
                                                    tmp_compexpr_right_13 = const_str_plain_hidden;
                                                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
                                                    if ( tmp_res == -1 )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                        exception_lineno = 127;
                                                        type_description_1 = "ooo";
                                                        goto try_except_handler_2;
                                                    }
                                                    tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                    if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                                                    {
                                                        goto branch_yes_13;
                                                    }
                                                    else
                                                    {
                                                        goto branch_no_13;
                                                    }
                                                    branch_yes_13:;
                                                    {
                                                        PyObject *tmp_assign_source_16;
                                                        PyObject *tmp_called_name_11;
                                                        PyObject *tmp_source_name_11;
                                                        PyObject *tmp_kw_name_11;
                                                        if ( var_attrs == NULL )
                                                        {

                                                            exception_type = PyExc_UnboundLocalError;
                                                            Py_INCREF( exception_type );
                                                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                            exception_tb = NULL;
                                                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                            CHAIN_EXCEPTION( exception_value );

                                                            exception_lineno = 128;
                                                            type_description_1 = "ooo";
                                                            goto try_except_handler_2;
                                                        }

                                                        tmp_source_name_11 = var_attrs;
                                                        tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__replace );
                                                        if ( tmp_called_name_11 == NULL )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 128;
                                                            type_description_1 = "ooo";
                                                            goto try_except_handler_2;
                                                        }
                                                        tmp_kw_name_11 = PyDict_Copy( const_dict_cd71fca483cf1c9e989c58412c8cfbab );
                                                        frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 128;
                                                        tmp_assign_source_16 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_11, tmp_kw_name_11 );
                                                        Py_DECREF( tmp_called_name_11 );
                                                        Py_DECREF( tmp_kw_name_11 );
                                                        if ( tmp_assign_source_16 == NULL )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 128;
                                                            type_description_1 = "ooo";
                                                            goto try_except_handler_2;
                                                        }
                                                        {
                                                            PyObject *old = var_attrs;
                                                            var_attrs = tmp_assign_source_16;
                                                            Py_XDECREF( old );
                                                        }

                                                    }
                                                    goto branch_end_13;
                                                    branch_no_13:;
                                                    {
                                                        nuitka_bool tmp_condition_result_14;
                                                        PyObject *tmp_compexpr_left_14;
                                                        PyObject *tmp_compexpr_right_14;
                                                        CHECK_OBJECT( var_part );
                                                        tmp_compexpr_left_14 = var_part;
                                                        tmp_compexpr_right_14 = const_str_plain_nohidden;
                                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
                                                        if ( tmp_res == -1 )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 129;
                                                            type_description_1 = "ooo";
                                                            goto try_except_handler_2;
                                                        }
                                                        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                                                        {
                                                            goto branch_yes_14;
                                                        }
                                                        else
                                                        {
                                                            goto branch_no_14;
                                                        }
                                                        branch_yes_14:;
                                                        {
                                                            PyObject *tmp_assign_source_17;
                                                            PyObject *tmp_called_name_12;
                                                            PyObject *tmp_source_name_12;
                                                            PyObject *tmp_kw_name_12;
                                                            if ( var_attrs == NULL )
                                                            {

                                                                exception_type = PyExc_UnboundLocalError;
                                                                Py_INCREF( exception_type );
                                                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                                exception_tb = NULL;
                                                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                CHAIN_EXCEPTION( exception_value );

                                                                exception_lineno = 130;
                                                                type_description_1 = "ooo";
                                                                goto try_except_handler_2;
                                                            }

                                                            tmp_source_name_12 = var_attrs;
                                                            tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain__replace );
                                                            if ( tmp_called_name_12 == NULL )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 130;
                                                                type_description_1 = "ooo";
                                                                goto try_except_handler_2;
                                                            }
                                                            tmp_kw_name_12 = PyDict_Copy( const_dict_998421227eff0edc6c9497483c1109a1 );
                                                            frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 130;
                                                            tmp_assign_source_17 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_12, tmp_kw_name_12 );
                                                            Py_DECREF( tmp_called_name_12 );
                                                            Py_DECREF( tmp_kw_name_12 );
                                                            if ( tmp_assign_source_17 == NULL )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 130;
                                                                type_description_1 = "ooo";
                                                                goto try_except_handler_2;
                                                            }
                                                            {
                                                                PyObject *old = var_attrs;
                                                                var_attrs = tmp_assign_source_17;
                                                                Py_XDECREF( old );
                                                            }

                                                        }
                                                        goto branch_end_14;
                                                        branch_no_14:;
                                                        {
                                                            nuitka_bool tmp_condition_result_15;
                                                            PyObject *tmp_compexpr_left_15;
                                                            PyObject *tmp_compexpr_right_15;
                                                            CHECK_OBJECT( var_part );
                                                            tmp_compexpr_left_15 = var_part;
                                                            tmp_compexpr_right_15 = const_tuple_str_plain_roman_str_plain_sans_str_plain_mono_tuple;
                                                            tmp_res = PySequence_Contains( tmp_compexpr_right_15, tmp_compexpr_left_15 );
                                                            if ( tmp_res == -1 )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 133;
                                                                type_description_1 = "ooo";
                                                                goto try_except_handler_2;
                                                            }
                                                            tmp_condition_result_15 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                                                            {
                                                                goto branch_yes_15;
                                                            }
                                                            else
                                                            {
                                                                goto branch_no_15;
                                                            }
                                                            branch_yes_15:;
                                                            {
                                                                nuitka_bool tmp_condition_result_16;
                                                                PyObject *tmp_operand_name_2;
                                                                PyObject *tmp_called_instance_2;
                                                                CHECK_OBJECT( var_part );
                                                                tmp_called_instance_2 = var_part;
                                                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 135;
                                                                tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_840be0eb3249cfd3cea55eed4ae6cfe3_tuple, 0 ) );

                                                                if ( tmp_operand_name_2 == NULL )
                                                                {
                                                                    assert( ERROR_OCCURRED() );

                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                    exception_lineno = 135;
                                                                    type_description_1 = "ooo";
                                                                    goto try_except_handler_2;
                                                                }
                                                                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                                                                Py_DECREF( tmp_operand_name_2 );
                                                                if ( tmp_res == -1 )
                                                                {
                                                                    assert( ERROR_OCCURRED() );

                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                    exception_lineno = 135;
                                                                    type_description_1 = "ooo";
                                                                    goto try_except_handler_2;
                                                                }
                                                                tmp_condition_result_16 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                                                                {
                                                                    goto branch_yes_16;
                                                                }
                                                                else
                                                                {
                                                                    goto branch_no_16;
                                                                }
                                                                branch_yes_16:;
                                                                {
                                                                    nuitka_bool tmp_condition_result_17;
                                                                    PyObject *tmp_operand_name_3;
                                                                    int tmp_and_left_truth_1;
                                                                    PyObject *tmp_and_left_value_1;
                                                                    PyObject *tmp_and_right_value_1;
                                                                    PyObject *tmp_called_instance_3;
                                                                    PyObject *tmp_called_instance_4;
                                                                    CHECK_OBJECT( var_part );
                                                                    tmp_called_instance_3 = var_part;
                                                                    frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 140;
                                                                    tmp_and_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_chr_91_tuple, 0 ) );

                                                                    if ( tmp_and_left_value_1 == NULL )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 140;
                                                                        type_description_1 = "ooo";
                                                                        goto try_except_handler_2;
                                                                    }
                                                                    tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
                                                                    if ( tmp_and_left_truth_1 == -1 )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                        Py_DECREF( tmp_and_left_value_1 );

                                                                        exception_lineno = 140;
                                                                        type_description_1 = "ooo";
                                                                        goto try_except_handler_2;
                                                                    }
                                                                    if ( tmp_and_left_truth_1 == 1 )
                                                                    {
                                                                        goto and_right_1;
                                                                    }
                                                                    else
                                                                    {
                                                                        goto and_left_1;
                                                                    }
                                                                    and_right_1:;
                                                                    Py_DECREF( tmp_and_left_value_1 );
                                                                    CHECK_OBJECT( var_part );
                                                                    tmp_called_instance_4 = var_part;
                                                                    frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 140;
                                                                    tmp_and_right_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_chr_93_tuple, 0 ) );

                                                                    if ( tmp_and_right_value_1 == NULL )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 140;
                                                                        type_description_1 = "ooo";
                                                                        goto try_except_handler_2;
                                                                    }
                                                                    tmp_operand_name_3 = tmp_and_right_value_1;
                                                                    goto and_end_1;
                                                                    and_left_1:;
                                                                    tmp_operand_name_3 = tmp_and_left_value_1;
                                                                    and_end_1:;
                                                                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                                                                    Py_DECREF( tmp_operand_name_3 );
                                                                    if ( tmp_res == -1 )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 140;
                                                                        type_description_1 = "ooo";
                                                                        goto try_except_handler_2;
                                                                    }
                                                                    tmp_condition_result_17 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                    if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                                                                    {
                                                                        goto branch_yes_17;
                                                                    }
                                                                    else
                                                                    {
                                                                        goto branch_no_17;
                                                                    }
                                                                    branch_yes_17:;
                                                                    {
                                                                        nuitka_bool tmp_condition_result_18;
                                                                        PyObject *tmp_called_instance_5;
                                                                        PyObject *tmp_call_result_1;
                                                                        int tmp_truth_name_1;
                                                                        CHECK_OBJECT( var_part );
                                                                        tmp_called_instance_5 = var_part;
                                                                        frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 144;
                                                                        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8_tuple, 0 ) );

                                                                        if ( tmp_call_result_1 == NULL )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 144;
                                                                            type_description_1 = "ooo";
                                                                            goto try_except_handler_2;
                                                                        }
                                                                        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
                                                                        if ( tmp_truth_name_1 == -1 )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                            Py_DECREF( tmp_call_result_1 );

                                                                            exception_lineno = 144;
                                                                            type_description_1 = "ooo";
                                                                            goto try_except_handler_2;
                                                                        }
                                                                        tmp_condition_result_18 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                        Py_DECREF( tmp_call_result_1 );
                                                                        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
                                                                        {
                                                                            goto branch_yes_18;
                                                                        }
                                                                        else
                                                                        {
                                                                            goto branch_no_18;
                                                                        }
                                                                        branch_yes_18:;
                                                                        {
                                                                            PyObject *tmp_assign_source_18;
                                                                            PyObject *tmp_called_name_13;
                                                                            PyObject *tmp_source_name_13;
                                                                            PyObject *tmp_kw_name_13;
                                                                            PyObject *tmp_dict_key_1;
                                                                            PyObject *tmp_dict_value_1;
                                                                            PyObject *tmp_called_name_14;
                                                                            PyObject *tmp_mvar_value_3;
                                                                            PyObject *tmp_args_element_name_1;
                                                                            PyObject *tmp_subscribed_name_1;
                                                                            PyObject *tmp_subscript_name_1;
                                                                            if ( var_attrs == NULL )
                                                                            {

                                                                                exception_type = PyExc_UnboundLocalError;
                                                                                Py_INCREF( exception_type );
                                                                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                                                exception_tb = NULL;
                                                                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                CHAIN_EXCEPTION( exception_value );

                                                                                exception_lineno = 145;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }

                                                                            tmp_source_name_13 = var_attrs;
                                                                            tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain__replace );
                                                                            if ( tmp_called_name_13 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 145;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            tmp_dict_key_1 = const_str_plain_bgcolor;
                                                                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_parse_color );

                                                                            if (unlikely( tmp_mvar_value_3 == NULL ))
                                                                            {
                                                                                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parse_color );
                                                                            }

                                                                            if ( tmp_mvar_value_3 == NULL )
                                                                            {
                                                                                Py_DECREF( tmp_called_name_13 );
                                                                                exception_type = PyExc_NameError;
                                                                                Py_INCREF( exception_type );
                                                                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parse_color" );
                                                                                exception_tb = NULL;
                                                                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                CHAIN_EXCEPTION( exception_value );

                                                                                exception_lineno = 145;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }

                                                                            tmp_called_name_14 = tmp_mvar_value_3;
                                                                            CHECK_OBJECT( var_part );
                                                                            tmp_subscribed_name_1 = var_part;
                                                                            tmp_subscript_name_1 = const_slice_int_pos_3_none_none;
                                                                            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                                                                            if ( tmp_args_element_name_1 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                Py_DECREF( tmp_called_name_13 );

                                                                                exception_lineno = 145;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 145;
                                                                            {
                                                                                PyObject *call_args[] = { tmp_args_element_name_1 };
                                                                                tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
                                                                            }

                                                                            Py_DECREF( tmp_args_element_name_1 );
                                                                            if ( tmp_dict_value_1 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                Py_DECREF( tmp_called_name_13 );

                                                                                exception_lineno = 145;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            tmp_kw_name_13 = _PyDict_NewPresized( 1 );
                                                                            tmp_res = PyDict_SetItem( tmp_kw_name_13, tmp_dict_key_1, tmp_dict_value_1 );
                                                                            Py_DECREF( tmp_dict_value_1 );
                                                                            assert( !(tmp_res != 0) );
                                                                            frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 145;
                                                                            tmp_assign_source_18 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_13, tmp_kw_name_13 );
                                                                            Py_DECREF( tmp_called_name_13 );
                                                                            Py_DECREF( tmp_kw_name_13 );
                                                                            if ( tmp_assign_source_18 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 145;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            {
                                                                                PyObject *old = var_attrs;
                                                                                var_attrs = tmp_assign_source_18;
                                                                                Py_XDECREF( old );
                                                                            }

                                                                        }
                                                                        goto branch_end_18;
                                                                        branch_no_18:;
                                                                        {
                                                                            nuitka_bool tmp_condition_result_19;
                                                                            PyObject *tmp_called_instance_6;
                                                                            PyObject *tmp_call_result_2;
                                                                            int tmp_truth_name_2;
                                                                            CHECK_OBJECT( var_part );
                                                                            tmp_called_instance_6 = var_part;
                                                                            frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 146;
                                                                            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_414e7c1a37ecdf47d805591c54eebf9e_tuple, 0 ) );

                                                                            if ( tmp_call_result_2 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 146;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
                                                                            if ( tmp_truth_name_2 == -1 )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                Py_DECREF( tmp_call_result_2 );

                                                                                exception_lineno = 146;
                                                                                type_description_1 = "ooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            tmp_condition_result_19 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                            Py_DECREF( tmp_call_result_2 );
                                                                            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
                                                                            {
                                                                                goto branch_yes_19;
                                                                            }
                                                                            else
                                                                            {
                                                                                goto branch_no_19;
                                                                            }
                                                                            branch_yes_19:;
                                                                            {
                                                                                PyObject *tmp_assign_source_19;
                                                                                PyObject *tmp_called_name_15;
                                                                                PyObject *tmp_source_name_14;
                                                                                PyObject *tmp_kw_name_14;
                                                                                PyObject *tmp_dict_key_2;
                                                                                PyObject *tmp_dict_value_2;
                                                                                PyObject *tmp_called_name_16;
                                                                                PyObject *tmp_mvar_value_4;
                                                                                PyObject *tmp_args_element_name_2;
                                                                                PyObject *tmp_subscribed_name_2;
                                                                                PyObject *tmp_subscript_name_2;
                                                                                if ( var_attrs == NULL )
                                                                                {

                                                                                    exception_type = PyExc_UnboundLocalError;
                                                                                    Py_INCREF( exception_type );
                                                                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                                                    exception_tb = NULL;
                                                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                    CHAIN_EXCEPTION( exception_value );

                                                                                    exception_lineno = 147;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }

                                                                                tmp_source_name_14 = var_attrs;
                                                                                tmp_called_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain__replace );
                                                                                if ( tmp_called_name_15 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 147;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                tmp_dict_key_2 = const_str_plain_color;
                                                                                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_parse_color );

                                                                                if (unlikely( tmp_mvar_value_4 == NULL ))
                                                                                {
                                                                                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parse_color );
                                                                                }

                                                                                if ( tmp_mvar_value_4 == NULL )
                                                                                {
                                                                                    Py_DECREF( tmp_called_name_15 );
                                                                                    exception_type = PyExc_NameError;
                                                                                    Py_INCREF( exception_type );
                                                                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parse_color" );
                                                                                    exception_tb = NULL;
                                                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                    CHAIN_EXCEPTION( exception_value );

                                                                                    exception_lineno = 147;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }

                                                                                tmp_called_name_16 = tmp_mvar_value_4;
                                                                                CHECK_OBJECT( var_part );
                                                                                tmp_subscribed_name_2 = var_part;
                                                                                tmp_subscript_name_2 = const_slice_int_pos_3_none_none;
                                                                                tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                                                                                if ( tmp_args_element_name_2 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                    Py_DECREF( tmp_called_name_15 );

                                                                                    exception_lineno = 147;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 147;
                                                                                {
                                                                                    PyObject *call_args[] = { tmp_args_element_name_2 };
                                                                                    tmp_dict_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
                                                                                }

                                                                                Py_DECREF( tmp_args_element_name_2 );
                                                                                if ( tmp_dict_value_2 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                    Py_DECREF( tmp_called_name_15 );

                                                                                    exception_lineno = 147;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                tmp_kw_name_14 = _PyDict_NewPresized( 1 );
                                                                                tmp_res = PyDict_SetItem( tmp_kw_name_14, tmp_dict_key_2, tmp_dict_value_2 );
                                                                                Py_DECREF( tmp_dict_value_2 );
                                                                                assert( !(tmp_res != 0) );
                                                                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 147;
                                                                                tmp_assign_source_19 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_15, tmp_kw_name_14 );
                                                                                Py_DECREF( tmp_called_name_15 );
                                                                                Py_DECREF( tmp_kw_name_14 );
                                                                                if ( tmp_assign_source_19 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 147;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                {
                                                                                    PyObject *old = var_attrs;
                                                                                    var_attrs = tmp_assign_source_19;
                                                                                    Py_XDECREF( old );
                                                                                }

                                                                            }
                                                                            goto branch_end_19;
                                                                            branch_no_19:;
                                                                            {
                                                                                PyObject *tmp_assign_source_20;
                                                                                PyObject *tmp_called_name_17;
                                                                                PyObject *tmp_source_name_15;
                                                                                PyObject *tmp_kw_name_15;
                                                                                PyObject *tmp_dict_key_3;
                                                                                PyObject *tmp_dict_value_3;
                                                                                PyObject *tmp_called_name_18;
                                                                                PyObject *tmp_mvar_value_5;
                                                                                PyObject *tmp_args_element_name_3;
                                                                                if ( var_attrs == NULL )
                                                                                {

                                                                                    exception_type = PyExc_UnboundLocalError;
                                                                                    Py_INCREF( exception_type );
                                                                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
                                                                                    exception_tb = NULL;
                                                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                    CHAIN_EXCEPTION( exception_value );

                                                                                    exception_lineno = 149;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }

                                                                                tmp_source_name_15 = var_attrs;
                                                                                tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain__replace );
                                                                                if ( tmp_called_name_17 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 149;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                tmp_dict_key_3 = const_str_plain_color;
                                                                                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_parse_color );

                                                                                if (unlikely( tmp_mvar_value_5 == NULL ))
                                                                                {
                                                                                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parse_color );
                                                                                }

                                                                                if ( tmp_mvar_value_5 == NULL )
                                                                                {
                                                                                    Py_DECREF( tmp_called_name_17 );
                                                                                    exception_type = PyExc_NameError;
                                                                                    Py_INCREF( exception_type );
                                                                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parse_color" );
                                                                                    exception_tb = NULL;
                                                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                    CHAIN_EXCEPTION( exception_value );

                                                                                    exception_lineno = 149;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }

                                                                                tmp_called_name_18 = tmp_mvar_value_5;
                                                                                CHECK_OBJECT( var_part );
                                                                                tmp_args_element_name_3 = var_part;
                                                                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 149;
                                                                                {
                                                                                    PyObject *call_args[] = { tmp_args_element_name_3 };
                                                                                    tmp_dict_value_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
                                                                                }

                                                                                if ( tmp_dict_value_3 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                    Py_DECREF( tmp_called_name_17 );

                                                                                    exception_lineno = 149;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                tmp_kw_name_15 = _PyDict_NewPresized( 1 );
                                                                                tmp_res = PyDict_SetItem( tmp_kw_name_15, tmp_dict_key_3, tmp_dict_value_3 );
                                                                                Py_DECREF( tmp_dict_value_3 );
                                                                                assert( !(tmp_res != 0) );
                                                                                frame_d4f69ab891d763c49c51182a5edf512b->m_frame.f_lineno = 149;
                                                                                tmp_assign_source_20 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_17, tmp_kw_name_15 );
                                                                                Py_DECREF( tmp_called_name_17 );
                                                                                Py_DECREF( tmp_kw_name_15 );
                                                                                if ( tmp_assign_source_20 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 149;
                                                                                    type_description_1 = "ooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                {
                                                                                    PyObject *old = var_attrs;
                                                                                    var_attrs = tmp_assign_source_20;
                                                                                    Py_XDECREF( old );
                                                                                }

                                                                            }
                                                                            branch_end_19:;
                                                                        }
                                                                        branch_end_18:;
                                                                    }
                                                                    branch_no_17:;
                                                                }
                                                                branch_no_16:;
                                                            }
                                                            branch_no_15:;
                                                        }
                                                        branch_end_14:;
                                                    }
                                                    branch_end_13:;
                                                }
                                                branch_end_12:;
                                            }
                                            branch_end_11:;
                                        }
                                        branch_end_10:;
                                    }
                                    branch_end_9:;
                                }
                                branch_end_8:;
                            }
                            branch_end_7:;
                        }
                        branch_end_6:;
                    }
                    branch_end_5:;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    if ( var_attrs == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "attrs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 151;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_attrs;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4f69ab891d763c49c51182a5edf512b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4f69ab891d763c49c51182a5edf512b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4f69ab891d763c49c51182a5edf512b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d4f69ab891d763c49c51182a5edf512b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d4f69ab891d763c49c51182a5edf512b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d4f69ab891d763c49c51182a5edf512b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d4f69ab891d763c49c51182a5edf512b,
        type_description_1,
        par_style_str,
        var_attrs,
        var_part
    );


    // Release cached frame.
    if ( frame_d4f69ab891d763c49c51182a5edf512b == cache_frame_d4f69ab891d763c49c51182a5edf512b )
    {
        Py_DECREF( frame_d4f69ab891d763c49c51182a5edf512b );
    }
    cache_frame_d4f69ab891d763c49c51182a5edf512b = NULL;

    assertFrameObject( frame_d4f69ab891d763c49c51182a5edf512b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_3__parse_style_str );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_style_str );
    Py_DECREF( par_style_str );
    par_style_str = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_part );
    var_part = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_style_str );
    Py_DECREF( par_style_str );
    par_style_str = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_part );
    var_part = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_3__parse_style_str );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_4___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_style_rules = python_pars[ 1 ];
    PyObject *var_class_names_and_attrs = NULL;
    PyObject *var_class_names = NULL;
    PyObject *var_style_str = NULL;
    PyObject *var_attrs = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_297839a72227d66dabefa647f05bd6a6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_297839a72227d66dabefa647f05bd6a6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_297839a72227d66dabefa647f05bd6a6, codeobj_297839a72227d66dabefa647f05bd6a6, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_297839a72227d66dabefa647f05bd6a6 = cache_frame_297839a72227d66dabefa647f05bd6a6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_297839a72227d66dabefa647f05bd6a6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_297839a72227d66dabefa647f05bd6a6 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_style_rules );
        tmp_isinstance_inst_1 = par_style_rules;
        tmp_isinstance_cls_1 = (PyObject *)&PyList_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 211;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_class_names_and_attrs == NULL );
        var_class_names_and_attrs = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_style_rules );
        tmp_iter_arg_1 = par_style_rules;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 217;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 217;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 217;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 217;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 217;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_class_names;
            var_class_names = tmp_assign_source_7;
            Py_INCREF( var_class_names );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_style_str;
            var_style_str = tmp_assign_source_8;
            Py_INCREF( var_style_str );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_CLASS_NAMES_RE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CLASS_NAMES_RE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CLASS_NAMES_RE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 218;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_class_names );
        tmp_args_element_name_1 = var_class_names;
        frame_297839a72227d66dabefa647f05bd6a6->m_frame.f_lineno = 218;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_match, call_args );
        }

        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_raise_value_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_operand_name_3;
            tmp_raise_type_2 = PyExc_AssertionError;
            CHECK_OBJECT( var_class_names );
            tmp_operand_name_3 = var_class_names;
            tmp_tuple_element_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_3 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 218;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            tmp_raise_value_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_raise_value_1, 0, tmp_tuple_element_1 );
            exception_type = tmp_raise_type_2;
            Py_INCREF( tmp_raise_type_2 );
            exception_value = tmp_raise_value_1;
            exception_lineno = 218;
            RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_frozenset_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_class_names );
        tmp_called_instance_3 = var_class_names;
        frame_297839a72227d66dabefa647f05bd6a6->m_frame.f_lineno = 222;
        tmp_called_instance_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_lower );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        frame_297839a72227d66dabefa647f05bd6a6->m_frame.f_lineno = 222;
        tmp_frozenset_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_split );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_frozenset_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_9 = PyFrozenSet_New( tmp_frozenset_arg_1 );
        Py_DECREF( tmp_frozenset_arg_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_class_names;
            assert( old != NULL );
            var_class_names = tmp_assign_source_9;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__parse_style_str );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__parse_style_str );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_parse_style_str" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 223;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( var_style_str );
        tmp_args_element_name_2 = var_style_str;
        frame_297839a72227d66dabefa647f05bd6a6->m_frame.f_lineno = 223;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_attrs;
            var_attrs = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_tuple_element_2;
        CHECK_OBJECT( var_class_names_and_attrs );
        tmp_called_instance_4 = var_class_names_and_attrs;
        CHECK_OBJECT( var_class_names );
        tmp_tuple_element_2 = var_class_names;
        tmp_args_element_name_3 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_element_name_3, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var_attrs );
        tmp_tuple_element_2 = var_attrs;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_element_name_3, 1, tmp_tuple_element_2 );
        frame_297839a72227d66dabefa647f05bd6a6->m_frame.f_lineno = 225;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 217;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_style_rules );
        tmp_assattr_name_1 = par_style_rules;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__style_rules, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( var_class_names_and_attrs );
        tmp_assattr_name_2 = var_class_names_and_attrs;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_class_names_and_attrs, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_297839a72227d66dabefa647f05bd6a6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_297839a72227d66dabefa647f05bd6a6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_297839a72227d66dabefa647f05bd6a6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_297839a72227d66dabefa647f05bd6a6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_297839a72227d66dabefa647f05bd6a6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_297839a72227d66dabefa647f05bd6a6,
        type_description_1,
        par_self,
        par_style_rules,
        var_class_names_and_attrs,
        var_class_names,
        var_style_str,
        var_attrs
    );


    // Release cached frame.
    if ( frame_297839a72227d66dabefa647f05bd6a6 == cache_frame_297839a72227d66dabefa647f05bd6a6 )
    {
        Py_DECREF( frame_297839a72227d66dabefa647f05bd6a6 );
    }
    cache_frame_297839a72227d66dabefa647f05bd6a6 = NULL;

    assertFrameObject( frame_297839a72227d66dabefa647f05bd6a6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_4___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_style_rules );
    Py_DECREF( par_style_rules );
    par_style_rules = NULL;

    CHECK_OBJECT( (PyObject *)var_class_names_and_attrs );
    Py_DECREF( var_class_names_and_attrs );
    var_class_names_and_attrs = NULL;

    Py_XDECREF( var_class_names );
    var_class_names = NULL;

    Py_XDECREF( var_style_str );
    var_style_str = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_style_rules );
    Py_DECREF( par_style_rules );
    par_style_rules = NULL;

    Py_XDECREF( var_class_names_and_attrs );
    var_class_names_and_attrs = NULL;

    Py_XDECREF( var_class_names );
    var_class_names = NULL;

    Py_XDECREF( var_style_str );
    var_style_str = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_4___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_5_style_rules( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_499daadb8d1c0bc8c11425a0b9d1d69a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_499daadb8d1c0bc8c11425a0b9d1d69a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_499daadb8d1c0bc8c11425a0b9d1d69a, codeobj_499daadb8d1c0bc8c11425a0b9d1d69a, module_prompt_toolkit$styles$style, sizeof(void *) );
    frame_499daadb8d1c0bc8c11425a0b9d1d69a = cache_frame_499daadb8d1c0bc8c11425a0b9d1d69a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_499daadb8d1c0bc8c11425a0b9d1d69a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_499daadb8d1c0bc8c11425a0b9d1d69a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__style_rules );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_499daadb8d1c0bc8c11425a0b9d1d69a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_499daadb8d1c0bc8c11425a0b9d1d69a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_499daadb8d1c0bc8c11425a0b9d1d69a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_499daadb8d1c0bc8c11425a0b9d1d69a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_499daadb8d1c0bc8c11425a0b9d1d69a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_499daadb8d1c0bc8c11425a0b9d1d69a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_499daadb8d1c0bc8c11425a0b9d1d69a,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_499daadb8d1c0bc8c11425a0b9d1d69a == cache_frame_499daadb8d1c0bc8c11425a0b9d1d69a )
    {
        Py_DECREF( frame_499daadb8d1c0bc8c11425a0b9d1d69a );
    }
    cache_frame_499daadb8d1c0bc8c11425a0b9d1d69a = NULL;

    assertFrameObject( frame_499daadb8d1c0bc8c11425a0b9d1d69a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_5_style_rules );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_5_style_rules );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_6_from_dict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_style_dict = python_pars[ 1 ];
    PyObject *par_priority = python_pars[ 2 ];
    PyObject *var_key = NULL;
    struct Nuitka_FrameObject *frame_bf9ec9bffa6966be9cdb21ecb7bba484;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_bf9ec9bffa6966be9cdb21ecb7bba484 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bf9ec9bffa6966be9cdb21ecb7bba484, codeobj_bf9ec9bffa6966be9cdb21ecb7bba484, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bf9ec9bffa6966be9cdb21ecb7bba484 = cache_frame_bf9ec9bffa6966be9cdb21ecb7bba484;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bf9ec9bffa6966be9cdb21ecb7bba484 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bf9ec9bffa6966be9cdb21ecb7bba484 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_priority );
        tmp_compexpr_left_1 = par_priority;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Priority );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Priority );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Priority" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__ALL );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 240;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_priority );
        tmp_compexpr_left_2 = par_priority;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Priority );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Priority );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Priority" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_MOST_PRECISE );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key(  );



            assert( var_key == NULL );
            var_key = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            CHECK_OBJECT( par_cls );
            tmp_called_name_1 = par_cls;
            tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_sorted );
            assert( tmp_called_name_2 != NULL );
            CHECK_OBJECT( par_style_dict );
            tmp_called_instance_1 = par_style_dict;
            frame_bf9ec9bffa6966be9cdb21ecb7bba484->m_frame.f_lineno = 247;
            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 247;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_args_name_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_key;
            CHECK_OBJECT( var_key );
            tmp_dict_value_1 = var_key;
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_bf9ec9bffa6966be9cdb21ecb7bba484->m_frame.f_lineno = 247;
            tmp_args_element_name_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 247;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_bf9ec9bffa6966be9cdb21ecb7bba484->m_frame.f_lineno = 247;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 247;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_list_arg_1;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( par_cls );
            tmp_called_name_3 = par_cls;
            CHECK_OBJECT( par_style_dict );
            tmp_called_instance_2 = par_style_dict;
            frame_bf9ec9bffa6966be9cdb21ecb7bba484->m_frame.f_lineno = 249;
            tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_items );
            if ( tmp_list_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 249;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_2 = PySequence_List( tmp_list_arg_1 );
            Py_DECREF( tmp_list_arg_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 249;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_bf9ec9bffa6966be9cdb21ecb7bba484->m_frame.f_lineno = 249;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 249;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf9ec9bffa6966be9cdb21ecb7bba484 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf9ec9bffa6966be9cdb21ecb7bba484 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bf9ec9bffa6966be9cdb21ecb7bba484 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bf9ec9bffa6966be9cdb21ecb7bba484, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bf9ec9bffa6966be9cdb21ecb7bba484->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bf9ec9bffa6966be9cdb21ecb7bba484, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bf9ec9bffa6966be9cdb21ecb7bba484,
        type_description_1,
        par_cls,
        par_style_dict,
        par_priority,
        var_key
    );


    // Release cached frame.
    if ( frame_bf9ec9bffa6966be9cdb21ecb7bba484 == cache_frame_bf9ec9bffa6966be9cdb21ecb7bba484 )
    {
        Py_DECREF( frame_bf9ec9bffa6966be9cdb21ecb7bba484 );
    }
    cache_frame_bf9ec9bffa6966be9cdb21ecb7bba484 = NULL;

    assertFrameObject( frame_bf9ec9bffa6966be9cdb21ecb7bba484 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_6_from_dict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_style_dict );
    Py_DECREF( par_style_dict );
    par_style_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_priority );
    Py_DECREF( par_priority );
    par_priority = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_style_dict );
    Py_DECREF( par_style_dict );
    par_style_dict = NULL;

    CHECK_OBJECT( (PyObject *)par_priority );
    Py_DECREF( par_priority );
    par_priority = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_6_from_dict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_item = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_480159fc265e16f5844c0552e15bd18f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_480159fc265e16f5844c0552e15bd18f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_480159fc265e16f5844c0552e15bd18f, codeobj_480159fc265e16f5844c0552e15bd18f, module_prompt_toolkit$styles$style, sizeof(void *) );
    frame_480159fc265e16f5844c0552e15bd18f = cache_frame_480159fc265e16f5844c0552e15bd18f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_480159fc265e16f5844c0552e15bd18f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_480159fc265e16f5844c0552e15bd18f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_sum_sequence_1;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_item );
            tmp_subscribed_name_1 = par_item;
            tmp_subscript_name_1 = const_int_0;
            tmp_called_instance_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_480159fc265e16f5844c0552e15bd18f->m_frame.f_lineno = 245;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_sum_sequence_1 = prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_sum_sequence_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key );
        return NULL;
        outline_result_1:;
        tmp_return_value = BUILTIN_SUM1( tmp_sum_sequence_1 );
        Py_DECREF( tmp_sum_sequence_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_480159fc265e16f5844c0552e15bd18f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_480159fc265e16f5844c0552e15bd18f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_480159fc265e16f5844c0552e15bd18f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_480159fc265e16f5844c0552e15bd18f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_480159fc265e16f5844c0552e15bd18f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_480159fc265e16f5844c0552e15bd18f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_480159fc265e16f5844c0552e15bd18f,
        type_description_1,
        par_item
    );


    // Release cached frame.
    if ( frame_480159fc265e16f5844c0552e15bd18f == cache_frame_480159fc265e16f5844c0552e15bd18f )
    {
        Py_DECREF( frame_480159fc265e16f5844c0552e15bd18f );
    }
    cache_frame_480159fc265e16f5844c0552e15bd18f = NULL;

    assertFrameObject( frame_480159fc265e16f5844c0552e15bd18f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_item );
    Py_DECREF( par_item );
    par_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_item );
    Py_DECREF( par_item );
    par_item = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_locals {
    PyObject *var_i;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_53470a7a2592edfac0b1898a0f096b92, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 245;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_called_instance_1 = generator_heap->var_i;
        generator->m_frame->m_frame.f_lineno = 245;
        tmp_len_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 245;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 245;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_len_arg_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_len_arg_1, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 245;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 245;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_i
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$styles$style,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_d11448a99d1fef69467026fbe80d70eb,
#endif
        codeobj_53470a7a2592edfac0b1898a0f096b92,
        1,
        sizeof(struct prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_style_str = python_pars[ 1 ];
    PyObject *par_default = python_pars[ 2 ];
    PyObject *var_list_of_attrs = NULL;
    PyObject *var_class_names = NULL;
    PyObject *var_names = NULL;
    PyObject *var_attr = NULL;
    PyObject *var_part = NULL;
    PyObject *var_new_class_names = NULL;
    PyObject *var_p = NULL;
    PyObject *var_new_name = NULL;
    PyObject *var_combos = NULL;
    PyObject *var_count = NULL;
    PyObject *var_c2 = NULL;
    PyObject *var_inline_attrs = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    PyObject *tmp_for_loop_5__for_iterator = NULL;
    PyObject *tmp_for_loop_5__iter_value = NULL;
    PyObject *tmp_for_loop_6__for_iterator = NULL;
    PyObject *tmp_for_loop_6__iter_value = NULL;
    PyObject *tmp_for_loop_7__for_iterator = NULL;
    PyObject *tmp_for_loop_7__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_46343e520769db6726bd3f118076dfab;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_46343e520769db6726bd3f118076dfab = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_list_element_1;
        CHECK_OBJECT( par_default );
        tmp_list_element_1 = par_default;
        tmp_assign_source_1 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_assign_source_1, 0, tmp_list_element_1 );
        assert( var_list_of_attrs == NULL );
        var_list_of_attrs = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PySet_New( NULL );
        assert( var_class_names == NULL );
        var_class_names = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_46343e520769db6726bd3f118076dfab, codeobj_46343e520769db6726bd3f118076dfab, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_46343e520769db6726bd3f118076dfab = cache_frame_46343e520769db6726bd3f118076dfab;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_46343e520769db6726bd3f118076dfab );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_46343e520769db6726bd3f118076dfab ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_class_names_and_attrs );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooo";
                exception_lineno = 259;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooo";
            exception_lineno = 259;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooo";
            exception_lineno = 259;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooo";
                    exception_lineno = 259;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooooooo";
            exception_lineno = 259;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_names;
            var_names = tmp_assign_source_8;
            Py_INCREF( var_names );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_attr;
            var_attr = tmp_assign_source_9;
            Py_INCREF( var_attr );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_names );
        tmp_operand_name_1 = var_names;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_list_of_attrs );
            tmp_called_instance_1 = var_list_of_attrs;
            CHECK_OBJECT( var_attr );
            tmp_args_element_name_1 = var_attr;
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 261;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 261;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 259;
        type_description_1 = "ooooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_style_str );
        tmp_called_instance_2 = par_style_str;
        frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 265;
        tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_split );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_10;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooo";
                exception_lineno = 265;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_12 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_part;
            var_part = tmp_assign_source_12;
            Py_INCREF( var_part );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_part );
        tmp_called_instance_3 = var_part;
        frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 269;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_7b2e486efb24f89284503a5b81c960c1_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_5;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 269;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = PyList_New( 0 );
            {
                PyObject *old = var_new_class_names;
                var_new_class_names = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_iter_arg_4;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_part );
            tmp_subscribed_name_1 = var_part;
            tmp_subscript_name_1 = const_slice_int_pos_6_none_none;
            tmp_called_instance_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_called_instance_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 272;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 272;
            tmp_called_instance_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_lower );
            Py_DECREF( tmp_called_instance_5 );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 272;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 272;
            tmp_iter_arg_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_44_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_iter_arg_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 272;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_4 );
            Py_DECREF( tmp_iter_arg_4 );
            if ( tmp_assign_source_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 272;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_for_loop_3__for_iterator;
                tmp_for_loop_3__for_iterator = tmp_assign_source_14;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( tmp_for_loop_3__for_iterator );
            tmp_next_source_3 = tmp_for_loop_3__for_iterator;
            tmp_assign_source_15 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooo";
                    exception_lineno = 272;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_for_loop_3__iter_value;
                tmp_for_loop_3__iter_value = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( tmp_for_loop_3__iter_value );
            tmp_assign_source_16 = tmp_for_loop_3__iter_value;
            {
                PyObject *old = var_p;
                var_p = tmp_assign_source_16;
                Py_INCREF( var_p );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( var_new_class_names );
            tmp_source_name_2 = var_new_class_names;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_extend );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_6;
            }
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__expand_classname );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__expand_classname );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_expand_classname" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 273;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_6;
            }

            tmp_called_name_2 = tmp_mvar_value_1;
            CHECK_OBJECT( var_p );
            tmp_args_element_name_3 = var_p;
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 273;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 273;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_6;
            }
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 273;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 273;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_6;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_6;
        }
        goto loop_start_3;
        loop_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_3__iter_value );
        tmp_for_loop_3__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
        Py_DECREF( tmp_for_loop_3__for_iterator );
        tmp_for_loop_3__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        try_end_4:;
        Py_XDECREF( tmp_for_loop_3__iter_value );
        tmp_for_loop_3__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
        Py_DECREF( tmp_for_loop_3__for_iterator );
        tmp_for_loop_3__for_iterator = NULL;

        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_iter_arg_5;
            CHECK_OBJECT( var_new_class_names );
            tmp_iter_arg_5 = var_new_class_names;
            tmp_assign_source_17 = MAKE_ITERATOR( tmp_iter_arg_5 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = tmp_for_loop_4__for_iterator;
                tmp_for_loop_4__for_iterator = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_4:;
        {
            PyObject *tmp_next_source_4;
            PyObject *tmp_assign_source_18;
            CHECK_OBJECT( tmp_for_loop_4__for_iterator );
            tmp_next_source_4 = tmp_for_loop_4__for_iterator;
            tmp_assign_source_18 = ITERATOR_NEXT( tmp_next_source_4 );
            if ( tmp_assign_source_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_4;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooo";
                    exception_lineno = 275;
                    goto try_except_handler_7;
                }
            }

            {
                PyObject *old = tmp_for_loop_4__iter_value;
                tmp_for_loop_4__iter_value = tmp_assign_source_18;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_19;
            CHECK_OBJECT( tmp_for_loop_4__iter_value );
            tmp_assign_source_19 = tmp_for_loop_4__iter_value;
            {
                PyObject *old = var_new_name;
                var_new_name = tmp_assign_source_19;
                Py_INCREF( var_new_name );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_20;
            tmp_assign_source_20 = PySet_New( NULL );
            {
                PyObject *old = var_combos;
                var_combos = tmp_assign_source_20;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_frozenset_arg_1;
            PyObject *tmp_list_element_2;
            CHECK_OBJECT( var_combos );
            tmp_source_name_3 = var_combos;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_add );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 278;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            CHECK_OBJECT( var_new_name );
            tmp_list_element_2 = var_new_name;
            tmp_frozenset_arg_1 = PyList_New( 1 );
            Py_INCREF( tmp_list_element_2 );
            PyList_SET_ITEM( tmp_frozenset_arg_1, 0, tmp_list_element_2 );
            tmp_args_element_name_4 = PyFrozenSet_New( tmp_frozenset_arg_1 );
            Py_DECREF( tmp_frozenset_arg_1 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 278;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 278;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 278;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_iter_arg_6;
            PyObject *tmp_xrange_low_1;
            PyObject *tmp_xrange_high_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_len_arg_1;
            PyObject *tmp_right_name_1;
            tmp_xrange_low_1 = const_int_pos_1;
            CHECK_OBJECT( var_class_names );
            tmp_len_arg_1 = var_class_names;
            tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
            assert( !(tmp_left_name_1 == NULL) );
            tmp_right_name_1 = const_int_pos_1;
            tmp_xrange_high_1 = BINARY_OPERATION_ADD_LONG_LONG( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            assert( !(tmp_xrange_high_1 == NULL) );
            tmp_iter_arg_6 = BUILTIN_XRANGE2( tmp_xrange_low_1, tmp_xrange_high_1 );
            Py_DECREF( tmp_xrange_high_1 );
            if ( tmp_iter_arg_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 280;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_6 );
            Py_DECREF( tmp_iter_arg_6 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 280;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_for_loop_5__for_iterator;
                tmp_for_loop_5__for_iterator = tmp_assign_source_21;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_5:;
        {
            PyObject *tmp_next_source_5;
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( tmp_for_loop_5__for_iterator );
            tmp_next_source_5 = tmp_for_loop_5__for_iterator;
            tmp_assign_source_22 = ITERATOR_NEXT( tmp_next_source_5 );
            if ( tmp_assign_source_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_5;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooo";
                    exception_lineno = 280;
                    goto try_except_handler_8;
                }
            }

            {
                PyObject *old = tmp_for_loop_5__iter_value;
                tmp_for_loop_5__iter_value = tmp_assign_source_22;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_23;
            CHECK_OBJECT( tmp_for_loop_5__iter_value );
            tmp_assign_source_23 = tmp_for_loop_5__iter_value;
            {
                PyObject *old = var_count;
                var_count = tmp_assign_source_23;
                Py_INCREF( var_count );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_iter_arg_7;
            PyObject *tmp_called_instance_6;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_itertools );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_itertools );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "itertools" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 281;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_8;
            }

            tmp_called_instance_6 = tmp_mvar_value_2;
            CHECK_OBJECT( var_class_names );
            tmp_args_element_name_5 = var_class_names;
            CHECK_OBJECT( var_count );
            tmp_args_element_name_6 = var_count;
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 281;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_iter_arg_7 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain_combinations, call_args );
            }

            if ( tmp_iter_arg_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 281;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_8;
            }
            tmp_assign_source_24 = MAKE_ITERATOR( tmp_iter_arg_7 );
            Py_DECREF( tmp_iter_arg_7 );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 281;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_8;
            }
            {
                PyObject *old = tmp_for_loop_6__for_iterator;
                tmp_for_loop_6__for_iterator = tmp_assign_source_24;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_6:;
        {
            PyObject *tmp_next_source_6;
            PyObject *tmp_assign_source_25;
            CHECK_OBJECT( tmp_for_loop_6__for_iterator );
            tmp_next_source_6 = tmp_for_loop_6__for_iterator;
            tmp_assign_source_25 = ITERATOR_NEXT( tmp_next_source_6 );
            if ( tmp_assign_source_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_6;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooo";
                    exception_lineno = 281;
                    goto try_except_handler_9;
                }
            }

            {
                PyObject *old = tmp_for_loop_6__iter_value;
                tmp_for_loop_6__iter_value = tmp_assign_source_25;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_26;
            CHECK_OBJECT( tmp_for_loop_6__iter_value );
            tmp_assign_source_26 = tmp_for_loop_6__iter_value;
            {
                PyObject *old = var_c2;
                var_c2 = tmp_assign_source_26;
                Py_INCREF( var_c2 );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_4;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_frozenset_arg_2;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( var_combos );
            tmp_source_name_4 = var_combos;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_add );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 282;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_9;
            }
            CHECK_OBJECT( var_c2 );
            tmp_left_name_2 = var_c2;
            CHECK_OBJECT( var_new_name );
            tmp_tuple_element_1 = var_new_name;
            tmp_right_name_2 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_1 );
            tmp_frozenset_arg_2 = BINARY_OPERATION_ADD_OBJECT_TUPLE( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_frozenset_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 282;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_9;
            }
            tmp_args_element_name_7 = PyFrozenSet_New( tmp_frozenset_arg_2 );
            Py_DECREF( tmp_frozenset_arg_2 );
            if ( tmp_args_element_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 282;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_9;
            }
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 282;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 282;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_9;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_9;
        }
        goto loop_start_6;
        loop_end_6:;
        goto try_end_5;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_6__iter_value );
        tmp_for_loop_6__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_6__for_iterator );
        Py_DECREF( tmp_for_loop_6__for_iterator );
        tmp_for_loop_6__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_8;
        // End of try:
        try_end_5:;
        Py_XDECREF( tmp_for_loop_6__iter_value );
        tmp_for_loop_6__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_6__for_iterator );
        Py_DECREF( tmp_for_loop_6__for_iterator );
        tmp_for_loop_6__for_iterator = NULL;

        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_8;
        }
        goto loop_start_5;
        loop_end_5:;
        goto try_end_6;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_5__iter_value );
        tmp_for_loop_5__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
        Py_DECREF( tmp_for_loop_5__for_iterator );
        tmp_for_loop_5__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        try_end_6:;
        Py_XDECREF( tmp_for_loop_5__iter_value );
        tmp_for_loop_5__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
        Py_DECREF( tmp_for_loop_5__for_iterator );
        tmp_for_loop_5__for_iterator = NULL;

        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_iter_arg_8;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_iter_arg_8 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_class_names_and_attrs );
            if ( tmp_iter_arg_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 285;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            tmp_assign_source_27 = MAKE_ITERATOR( tmp_iter_arg_8 );
            Py_DECREF( tmp_iter_arg_8 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 285;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_for_loop_7__for_iterator;
                tmp_for_loop_7__for_iterator = tmp_assign_source_27;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_7:;
        {
            PyObject *tmp_next_source_7;
            PyObject *tmp_assign_source_28;
            CHECK_OBJECT( tmp_for_loop_7__for_iterator );
            tmp_next_source_7 = tmp_for_loop_7__for_iterator;
            tmp_assign_source_28 = ITERATOR_NEXT( tmp_next_source_7 );
            if ( tmp_assign_source_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_7;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooooooo";
                    exception_lineno = 285;
                    goto try_except_handler_10;
                }
            }

            {
                PyObject *old = tmp_for_loop_7__iter_value;
                tmp_for_loop_7__iter_value = tmp_assign_source_28;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_iter_arg_9;
            CHECK_OBJECT( tmp_for_loop_7__iter_value );
            tmp_iter_arg_9 = tmp_for_loop_7__iter_value;
            tmp_assign_source_29 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_9 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 285;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_11;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__source_iter;
                tmp_tuple_unpack_2__source_iter = tmp_assign_source_29;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
            if ( tmp_assign_source_30 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooo";
                exception_lineno = 285;
                goto try_except_handler_12;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_1;
                tmp_tuple_unpack_2__element_1 = tmp_assign_source_30;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_31 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
            if ( tmp_assign_source_31 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooo";
                exception_lineno = 285;
                goto try_except_handler_12;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_2;
                tmp_tuple_unpack_2__element_2 = tmp_assign_source_31;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooo";
                        exception_lineno = 285;
                        goto try_except_handler_12;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooooo";
                exception_lineno = 285;
                goto try_except_handler_12;
            }
        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_11;
        // End of try:
        try_end_7:;
        goto try_end_8;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_10;
        // End of try:
        try_end_8:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_32;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_32 = tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = var_names;
                var_names = tmp_assign_source_32;
                Py_INCREF( var_names );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_33;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_33 = tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = var_attr;
                var_attr = tmp_assign_source_33;
                Py_INCREF( var_attr );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_names );
            tmp_compexpr_left_1 = var_names;
            CHECK_OBJECT( var_combos );
            tmp_compexpr_right_1 = var_combos;
            tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 286;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_instance_7;
                PyObject *tmp_call_result_6;
                PyObject *tmp_args_element_name_8;
                CHECK_OBJECT( var_list_of_attrs );
                tmp_called_instance_7 = var_list_of_attrs;
                CHECK_OBJECT( var_attr );
                tmp_args_element_name_8 = var_attr;
                frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 287;
                {
                    PyObject *call_args[] = { tmp_args_element_name_8 };
                    tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 287;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_10;
                }
                Py_DECREF( tmp_call_result_6 );
            }
            branch_no_3:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_10;
        }
        goto loop_start_7;
        loop_end_7:;
        goto try_end_9;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_7__iter_value );
        tmp_for_loop_7__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_7__for_iterator );
        Py_DECREF( tmp_for_loop_7__for_iterator );
        tmp_for_loop_7__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_7;
        // End of try:
        try_end_9:;
        Py_XDECREF( tmp_for_loop_7__iter_value );
        tmp_for_loop_7__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_7__for_iterator );
        Py_DECREF( tmp_for_loop_7__for_iterator );
        tmp_for_loop_7__for_iterator = NULL;

        {
            PyObject *tmp_called_instance_8;
            PyObject *tmp_call_result_7;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( var_class_names );
            tmp_called_instance_8 = var_class_names;
            CHECK_OBJECT( var_new_name );
            tmp_args_element_name_9 = var_new_name;
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 289;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_add, call_args );
            }

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_7;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_7;
        }
        goto loop_start_4;
        loop_end_4:;
        goto try_end_10;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_4__iter_value );
        tmp_for_loop_4__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
        Py_DECREF( tmp_for_loop_4__for_iterator );
        tmp_for_loop_4__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto try_except_handler_5;
        // End of try:
        try_end_10:;
        Py_XDECREF( tmp_for_loop_4__iter_value );
        tmp_for_loop_4__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
        Py_DECREF( tmp_for_loop_4__for_iterator );
        tmp_for_loop_4__for_iterator = NULL;

        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_10;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__parse_style_str );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__parse_style_str );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_parse_style_str" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 293;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }

            tmp_called_name_5 = tmp_mvar_value_3;
            CHECK_OBJECT( var_part );
            tmp_args_element_name_10 = var_part;
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 293;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 293;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = var_inline_attrs;
                var_inline_attrs = tmp_assign_source_34;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_call_result_8;
            PyObject *tmp_args_element_name_11;
            CHECK_OBJECT( var_list_of_attrs );
            tmp_called_instance_9 = var_list_of_attrs;
            CHECK_OBJECT( var_inline_attrs );
            tmp_args_element_name_11 = var_inline_attrs;
            frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 294;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 294;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_end_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 265;
        type_description_1 = "ooooooooooooooo";
        goto try_except_handler_5;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_11:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__merge_attrs );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__merge_attrs );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_merge_attrs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_4;
        CHECK_OBJECT( var_list_of_attrs );
        tmp_args_element_name_12 = var_list_of_attrs;
        frame_46343e520769db6726bd3f118076dfab->m_frame.f_lineno = 296;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 296;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46343e520769db6726bd3f118076dfab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_46343e520769db6726bd3f118076dfab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46343e520769db6726bd3f118076dfab );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_46343e520769db6726bd3f118076dfab, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_46343e520769db6726bd3f118076dfab->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_46343e520769db6726bd3f118076dfab, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_46343e520769db6726bd3f118076dfab,
        type_description_1,
        par_self,
        par_style_str,
        par_default,
        var_list_of_attrs,
        var_class_names,
        var_names,
        var_attr,
        var_part,
        var_new_class_names,
        var_p,
        var_new_name,
        var_combos,
        var_count,
        var_c2,
        var_inline_attrs
    );


    // Release cached frame.
    if ( frame_46343e520769db6726bd3f118076dfab == cache_frame_46343e520769db6726bd3f118076dfab )
    {
        Py_DECREF( frame_46343e520769db6726bd3f118076dfab );
    }
    cache_frame_46343e520769db6726bd3f118076dfab = NULL;

    assertFrameObject( frame_46343e520769db6726bd3f118076dfab );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_style_str );
    Py_DECREF( par_style_str );
    par_style_str = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    CHECK_OBJECT( (PyObject *)var_list_of_attrs );
    Py_DECREF( var_list_of_attrs );
    var_list_of_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var_class_names );
    Py_DECREF( var_class_names );
    var_class_names = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    Py_XDECREF( var_part );
    var_part = NULL;

    Py_XDECREF( var_new_class_names );
    var_new_class_names = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_new_name );
    var_new_name = NULL;

    Py_XDECREF( var_combos );
    var_combos = NULL;

    Py_XDECREF( var_count );
    var_count = NULL;

    Py_XDECREF( var_c2 );
    var_c2 = NULL;

    Py_XDECREF( var_inline_attrs );
    var_inline_attrs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_style_str );
    Py_DECREF( par_style_str );
    par_style_str = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    CHECK_OBJECT( (PyObject *)var_list_of_attrs );
    Py_DECREF( var_list_of_attrs );
    var_list_of_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var_class_names );
    Py_DECREF( var_class_names );
    var_class_names = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    Py_XDECREF( var_part );
    var_part = NULL;

    Py_XDECREF( var_new_class_names );
    var_new_class_names = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_new_name );
    var_new_name = NULL;

    Py_XDECREF( var_combos );
    var_combos = NULL;

    Py_XDECREF( var_count );
    var_count = NULL;

    Py_XDECREF( var_c2 );
    var_c2 = NULL;

    Py_XDECREF( var_inline_attrs );
    var_inline_attrs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_8_invalidation_hash( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c59078e4979f48e64e8e3b91755632f0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c59078e4979f48e64e8e3b91755632f0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c59078e4979f48e64e8e3b91755632f0, codeobj_c59078e4979f48e64e8e3b91755632f0, module_prompt_toolkit$styles$style, sizeof(void *) );
    frame_c59078e4979f48e64e8e3b91755632f0 = cache_frame_c59078e4979f48e64e8e3b91755632f0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c59078e4979f48e64e8e3b91755632f0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c59078e4979f48e64e8e3b91755632f0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_id_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_id_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_class_names_and_attrs );
        if ( tmp_id_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = PyLong_FromVoidPtr( tmp_id_arg_1 );
        Py_DECREF( tmp_id_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c59078e4979f48e64e8e3b91755632f0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c59078e4979f48e64e8e3b91755632f0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c59078e4979f48e64e8e3b91755632f0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c59078e4979f48e64e8e3b91755632f0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c59078e4979f48e64e8e3b91755632f0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c59078e4979f48e64e8e3b91755632f0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c59078e4979f48e64e8e3b91755632f0,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_c59078e4979f48e64e8e3b91755632f0 == cache_frame_c59078e4979f48e64e8e3b91755632f0 )
    {
        Py_DECREF( frame_c59078e4979f48e64e8e3b91755632f0 );
    }
    cache_frame_c59078e4979f48e64e8e3b91755632f0 = NULL;

    assertFrameObject( frame_c59078e4979f48e64e8e3b91755632f0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_8_invalidation_hash );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_8_invalidation_hash );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_9__merge_attrs( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_list_of_attrs = python_pars[ 0 ];
    PyObject *var__or = NULL;
    PyObject *outline_0_var_a = NULL;
    PyObject *outline_1_var_a = NULL;
    PyObject *outline_2_var_a = NULL;
    PyObject *outline_3_var_a = NULL;
    PyObject *outline_4_var_a = NULL;
    PyObject *outline_5_var_a = NULL;
    PyObject *outline_6_var_a = NULL;
    PyObject *outline_7_var_a = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    PyObject *tmp_listcomp_3__$0 = NULL;
    PyObject *tmp_listcomp_3__contraction = NULL;
    PyObject *tmp_listcomp_3__iter_value_0 = NULL;
    PyObject *tmp_listcomp_4__$0 = NULL;
    PyObject *tmp_listcomp_4__contraction = NULL;
    PyObject *tmp_listcomp_4__iter_value_0 = NULL;
    PyObject *tmp_listcomp_5__$0 = NULL;
    PyObject *tmp_listcomp_5__contraction = NULL;
    PyObject *tmp_listcomp_5__iter_value_0 = NULL;
    PyObject *tmp_listcomp_6__$0 = NULL;
    PyObject *tmp_listcomp_6__contraction = NULL;
    PyObject *tmp_listcomp_6__iter_value_0 = NULL;
    PyObject *tmp_listcomp_7__$0 = NULL;
    PyObject *tmp_listcomp_7__contraction = NULL;
    PyObject *tmp_listcomp_7__iter_value_0 = NULL;
    PyObject *tmp_listcomp_8__$0 = NULL;
    PyObject *tmp_listcomp_8__contraction = NULL;
    PyObject *tmp_listcomp_8__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_6a637ac17fcf2384c9063cb586fde7d3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_e97f894cee234ee49bd77eda907bc49f_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_e97f894cee234ee49bd77eda907bc49f_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_21282692aeb5fcf488820c1ff22e5765_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_21282692aeb5fcf488820c1ff22e5765_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    struct Nuitka_FrameObject *frame_b4b001164f03a3033d420cdf09338ea9_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_b4b001164f03a3033d420cdf09338ea9_4 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    struct Nuitka_FrameObject *frame_baf9490416e1e63c9e5694e9c0b9c318_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_baf9490416e1e63c9e5694e9c0b9c318_5 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    struct Nuitka_FrameObject *frame_81191b2c28d26781ff9f891146f1b5e8_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    static struct Nuitka_FrameObject *cache_frame_81191b2c28d26781ff9f891146f1b5e8_6 = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    struct Nuitka_FrameObject *frame_634b8a7a2963086040aad95fc83f076b_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    static struct Nuitka_FrameObject *cache_frame_634b8a7a2963086040aad95fc83f076b_7 = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    struct Nuitka_FrameObject *frame_1cc926fb2d7eec06c400f6f61016c5ac_8;
    NUITKA_MAY_BE_UNUSED char const *type_description_8 = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    static struct Nuitka_FrameObject *cache_frame_1cc926fb2d7eec06c400f6f61016c5ac_8 = NULL;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    struct Nuitka_FrameObject *frame_6c860282339a3115d86b5bb5c4691800_9;
    NUITKA_MAY_BE_UNUSED char const *type_description_9 = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    static struct Nuitka_FrameObject *cache_frame_6c860282339a3115d86b5bb5c4691800_9 = NULL;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    static struct Nuitka_FrameObject *cache_frame_6a637ac17fcf2384c9063cb586fde7d3 = NULL;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or(  );



        assert( var__or == NULL );
        var__or = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6a637ac17fcf2384c9063cb586fde7d3, codeobj_6a637ac17fcf2384c9063cb586fde7d3, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    frame_6a637ac17fcf2384c9063cb586fde7d3 = cache_frame_6a637ac17fcf2384c9063cb586fde7d3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6a637ac17fcf2384c9063cb586fde7d3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6a637ac17fcf2384c9063cb586fde7d3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_dircall_arg3_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dircall_arg1_3;
        PyObject *tmp_dircall_arg2_3;
        PyObject *tmp_dircall_arg3_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dircall_arg1_4;
        PyObject *tmp_dircall_arg2_4;
        PyObject *tmp_dircall_arg3_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dircall_arg1_5;
        PyObject *tmp_dircall_arg2_5;
        PyObject *tmp_dircall_arg3_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dircall_arg1_6;
        PyObject *tmp_dircall_arg2_6;
        PyObject *tmp_dircall_arg3_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_dircall_arg1_7;
        PyObject *tmp_dircall_arg2_7;
        PyObject *tmp_dircall_arg3_7;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_dircall_arg1_8;
        PyObject *tmp_dircall_arg2_8;
        PyObject *tmp_dircall_arg3_8;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Attrs );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Attrs );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Attrs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 314;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_color;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_1 = var__or;
        tmp_dircall_arg2_1 = const_tuple_str_empty_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_1 = par_list_of_attrs;
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_1 = "oo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_e97f894cee234ee49bd77eda907bc49f_2, codeobj_e97f894cee234ee49bd77eda907bc49f, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_e97f894cee234ee49bd77eda907bc49f_2 = cache_frame_e97f894cee234ee49bd77eda907bc49f_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_e97f894cee234ee49bd77eda907bc49f_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_e97f894cee234ee49bd77eda907bc49f_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 315;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_a;
                outline_0_var_a = tmp_assign_source_5;
                Py_INCREF( outline_0_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_a );
            tmp_source_name_1 = outline_0_var_a;
            tmp_append_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_color );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 315;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_dircall_arg3_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_dircall_arg3_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e97f894cee234ee49bd77eda907bc49f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_e97f894cee234ee49bd77eda907bc49f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e97f894cee234ee49bd77eda907bc49f_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_e97f894cee234ee49bd77eda907bc49f_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_e97f894cee234ee49bd77eda907bc49f_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_e97f894cee234ee49bd77eda907bc49f_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_e97f894cee234ee49bd77eda907bc49f_2,
            type_description_2,
            outline_0_var_a
        );


        // Release cached frame.
        if ( frame_e97f894cee234ee49bd77eda907bc49f_2 == cache_frame_e97f894cee234ee49bd77eda907bc49f_2 )
        {
            Py_DECREF( frame_e97f894cee234ee49bd77eda907bc49f_2 );
        }
        cache_frame_e97f894cee234ee49bd77eda907bc49f_2 = NULL;

        assertFrameObject( frame_e97f894cee234ee49bd77eda907bc49f_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_a );
        outline_0_var_a = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_a );
        outline_0_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_1:;
        exception_lineno = 315;
        goto frame_exception_exit_1;
        outline_result_1:;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_dict_value_1 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 8 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_bgcolor;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_2 = var__or;
        tmp_dircall_arg2_2 = const_tuple_str_empty_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_2;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_2 = par_list_of_attrs;
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 316;
                type_description_1 = "oo";
                goto try_except_handler_4;
            }
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_21282692aeb5fcf488820c1ff22e5765_3, codeobj_21282692aeb5fcf488820c1ff22e5765, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_21282692aeb5fcf488820c1ff22e5765_3 = cache_frame_21282692aeb5fcf488820c1ff22e5765_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_21282692aeb5fcf488820c1ff22e5765_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_21282692aeb5fcf488820c1ff22e5765_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_2 = tmp_listcomp_2__$0;
            tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 316;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_9 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_a;
                outline_1_var_a = tmp_assign_source_9;
                Py_INCREF( outline_1_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_2;
            PyObject *tmp_append_value_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_append_list_2 = tmp_listcomp_2__contraction;
            CHECK_OBJECT( outline_1_var_a );
            tmp_source_name_2 = outline_1_var_a;
            tmp_append_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_bgcolor );
            if ( tmp_append_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 316;
                type_description_2 = "o";
                goto try_except_handler_5;
            }
            assert( PyList_Check( tmp_append_list_2 ) );
            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
            Py_DECREF( tmp_append_value_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 316;
                type_description_2 = "o";
                goto try_except_handler_5;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_2 = "o";
            goto try_except_handler_5;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_dircall_arg3_2 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_dircall_arg3_2 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_21282692aeb5fcf488820c1ff22e5765_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_21282692aeb5fcf488820c1ff22e5765_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_4;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_21282692aeb5fcf488820c1ff22e5765_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_21282692aeb5fcf488820c1ff22e5765_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_21282692aeb5fcf488820c1ff22e5765_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_21282692aeb5fcf488820c1ff22e5765_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_21282692aeb5fcf488820c1ff22e5765_3,
            type_description_2,
            outline_1_var_a
        );


        // Release cached frame.
        if ( frame_21282692aeb5fcf488820c1ff22e5765_3 == cache_frame_21282692aeb5fcf488820c1ff22e5765_3 )
        {
            Py_DECREF( frame_21282692aeb5fcf488820c1ff22e5765_3 );
        }
        cache_frame_21282692aeb5fcf488820c1ff22e5765_3 = NULL;

        assertFrameObject( frame_21282692aeb5fcf488820c1ff22e5765_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "oo";
        goto try_except_handler_4;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( outline_1_var_a );
        outline_1_var_a = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_a );
        outline_1_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_2:;
        exception_lineno = 316;
        goto frame_exception_exit_1;
        outline_result_2:;
        Py_INCREF( tmp_dircall_arg1_2 );
        Py_INCREF( tmp_dircall_arg2_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
            tmp_dict_value_2 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 316;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_bold;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_3 = var__or;
        tmp_dircall_arg2_3 = const_tuple_false_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_iter_arg_3;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_3 = par_list_of_attrs;
            tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_1 = "oo";
                goto try_except_handler_6;
            }
            assert( tmp_listcomp_3__$0 == NULL );
            tmp_listcomp_3__$0 = tmp_assign_source_10;
        }
        {
            PyObject *tmp_assign_source_11;
            tmp_assign_source_11 = PyList_New( 0 );
            assert( tmp_listcomp_3__contraction == NULL );
            tmp_listcomp_3__contraction = tmp_assign_source_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_b4b001164f03a3033d420cdf09338ea9_4, codeobj_b4b001164f03a3033d420cdf09338ea9, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_b4b001164f03a3033d420cdf09338ea9_4 = cache_frame_b4b001164f03a3033d420cdf09338ea9_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_b4b001164f03a3033d420cdf09338ea9_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_b4b001164f03a3033d420cdf09338ea9_4 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_12;
            CHECK_OBJECT( tmp_listcomp_3__$0 );
            tmp_next_source_3 = tmp_listcomp_3__$0;
            tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 317;
                    goto try_except_handler_7;
                }
            }

            {
                PyObject *old = tmp_listcomp_3__iter_value_0;
                tmp_listcomp_3__iter_value_0 = tmp_assign_source_12;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_13;
            CHECK_OBJECT( tmp_listcomp_3__iter_value_0 );
            tmp_assign_source_13 = tmp_listcomp_3__iter_value_0;
            {
                PyObject *old = outline_2_var_a;
                outline_2_var_a = tmp_assign_source_13;
                Py_INCREF( outline_2_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_3;
            PyObject *tmp_append_value_3;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_listcomp_3__contraction );
            tmp_append_list_3 = tmp_listcomp_3__contraction;
            CHECK_OBJECT( outline_2_var_a );
            tmp_source_name_3 = outline_2_var_a;
            tmp_append_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_bold );
            if ( tmp_append_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto try_except_handler_7;
            }
            assert( PyList_Check( tmp_append_list_3 ) );
            tmp_res = PyList_Append( tmp_append_list_3, tmp_append_value_3 );
            Py_DECREF( tmp_append_value_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;
                type_description_2 = "o";
                goto try_except_handler_7;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_2 = "o";
            goto try_except_handler_7;
        }
        goto loop_start_3;
        loop_end_3:;
        CHECK_OBJECT( tmp_listcomp_3__contraction );
        tmp_dircall_arg3_3 = tmp_listcomp_3__contraction;
        Py_INCREF( tmp_dircall_arg3_3 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_3__$0 );
        Py_DECREF( tmp_listcomp_3__$0 );
        tmp_listcomp_3__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_3__contraction );
        Py_DECREF( tmp_listcomp_3__contraction );
        tmp_listcomp_3__contraction = NULL;

        Py_XDECREF( tmp_listcomp_3__iter_value_0 );
        tmp_listcomp_3__iter_value_0 = NULL;

        goto frame_return_exit_4;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_3__$0 );
        Py_DECREF( tmp_listcomp_3__$0 );
        tmp_listcomp_3__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_3__contraction );
        Py_DECREF( tmp_listcomp_3__contraction );
        tmp_listcomp_3__contraction = NULL;

        Py_XDECREF( tmp_listcomp_3__iter_value_0 );
        tmp_listcomp_3__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto frame_exception_exit_4;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_b4b001164f03a3033d420cdf09338ea9_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_return_exit_4:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_b4b001164f03a3033d420cdf09338ea9_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_6;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_b4b001164f03a3033d420cdf09338ea9_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_b4b001164f03a3033d420cdf09338ea9_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_b4b001164f03a3033d420cdf09338ea9_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_b4b001164f03a3033d420cdf09338ea9_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_b4b001164f03a3033d420cdf09338ea9_4,
            type_description_2,
            outline_2_var_a
        );


        // Release cached frame.
        if ( frame_b4b001164f03a3033d420cdf09338ea9_4 == cache_frame_b4b001164f03a3033d420cdf09338ea9_4 )
        {
            Py_DECREF( frame_b4b001164f03a3033d420cdf09338ea9_4 );
        }
        cache_frame_b4b001164f03a3033d420cdf09338ea9_4 = NULL;

        assertFrameObject( frame_b4b001164f03a3033d420cdf09338ea9_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;
        type_description_1 = "oo";
        goto try_except_handler_6;
        skip_nested_handling_3:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        Py_XDECREF( outline_2_var_a );
        outline_2_var_a = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_2_var_a );
        outline_2_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_3:;
        exception_lineno = 317;
        goto frame_exception_exit_1;
        outline_result_3:;
        Py_INCREF( tmp_dircall_arg1_3 );
        Py_INCREF( tmp_dircall_arg2_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3, tmp_dircall_arg2_3, tmp_dircall_arg3_3};
            tmp_dict_value_3 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 317;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_underline;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_4 = var__or;
        tmp_dircall_arg2_4 = const_tuple_false_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_iter_arg_4;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_4 = par_list_of_attrs;
            tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_4 );
            if ( tmp_assign_source_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 318;
                type_description_1 = "oo";
                goto try_except_handler_8;
            }
            assert( tmp_listcomp_4__$0 == NULL );
            tmp_listcomp_4__$0 = tmp_assign_source_14;
        }
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = PyList_New( 0 );
            assert( tmp_listcomp_4__contraction == NULL );
            tmp_listcomp_4__contraction = tmp_assign_source_15;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_baf9490416e1e63c9e5694e9c0b9c318_5, codeobj_baf9490416e1e63c9e5694e9c0b9c318, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_baf9490416e1e63c9e5694e9c0b9c318_5 = cache_frame_baf9490416e1e63c9e5694e9c0b9c318_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_baf9490416e1e63c9e5694e9c0b9c318_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_baf9490416e1e63c9e5694e9c0b9c318_5 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_4:;
        {
            PyObject *tmp_next_source_4;
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( tmp_listcomp_4__$0 );
            tmp_next_source_4 = tmp_listcomp_4__$0;
            tmp_assign_source_16 = ITERATOR_NEXT( tmp_next_source_4 );
            if ( tmp_assign_source_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_4;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 318;
                    goto try_except_handler_9;
                }
            }

            {
                PyObject *old = tmp_listcomp_4__iter_value_0;
                tmp_listcomp_4__iter_value_0 = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_17;
            CHECK_OBJECT( tmp_listcomp_4__iter_value_0 );
            tmp_assign_source_17 = tmp_listcomp_4__iter_value_0;
            {
                PyObject *old = outline_3_var_a;
                outline_3_var_a = tmp_assign_source_17;
                Py_INCREF( outline_3_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_4;
            PyObject *tmp_append_value_4;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_listcomp_4__contraction );
            tmp_append_list_4 = tmp_listcomp_4__contraction;
            CHECK_OBJECT( outline_3_var_a );
            tmp_source_name_4 = outline_3_var_a;
            tmp_append_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_underline );
            if ( tmp_append_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 318;
                type_description_2 = "o";
                goto try_except_handler_9;
            }
            assert( PyList_Check( tmp_append_list_4 ) );
            tmp_res = PyList_Append( tmp_append_list_4, tmp_append_value_4 );
            Py_DECREF( tmp_append_value_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 318;
                type_description_2 = "o";
                goto try_except_handler_9;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_2 = "o";
            goto try_except_handler_9;
        }
        goto loop_start_4;
        loop_end_4:;
        CHECK_OBJECT( tmp_listcomp_4__contraction );
        tmp_dircall_arg3_4 = tmp_listcomp_4__contraction;
        Py_INCREF( tmp_dircall_arg3_4 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__$0 );
        Py_DECREF( tmp_listcomp_4__$0 );
        tmp_listcomp_4__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__contraction );
        Py_DECREF( tmp_listcomp_4__contraction );
        tmp_listcomp_4__contraction = NULL;

        Py_XDECREF( tmp_listcomp_4__iter_value_0 );
        tmp_listcomp_4__iter_value_0 = NULL;

        goto frame_return_exit_5;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__$0 );
        Py_DECREF( tmp_listcomp_4__$0 );
        tmp_listcomp_4__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_4__contraction );
        Py_DECREF( tmp_listcomp_4__contraction );
        tmp_listcomp_4__contraction = NULL;

        Py_XDECREF( tmp_listcomp_4__iter_value_0 );
        tmp_listcomp_4__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_5;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_baf9490416e1e63c9e5694e9c0b9c318_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_return_exit_5:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_baf9490416e1e63c9e5694e9c0b9c318_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_8;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_baf9490416e1e63c9e5694e9c0b9c318_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_baf9490416e1e63c9e5694e9c0b9c318_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_baf9490416e1e63c9e5694e9c0b9c318_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_baf9490416e1e63c9e5694e9c0b9c318_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_baf9490416e1e63c9e5694e9c0b9c318_5,
            type_description_2,
            outline_3_var_a
        );


        // Release cached frame.
        if ( frame_baf9490416e1e63c9e5694e9c0b9c318_5 == cache_frame_baf9490416e1e63c9e5694e9c0b9c318_5 )
        {
            Py_DECREF( frame_baf9490416e1e63c9e5694e9c0b9c318_5 );
        }
        cache_frame_baf9490416e1e63c9e5694e9c0b9c318_5 = NULL;

        assertFrameObject( frame_baf9490416e1e63c9e5694e9c0b9c318_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;
        type_description_1 = "oo";
        goto try_except_handler_8;
        skip_nested_handling_4:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_8:;
        Py_XDECREF( outline_3_var_a );
        outline_3_var_a = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_3_var_a );
        outline_3_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_4:;
        exception_lineno = 318;
        goto frame_exception_exit_1;
        outline_result_4:;
        Py_INCREF( tmp_dircall_arg1_4 );
        Py_INCREF( tmp_dircall_arg2_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4, tmp_dircall_arg2_4, tmp_dircall_arg3_4};
            tmp_dict_value_4 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 318;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_italic;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_5 = var__or;
        tmp_dircall_arg2_5 = const_tuple_false_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_iter_arg_5;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_5 = par_list_of_attrs;
            tmp_assign_source_18 = MAKE_ITERATOR( tmp_iter_arg_5 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_1 = "oo";
                goto try_except_handler_10;
            }
            assert( tmp_listcomp_5__$0 == NULL );
            tmp_listcomp_5__$0 = tmp_assign_source_18;
        }
        {
            PyObject *tmp_assign_source_19;
            tmp_assign_source_19 = PyList_New( 0 );
            assert( tmp_listcomp_5__contraction == NULL );
            tmp_listcomp_5__contraction = tmp_assign_source_19;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_81191b2c28d26781ff9f891146f1b5e8_6, codeobj_81191b2c28d26781ff9f891146f1b5e8, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_81191b2c28d26781ff9f891146f1b5e8_6 = cache_frame_81191b2c28d26781ff9f891146f1b5e8_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_81191b2c28d26781ff9f891146f1b5e8_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_81191b2c28d26781ff9f891146f1b5e8_6 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_5:;
        {
            PyObject *tmp_next_source_5;
            PyObject *tmp_assign_source_20;
            CHECK_OBJECT( tmp_listcomp_5__$0 );
            tmp_next_source_5 = tmp_listcomp_5__$0;
            tmp_assign_source_20 = ITERATOR_NEXT( tmp_next_source_5 );
            if ( tmp_assign_source_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_5;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 319;
                    goto try_except_handler_11;
                }
            }

            {
                PyObject *old = tmp_listcomp_5__iter_value_0;
                tmp_listcomp_5__iter_value_0 = tmp_assign_source_20;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_21;
            CHECK_OBJECT( tmp_listcomp_5__iter_value_0 );
            tmp_assign_source_21 = tmp_listcomp_5__iter_value_0;
            {
                PyObject *old = outline_4_var_a;
                outline_4_var_a = tmp_assign_source_21;
                Py_INCREF( outline_4_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_5;
            PyObject *tmp_append_value_5;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_listcomp_5__contraction );
            tmp_append_list_5 = tmp_listcomp_5__contraction;
            CHECK_OBJECT( outline_4_var_a );
            tmp_source_name_5 = outline_4_var_a;
            tmp_append_value_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_italic );
            if ( tmp_append_value_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto try_except_handler_11;
            }
            assert( PyList_Check( tmp_append_list_5 ) );
            tmp_res = PyList_Append( tmp_append_list_5, tmp_append_value_5 );
            Py_DECREF( tmp_append_value_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 319;
                type_description_2 = "o";
                goto try_except_handler_11;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_2 = "o";
            goto try_except_handler_11;
        }
        goto loop_start_5;
        loop_end_5:;
        CHECK_OBJECT( tmp_listcomp_5__contraction );
        tmp_dircall_arg3_5 = tmp_listcomp_5__contraction;
        Py_INCREF( tmp_dircall_arg3_5 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_11:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_5__$0 );
        Py_DECREF( tmp_listcomp_5__$0 );
        tmp_listcomp_5__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_5__contraction );
        Py_DECREF( tmp_listcomp_5__contraction );
        tmp_listcomp_5__contraction = NULL;

        Py_XDECREF( tmp_listcomp_5__iter_value_0 );
        tmp_listcomp_5__iter_value_0 = NULL;

        goto frame_return_exit_6;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_5__$0 );
        Py_DECREF( tmp_listcomp_5__$0 );
        tmp_listcomp_5__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_5__contraction );
        Py_DECREF( tmp_listcomp_5__contraction );
        tmp_listcomp_5__contraction = NULL;

        Py_XDECREF( tmp_listcomp_5__iter_value_0 );
        tmp_listcomp_5__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto frame_exception_exit_6;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_81191b2c28d26781ff9f891146f1b5e8_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_return_exit_6:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_81191b2c28d26781ff9f891146f1b5e8_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_10;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_81191b2c28d26781ff9f891146f1b5e8_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_81191b2c28d26781ff9f891146f1b5e8_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_81191b2c28d26781ff9f891146f1b5e8_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_81191b2c28d26781ff9f891146f1b5e8_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_81191b2c28d26781ff9f891146f1b5e8_6,
            type_description_2,
            outline_4_var_a
        );


        // Release cached frame.
        if ( frame_81191b2c28d26781ff9f891146f1b5e8_6 == cache_frame_81191b2c28d26781ff9f891146f1b5e8_6 )
        {
            Py_DECREF( frame_81191b2c28d26781ff9f891146f1b5e8_6 );
        }
        cache_frame_81191b2c28d26781ff9f891146f1b5e8_6 = NULL;

        assertFrameObject( frame_81191b2c28d26781ff9f891146f1b5e8_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_5:;
        type_description_1 = "oo";
        goto try_except_handler_10;
        skip_nested_handling_5:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_10:;
        Py_XDECREF( outline_4_var_a );
        outline_4_var_a = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_4_var_a );
        outline_4_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_5:;
        exception_lineno = 319;
        goto frame_exception_exit_1;
        outline_result_5:;
        Py_INCREF( tmp_dircall_arg1_5 );
        Py_INCREF( tmp_dircall_arg2_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5, tmp_dircall_arg2_5, tmp_dircall_arg3_5};
            tmp_dict_value_5 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 319;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_blink;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_6 = var__or;
        tmp_dircall_arg2_6 = const_tuple_false_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_iter_arg_6;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_6 = par_list_of_attrs;
            tmp_assign_source_22 = MAKE_ITERATOR( tmp_iter_arg_6 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oo";
                goto try_except_handler_12;
            }
            assert( tmp_listcomp_6__$0 == NULL );
            tmp_listcomp_6__$0 = tmp_assign_source_22;
        }
        {
            PyObject *tmp_assign_source_23;
            tmp_assign_source_23 = PyList_New( 0 );
            assert( tmp_listcomp_6__contraction == NULL );
            tmp_listcomp_6__contraction = tmp_assign_source_23;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_634b8a7a2963086040aad95fc83f076b_7, codeobj_634b8a7a2963086040aad95fc83f076b, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_634b8a7a2963086040aad95fc83f076b_7 = cache_frame_634b8a7a2963086040aad95fc83f076b_7;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_634b8a7a2963086040aad95fc83f076b_7 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_634b8a7a2963086040aad95fc83f076b_7 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_6:;
        {
            PyObject *tmp_next_source_6;
            PyObject *tmp_assign_source_24;
            CHECK_OBJECT( tmp_listcomp_6__$0 );
            tmp_next_source_6 = tmp_listcomp_6__$0;
            tmp_assign_source_24 = ITERATOR_NEXT( tmp_next_source_6 );
            if ( tmp_assign_source_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_6;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 320;
                    goto try_except_handler_13;
                }
            }

            {
                PyObject *old = tmp_listcomp_6__iter_value_0;
                tmp_listcomp_6__iter_value_0 = tmp_assign_source_24;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_25;
            CHECK_OBJECT( tmp_listcomp_6__iter_value_0 );
            tmp_assign_source_25 = tmp_listcomp_6__iter_value_0;
            {
                PyObject *old = outline_5_var_a;
                outline_5_var_a = tmp_assign_source_25;
                Py_INCREF( outline_5_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_6;
            PyObject *tmp_append_value_6;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( tmp_listcomp_6__contraction );
            tmp_append_list_6 = tmp_listcomp_6__contraction;
            CHECK_OBJECT( outline_5_var_a );
            tmp_source_name_6 = outline_5_var_a;
            tmp_append_value_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_blink );
            if ( tmp_append_value_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_2 = "o";
                goto try_except_handler_13;
            }
            assert( PyList_Check( tmp_append_list_6 ) );
            tmp_res = PyList_Append( tmp_append_list_6, tmp_append_value_6 );
            Py_DECREF( tmp_append_value_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_2 = "o";
                goto try_except_handler_13;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 320;
            type_description_2 = "o";
            goto try_except_handler_13;
        }
        goto loop_start_6;
        loop_end_6:;
        CHECK_OBJECT( tmp_listcomp_6__contraction );
        tmp_dircall_arg3_6 = tmp_listcomp_6__contraction;
        Py_INCREF( tmp_dircall_arg3_6 );
        goto try_return_handler_13;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_13:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_6__$0 );
        Py_DECREF( tmp_listcomp_6__$0 );
        tmp_listcomp_6__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_6__contraction );
        Py_DECREF( tmp_listcomp_6__contraction );
        tmp_listcomp_6__contraction = NULL;

        Py_XDECREF( tmp_listcomp_6__iter_value_0 );
        tmp_listcomp_6__iter_value_0 = NULL;

        goto frame_return_exit_7;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_6__$0 );
        Py_DECREF( tmp_listcomp_6__$0 );
        tmp_listcomp_6__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_6__contraction );
        Py_DECREF( tmp_listcomp_6__contraction );
        tmp_listcomp_6__contraction = NULL;

        Py_XDECREF( tmp_listcomp_6__iter_value_0 );
        tmp_listcomp_6__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto frame_exception_exit_7;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_634b8a7a2963086040aad95fc83f076b_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_6;

        frame_return_exit_7:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_634b8a7a2963086040aad95fc83f076b_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_12;

        frame_exception_exit_7:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_634b8a7a2963086040aad95fc83f076b_7 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_634b8a7a2963086040aad95fc83f076b_7, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_634b8a7a2963086040aad95fc83f076b_7->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_634b8a7a2963086040aad95fc83f076b_7, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_634b8a7a2963086040aad95fc83f076b_7,
            type_description_2,
            outline_5_var_a
        );


        // Release cached frame.
        if ( frame_634b8a7a2963086040aad95fc83f076b_7 == cache_frame_634b8a7a2963086040aad95fc83f076b_7 )
        {
            Py_DECREF( frame_634b8a7a2963086040aad95fc83f076b_7 );
        }
        cache_frame_634b8a7a2963086040aad95fc83f076b_7 = NULL;

        assertFrameObject( frame_634b8a7a2963086040aad95fc83f076b_7 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_6;

        frame_no_exception_6:;
        goto skip_nested_handling_6;
        nested_frame_exit_6:;
        type_description_1 = "oo";
        goto try_except_handler_12;
        skip_nested_handling_6:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_12:;
        Py_XDECREF( outline_5_var_a );
        outline_5_var_a = NULL;

        goto outline_result_6;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_5_var_a );
        outline_5_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto outline_exception_6;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_6:;
        exception_lineno = 320;
        goto frame_exception_exit_1;
        outline_result_6:;
        Py_INCREF( tmp_dircall_arg1_6 );
        Py_INCREF( tmp_dircall_arg2_6 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_6, tmp_dircall_arg2_6, tmp_dircall_arg3_6};
            tmp_dict_value_6 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 320;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_reverse;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_7 = var__or;
        tmp_dircall_arg2_7 = const_tuple_false_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_iter_arg_7;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_7 = par_list_of_attrs;
            tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_7 );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_1 = "oo";
                goto try_except_handler_14;
            }
            assert( tmp_listcomp_7__$0 == NULL );
            tmp_listcomp_7__$0 = tmp_assign_source_26;
        }
        {
            PyObject *tmp_assign_source_27;
            tmp_assign_source_27 = PyList_New( 0 );
            assert( tmp_listcomp_7__contraction == NULL );
            tmp_listcomp_7__contraction = tmp_assign_source_27;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_1cc926fb2d7eec06c400f6f61016c5ac_8, codeobj_1cc926fb2d7eec06c400f6f61016c5ac, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_1cc926fb2d7eec06c400f6f61016c5ac_8 = cache_frame_1cc926fb2d7eec06c400f6f61016c5ac_8;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_7:;
        {
            PyObject *tmp_next_source_7;
            PyObject *tmp_assign_source_28;
            CHECK_OBJECT( tmp_listcomp_7__$0 );
            tmp_next_source_7 = tmp_listcomp_7__$0;
            tmp_assign_source_28 = ITERATOR_NEXT( tmp_next_source_7 );
            if ( tmp_assign_source_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_7;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 321;
                    goto try_except_handler_15;
                }
            }

            {
                PyObject *old = tmp_listcomp_7__iter_value_0;
                tmp_listcomp_7__iter_value_0 = tmp_assign_source_28;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_29;
            CHECK_OBJECT( tmp_listcomp_7__iter_value_0 );
            tmp_assign_source_29 = tmp_listcomp_7__iter_value_0;
            {
                PyObject *old = outline_6_var_a;
                outline_6_var_a = tmp_assign_source_29;
                Py_INCREF( outline_6_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_7;
            PyObject *tmp_append_value_7;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_listcomp_7__contraction );
            tmp_append_list_7 = tmp_listcomp_7__contraction;
            CHECK_OBJECT( outline_6_var_a );
            tmp_source_name_7 = outline_6_var_a;
            tmp_append_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_reverse );
            if ( tmp_append_value_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto try_except_handler_15;
            }
            assert( PyList_Check( tmp_append_list_7 ) );
            tmp_res = PyList_Append( tmp_append_list_7, tmp_append_value_7 );
            Py_DECREF( tmp_append_value_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_2 = "o";
                goto try_except_handler_15;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 321;
            type_description_2 = "o";
            goto try_except_handler_15;
        }
        goto loop_start_7;
        loop_end_7:;
        CHECK_OBJECT( tmp_listcomp_7__contraction );
        tmp_dircall_arg3_7 = tmp_listcomp_7__contraction;
        Py_INCREF( tmp_dircall_arg3_7 );
        goto try_return_handler_15;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_15:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_7__$0 );
        Py_DECREF( tmp_listcomp_7__$0 );
        tmp_listcomp_7__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_7__contraction );
        Py_DECREF( tmp_listcomp_7__contraction );
        tmp_listcomp_7__contraction = NULL;

        Py_XDECREF( tmp_listcomp_7__iter_value_0 );
        tmp_listcomp_7__iter_value_0 = NULL;

        goto frame_return_exit_8;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_7__$0 );
        Py_DECREF( tmp_listcomp_7__$0 );
        tmp_listcomp_7__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_7__contraction );
        Py_DECREF( tmp_listcomp_7__contraction );
        tmp_listcomp_7__contraction = NULL;

        Py_XDECREF( tmp_listcomp_7__iter_value_0 );
        tmp_listcomp_7__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto frame_exception_exit_8;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_7;

        frame_return_exit_8:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_14;

        frame_exception_exit_8:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_1cc926fb2d7eec06c400f6f61016c5ac_8, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_1cc926fb2d7eec06c400f6f61016c5ac_8->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_1cc926fb2d7eec06c400f6f61016c5ac_8, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_1cc926fb2d7eec06c400f6f61016c5ac_8,
            type_description_2,
            outline_6_var_a
        );


        // Release cached frame.
        if ( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 == cache_frame_1cc926fb2d7eec06c400f6f61016c5ac_8 )
        {
            Py_DECREF( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 );
        }
        cache_frame_1cc926fb2d7eec06c400f6f61016c5ac_8 = NULL;

        assertFrameObject( frame_1cc926fb2d7eec06c400f6f61016c5ac_8 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_7;

        frame_no_exception_7:;
        goto skip_nested_handling_7;
        nested_frame_exit_7:;
        type_description_1 = "oo";
        goto try_except_handler_14;
        skip_nested_handling_7:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_14:;
        Py_XDECREF( outline_6_var_a );
        outline_6_var_a = NULL;

        goto outline_result_7;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_6_var_a );
        outline_6_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto outline_exception_7;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_7:;
        exception_lineno = 321;
        goto frame_exception_exit_1;
        outline_result_7:;
        Py_INCREF( tmp_dircall_arg1_7 );
        Py_INCREF( tmp_dircall_arg2_7 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_7, tmp_dircall_arg2_7, tmp_dircall_arg3_7};
            tmp_dict_value_7 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 321;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_hidden;
        CHECK_OBJECT( var__or );
        tmp_dircall_arg1_8 = var__or;
        tmp_dircall_arg2_8 = const_tuple_false_tuple;
        // Tried code:
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_iter_arg_8;
            CHECK_OBJECT( par_list_of_attrs );
            tmp_iter_arg_8 = par_list_of_attrs;
            tmp_assign_source_30 = MAKE_ITERATOR( tmp_iter_arg_8 );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 322;
                type_description_1 = "oo";
                goto try_except_handler_16;
            }
            assert( tmp_listcomp_8__$0 == NULL );
            tmp_listcomp_8__$0 = tmp_assign_source_30;
        }
        {
            PyObject *tmp_assign_source_31;
            tmp_assign_source_31 = PyList_New( 0 );
            assert( tmp_listcomp_8__contraction == NULL );
            tmp_listcomp_8__contraction = tmp_assign_source_31;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_6c860282339a3115d86b5bb5c4691800_9, codeobj_6c860282339a3115d86b5bb5c4691800, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_6c860282339a3115d86b5bb5c4691800_9 = cache_frame_6c860282339a3115d86b5bb5c4691800_9;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_6c860282339a3115d86b5bb5c4691800_9 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_6c860282339a3115d86b5bb5c4691800_9 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_8:;
        {
            PyObject *tmp_next_source_8;
            PyObject *tmp_assign_source_32;
            CHECK_OBJECT( tmp_listcomp_8__$0 );
            tmp_next_source_8 = tmp_listcomp_8__$0;
            tmp_assign_source_32 = ITERATOR_NEXT( tmp_next_source_8 );
            if ( tmp_assign_source_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_8;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 322;
                    goto try_except_handler_17;
                }
            }

            {
                PyObject *old = tmp_listcomp_8__iter_value_0;
                tmp_listcomp_8__iter_value_0 = tmp_assign_source_32;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_33;
            CHECK_OBJECT( tmp_listcomp_8__iter_value_0 );
            tmp_assign_source_33 = tmp_listcomp_8__iter_value_0;
            {
                PyObject *old = outline_7_var_a;
                outline_7_var_a = tmp_assign_source_33;
                Py_INCREF( outline_7_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_8;
            PyObject *tmp_append_value_8;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( tmp_listcomp_8__contraction );
            tmp_append_list_8 = tmp_listcomp_8__contraction;
            CHECK_OBJECT( outline_7_var_a );
            tmp_source_name_8 = outline_7_var_a;
            tmp_append_value_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_hidden );
            if ( tmp_append_value_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 322;
                type_description_2 = "o";
                goto try_except_handler_17;
            }
            assert( PyList_Check( tmp_append_list_8 ) );
            tmp_res = PyList_Append( tmp_append_list_8, tmp_append_value_8 );
            Py_DECREF( tmp_append_value_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 322;
                type_description_2 = "o";
                goto try_except_handler_17;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;
            type_description_2 = "o";
            goto try_except_handler_17;
        }
        goto loop_start_8;
        loop_end_8:;
        CHECK_OBJECT( tmp_listcomp_8__contraction );
        tmp_dircall_arg3_8 = tmp_listcomp_8__contraction;
        Py_INCREF( tmp_dircall_arg3_8 );
        goto try_return_handler_17;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_17:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_8__$0 );
        Py_DECREF( tmp_listcomp_8__$0 );
        tmp_listcomp_8__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_8__contraction );
        Py_DECREF( tmp_listcomp_8__contraction );
        tmp_listcomp_8__contraction = NULL;

        Py_XDECREF( tmp_listcomp_8__iter_value_0 );
        tmp_listcomp_8__iter_value_0 = NULL;

        goto frame_return_exit_9;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_8__$0 );
        Py_DECREF( tmp_listcomp_8__$0 );
        tmp_listcomp_8__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_8__contraction );
        Py_DECREF( tmp_listcomp_8__contraction );
        tmp_listcomp_8__contraction = NULL;

        Py_XDECREF( tmp_listcomp_8__iter_value_0 );
        tmp_listcomp_8__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto frame_exception_exit_9;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6c860282339a3115d86b5bb5c4691800_9 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_8;

        frame_return_exit_9:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_6c860282339a3115d86b5bb5c4691800_9 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_16;

        frame_exception_exit_9:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_6c860282339a3115d86b5bb5c4691800_9 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_6c860282339a3115d86b5bb5c4691800_9, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_6c860282339a3115d86b5bb5c4691800_9->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_6c860282339a3115d86b5bb5c4691800_9, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_6c860282339a3115d86b5bb5c4691800_9,
            type_description_2,
            outline_7_var_a
        );


        // Release cached frame.
        if ( frame_6c860282339a3115d86b5bb5c4691800_9 == cache_frame_6c860282339a3115d86b5bb5c4691800_9 )
        {
            Py_DECREF( frame_6c860282339a3115d86b5bb5c4691800_9 );
        }
        cache_frame_6c860282339a3115d86b5bb5c4691800_9 = NULL;

        assertFrameObject( frame_6c860282339a3115d86b5bb5c4691800_9 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_8;

        frame_no_exception_8:;
        goto skip_nested_handling_8;
        nested_frame_exit_8:;
        type_description_1 = "oo";
        goto try_except_handler_16;
        skip_nested_handling_8:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        // Return handler code:
        try_return_handler_16:;
        Py_XDECREF( outline_7_var_a );
        outline_7_var_a = NULL;

        goto outline_result_8;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_7_var_a );
        outline_7_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto outline_exception_8;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
        return NULL;
        outline_exception_8:;
        exception_lineno = 322;
        goto frame_exception_exit_1;
        outline_result_8:;
        Py_INCREF( tmp_dircall_arg1_8 );
        Py_INCREF( tmp_dircall_arg2_8 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_8, tmp_dircall_arg2_8, tmp_dircall_arg3_8};
            tmp_dict_value_8 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_dict_value_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 322;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_6a637ac17fcf2384c9063cb586fde7d3->m_frame.f_lineno = 314;
        tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a637ac17fcf2384c9063cb586fde7d3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_9;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a637ac17fcf2384c9063cb586fde7d3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a637ac17fcf2384c9063cb586fde7d3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6a637ac17fcf2384c9063cb586fde7d3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6a637ac17fcf2384c9063cb586fde7d3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6a637ac17fcf2384c9063cb586fde7d3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6a637ac17fcf2384c9063cb586fde7d3,
        type_description_1,
        par_list_of_attrs,
        var__or
    );


    // Release cached frame.
    if ( frame_6a637ac17fcf2384c9063cb586fde7d3 == cache_frame_6a637ac17fcf2384c9063cb586fde7d3 )
    {
        Py_DECREF( frame_6a637ac17fcf2384c9063cb586fde7d3 );
    }
    cache_frame_6a637ac17fcf2384c9063cb586fde7d3 = NULL;

    assertFrameObject( frame_6a637ac17fcf2384c9063cb586fde7d3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_9:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_list_of_attrs );
    Py_DECREF( par_list_of_attrs );
    par_list_of_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var__or );
    Py_DECREF( var__or );
    var__or = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_list_of_attrs );
    Py_DECREF( par_list_of_attrs );
    par_list_of_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var__or );
    Py_DECREF( var__or );
    var__or = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_17;
    exception_value = exception_keeper_value_17;
    exception_tb = exception_keeper_tb_17;
    exception_lineno = exception_keeper_lineno_17;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_values = python_pars[ 0 ];
    PyObject *var_v = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_efe509c90dd5bac6257972e3587c609f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_efe509c90dd5bac6257972e3587c609f = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_efe509c90dd5bac6257972e3587c609f, codeobj_efe509c90dd5bac6257972e3587c609f, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    frame_efe509c90dd5bac6257972e3587c609f = cache_frame_efe509c90dd5bac6257972e3587c609f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_efe509c90dd5bac6257972e3587c609f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_efe509c90dd5bac6257972e3587c609f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_values );
        tmp_subscribed_name_1 = par_values;
        tmp_subscript_name_1 = const_slice_none_none_int_neg_1;
        tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                exception_lineno = 310;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_3;
            Py_INCREF( var_v );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_v );
        tmp_compexpr_left_1 = var_v;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( var_v );
        tmp_return_value = var_v;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_2;
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 310;
        type_description_1 = "oo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_efe509c90dd5bac6257972e3587c609f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_efe509c90dd5bac6257972e3587c609f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_efe509c90dd5bac6257972e3587c609f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_efe509c90dd5bac6257972e3587c609f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_efe509c90dd5bac6257972e3587c609f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_efe509c90dd5bac6257972e3587c609f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_efe509c90dd5bac6257972e3587c609f,
        type_description_1,
        par_values,
        var_v
    );


    // Release cached frame.
    if ( frame_efe509c90dd5bac6257972e3587c609f == cache_frame_efe509c90dd5bac6257972e3587c609f )
    {
        Py_DECREF( frame_efe509c90dd5bac6257972e3587c609f );
    }
    cache_frame_efe509c90dd5bac6257972e3587c609f = NULL;

    assertFrameObject( frame_efe509c90dd5bac6257972e3587c609f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_values );
    Py_DECREF( par_values );
    par_values = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_values );
    Py_DECREF( par_values );
    par_values = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_10_merge_styles( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_styles = python_pars[ 0 ];
    PyObject *outline_0_var_s = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_ff1ef7f2fa661a5079acef48c017cd49;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_fadc48548147d75e6d5a8d3f7f9ae15a_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_ff1ef7f2fa661a5079acef48c017cd49 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ff1ef7f2fa661a5079acef48c017cd49, codeobj_ff1ef7f2fa661a5079acef48c017cd49, module_prompt_toolkit$styles$style, sizeof(void *) );
    frame_ff1ef7f2fa661a5079acef48c017cd49 = cache_frame_ff1ef7f2fa661a5079acef48c017cd49;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ff1ef7f2fa661a5079acef48c017cd49 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ff1ef7f2fa661a5079acef48c017cd49 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_styles );
            tmp_iter_arg_1 = par_styles;
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 329;
                type_description_1 = "o";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_fadc48548147d75e6d5a8d3f7f9ae15a_2, codeobj_fadc48548147d75e6d5a8d3f7f9ae15a, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 = cache_frame_fadc48548147d75e6d5a8d3f7f9ae15a_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 329;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_s;
                outline_0_var_s = tmp_assign_source_5;
                Py_INCREF( outline_0_var_s );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_1;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( outline_0_var_s );
            tmp_compexpr_left_1 = outline_0_var_s;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_1;
            }
            else
            {
                goto branch_no_1;
            }
            branch_yes_1:;
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_s );
                tmp_append_value_1 = outline_0_var_s;
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 329;
                    type_description_2 = "o";
                    goto try_except_handler_3;
                }
            }
            branch_no_1:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_10_merge_styles );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_fadc48548147d75e6d5a8d3f7f9ae15a_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_fadc48548147d75e6d5a8d3f7f9ae15a_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_fadc48548147d75e6d5a8d3f7f9ae15a_2,
            type_description_2,
            outline_0_var_s
        );


        // Release cached frame.
        if ( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 == cache_frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 )
        {
            Py_DECREF( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 );
        }
        cache_frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 = NULL;

        assertFrameObject( frame_fadc48548147d75e6d5a8d3f7f9ae15a_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "o";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_10_merge_styles );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_s );
        outline_0_var_s = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_s );
        outline_0_var_s = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_10_merge_styles );
        return NULL;
        outline_exception_1:;
        exception_lineno = 329;
        goto frame_exception_exit_1;
        outline_result_1:;
        {
            PyObject *old = par_styles;
            assert( old != NULL );
            par_styles = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__MergedStyle );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__MergedStyle );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_MergedStyle" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 330;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_styles );
        tmp_args_element_name_1 = par_styles;
        frame_ff1ef7f2fa661a5079acef48c017cd49->m_frame.f_lineno = 330;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 330;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff1ef7f2fa661a5079acef48c017cd49 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff1ef7f2fa661a5079acef48c017cd49 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ff1ef7f2fa661a5079acef48c017cd49 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ff1ef7f2fa661a5079acef48c017cd49, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ff1ef7f2fa661a5079acef48c017cd49->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ff1ef7f2fa661a5079acef48c017cd49, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ff1ef7f2fa661a5079acef48c017cd49,
        type_description_1,
        par_styles
    );


    // Release cached frame.
    if ( frame_ff1ef7f2fa661a5079acef48c017cd49 == cache_frame_ff1ef7f2fa661a5079acef48c017cd49 )
    {
        Py_DECREF( frame_ff1ef7f2fa661a5079acef48c017cd49 );
    }
    cache_frame_ff1ef7f2fa661a5079acef48c017cd49 = NULL;

    assertFrameObject( frame_ff1ef7f2fa661a5079acef48c017cd49 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_10_merge_styles );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_styles );
    Py_DECREF( par_styles );
    par_styles = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_styles );
    Py_DECREF( par_styles );
    par_styles = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_10_merge_styles );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_11___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_styles = python_pars[ 1 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_f3562bd77c1aec8c9508f6fabcf4263c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_f3562bd77c1aec8c9508f6fabcf4263c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f3562bd77c1aec8c9508f6fabcf4263c, codeobj_f3562bd77c1aec8c9508f6fabcf4263c, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    frame_f3562bd77c1aec8c9508f6fabcf4263c = cache_frame_f3562bd77c1aec8c9508f6fabcf4263c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f3562bd77c1aec8c9508f6fabcf4263c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f3562bd77c1aec8c9508f6fabcf4263c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_all );
        assert( tmp_called_name_1 != NULL );
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_styles );
            tmp_iter_arg_1 = par_styles;
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 349;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_args_element_name_1 = prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_11___init__ );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_11___init__ );
        return NULL;
        outline_result_1:;
        frame_f3562bd77c1aec8c9508f6fabcf4263c->m_frame.f_lineno = 349;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 349;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_styles );
        tmp_assattr_name_1 = par_styles;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_styles, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 351;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_SimpleCache );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SimpleCache );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SimpleCache" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 352;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        tmp_kw_name_1 = PyDict_Copy( const_dict_9cebe5e8e339af1b92c19ff6455c60db );
        frame_f3562bd77c1aec8c9508f6fabcf4263c->m_frame.f_lineno = 352;
        tmp_assattr_name_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 352;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__style, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 352;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f3562bd77c1aec8c9508f6fabcf4263c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f3562bd77c1aec8c9508f6fabcf4263c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f3562bd77c1aec8c9508f6fabcf4263c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f3562bd77c1aec8c9508f6fabcf4263c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f3562bd77c1aec8c9508f6fabcf4263c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f3562bd77c1aec8c9508f6fabcf4263c,
        type_description_1,
        par_self,
        par_styles
    );


    // Release cached frame.
    if ( frame_f3562bd77c1aec8c9508f6fabcf4263c == cache_frame_f3562bd77c1aec8c9508f6fabcf4263c )
    {
        Py_DECREF( frame_f3562bd77c1aec8c9508f6fabcf4263c );
    }
    cache_frame_f3562bd77c1aec8c9508f6fabcf4263c = NULL;

    assertFrameObject( frame_f3562bd77c1aec8c9508f6fabcf4263c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_11___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_styles );
    Py_DECREF( par_styles );
    par_styles = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_styles );
    Py_DECREF( par_styles );
    par_styles = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_11___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_locals {
    PyObject *var_style;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_style = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_13075177ec6ed53f119134c2dd701cef, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 349;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_style;
            generator_heap->var_style = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_style );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_style );
        tmp_isinstance_inst_1 = generator_heap->var_style;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_BaseStyle );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BaseStyle );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BaseStyle" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 349;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        generator_heap->tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 349;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ( generator_heap->tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_expression_name_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_isinstance_inst_1, sizeof(PyObject *), &tmp_isinstance_cls_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_isinstance_inst_1, sizeof(PyObject *), &tmp_isinstance_cls_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 349;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 349;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_style
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_style );
    generator_heap->var_style = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_style );
    generator_heap->var_style = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$styles$style,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_97d512a018c5d28ce3f196729552a02b,
#endif
        codeobj_13075177ec6ed53f119134c2dd701cef,
        1,
        sizeof(struct prompt_toolkit$styles$style$$$function_11___init__$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_12__merged_style( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_get = NULL;
    struct Nuitka_FrameObject *frame_e9dd8c8eab0cd0a25c2926d6e167cbbc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e9dd8c8eab0cd0a25c2926d6e167cbbc = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style$$$function_1_get(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_self;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_get == NULL );
        var_get = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e9dd8c8eab0cd0a25c2926d6e167cbbc, codeobj_e9dd8c8eab0cd0a25c2926d6e167cbbc, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    frame_e9dd8c8eab0cd0a25c2926d6e167cbbc = cache_frame_e9dd8c8eab0cd0a25c2926d6e167cbbc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( PyCell_GET( par_self ) );
        tmp_source_name_2 = PyCell_GET( par_self );
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__style );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_self ) );
        tmp_called_instance_1 = PyCell_GET( par_self );
        frame_e9dd8c8eab0cd0a25c2926d6e167cbbc->m_frame.f_lineno = 359;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_invalidation_hash );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 359;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_get );
        tmp_args_element_name_2 = var_get;
        frame_e9dd8c8eab0cd0a25c2926d6e167cbbc->m_frame.f_lineno = 359;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e9dd8c8eab0cd0a25c2926d6e167cbbc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e9dd8c8eab0cd0a25c2926d6e167cbbc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e9dd8c8eab0cd0a25c2926d6e167cbbc,
        type_description_1,
        par_self,
        var_get
    );


    // Release cached frame.
    if ( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc == cache_frame_e9dd8c8eab0cd0a25c2926d6e167cbbc )
    {
        Py_DECREF( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc );
    }
    cache_frame_e9dd8c8eab0cd0a25c2926d6e167cbbc = NULL;

    assertFrameObject( frame_e9dd8c8eab0cd0a25c2926d6e167cbbc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_12__merged_style );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_get );
    Py_DECREF( var_get );
    var_get = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_get );
    Py_DECREF( var_get );
    var_get = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_12__merged_style );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_12__merged_style$$$function_1_get( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_50093a0c2c2673fa118f6ebc5d4d4282;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_50093a0c2c2673fa118f6ebc5d4d4282 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_50093a0c2c2673fa118f6ebc5d4d4282, codeobj_50093a0c2c2673fa118f6ebc5d4d4282, module_prompt_toolkit$styles$style, sizeof(void *) );
    frame_50093a0c2c2673fa118f6ebc5d4d4282 = cache_frame_50093a0c2c2673fa118f6ebc5d4d4282;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_50093a0c2c2673fa118f6ebc5d4d4282 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_50093a0c2c2673fa118f6ebc5d4d4282 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Style );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Style );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Style" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_style_rules );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        frame_50093a0c2c2673fa118f6ebc5d4d4282->m_frame.f_lineno = 358;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "c";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50093a0c2c2673fa118f6ebc5d4d4282 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_50093a0c2c2673fa118f6ebc5d4d4282 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50093a0c2c2673fa118f6ebc5d4d4282 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_50093a0c2c2673fa118f6ebc5d4d4282, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_50093a0c2c2673fa118f6ebc5d4d4282->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_50093a0c2c2673fa118f6ebc5d4d4282, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_50093a0c2c2673fa118f6ebc5d4d4282,
        type_description_1,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_50093a0c2c2673fa118f6ebc5d4d4282 == cache_frame_50093a0c2c2673fa118f6ebc5d4d4282 )
    {
        Py_DECREF( frame_50093a0c2c2673fa118f6ebc5d4d4282 );
    }
    cache_frame_50093a0c2c2673fa118f6ebc5d4d4282 = NULL;

    assertFrameObject( frame_50093a0c2c2673fa118f6ebc5d4d4282 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_12__merged_style$$$function_1_get );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_13_style_rules( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_style_rules = NULL;
    PyObject *var_s = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_dac7c133e5149ac106fa2011c3316895;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_dac7c133e5149ac106fa2011c3316895 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_style_rules == NULL );
        var_style_rules = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dac7c133e5149ac106fa2011c3316895, codeobj_dac7c133e5149ac106fa2011c3316895, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_dac7c133e5149ac106fa2011c3316895 = cache_frame_dac7c133e5149ac106fa2011c3316895;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dac7c133e5149ac106fa2011c3316895 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dac7c133e5149ac106fa2011c3316895 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_styles );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 364;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_s;
            var_s = tmp_assign_source_4;
            Py_INCREF( var_s );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_style_rules );
        tmp_source_name_2 = var_style_rules;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_extend );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_s );
        tmp_source_name_3 = var_s;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_style_rules );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 365;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        frame_dac7c133e5149ac106fa2011c3316895->m_frame.f_lineno = 365;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 364;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dac7c133e5149ac106fa2011c3316895 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dac7c133e5149ac106fa2011c3316895 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dac7c133e5149ac106fa2011c3316895, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dac7c133e5149ac106fa2011c3316895->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dac7c133e5149ac106fa2011c3316895, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dac7c133e5149ac106fa2011c3316895,
        type_description_1,
        par_self,
        var_style_rules,
        var_s
    );


    // Release cached frame.
    if ( frame_dac7c133e5149ac106fa2011c3316895 == cache_frame_dac7c133e5149ac106fa2011c3316895 )
    {
        Py_DECREF( frame_dac7c133e5149ac106fa2011c3316895 );
    }
    cache_frame_dac7c133e5149ac106fa2011c3316895 = NULL;

    assertFrameObject( frame_dac7c133e5149ac106fa2011c3316895 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_style_rules );
    tmp_return_value = var_style_rules;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_13_style_rules );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_style_rules );
    Py_DECREF( var_style_rules );
    var_style_rules = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_style_rules );
    Py_DECREF( var_style_rules );
    var_style_rules = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_13_style_rules );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_style_str = python_pars[ 1 ];
    PyObject *par_default = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1a6ee3024f37feb13a005e47a16e621b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1a6ee3024f37feb13a005e47a16e621b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1a6ee3024f37feb13a005e47a16e621b, codeobj_1a6ee3024f37feb13a005e47a16e621b, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1a6ee3024f37feb13a005e47a16e621b = cache_frame_1a6ee3024f37feb13a005e47a16e621b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1a6ee3024f37feb13a005e47a16e621b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1a6ee3024f37feb13a005e47a16e621b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__merged_style );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_style_str );
        tmp_args_element_name_1 = par_style_str;
        CHECK_OBJECT( par_default );
        tmp_args_element_name_2 = par_default;
        frame_1a6ee3024f37feb13a005e47a16e621b->m_frame.f_lineno = 369;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get_attrs_for_style_str, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1a6ee3024f37feb13a005e47a16e621b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1a6ee3024f37feb13a005e47a16e621b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1a6ee3024f37feb13a005e47a16e621b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1a6ee3024f37feb13a005e47a16e621b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1a6ee3024f37feb13a005e47a16e621b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1a6ee3024f37feb13a005e47a16e621b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1a6ee3024f37feb13a005e47a16e621b,
        type_description_1,
        par_self,
        par_style_str,
        par_default
    );


    // Release cached frame.
    if ( frame_1a6ee3024f37feb13a005e47a16e621b == cache_frame_1a6ee3024f37feb13a005e47a16e621b )
    {
        Py_DECREF( frame_1a6ee3024f37feb13a005e47a16e621b );
    }
    cache_frame_1a6ee3024f37feb13a005e47a16e621b = NULL;

    assertFrameObject( frame_1a6ee3024f37feb13a005e47a16e621b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_style_str );
    Py_DECREF( par_style_str );
    par_style_str = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_style_str );
    Py_DECREF( par_style_str );
    par_style_str = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$styles$style$$$function_15_invalidation_hash( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d, codeobj_c8dec6dabe0b6ae8ddc8c1fbbcad346d, module_prompt_toolkit$styles$style, sizeof(void *) );
    frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d = cache_frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_tuple_arg_1;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_1 = par_self;
            tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_styles );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 372;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 372;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_tuple_arg_1 = prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_tuple_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_15_invalidation_hash );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_15_invalidation_hash );
        return NULL;
        outline_result_1:;
        tmp_return_value = PySequence_Tuple( tmp_tuple_arg_1 );
        Py_DECREF( tmp_tuple_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 372;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d == cache_frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d )
    {
        Py_DECREF( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d );
    }
    cache_frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d = NULL;

    assertFrameObject( frame_c8dec6dabe0b6ae8ddc8c1fbbcad346d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_15_invalidation_hash );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style$$$function_15_invalidation_hash );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_locals {
    PyObject *var_s;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_s = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5e1fa19d313cfe381aa6dba097b1d284, module_prompt_toolkit$styles$style, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 372;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_s;
            generator_heap->var_s = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_s );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_s );
        tmp_called_instance_1 = generator_heap->var_s;
        generator->m_frame->m_frame.f_lineno = 372;
        tmp_expression_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_invalidation_hash );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 372;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 372;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 372;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_s
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$styles$style,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_1f86dfbfc872b260c34f936393749341,
#endif
        codeobj_5e1fa19d313cfe381aa6dba097b1d284,
        1,
        sizeof(struct prompt_toolkit$styles$style$$$function_15_invalidation_hash$$$genexpr_1_genexpr_locals)
    );
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_10_merge_styles(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_10_merge_styles,
        const_str_plain_merge_styles,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ff1ef7f2fa661a5079acef48c017cd49,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_97ef8aab1125d13cd336fe8e99d6c41c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_11___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_11___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_39c92ff1ce0df0e177ad7ebcfc00940b,
#endif
        codeobj_f3562bd77c1aec8c9508f6fabcf4263c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_12__merged_style,
        const_str_plain__merged_style,
#if PYTHON_VERSION >= 300
        const_str_digest_c7bcb3bb1990002fb3110ff9c8be69ea,
#endif
        codeobj_e9dd8c8eab0cd0a25c2926d6e167cbbc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_887fd040eff1b3854508a5fdf7741541,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style$$$function_1_get(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_12__merged_style$$$function_1_get,
        const_str_plain_get,
#if PYTHON_VERSION >= 300
        const_str_digest_2a05c7d88909eec54fdd707adc7a3ca7,
#endif
        codeobj_50093a0c2c2673fa118f6ebc5d4d4282,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_13_style_rules(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_13_style_rules,
        const_str_plain_style_rules,
#if PYTHON_VERSION >= 300
        const_str_digest_203d51646015f8d807838193fada14f3,
#endif
        codeobj_dac7c133e5149ac106fa2011c3316895,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str,
        const_str_plain_get_attrs_for_style_str,
#if PYTHON_VERSION >= 300
        const_str_digest_12a998220967f141b9b43b9a76571d53,
#endif
        codeobj_1a6ee3024f37feb13a005e47a16e621b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_15_invalidation_hash(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_15_invalidation_hash,
        const_str_plain_invalidation_hash,
#if PYTHON_VERSION >= 300
        const_str_digest_d77b3a90020c5798da9795100e0e0473,
#endif
        codeobj_c8dec6dabe0b6ae8ddc8c1fbbcad346d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_1_parse_color(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_1_parse_color,
        const_str_plain_parse_color,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1cbf91231e6347669238443f05030aa3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_932a3e82dba3b9bafdbe544436b2bd07,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_2__expand_classname(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_2__expand_classname,
        const_str_plain__expand_classname,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cfc5159abc4115925f0d1731e506f4a0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_dd962ba54e036373b889cc52e17b49e8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_3__parse_style_str(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_3__parse_style_str,
        const_str_plain__parse_style_str,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d4f69ab891d763c49c51182a5edf512b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_1407764f6b6711f9b3bc2282002e3660,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_4___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_4___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_c6b48af0f15c2ae1b16cff1d896cd5e2,
#endif
        codeobj_297839a72227d66dabefa647f05bd6a6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_5_style_rules(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_5_style_rules,
        const_str_plain_style_rules,
#if PYTHON_VERSION >= 300
        const_str_digest_9e071caae90aa3b849c0b822a5a20520,
#endif
        codeobj_499daadb8d1c0bc8c11425a0b9d1d69a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_6_from_dict,
        const_str_plain_from_dict,
#if PYTHON_VERSION >= 300
        const_str_digest_7303bc0c82644766e24f5f5df5b5795e,
#endif
        codeobj_bf9ec9bffa6966be9cdb21ecb7bba484,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_fb78513695615235f1a6d5cf86686c09,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_6_from_dict$$$function_1_key,
        const_str_plain_key,
#if PYTHON_VERSION >= 300
        const_str_digest_2a099de932416571369332a839857661,
#endif
        codeobj_480159fc265e16f5844c0552e15bd18f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str,
        const_str_plain_get_attrs_for_style_str,
#if PYTHON_VERSION >= 300
        const_str_digest_fe2155e54b379def61c84e8893a851a7,
#endif
        codeobj_46343e520769db6726bd3f118076dfab,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_0383abc0192e69ed29885314af8deab7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_8_invalidation_hash(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_8_invalidation_hash,
        const_str_plain_invalidation_hash,
#if PYTHON_VERSION >= 300
        const_str_digest_48a933953d719432d3a3e25030df4190,
#endif
        codeobj_c59078e4979f48e64e8e3b91755632f0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_9__merge_attrs(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_9__merge_attrs,
        const_str_plain__merge_attrs,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6a637ac17fcf2384c9063cb586fde7d3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_30f57ccd20ef5453da9844c1efba42ea,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$styles$style$$$function_9__merge_attrs$$$function_1__or,
        const_str_plain__or,
#if PYTHON_VERSION >= 300
        const_str_digest_249676a808dd31d5f4bc74f3fab6177b,
#endif
        codeobj_efe509c90dd5bac6257972e3587c609f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$styles$style,
        const_str_digest_87d99b725539b32c70b78c33a71d1e6b,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$styles$style =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.styles.style",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$styles$style)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$styles$style)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$styles$style );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.styles.style: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.styles.style: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.styles.style: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$styles$style" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$styles$style = Py_InitModule4(
        "prompt_toolkit.styles.style",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$styles$style = PyModule_Create( &mdef_prompt_toolkit$styles$style );
#endif

    moduledict_prompt_toolkit$styles$style = MODULE_DICT( module_prompt_toolkit$styles$style );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$styles$style,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$styles$style,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$styles$style,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$styles$style,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$styles$style );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_7f01f8a02212dab8d7b62920b65eb392, module_prompt_toolkit$styles$style );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_a15d48e1ef886f9277cadf9c3fb2034e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_prompt_toolkit$styles$style_157 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_prompt_toolkit$styles$style_189 = NULL;
    struct Nuitka_FrameObject *frame_ced792830c6b3aa30a9736212caeb0ed_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_ced792830c6b3aa30a9736212caeb0ed_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_prompt_toolkit$styles$style_333 = NULL;
    struct Nuitka_FrameObject *frame_3307939780071f890ee385f4804005da_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3307939780071f890ee385f4804005da_4 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_0dbb553e8fbc140d7df3be1eb49afac9;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_a15d48e1ef886f9277cadf9c3fb2034e = MAKE_MODULE_FRAME( codeobj_a15d48e1ef886f9277cadf9c3fb2034e, module_prompt_toolkit$styles$style );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_a15d48e1ef886f9277cadf9c3fb2034e );
    assert( Py_REFCNT( frame_a15d48e1ef886f9277cadf9c3fb2034e ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 4;
        tmp_assign_source_4 = PyImport_ImportModule("__future__");
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_import_from_1__module == NULL );
        Py_INCREF( tmp_assign_source_4 );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_absolute_import );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_itertools;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$styles$style;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 5;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_7 == NULL) );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_itertools, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_re;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$styles$style;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 6;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_sys;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$styles$style;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 7;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        assert( !(tmp_assign_source_9 == NULL) );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_base;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$styles$style;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_bd06d735be1796192b90f0c898f1da8c_tuple;
        tmp_level_name_4 = const_int_pos_1;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 8;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_3 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_prompt_toolkit$styles$style,
                const_str_plain_BaseStyle,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_BaseStyle );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_BaseStyle, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_prompt_toolkit$styles$style,
                const_str_plain_DEFAULT_ATTRS,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_DEFAULT_ATTRS );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_prompt_toolkit$styles$style,
                const_str_plain_ANSI_COLOR_NAMES,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_ANSI_COLOR_NAMES );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_prompt_toolkit$styles$style,
                const_str_plain_ANSI_COLOR_NAMES_ALIASES,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_ANSI_COLOR_NAMES_ALIASES );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_ANSI_COLOR_NAMES_ALIASES, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_prompt_toolkit$styles$style,
                const_str_plain_Attrs,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Attrs );
        }

        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Attrs, tmp_assign_source_15 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_named_colors;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$styles$style;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_NAMED_COLORS_tuple;
        tmp_level_name_5 = const_int_pos_1;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 9;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_16 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_prompt_toolkit$styles$style,
                const_str_plain_NAMED_COLORS,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_NAMED_COLORS );
        }

        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_NAMED_COLORS, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_af79046d82e88e002b260ea4f0738b6b;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$styles$style;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_SimpleCache_tuple;
        tmp_level_name_6 = const_int_0;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 10;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_SimpleCache );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_SimpleCache, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = LIST_COPY( const_list_9b4875dc865751f38c5731f75997792e_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_dict_seq_1;
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_NAMED_COLORS );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAMED_COLORS );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAMED_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_3;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 20;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_20 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_20;
        }
        // Tried code:
        tmp_dict_seq_1 = prompt_toolkit$styles$style$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_dict_seq_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        outline_result_1:;
        tmp_assign_source_19 = TO_DICT( tmp_dict_seq_1, NULL );
        Py_DECREF( tmp_dict_seq_1 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__named_colors_lowercase, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_1_parse_color(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_parse_color, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Attrs );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Attrs );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Attrs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_4;
        tmp_kw_name_1 = PyDict_Copy( const_dict_8ce99e3cf979f423b4467cc99c456ab9 );
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 71;
        tmp_assign_source_22 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__EMPTY_ATTRS, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_2__expand_classname(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__expand_classname, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_3__parse_style_str(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__parse_style_str, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_5;
        frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 154;
        tmp_assign_source_25 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_880d7c26405f7490c45b0304c064c26c_tuple, 0 ) );

        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_CLASS_NAMES_RE, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_26;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_1:;
        tmp_bases_name_1 = const_tuple_empty;
        tmp_assign_source_27 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_27;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_4;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_4;
            }
            tmp_args_name_1 = const_tuple_str_plain_Priority_tuple_empty_tuple;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 157;
            tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_28;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_4;
            }
            tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_1;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 157;

                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_1 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_1 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 157;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 157;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 157;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_29;
            tmp_assign_source_29 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_29;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_30;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_prompt_toolkit$styles$style_157 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_7f01f8a02212dab8d7b62920b65eb392;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_157, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_digest_03c6720c104c9f7b2ed5075c12663074;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_157, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_Priority;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_157, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2, codeobj_0a71b46f9cd9d9d57f0972b1b7f13fda, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 = cache_frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = const_str_plain_KEY_ORDER;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_157, const_str_plain_DICT_KEY_ORDER, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_str_plain_MOST_PRECISE;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_157, const_str_plain_MOST_PRECISE, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_list_element_1;
            tmp_list_element_1 = PyObject_GetItem( locals_prompt_toolkit$styles$style_157, const_str_plain_DICT_KEY_ORDER );

            if ( tmp_list_element_1 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DICT_KEY_ORDER" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 176;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_dictset_value = PyList_New( 2 );
            PyList_SET_ITEM( tmp_dictset_value, 0, tmp_list_element_1 );
            tmp_list_element_1 = PyObject_GetItem( locals_prompt_toolkit$styles$style_157, const_str_plain_MOST_PRECISE );

            if ( tmp_list_element_1 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {
                Py_DECREF( tmp_dictset_value );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MOST_PRECISE" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 176;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_dictset_value );

                exception_lineno = 176;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            PyList_SET_ITEM( tmp_dictset_value, 1, tmp_list_element_1 );
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_157, const_str_plain__ALL, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 == cache_frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 )
        {
            Py_DECREF( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 );
        }
        cache_frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 = NULL;

        assertFrameObject( frame_0a71b46f9cd9d9d57f0972b1b7f13fda_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_3 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_2 = const_str_plain_Priority;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_tuple_empty;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = locals_prompt_toolkit$styles$style_157;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_1__class_decl_dict;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 157;
            tmp_assign_source_31 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_3 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_31;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_30 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_30 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_prompt_toolkit$styles$style_157 );
        locals_prompt_toolkit$styles$style_157 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_prompt_toolkit$styles$style_157 );
        locals_prompt_toolkit$styles$style_157 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 157;
        goto try_except_handler_4;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Priority, tmp_assign_source_30 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_6;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_version_info );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_tuple_int_pos_3_int_pos_6_tuple;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_7;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Priority );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Priority );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Priority" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 184;

                goto frame_exception_exit_1;
            }

            tmp_source_name_6 = tmp_mvar_value_7;
            tmp_assign_source_32 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_DICT_KEY_ORDER );
            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_default_priority, tmp_assign_source_32 );
        }
        goto branch_end_4;
        branch_no_4:;
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_8;
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Priority );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Priority );
            }

            if ( tmp_mvar_value_8 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Priority" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 186;

                goto frame_exception_exit_1;
            }

            tmp_source_name_7 = tmp_mvar_value_8;
            tmp_assign_source_33 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_MOST_PRECISE );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 186;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_default_priority, tmp_assign_source_33 );
        }
        branch_end_4:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_BaseStyle );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BaseStyle );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BaseStyle" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;

            goto try_except_handler_7;
        }

        tmp_tuple_element_3 = tmp_mvar_value_9;
        tmp_assign_source_34 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_assign_source_34, 0, tmp_tuple_element_3 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_35 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_35;
    }
    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_36;
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_7;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        tmp_condition_result_7 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_1 = tmp_class_creation_2__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_2 );
        Py_DECREF( tmp_type_arg_2 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_3:;
        condexpr_end_2:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_37 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_37;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_7;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_8 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_8, const_str_plain___prepare__ );
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_9;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_9 = tmp_class_creation_2__metaclass;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___prepare__ );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;

                goto try_except_handler_7;
            }
            tmp_tuple_element_4 = const_str_plain_Style;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_4 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 189;
            tmp_assign_source_38 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_4 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;

                goto try_except_handler_7;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_38;
        }
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_10;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_10 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_10, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;

                goto try_except_handler_7;
            }
            tmp_condition_result_10 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_11;
                PyObject *tmp_type_arg_3;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_5 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_7;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_5 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_3 = tmp_class_creation_2__prepared;
                tmp_source_name_11 = BUILTIN_TYPE1( tmp_type_arg_3 );
                assert( !(tmp_source_name_11 == NULL) );
                tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_11 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 189;

                    goto try_except_handler_7;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_5 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;

                    goto try_except_handler_7;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 189;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_7;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_39;
            tmp_assign_source_39 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_39;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_40;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_prompt_toolkit$styles$style_189 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_7f01f8a02212dab8d7b62920b65eb392;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_digest_e8819e97da12c4a29728cc28ea343534;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_9;
        }
        tmp_dictset_value = const_str_plain_Style;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;

            goto try_except_handler_9;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_ced792830c6b3aa30a9736212caeb0ed_3, codeobj_ced792830c6b3aa30a9736212caeb0ed, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_ced792830c6b3aa30a9736212caeb0ed_3 = cache_frame_ced792830c6b3aa30a9736212caeb0ed_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_ced792830c6b3aa30a9736212caeb0ed_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_ced792830c6b3aa30a9736212caeb0ed_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_4___init__(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_element_name_2;
            tmp_res = MAPPING_HAS_ITEM( locals_prompt_toolkit$styles$style_189, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_11 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            tmp_called_name_5 = PyObject_GetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_property );

            if ( tmp_called_name_5 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 230;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_args_element_name_1 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_5_style_rules(  );



            frame_ced792830c6b3aa30a9736212caeb0ed_3->m_frame.f_lineno = 230;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_called_name_6 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_2 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_5_style_rules(  );



            frame_ced792830c6b3aa30a9736212caeb0ed_3->m_frame.f_lineno = 230;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_4:;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_style_rules, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_defaults_1;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_classmethod_arg_1;
            PyObject *tmp_defaults_2;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_mvar_value_11;
            tmp_res = MAPPING_HAS_ITEM( locals_prompt_toolkit$styles$style_189, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_12 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_5;
            }
            else
            {
                goto condexpr_false_5;
            }
            condexpr_true_5:;
            tmp_called_name_7 = PyObject_GetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_classmethod );

            if ( tmp_called_name_7 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 234;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_tuple_element_6 = PyObject_GetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_default_priority );

            if ( tmp_tuple_element_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_default_priority );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_default_priority );
                }

                if ( tmp_mvar_value_10 == NULL )
                {
                    Py_DECREF( tmp_called_name_7 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "default_priority" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 235;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_6 = tmp_mvar_value_10;
                Py_INCREF( tmp_tuple_element_6 );
                }
            }

            tmp_defaults_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_6 );
            tmp_args_element_name_3 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict( tmp_defaults_1 );



            frame_ced792830c6b3aa30a9736212caeb0ed_3->m_frame.f_lineno = 234;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_5;
            condexpr_false_5:;
            tmp_tuple_element_7 = PyObject_GetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_default_priority );

            if ( tmp_tuple_element_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_default_priority );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_default_priority );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "default_priority" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 235;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_7 = tmp_mvar_value_11;
                Py_INCREF( tmp_tuple_element_7 );
                }
            }

            tmp_defaults_2 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_7 );
            tmp_classmethod_arg_1 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_6_from_dict( tmp_defaults_2 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_5:;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_from_dict, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_3;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_mvar_value_12;
            tmp_tuple_element_8 = PyObject_GetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_DEFAULT_ATTRS );

            if ( tmp_tuple_element_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DEFAULT_ATTRS" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 251;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_8 = tmp_mvar_value_12;
                Py_INCREF( tmp_tuple_element_8 );
                }
            }

            tmp_defaults_3 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_defaults_3, 0, tmp_tuple_element_8 );
            tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_7_get_attrs_for_style_str( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_get_attrs_for_style_str, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 251;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_8_invalidation_hash(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain_invalidation_hash, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 298;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ced792830c6b3aa30a9736212caeb0ed_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ced792830c6b3aa30a9736212caeb0ed_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_ced792830c6b3aa30a9736212caeb0ed_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_ced792830c6b3aa30a9736212caeb0ed_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_ced792830c6b3aa30a9736212caeb0ed_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_ced792830c6b3aa30a9736212caeb0ed_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_ced792830c6b3aa30a9736212caeb0ed_3 == cache_frame_ced792830c6b3aa30a9736212caeb0ed_3 )
        {
            Py_DECREF( frame_ced792830c6b3aa30a9736212caeb0ed_3 );
        }
        cache_frame_ced792830c6b3aa30a9736212caeb0ed_3 = NULL;

        assertFrameObject( frame_ced792830c6b3aa30a9736212caeb0ed_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_9;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;

                goto try_except_handler_9;
            }
            tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_189, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;

                goto try_except_handler_9;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_8 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_9 = const_str_plain_Style;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_9 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_9 );
            tmp_tuple_element_9 = locals_prompt_toolkit$styles$style_189;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_2__class_decl_dict;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 189;
            tmp_assign_source_41 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_4, tmp_kw_name_5 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;

                goto try_except_handler_9;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_41;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_40 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_40 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        Py_DECREF( locals_prompt_toolkit$styles$style_189 );
        locals_prompt_toolkit$styles$style_189 = NULL;
        goto try_return_handler_8;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_prompt_toolkit$styles$style_189 );
        locals_prompt_toolkit$styles$style_189 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_8;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 189;
        goto try_except_handler_7;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_Style, tmp_assign_source_40 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_9__merge_attrs(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__merge_attrs, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        tmp_assign_source_43 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_10_merge_styles(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_merge_styles, tmp_assign_source_43 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_tuple_element_10;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_BaseStyle );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BaseStyle );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BaseStyle" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 333;

            goto try_except_handler_10;
        }

        tmp_tuple_element_10 = tmp_mvar_value_13;
        tmp_assign_source_44 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_10 );
        PyTuple_SET_ITEM( tmp_assign_source_44, 0, tmp_tuple_element_10 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_44;
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_45 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_45;
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_46;
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_15;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_4;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        tmp_condition_result_15 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_2 = tmp_class_creation_3__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_4 );
        Py_DECREF( tmp_type_arg_4 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_7:;
        condexpr_end_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_47 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_47;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_10;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_source_name_12;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_12 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_12, const_str_plain___prepare__ );
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_48;
            PyObject *tmp_called_name_9;
            PyObject *tmp_source_name_13;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_13 = tmp_class_creation_3__metaclass;
            tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain___prepare__ );
            if ( tmp_called_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 333;

                goto try_except_handler_10;
            }
            tmp_tuple_element_11 = const_str_plain__MergedStyle;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_11 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 333;
            tmp_assign_source_48 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_5, tmp_kw_name_6 );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 333;

                goto try_except_handler_10;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_48;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_14;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_14 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_14, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 333;

                goto try_except_handler_10;
            }
            tmp_condition_result_18 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_12;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_15;
                PyObject *tmp_type_arg_5;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_12 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 333;

                    goto try_except_handler_10;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_12 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_5 = tmp_class_creation_3__prepared;
                tmp_source_name_15 = BUILTIN_TYPE1( tmp_type_arg_5 );
                assert( !(tmp_source_name_15 == NULL) );
                tmp_tuple_element_12 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_15 );
                if ( tmp_tuple_element_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 333;

                    goto try_except_handler_10;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_12 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 333;

                    goto try_except_handler_10;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 333;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_10;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_49;
            tmp_assign_source_49 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_49;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_50;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_prompt_toolkit$styles$style_333 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_7f01f8a02212dab8d7b62920b65eb392;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_12;
        }
        tmp_dictset_value = const_str_digest_93bf0b002207c69e0706ac364311f835;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_12;
        }
        tmp_dictset_value = const_str_plain__MergedStyle;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;

            goto try_except_handler_12;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3307939780071f890ee385f4804005da_4, codeobj_3307939780071f890ee385f4804005da, module_prompt_toolkit$styles$style, sizeof(void *) );
        frame_3307939780071f890ee385f4804005da_4 = cache_frame_3307939780071f890ee385f4804005da_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3307939780071f890ee385f4804005da_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3307939780071f890ee385f4804005da_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_11___init__(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 348;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_called_name_11;
            PyObject *tmp_args_element_name_5;
            tmp_res = MAPPING_HAS_ITEM( locals_prompt_toolkit$styles$style_333, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_19 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_8;
            }
            else
            {
                goto condexpr_false_8;
            }
            condexpr_true_8:;
            tmp_called_name_10 = PyObject_GetItem( locals_prompt_toolkit$styles$style_333, const_str_plain_property );

            if ( tmp_called_name_10 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 354;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_4 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style(  );



            frame_3307939780071f890ee385f4804005da_4->m_frame.f_lineno = 354;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_8;
            condexpr_false_8:;
            tmp_called_name_11 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_5 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_12__merged_style(  );



            frame_3307939780071f890ee385f4804005da_4->m_frame.f_lineno = 354;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_8:;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain__merged_style, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_called_name_13;
            PyObject *tmp_args_element_name_7;
            tmp_res = MAPPING_HAS_ITEM( locals_prompt_toolkit$styles$style_333, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_20 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_9;
            }
            else
            {
                goto condexpr_false_9;
            }
            condexpr_true_9:;
            tmp_called_name_12 = PyObject_GetItem( locals_prompt_toolkit$styles$style_333, const_str_plain_property );

            if ( tmp_called_name_12 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_6 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_13_style_rules(  );



            frame_3307939780071f890ee385f4804005da_4->m_frame.f_lineno = 361;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_9;
            condexpr_false_9:;
            tmp_called_name_13 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_7 = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_13_style_rules(  );



            frame_3307939780071f890ee385f4804005da_4->m_frame.f_lineno = 361;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
            }

            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_9:;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain_style_rules, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_defaults_4;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_mvar_value_14;
            tmp_tuple_element_13 = PyObject_GetItem( locals_prompt_toolkit$styles$style_333, const_str_plain_DEFAULT_ATTRS );

            if ( tmp_tuple_element_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DEFAULT_ATTRS );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DEFAULT_ATTRS" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 368;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_tuple_element_13 = tmp_mvar_value_14;
                Py_INCREF( tmp_tuple_element_13 );
                }
            }

            tmp_defaults_4 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_defaults_4, 0, tmp_tuple_element_13 );
            tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_14_get_attrs_for_style_str( tmp_defaults_4 );



            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain_get_attrs_for_style_str, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 368;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$styles$style$$$function_15_invalidation_hash(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain_invalidation_hash, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3307939780071f890ee385f4804005da_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3307939780071f890ee385f4804005da_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3307939780071f890ee385f4804005da_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3307939780071f890ee385f4804005da_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3307939780071f890ee385f4804005da_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3307939780071f890ee385f4804005da_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_3307939780071f890ee385f4804005da_4 == cache_frame_3307939780071f890ee385f4804005da_4 )
        {
            Py_DECREF( frame_3307939780071f890ee385f4804005da_4 );
        }
        cache_frame_3307939780071f890ee385f4804005da_4 = NULL;

        assertFrameObject( frame_3307939780071f890ee385f4804005da_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_12;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_21;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 333;

                goto try_except_handler_12;
            }
            tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$styles$style_333, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 333;

                goto try_except_handler_12;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_51;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_14 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_14 = const_str_plain__MergedStyle;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_14 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_14 );
            tmp_tuple_element_14 = locals_prompt_toolkit$styles$style_333;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_3__class_decl_dict;
            frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame.f_lineno = 333;
            tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_6, tmp_kw_name_7 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 333;

                goto try_except_handler_12;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_51;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_50 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_50 );
        goto try_return_handler_12;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_12:;
        Py_DECREF( locals_prompt_toolkit$styles$style_333 );
        locals_prompt_toolkit$styles$style_333 = NULL;
        goto try_return_handler_11;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_prompt_toolkit$styles$style_333 );
        locals_prompt_toolkit$styles$style_333 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_11;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$styles$style );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 333;
        goto try_except_handler_10;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$styles$style, (Nuitka_StringObject *)const_str_plain__MergedStyle, tmp_assign_source_50 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a15d48e1ef886f9277cadf9c3fb2034e );
#endif
    popFrameStack();

    assertFrameObject( frame_a15d48e1ef886f9277cadf9c3fb2034e );

    goto frame_no_exception_4;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a15d48e1ef886f9277cadf9c3fb2034e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a15d48e1ef886f9277cadf9c3fb2034e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a15d48e1ef886f9277cadf9c3fb2034e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a15d48e1ef886f9277cadf9c3fb2034e, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;


    return MOD_RETURN_VALUE( module_prompt_toolkit$styles$style );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
