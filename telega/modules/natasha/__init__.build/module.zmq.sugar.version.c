/* Generated code for Python module 'zmq.sugar.version'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_zmq$sugar$version" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_zmq$sugar$version;
PyDictObject *moduledict_zmq$sugar$version;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_zmq_version;
static PyObject *const_str_plain___revision__;
extern PyObject *const_str_plain_zmq_version_info;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_str_plain_zmq_version_info_tuple;
extern PyObject *const_float_inf;
static PyObject *const_str_plain_VERSION_EXTRA;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_digest_0d6f34d7e2a2bc8047c63fd4afaef1b8;
extern PyObject *const_int_pos_1;
extern PyObject *const_slice_none_int_pos_6_none;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain___version__;
static PyObject *const_str_plain_VERSION_MAJOR;
extern PyObject *const_int_0;
static PyObject *const_str_digest_958d8c4d3b62a7dfbc577aa6bb0fc834;
extern PyObject *const_str_digest_5c04d7e337f9ddd8a54dde1b51b2cd69;
static PyObject *const_str_digest_5664a4dc72a52970bb917b6de5fcfeb3;
extern PyObject *const_int_pos_18;
static PyObject *const_str_plain_pyzmq_version_info;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_chr_64;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_plain_None;
extern PyObject *const_int_pos_6;
static PyObject *const_list_e6616e285127eeb98b381ac50f011ee9_list;
static PyObject *const_str_plain_pyzmq_version;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_0b955057f39e025845ea42ba1abdb0ef;
extern PyObject *const_str_digest_d41475455475b7b7deb9f8361d007624;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_VERSION_MINOR;
extern PyObject *const_str_digest_3114c7847ed30512509505e34fc4f6e0;
static PyObject *const_str_plain_VERSION_PATCH;
static PyObject *const_str_digest_4f89a176f698b220cbc7d073d5d7cc7b;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_096b97ed45a1156f8910dde29c89b973;
extern PyObject *const_str_empty;
static PyObject *const_str_digest_dfe742c9c0f357d3d8894a225d84d6e7;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain___revision__ = UNSTREAM_STRING_ASCII( &constant_bin[ 5845839 ], 12, 1 );
    const_str_plain_VERSION_EXTRA = UNSTREAM_STRING_ASCII( &constant_bin[ 5845851 ], 13, 1 );
    const_str_digest_0d6f34d7e2a2bc8047c63fd4afaef1b8 = UNSTREAM_STRING_ASCII( &constant_bin[ 5845864 ], 20, 0 );
    const_str_plain_VERSION_MAJOR = UNSTREAM_STRING_ASCII( &constant_bin[ 5845884 ], 13, 1 );
    const_str_digest_958d8c4d3b62a7dfbc577aa6bb0fc834 = UNSTREAM_STRING_ASCII( &constant_bin[ 5845897 ], 39, 0 );
    const_str_digest_5664a4dc72a52970bb917b6de5fcfeb3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5845936 ], 26, 0 );
    const_str_plain_pyzmq_version_info = UNSTREAM_STRING_ASCII( &constant_bin[ 5845962 ], 18, 1 );
    const_list_e6616e285127eeb98b381ac50f011ee9_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_e6616e285127eeb98b381ac50f011ee9_list, 0, const_str_plain_zmq_version ); Py_INCREF( const_str_plain_zmq_version );
    PyList_SET_ITEM( const_list_e6616e285127eeb98b381ac50f011ee9_list, 1, const_str_plain_zmq_version_info ); Py_INCREF( const_str_plain_zmq_version_info );
    const_str_plain_pyzmq_version = UNSTREAM_STRING_ASCII( &constant_bin[ 5845962 ], 13, 1 );
    PyList_SET_ITEM( const_list_e6616e285127eeb98b381ac50f011ee9_list, 2, const_str_plain_pyzmq_version ); Py_INCREF( const_str_plain_pyzmq_version );
    PyList_SET_ITEM( const_list_e6616e285127eeb98b381ac50f011ee9_list, 3, const_str_plain_pyzmq_version_info ); Py_INCREF( const_str_plain_pyzmq_version_info );
    PyList_SET_ITEM( const_list_e6616e285127eeb98b381ac50f011ee9_list, 4, const_str_plain___version__ ); Py_INCREF( const_str_plain___version__ );
    PyList_SET_ITEM( const_list_e6616e285127eeb98b381ac50f011ee9_list, 5, const_str_plain___revision__ ); Py_INCREF( const_str_plain___revision__ );
    const_str_digest_0b955057f39e025845ea42ba1abdb0ef = UNSTREAM_STRING_ASCII( &constant_bin[ 5845980 ], 40, 0 );
    const_str_plain_VERSION_MINOR = UNSTREAM_STRING_ASCII( &constant_bin[ 5846020 ], 13, 1 );
    const_str_plain_VERSION_PATCH = UNSTREAM_STRING_ASCII( &constant_bin[ 5846033 ], 13, 1 );
    const_str_digest_4f89a176f698b220cbc7d073d5d7cc7b = UNSTREAM_STRING_ASCII( &constant_bin[ 5846046 ], 8, 0 );
    const_str_digest_096b97ed45a1156f8910dde29c89b973 = UNSTREAM_STRING_ASCII( &constant_bin[ 5846054 ], 32, 0 );
    const_str_digest_dfe742c9c0f357d3d8894a225d84d6e7 = UNSTREAM_STRING_ASCII( &constant_bin[ 5846086 ], 158, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_zmq$sugar$version( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_f2d27fcf220a86e435a1a216d4ea3238;
static PyCodeObject *codeobj_59a596a00c2a2f5eee09a3b0f7e781c3;
static PyCodeObject *codeobj_9bae7bbd1b92ef72e898d4ed137eb8dc;
static PyCodeObject *codeobj_7696ca10c262c5b677bd58a4ce97ef9d;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_0d6f34d7e2a2bc8047c63fd4afaef1b8 );
    codeobj_f2d27fcf220a86e435a1a216d4ea3238 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_5664a4dc72a52970bb917b6de5fcfeb3, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_59a596a00c2a2f5eee09a3b0f7e781c3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_pyzmq_version, 24, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9bae7bbd1b92ef72e898d4ed137eb8dc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_pyzmq_version_info, 31, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7696ca10c262c5b677bd58a4ce97ef9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_zmq_version, 39, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_zmq$sugar$version$$$function_1_pyzmq_version(  );


static PyObject *MAKE_FUNCTION_zmq$sugar$version$$$function_2_pyzmq_version_info(  );


static PyObject *MAKE_FUNCTION_zmq$sugar$version$$$function_3_zmq_version(  );


// The module function definitions.
static PyObject *impl_zmq$sugar$version$$$function_1_pyzmq_version( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_59a596a00c2a2f5eee09a3b0f7e781c3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_59a596a00c2a2f5eee09a3b0f7e781c3 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_59a596a00c2a2f5eee09a3b0f7e781c3, codeobj_59a596a00c2a2f5eee09a3b0f7e781c3, module_zmq$sugar$version, 0 );
    frame_59a596a00c2a2f5eee09a3b0f7e781c3 = cache_frame_59a596a00c2a2f5eee09a3b0f7e781c3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_59a596a00c2a2f5eee09a3b0f7e781c3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_59a596a00c2a2f5eee09a3b0f7e781c3 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_mvar_value_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___revision__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___revision__ );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__revision__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 26;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_list_element_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_subscript_name_1;
            tmp_source_name_1 = const_str_chr_64;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
            assert( !(tmp_called_name_1 == NULL) );
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___version__ );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___version__ );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__version__" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 27;

                goto frame_exception_exit_1;
            }

            tmp_list_element_1 = tmp_mvar_value_2;
            tmp_args_element_name_1 = PyList_New( 2 );
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_1, 0, tmp_list_element_1 );
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___revision__ );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___revision__ );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__revision__" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 27;

                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_3;
            tmp_subscript_name_1 = const_slice_none_int_pos_6_none;
            tmp_list_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 27;

                goto frame_exception_exit_1;
            }
            PyList_SET_ITEM( tmp_args_element_name_1, 1, tmp_list_element_1 );
            frame_59a596a00c2a2f5eee09a3b0f7e781c3->m_frame.f_lineno = 27;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 27;

                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___version__ );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___version__ );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__version__" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 29;

                goto frame_exception_exit_1;
            }

            tmp_return_value = tmp_mvar_value_4;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59a596a00c2a2f5eee09a3b0f7e781c3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_59a596a00c2a2f5eee09a3b0f7e781c3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59a596a00c2a2f5eee09a3b0f7e781c3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_59a596a00c2a2f5eee09a3b0f7e781c3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_59a596a00c2a2f5eee09a3b0f7e781c3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_59a596a00c2a2f5eee09a3b0f7e781c3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_59a596a00c2a2f5eee09a3b0f7e781c3,
        type_description_1
    );


    // Release cached frame.
    if ( frame_59a596a00c2a2f5eee09a3b0f7e781c3 == cache_frame_59a596a00c2a2f5eee09a3b0f7e781c3 )
    {
        Py_DECREF( frame_59a596a00c2a2f5eee09a3b0f7e781c3 );
    }
    cache_frame_59a596a00c2a2f5eee09a3b0f7e781c3 = NULL;

    assertFrameObject( frame_59a596a00c2a2f5eee09a3b0f7e781c3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$sugar$version$$$function_1_pyzmq_version );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_zmq$sugar$version$$$function_2_pyzmq_version_info( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_9bae7bbd1b92ef72e898d4ed137eb8dc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9bae7bbd1b92ef72e898d4ed137eb8dc = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_9bae7bbd1b92ef72e898d4ed137eb8dc, codeobj_9bae7bbd1b92ef72e898d4ed137eb8dc, module_zmq$sugar$version, 0 );
    frame_9bae7bbd1b92ef72e898d4ed137eb8dc = cache_frame_9bae7bbd1b92ef72e898d4ed137eb8dc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9bae7bbd1b92ef72e898d4ed137eb8dc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9bae7bbd1b92ef72e898d4ed137eb8dc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_version_info );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_version_info );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "version_info" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;

            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9bae7bbd1b92ef72e898d4ed137eb8dc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9bae7bbd1b92ef72e898d4ed137eb8dc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9bae7bbd1b92ef72e898d4ed137eb8dc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9bae7bbd1b92ef72e898d4ed137eb8dc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9bae7bbd1b92ef72e898d4ed137eb8dc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9bae7bbd1b92ef72e898d4ed137eb8dc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9bae7bbd1b92ef72e898d4ed137eb8dc,
        type_description_1
    );


    // Release cached frame.
    if ( frame_9bae7bbd1b92ef72e898d4ed137eb8dc == cache_frame_9bae7bbd1b92ef72e898d4ed137eb8dc )
    {
        Py_DECREF( frame_9bae7bbd1b92ef72e898d4ed137eb8dc );
    }
    cache_frame_9bae7bbd1b92ef72e898d4ed137eb8dc = NULL;

    assertFrameObject( frame_9bae7bbd1b92ef72e898d4ed137eb8dc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$sugar$version$$$function_2_pyzmq_version_info );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_zmq$sugar$version$$$function_3_zmq_version( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_7696ca10c262c5b677bd58a4ce97ef9d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7696ca10c262c5b677bd58a4ce97ef9d = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_7696ca10c262c5b677bd58a4ce97ef9d, codeobj_7696ca10c262c5b677bd58a4ce97ef9d, module_zmq$sugar$version, 0 );
    frame_7696ca10c262c5b677bd58a4ce97ef9d = cache_frame_7696ca10c262c5b677bd58a4ce97ef9d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7696ca10c262c5b677bd58a4ce97ef9d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7696ca10c262c5b677bd58a4ce97ef9d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_left_name_1 = const_str_digest_4f89a176f698b220cbc7d073d5d7cc7b;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_zmq_version_info );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zmq_version_info );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zmq_version_info" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 41;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_7696ca10c262c5b677bd58a4ce97ef9d->m_frame.f_lineno = 41;
        tmp_right_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7696ca10c262c5b677bd58a4ce97ef9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7696ca10c262c5b677bd58a4ce97ef9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7696ca10c262c5b677bd58a4ce97ef9d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7696ca10c262c5b677bd58a4ce97ef9d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7696ca10c262c5b677bd58a4ce97ef9d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7696ca10c262c5b677bd58a4ce97ef9d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7696ca10c262c5b677bd58a4ce97ef9d,
        type_description_1
    );


    // Release cached frame.
    if ( frame_7696ca10c262c5b677bd58a4ce97ef9d == cache_frame_7696ca10c262c5b677bd58a4ce97ef9d )
    {
        Py_DECREF( frame_7696ca10c262c5b677bd58a4ce97ef9d );
    }
    cache_frame_7696ca10c262c5b677bd58a4ce97ef9d = NULL;

    assertFrameObject( frame_7696ca10c262c5b677bd58a4ce97ef9d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$sugar$version$$$function_3_zmq_version );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_zmq$sugar$version$$$function_1_pyzmq_version(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$sugar$version$$$function_1_pyzmq_version,
        const_str_plain_pyzmq_version,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_59a596a00c2a2f5eee09a3b0f7e781c3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$sugar$version,
        const_str_digest_958d8c4d3b62a7dfbc577aa6bb0fc834,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_zmq$sugar$version$$$function_2_pyzmq_version_info(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$sugar$version$$$function_2_pyzmq_version_info,
        const_str_plain_pyzmq_version_info,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9bae7bbd1b92ef72e898d4ed137eb8dc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$sugar$version,
        const_str_digest_dfe742c9c0f357d3d8894a225d84d6e7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_zmq$sugar$version$$$function_3_zmq_version(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$sugar$version$$$function_3_zmq_version,
        const_str_plain_zmq_version,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7696ca10c262c5b677bd58a4ce97ef9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$sugar$version,
        const_str_digest_0b955057f39e025845ea42ba1abdb0ef,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_zmq$sugar$version =
{
    PyModuleDef_HEAD_INIT,
    "zmq.sugar.version",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(zmq$sugar$version)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(zmq$sugar$version)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_zmq$sugar$version );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("zmq.sugar.version: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.sugar.version: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.sugar.version: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initzmq$sugar$version" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_zmq$sugar$version = Py_InitModule4(
        "zmq.sugar.version",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_zmq$sugar$version = PyModule_Create( &mdef_zmq$sugar$version );
#endif

    moduledict_zmq$sugar$version = MODULE_DICT( module_zmq$sugar$version );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_zmq$sugar$version,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_zmq$sugar$version,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$sugar$version,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$sugar$version,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_zmq$sugar$version );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_5c04d7e337f9ddd8a54dde1b51b2cd69, module_zmq$sugar$version );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_f2d27fcf220a86e435a1a216d4ea3238;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_096b97ed45a1156f8910dde29c89b973;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_f2d27fcf220a86e435a1a216d4ea3238 = MAKE_MODULE_FRAME( codeobj_f2d27fcf220a86e435a1a216d4ea3238, module_zmq$sugar$version );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_f2d27fcf220a86e435a1a216d4ea3238 );
    assert( Py_REFCNT( frame_f2d27fcf220a86e435a1a216d4ea3238 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_d41475455475b7b7deb9f8361d007624;
        tmp_globals_name_1 = (PyObject *)moduledict_zmq$sugar$version;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_zmq_version_info_tuple;
        tmp_level_name_1 = const_int_0;
        frame_f2d27fcf220a86e435a1a216d4ea3238->m_frame.f_lineno = 7;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_zmq_version_info );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_zmq_version_info, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = const_int_pos_18;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = const_int_0;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = const_int_pos_1;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_str_empty;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_EXTRA, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_mvar_value_5;
        tmp_left_name_1 = const_str_digest_4f89a176f698b220cbc7d073d5d7cc7b;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_right_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_tuple_element_1 = tmp_mvar_value_4;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_tuple_element_1 = tmp_mvar_value_5;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        tmp_assign_source_9 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_9 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_mvar_value_6;
        int tmp_truth_name_1;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_EXTRA );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_EXTRA );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_EXTRA" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 16;

            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_6 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_mvar_value_8;
            tmp_left_name_2 = const_str_digest_3114c7847ed30512509505e34fc4f6e0;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___version__ );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___version__ );
            }

            CHECK_OBJECT( tmp_mvar_value_7 );
            tmp_tuple_element_2 = tmp_mvar_value_7;
            tmp_right_name_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_2 );
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_EXTRA );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_EXTRA );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_right_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_EXTRA" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 17;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_2 = tmp_mvar_value_8;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_2 );
            tmp_assign_source_10 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto frame_exception_exit_1;
            }
            UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_10 );
        }
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_mvar_value_11;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_MAJOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 18;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_3 = tmp_mvar_value_9;
            tmp_assign_source_11 = PyTuple_New( 4 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_assign_source_11, 0, tmp_tuple_element_3 );
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR );
            }

            if ( tmp_mvar_value_10 == NULL )
            {
                Py_DECREF( tmp_assign_source_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_MINOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 18;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_3 = tmp_mvar_value_10;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_assign_source_11, 1, tmp_tuple_element_3 );
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH );
            }

            if ( tmp_mvar_value_11 == NULL )
            {
                Py_DECREF( tmp_assign_source_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_PATCH" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 18;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_3 = tmp_mvar_value_11;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_assign_source_11, 2, tmp_tuple_element_3 );
            tmp_tuple_element_3 = const_float_inf;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_assign_source_11, 3, tmp_tuple_element_3 );
            UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_version_info, tmp_assign_source_11 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_mvar_value_14;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_MAJOR );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_MAJOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_4 = tmp_mvar_value_12;
            tmp_assign_source_12 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_assign_source_12, 0, tmp_tuple_element_4 );
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_MINOR );
            }

            if ( tmp_mvar_value_13 == NULL )
            {
                Py_DECREF( tmp_assign_source_12 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_MINOR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_4 = tmp_mvar_value_13;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_assign_source_12, 1, tmp_tuple_element_4 );
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_VERSION_PATCH );
            }

            if ( tmp_mvar_value_14 == NULL )
            {
                Py_DECREF( tmp_assign_source_12 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "VERSION_PATCH" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto frame_exception_exit_1;
            }

            tmp_tuple_element_4 = tmp_mvar_value_14;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_assign_source_12, 2, tmp_tuple_element_4 );
            UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_version_info, tmp_assign_source_12 );
        }
        branch_end_1:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2d27fcf220a86e435a1a216d4ea3238 );
#endif
    popFrameStack();

    assertFrameObject( frame_f2d27fcf220a86e435a1a216d4ea3238 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2d27fcf220a86e435a1a216d4ea3238 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f2d27fcf220a86e435a1a216d4ea3238, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f2d27fcf220a86e435a1a216d4ea3238->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f2d27fcf220a86e435a1a216d4ea3238, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = const_str_empty;
        UPDATE_STRING_DICT0( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___revision__, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_zmq$sugar$version$$$function_1_pyzmq_version(  );



        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_pyzmq_version, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_zmq$sugar$version$$$function_2_pyzmq_version_info(  );



        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_pyzmq_version_info, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_zmq$sugar$version$$$function_3_zmq_version(  );



        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain_zmq_version, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = LIST_COPY( const_list_e6616e285127eeb98b381ac50f011ee9_list );
        UPDATE_STRING_DICT1( moduledict_zmq$sugar$version, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_17 );
    }

    return MOD_RETURN_VALUE( module_zmq$sugar$version );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
