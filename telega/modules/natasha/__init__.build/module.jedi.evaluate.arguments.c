/* Generated code for Python module 'jedi.evaluate.arguments'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jedi$evaluate$arguments" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jedi$evaluate$arguments;
PyDictObject *moduledict_jedi$evaluate$arguments;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_ac1f38bec9728bd5b20209f335579d2f;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_str_chr_42_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e_tuple;
extern PyObject *const_str_plain_zip_longest;
extern PyObject *const_str_digest_c32a307eb4cbe1d85147469cdfd3f906;
extern PyObject *const_str_plain_array;
static PyObject *const_tuple_str_plain_self_str_plain_execution_context_tuple;
extern PyObject *const_str_plain_analysis;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_dict;
static PyObject *const_str_digest_bd59610485e783b5a38c0028142fe0a4;
extern PyObject *const_tuple_str_plain_iterable_tuple;
extern PyObject *const_str_plain_comp;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_infer;
extern PyObject *const_str_plain_string;
extern PyObject *const_str_plain_iterable;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_digest_cfdbbd2b4f173bd816c499641f213719;
static PyObject *const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple;
static PyObject *const_str_plain_allow_kwargs;
static PyObject *const_str_digest_81d2e612c5ac85c7db6de6890bf5da00;
extern PyObject *const_str_plain_group;
extern PyObject *const_str_plain_tree;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_tree_node;
extern PyObject *const_str_plain_execution_context;
extern PyObject *const_str_plain_argument_node;
extern PyObject *const_str_plain_unpack;
static PyObject *const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple;
static PyObject *const_tuple_a6749cbf9ef3947631e248c025678c71_tuple;
extern PyObject *const_str_plain_default;
extern PyObject *const_str_plain_a;
static PyObject *const_tuple_2cf64c43445ab3180891c0373e341b50_tuple;
static PyObject *const_str_digest_9634b3def8abb74fdda61594adb8a89a;
static PyObject *const_str_plain_iter_;
static PyObject *const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple;
extern PyObject *const_str_plain_param;
static PyObject *const_str_digest_0e3318c63d5653247032da60bc3c9c1f;
extern PyObject *const_str_chr_42;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_digest_4b6ae0a4a7c5ef73221de5bb2b8ff0e4;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_1a4d1d0ac4461ae617038f2c10257b40;
static PyObject *const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple;
extern PyObject *const_str_plain_iterators;
extern PyObject *const_str_plain_try_iter_content;
extern PyObject *const_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e;
extern PyObject *const_str_plain_func;
static PyObject *const_str_digest_222afe807ed606367487725a32c52c7c;
static PyObject *const_str_digest_406a297b9f88bef7616554d036a1dfce;
extern PyObject *const_str_plain_names;
static PyObject *const_str_plain__star_star_dict;
extern PyObject *const_tuple_str_plain_v_tuple;
extern PyObject *const_str_plain_wrapper;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain_children;
extern PyObject *const_str_digest_5ec6de478bf8667b4b45176a02cc0af8;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_f088166ebe9aa8754d7a4327a52e54aa;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_5de7b9637e12a56e87449e32f6814e59;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_a550d01a3bba920d4d857eb16ce6bfb2;
extern PyObject *const_str_plain_AbstractArguments;
static PyObject *const_str_digest_5113a0c12054796e82cd2a031a93a92b;
static PyObject *const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_iterator_str_plain_child_tuple;
static PyObject *const_tuple_029cf55f0cc619c4eb7e6a635e7c109f_tuple;
extern PyObject *const_str_plain_add;
extern PyObject *const_slice_none_none_int_pos_2;
extern PyObject *const_str_plain_eval_all;
extern PyObject *const_str_chr_44;
static PyObject *const_tuple_str_plain_arglist_str_plain_testlist_tuple;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_5498161026ca17b47252cf3b548d9743;
static PyObject *const_str_plain_clinic_args;
static PyObject *const_str_digest_bb44427537b9f39e513bbbac7876eb36;
extern PyObject *const_str_plain_depth;
static PyObject *const_tuple_str_plain_DynamicExecutedParams_tuple;
extern PyObject *const_str_plain_arguments;
extern PyObject *const_str_plain_string_name;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_parameters;
extern PyObject *const_tuple_str_plain_arguments_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_values_list_tuple;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain__evaluator;
extern PyObject *const_str_plain_parent;
extern PyObject *const_tuple_str_plain_analysis_tuple;
extern PyObject *const_str_plain_ExecutedParam;
extern PyObject *const_str_plain_get_merged_lazy_context;
extern PyObject *const_str_plain_NO_CONTEXTS;
static PyObject *const_str_digest_47015d9d6e865ff086a6c5a7555d2998;
extern PyObject *const_str_plain_message;
extern PyObject *const_str_digest_74a64e2326d0e94475afe0623d07b03d;
extern PyObject *const_str_plain_var_args;
static PyObject *const_str_digest_1438f90ac5cf0b076569edae30e5d38f;
extern PyObject *const_str_plain_GeneratorComprehension;
extern PyObject *const_str_plain_context;
extern PyObject *const_tuple_str_plain_debug_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_get_calling_nodes;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_str_plain_search_params;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
static PyObject *const_str_digest_6c0ce7584fef273d51290df2a2fe598e;
extern PyObject *const_tuple_str_plain_zip_longest_tuple;
extern PyObject *const_str_plain_reversed;
extern PyObject *const_str_plain_name;
static PyObject *const_tuple_45cbe4d262d03a59643b4af02b612069_tuple;
extern PyObject *const_str_plain_exact_key_items;
extern PyObject *const_int_pos_10;
extern PyObject *const_str_plain_py__iter__;
static PyObject *const_str_digest_58e0c401994ef9a5ed2ad7ac21d5fd74;
static PyObject *const_str_digest_b8654fb5f883f28e1aa51998ecfd80be;
extern PyObject *const_str_plain_match;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_funcdef;
extern PyObject *const_str_plain_eval_node;
extern PyObject *const_str_plain_goto;
extern PyObject *const_str_plain_False;
static PyObject *const_str_digest_4523d9a1b64655374e1942fd87c2a659;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_digest_93e555a5b1643558b28cf6b2256158dc;
static PyObject *const_tuple_str_plain_get_executed_params_str_plain_ExecutedParam_tuple;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_digest_36e46c252a3e3bf20afff1d05096f2ab;
extern PyObject *const_str_plain_array_type;
extern PyObject *const_str_plain_get_param;
extern PyObject *const_str_plain_arrays;
static PyObject *const_str_digest_4b95f835c9f8b3337fa03581580fe419;
extern PyObject *const_tuple_str_plain_CompiledInstance_tuple;
extern PyObject *const_str_plain_pop;
extern PyObject *const_str_plain_keep_arguments_param;
static PyObject *const_str_plain_as_tree_tuple_objects;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_DynamicExecutedParams;
extern PyObject *const_str_plain_trailer;
static PyObject *const_str_digest_7672e0ecf9f07e55e6b5cbaaa0903ba1;
extern PyObject *const_str_plain_Sequence;
extern PyObject *const_tuple_str_plain_func_str_plain_wrapper_tuple;
extern PyObject *const_str_plain_repack_with_argument_clinic;
static PyObject *const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple;
extern PyObject *const_tuple_int_pos_2_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_funcdef_str_plain_values_tuple;
static PyObject *const_str_digest_26ad4ced4c4df3f616989585528cdeb7;
extern PyObject *const_str_plain_LazyKnownContexts;
static PyObject *const_str_plain_named_arg;
extern PyObject *const_str_plain_context_set;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_95594c52b163dd9f042a1f828995d3ba;
extern PyObject *const_str_plain_testlist;
extern PyObject *const_str_plain_get_executed_params;
extern PyObject *const_str_plain_ValuesArguments;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_be314bee9b21ffc1600b7cfc80d2d6c2;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_plain_warning;
extern PyObject *const_str_plain_lazy_context;
static PyObject *const_str_plain__iterate_star_args;
extern PyObject *const_str_digest_9762bd19b18a11af1c0649d1b042a42f;
static PyObject *const_str_plain_named_args;
extern PyObject *const_str_digest_4eff24856086ad17c9fda82689f59bbb;
extern PyObject *const_str_digest_4ea3c739c0994e1c59a1638cbf9bd81c;
extern PyObject *const_str_plain_TreeArguments;
extern PyObject *const_str_plain_input_node;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_iterator;
static PyObject *const_str_plain__parse_argument_clinic;
static PyObject *const_tuple_3c79b651bc50d92f9175e4f209072883_tuple;
extern PyObject *const_tuple_str_plain_tree_tuple;
extern PyObject *const_str_plain_word;
extern PyObject *const_str_digest_9eca213790eaaf0b90047bb5e7656ae9;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_dct;
extern PyObject *const_str_plain_LazyTreeContext;
extern PyObject *const_tuple_str_plain_search_params_tuple;
extern PyObject *const_str_plain_star_count;
extern PyObject *const_str_plain_v;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_arglist;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_digest_785ebe15053a4fa731c98e296de8edd7;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_el;
extern PyObject *const_str_plain_debug;
extern PyObject *const_str_plain_typ;
static PyObject *const_str_plain__split;
extern PyObject *const_str_plain_optional;
extern PyObject *const_str_plain_LazyKnownContext;
static PyObject *const_str_digest_94ee30445d8d1cbe81a0b02bab7e510a;
extern PyObject *const_str_plain_CompiledInstance;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_values;
static PyObject *const_str_digest_ccdc881ece2ecf71de99e64173a2e77f;
extern PyObject *const_str_plain_key;
extern PyObject *const_str_plain_jedi;
static PyObject *const_tuple_1f2596990bedd989c64147c10bcd6218_tuple;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_types;
static PyObject *const_tuple_str_digest_81d2e612c5ac85c7db6de6890bf5da00_tuple;
static PyObject *const_tuple_3096b42ed3daccd567609f48c1008916_tuple;
static PyObject *const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple;
extern PyObject *const_tuple_str_plain_NO_CONTEXTS_tuple;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_old_arguments_list;
extern PyObject *const_str_plain_argument;
extern PyObject *const_str_plain_child;
extern PyObject *const_str_plain_Name;
extern PyObject *const_str_plain___repr__;
static PyObject *const_str_digest_92556a1adf162047547119b2621563b3;
static PyObject *const_str_plain__iterate_argument_clinic;
extern PyObject *const_str_plain_AnonymousArguments;
extern PyObject *const_str_digest_3d8c90d9a9bb20202fbddcdf6f6b7d19;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_evaluator;
static PyObject *const_str_plain__values_list;
static PyObject *const_tuple_str_plain_ParamName_tuple;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_ba8aaf77fcf21b9f37e561daf1fdf76b;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_8f87afbcc6dec4c2f8f0d4adb1a1b431;
static PyObject *const_str_digest_35a3634dcbac6afef9b1545a33d3fba5;
extern PyObject *const_str_plain_ParamName;
static PyObject *const_tuple_str_plain_self_str_plain_funcdef_tuple;
extern PyObject *const_tuple_none_none_tuple;
static PyObject *const_str_plain_values_list;
extern PyObject *const_str_plain_decorator;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_ac1f38bec9728bd5b20209f335579d2f = UNSTREAM_STRING_ASCII( &constant_bin[ 964386 ], 59, 0 );
    const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple, 1, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    PyTuple_SET_ITEM( const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple, 2, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple, 3, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    PyTuple_SET_ITEM( const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple, 4, const_str_plain_types ); Py_INCREF( const_str_plain_types );
    const_tuple_str_plain_self_str_plain_execution_context_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_execution_context_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_execution_context_tuple, 1, const_str_plain_execution_context ); Py_INCREF( const_str_plain_execution_context );
    const_str_digest_bd59610485e783b5a38c0028142fe0a4 = UNSTREAM_STRING_ASCII( &constant_bin[ 964445 ], 52, 0 );
    const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple, 0, const_str_plain_LazyKnownContext ); Py_INCREF( const_str_plain_LazyKnownContext );
    PyTuple_SET_ITEM( const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple, 1, const_str_plain_LazyKnownContexts ); Py_INCREF( const_str_plain_LazyKnownContexts );
    PyTuple_SET_ITEM( const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple, 2, const_str_plain_LazyTreeContext ); Py_INCREF( const_str_plain_LazyTreeContext );
    PyTuple_SET_ITEM( const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple, 3, const_str_plain_get_merged_lazy_context ); Py_INCREF( const_str_plain_get_merged_lazy_context );
    const_str_plain_allow_kwargs = UNSTREAM_STRING_ASCII( &constant_bin[ 964497 ], 12, 1 );
    const_str_digest_81d2e612c5ac85c7db6de6890bf5da00 = UNSTREAM_STRING_ASCII( &constant_bin[ 964509 ], 65, 0 );
    const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple, 0, const_str_plain_types ); Py_INCREF( const_str_plain_types );
    PyTuple_SET_ITEM( const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple, 1, const_str_plain_depth ); Py_INCREF( const_str_plain_depth );
    PyTuple_SET_ITEM( const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple, 2, const_str_plain_typ ); Py_INCREF( const_str_plain_typ );
    PyTuple_SET_ITEM( const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple, 3, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple, 4, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    const_tuple_a6749cbf9ef3947631e248c025678c71_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 1, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    const_str_plain_named_args = UNSTREAM_STRING_ASCII( &constant_bin[ 964574 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 2, const_str_plain_named_args ); Py_INCREF( const_str_plain_named_args );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 3, const_str_plain_star_count ); Py_INCREF( const_str_plain_star_count );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 4, const_str_plain_el ); Py_INCREF( const_str_plain_el );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 5, const_str_plain_arrays ); Py_INCREF( const_str_plain_arrays );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 6, const_str_plain_iterators ); Py_INCREF( const_str_plain_iterators );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 7, const_str_plain_values ); Py_INCREF( const_str_plain_values );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 8, const_str_plain_dct ); Py_INCREF( const_str_plain_dct );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 9, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 10, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 11, const_str_plain_comp ); Py_INCREF( const_str_plain_comp );
    const_str_plain_named_arg = UNSTREAM_STRING_ASCII( &constant_bin[ 964574 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 12, const_str_plain_named_arg ); Py_INCREF( const_str_plain_named_arg );
    const_tuple_2cf64c43445ab3180891c0373e341b50_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 0, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 1, const_str_plain_parameters ); Py_INCREF( const_str_plain_parameters );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 2, const_str_plain_iterator ); Py_INCREF( const_str_plain_iterator );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 3, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 4, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 5, const_str_plain_optional ); Py_INCREF( const_str_plain_optional );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 6, const_str_plain_allow_kwargs ); Py_INCREF( const_str_plain_allow_kwargs );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 7, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 8, const_str_plain_argument ); Py_INCREF( const_str_plain_argument );
    PyTuple_SET_ITEM( const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 9, const_str_plain_context_set ); Py_INCREF( const_str_plain_context_set );
    const_str_digest_9634b3def8abb74fdda61594adb8a89a = UNSTREAM_STRING_ASCII( &constant_bin[ 964584 ], 26, 0 );
    const_str_plain_iter_ = UNSTREAM_STRING_ASCII( &constant_bin[ 34960 ], 5, 1 );
    const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 0, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 1, const_str_plain_array ); Py_INCREF( const_str_plain_array );
    PyTuple_SET_ITEM( const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 2, const_str_plain_input_node ); Py_INCREF( const_str_plain_input_node );
    PyTuple_SET_ITEM( const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 3, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    PyTuple_SET_ITEM( const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 4, const_str_plain_CompiledInstance ); Py_INCREF( const_str_plain_CompiledInstance );
    PyTuple_SET_ITEM( const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 5, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    const_str_digest_0e3318c63d5653247032da60bc3c9c1f = UNSTREAM_STRING_ASCII( &constant_bin[ 964610 ], 34, 0 );
    const_str_digest_1a4d1d0ac4461ae617038f2c10257b40 = UNSTREAM_STRING_ASCII( &constant_bin[ 964644 ], 35, 0 );
    const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple, 0, const_str_plain_string ); Py_INCREF( const_str_plain_string );
    PyTuple_SET_ITEM( const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple, 1, const_str_plain_keep_arguments_param ); Py_INCREF( const_str_plain_keep_arguments_param );
    const_str_plain_clinic_args = UNSTREAM_STRING_ASCII( &constant_bin[ 964679 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple, 2, const_str_plain_clinic_args ); Py_INCREF( const_str_plain_clinic_args );
    PyTuple_SET_ITEM( const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple, 3, const_str_plain_decorator ); Py_INCREF( const_str_plain_decorator );
    const_str_digest_222afe807ed606367487725a32c52c7c = UNSTREAM_STRING_ASCII( &constant_bin[ 964690 ], 99, 0 );
    const_str_digest_406a297b9f88bef7616554d036a1dfce = UNSTREAM_STRING_ASCII( &constant_bin[ 964789 ], 27, 0 );
    const_str_plain__star_star_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 964816 ], 15, 1 );
    const_str_digest_5de7b9637e12a56e87449e32f6814e59 = UNSTREAM_STRING_ASCII( &constant_bin[ 964831 ], 24, 0 );
    const_str_digest_a550d01a3bba920d4d857eb16ce6bfb2 = UNSTREAM_STRING_ASCII( &constant_bin[ 964855 ], 26, 0 );
    const_str_digest_5113a0c12054796e82cd2a031a93a92b = UNSTREAM_STRING_ASCII( &constant_bin[ 964881 ], 46, 0 );
    const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple, 1, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple, 2, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple, 3, const_str_plain_argument_node ); Py_INCREF( const_str_plain_argument_node );
    PyTuple_SET_ITEM( const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple, 4, const_str_plain_trailer ); Py_INCREF( const_str_plain_trailer );
    const_tuple_str_plain_self_str_plain_iterator_str_plain_child_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_iterator_str_plain_child_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_iterator_str_plain_child_tuple, 1, const_str_plain_iterator ); Py_INCREF( const_str_plain_iterator );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_iterator_str_plain_child_tuple, 2, const_str_plain_child ); Py_INCREF( const_str_plain_child );
    const_tuple_029cf55f0cc619c4eb7e6a635e7c109f_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_029cf55f0cc619c4eb7e6a635e7c109f_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_029cf55f0cc619c4eb7e6a635e7c109f_tuple, 1, const_str_plain_execution_context ); Py_INCREF( const_str_plain_execution_context );
    PyTuple_SET_ITEM( const_tuple_029cf55f0cc619c4eb7e6a635e7c109f_tuple, 2, const_str_plain_search_params ); Py_INCREF( const_str_plain_search_params );
    const_tuple_str_plain_arglist_str_plain_testlist_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_arglist_str_plain_testlist_tuple, 0, const_str_plain_arglist ); Py_INCREF( const_str_plain_arglist );
    PyTuple_SET_ITEM( const_tuple_str_plain_arglist_str_plain_testlist_tuple, 1, const_str_plain_testlist ); Py_INCREF( const_str_plain_testlist );
    const_str_digest_5498161026ca17b47252cf3b548d9743 = UNSTREAM_STRING_ASCII( &constant_bin[ 964927 ], 31, 0 );
    const_str_digest_bb44427537b9f39e513bbbac7876eb36 = UNSTREAM_STRING_ASCII( &constant_bin[ 964958 ], 22, 0 );
    const_tuple_str_plain_DynamicExecutedParams_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_DynamicExecutedParams_tuple, 0, const_str_plain_DynamicExecutedParams ); Py_INCREF( const_str_plain_DynamicExecutedParams );
    const_tuple_str_plain_self_str_plain_values_list_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_values_list_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_values_list = UNSTREAM_STRING_ASCII( &constant_bin[ 964980 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_values_list_tuple, 1, const_str_plain_values_list ); Py_INCREF( const_str_plain_values_list );
    const_str_digest_47015d9d6e865ff086a6c5a7555d2998 = UNSTREAM_STRING_ASCII( &constant_bin[ 964991 ], 24, 0 );
    const_str_digest_1438f90ac5cf0b076569edae30e5d38f = UNSTREAM_STRING_ASCII( &constant_bin[ 965015 ], 311, 0 );
    const_str_digest_6c0ce7584fef273d51290df2a2fe598e = UNSTREAM_STRING_ASCII( &constant_bin[ 965326 ], 22, 0 );
    const_tuple_45cbe4d262d03a59643b4af02b612069_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_45cbe4d262d03a59643b4af02b612069_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_45cbe4d262d03a59643b4af02b612069_tuple, 1, const_str_plain_star_count ); Py_INCREF( const_str_plain_star_count );
    PyTuple_SET_ITEM( const_tuple_45cbe4d262d03a59643b4af02b612069_tuple, 2, const_str_plain_argument ); Py_INCREF( const_str_plain_argument );
    PyTuple_SET_ITEM( const_tuple_45cbe4d262d03a59643b4af02b612069_tuple, 3, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_str_digest_58e0c401994ef9a5ed2ad7ac21d5fd74 = UNSTREAM_STRING_ASCII( &constant_bin[ 965348 ], 35, 0 );
    const_str_digest_b8654fb5f883f28e1aa51998ecfd80be = UNSTREAM_STRING_ASCII( &constant_bin[ 965383 ], 24, 0 );
    const_str_digest_4523d9a1b64655374e1942fd87c2a659 = UNSTREAM_STRING_ASCII( &constant_bin[ 965407 ], 34, 0 );
    const_tuple_str_plain_get_executed_params_str_plain_ExecutedParam_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_executed_params_str_plain_ExecutedParam_tuple, 0, const_str_plain_get_executed_params ); Py_INCREF( const_str_plain_get_executed_params );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_executed_params_str_plain_ExecutedParam_tuple, 1, const_str_plain_ExecutedParam ); Py_INCREF( const_str_plain_ExecutedParam );
    const_str_digest_4b95f835c9f8b3337fa03581580fe419 = UNSTREAM_STRING_ASCII( &constant_bin[ 965441 ], 36, 0 );
    const_str_plain_as_tree_tuple_objects = UNSTREAM_STRING_ASCII( &constant_bin[ 964658 ], 21, 1 );
    const_str_digest_7672e0ecf9f07e55e6b5cbaaa0903ba1 = UNSTREAM_STRING_ASCII( &constant_bin[ 965477 ], 57, 0 );
    const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 1, const_str_plain_DynamicExecutedParams ); Py_INCREF( const_str_plain_DynamicExecutedParams );
    const_str_plain_old_arguments_list = UNSTREAM_STRING_ASCII( &constant_bin[ 965534 ], 18, 1 );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 2, const_str_plain_old_arguments_list ); Py_INCREF( const_str_plain_old_arguments_list );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 3, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 4, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 5, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 6, const_str_plain_star_count ); Py_INCREF( const_str_plain_star_count );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 7, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 8, const_str_plain_param ); Py_INCREF( const_str_plain_param );
    const_tuple_str_plain_self_str_plain_funcdef_str_plain_values_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_funcdef_str_plain_values_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_funcdef_str_plain_values_tuple, 1, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_funcdef_str_plain_values_tuple, 2, const_str_plain_values ); Py_INCREF( const_str_plain_values );
    const_str_digest_26ad4ced4c4df3f616989585528cdeb7 = UNSTREAM_STRING_ASCII( &constant_bin[ 965552 ], 20, 0 );
    const_str_digest_95594c52b163dd9f042a1f828995d3ba = UNSTREAM_STRING_ASCII( &constant_bin[ 965572 ], 63, 0 );
    const_str_digest_be314bee9b21ffc1600b7cfc80d2d6c2 = UNSTREAM_STRING_ASCII( &constant_bin[ 965635 ], 22, 0 );
    const_str_plain__iterate_star_args = UNSTREAM_STRING_ASCII( &constant_bin[ 965657 ], 18, 1 );
    const_str_plain__parse_argument_clinic = UNSTREAM_STRING_ASCII( &constant_bin[ 965675 ], 22, 1 );
    const_tuple_3c79b651bc50d92f9175e4f209072883_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 2, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 3, const_str_plain_keep_arguments_param ); Py_INCREF( const_str_plain_keep_arguments_param );
    PyTuple_SET_ITEM( const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 4, const_str_plain_clinic_args ); Py_INCREF( const_str_plain_clinic_args );
    PyTuple_SET_ITEM( const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 5, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_str_plain__split = UNSTREAM_STRING_ASCII( &constant_bin[ 145611 ], 6, 1 );
    const_str_digest_94ee30445d8d1cbe81a0b02bab7e510a = UNSTREAM_STRING_ASCII( &constant_bin[ 965697 ], 334, 0 );
    const_str_digest_ccdc881ece2ecf71de99e64173a2e77f = UNSTREAM_STRING_ASCII( &constant_bin[ 966031 ], 37, 0 );
    const_tuple_1f2596990bedd989c64147c10bcd6218_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_1f2596990bedd989c64147c10bcd6218_tuple, 0, const_str_plain_string ); Py_INCREF( const_str_plain_string );
    PyTuple_SET_ITEM( const_tuple_1f2596990bedd989c64147c10bcd6218_tuple, 1, const_str_plain_allow_kwargs ); Py_INCREF( const_str_plain_allow_kwargs );
    PyTuple_SET_ITEM( const_tuple_1f2596990bedd989c64147c10bcd6218_tuple, 2, const_str_plain_optional ); Py_INCREF( const_str_plain_optional );
    PyTuple_SET_ITEM( const_tuple_1f2596990bedd989c64147c10bcd6218_tuple, 3, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    PyTuple_SET_ITEM( const_tuple_1f2596990bedd989c64147c10bcd6218_tuple, 4, const_str_plain_word ); Py_INCREF( const_str_plain_word );
    const_tuple_str_digest_81d2e612c5ac85c7db6de6890bf5da00_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_81d2e612c5ac85c7db6de6890bf5da00_tuple, 0, const_str_digest_81d2e612c5ac85c7db6de6890bf5da00 ); Py_INCREF( const_str_digest_81d2e612c5ac85c7db6de6890bf5da00 );
    const_tuple_3096b42ed3daccd567609f48c1008916_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 0, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 1, const_str_plain_array ); Py_INCREF( const_str_plain_array );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 2, const_str_plain_input_node ); Py_INCREF( const_str_plain_input_node );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 3, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 4, const_str_plain_iter_ ); Py_INCREF( const_str_plain_iter_ );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 5, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 6, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple, 0, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple, 1, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple, 2, const_str_plain_el ); Py_INCREF( const_str_plain_el );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple, 3, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    const_str_digest_92556a1adf162047547119b2621563b3 = UNSTREAM_STRING_ASCII( &constant_bin[ 966068 ], 20, 0 );
    const_str_plain__iterate_argument_clinic = UNSTREAM_STRING_ASCII( &constant_bin[ 966088 ], 24, 1 );
    const_str_plain__values_list = UNSTREAM_STRING_ASCII( &constant_bin[ 964979 ], 12, 1 );
    const_tuple_str_plain_ParamName_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ParamName_tuple, 0, const_str_plain_ParamName ); Py_INCREF( const_str_plain_ParamName );
    const_str_digest_ba8aaf77fcf21b9f37e561daf1fdf76b = UNSTREAM_STRING_ASCII( &constant_bin[ 966112 ], 38, 0 );
    const_str_digest_8f87afbcc6dec4c2f8f0d4adb1a1b431 = UNSTREAM_STRING_ASCII( &constant_bin[ 966150 ], 32, 0 );
    const_str_digest_35a3634dcbac6afef9b1545a33d3fba5 = UNSTREAM_STRING_ASCII( &constant_bin[ 966182 ], 59, 0 );
    const_tuple_str_plain_self_str_plain_funcdef_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_funcdef_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_funcdef_tuple, 1, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jedi$evaluate$arguments( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_954935d8cfae61494ac9145b532bef9c;
static PyCodeObject *codeobj_68cfc9138ef07e15b5263a4d3603a93d;
static PyCodeObject *codeobj_c03f1e1d5ff3897f482f9b6f063fc57d;
static PyCodeObject *codeobj_0b0766d805c3751504e8c1aa03e56f60;
static PyCodeObject *codeobj_06dcdb711788f30b999a61171c023218;
static PyCodeObject *codeobj_5f2059120328d2005b855a1561388c69;
static PyCodeObject *codeobj_c260aa4eb415ead534da046b6a064a61;
static PyCodeObject *codeobj_4aec7604550177c33f576602ae68e7aa;
static PyCodeObject *codeobj_f4b160ad19e6052a9e5bfb14154f52a1;
static PyCodeObject *codeobj_f74863e3a40c1c188a4e2a354b3186aa;
static PyCodeObject *codeobj_eb74dbad402bcef7e4fb4a91dce58de7;
static PyCodeObject *codeobj_eea3050785657066e34a3884f81785c9;
static PyCodeObject *codeobj_c79604a1e1235335690b09bbc78cc449;
static PyCodeObject *codeobj_84fa22eb45b2fbb8c121d4f60fd9de0f;
static PyCodeObject *codeobj_f4e7584e431f136d5523962d1dd15696;
static PyCodeObject *codeobj_d67683786dac3d83577a480799f42096;
static PyCodeObject *codeobj_5c5e6b5570fd6739cfeb97dcdf11c626;
static PyCodeObject *codeobj_d292d3f28731963bec78e997a2fdb614;
static PyCodeObject *codeobj_550ddc84fe7b1acc1934dc45a10b6905;
static PyCodeObject *codeobj_511e84622be8967aa3d3dad49070cbde;
static PyCodeObject *codeobj_4b62345805eae7a2eea7f9bf5aa7f87f;
static PyCodeObject *codeobj_22192fb76260e227da513a98be74627a;
static PyCodeObject *codeobj_1cb7a2b5f3c011c02a21a7ee5a36ae4c;
static PyCodeObject *codeobj_54d7da2c1dfc5702d05918ca8ddc2bf2;
static PyCodeObject *codeobj_0a4cabe4811bd13cbfd06415b68c1fe6;
static PyCodeObject *codeobj_8f7244f6e41fb0b67e0561df2444b8b0;
static PyCodeObject *codeobj_2ddac29db203e3015a4f8876a14332ec;
static PyCodeObject *codeobj_c84bee060b6acd8351ea26c2a9cbc92d;
static PyCodeObject *codeobj_0f00421a7cf8fa6cf871ee36b36b8ba7;
static PyCodeObject *codeobj_97283cf1a5f77f9719abc1145357f94b;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_9634b3def8abb74fdda61594adb8a89a );
    codeobj_954935d8cfae61494ac9145b532bef9c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 187, const_tuple_str_plain_a_str_plain_self_str_plain_el_str_plain_funcdef_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_68cfc9138ef07e15b5263a4d3603a93d = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 193, const_tuple_str_plain_v_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c03f1e1d5ff3897f482f9b6f063fc57d = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8f87afbcc6dec4c2f8f0d4adb1a1b431, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_0b0766d805c3751504e8c1aa03e56f60 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AbstractArguments, 105, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_06dcdb711788f30b999a61171c023218 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AnonymousArguments, 129, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_5f2059120328d2005b855a1561388c69 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_TreeArguments, 142, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_c260aa4eb415ead534da046b6a064a61 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ValuesArguments, 266, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_4aec7604550177c33f576602ae68e7aa = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 143, const_tuple_b06f7fa098d387f02a7238d187bab94b_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f4b160ad19e6052a9e5bfb14154f52a1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 267, const_tuple_str_plain_self_str_plain_values_list_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f74863e3a40c1c188a4e2a354b3186aa = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 138, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eb74dbad402bcef7e4fb4a91dce58de7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 226, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eea3050785657066e34a3884f81785c9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 274, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c79604a1e1235335690b09bbc78cc449 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__iterate_argument_clinic, 63, const_tuple_2cf64c43445ab3180891c0373e341b50_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_84fa22eb45b2fbb8c121d4f60fd9de0f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__iterate_star_args, 278, const_tuple_3096b42ed3daccd567609f48c1008916_tuple, 4, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f4e7584e431f136d5523962d1dd15696 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__parse_argument_clinic, 87, const_tuple_1f2596990bedd989c64147c10bcd6218_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d67683786dac3d83577a480799f42096 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__split, 156, const_tuple_str_plain_self_str_plain_iterator_str_plain_child_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5c5e6b5570fd6739cfeb97dcdf11c626 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__star_star_dict, 292, const_tuple_18422d586efc9def1ec8cdf645c78e73_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d292d3f28731963bec78e997a2fdb614 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_as_tree_tuple_objects, 218, const_tuple_45cbe4d262d03a59643b4af02b612069_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_550ddc84fe7b1acc1934dc45a10b6905 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decorator, 46, const_tuple_str_plain_func_str_plain_wrapper_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_511e84622be8967aa3d3dad49070cbde = MAKE_CODEOBJ( module_filename_obj, const_str_plain_eval_all, 110, const_tuple_e64532da78275a486d4b8dca2afc9a3d_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4b62345805eae7a2eea7f9bf5aa7f87f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_calling_nodes, 119, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_22192fb76260e227da513a98be74627a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_calling_nodes, 229, const_tuple_7771ac046f2d5676f477d69fdb78cdc4_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1cb7a2b5f3c011c02a21a7ee5a36ae4c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_executed_params, 125, const_tuple_str_plain_self_str_plain_execution_context_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_54d7da2c1dfc5702d05918ca8ddc2bf2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_executed_params, 130, const_tuple_029cf55f0cc619c4eb7e6a635e7c109f_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0a4cabe4811bd13cbfd06415b68c1fe6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_repack_with_argument_clinic, 33, const_tuple_3c29d481497a9bae50682ed5faf1bdbe_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8f7244f6e41fb0b67e0561df2444b8b0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_try_iter_content, 16, const_tuple_545eaf7f107b923ff2265fe80d9c589a_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ddac29db203e3015a4f8876a14332ec = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack, 122, const_tuple_str_plain_self_str_plain_funcdef_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c84bee060b6acd8351ea26c2a9cbc92d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack, 182, const_tuple_a6749cbf9ef3947631e248c025678c71_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0f00421a7cf8fa6cf871ee36b36b8ba7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack, 270, const_tuple_str_plain_self_str_plain_funcdef_str_plain_values_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_97283cf1a5f77f9719abc1145357f94b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 47, const_tuple_3c79b651bc50d92f9175e4f209072883_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
}

// The module function declarations.
static PyObject *jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_maker( void );


static PyObject *jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_maker( void );


static PyObject *jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_maker( void );


static PyObject *jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_maker( void );


static PyObject *jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_maker( void );


static PyObject *jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_maker( void );


static PyObject *jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_10___repr__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_11___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_12__split(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_13_unpack( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_15___repr__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_16_get_calling_nodes(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_17___init__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_18_unpack( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_19___repr__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_1_try_iter_content( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_20__iterate_star_args( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_21__star_star_dict(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_3__iterate_argument_clinic(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_4__parse_argument_clinic(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_5_eval_all( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_6_get_calling_nodes(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_7_unpack( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_8_get_executed_params(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_9_get_executed_params(  );


// The module function definitions.
static PyObject *impl_jedi$evaluate$arguments$$$function_1_try_iter_content( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_types = python_pars[ 0 ];
    PyObject *par_depth = python_pars[ 1 ];
    PyObject *var_typ = NULL;
    PyObject *var_f = NULL;
    PyObject *var_lazy_context = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_try_except_1__unhandled_indicator = NULL;
    struct Nuitka_FrameObject *frame_8f7244f6e41fb0b67e0561df2444b8b0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_8f7244f6e41fb0b67e0561df2444b8b0 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8f7244f6e41fb0b67e0561df2444b8b0, codeobj_8f7244f6e41fb0b67e0561df2444b8b0, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8f7244f6e41fb0b67e0561df2444b8b0 = cache_frame_8f7244f6e41fb0b67e0561df2444b8b0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8f7244f6e41fb0b67e0561df2444b8b0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8f7244f6e41fb0b67e0561df2444b8b0 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_depth );
        tmp_compexpr_left_1 = par_depth;
        tmp_compexpr_right_1 = const_int_pos_10;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_types );
        tmp_iter_arg_1 = par_types;
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 23;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_typ;
            var_typ = tmp_assign_source_3;
            Py_INCREF( var_typ );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = Py_True;
        {
            PyObject *old = tmp_try_except_1__unhandled_indicator;
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_4;
            Py_INCREF( tmp_try_except_1__unhandled_indicator );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_typ );
        tmp_source_name_1 = var_typ;
        tmp_assign_source_5 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_py__iter__ );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = var_f;
            var_f = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = Py_False;
        {
            PyObject *old = tmp_try_except_1__unhandled_indicator;
            assert( old != NULL );
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_6;
            Py_INCREF( tmp_try_except_1__unhandled_indicator );
            Py_DECREF( old );
        }

    }
    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_8f7244f6e41fb0b67e0561df2444b8b0, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_8f7244f6e41fb0b67e0561df2444b8b0, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "ooooo";
            goto try_except_handler_5;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "ooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 24;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_8f7244f6e41fb0b67e0561df2444b8b0->m_frame) frame_8f7244f6e41fb0b67e0561df2444b8b0->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooo";
        goto try_except_handler_5;
        branch_no_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_1_try_iter_content );
    return NULL;
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        int tmp_truth_name_1;
        CHECK_OBJECT( tmp_try_except_1__unhandled_indicator );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_try_except_1__unhandled_indicator );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_left_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_iter_arg_2;
            PyObject *tmp_called_name_1;
            if ( var_f == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "f" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 29;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_1 = var_f;
            frame_8f7244f6e41fb0b67e0561df2444b8b0->m_frame.f_lineno = 29;
            tmp_iter_arg_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_iter_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_2 );
            Py_DECREF( tmp_iter_arg_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = tmp_for_loop_2__for_iterator;
                tmp_for_loop_2__for_iterator = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_for_loop_2__for_iterator );
            tmp_next_source_2 = tmp_for_loop_2__for_iterator;
            tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooo";
                    exception_lineno = 29;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_for_loop_2__iter_value;
                tmp_for_loop_2__iter_value = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_for_loop_2__iter_value );
            tmp_assign_source_9 = tmp_for_loop_2__iter_value;
            {
                PyObject *old = var_lazy_context;
                var_lazy_context = tmp_assign_source_9;
                Py_INCREF( var_lazy_context );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_try_iter_content );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_try_iter_content );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "try_iter_content" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 30;
                type_description_1 = "ooooo";
                goto try_except_handler_6;
            }

            tmp_called_name_2 = tmp_mvar_value_1;
            CHECK_OBJECT( var_lazy_context );
            tmp_called_instance_1 = var_lazy_context;
            frame_8f7244f6e41fb0b67e0561df2444b8b0->m_frame.f_lineno = 30;
            tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_infer );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "ooooo";
                goto try_except_handler_6;
            }
            CHECK_OBJECT( par_depth );
            tmp_left_name_1 = par_depth;
            tmp_right_name_1 = const_int_pos_1;
            tmp_args_element_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 30;
                type_description_1 = "ooooo";
                goto try_except_handler_6;
            }
            frame_8f7244f6e41fb0b67e0561df2444b8b0->m_frame.f_lineno = 30;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "ooooo";
                goto try_except_handler_6;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooooo";
            goto try_except_handler_6;
        }
        goto loop_start_2;
        loop_end_2:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_2__iter_value );
        tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
        Py_DECREF( tmp_for_loop_2__for_iterator );
        tmp_for_loop_2__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_3;
        // End of try:
        try_end_3:;
        Py_XDECREF( tmp_for_loop_2__iter_value );
        tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
        Py_DECREF( tmp_for_loop_2__for_iterator );
        tmp_for_loop_2__for_iterator = NULL;

        branch_no_3:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
    Py_DECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f7244f6e41fb0b67e0561df2444b8b0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f7244f6e41fb0b67e0561df2444b8b0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f7244f6e41fb0b67e0561df2444b8b0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8f7244f6e41fb0b67e0561df2444b8b0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8f7244f6e41fb0b67e0561df2444b8b0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8f7244f6e41fb0b67e0561df2444b8b0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8f7244f6e41fb0b67e0561df2444b8b0,
        type_description_1,
        par_types,
        par_depth,
        var_typ,
        var_f,
        var_lazy_context
    );


    // Release cached frame.
    if ( frame_8f7244f6e41fb0b67e0561df2444b8b0 == cache_frame_8f7244f6e41fb0b67e0561df2444b8b0 )
    {
        Py_DECREF( frame_8f7244f6e41fb0b67e0561df2444b8b0 );
    }
    cache_frame_8f7244f6e41fb0b67e0561df2444b8b0 = NULL;

    assertFrameObject( frame_8f7244f6e41fb0b67e0561df2444b8b0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_1_try_iter_content );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_types );
    Py_DECREF( par_types );
    par_types = NULL;

    CHECK_OBJECT( (PyObject *)par_depth );
    Py_DECREF( par_depth );
    par_depth = NULL;

    Py_XDECREF( var_typ );
    var_typ = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_types );
    Py_DECREF( par_types );
    par_types = NULL;

    CHECK_OBJECT( (PyObject *)par_depth );
    Py_DECREF( par_depth );
    par_depth = NULL;

    Py_XDECREF( var_typ );
    var_typ = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_1_try_iter_content );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    struct Nuitka_CellObject *par_keep_arguments_param = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *var_clinic_args = PyCell_EMPTY();
    PyObject *var_decorator = NULL;
    struct Nuitka_FrameObject *frame_0a4cabe4811bd13cbfd06415b68c1fe6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0a4cabe4811bd13cbfd06415b68c1fe6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0a4cabe4811bd13cbfd06415b68c1fe6, codeobj_0a4cabe4811bd13cbfd06415b68c1fe6, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0a4cabe4811bd13cbfd06415b68c1fe6 = cache_frame_0a4cabe4811bd13cbfd06415b68c1fe6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0a4cabe4811bd13cbfd06415b68c1fe6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0a4cabe4811bd13cbfd06415b68c1fe6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__parse_argument_clinic );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__parse_argument_clinic );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_parse_argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_string );
        tmp_args_element_name_1 = par_string;
        frame_0a4cabe4811bd13cbfd06415b68c1fe6->m_frame.f_lineno = 44;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_clinic_args ) == NULL );
        PyCell_SET( var_clinic_args, tmp_assign_source_1 );

    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a4cabe4811bd13cbfd06415b68c1fe6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a4cabe4811bd13cbfd06415b68c1fe6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0a4cabe4811bd13cbfd06415b68c1fe6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0a4cabe4811bd13cbfd06415b68c1fe6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0a4cabe4811bd13cbfd06415b68c1fe6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0a4cabe4811bd13cbfd06415b68c1fe6,
        type_description_1,
        par_string,
        par_keep_arguments_param,
        var_clinic_args,
        var_decorator
    );


    // Release cached frame.
    if ( frame_0a4cabe4811bd13cbfd06415b68c1fe6 == cache_frame_0a4cabe4811bd13cbfd06415b68c1fe6 )
    {
        Py_DECREF( frame_0a4cabe4811bd13cbfd06415b68c1fe6 );
    }
    cache_frame_0a4cabe4811bd13cbfd06415b68c1fe6 = NULL;

    assertFrameObject( frame_0a4cabe4811bd13cbfd06415b68c1fe6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var_clinic_args;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = par_keep_arguments_param;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );


        assert( var_decorator == NULL );
        var_decorator = tmp_assign_source_2;
    }
    CHECK_OBJECT( var_decorator );
    tmp_return_value = var_decorator;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    CHECK_OBJECT( (PyObject *)par_keep_arguments_param );
    Py_DECREF( par_keep_arguments_param );
    par_keep_arguments_param = NULL;

    CHECK_OBJECT( (PyObject *)var_clinic_args );
    Py_DECREF( var_clinic_args );
    var_clinic_args = NULL;

    CHECK_OBJECT( (PyObject *)var_decorator );
    Py_DECREF( var_decorator );
    var_decorator = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    CHECK_OBJECT( (PyObject *)par_keep_arguments_param );
    Py_DECREF( par_keep_arguments_param );
    par_keep_arguments_param = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_func = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_wrapper = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = self->m_closure[0];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] = par_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[2] = self->m_closure[1];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[2] );


        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapper );
    Py_DECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_arguments = NULL;
    struct Nuitka_FrameObject *frame_97283cf1a5f77f9719abc1145357f94b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_97283cf1a5f77f9719abc1145357f94b = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_97283cf1a5f77f9719abc1145357f94b, codeobj_97283cf1a5f77f9719abc1145357f94b, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_97283cf1a5f77f9719abc1145357f94b = cache_frame_97283cf1a5f77f9719abc1145357f94b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_97283cf1a5f77f9719abc1145357f94b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_97283cf1a5f77f9719abc1145357f94b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "keep_arguments_param" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[2] ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_kwargs );
            tmp_subscribed_name_1 = par_kwargs;
            tmp_subscript_name_1 = const_str_plain_arguments;
            tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            assert( var_arguments == NULL );
            var_arguments = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_kwargs );
            tmp_called_instance_1 = par_kwargs;
            frame_97283cf1a5f77f9719abc1145357f94b->m_frame.f_lineno = 51;
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_arguments_tuple, 0 ) );

            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            assert( var_arguments == NULL );
            var_arguments = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_args );
        tmp_left_name_1 = par_args;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__iterate_argument_clinic );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__iterate_argument_clinic );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_iterate_argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "oooccc";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_arguments );
        tmp_args_element_name_1 = var_arguments;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "clinic_args" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "oooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_2 = PyCell_GET( self->m_closure[0] );
        frame_97283cf1a5f77f9719abc1145357f94b->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_tuple_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooccc";
            goto try_except_handler_2;
        }
        tmp_right_name_1 = PySequence_Tuple( tmp_tuple_arg_1 );
        Py_DECREF( tmp_tuple_arg_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooccc";
            goto try_except_handler_2;
        }
        tmp_result = BINARY_OPERATION_ADD_OBJECT_TUPLE_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooccc";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = tmp_left_name_1;
        par_args = tmp_assign_source_3;

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_97283cf1a5f77f9719abc1145357f94b, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_97283cf1a5f77f9719abc1145357f94b, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ValueError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "oooccc";
            goto try_except_handler_3;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 55;
                type_description_1 = "oooccc";
                goto try_except_handler_3;
            }

            tmp_return_value = tmp_mvar_value_2;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_3;
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 52;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_97283cf1a5f77f9719abc1145357f94b->m_frame) frame_97283cf1a5f77f9719abc1145357f94b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooccc";
        goto try_except_handler_3;
        branch_end_2:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_97283cf1a5f77f9719abc1145357f94b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_97283cf1a5f77f9719abc1145357f94b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_97283cf1a5f77f9719abc1145357f94b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_97283cf1a5f77f9719abc1145357f94b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_97283cf1a5f77f9719abc1145357f94b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_97283cf1a5f77f9719abc1145357f94b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_97283cf1a5f77f9719abc1145357f94b,
        type_description_1,
        par_args,
        par_kwargs,
        var_arguments,
        self->m_closure[2],
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_97283cf1a5f77f9719abc1145357f94b == cache_frame_97283cf1a5f77f9719abc1145357f94b )
    {
        Py_DECREF( frame_97283cf1a5f77f9719abc1145357f94b );
    }
    cache_frame_97283cf1a5f77f9719abc1145357f94b = NULL;

    assertFrameObject( frame_97283cf1a5f77f9719abc1145357f94b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_arguments );
    Py_DECREF( var_arguments );
    var_arguments = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_arguments );
    var_arguments = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_3__iterate_argument_clinic( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_arguments = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_parameters = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_arguments;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_parameters;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_3__iterate_argument_clinic );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_parameters );
    Py_DECREF( par_parameters );
    par_parameters = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_parameters );
    Py_DECREF( par_parameters );
    par_parameters = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_3__iterate_argument_clinic );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_locals {
    PyObject *var_iterator;
    PyObject *var_i;
    PyObject *var_name;
    PyObject *var_optional;
    PyObject *var_allow_kwargs;
    PyObject *var_key;
    PyObject *var_argument;
    PyObject *var_context_set;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    PyObject *tmp_tuple_unpack_2__element_1;
    PyObject *tmp_tuple_unpack_2__element_2;
    PyObject *tmp_tuple_unpack_2__element_3;
    PyObject *tmp_tuple_unpack_2__source_iter;
    PyObject *tmp_tuple_unpack_3__element_1;
    PyObject *tmp_tuple_unpack_3__element_2;
    PyObject *tmp_tuple_unpack_3__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    int exception_keeper_lineno_6;
    int tmp_res;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    int exception_keeper_lineno_8;
};

static PyObject *jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_iterator = NULL;
    generator_heap->var_i = NULL;
    generator_heap->var_name = NULL;
    generator_heap->var_optional = NULL;
    generator_heap->var_allow_kwargs = NULL;
    generator_heap->var_key = NULL;
    generator_heap->var_argument = NULL;
    generator_heap->var_context_set = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->tmp_tuple_unpack_2__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_2__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_2__element_3 = NULL;
    generator_heap->tmp_tuple_unpack_2__source_iter = NULL;
    generator_heap->tmp_tuple_unpack_3__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_3__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_3__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_c79604a1e1235335690b09bbc78cc449, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "arguments" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 65;
            generator_heap->type_description_1 = "ccoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 65;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_unpack );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 65;
            generator_heap->type_description_1 = "ccoooooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_iterator == NULL );
        generator_heap->var_iterator = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_called_name_1 = (PyObject *)&PyEnum_Type;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "parameters" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 66;
            generator_heap->type_description_1 = "ccoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( generator->m_closure[1] );
        generator->m_frame->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 66;
            generator_heap->type_description_1 = "ccoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 66;
            generator_heap->type_description_1 = "ccoooooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccoooooooo";
                generator_heap->exception_lineno = 66;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = generator_heap->tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 66;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "ccoooooooo";
                    generator_heap->exception_lineno = 66;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_7;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_iter_arg_3;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_iter_arg_3 = generator_heap->tmp_tuple_unpack_1__element_2;
        tmp_assign_source_8 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 66;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_5;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_2__source_iter;
            generator_heap->tmp_tuple_unpack_2__source_iter = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = generator_heap->tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_3, 0, 3 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_6;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_2__element_1;
            generator_heap->tmp_tuple_unpack_2__element_1 = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = generator_heap->tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_10 = UNPACK_NEXT( tmp_unpack_4, 1, 3 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_6;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_2__element_2;
            generator_heap->tmp_tuple_unpack_2__element_2 = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_unpack_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
        tmp_unpack_5 = generator_heap->tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_5, 2, 3 );
        if ( tmp_assign_source_11 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_6;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_2__element_3;
            generator_heap->tmp_tuple_unpack_2__element_3 = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = generator_heap->tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "ccoooooooo";
                    generator_heap->exception_lineno = 66;
                    goto try_except_handler_6;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 66;
            goto try_except_handler_6;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_6:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_2__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_2__source_iter );
    generator_heap->tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_5;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_1 );
    generator_heap->tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_2 );
    generator_heap->tmp_tuple_unpack_2__element_2 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_3 );
    generator_heap->tmp_tuple_unpack_2__element_3 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_2__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_2__source_iter );
    generator_heap->tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_12 = generator_heap->tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = generator_heap->var_name;
            generator_heap->var_name = tmp_assign_source_12;
            Py_INCREF( generator_heap->var_name );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_1 );
    generator_heap->tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_13 = generator_heap->tmp_tuple_unpack_2__element_2;
        {
            PyObject *old = generator_heap->var_optional;
            generator_heap->var_optional = tmp_assign_source_13;
            Py_INCREF( generator_heap->var_optional );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_2 );
    generator_heap->tmp_tuple_unpack_2__element_2 = NULL;

    {
        PyObject *tmp_assign_source_14;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_3 );
        tmp_assign_source_14 = generator_heap->tmp_tuple_unpack_2__element_3;
        {
            PyObject *old = generator_heap->var_allow_kwargs;
            generator_heap->var_allow_kwargs = tmp_assign_source_14;
            Py_INCREF( generator_heap->var_allow_kwargs );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_3 );
    generator_heap->tmp_tuple_unpack_2__element_3 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_iter_arg_4;
        PyObject *tmp_next_arg_1;
        PyObject *tmp_next_default_1;
        CHECK_OBJECT( generator_heap->var_iterator );
        tmp_next_arg_1 = generator_heap->var_iterator;
        tmp_next_default_1 = const_tuple_none_none_tuple;
        tmp_iter_arg_4 = BUILTIN_NEXT2( tmp_next_arg_1, tmp_next_default_1 );
        if ( tmp_iter_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 67;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_7;
        }
        tmp_assign_source_15 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
        Py_DECREF( tmp_iter_arg_4 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 67;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_7;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_3__source_iter;
            generator_heap->tmp_tuple_unpack_3__source_iter = tmp_assign_source_15;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_unpack_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_3__source_iter );
        tmp_unpack_6 = generator_heap->tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_6, 0, 2 );
        if ( tmp_assign_source_16 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 67;
            goto try_except_handler_8;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_3__element_1;
            generator_heap->tmp_tuple_unpack_3__element_1 = tmp_assign_source_16;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_unpack_7;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_3__source_iter );
        tmp_unpack_7 = generator_heap->tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_7, 1, 2 );
        if ( tmp_assign_source_17 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 67;
            goto try_except_handler_8;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_3__element_2;
            generator_heap->tmp_tuple_unpack_3__element_2 = tmp_assign_source_17;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_3;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_3 = generator_heap->tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "ccoooooooo";
                    generator_heap->exception_lineno = 67;
                    goto try_except_handler_8;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "ccoooooooo";
            generator_heap->exception_lineno = 67;
            goto try_except_handler_8;
        }
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_8:;
    generator_heap->exception_keeper_type_5 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_5 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_5 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_5 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_3__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_3__source_iter );
    generator_heap->tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_5;
    generator_heap->exception_value = generator_heap->exception_keeper_value_5;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_5;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_5;

    goto try_except_handler_7;
    // End of try:
    try_end_5:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_7:;
    generator_heap->exception_keeper_type_6 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_6 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_6 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_6 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_3__element_1 );
    generator_heap->tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_3__element_2 );
    generator_heap->tmp_tuple_unpack_3__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_6;
    generator_heap->exception_value = generator_heap->exception_keeper_value_6;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_6;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_6;

    goto try_except_handler_2;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_3__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_3__source_iter );
    generator_heap->tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assign_source_18;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_3__element_1 );
        tmp_assign_source_18 = generator_heap->tmp_tuple_unpack_3__element_1;
        {
            PyObject *old = generator_heap->var_key;
            generator_heap->var_key = tmp_assign_source_18;
            Py_INCREF( generator_heap->var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_3__element_1 );
    generator_heap->tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_19;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_19 = generator_heap->tmp_tuple_unpack_3__element_2;
        {
            PyObject *old = generator_heap->var_argument;
            generator_heap->var_argument = tmp_assign_source_19;
            Py_INCREF( generator_heap->var_argument );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_3__element_2 );
    generator_heap->tmp_tuple_unpack_3__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( generator_heap->var_key );
        tmp_compexpr_left_1 = generator_heap->var_key;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_debug );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 69;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }

            tmp_called_instance_2 = tmp_mvar_value_1;
            generator->m_frame->m_frame.f_lineno = 69;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_warning, &PyTuple_GET_ITEM( const_tuple_str_digest_81d2e612c5ac85c7db6de6890bf5da00_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 69;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_ValueError;
            generator_heap->exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            generator_heap->exception_lineno = 70;
            RAISE_EXCEPTION_WITH_TYPE( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( generator_heap->var_argument );
        tmp_compexpr_left_2 = generator_heap->var_argument;
        tmp_compexpr_right_2 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( generator_heap->var_optional );
        tmp_operand_name_1 = generator_heap->var_optional;
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 71;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_len_arg_1;
            PyObject *tmp_args_element_name_5;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_debug );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 72;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warning );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 72;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }
            tmp_args_element_name_2 = const_str_digest_bd59610485e783b5a38c0028142fe0a4;
            CHECK_OBJECT( generator_heap->var_name );
            tmp_args_element_name_3 = generator_heap->var_name;
            if ( PyCell_GET( generator->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "parameters" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 73;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }

            tmp_len_arg_1 = PyCell_GET( generator->m_closure[1] );
            tmp_args_element_name_4 = BUILTIN_LEN( tmp_len_arg_1 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_called_name_2 );

                generator_heap->exception_lineno = 73;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( generator_heap->var_i );
            tmp_args_element_name_5 = generator_heap->var_i;
            generator->m_frame->m_frame.f_lineno = 72;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 72;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_raise_type_2;
            tmp_raise_type_2 = PyExc_ValueError;
            generator_heap->exception_type = tmp_raise_type_2;
            Py_INCREF( tmp_raise_type_2 );
            generator_heap->exception_lineno = 74;
            RAISE_EXCEPTION_WITH_TYPE( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_20;
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( generator_heap->var_argument );
        tmp_compexpr_left_3 = generator_heap->var_argument;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 76;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }

        tmp_assign_source_20 = tmp_mvar_value_3;
        Py_INCREF( tmp_assign_source_20 );
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( generator_heap->var_argument );
        tmp_called_instance_3 = generator_heap->var_argument;
        generator->m_frame->m_frame.f_lineno = 76;
        tmp_assign_source_20 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_infer );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 76;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        condexpr_end_1:;
        {
            PyObject *old = generator_heap->var_context_set;
            generator_heap->var_context_set = tmp_assign_source_20;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_4;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_operand_name_3;
        CHECK_OBJECT( generator_heap->var_context_set );
        tmp_operand_name_2 = generator_heap->var_context_set;
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        tmp_and_left_value_2 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        CHECK_OBJECT( generator_heap->var_optional );
        tmp_operand_name_3 = generator_heap->var_optional;
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        tmp_and_right_value_2 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_4 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_condition_result_4 = tmp_and_left_value_2;
        and_end_2:;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_debug );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 82;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }

            tmp_called_instance_4 = tmp_mvar_value_4;
            tmp_args_element_name_6 = const_str_digest_4b95f835c9f8b3337fa03581580fe419;
            CHECK_OBJECT( generator_heap->var_name );
            tmp_args_element_name_7 = generator_heap->var_name;
            generator->m_frame->m_frame.f_lineno = 82;
            {
                PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_warning, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 82;
                generator_heap->type_description_1 = "ccoooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        {
            PyObject *tmp_raise_type_3;
            tmp_raise_type_3 = PyExc_ValueError;
            generator_heap->exception_type = tmp_raise_type_3;
            Py_INCREF( tmp_raise_type_3 );
            generator_heap->exception_lineno = 83;
            RAISE_EXCEPTION_WITH_TYPE( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_context_set );
        tmp_expression_name_1 = generator_heap->var_context_set;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 84;
            generator_heap->type_description_1 = "ccoooooooo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 66;
        generator_heap->type_description_1 = "ccoooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_7 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_7 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_7 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_7 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_7;
    generator_heap->exception_value = generator_heap->exception_keeper_value_7;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_7;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator->m_closure[1],
            generator_heap->var_iterator,
            generator_heap->var_i,
            generator_heap->var_name,
            generator_heap->var_optional,
            generator_heap->var_allow_kwargs,
            generator_heap->var_key,
            generator_heap->var_argument,
            generator_heap->var_context_set
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_8 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_8 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_8 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_8 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_iterator );
    generator_heap->var_iterator = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;

    Py_XDECREF( generator_heap->var_optional );
    generator_heap->var_optional = NULL;

    Py_XDECREF( generator_heap->var_allow_kwargs );
    generator_heap->var_allow_kwargs = NULL;

    Py_XDECREF( generator_heap->var_key );
    generator_heap->var_key = NULL;

    Py_XDECREF( generator_heap->var_argument );
    generator_heap->var_argument = NULL;

    Py_XDECREF( generator_heap->var_context_set );
    generator_heap->var_context_set = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_8;
    generator_heap->exception_value = generator_heap->exception_keeper_value_8;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_8;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:
    try_end_8:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_iterator );
    Py_DECREF( generator_heap->var_iterator );
    generator_heap->var_iterator = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;

    Py_XDECREF( generator_heap->var_optional );
    generator_heap->var_optional = NULL;

    Py_XDECREF( generator_heap->var_allow_kwargs );
    generator_heap->var_allow_kwargs = NULL;

    Py_XDECREF( generator_heap->var_key );
    generator_heap->var_key = NULL;

    Py_XDECREF( generator_heap->var_argument );
    generator_heap->var_argument = NULL;

    Py_XDECREF( generator_heap->var_context_set );
    generator_heap->var_context_set = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_context,
        module_jedi$evaluate$arguments,
        const_str_plain__iterate_argument_clinic,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_c79604a1e1235335690b09bbc78cc449,
        2,
        sizeof(struct jedi$evaluate$arguments$$$function_3__iterate_argument_clinic$$$genobj_1__iterate_argument_clinic_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_4__parse_argument_clinic( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_string = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_string;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_4__parse_argument_clinic );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_4__parse_argument_clinic );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_locals {
    PyObject *var_allow_kwargs;
    PyObject *var_optional;
    PyObject *var_match;
    PyObject *var_word;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
};

static PyObject *jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_allow_kwargs = NULL;
    generator_heap->var_optional = NULL;
    generator_heap->var_match = NULL;
    generator_heap->var_word = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_False;
        assert( generator_heap->var_allow_kwargs == NULL );
        Py_INCREF( tmp_assign_source_1 );
        generator_heap->var_allow_kwargs = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = Py_False;
        assert( generator_heap->var_optional == NULL );
        Py_INCREF( tmp_assign_source_2 );
        generator_heap->var_optional = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_f4e7584e431f136d5523962d1dd15696, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "string" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 90;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = PyCell_GET( generator->m_closure[0] );
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 90;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 95;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_match );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 95;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_digest_0e3318c63d5653247032da60bc3c9c1f;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "string" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 95;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 95;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 95;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = generator_heap->var_match;
            generator_heap->var_match = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "string" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 96;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = PyCell_GET( generator->m_closure[0] );
        CHECK_OBJECT( generator_heap->var_match );
        tmp_called_instance_1 = generator_heap->var_match;
        generator->m_frame->m_frame.f_lineno = 96;
        tmp_len_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 96;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_start_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_start_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 96;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_stop_name_1 = Py_None;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_start_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_assign_source_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 96;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = PyCell_GET( generator->m_closure[0] );
            PyCell_SET( generator->m_closure[0], tmp_assign_source_4 );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( generator_heap->var_match );
        tmp_called_instance_2 = generator_heap->var_match;
        generator->m_frame->m_frame.f_lineno = 97;
        tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 97;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 97;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = Py_True;
            {
                PyObject *old = generator_heap->var_allow_kwargs;
                assert( old != NULL );
                generator_heap->var_allow_kwargs = tmp_assign_source_5;
                Py_INCREF( generator_heap->var_allow_kwargs );
                Py_DECREF( old );
            }

        }
        goto loop_start_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_6;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_value_name_1;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( generator_heap->var_optional );
        tmp_or_left_value_1 = generator_heap->var_optional;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 100;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( generator_heap->var_match );
        tmp_called_instance_3 = generator_heap->var_match;
        generator->m_frame->m_frame.f_lineno = 100;
        tmp_value_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 100;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 100;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = ( generator_heap->tmp_res != 0 ) ? Py_True : Py_False;
        tmp_assign_source_6 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_assign_source_6 = tmp_or_left_value_1;
        or_end_1:;
        {
            PyObject *old = generator_heap->var_optional;
            assert( old != NULL );
            generator_heap->var_optional = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_optional );
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( generator_heap->var_match );
        tmp_called_instance_4 = generator_heap->var_match;
        generator->m_frame->m_frame.f_lineno = 101;
        tmp_assign_source_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 101;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = generator_heap->var_word;
            generator_heap->var_word = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_word );
        tmp_tuple_element_1 = generator_heap->var_word;
        tmp_expression_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_optional );
        tmp_tuple_element_1 = generator_heap->var_optional;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_allow_kwargs );
        tmp_tuple_element_1 = generator_heap->var_allow_kwargs;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 2, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 102;
            generator_heap->type_description_1 = "coooo";
            goto frame_exception_exit_1;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 90;
        generator_heap->type_description_1 = "coooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator_heap->var_allow_kwargs,
            generator_heap->var_optional,
            generator_heap->var_match,
            generator_heap->var_word
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->var_allow_kwargs );
    Py_DECREF( generator_heap->var_allow_kwargs );
    generator_heap->var_allow_kwargs = NULL;

    Py_XDECREF( generator_heap->var_optional );
    generator_heap->var_optional = NULL;

    Py_XDECREF( generator_heap->var_match );
    generator_heap->var_match = NULL;

    Py_XDECREF( generator_heap->var_word );
    generator_heap->var_word = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)generator_heap->var_allow_kwargs );
    Py_DECREF( generator_heap->var_allow_kwargs );
    generator_heap->var_allow_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_optional );
    Py_DECREF( generator_heap->var_optional );
    generator_heap->var_optional = NULL;

    Py_XDECREF( generator_heap->var_match );
    generator_heap->var_match = NULL;

    Py_XDECREF( generator_heap->var_word );
    generator_heap->var_word = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_context,
        module_jedi$evaluate$arguments,
        const_str_plain__parse_argument_clinic,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_f4e7584e431f136d5523962d1dd15696,
        1,
        sizeof(struct jedi$evaluate$arguments$$$function_4__parse_argument_clinic$$$genobj_1__parse_argument_clinic_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_5_eval_all( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_funcdef = python_pars[ 1 ];
    PyObject *var_key = NULL;
    PyObject *var_lazy_context = NULL;
    PyObject *var_types = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_511e84622be8967aa3d3dad49070cbde;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_511e84622be8967aa3d3dad49070cbde = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_511e84622be8967aa3d3dad49070cbde, codeobj_511e84622be8967aa3d3dad49070cbde, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_511e84622be8967aa3d3dad49070cbde = cache_frame_511e84622be8967aa3d3dad49070cbde;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_511e84622be8967aa3d3dad49070cbde );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_511e84622be8967aa3d3dad49070cbde ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_511e84622be8967aa3d3dad49070cbde->m_frame.f_lineno = 115;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_unpack );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 115;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 115;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 115;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 115;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 115;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_6;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_lazy_context;
            var_lazy_context = tmp_assign_source_7;
            Py_INCREF( var_lazy_context );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_lazy_context );
        tmp_called_instance_2 = var_lazy_context;
        frame_511e84622be8967aa3d3dad49070cbde->m_frame.f_lineno = 116;
        tmp_assign_source_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_infer );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_types;
            var_types = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_try_iter_content );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_try_iter_content );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "try_iter_content" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_types );
        tmp_args_element_name_1 = var_types;
        frame_511e84622be8967aa3d3dad49070cbde->m_frame.f_lineno = 117;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 115;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_511e84622be8967aa3d3dad49070cbde );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_511e84622be8967aa3d3dad49070cbde );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_511e84622be8967aa3d3dad49070cbde, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_511e84622be8967aa3d3dad49070cbde->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_511e84622be8967aa3d3dad49070cbde, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_511e84622be8967aa3d3dad49070cbde,
        type_description_1,
        par_self,
        par_funcdef,
        var_key,
        var_lazy_context,
        var_types
    );


    // Release cached frame.
    if ( frame_511e84622be8967aa3d3dad49070cbde == cache_frame_511e84622be8967aa3d3dad49070cbde )
    {
        Py_DECREF( frame_511e84622be8967aa3d3dad49070cbde );
    }
    cache_frame_511e84622be8967aa3d3dad49070cbde = NULL;

    assertFrameObject( frame_511e84622be8967aa3d3dad49070cbde );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_5_eval_all );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    Py_XDECREF( var_types );
    var_types = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    Py_XDECREF( var_types );
    var_types = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_5_eval_all );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_6_get_calling_nodes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = PyList_New( 0 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_6_get_calling_nodes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_6_get_calling_nodes );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_7_unpack( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_funcdef = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_2ddac29db203e3015a4f8876a14332ec;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2ddac29db203e3015a4f8876a14332ec = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ddac29db203e3015a4f8876a14332ec, codeobj_2ddac29db203e3015a4f8876a14332ec, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *) );
    frame_2ddac29db203e3015a4f8876a14332ec = cache_frame_2ddac29db203e3015a4f8876a14332ec;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ddac29db203e3015a4f8876a14332ec );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ddac29db203e3015a4f8876a14332ec ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        tmp_raise_type_1 = PyExc_NotImplementedError;
        exception_type = tmp_raise_type_1;
        Py_INCREF( tmp_raise_type_1 );
        exception_lineno = 123;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ddac29db203e3015a4f8876a14332ec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ddac29db203e3015a4f8876a14332ec );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ddac29db203e3015a4f8876a14332ec, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ddac29db203e3015a4f8876a14332ec->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ddac29db203e3015a4f8876a14332ec, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ddac29db203e3015a4f8876a14332ec,
        type_description_1,
        par_self,
        par_funcdef
    );


    // Release cached frame.
    if ( frame_2ddac29db203e3015a4f8876a14332ec == cache_frame_2ddac29db203e3015a4f8876a14332ec )
    {
        Py_DECREF( frame_2ddac29db203e3015a4f8876a14332ec );
    }
    cache_frame_2ddac29db203e3015a4f8876a14332ec = NULL;

    assertFrameObject( frame_2ddac29db203e3015a4f8876a14332ec );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_7_unpack );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_7_unpack );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_jedi$evaluate$arguments$$$function_8_get_executed_params( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_execution_context = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c, codeobj_1cb7a2b5f3c011c02a21a7ee5a36ae4c, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *) );
    frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c = cache_frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_get_executed_params );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_executed_params );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_executed_params" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 126;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_execution_context );
        tmp_args_element_name_1 = par_execution_context;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_2 = par_self;
        frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c->m_frame.f_lineno = 126;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c,
        type_description_1,
        par_self,
        par_execution_context
    );


    // Release cached frame.
    if ( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c == cache_frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c )
    {
        Py_DECREF( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c );
    }
    cache_frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c = NULL;

    assertFrameObject( frame_1cb7a2b5f3c011c02a21a7ee5a36ae4c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_8_get_executed_params );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_8_get_executed_params );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_9_get_executed_params( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_execution_context = python_pars[ 1 ];
    PyObject *var_search_params = NULL;
    struct Nuitka_FrameObject *frame_54d7da2c1dfc5702d05918ca8ddc2bf2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_54d7da2c1dfc5702d05918ca8ddc2bf2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_54d7da2c1dfc5702d05918ca8ddc2bf2, codeobj_54d7da2c1dfc5702d05918ca8ddc2bf2, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_54d7da2c1dfc5702d05918ca8ddc2bf2 = cache_frame_54d7da2c1dfc5702d05918ca8ddc2bf2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_4eff24856086ad17c9fda82689f59bbb;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_search_params_tuple;
        tmp_level_name_1 = const_int_0;
        frame_54d7da2c1dfc5702d05918ca8ddc2bf2->m_frame.f_lineno = 131;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_search_params );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_search_params == NULL );
        var_search_params = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_search_params );
        tmp_called_name_1 = var_search_params;
        CHECK_OBJECT( par_execution_context );
        tmp_source_name_1 = par_execution_context;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_evaluator );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_execution_context );
        tmp_args_element_name_2 = par_execution_context;
        CHECK_OBJECT( par_execution_context );
        tmp_source_name_2 = par_execution_context;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_tree_node );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 135;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_54d7da2c1dfc5702d05918ca8ddc2bf2->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_54d7da2c1dfc5702d05918ca8ddc2bf2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_54d7da2c1dfc5702d05918ca8ddc2bf2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_54d7da2c1dfc5702d05918ca8ddc2bf2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_54d7da2c1dfc5702d05918ca8ddc2bf2,
        type_description_1,
        par_self,
        par_execution_context,
        var_search_params
    );


    // Release cached frame.
    if ( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 == cache_frame_54d7da2c1dfc5702d05918ca8ddc2bf2 )
    {
        Py_DECREF( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 );
    }
    cache_frame_54d7da2c1dfc5702d05918ca8ddc2bf2 = NULL;

    assertFrameObject( frame_54d7da2c1dfc5702d05918ca8ddc2bf2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_9_get_executed_params );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)var_search_params );
    Py_DECREF( var_search_params );
    var_search_params = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    Py_XDECREF( var_search_params );
    var_search_params = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_9_get_executed_params );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_10___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_f74863e3a40c1c188a4e2a354b3186aa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f74863e3a40c1c188a4e2a354b3186aa = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f74863e3a40c1c188a4e2a354b3186aa, codeobj_f74863e3a40c1c188a4e2a354b3186aa, module_jedi$evaluate$arguments, sizeof(void *) );
    frame_f74863e3a40c1c188a4e2a354b3186aa = cache_frame_f74863e3a40c1c188a4e2a354b3186aa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f74863e3a40c1c188a4e2a354b3186aa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f74863e3a40c1c188a4e2a354b3186aa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        tmp_left_name_1 = const_str_digest_cfdbbd2b4f173bd816c499641f213719;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f74863e3a40c1c188a4e2a354b3186aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f74863e3a40c1c188a4e2a354b3186aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f74863e3a40c1c188a4e2a354b3186aa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f74863e3a40c1c188a4e2a354b3186aa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f74863e3a40c1c188a4e2a354b3186aa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f74863e3a40c1c188a4e2a354b3186aa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f74863e3a40c1c188a4e2a354b3186aa,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_f74863e3a40c1c188a4e2a354b3186aa == cache_frame_f74863e3a40c1c188a4e2a354b3186aa )
    {
        Py_DECREF( frame_f74863e3a40c1c188a4e2a354b3186aa );
    }
    cache_frame_f74863e3a40c1c188a4e2a354b3186aa = NULL;

    assertFrameObject( frame_f74863e3a40c1c188a4e2a354b3186aa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_10___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_10___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_11___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_evaluator = python_pars[ 1 ];
    PyObject *par_context = python_pars[ 2 ];
    PyObject *par_argument_node = python_pars[ 3 ];
    PyObject *par_trailer = python_pars[ 4 ];
    struct Nuitka_FrameObject *frame_4aec7604550177c33f576602ae68e7aa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4aec7604550177c33f576602ae68e7aa = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4aec7604550177c33f576602ae68e7aa, codeobj_4aec7604550177c33f576602ae68e7aa, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4aec7604550177c33f576602ae68e7aa = cache_frame_4aec7604550177c33f576602ae68e7aa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4aec7604550177c33f576602ae68e7aa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4aec7604550177c33f576602ae68e7aa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_argument_node );
        tmp_assattr_name_1 = par_argument_node;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_argument_node, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_context );
        tmp_assattr_name_2 = par_context;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_context, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_evaluator );
        tmp_assattr_name_3 = par_evaluator;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__evaluator, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_trailer );
        tmp_assattr_name_4 = par_trailer;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_trailer, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4aec7604550177c33f576602ae68e7aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4aec7604550177c33f576602ae68e7aa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4aec7604550177c33f576602ae68e7aa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4aec7604550177c33f576602ae68e7aa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4aec7604550177c33f576602ae68e7aa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4aec7604550177c33f576602ae68e7aa,
        type_description_1,
        par_self,
        par_evaluator,
        par_context,
        par_argument_node,
        par_trailer
    );


    // Release cached frame.
    if ( frame_4aec7604550177c33f576602ae68e7aa == cache_frame_4aec7604550177c33f576602ae68e7aa )
    {
        Py_DECREF( frame_4aec7604550177c33f576602ae68e7aa );
    }
    cache_frame_4aec7604550177c33f576602ae68e7aa = NULL;

    assertFrameObject( frame_4aec7604550177c33f576602ae68e7aa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_11___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_argument_node );
    Py_DECREF( par_argument_node );
    par_argument_node = NULL;

    CHECK_OBJECT( (PyObject *)par_trailer );
    Py_DECREF( par_trailer );
    par_trailer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_argument_node );
    Py_DECREF( par_argument_node );
    par_argument_node = NULL;

    CHECK_OBJECT( (PyObject *)par_trailer );
    Py_DECREF( par_trailer );
    par_trailer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_11___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_12__split( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_self;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_12__split );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_12__split );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_locals {
    PyObject *var_iterator;
    PyObject *var_child;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_return_value;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_iterator = NULL;
    generator_heap->var_child = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;
    generator_heap->tmp_return_value = NULL;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_d67683786dac3d83577a480799f42096, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 157;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( generator->m_closure[0] );
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_argument_node );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 157;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        generator_heap->tmp_return_value = Py_None;
        Py_INCREF( generator_heap->tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_subscript_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 162;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( generator->m_closure[0] );
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_argument_node );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 162;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_type );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 162;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_tuple_str_plain_arglist_str_plain_testlist_tuple;
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 162;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( generator_heap->tmp_res == 1 ) ? Py_True : Py_False;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 164;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = PyCell_GET( generator->m_closure[0] );
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_argument_node );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 164;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_type );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 164;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = const_str_plain_argument;
        tmp_and_left_value_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_and_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 164;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_and_left_value_1 );

            generator_heap->exception_lineno = 165;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        Py_DECREF( tmp_and_left_value_1 );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 165;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = PyCell_GET( generator->m_closure[0] );
        tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_argument_node );
        if ( tmp_source_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 165;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_children );
        Py_DECREF( tmp_source_name_6 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 165;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 165;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_tuple_str_chr_42_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e_tuple;
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 165;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( generator_heap->tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_and_right_value_1 );
        tmp_or_right_value_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_or_right_value_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_operand_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_operand_name_1 = tmp_or_left_value_1;
        or_end_1:;
        generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 162;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_expression_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_source_name_8;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            tmp_tuple_element_1 = const_int_0;
            tmp_expression_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
            if ( PyCell_GET( generator->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_expression_name_1 );
                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 166;
                generator_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_8 = PyCell_GET( generator->m_closure[0] );
            tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_argument_node );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_expression_name_1 );

                generator_heap->exception_lineno = 166;
                generator_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_operand_name_1, sizeof(PyObject *), &tmp_or_left_truth_1, sizeof(int), &tmp_or_left_value_1, sizeof(PyObject *), &tmp_or_right_value_1, sizeof(PyObject *), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(PyObject *), &tmp_and_right_value_1, sizeof(PyObject *), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_source_name_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_source_name_6, sizeof(PyObject *), &tmp_source_name_7, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_source_name_8, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_operand_name_1, sizeof(PyObject *), &tmp_or_left_truth_1, sizeof(int), &tmp_or_left_value_1, sizeof(PyObject *), &tmp_or_right_value_1, sizeof(PyObject *), &tmp_compexpr_left_2, sizeof(PyObject *), &tmp_compexpr_right_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_source_name_3, sizeof(PyObject *), &tmp_and_left_truth_1, sizeof(int), &tmp_and_left_value_1, sizeof(PyObject *), &tmp_and_right_value_1, sizeof(PyObject *), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_source_name_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_source_name_6, sizeof(PyObject *), &tmp_source_name_7, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_source_name_8, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 166;
                generator_heap->type_description_1 = "coo";
                goto frame_exception_exit_1;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        generator_heap->tmp_return_value = Py_None;
        Py_INCREF( generator_heap->tmp_return_value );
        goto frame_return_exit_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_9;
        PyObject *tmp_source_name_10;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 169;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = PyCell_GET( generator->m_closure[0] );
        tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_argument_node );
        if ( tmp_source_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 169;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_children );
        Py_DECREF( tmp_source_name_9 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 169;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 169;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_iterator == NULL );
        generator_heap->var_iterator = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( generator_heap->var_iterator );
        tmp_iter_arg_2 = generator_heap->var_iterator;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 170;
            generator_heap->type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "coo";
                generator_heap->exception_lineno = 170;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_child;
            generator_heap->var_child = tmp_assign_source_4;
            Py_INCREF( generator_heap->var_child );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( generator_heap->var_child );
        tmp_compexpr_left_5 = generator_heap->var_child;
        tmp_compexpr_right_5 = const_str_chr_44;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 171;
            generator_heap->type_description_1 = "coo";
            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        goto loop_start_1;
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( generator_heap->var_child );
            tmp_compexpr_left_6 = generator_heap->var_child;
            tmp_compexpr_right_6 = const_tuple_str_chr_42_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e_tuple;
            generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 173;
                generator_heap->type_description_1 = "coo";
                goto try_except_handler_2;
            }
            tmp_condition_result_4 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_expression_name_2;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_len_arg_1;
                PyObject *tmp_source_name_11;
                PyObject *tmp_value_name_1;
                NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
                CHECK_OBJECT( generator_heap->var_child );
                tmp_source_name_11 = generator_heap->var_child;
                tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_value );
                if ( tmp_len_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 174;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_tuple_element_2 = BUILTIN_LEN( tmp_len_arg_1 );
                Py_DECREF( tmp_len_arg_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 174;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_expression_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_expression_name_2, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( generator_heap->var_iterator );
                tmp_value_name_1 = generator_heap->var_iterator;
                tmp_tuple_element_2 = ITERATOR_NEXT( tmp_value_name_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        generator_heap->exception_type = PyExc_StopIteration;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = NULL;
                        generator_heap->exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    }
                    Py_DECREF( tmp_expression_name_2 );

                    generator_heap->type_description_1 = "coo";
                    generator_heap->exception_lineno = 174;
                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_expression_name_2, 1, tmp_tuple_element_2 );
                Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_6, sizeof(PyObject *), &tmp_compexpr_right_6, sizeof(PyObject *), &tmp_tuple_element_2, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_source_name_11, sizeof(PyObject *), &tmp_value_name_1, sizeof(PyObject *), NULL );
                generator->m_yield_return_index = 2;
                return tmp_expression_name_2;
                yield_return_2:
                Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_6, sizeof(PyObject *), &tmp_compexpr_right_6, sizeof(PyObject *), &tmp_tuple_element_2, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_source_name_11, sizeof(PyObject *), &tmp_value_name_1, sizeof(PyObject *), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 174;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_yield_result_2 = yield_return_value;
            }
            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                int tmp_and_left_truth_2;
                nuitka_bool tmp_and_left_value_2;
                nuitka_bool tmp_and_right_value_2;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                PyObject *tmp_source_name_12;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_source_name_13;
                PyObject *tmp_subscript_name_2;
                CHECK_OBJECT( generator_heap->var_child );
                tmp_source_name_12 = generator_heap->var_child;
                tmp_compexpr_left_7 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_type );
                if ( tmp_compexpr_left_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 175;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_compexpr_right_7 = const_str_plain_argument;
                generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                Py_DECREF( tmp_compexpr_left_7 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 175;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_and_left_value_2 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
                if ( tmp_and_left_truth_2 == 1 )
                {
                    goto and_right_2;
                }
                else
                {
                    goto and_left_2;
                }
                and_right_2:;
                CHECK_OBJECT( generator_heap->var_child );
                tmp_source_name_13 = generator_heap->var_child;
                tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_children );
                if ( tmp_subscribed_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 176;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_subscript_name_2 = const_int_0;
                tmp_compexpr_left_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
                Py_DECREF( tmp_subscribed_name_2 );
                if ( tmp_compexpr_left_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 176;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_compexpr_right_8 = const_tuple_str_chr_42_str_digest_29bbf2a0ea668f7cb550eb66ebe90f9e_tuple;
                generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_8, tmp_compexpr_left_8 );
                Py_DECREF( tmp_compexpr_left_8 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 176;
                    generator_heap->type_description_1 = "coo";
                    goto try_except_handler_2;
                }
                tmp_and_right_value_2 = ( generator_heap->tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_condition_result_5 = tmp_and_right_value_2;
                goto and_end_2;
                and_left_2:;
                tmp_condition_result_5 = tmp_and_left_value_2;
                and_end_2:;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    nuitka_bool tmp_condition_result_6;
                    PyObject *tmp_compexpr_left_9;
                    PyObject *tmp_compexpr_right_9;
                    PyObject *tmp_len_arg_2;
                    PyObject *tmp_source_name_14;
                    CHECK_OBJECT( generator_heap->var_child );
                    tmp_source_name_14 = generator_heap->var_child;
                    tmp_len_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_children );
                    if ( tmp_len_arg_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 177;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_compexpr_left_9 = BUILTIN_LEN( tmp_len_arg_2 );
                    Py_DECREF( tmp_len_arg_2 );
                    if ( tmp_compexpr_left_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 177;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_compexpr_right_9 = const_int_pos_2;
                    generator_heap->tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                    Py_DECREF( tmp_compexpr_left_9 );
                    assert( !(generator_heap->tmp_res == -1) );
                    tmp_condition_result_6 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_raise_type_1;
                        tmp_raise_type_1 = PyExc_AssertionError;
                        generator_heap->exception_type = tmp_raise_type_1;
                        Py_INCREF( tmp_raise_type_1 );
                        generator_heap->exception_lineno = 177;
                        RAISE_EXCEPTION_WITH_TYPE( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    branch_no_6:;
                }
                {
                    PyObject *tmp_expression_name_3;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_len_arg_3;
                    PyObject *tmp_source_name_15;
                    PyObject *tmp_subscribed_name_3;
                    PyObject *tmp_source_name_16;
                    PyObject *tmp_subscript_name_3;
                    PyObject *tmp_subscribed_name_4;
                    PyObject *tmp_source_name_17;
                    PyObject *tmp_subscript_name_4;
                    NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_3;
                    CHECK_OBJECT( generator_heap->var_child );
                    tmp_source_name_16 = generator_heap->var_child;
                    tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_children );
                    if ( tmp_subscribed_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_subscript_name_3 = const_int_0;
                    tmp_source_name_15 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    if ( tmp_source_name_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_len_arg_3 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_value );
                    Py_DECREF( tmp_source_name_15 );
                    if ( tmp_len_arg_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_tuple_element_3 = BUILTIN_LEN( tmp_len_arg_3 );
                    Py_DECREF( tmp_len_arg_3 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_expression_name_3 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_expression_name_3, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( generator_heap->var_child );
                    tmp_source_name_17 = generator_heap->var_child;
                    tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_children );
                    if ( tmp_subscribed_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        Py_DECREF( tmp_expression_name_3 );

                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_subscript_name_4 = const_int_pos_1;
                    tmp_tuple_element_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 1 );
                    Py_DECREF( tmp_subscribed_name_4 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        Py_DECREF( tmp_expression_name_3 );

                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    PyTuple_SET_ITEM( tmp_expression_name_3, 1, tmp_tuple_element_3 );
                    Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_6, sizeof(PyObject *), &tmp_compexpr_right_6, sizeof(PyObject *), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_and_left_truth_2, sizeof(int), &tmp_and_left_value_2, sizeof(nuitka_bool), &tmp_and_right_value_2, sizeof(nuitka_bool), &tmp_compexpr_left_7, sizeof(PyObject *), &tmp_compexpr_right_7, sizeof(PyObject *), &tmp_source_name_12, sizeof(PyObject *), &tmp_compexpr_left_8, sizeof(PyObject *), &tmp_compexpr_right_8, sizeof(PyObject *), &tmp_subscribed_name_2, sizeof(PyObject *), &tmp_source_name_13, sizeof(PyObject *), &tmp_subscript_name_2, sizeof(PyObject *), &tmp_tuple_element_3, sizeof(PyObject *), &tmp_len_arg_3, sizeof(PyObject *), &tmp_source_name_15, sizeof(PyObject *), &tmp_subscribed_name_3, sizeof(PyObject *), &tmp_source_name_16, sizeof(PyObject *), &tmp_subscript_name_3, sizeof(PyObject *), &tmp_subscribed_name_4, sizeof(PyObject *), &tmp_source_name_17, sizeof(PyObject *), &tmp_subscript_name_4, sizeof(PyObject *), NULL );
                    generator->m_yield_return_index = 3;
                    return tmp_expression_name_3;
                    yield_return_3:
                    Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_6, sizeof(PyObject *), &tmp_compexpr_right_6, sizeof(PyObject *), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_and_left_truth_2, sizeof(int), &tmp_and_left_value_2, sizeof(nuitka_bool), &tmp_and_right_value_2, sizeof(nuitka_bool), &tmp_compexpr_left_7, sizeof(PyObject *), &tmp_compexpr_right_7, sizeof(PyObject *), &tmp_source_name_12, sizeof(PyObject *), &tmp_compexpr_left_8, sizeof(PyObject *), &tmp_compexpr_right_8, sizeof(PyObject *), &tmp_subscribed_name_2, sizeof(PyObject *), &tmp_source_name_13, sizeof(PyObject *), &tmp_subscript_name_2, sizeof(PyObject *), &tmp_tuple_element_3, sizeof(PyObject *), &tmp_len_arg_3, sizeof(PyObject *), &tmp_source_name_15, sizeof(PyObject *), &tmp_subscribed_name_3, sizeof(PyObject *), &tmp_source_name_16, sizeof(PyObject *), &tmp_subscript_name_3, sizeof(PyObject *), &tmp_subscribed_name_4, sizeof(PyObject *), &tmp_source_name_17, sizeof(PyObject *), &tmp_subscript_name_4, sizeof(PyObject *), NULL );
                    if ( yield_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 178;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_yield_result_3 = yield_return_value;
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    PyObject *tmp_expression_name_4;
                    PyObject *tmp_tuple_element_4;
                    NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_4;
                    tmp_tuple_element_4 = const_int_0;
                    tmp_expression_name_4 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_4 );
                    PyTuple_SET_ITEM( tmp_expression_name_4, 0, tmp_tuple_element_4 );
                    CHECK_OBJECT( generator_heap->var_child );
                    tmp_tuple_element_4 = generator_heap->var_child;
                    Py_INCREF( tmp_tuple_element_4 );
                    PyTuple_SET_ITEM( tmp_expression_name_4, 1, tmp_tuple_element_4 );
                    Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_6, sizeof(PyObject *), &tmp_compexpr_right_6, sizeof(PyObject *), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_and_left_truth_2, sizeof(int), &tmp_and_left_value_2, sizeof(nuitka_bool), &tmp_and_right_value_2, sizeof(nuitka_bool), &tmp_compexpr_left_7, sizeof(PyObject *), &tmp_compexpr_right_7, sizeof(PyObject *), &tmp_source_name_12, sizeof(PyObject *), &tmp_compexpr_left_8, sizeof(PyObject *), &tmp_compexpr_right_8, sizeof(PyObject *), &tmp_subscribed_name_2, sizeof(PyObject *), &tmp_source_name_13, sizeof(PyObject *), &tmp_subscript_name_2, sizeof(PyObject *), &tmp_tuple_element_4, sizeof(PyObject *), NULL );
                    generator->m_yield_return_index = 4;
                    return tmp_expression_name_4;
                    yield_return_4:
                    Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_6, sizeof(PyObject *), &tmp_compexpr_right_6, sizeof(PyObject *), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_and_left_truth_2, sizeof(int), &tmp_and_left_value_2, sizeof(nuitka_bool), &tmp_and_right_value_2, sizeof(nuitka_bool), &tmp_compexpr_left_7, sizeof(PyObject *), &tmp_compexpr_right_7, sizeof(PyObject *), &tmp_source_name_12, sizeof(PyObject *), &tmp_compexpr_left_8, sizeof(PyObject *), &tmp_compexpr_right_8, sizeof(PyObject *), &tmp_subscribed_name_2, sizeof(PyObject *), &tmp_source_name_13, sizeof(PyObject *), &tmp_subscript_name_2, sizeof(PyObject *), &tmp_tuple_element_4, sizeof(PyObject *), NULL );
                    if ( yield_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 180;
                        generator_heap->type_description_1 = "coo";
                        goto try_except_handler_2;
                    }
                    tmp_yield_result_4 = yield_return_value;
                }
                branch_end_5:;
            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 170;
        generator_heap->type_description_1 = "coo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator_heap->var_iterator,
            generator_heap->var_child
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_iterator );
    generator_heap->var_iterator = NULL;

    Py_XDECREF( generator_heap->var_child );
    generator_heap->var_child = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_iterator );
    Py_DECREF( generator_heap->var_iterator );
    generator_heap->var_iterator = NULL;

    Py_XDECREF( generator_heap->var_child );
    generator_heap->var_child = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;
    // The above won't return, but we need to make it clear to the compiler
    // as well, or else it will complain and/or generate inferior code.
    assert(false);
    return NULL;

    function_return_exit:
#if PYTHON_VERSION >= 300
    generator->m_returned = generator_heap->tmp_return_value;
#endif

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_context,
        module_jedi$evaluate$arguments,
        const_str_plain__split,
#if PYTHON_VERSION >= 350
        const_str_digest_92556a1adf162047547119b2621563b3,
#endif
        codeobj_d67683786dac3d83577a480799f42096,
        1,
        sizeof(struct jedi$evaluate$arguments$$$function_12__split$$$genobj_1__split_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_13_unpack( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_funcdef = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_funcdef;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_self;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_locals {
    PyObject *var_named_args;
    PyObject *var_star_count;
    PyObject *var_el;
    PyObject *var_arrays;
    PyObject *var_iterators;
    PyObject *var_values;
    PyObject *var_dct;
    PyObject *var_key;
    PyObject *var_c;
    PyObject *var_comp;
    PyObject *var_named_arg;
    PyObject *outline_0_var_a;
    PyObject *outline_1_var_v;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_for_loop_2__for_iterator;
    PyObject *tmp_for_loop_2__iter_value;
    PyObject *tmp_for_loop_3__for_iterator;
    PyObject *tmp_for_loop_3__iter_value;
    PyObject *tmp_for_loop_4__for_iterator;
    PyObject *tmp_for_loop_4__iter_value;
    PyObject *tmp_for_loop_5__for_iterator;
    PyObject *tmp_for_loop_5__iter_value;
    PyObject *tmp_listcomp_1__$0;
    PyObject *tmp_listcomp_1__contraction;
    PyObject *tmp_listcomp_1__iter_value_0;
    PyObject *tmp_listcomp_2__$0;
    PyObject *tmp_listcomp_2__contraction;
    PyObject *tmp_listcomp_2__iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    PyObject *tmp_tuple_unpack_2__element_1;
    PyObject *tmp_tuple_unpack_2__element_2;
    PyObject *tmp_tuple_unpack_2__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    int tmp_res;
    struct Nuitka_FrameObject *frame_954935d8cfae61494ac9145b532bef9c_2;
    char const *type_description_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    struct Nuitka_FrameObject *frame_68cfc9138ef07e15b5263a4d3603a93d_3;
    char const *type_description_3;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    int exception_keeper_lineno_6;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    int exception_keeper_lineno_14;
};

static PyObject *jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 5: goto yield_return_5;
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_frame_954935d8cfae61494ac9145b532bef9c_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_68cfc9138ef07e15b5263a4d3603a93d_3 = NULL;
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_named_args = NULL;
    generator_heap->var_star_count = NULL;
    generator_heap->var_el = NULL;
    generator_heap->var_arrays = NULL;
    generator_heap->var_iterators = NULL;
    generator_heap->var_values = NULL;
    generator_heap->var_dct = NULL;
    generator_heap->var_key = NULL;
    generator_heap->var_c = NULL;
    generator_heap->var_comp = NULL;
    generator_heap->var_named_arg = NULL;
    generator_heap->outline_0_var_a = NULL;
    generator_heap->outline_1_var_v = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_for_loop_2__for_iterator = NULL;
    generator_heap->tmp_for_loop_2__iter_value = NULL;
    generator_heap->tmp_for_loop_3__for_iterator = NULL;
    generator_heap->tmp_for_loop_3__iter_value = NULL;
    generator_heap->tmp_for_loop_4__for_iterator = NULL;
    generator_heap->tmp_for_loop_4__iter_value = NULL;
    generator_heap->tmp_for_loop_5__for_iterator = NULL;
    generator_heap->tmp_for_loop_5__iter_value = NULL;
    generator_heap->tmp_listcomp_1__$0 = NULL;
    generator_heap->tmp_listcomp_1__contraction = NULL;
    generator_heap->tmp_listcomp_1__iter_value_0 = NULL;
    generator_heap->tmp_listcomp_2__$0 = NULL;
    generator_heap->tmp_listcomp_2__contraction = NULL;
    generator_heap->tmp_listcomp_2__iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->tmp_tuple_unpack_2__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_2__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_2__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;
    generator_heap->type_description_2 = NULL;
    generator_heap->type_description_3 = NULL;

    // Actual generator function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( generator_heap->var_named_args == NULL );
        generator_heap->var_named_args = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_c84bee060b6acd8351ea26c2a9cbc92d, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 184;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( generator->m_closure[1] );
        generator->m_frame->m_frame.f_lineno = 184;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 184;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 184;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccooooooooooo";
                generator_heap->exception_lineno = 184;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = generator_heap->tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 184;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccooooooooooo";
            generator_heap->exception_lineno = 184;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "ccooooooooooo";
            generator_heap->exception_lineno = 184;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "ccooooooooooo";
                    generator_heap->exception_lineno = 184;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "ccooooooooooo";
            generator_heap->exception_lineno = 184;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_star_count;
            generator_heap->var_star_count = tmp_assign_source_7;
            Py_INCREF( generator_heap->var_star_count );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_el;
            generator_heap->var_el = tmp_assign_source_8;
            Py_INCREF( generator_heap->var_el );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( generator_heap->var_star_count );
        tmp_compexpr_left_1 = generator_heap->var_star_count;
        tmp_compexpr_right_1 = const_int_pos_1;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 185;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_args_element_name_1;
            if ( PyCell_GET( generator->m_closure[1] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 186;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }

            tmp_source_name_1 = PyCell_GET( generator->m_closure[1] );
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_context );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 186;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( generator_heap->var_el );
            tmp_args_element_name_1 = generator_heap->var_el;
            generator->m_frame->m_frame.f_lineno = 186;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_eval_node, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 186;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = generator_heap->var_arrays;
                generator_heap->var_arrays = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_10;
            // Tried code:
            {
                PyObject *tmp_assign_source_11;
                PyObject *tmp_iter_arg_3;
                CHECK_OBJECT( generator_heap->var_arrays );
                tmp_iter_arg_3 = generator_heap->var_arrays;
                tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_3 );
                if ( tmp_assign_source_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_5;
                }
                {
                    PyObject *old = generator_heap->tmp_listcomp_1__$0;
                    generator_heap->tmp_listcomp_1__$0 = tmp_assign_source_11;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_12;
                tmp_assign_source_12 = PyList_New( 0 );
                {
                    PyObject *old = generator_heap->tmp_listcomp_1__contraction;
                    generator_heap->tmp_listcomp_1__contraction = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            MAKE_OR_REUSE_FRAME( cache_frame_954935d8cfae61494ac9145b532bef9c_2, codeobj_954935d8cfae61494ac9145b532bef9c, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
            generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 = cache_frame_954935d8cfae61494ac9145b532bef9c_2;

            // Push the new frame as the currently active one.
            pushFrameStack( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_13;
                CHECK_OBJECT( generator_heap->tmp_listcomp_1__$0 );
                tmp_next_source_2 = generator_heap->tmp_listcomp_1__$0;
                tmp_assign_source_13 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_13 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_2 = "ococ";
                        generator_heap->exception_lineno = 187;
                        goto try_except_handler_6;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_listcomp_1__iter_value_0;
                    generator_heap->tmp_listcomp_1__iter_value_0 = tmp_assign_source_13;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_14;
                CHECK_OBJECT( generator_heap->tmp_listcomp_1__iter_value_0 );
                tmp_assign_source_14 = generator_heap->tmp_listcomp_1__iter_value_0;
                {
                    PyObject *old = generator_heap->outline_0_var_a;
                    generator_heap->outline_0_var_a = tmp_assign_source_14;
                    Py_INCREF( generator_heap->outline_0_var_a );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_source_name_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                CHECK_OBJECT( generator_heap->tmp_listcomp_1__contraction );
                tmp_append_list_1 = generator_heap->tmp_listcomp_1__contraction;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__iterate_star_args );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__iterate_star_args );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_iterate_star_args" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_2 = "ococ";
                    goto try_except_handler_6;
                }

                tmp_called_name_1 = tmp_mvar_value_1;
                if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_2 = "ococ";
                    goto try_except_handler_6;
                }

                tmp_source_name_2 = PyCell_GET( generator->m_closure[1] );
                tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_context );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_2 = "ococ";
                    goto try_except_handler_6;
                }
                CHECK_OBJECT( generator_heap->outline_0_var_a );
                tmp_args_element_name_3 = generator_heap->outline_0_var_a;
                CHECK_OBJECT( generator_heap->var_el );
                tmp_args_element_name_4 = generator_heap->var_el;
                if ( PyCell_GET( generator->m_closure[0] ) == NULL )
                {
                    Py_DECREF( tmp_args_element_name_2 );
                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "funcdef" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_2 = "ococ";
                    goto try_except_handler_6;
                }

                tmp_args_element_name_5 = PyCell_GET( generator->m_closure[0] );
                generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2->m_frame.f_lineno = 187;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                    tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_append_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_2 = "ococ";
                    goto try_except_handler_6;
                }
                assert( PyList_Check( tmp_append_list_1 ) );
                generator_heap->tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 187;
                    generator_heap->type_description_2 = "ococ";
                    goto try_except_handler_6;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 187;
                generator_heap->type_description_2 = "ococ";
                goto try_except_handler_6;
            }
            goto loop_start_2;
            loop_end_2:;
            CHECK_OBJECT( generator_heap->tmp_listcomp_1__contraction );
            tmp_assign_source_10 = generator_heap->tmp_listcomp_1__contraction;
            Py_INCREF( tmp_assign_source_10 );
            goto try_return_handler_6;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack );
            return NULL;
            // Return handler code:
            try_return_handler_6:;
            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__$0 );
            Py_DECREF( generator_heap->tmp_listcomp_1__$0 );
            generator_heap->tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__contraction );
            Py_DECREF( generator_heap->tmp_listcomp_1__contraction );
            generator_heap->tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( generator_heap->tmp_listcomp_1__iter_value_0 );
            generator_heap->tmp_listcomp_1__iter_value_0 = NULL;

            goto frame_return_exit_1;
            // Exception handler code:
            try_except_handler_6:;
            generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__$0 );
            Py_DECREF( generator_heap->tmp_listcomp_1__$0 );
            generator_heap->tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__contraction );
            Py_DECREF( generator_heap->tmp_listcomp_1__contraction );
            generator_heap->tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( generator_heap->tmp_listcomp_1__iter_value_0 );
            generator_heap->tmp_listcomp_1__iter_value_0 = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_3;
            generator_heap->exception_value = generator_heap->exception_keeper_value_3;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_1:;
#if 0
            RESTORE_FRAME_EXCEPTION( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_5;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 );
#endif

            if ( generator_heap->exception_tb == NULL )
            {
                generator_heap->exception_tb = MAKE_TRACEBACK( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2, generator_heap->exception_lineno );
            }
            else if ( generator_heap->exception_tb->tb_frame != &generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2->m_frame )
            {
                generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2, generator_heap->exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2,
                generator_heap->type_description_2,
                generator_heap->outline_0_var_a,
                generator->m_closure[1],
                generator_heap->var_el,
                generator->m_closure[0]
            );


            // Release cached frame.
            if ( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 == cache_frame_954935d8cfae61494ac9145b532bef9c_2 )
            {
                Py_DECREF( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 );
            }
            cache_frame_954935d8cfae61494ac9145b532bef9c_2 = NULL;

            assertFrameObject( generator_heap->frame_954935d8cfae61494ac9145b532bef9c_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto try_except_handler_5;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack );
            return NULL;
            // Return handler code:
            try_return_handler_5:;
            Py_XDECREF( generator_heap->outline_0_var_a );
            generator_heap->outline_0_var_a = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_5:;
            generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->outline_0_var_a );
            generator_heap->outline_0_var_a = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_4;
            generator_heap->exception_value = generator_heap->exception_keeper_value_4;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack );
            return NULL;
            outline_exception_1:;
            generator_heap->exception_lineno = 187;
            goto try_except_handler_2;
            outline_result_1:;
            {
                PyObject *old = generator_heap->var_iterators;
                generator_heap->var_iterators = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_iter_arg_4;
            PyObject *tmp_list_arg_1;
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_dircall_arg2_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_zip_longest );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zip_longest );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zip_longest" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 189;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }

            tmp_dircall_arg1_1 = tmp_mvar_value_2;
            CHECK_OBJECT( generator_heap->var_iterators );
            tmp_dircall_arg2_1 = generator_heap->var_iterators;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg2_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
                tmp_list_arg_1 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
            }
            if ( tmp_list_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 189;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }
            tmp_iter_arg_4 = PySequence_List( tmp_list_arg_1 );
            Py_DECREF( tmp_list_arg_1 );
            if ( tmp_iter_arg_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 189;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_15 = MAKE_ITERATOR( tmp_iter_arg_4 );
            Py_DECREF( tmp_iter_arg_4 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 189;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = generator_heap->tmp_for_loop_2__for_iterator;
                generator_heap->tmp_for_loop_2__for_iterator = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( generator_heap->tmp_for_loop_2__for_iterator );
            tmp_next_source_3 = generator_heap->tmp_for_loop_2__for_iterator;
            tmp_assign_source_16 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    generator_heap->type_description_1 = "ccooooooooooo";
                    generator_heap->exception_lineno = 189;
                    goto try_except_handler_7;
                }
            }

            {
                PyObject *old = generator_heap->tmp_for_loop_2__iter_value;
                generator_heap->tmp_for_loop_2__iter_value = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_17;
            CHECK_OBJECT( generator_heap->tmp_for_loop_2__iter_value );
            tmp_assign_source_17 = generator_heap->tmp_for_loop_2__iter_value;
            {
                PyObject *old = generator_heap->var_values;
                generator_heap->var_values = tmp_assign_source_17;
                Py_INCREF( generator_heap->var_values );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_expression_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_6;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            tmp_tuple_element_1 = Py_None;
            tmp_expression_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_get_merged_lazy_context );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_merged_lazy_context );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_expression_name_1 );
                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_merged_lazy_context" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 192;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_7;
            }

            tmp_called_name_2 = tmp_mvar_value_3;
            // Tried code:
            {
                PyObject *tmp_assign_source_18;
                PyObject *tmp_iter_arg_5;
                CHECK_OBJECT( generator_heap->var_values );
                tmp_iter_arg_5 = generator_heap->var_values;
                tmp_assign_source_18 = MAKE_ITERATOR( tmp_iter_arg_5 );
                if ( tmp_assign_source_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 193;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_8;
                }
                {
                    PyObject *old = generator_heap->tmp_listcomp_2__$0;
                    generator_heap->tmp_listcomp_2__$0 = tmp_assign_source_18;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_19;
                tmp_assign_source_19 = PyList_New( 0 );
                {
                    PyObject *old = generator_heap->tmp_listcomp_2__contraction;
                    generator_heap->tmp_listcomp_2__contraction = tmp_assign_source_19;
                    Py_XDECREF( old );
                }

            }
            MAKE_OR_REUSE_FRAME( cache_frame_68cfc9138ef07e15b5263a4d3603a93d_3, codeobj_68cfc9138ef07e15b5263a4d3603a93d, module_jedi$evaluate$arguments, sizeof(void *) );
            generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 = cache_frame_68cfc9138ef07e15b5263a4d3603a93d_3;

            // Push the new frame as the currently active one.
            pushFrameStack( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_4:;
            {
                PyObject *tmp_next_source_4;
                PyObject *tmp_assign_source_20;
                CHECK_OBJECT( generator_heap->tmp_listcomp_2__$0 );
                tmp_next_source_4 = generator_heap->tmp_listcomp_2__$0;
                tmp_assign_source_20 = ITERATOR_NEXT( tmp_next_source_4 );
                if ( tmp_assign_source_20 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_4;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_2 = "o";
                        generator_heap->exception_lineno = 193;
                        goto try_except_handler_9;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_listcomp_2__iter_value_0;
                    generator_heap->tmp_listcomp_2__iter_value_0 = tmp_assign_source_20;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_21;
                CHECK_OBJECT( generator_heap->tmp_listcomp_2__iter_value_0 );
                tmp_assign_source_21 = generator_heap->tmp_listcomp_2__iter_value_0;
                {
                    PyObject *old = generator_heap->outline_1_var_v;
                    generator_heap->outline_1_var_v = tmp_assign_source_21;
                    Py_INCREF( generator_heap->outline_1_var_v );
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_2;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                CHECK_OBJECT( generator_heap->outline_1_var_v );
                tmp_compexpr_left_2 = generator_heap->outline_1_var_v;
                tmp_compexpr_right_2 = Py_None;
                tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_2;
                }
                else
                {
                    goto branch_no_2;
                }
                branch_yes_2:;
                {
                    PyObject *tmp_append_list_2;
                    PyObject *tmp_append_value_2;
                    CHECK_OBJECT( generator_heap->tmp_listcomp_2__contraction );
                    tmp_append_list_2 = generator_heap->tmp_listcomp_2__contraction;
                    CHECK_OBJECT( generator_heap->outline_1_var_v );
                    tmp_append_value_2 = generator_heap->outline_1_var_v;
                    assert( PyList_Check( tmp_append_list_2 ) );
                    generator_heap->tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 193;
                        generator_heap->type_description_2 = "o";
                        goto try_except_handler_9;
                    }
                }
                branch_no_2:;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 193;
                generator_heap->type_description_2 = "o";
                goto try_except_handler_9;
            }
            goto loop_start_4;
            loop_end_4:;
            CHECK_OBJECT( generator_heap->tmp_listcomp_2__contraction );
            tmp_args_element_name_6 = generator_heap->tmp_listcomp_2__contraction;
            Py_INCREF( tmp_args_element_name_6 );
            goto try_return_handler_9;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack );
            return NULL;
            // Return handler code:
            try_return_handler_9:;
            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_2__$0 );
            Py_DECREF( generator_heap->tmp_listcomp_2__$0 );
            generator_heap->tmp_listcomp_2__$0 = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_2__contraction );
            Py_DECREF( generator_heap->tmp_listcomp_2__contraction );
            generator_heap->tmp_listcomp_2__contraction = NULL;

            Py_XDECREF( generator_heap->tmp_listcomp_2__iter_value_0 );
            generator_heap->tmp_listcomp_2__iter_value_0 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_9:;
            generator_heap->exception_keeper_type_5 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_5 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_5 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_5 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_2__$0 );
            Py_DECREF( generator_heap->tmp_listcomp_2__$0 );
            generator_heap->tmp_listcomp_2__$0 = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_2__contraction );
            Py_DECREF( generator_heap->tmp_listcomp_2__contraction );
            generator_heap->tmp_listcomp_2__contraction = NULL;

            Py_XDECREF( generator_heap->tmp_listcomp_2__iter_value_0 );
            generator_heap->tmp_listcomp_2__iter_value_0 = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_5;
            generator_heap->exception_value = generator_heap->exception_keeper_value_5;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_5;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_5;

            goto frame_exception_exit_3;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_2;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_8;

            frame_exception_exit_3:;

#if 0
            RESTORE_FRAME_EXCEPTION( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 );
#endif

            if ( generator_heap->exception_tb == NULL )
            {
                generator_heap->exception_tb = MAKE_TRACEBACK( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3, generator_heap->exception_lineno );
            }
            else if ( generator_heap->exception_tb->tb_frame != &generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3->m_frame )
            {
                generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3, generator_heap->exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3,
                generator_heap->type_description_2,
                generator_heap->outline_1_var_v
            );


            // Release cached frame.
            if ( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 == cache_frame_68cfc9138ef07e15b5263a4d3603a93d_3 )
            {
                Py_DECREF( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 );
            }
            cache_frame_68cfc9138ef07e15b5263a4d3603a93d_3 = NULL;

            assertFrameObject( generator_heap->frame_68cfc9138ef07e15b5263a4d3603a93d_3 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_2;

            frame_no_exception_2:;
            goto skip_nested_handling_2;
            nested_frame_exit_2:;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto try_except_handler_8;
            skip_nested_handling_2:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack );
            return NULL;
            // Return handler code:
            try_return_handler_8:;
            Py_XDECREF( generator_heap->outline_1_var_v );
            generator_heap->outline_1_var_v = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_8:;
            generator_heap->exception_keeper_type_6 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_6 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_6 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_6 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->outline_1_var_v );
            generator_heap->outline_1_var_v = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_6;
            generator_heap->exception_value = generator_heap->exception_keeper_value_6;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_6;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_6;

            goto outline_exception_2;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack );
            return NULL;
            outline_exception_2:;
            generator_heap->exception_lineno = 193;
            goto try_except_handler_7;
            outline_result_2:;
            generator->m_frame->m_frame.f_lineno = 192;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_expression_name_1 );

                generator_heap->exception_lineno = 192;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_7;
            }
            PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_3, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_3, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 192;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_7;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto try_except_handler_7;
        }
        goto loop_start_3;
        loop_end_3:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_7:;
        generator_heap->exception_keeper_type_7 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_7 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_7 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_7 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
        generator_heap->tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
        Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
        generator_heap->tmp_for_loop_2__for_iterator = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_7;
        generator_heap->exception_value = generator_heap->exception_keeper_value_7;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_7;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_7;

        goto try_except_handler_2;
        // End of try:
        try_end_3:;
        Py_XDECREF( generator_heap->tmp_for_loop_2__iter_value );
        generator_heap->tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_2__for_iterator );
        Py_DECREF( generator_heap->tmp_for_loop_2__for_iterator );
        generator_heap->tmp_for_loop_2__for_iterator = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( generator_heap->var_star_count );
            tmp_compexpr_left_3 = generator_heap->var_star_count;
            tmp_compexpr_right_3 = const_int_pos_2;
            generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 195;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_3 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_22;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_source_name_3;
                PyObject *tmp_args_element_name_7;
                if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 196;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_source_name_3 = PyCell_GET( generator->m_closure[1] );
                tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_context );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 196;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( generator_heap->var_el );
                tmp_args_element_name_7 = generator_heap->var_el;
                generator->m_frame->m_frame.f_lineno = 196;
                {
                    PyObject *call_args[] = { tmp_args_element_name_7 };
                    tmp_assign_source_22 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_eval_node, call_args );
                }

                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_assign_source_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 196;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = generator_heap->var_arrays;
                    generator_heap->var_arrays = tmp_assign_source_22;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_23;
                PyObject *tmp_iter_arg_6;
                CHECK_OBJECT( generator_heap->var_arrays );
                tmp_iter_arg_6 = generator_heap->var_arrays;
                tmp_assign_source_23 = MAKE_ITERATOR( tmp_iter_arg_6 );
                if ( tmp_assign_source_23 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 197;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = generator_heap->tmp_for_loop_3__for_iterator;
                    generator_heap->tmp_for_loop_3__for_iterator = tmp_assign_source_23;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_5:;
            {
                PyObject *tmp_next_source_5;
                PyObject *tmp_assign_source_24;
                CHECK_OBJECT( generator_heap->tmp_for_loop_3__for_iterator );
                tmp_next_source_5 = generator_heap->tmp_for_loop_3__for_iterator;
                tmp_assign_source_24 = ITERATOR_NEXT( tmp_next_source_5 );
                if ( tmp_assign_source_24 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_5;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "ccooooooooooo";
                        generator_heap->exception_lineno = 197;
                        goto try_except_handler_10;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_for_loop_3__iter_value;
                    generator_heap->tmp_for_loop_3__iter_value = tmp_assign_source_24;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_25;
                CHECK_OBJECT( generator_heap->tmp_for_loop_3__iter_value );
                tmp_assign_source_25 = generator_heap->tmp_for_loop_3__iter_value;
                {
                    PyObject *old = generator_heap->var_dct;
                    generator_heap->var_dct = tmp_assign_source_25;
                    Py_INCREF( generator_heap->var_dct );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_26;
                PyObject *tmp_iter_arg_7;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_8;
                PyObject *tmp_source_name_4;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_args_element_name_10;
                PyObject *tmp_args_element_name_11;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__star_star_dict );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__star_star_dict );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_star_star_dict" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_10;
                }

                tmp_called_name_3 = tmp_mvar_value_4;
                if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_10;
                }

                tmp_source_name_4 = PyCell_GET( generator->m_closure[1] );
                tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_context );
                if ( tmp_args_element_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_10;
                }
                CHECK_OBJECT( generator_heap->var_dct );
                tmp_args_element_name_9 = generator_heap->var_dct;
                CHECK_OBJECT( generator_heap->var_el );
                tmp_args_element_name_10 = generator_heap->var_el;
                if ( PyCell_GET( generator->m_closure[0] ) == NULL )
                {
                    Py_DECREF( tmp_args_element_name_8 );
                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "funcdef" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_10;
                }

                tmp_args_element_name_11 = PyCell_GET( generator->m_closure[0] );
                generator->m_frame->m_frame.f_lineno = 198;
                {
                    PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11 };
                    tmp_iter_arg_7 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_args_element_name_8 );
                if ( tmp_iter_arg_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_10;
                }
                tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_7 );
                Py_DECREF( tmp_iter_arg_7 );
                if ( tmp_assign_source_26 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_10;
                }
                {
                    PyObject *old = generator_heap->tmp_for_loop_4__for_iterator;
                    generator_heap->tmp_for_loop_4__for_iterator = tmp_assign_source_26;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_6:;
            {
                PyObject *tmp_next_source_6;
                PyObject *tmp_assign_source_27;
                CHECK_OBJECT( generator_heap->tmp_for_loop_4__for_iterator );
                tmp_next_source_6 = generator_heap->tmp_for_loop_4__for_iterator;
                tmp_assign_source_27 = ITERATOR_NEXT( tmp_next_source_6 );
                if ( tmp_assign_source_27 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_6;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        generator_heap->type_description_1 = "ccooooooooooo";
                        generator_heap->exception_lineno = 198;
                        goto try_except_handler_11;
                    }
                }

                {
                    PyObject *old = generator_heap->tmp_for_loop_4__iter_value;
                    generator_heap->tmp_for_loop_4__iter_value = tmp_assign_source_27;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_28;
                PyObject *tmp_iter_arg_8;
                CHECK_OBJECT( generator_heap->tmp_for_loop_4__iter_value );
                tmp_iter_arg_8 = generator_heap->tmp_for_loop_4__iter_value;
                tmp_assign_source_28 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_8 );
                if ( tmp_assign_source_28 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 198;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = generator_heap->tmp_tuple_unpack_2__source_iter;
                    generator_heap->tmp_tuple_unpack_2__source_iter = tmp_assign_source_28;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_29;
                PyObject *tmp_unpack_3;
                CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
                tmp_unpack_3 = generator_heap->tmp_tuple_unpack_2__source_iter;
                tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                if ( tmp_assign_source_29 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        generator_heap->exception_type = PyExc_StopIteration;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = NULL;
                        generator_heap->exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    }


                    generator_heap->type_description_1 = "ccooooooooooo";
                    generator_heap->exception_lineno = 198;
                    goto try_except_handler_13;
                }
                {
                    PyObject *old = generator_heap->tmp_tuple_unpack_2__element_1;
                    generator_heap->tmp_tuple_unpack_2__element_1 = tmp_assign_source_29;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_30;
                PyObject *tmp_unpack_4;
                CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
                tmp_unpack_4 = generator_heap->tmp_tuple_unpack_2__source_iter;
                tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                if ( tmp_assign_source_30 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        generator_heap->exception_type = PyExc_StopIteration;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = NULL;
                        generator_heap->exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    }


                    generator_heap->type_description_1 = "ccooooooooooo";
                    generator_heap->exception_lineno = 198;
                    goto try_except_handler_13;
                }
                {
                    PyObject *old = generator_heap->tmp_tuple_unpack_2__element_2;
                    generator_heap->tmp_tuple_unpack_2__element_2 = tmp_assign_source_30;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_2;
                CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
                tmp_iterator_name_2 = generator_heap->tmp_tuple_unpack_2__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

                generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

                if (likely( generator_heap->tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                            generator_heap->type_description_1 = "ccooooooooooo";
                            generator_heap->exception_lineno = 198;
                            goto try_except_handler_13;
                        }
                    }
                }
                else
                {
                    Py_DECREF( generator_heap->tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "ccooooooooooo";
                    generator_heap->exception_lineno = 198;
                    goto try_except_handler_13;
                }
            }
            goto try_end_4;
            // Exception handler code:
            try_except_handler_13:;
            generator_heap->exception_keeper_type_8 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_8 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_8 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_8 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_2__source_iter );
            Py_DECREF( generator_heap->tmp_tuple_unpack_2__source_iter );
            generator_heap->tmp_tuple_unpack_2__source_iter = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_8;
            generator_heap->exception_value = generator_heap->exception_keeper_value_8;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_8;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_8;

            goto try_except_handler_12;
            // End of try:
            try_end_4:;
            goto try_end_5;
            // Exception handler code:
            try_except_handler_12:;
            generator_heap->exception_keeper_type_9 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_9 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_9 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_9 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_1 );
            generator_heap->tmp_tuple_unpack_2__element_1 = NULL;

            Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_2 );
            generator_heap->tmp_tuple_unpack_2__element_2 = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_9;
            generator_heap->exception_value = generator_heap->exception_keeper_value_9;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_9;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_9;

            goto try_except_handler_11;
            // End of try:
            try_end_5:;
            CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_2__source_iter );
            Py_DECREF( generator_heap->tmp_tuple_unpack_2__source_iter );
            generator_heap->tmp_tuple_unpack_2__source_iter = NULL;

            {
                PyObject *tmp_assign_source_31;
                CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_1 );
                tmp_assign_source_31 = generator_heap->tmp_tuple_unpack_2__element_1;
                {
                    PyObject *old = generator_heap->var_key;
                    generator_heap->var_key = tmp_assign_source_31;
                    Py_INCREF( generator_heap->var_key );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_1 );
            generator_heap->tmp_tuple_unpack_2__element_1 = NULL;

            {
                PyObject *tmp_assign_source_32;
                CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_2 );
                tmp_assign_source_32 = generator_heap->tmp_tuple_unpack_2__element_2;
                {
                    PyObject *old = generator_heap->var_values;
                    generator_heap->var_values = tmp_assign_source_32;
                    Py_INCREF( generator_heap->var_values );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_2 );
            generator_heap->tmp_tuple_unpack_2__element_2 = NULL;

            {
                PyObject *tmp_expression_name_2;
                PyObject *tmp_tuple_element_2;
                NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
                CHECK_OBJECT( generator_heap->var_key );
                tmp_tuple_element_2 = generator_heap->var_key;
                tmp_expression_name_2 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_expression_name_2, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( generator_heap->var_values );
                tmp_tuple_element_2 = generator_heap->var_values;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_expression_name_2, 1, tmp_tuple_element_2 );
                Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_tuple_element_2, sizeof(PyObject *), NULL );
                generator->m_yield_return_index = 2;
                return tmp_expression_name_2;
                yield_return_2:
                Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_tuple_element_2, sizeof(PyObject *), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 199;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_11;
                }
                tmp_yield_result_2 = yield_return_value;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 198;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_11;
            }
            goto loop_start_6;
            loop_end_6:;
            goto try_end_6;
            // Exception handler code:
            try_except_handler_11:;
            generator_heap->exception_keeper_type_10 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_10 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_10 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_10 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_for_loop_4__iter_value );
            generator_heap->tmp_for_loop_4__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_4__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_4__for_iterator );
            generator_heap->tmp_for_loop_4__for_iterator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_10;
            generator_heap->exception_value = generator_heap->exception_keeper_value_10;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_10;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_10;

            goto try_except_handler_10;
            // End of try:
            try_end_6:;
            Py_XDECREF( generator_heap->tmp_for_loop_4__iter_value );
            generator_heap->tmp_for_loop_4__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_4__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_4__for_iterator );
            generator_heap->tmp_for_loop_4__for_iterator = NULL;

            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 197;
                generator_heap->type_description_1 = "ccooooooooooo";
                goto try_except_handler_10;
            }
            goto loop_start_5;
            loop_end_5:;
            goto try_end_7;
            // Exception handler code:
            try_except_handler_10:;
            generator_heap->exception_keeper_type_11 = generator_heap->exception_type;
            generator_heap->exception_keeper_value_11 = generator_heap->exception_value;
            generator_heap->exception_keeper_tb_11 = generator_heap->exception_tb;
            generator_heap->exception_keeper_lineno_11 = generator_heap->exception_lineno;
            generator_heap->exception_type = NULL;
            generator_heap->exception_value = NULL;
            generator_heap->exception_tb = NULL;
            generator_heap->exception_lineno = 0;

            Py_XDECREF( generator_heap->tmp_for_loop_3__iter_value );
            generator_heap->tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_3__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_3__for_iterator );
            generator_heap->tmp_for_loop_3__for_iterator = NULL;

            // Re-raise.
            generator_heap->exception_type = generator_heap->exception_keeper_type_11;
            generator_heap->exception_value = generator_heap->exception_keeper_value_11;
            generator_heap->exception_tb = generator_heap->exception_keeper_tb_11;
            generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_11;

            goto try_except_handler_2;
            // End of try:
            try_end_7:;
            Py_XDECREF( generator_heap->tmp_for_loop_3__iter_value );
            generator_heap->tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_3__for_iterator );
            Py_DECREF( generator_heap->tmp_for_loop_3__for_iterator );
            generator_heap->tmp_for_loop_3__for_iterator = NULL;

            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_source_name_5;
                CHECK_OBJECT( generator_heap->var_el );
                tmp_source_name_5 = generator_heap->var_el;
                tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_type );
                if ( tmp_compexpr_left_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 201;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_compexpr_right_4 = const_str_plain_argument;
                generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                Py_DECREF( tmp_compexpr_left_4 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 201;
                    generator_heap->type_description_1 = "ccooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_33;
                    PyObject *tmp_source_name_6;
                    CHECK_OBJECT( generator_heap->var_el );
                    tmp_source_name_6 = generator_heap->var_el;
                    tmp_assign_source_33 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_children );
                    if ( tmp_assign_source_33 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 202;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = generator_heap->var_c;
                        generator_heap->var_c = tmp_assign_source_33;
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    PyObject *tmp_len_arg_1;
                    CHECK_OBJECT( generator_heap->var_c );
                    tmp_len_arg_1 = generator_heap->var_c;
                    tmp_compexpr_left_5 = BUILTIN_LEN( tmp_len_arg_1 );
                    if ( tmp_compexpr_left_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 203;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_compexpr_right_5 = const_int_pos_3;
                    generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    Py_DECREF( tmp_compexpr_left_5 );
                    assert( !(generator_heap->tmp_res == -1) );
                    tmp_condition_result_5 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_source_name_7;
                        PyObject *tmp_call_result_1;
                        PyObject *tmp_args_element_name_12;
                        PyObject *tmp_tuple_element_3;
                        PyObject *tmp_source_name_8;
                        PyObject *tmp_subscribed_name_1;
                        PyObject *tmp_subscript_name_1;
                        PyObject *tmp_called_name_5;
                        PyObject *tmp_mvar_value_5;
                        PyObject *tmp_args_element_name_13;
                        PyObject *tmp_source_name_9;
                        PyObject *tmp_args_element_name_14;
                        PyObject *tmp_subscribed_name_2;
                        PyObject *tmp_subscript_name_2;
                        CHECK_OBJECT( generator_heap->var_named_args );
                        tmp_source_name_7 = generator_heap->var_named_args;
                        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
                        if ( tmp_called_name_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        CHECK_OBJECT( generator_heap->var_c );
                        tmp_subscribed_name_1 = generator_heap->var_c;
                        tmp_subscript_name_1 = const_int_0;
                        tmp_source_name_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                        if ( tmp_source_name_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_4 );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_value );
                        Py_DECREF( tmp_source_name_8 );
                        if ( tmp_tuple_element_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_4 );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_args_element_name_12 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_args_element_name_12, 0, tmp_tuple_element_3 );
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

                        if (unlikely( tmp_mvar_value_5 == NULL ))
                        {
                            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
                        }

                        if ( tmp_mvar_value_5 == NULL )
                        {
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_12 );
                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_called_name_5 = tmp_mvar_value_5;
                        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                        {
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_12 );
                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_source_name_9 = PyCell_GET( generator->m_closure[1] );
                        tmp_args_element_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_context );
                        if ( tmp_args_element_name_13 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_12 );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        CHECK_OBJECT( generator_heap->var_c );
                        tmp_subscribed_name_2 = generator_heap->var_c;
                        tmp_subscript_name_2 = const_int_pos_2;
                        tmp_args_element_name_14 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 2 );
                        if ( tmp_args_element_name_14 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_12 );
                            Py_DECREF( tmp_args_element_name_13 );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        generator->m_frame->m_frame.f_lineno = 204;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14 };
                            tmp_tuple_element_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_13 );
                        Py_DECREF( tmp_args_element_name_14 );
                        if ( tmp_tuple_element_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_4 );
                            Py_DECREF( tmp_args_element_name_12 );

                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        PyTuple_SET_ITEM( tmp_args_element_name_12, 1, tmp_tuple_element_3 );
                        generator->m_frame->m_frame.f_lineno = 204;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_12 };
                            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                        }

                        Py_DECREF( tmp_called_name_4 );
                        Py_DECREF( tmp_args_element_name_12 );
                        if ( tmp_call_result_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 204;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        Py_DECREF( tmp_call_result_1 );
                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        PyObject *tmp_assign_source_34;
                        PyObject *tmp_called_name_6;
                        PyObject *tmp_source_name_10;
                        PyObject *tmp_mvar_value_6;
                        PyObject *tmp_args_element_name_15;
                        PyObject *tmp_source_name_11;
                        PyObject *tmp_args_element_name_16;
                        PyObject *tmp_source_name_12;
                        PyObject *tmp_args_element_name_17;
                        PyObject *tmp_source_name_13;
                        PyObject *tmp_source_name_14;
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_iterable );

                        if (unlikely( tmp_mvar_value_6 == NULL ))
                        {
                            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
                        }

                        if ( tmp_mvar_value_6 == NULL )
                        {

                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 207;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_source_name_10 = tmp_mvar_value_6;
                        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_GeneratorComprehension );
                        if ( tmp_called_name_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 207;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                        {
                            Py_DECREF( tmp_called_name_6 );
                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_source_name_11 = PyCell_GET( generator->m_closure[1] );
                        tmp_args_element_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__evaluator );
                        if ( tmp_args_element_name_15 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_6 );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                        {
                            Py_DECREF( tmp_called_name_6 );
                            Py_DECREF( tmp_args_element_name_15 );
                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_source_name_12 = PyCell_GET( generator->m_closure[1] );
                        tmp_args_element_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_context );
                        if ( tmp_args_element_name_16 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_6 );
                            Py_DECREF( tmp_args_element_name_15 );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                        {
                            Py_DECREF( tmp_called_name_6 );
                            Py_DECREF( tmp_args_element_name_15 );
                            Py_DECREF( tmp_args_element_name_16 );
                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_source_name_14 = PyCell_GET( generator->m_closure[1] );
                        tmp_source_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_argument_node );
                        if ( tmp_source_name_13 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_6 );
                            Py_DECREF( tmp_args_element_name_15 );
                            Py_DECREF( tmp_args_element_name_16 );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_args_element_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_parent );
                        Py_DECREF( tmp_source_name_13 );
                        if ( tmp_args_element_name_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_called_name_6 );
                            Py_DECREF( tmp_args_element_name_15 );
                            Py_DECREF( tmp_args_element_name_16 );

                            generator_heap->exception_lineno = 208;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        generator->m_frame->m_frame.f_lineno = 207;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16, tmp_args_element_name_17 };
                            tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, call_args );
                        }

                        Py_DECREF( tmp_called_name_6 );
                        Py_DECREF( tmp_args_element_name_15 );
                        Py_DECREF( tmp_args_element_name_16 );
                        Py_DECREF( tmp_args_element_name_17 );
                        if ( tmp_assign_source_34 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 207;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        {
                            PyObject *old = generator_heap->var_comp;
                            generator_heap->var_comp = tmp_assign_source_34;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_expression_name_3;
                        PyObject *tmp_tuple_element_4;
                        PyObject *tmp_called_name_7;
                        PyObject *tmp_mvar_value_7;
                        PyObject *tmp_args_element_name_18;
                        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_3;
                        tmp_tuple_element_4 = Py_None;
                        tmp_expression_name_3 = PyTuple_New( 2 );
                        Py_INCREF( tmp_tuple_element_4 );
                        PyTuple_SET_ITEM( tmp_expression_name_3, 0, tmp_tuple_element_4 );
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );

                        if (unlikely( tmp_mvar_value_7 == NULL ))
                        {
                            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );
                        }

                        if ( tmp_mvar_value_7 == NULL )
                        {
                            Py_DECREF( tmp_expression_name_3 );
                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyKnownContext" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 209;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_called_name_7 = tmp_mvar_value_7;
                        CHECK_OBJECT( generator_heap->var_comp );
                        tmp_args_element_name_18 = generator_heap->var_comp;
                        generator->m_frame->m_frame.f_lineno = 209;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_18 };
                            tmp_tuple_element_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
                        }

                        if ( tmp_tuple_element_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            Py_DECREF( tmp_expression_name_3 );

                            generator_heap->exception_lineno = 209;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        PyTuple_SET_ITEM( tmp_expression_name_3, 1, tmp_tuple_element_4 );
                        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_tuple_element_4, sizeof(PyObject *), &tmp_called_name_7, sizeof(PyObject *), &tmp_mvar_value_7, sizeof(PyObject *), &tmp_args_element_name_18, sizeof(PyObject *), NULL );
                        generator->m_yield_return_index = 3;
                        return tmp_expression_name_3;
                        yield_return_3:
                        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_condition_result_5, sizeof(nuitka_bool), &tmp_compexpr_left_5, sizeof(PyObject *), &tmp_compexpr_right_5, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_tuple_element_4, sizeof(PyObject *), &tmp_called_name_7, sizeof(PyObject *), &tmp_mvar_value_7, sizeof(PyObject *), &tmp_args_element_name_18, sizeof(PyObject *), NULL );
                        if ( yield_return_value == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 209;
                            generator_heap->type_description_1 = "ccooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_yield_result_3 = yield_return_value;
                    }
                    branch_end_5:;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    PyObject *tmp_expression_name_4;
                    PyObject *tmp_tuple_element_5;
                    PyObject *tmp_called_name_8;
                    PyObject *tmp_mvar_value_8;
                    PyObject *tmp_args_element_name_19;
                    PyObject *tmp_source_name_15;
                    PyObject *tmp_args_element_name_20;
                    NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_4;
                    tmp_tuple_element_5 = Py_None;
                    tmp_expression_name_4 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_5 );
                    PyTuple_SET_ITEM( tmp_expression_name_4, 0, tmp_tuple_element_5 );
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

                    if (unlikely( tmp_mvar_value_8 == NULL ))
                    {
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
                    }

                    if ( tmp_mvar_value_8 == NULL )
                    {
                        Py_DECREF( tmp_expression_name_4 );
                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 211;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_called_name_8 = tmp_mvar_value_8;
                    if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                    {
                        Py_DECREF( tmp_expression_name_4 );
                        generator_heap->exception_type = PyExc_NameError;
                        Py_INCREF( generator_heap->exception_type );
                        generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                        generator_heap->exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        CHAIN_EXCEPTION( generator_heap->exception_value );

                        generator_heap->exception_lineno = 211;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_source_name_15 = PyCell_GET( generator->m_closure[1] );
                    tmp_args_element_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_context );
                    if ( tmp_args_element_name_19 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        Py_DECREF( tmp_expression_name_4 );

                        generator_heap->exception_lineno = 211;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( generator_heap->var_el );
                    tmp_args_element_name_20 = generator_heap->var_el;
                    generator->m_frame->m_frame.f_lineno = 211;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_20 };
                        tmp_tuple_element_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_19 );
                    if ( tmp_tuple_element_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                        Py_DECREF( tmp_expression_name_4 );

                        generator_heap->exception_lineno = 211;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }
                    PyTuple_SET_ITEM( tmp_expression_name_4, 1, tmp_tuple_element_5 );
                    Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_tuple_element_5, sizeof(PyObject *), &tmp_called_name_8, sizeof(PyObject *), &tmp_mvar_value_8, sizeof(PyObject *), &tmp_args_element_name_19, sizeof(PyObject *), &tmp_source_name_15, sizeof(PyObject *), &tmp_args_element_name_20, sizeof(PyObject *), NULL );
                    generator->m_yield_return_index = 4;
                    return tmp_expression_name_4;
                    yield_return_4:
                    Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_tuple_element_5, sizeof(PyObject *), &tmp_called_name_8, sizeof(PyObject *), &tmp_mvar_value_8, sizeof(PyObject *), &tmp_args_element_name_19, sizeof(PyObject *), &tmp_source_name_15, sizeof(PyObject *), &tmp_args_element_name_20, sizeof(PyObject *), NULL );
                    if ( yield_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 211;
                        generator_heap->type_description_1 = "ccooooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_yield_result_4 = yield_return_value;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 184;
        generator_heap->type_description_1 = "ccooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_12 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_12 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_12 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_12 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_12;
    generator_heap->exception_value = generator_heap->exception_keeper_value_12;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_12;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_12;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_iter_arg_9;
        CHECK_OBJECT( generator_heap->var_named_args );
        tmp_iter_arg_9 = generator_heap->var_named_args;
        tmp_assign_source_35 = MAKE_ITERATOR( tmp_iter_arg_9 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 215;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_5__for_iterator == NULL );
        generator_heap->tmp_for_loop_5__for_iterator = tmp_assign_source_35;
    }
    // Tried code:
    loop_start_7:;
    {
        PyObject *tmp_next_source_7;
        PyObject *tmp_assign_source_36;
        CHECK_OBJECT( generator_heap->tmp_for_loop_5__for_iterator );
        tmp_next_source_7 = generator_heap->tmp_for_loop_5__for_iterator;
        tmp_assign_source_36 = ITERATOR_NEXT( tmp_next_source_7 );
        if ( tmp_assign_source_36 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_7;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccooooooooooo";
                generator_heap->exception_lineno = 215;
                goto try_except_handler_14;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_5__iter_value;
            generator_heap->tmp_for_loop_5__iter_value = tmp_assign_source_36;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_37;
        CHECK_OBJECT( generator_heap->tmp_for_loop_5__iter_value );
        tmp_assign_source_37 = generator_heap->tmp_for_loop_5__iter_value;
        {
            PyObject *old = generator_heap->var_named_arg;
            generator_heap->var_named_arg = tmp_assign_source_37;
            Py_INCREF( generator_heap->var_named_arg );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_5;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_5;
        CHECK_OBJECT( generator_heap->var_named_arg );
        tmp_expression_name_5 = generator_heap->var_named_arg;
        Py_INCREF( tmp_expression_name_5 );
        generator->m_yield_return_index = 5;
        return tmp_expression_name_5;
        yield_return_5:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 216;
            generator_heap->type_description_1 = "ccooooooooooo";
            goto try_except_handler_14;
        }
        tmp_yield_result_5 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 215;
        generator_heap->type_description_1 = "ccooooooooooo";
        goto try_except_handler_14;
    }
    goto loop_start_7;
    loop_end_7:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_14:;
    generator_heap->exception_keeper_type_13 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_13 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_13 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_13 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_5__iter_value );
    generator_heap->tmp_for_loop_5__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_5__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_5__for_iterator );
    generator_heap->tmp_for_loop_5__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_13;
    generator_heap->exception_value = generator_heap->exception_keeper_value_13;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_13;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_13;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_3;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[1],
            generator->m_closure[0],
            generator_heap->var_named_args,
            generator_heap->var_star_count,
            generator_heap->var_el,
            generator_heap->var_arrays,
            generator_heap->var_iterators,
            generator_heap->var_values,
            generator_heap->var_dct,
            generator_heap->var_key,
            generator_heap->var_c,
            generator_heap->var_comp,
            generator_heap->var_named_arg
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    goto try_end_10;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_14 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_14 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_14 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_14 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->var_named_args );
    Py_DECREF( generator_heap->var_named_args );
    generator_heap->var_named_args = NULL;

    Py_XDECREF( generator_heap->var_star_count );
    generator_heap->var_star_count = NULL;

    Py_XDECREF( generator_heap->var_el );
    generator_heap->var_el = NULL;

    Py_XDECREF( generator_heap->var_arrays );
    generator_heap->var_arrays = NULL;

    Py_XDECREF( generator_heap->var_iterators );
    generator_heap->var_iterators = NULL;

    Py_XDECREF( generator_heap->var_values );
    generator_heap->var_values = NULL;

    Py_XDECREF( generator_heap->var_dct );
    generator_heap->var_dct = NULL;

    Py_XDECREF( generator_heap->var_key );
    generator_heap->var_key = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    Py_XDECREF( generator_heap->var_comp );
    generator_heap->var_comp = NULL;

    Py_XDECREF( generator_heap->var_named_arg );
    generator_heap->var_named_arg = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_14;
    generator_heap->exception_value = generator_heap->exception_keeper_value_14;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_14;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_14;

    goto function_exception_exit;
    // End of try:
    try_end_10:;
    Py_XDECREF( generator_heap->tmp_for_loop_5__iter_value );
    generator_heap->tmp_for_loop_5__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_5__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_5__for_iterator );
    generator_heap->tmp_for_loop_5__for_iterator = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_named_args );
    Py_DECREF( generator_heap->var_named_args );
    generator_heap->var_named_args = NULL;

    Py_XDECREF( generator_heap->var_star_count );
    generator_heap->var_star_count = NULL;

    Py_XDECREF( generator_heap->var_el );
    generator_heap->var_el = NULL;

    Py_XDECREF( generator_heap->var_arrays );
    generator_heap->var_arrays = NULL;

    Py_XDECREF( generator_heap->var_iterators );
    generator_heap->var_iterators = NULL;

    Py_XDECREF( generator_heap->var_values );
    generator_heap->var_values = NULL;

    Py_XDECREF( generator_heap->var_dct );
    generator_heap->var_dct = NULL;

    Py_XDECREF( generator_heap->var_key );
    generator_heap->var_key = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    Py_XDECREF( generator_heap->var_comp );
    generator_heap->var_comp = NULL;

    Py_XDECREF( generator_heap->var_named_arg );
    generator_heap->var_named_arg = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_context,
        module_jedi$evaluate$arguments,
        const_str_plain_unpack,
#if PYTHON_VERSION >= 350
        const_str_digest_26ad4ced4c4df3f616989585528cdeb7,
#endif
        codeobj_c84bee060b6acd8351ea26c2a9cbc92d,
        2,
        sizeof(struct jedi$evaluate$arguments$$$function_13_unpack$$$genobj_1_unpack_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_self;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_locals {
    PyObject *var_star_count;
    PyObject *var_argument;
    PyObject *var_default;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    PyObject *tmp_tuple_unpack_2__element_1;
    PyObject *tmp_tuple_unpack_2__element_2;
    PyObject *tmp_tuple_unpack_2__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    int exception_keeper_lineno_6;
};

static PyObject *jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_star_count = NULL;
    generator_heap->var_argument = NULL;
    generator_heap->var_default = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->tmp_tuple_unpack_2__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_2__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_2__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_d292d3f28731963bec78e997a2fdb614, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 219;
            generator_heap->type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 219;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 219;
            generator_heap->type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 219;
            generator_heap->type_description_1 = "cooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "cooo";
                generator_heap->exception_lineno = 219;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = generator_heap->tmp_for_loop_1__iter_value;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 219;
            generator_heap->type_description_1 = "cooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "cooo";
            generator_heap->exception_lineno = 219;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "cooo";
            generator_heap->exception_lineno = 219;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "cooo";
                    generator_heap->exception_lineno = 219;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "cooo";
            generator_heap->exception_lineno = 219;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_star_count;
            generator_heap->var_star_count = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_star_count );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_argument;
            generator_heap->var_argument = tmp_assign_source_7;
            Py_INCREF( generator_heap->var_argument );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( generator_heap->var_argument );
        tmp_source_name_1 = generator_heap->var_argument;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "cooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = const_str_plain_argument;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "cooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( generator_heap->var_argument );
            tmp_source_name_2 = generator_heap->var_argument;
            tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_children );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 221;
                generator_heap->type_description_1 = "cooo";
                goto try_except_handler_5;
            }
            tmp_subscript_name_1 = const_slice_none_none_int_pos_2;
            tmp_iter_arg_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 221;
                generator_heap->type_description_1 = "cooo";
                goto try_except_handler_5;
            }
            tmp_assign_source_8 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 221;
                generator_heap->type_description_1 = "cooo";
                goto try_except_handler_5;
            }
            {
                PyObject *old = generator_heap->tmp_tuple_unpack_2__source_iter;
                generator_heap->tmp_tuple_unpack_2__source_iter = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = generator_heap->tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
            if ( tmp_assign_source_9 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    generator_heap->exception_type = PyExc_StopIteration;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = NULL;
                    generator_heap->exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                }


                generator_heap->type_description_1 = "cooo";
                generator_heap->exception_lineno = 221;
                goto try_except_handler_6;
            }
            {
                PyObject *old = generator_heap->tmp_tuple_unpack_2__element_1;
                generator_heap->tmp_tuple_unpack_2__element_1 = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = generator_heap->tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_10 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
            if ( tmp_assign_source_10 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    generator_heap->exception_type = PyExc_StopIteration;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = NULL;
                    generator_heap->exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                }


                generator_heap->type_description_1 = "cooo";
                generator_heap->exception_lineno = 221;
                goto try_except_handler_6;
            }
            {
                PyObject *old = generator_heap->tmp_tuple_unpack_2__element_2;
                generator_heap->tmp_tuple_unpack_2__element_2 = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = generator_heap->tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( generator_heap->tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                        generator_heap->type_description_1 = "cooo";
                        generator_heap->exception_lineno = 221;
                        goto try_except_handler_6;
                    }
                }
            }
            else
            {
                Py_DECREF( generator_heap->tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                generator_heap->type_description_1 = "cooo";
                generator_heap->exception_lineno = 221;
                goto try_except_handler_6;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_6:;
        generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_2__source_iter );
        Py_DECREF( generator_heap->tmp_tuple_unpack_2__source_iter );
        generator_heap->tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_3;
        generator_heap->exception_value = generator_heap->exception_keeper_value_3;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

        goto try_except_handler_5;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_5:;
        generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_1 );
        generator_heap->tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_2 );
        generator_heap->tmp_tuple_unpack_2__element_2 = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_4;
        generator_heap->exception_value = generator_heap->exception_keeper_value_4;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

        goto try_except_handler_2;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_2__source_iter );
        Py_DECREF( generator_heap->tmp_tuple_unpack_2__source_iter );
        generator_heap->tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_11;
            CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_11 = generator_heap->tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = generator_heap->var_argument;
                assert( old != NULL );
                generator_heap->var_argument = tmp_assign_source_11;
                Py_INCREF( generator_heap->var_argument );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_1 );
        generator_heap->tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_12;
            CHECK_OBJECT( generator_heap->tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_12 = generator_heap->tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = generator_heap->var_default;
                generator_heap->var_default = tmp_assign_source_12;
                Py_INCREF( generator_heap->var_default );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( generator_heap->tmp_tuple_unpack_2__element_2 );
        generator_heap->tmp_tuple_unpack_2__element_2 = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = Py_None;
            {
                PyObject *old = generator_heap->var_default;
                generator_heap->var_default = tmp_assign_source_13;
                Py_INCREF( generator_heap->var_default );
                Py_XDECREF( old );
            }

        }
        branch_end_1:;
    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_argument );
        tmp_tuple_element_1 = generator_heap->var_argument;
        tmp_expression_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_default );
        tmp_tuple_element_1 = generator_heap->var_default;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_star_count );
        tmp_tuple_element_1 = generator_heap->var_star_count;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 2, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 224;
            generator_heap->type_description_1 = "cooo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 219;
        generator_heap->type_description_1 = "cooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_5 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_5 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_5 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_5 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_5;
    generator_heap->exception_value = generator_heap->exception_keeper_value_5;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_5;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator_heap->var_star_count,
            generator_heap->var_argument,
            generator_heap->var_default
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_6 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_6 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_6 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_6 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_star_count );
    generator_heap->var_star_count = NULL;

    Py_XDECREF( generator_heap->var_argument );
    generator_heap->var_argument = NULL;

    Py_XDECREF( generator_heap->var_default );
    generator_heap->var_default = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_6;
    generator_heap->exception_value = generator_heap->exception_keeper_value_6;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_6;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:
    try_end_6:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    Py_XDECREF( generator_heap->var_star_count );
    generator_heap->var_star_count = NULL;

    Py_XDECREF( generator_heap->var_argument );
    generator_heap->var_argument = NULL;

    Py_XDECREF( generator_heap->var_default );
    generator_heap->var_default = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_context,
        module_jedi$evaluate$arguments,
        const_str_plain_as_tree_tuple_objects,
#if PYTHON_VERSION >= 350
        const_str_digest_1a4d1d0ac4461ae617038f2c10257b40,
#endif
        codeobj_d292d3f28731963bec78e997a2fdb614,
        1,
        sizeof(struct jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects$$$genobj_1_as_tree_tuple_objects_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_15___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_eb74dbad402bcef7e4fb4a91dce58de7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_eb74dbad402bcef7e4fb4a91dce58de7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eb74dbad402bcef7e4fb4a91dce58de7, codeobj_eb74dbad402bcef7e4fb4a91dce58de7, module_jedi$evaluate$arguments, sizeof(void *) );
    frame_eb74dbad402bcef7e4fb4a91dce58de7 = cache_frame_eb74dbad402bcef7e4fb4a91dce58de7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb74dbad402bcef7e4fb4a91dce58de7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb74dbad402bcef7e4fb4a91dce58de7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        tmp_left_name_1 = const_str_digest_f088166ebe9aa8754d7a4327a52e54aa;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_argument_node );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 227;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb74dbad402bcef7e4fb4a91dce58de7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb74dbad402bcef7e4fb4a91dce58de7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb74dbad402bcef7e4fb4a91dce58de7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb74dbad402bcef7e4fb4a91dce58de7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb74dbad402bcef7e4fb4a91dce58de7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb74dbad402bcef7e4fb4a91dce58de7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb74dbad402bcef7e4fb4a91dce58de7,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_eb74dbad402bcef7e4fb4a91dce58de7 == cache_frame_eb74dbad402bcef7e4fb4a91dce58de7 )
    {
        Py_DECREF( frame_eb74dbad402bcef7e4fb4a91dce58de7 );
    }
    cache_frame_eb74dbad402bcef7e4fb4a91dce58de7 = NULL;

    assertFrameObject( frame_eb74dbad402bcef7e4fb4a91dce58de7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_15___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_15___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_16_get_calling_nodes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_DynamicExecutedParams = NULL;
    PyObject *var_old_arguments_list = NULL;
    PyObject *var_arguments = NULL;
    PyObject *var_name = NULL;
    PyObject *var_default = NULL;
    PyObject *var_star_count = NULL;
    PyObject *var_names = NULL;
    PyObject *var_param = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_22192fb76260e227da513a98be74627a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_22192fb76260e227da513a98be74627a = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_22192fb76260e227da513a98be74627a, codeobj_22192fb76260e227da513a98be74627a, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_22192fb76260e227da513a98be74627a = cache_frame_22192fb76260e227da513a98be74627a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_22192fb76260e227da513a98be74627a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_22192fb76260e227da513a98be74627a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_4eff24856086ad17c9fda82689f59bbb;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_DynamicExecutedParams_tuple;
        tmp_level_name_1 = const_int_0;
        frame_22192fb76260e227da513a98be74627a->m_frame.f_lineno = 230;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 230;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_DynamicExecutedParams );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 230;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_DynamicExecutedParams == NULL );
        var_DynamicExecutedParams = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_old_arguments_list == NULL );
        var_old_arguments_list = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( par_self );
        tmp_assign_source_3 = par_self;
        assert( var_arguments == NULL );
        Py_INCREF( tmp_assign_source_3 );
        var_arguments = tmp_assign_source_3;
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( var_arguments == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 234;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = var_arguments;
        CHECK_OBJECT( var_old_arguments_list );
        tmp_compexpr_right_1 = var_old_arguments_list;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        if ( var_arguments == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 235;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_inst_1 = var_arguments;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_TreeArguments );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TreeArguments );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TreeArguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 235;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 235;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 235;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        goto loop_end_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_old_arguments_list );
        tmp_source_name_1 = var_old_arguments_list;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        if ( var_arguments == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = var_arguments;
        frame_22192fb76260e227da513a98be74627a->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_1;
        tmp_called_name_2 = (PyObject *)&PyReversed_Type;
        if ( var_arguments == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = var_arguments;
        frame_22192fb76260e227da513a98be74627a->m_frame.f_lineno = 239;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_as_tree_tuple_objects );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        frame_22192fb76260e227da513a98be74627a->m_frame.f_lineno = 239;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = tmp_for_loop_1__for_iterator;
            tmp_for_loop_1__for_iterator = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooo";
                exception_lineno = 239;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooo";
            exception_lineno = 239;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooo";
            exception_lineno = 239;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooo";
            exception_lineno = 239;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_3;
            tmp_tuple_unpack_1__element_3 = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooo";
                    exception_lineno = 239;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooo";
            exception_lineno = 239;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_10 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_name;
            var_name = tmp_assign_source_10;
            Py_INCREF( var_name );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_11 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_default;
            var_default = tmp_assign_source_11;
            Py_INCREF( var_default );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_12;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_12 = tmp_tuple_unpack_1__element_3;
        {
            PyObject *old = var_star_count;
            var_star_count = tmp_assign_source_12;
            Py_INCREF( var_star_count );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        nuitka_bool tmp_condition_result_3;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_isinstance_inst_2;
        PyObject *tmp_isinstance_cls_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_star_count );
        tmp_operand_name_2 = var_star_count;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( var_name );
        tmp_isinstance_inst_2 = var_name;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_tree );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tree );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tree" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_isinstance_cls_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_Name );
        if ( tmp_isinstance_cls_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
        Py_DECREF( tmp_isinstance_cls_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_or_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_3 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_3 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        goto loop_start_2;
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__evaluator );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_goto );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        if ( var_arguments == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_5 = var_arguments;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_context );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 243;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_name );
        tmp_args_element_name_4 = var_name;
        frame_22192fb76260e227da513a98be74627a->m_frame.f_lineno = 243;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_names;
            var_names = tmp_assign_source_13;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_names );
        tmp_len_arg_1 = var_names;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 244;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        goto loop_end_2;
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_operand_name_4;
        PyObject *tmp_isinstance_inst_3;
        PyObject *tmp_isinstance_cls_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_mvar_value_3;
        CHECK_OBJECT( var_names );
        tmp_subscribed_name_1 = var_names;
        tmp_subscript_name_1 = const_int_0;
        tmp_isinstance_inst_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_isinstance_inst_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_ParamName );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ParamName );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_isinstance_inst_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ParamName" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 246;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }

        tmp_isinstance_cls_3 = tmp_mvar_value_3;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_3, tmp_isinstance_cls_3 );
        Py_DECREF( tmp_isinstance_inst_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        goto loop_end_2;
        branch_no_5:;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( var_names );
        tmp_subscribed_name_2 = var_names;
        tmp_subscript_name_2 = const_int_0;
        tmp_called_instance_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        frame_22192fb76260e227da513a98be74627a->m_frame.f_lineno = 248;
        tmp_assign_source_14 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_param );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_param;
            var_param = tmp_assign_source_14;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_isinstance_inst_4;
        PyObject *tmp_isinstance_cls_4;
        CHECK_OBJECT( var_param );
        tmp_isinstance_inst_4 = var_param;
        CHECK_OBJECT( var_DynamicExecutedParams );
        tmp_isinstance_cls_4 = var_DynamicExecutedParams;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_4, tmp_isinstance_cls_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        tmp_return_value = PyList_New( 0 );
        goto try_return_handler_2;
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_operand_name_5;
        PyObject *tmp_isinstance_inst_5;
        PyObject *tmp_isinstance_cls_5;
        PyObject *tmp_mvar_value_4;
        CHECK_OBJECT( var_param );
        tmp_isinstance_inst_5 = var_param;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_ExecutedParam );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ExecutedParam );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ExecutedParam" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 252;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }

        tmp_isinstance_cls_5 = tmp_mvar_value_4;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_5, tmp_isinstance_cls_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        goto loop_end_2;
        branch_no_7:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( var_param );
        tmp_source_name_6 = var_param;
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_var_args );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_8 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        goto loop_end_2;
        branch_no_8:;
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( var_param );
        tmp_source_name_7 = var_param;
        tmp_assign_source_15 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_var_args );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_arguments;
            var_arguments = tmp_assign_source_15;
            Py_XDECREF( old );
        }

    }
    goto loop_end_2;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 239;
        type_description_1 = "ooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_3;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        type_description_1 = "ooooooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_8;
        if ( var_arguments == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 259;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = var_arguments;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_argument_node );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = Py_None;
        tmp_condition_result_9 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_list_element_1;
            PyObject *tmp_source_name_9;
            if ( var_arguments == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 260;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_9 = var_arguments;
            tmp_list_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_argument_node );
            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 260;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = PyList_New( 1 );
            PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_1 );
            goto frame_return_exit_1;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_10;
        if ( var_arguments == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 261;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = var_arguments;
        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_trailer );
        if ( tmp_compexpr_left_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_5 = Py_None;
        tmp_condition_result_10 = ( tmp_compexpr_left_5 != tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_5 );
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_list_element_2;
            PyObject *tmp_source_name_11;
            if ( var_arguments == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arguments" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 262;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_11 = var_arguments;
            tmp_list_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_trailer );
            if ( tmp_list_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 262;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = PyList_New( 1 );
            PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_2 );
            goto frame_return_exit_1;
        }
        branch_no_10:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22192fb76260e227da513a98be74627a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_22192fb76260e227da513a98be74627a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22192fb76260e227da513a98be74627a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_22192fb76260e227da513a98be74627a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_22192fb76260e227da513a98be74627a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_22192fb76260e227da513a98be74627a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_22192fb76260e227da513a98be74627a,
        type_description_1,
        par_self,
        var_DynamicExecutedParams,
        var_old_arguments_list,
        var_arguments,
        var_name,
        var_default,
        var_star_count,
        var_names,
        var_param
    );


    // Release cached frame.
    if ( frame_22192fb76260e227da513a98be74627a == cache_frame_22192fb76260e227da513a98be74627a )
    {
        Py_DECREF( frame_22192fb76260e227da513a98be74627a );
    }
    cache_frame_22192fb76260e227da513a98be74627a = NULL;

    assertFrameObject( frame_22192fb76260e227da513a98be74627a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = PyList_New( 0 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_16_get_calling_nodes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_DynamicExecutedParams );
    Py_DECREF( var_DynamicExecutedParams );
    var_DynamicExecutedParams = NULL;

    CHECK_OBJECT( (PyObject *)var_old_arguments_list );
    Py_DECREF( var_old_arguments_list );
    var_old_arguments_list = NULL;

    Py_XDECREF( var_arguments );
    var_arguments = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_default );
    var_default = NULL;

    Py_XDECREF( var_star_count );
    var_star_count = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_param );
    var_param = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_DynamicExecutedParams );
    var_DynamicExecutedParams = NULL;

    Py_XDECREF( var_old_arguments_list );
    var_old_arguments_list = NULL;

    Py_XDECREF( var_arguments );
    var_arguments = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_default );
    var_default = NULL;

    Py_XDECREF( var_star_count );
    var_star_count = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_param );
    var_param = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_16_get_calling_nodes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_17___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_values_list = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_f4b160ad19e6052a9e5bfb14154f52a1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f4b160ad19e6052a9e5bfb14154f52a1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f4b160ad19e6052a9e5bfb14154f52a1, codeobj_f4b160ad19e6052a9e5bfb14154f52a1, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *) );
    frame_f4b160ad19e6052a9e5bfb14154f52a1 = cache_frame_f4b160ad19e6052a9e5bfb14154f52a1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f4b160ad19e6052a9e5bfb14154f52a1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f4b160ad19e6052a9e5bfb14154f52a1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_values_list );
        tmp_assattr_name_1 = par_values_list;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__values_list, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 268;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4b160ad19e6052a9e5bfb14154f52a1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4b160ad19e6052a9e5bfb14154f52a1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f4b160ad19e6052a9e5bfb14154f52a1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f4b160ad19e6052a9e5bfb14154f52a1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f4b160ad19e6052a9e5bfb14154f52a1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f4b160ad19e6052a9e5bfb14154f52a1,
        type_description_1,
        par_self,
        par_values_list
    );


    // Release cached frame.
    if ( frame_f4b160ad19e6052a9e5bfb14154f52a1 == cache_frame_f4b160ad19e6052a9e5bfb14154f52a1 )
    {
        Py_DECREF( frame_f4b160ad19e6052a9e5bfb14154f52a1 );
    }
    cache_frame_f4b160ad19e6052a9e5bfb14154f52a1 = NULL;

    assertFrameObject( frame_f4b160ad19e6052a9e5bfb14154f52a1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_17___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_values_list );
    Py_DECREF( par_values_list );
    par_values_list = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_values_list );
    Py_DECREF( par_values_list );
    par_values_list = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_17___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_18_unpack( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_funcdef = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = PyCell_NEW0( par_funcdef );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_self;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_18_unpack );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_18_unpack );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_locals {
    PyObject *var_values;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_values = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_0f00421a7cf8fa6cf871ee36b36b8ba7, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 271;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( generator->m_closure[1] );
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__values_list );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 271;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 271;
            generator_heap->type_description_1 = "cco";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "cco";
                generator_heap->exception_lineno = 271;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_values;
            generator_heap->var_values = tmp_assign_source_3;
            Py_INCREF( generator_heap->var_values );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_tuple_element_1 = Py_None;
        tmp_expression_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyKnownContexts );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyKnownContexts );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyKnownContexts" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 272;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_values );
        tmp_args_element_name_1 = generator_heap->var_values;
        generator->m_frame->m_frame.f_lineno = 272;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 272;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 272;
            generator_heap->type_description_1 = "cco";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 271;
        generator_heap->type_description_1 = "cco";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[1],
            generator->m_closure[0],
            generator_heap->var_values
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_values );
    generator_heap->var_values = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    Py_XDECREF( generator_heap->var_values );
    generator_heap->var_values = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_context,
        module_jedi$evaluate$arguments,
        const_str_plain_unpack,
#if PYTHON_VERSION >= 350
        const_str_digest_6c0ce7584fef273d51290df2a2fe598e,
#endif
        codeobj_0f00421a7cf8fa6cf871ee36b36b8ba7,
        2,
        sizeof(struct jedi$evaluate$arguments$$$function_18_unpack$$$genobj_1_unpack_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_19___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_eea3050785657066e34a3884f81785c9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_eea3050785657066e34a3884f81785c9 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eea3050785657066e34a3884f81785c9, codeobj_eea3050785657066e34a3884f81785c9, module_jedi$evaluate$arguments, sizeof(void *) );
    frame_eea3050785657066e34a3884f81785c9 = cache_frame_eea3050785657066e34a3884f81785c9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eea3050785657066e34a3884f81785c9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eea3050785657066e34a3884f81785c9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        tmp_left_name_1 = const_str_digest_f088166ebe9aa8754d7a4327a52e54aa;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__values_list );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 275;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eea3050785657066e34a3884f81785c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eea3050785657066e34a3884f81785c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eea3050785657066e34a3884f81785c9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eea3050785657066e34a3884f81785c9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eea3050785657066e34a3884f81785c9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eea3050785657066e34a3884f81785c9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eea3050785657066e34a3884f81785c9,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_eea3050785657066e34a3884f81785c9 == cache_frame_eea3050785657066e34a3884f81785c9 )
    {
        Py_DECREF( frame_eea3050785657066e34a3884f81785c9 );
    }
    cache_frame_eea3050785657066e34a3884f81785c9 = NULL;

    assertFrameObject( frame_eea3050785657066e34a3884f81785c9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_19___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_19___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$arguments$$$function_20__iterate_star_args( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_context = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_array = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_input_node = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_funcdef = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_array;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = par_context;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] = par_funcdef;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[2] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] = par_input_node;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[3] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_20__iterate_star_args );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_array );
    Py_DECREF( par_array );
    par_array = NULL;

    CHECK_OBJECT( (PyObject *)par_input_node );
    Py_DECREF( par_input_node );
    par_input_node = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_array );
    Py_DECREF( par_array );
    par_array = NULL;

    CHECK_OBJECT( (PyObject *)par_input_node );
    Py_DECREF( par_input_node );
    par_input_node = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_20__iterate_star_args );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_locals {
    PyObject *var_iter_;
    PyObject *var_m;
    PyObject *var_lazy_context;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    nuitka_bool tmp_try_except_1__unhandled_indicator;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    int exception_keeper_lineno_5;
};

static PyObject *jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_locals *generator_heap = (struct jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_iter_ = NULL;
    generator_heap->var_m = NULL;
    generator_heap->var_lazy_context = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_try_except_1__unhandled_indicator = NUITKA_BOOL_UNASSIGNED;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    {
        nuitka_bool tmp_assign_source_1;
        tmp_assign_source_1 = NUITKA_BOOL_TRUE;
        generator_heap->tmp_try_except_1__unhandled_indicator = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_84fa22eb45b2fbb8c121d4f60fd9de0f, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "array" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 280;
            generator_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }

        tmp_source_name_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_py__iter__ );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 280;
            generator_heap->type_description_1 = "ccccooo";
            goto try_except_handler_3;
        }
        assert( generator_heap->var_iter_ == NULL );
        generator_heap->var_iter_ = tmp_assign_source_2;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_assign_source_3;
        tmp_assign_source_3 = NUITKA_BOOL_FALSE;
        generator_heap->tmp_try_except_1__unhandled_indicator = tmp_assign_source_3;
    }
    // Preserve existing published exception.
    generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_type_1 );
    generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_value_1 );
    generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_tb_1 );

    if ( generator_heap->exception_keeper_tb_1 == NULL )
    {
        generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }
    else if ( generator_heap->exception_keeper_lineno_1 != 0 )
    {
        generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 281;
            generator_heap->type_description_1 = "ccccooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            if ( PyCell_GET( generator->m_closure[2] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "funcdef" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 282;
                generator_heap->type_description_1 = "ccccooo";
                goto try_except_handler_4;
            }

            tmp_compexpr_left_2 = PyCell_GET( generator->m_closure[2] );
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_source_name_3;
                tmp_left_name_1 = const_str_digest_35a3634dcbac6afef9b1545a33d3fba5;
                if ( PyCell_GET( generator->m_closure[2] ) == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "funcdef" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 285;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }

                tmp_source_name_3 = PyCell_GET( generator->m_closure[2] );
                tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_name );
                if ( tmp_source_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 285;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_value );
                Py_DECREF( tmp_source_name_2 );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 285;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                if ( PyCell_GET( generator->m_closure[0] ) == NULL )
                {
                    Py_DECREF( tmp_right_name_1 );
                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "array" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 285;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }

                tmp_tuple_element_1 = PyCell_GET( generator->m_closure[0] );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
                tmp_assign_source_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 284;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }
                assert( generator_heap->var_m == NULL );
                generator_heap->var_m = tmp_assign_source_4;
            }
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_analysis );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 286;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }

                tmp_source_name_4 = tmp_mvar_value_1;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_add );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 286;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }
                if ( PyCell_GET( generator->m_closure[1] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_1 );
                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "context" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 286;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }

                tmp_tuple_element_2 = PyCell_GET( generator->m_closure[1] );
                tmp_args_name_1 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                tmp_tuple_element_2 = const_str_digest_9eca213790eaaf0b90047bb5e7656ae9;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
                if ( PyCell_GET( generator->m_closure[3] ) == NULL )
                {
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_name_1 );
                    generator_heap->exception_type = PyExc_NameError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "input_node" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 286;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }

                tmp_tuple_element_2 = PyCell_GET( generator->m_closure[3] );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_2 );
                tmp_dict_key_1 = const_str_plain_message;
                CHECK_OBJECT( generator_heap->var_m );
                tmp_dict_value_1 = generator_heap->var_m;
                tmp_kw_name_1 = _PyDict_NewPresized( 1 );
                generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(generator_heap->tmp_res != 0) );
                generator->m_frame->m_frame.f_lineno = 286;
                tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 286;
                    generator_heap->type_description_1 = "ccccooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
        if (unlikely( generator_heap->tmp_result == false ))
        {
            generator_heap->exception_lineno = 279;
        }

        if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;
        generator_heap->type_description_1 = "ccccooo";
        goto try_except_handler_4;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args );
    return NULL;
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( generator_heap->tmp_try_except_1__unhandled_indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = generator_heap->tmp_try_except_1__unhandled_indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_name_2;
            if ( generator_heap->var_iter_ == NULL )
            {

                generator_heap->exception_type = PyExc_UnboundLocalError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "iter_" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 288;
                generator_heap->type_description_1 = "ccccooo";
                goto try_except_handler_2;
            }

            tmp_called_name_2 = generator_heap->var_iter_;
            generator->m_frame->m_frame.f_lineno = 288;
            tmp_iter_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 288;
                generator_heap->type_description_1 = "ccccooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 288;
                generator_heap->type_description_1 = "ccccooo";
                goto try_except_handler_2;
            }
            assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
            generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_5;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = generator_heap->tmp_for_loop_1__for_iterator;
            tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    generator_heap->type_description_1 = "ccccooo";
                    generator_heap->exception_lineno = 288;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
                generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
            tmp_assign_source_7 = generator_heap->tmp_for_loop_1__iter_value;
            {
                PyObject *old = generator_heap->var_lazy_context;
                generator_heap->var_lazy_context = tmp_assign_source_7;
                Py_INCREF( generator_heap->var_lazy_context );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_expression_name_1;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            CHECK_OBJECT( generator_heap->var_lazy_context );
            tmp_expression_name_1 = generator_heap->var_lazy_context;
            Py_INCREF( tmp_expression_name_1 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(nuitka_bool), &tmp_compexpr_right_3, sizeof(nuitka_bool), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(nuitka_bool), &tmp_compexpr_right_3, sizeof(nuitka_bool), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 289;
                generator_heap->type_description_1 = "ccccooo";
                goto try_except_handler_5;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 288;
            generator_heap->type_description_1 = "ccccooo";
            goto try_except_handler_5;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
        generator_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
        generator_heap->tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_3;
        generator_heap->exception_value = generator_heap->exception_keeper_value_3;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

        goto try_except_handler_2;
        // End of try:
        try_end_3:;
        Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
        generator_heap->tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
        Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
        generator_heap->tmp_for_loop_1__for_iterator = NULL;

        branch_no_3:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[1],
            generator->m_closure[0],
            generator->m_closure[3],
            generator->m_closure[2],
            generator_heap->var_iter_,
            generator_heap->var_m,
            generator_heap->var_lazy_context
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_5 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_5 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_5 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_5 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_iter_ );
    generator_heap->var_iter_ = NULL;

    Py_XDECREF( generator_heap->var_m );
    generator_heap->var_m = NULL;

    Py_XDECREF( generator_heap->var_lazy_context );
    generator_heap->var_lazy_context = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_5;
    generator_heap->exception_value = generator_heap->exception_keeper_value_5;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_5;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:
    try_end_5:;
    Py_XDECREF( generator_heap->var_iter_ );
    generator_heap->var_iter_ = NULL;

    Py_XDECREF( generator_heap->var_m );
    generator_heap->var_m = NULL;

    Py_XDECREF( generator_heap->var_lazy_context );
    generator_heap->var_lazy_context = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_context,
        module_jedi$evaluate$arguments,
        const_str_plain__iterate_star_args,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_84fa22eb45b2fbb8c121d4f60fd9de0f,
        4,
        sizeof(struct jedi$evaluate$arguments$$$function_20__iterate_star_args$$$genobj_1__iterate_star_args_locals)
    );
}


static PyObject *impl_jedi$evaluate$arguments$$$function_21__star_star_dict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_context = python_pars[ 0 ];
    PyObject *par_array = python_pars[ 1 ];
    PyObject *par_input_node = python_pars[ 2 ];
    PyObject *par_funcdef = python_pars[ 3 ];
    PyObject *var_CompiledInstance = NULL;
    PyObject *var_m = NULL;
    struct Nuitka_FrameObject *frame_5c5e6b5570fd6739cfeb97dcdf11c626;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5c5e6b5570fd6739cfeb97dcdf11c626 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5c5e6b5570fd6739cfeb97dcdf11c626, codeobj_5c5e6b5570fd6739cfeb97dcdf11c626, module_jedi$evaluate$arguments, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5c5e6b5570fd6739cfeb97dcdf11c626 = cache_frame_5c5e6b5570fd6739cfeb97dcdf11c626;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5c5e6b5570fd6739cfeb97dcdf11c626 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5c5e6b5570fd6739cfeb97dcdf11c626 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_36e46c252a3e3bf20afff1d05096f2ab;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_CompiledInstance_tuple;
        tmp_level_name_1 = const_int_0;
        frame_5c5e6b5570fd6739cfeb97dcdf11c626->m_frame.f_lineno = 293;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_CompiledInstance );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_CompiledInstance == NULL );
        var_CompiledInstance = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_array );
        tmp_isinstance_inst_1 = par_array;
        CHECK_OBJECT( var_CompiledInstance );
        tmp_isinstance_cls_1 = var_CompiledInstance;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_array );
        tmp_source_name_2 = par_array;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_string_name );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_plain_dict;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = PyDict_New();
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_2;
            nuitka_bool tmp_and_left_value_2;
            nuitka_bool tmp_and_right_value_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_array );
            tmp_isinstance_inst_2 = par_array;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_iterable );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 298;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_1;
            tmp_isinstance_cls_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Sequence );
            if ( tmp_isinstance_cls_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 298;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            Py_DECREF( tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 298;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            CHECK_OBJECT( par_array );
            tmp_source_name_4 = par_array;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_array_type );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 298;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_str_plain_dict;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 298;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_condition_result_2 = tmp_and_left_value_2;
            and_end_2:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( par_array );
                tmp_called_instance_1 = par_array;
                frame_5c5e6b5570fd6739cfeb97dcdf11c626->m_frame.f_lineno = 299;
                tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exact_key_items );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 299;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( par_funcdef );
                tmp_compexpr_left_3 = par_funcdef;
                tmp_compexpr_right_3 = Py_None;
                tmp_condition_result_3 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_2;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_tuple_element_1;
                    PyObject *tmp_source_name_5;
                    PyObject *tmp_source_name_6;
                    tmp_left_name_1 = const_str_digest_7672e0ecf9f07e55e6b5cbaaa0903ba1;
                    CHECK_OBJECT( par_funcdef );
                    tmp_source_name_6 = par_funcdef;
                    tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_name );
                    if ( tmp_source_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 303;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_value );
                    Py_DECREF( tmp_source_name_5 );
                    if ( tmp_tuple_element_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 303;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_1 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                    CHECK_OBJECT( par_array );
                    tmp_tuple_element_1 = par_array;
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
                    tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_assign_source_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 302;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_m == NULL );
                    var_m = tmp_assign_source_2;
                }
                {
                    PyObject *tmp_called_name_1;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_call_result_1;
                    PyObject *tmp_args_name_1;
                    PyObject *tmp_tuple_element_2;
                    PyObject *tmp_kw_name_1;
                    PyObject *tmp_dict_key_1;
                    PyObject *tmp_dict_value_1;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_analysis );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 304;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_7 = tmp_mvar_value_2;
                    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_add );
                    if ( tmp_called_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 304;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_context );
                    tmp_tuple_element_2 = par_context;
                    tmp_args_name_1 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                    tmp_tuple_element_2 = const_str_digest_5ec6de478bf8667b4b45176a02cc0af8;
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
                    CHECK_OBJECT( par_input_node );
                    tmp_tuple_element_2 = par_input_node;
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_2 );
                    tmp_dict_key_1 = const_str_plain_message;
                    CHECK_OBJECT( var_m );
                    tmp_dict_value_1 = var_m;
                    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
                    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                    assert( !(tmp_res != 0) );
                    frame_5c5e6b5570fd6739cfeb97dcdf11c626->m_frame.f_lineno = 304;
                    tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_name_1 );
                    Py_DECREF( tmp_kw_name_1 );
                    if ( tmp_call_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 304;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_1 );
                }
                branch_no_3:;
            }
            tmp_return_value = PyDict_New();
            goto frame_return_exit_1;
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5c5e6b5570fd6739cfeb97dcdf11c626 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5c5e6b5570fd6739cfeb97dcdf11c626 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5c5e6b5570fd6739cfeb97dcdf11c626 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5c5e6b5570fd6739cfeb97dcdf11c626, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5c5e6b5570fd6739cfeb97dcdf11c626->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5c5e6b5570fd6739cfeb97dcdf11c626, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5c5e6b5570fd6739cfeb97dcdf11c626,
        type_description_1,
        par_context,
        par_array,
        par_input_node,
        par_funcdef,
        var_CompiledInstance,
        var_m
    );


    // Release cached frame.
    if ( frame_5c5e6b5570fd6739cfeb97dcdf11c626 == cache_frame_5c5e6b5570fd6739cfeb97dcdf11c626 )
    {
        Py_DECREF( frame_5c5e6b5570fd6739cfeb97dcdf11c626 );
    }
    cache_frame_5c5e6b5570fd6739cfeb97dcdf11c626 = NULL;

    assertFrameObject( frame_5c5e6b5570fd6739cfeb97dcdf11c626 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_21__star_star_dict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_array );
    Py_DECREF( par_array );
    par_array = NULL;

    CHECK_OBJECT( (PyObject *)par_input_node );
    Py_DECREF( par_input_node );
    par_input_node = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    CHECK_OBJECT( (PyObject *)var_CompiledInstance );
    Py_DECREF( var_CompiledInstance );
    var_CompiledInstance = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_array );
    Py_DECREF( par_array );
    par_array = NULL;

    CHECK_OBJECT( (PyObject *)par_input_node );
    Py_DECREF( par_input_node );
    par_input_node = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    Py_XDECREF( var_CompiledInstance );
    var_CompiledInstance = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments$$$function_21__star_star_dict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_10___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_10___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_406a297b9f88bef7616554d036a1dfce,
#endif
        codeobj_f74863e3a40c1c188a4e2a354b3186aa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_11___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_11___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_bb44427537b9f39e513bbbac7876eb36,
#endif
        codeobj_4aec7604550177c33f576602ae68e7aa,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        const_str_digest_1438f90ac5cf0b076569edae30e5d38f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_12__split(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_12__split,
        const_str_plain__split,
#if PYTHON_VERSION >= 300
        const_str_digest_92556a1adf162047547119b2621563b3,
#endif
        codeobj_d67683786dac3d83577a480799f42096,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_13_unpack( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_13_unpack,
        const_str_plain_unpack,
#if PYTHON_VERSION >= 300
        const_str_digest_26ad4ced4c4df3f616989585528cdeb7,
#endif
        codeobj_c84bee060b6acd8351ea26c2a9cbc92d,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects,
        const_str_plain_as_tree_tuple_objects,
#if PYTHON_VERSION >= 300
        const_str_digest_1a4d1d0ac4461ae617038f2c10257b40,
#endif
        codeobj_d292d3f28731963bec78e997a2fdb614,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_15___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_15___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_be314bee9b21ffc1600b7cfc80d2d6c2,
#endif
        codeobj_eb74dbad402bcef7e4fb4a91dce58de7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_16_get_calling_nodes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_16_get_calling_nodes,
        const_str_plain_get_calling_nodes,
#if PYTHON_VERSION >= 300
        const_str_digest_5498161026ca17b47252cf3b548d9743,
#endif
        codeobj_22192fb76260e227da513a98be74627a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_17___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_17___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_47015d9d6e865ff086a6c5a7555d2998,
#endif
        codeobj_f4b160ad19e6052a9e5bfb14154f52a1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_18_unpack( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_18_unpack,
        const_str_plain_unpack,
#if PYTHON_VERSION >= 300
        const_str_digest_6c0ce7584fef273d51290df2a2fe598e,
#endif
        codeobj_0f00421a7cf8fa6cf871ee36b36b8ba7,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_19___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_19___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_5de7b9637e12a56e87449e32f6814e59,
#endif
        codeobj_eea3050785657066e34a3884f81785c9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_1_try_iter_content( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_1_try_iter_content,
        const_str_plain_try_iter_content,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8f7244f6e41fb0b67e0561df2444b8b0,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        const_str_digest_4523d9a1b64655374e1942fd87c2a659,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_20__iterate_star_args( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_20__iterate_star_args,
        const_str_plain__iterate_star_args,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_84fa22eb45b2fbb8c121d4f60fd9de0f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_21__star_star_dict(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_21__star_star_dict,
        const_str_plain__star_star_dict,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5c5e6b5570fd6739cfeb97dcdf11c626,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic,
        const_str_plain_repack_with_argument_clinic,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0a4cabe4811bd13cbfd06415b68c1fe6,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        const_str_digest_94ee30445d8d1cbe81a0b02bab7e510a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator,
        const_str_plain_decorator,
#if PYTHON_VERSION >= 300
        const_str_digest_5113a0c12054796e82cd2a031a93a92b,
#endif
        codeobj_550ddc84fe7b1acc1934dc45a10b6905,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic$$$function_1_decorator$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_95594c52b163dd9f042a1f828995d3ba,
#endif
        codeobj_97283cf1a5f77f9719abc1145357f94b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        3
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_3__iterate_argument_clinic(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_3__iterate_argument_clinic,
        const_str_plain__iterate_argument_clinic,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c79604a1e1235335690b09bbc78cc449,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        const_str_digest_ac1f38bec9728bd5b20209f335579d2f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_4__parse_argument_clinic(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_4__parse_argument_clinic,
        const_str_plain__parse_argument_clinic,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f4e7584e431f136d5523962d1dd15696,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_5_eval_all( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_5_eval_all,
        const_str_plain_eval_all,
#if PYTHON_VERSION >= 300
        const_str_digest_a550d01a3bba920d4d857eb16ce6bfb2,
#endif
        codeobj_511e84622be8967aa3d3dad49070cbde,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        const_str_digest_222afe807ed606367487725a32c52c7c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_6_get_calling_nodes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_6_get_calling_nodes,
        const_str_plain_get_calling_nodes,
#if PYTHON_VERSION >= 300
        const_str_digest_58e0c401994ef9a5ed2ad7ac21d5fd74,
#endif
        codeobj_4b62345805eae7a2eea7f9bf5aa7f87f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_7_unpack( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_7_unpack,
        const_str_plain_unpack,
#if PYTHON_VERSION >= 300
        const_str_digest_b8654fb5f883f28e1aa51998ecfd80be,
#endif
        codeobj_2ddac29db203e3015a4f8876a14332ec,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_8_get_executed_params(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_8_get_executed_params,
        const_str_plain_get_executed_params,
#if PYTHON_VERSION >= 300
        const_str_digest_ccdc881ece2ecf71de99e64173a2e77f,
#endif
        codeobj_1cb7a2b5f3c011c02a21a7ee5a36ae4c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$arguments$$$function_9_get_executed_params(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$arguments$$$function_9_get_executed_params,
        const_str_plain_get_executed_params,
#if PYTHON_VERSION >= 300
        const_str_digest_ba8aaf77fcf21b9f37e561daf1fdf76b,
#endif
        codeobj_54d7da2c1dfc5702d05918ca8ddc2bf2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$arguments,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jedi$evaluate$arguments =
{
    PyModuleDef_HEAD_INIT,
    "jedi.evaluate.arguments",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jedi$evaluate$arguments)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jedi$evaluate$arguments)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jedi$evaluate$arguments );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.arguments: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.arguments: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.arguments: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjedi$evaluate$arguments" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jedi$evaluate$arguments = Py_InitModule4(
        "jedi.evaluate.arguments",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jedi$evaluate$arguments = PyModule_Create( &mdef_jedi$evaluate$arguments );
#endif

    moduledict_jedi$evaluate$arguments = MODULE_DICT( module_jedi$evaluate$arguments );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jedi$evaluate$arguments,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jedi$evaluate$arguments,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate$arguments,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate$arguments,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jedi$evaluate$arguments );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_93e555a5b1643558b28cf6b2256158dc, module_jedi$evaluate$arguments );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__bases_orig = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_c03f1e1d5ff3897f482f9b6f063fc57d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jedi$evaluate$arguments_105 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_0b0766d805c3751504e8c1aa03e56f60_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0b0766d805c3751504e8c1aa03e56f60_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_jedi$evaluate$arguments_129 = NULL;
    struct Nuitka_FrameObject *frame_06dcdb711788f30b999a61171c023218_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_06dcdb711788f30b999a61171c023218_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_jedi$evaluate$arguments_142 = NULL;
    struct Nuitka_FrameObject *frame_5f2059120328d2005b855a1561388c69_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_5f2059120328d2005b855a1561388c69_4 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *locals_jedi$evaluate$arguments_266 = NULL;
    struct Nuitka_FrameObject *frame_c260aa4eb415ead534da046b6a064a61_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_c260aa4eb415ead534da046b6a064a61_5 = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c03f1e1d5ff3897f482f9b6f063fc57d = MAKE_MODULE_FRAME( codeobj_c03f1e1d5ff3897f482f9b6f063fc57d, module_jedi$evaluate$arguments );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c03f1e1d5ff3897f482f9b6f063fc57d );
    assert( Py_REFCNT( frame_c03f1e1d5ff3897f482f9b6f063fc57d ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 1;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_3d8c90d9a9bb20202fbddcdf6f6b7d19;
        tmp_globals_name_2 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_tree_tuple;
        tmp_level_name_2 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 3;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_tree );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_tree, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_4ea3c739c0994e1c59a1638cbf9bd81c;
        tmp_globals_name_3 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_zip_longest_tuple;
        tmp_level_name_3 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 5;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_zip_longest );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_zip_longest, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_jedi;
        tmp_globals_name_4 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_debug_tuple;
        tmp_level_name_4 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 6;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_debug );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_debug, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_5 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_analysis_tuple;
        tmp_level_name_5 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 7;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_analysis );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_analysis, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_4b6ae0a4a7c5ef73221de5bb2b8ff0e4;
        tmp_globals_name_6 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_5636789a8b789ad5d0fc96a7e74745bb_tuple;
        tmp_level_name_6 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 8;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_9;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_LazyKnownContext );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyKnownContext, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_LazyKnownContexts );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyKnownContexts, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_LazyTreeContext );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_LazyTreeContext, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_8 = tmp_import_from_1__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_get_merged_lazy_context );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_get_merged_lazy_context, tmp_assign_source_13 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_c32a307eb4cbe1d85147469cdfd3f906;
        tmp_globals_name_7 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_ParamName_tuple;
        tmp_level_name_7 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 10;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_ParamName );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_ParamName, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_74a64e2326d0e94475afe0623d07b03d;
        tmp_globals_name_8 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_NO_CONTEXTS_tuple;
        tmp_level_name_8 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 11;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_NO_CONTEXTS );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_11;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_785ebe15053a4fa731c98e296de8edd7;
        tmp_globals_name_9 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_iterable_tuple;
        tmp_level_name_9 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 12;
        tmp_import_name_from_11 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_iterable );
        Py_DECREF( tmp_import_name_from_11 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_iterable, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_9762bd19b18a11af1c0649d1b042a42f;
        tmp_globals_name_10 = (PyObject *)moduledict_jedi$evaluate$arguments;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_get_executed_params_str_plain_ExecutedParam_tuple;
        tmp_level_name_10 = const_int_0;
        frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 13;
        tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_17;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_12 = tmp_import_from_2__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_get_executed_params );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_get_executed_params, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_13 = tmp_import_from_2__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_ExecutedParam );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_ExecutedParam, tmp_assign_source_19 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_int_0_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_20 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_1_try_iter_content( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_try_iter_content, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_false_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_21 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_2_repack_with_argument_clinic( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_repack_with_argument_clinic, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_3__iterate_argument_clinic(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__iterate_argument_clinic, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_4__parse_argument_clinic(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__parse_argument_clinic, tmp_assign_source_23 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_24 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_26 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_26;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;

                goto try_except_handler_3;
            }
            tmp_tuple_element_1 = const_str_plain_AbstractArguments;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 105;
            tmp_assign_source_27 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_27;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 105;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 105;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 105;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 105;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_28;
            tmp_assign_source_28 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_28;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_29;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jedi$evaluate$arguments_105 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_93e555a5b1643558b28cf6b2256158dc;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_AbstractArguments;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0b0766d805c3751504e8c1aa03e56f60_2, codeobj_0b0766d805c3751504e8c1aa03e56f60, module_jedi$evaluate$arguments, sizeof(void *) );
        frame_0b0766d805c3751504e8c1aa03e56f60_2 = cache_frame_0b0766d805c3751504e8c1aa03e56f60_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0b0766d805c3751504e8c1aa03e56f60_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0b0766d805c3751504e8c1aa03e56f60_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_context, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_argument_node, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_trailer, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_3;
            tmp_defaults_3 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_5_eval_all( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_eval_all, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 110;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_6_get_calling_nodes(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_get_calling_nodes, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_4;
            tmp_defaults_4 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_4 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_7_unpack( tmp_defaults_4 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_unpack, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_8_get_executed_params(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain_get_executed_params, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0b0766d805c3751504e8c1aa03e56f60_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0b0766d805c3751504e8c1aa03e56f60_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0b0766d805c3751504e8c1aa03e56f60_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0b0766d805c3751504e8c1aa03e56f60_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0b0766d805c3751504e8c1aa03e56f60_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0b0766d805c3751504e8c1aa03e56f60_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_0b0766d805c3751504e8c1aa03e56f60_2 == cache_frame_0b0766d805c3751504e8c1aa03e56f60_2 )
        {
            Py_DECREF( frame_0b0766d805c3751504e8c1aa03e56f60_2 );
        }
        cache_frame_0b0766d805c3751504e8c1aa03e56f60_2 = NULL;

        assertFrameObject( frame_0b0766d805c3751504e8c1aa03e56f60_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_105, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_AbstractArguments;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jedi$evaluate$arguments_105;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 105;
            tmp_assign_source_30 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_30;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_29 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_29 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_jedi$evaluate$arguments_105 );
        locals_jedi$evaluate$arguments_105 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$arguments_105 );
        locals_jedi$evaluate$arguments_105 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 105;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_AbstractArguments, tmp_assign_source_29 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_AbstractArguments );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AbstractArguments );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AbstractArguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;

            goto try_except_handler_6;
        }

        tmp_tuple_element_4 = tmp_mvar_value_3;
        tmp_assign_source_31 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assign_source_31, 0, tmp_tuple_element_4 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_32 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_34 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_34;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_6;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;

                goto try_except_handler_6;
            }
            tmp_tuple_element_5 = const_str_plain_AnonymousArguments;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 129;
            tmp_assign_source_35 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_35;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;

                goto try_except_handler_6;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 129;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 129;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 129;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 129;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_36;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_37;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_jedi$evaluate$arguments_129 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_93e555a5b1643558b28cf6b2256158dc;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_129, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_AnonymousArguments;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_129, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_06dcdb711788f30b999a61171c023218_3, codeobj_06dcdb711788f30b999a61171c023218, module_jedi$evaluate$arguments, sizeof(void *) );
        frame_06dcdb711788f30b999a61171c023218_3 = cache_frame_06dcdb711788f30b999a61171c023218_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_06dcdb711788f30b999a61171c023218_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_06dcdb711788f30b999a61171c023218_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_9_get_executed_params(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_129, const_str_plain_get_executed_params, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_10___repr__(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_129, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_06dcdb711788f30b999a61171c023218_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_06dcdb711788f30b999a61171c023218_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_06dcdb711788f30b999a61171c023218_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_06dcdb711788f30b999a61171c023218_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_06dcdb711788f30b999a61171c023218_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_06dcdb711788f30b999a61171c023218_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_06dcdb711788f30b999a61171c023218_3 == cache_frame_06dcdb711788f30b999a61171c023218_3 )
        {
            Py_DECREF( frame_06dcdb711788f30b999a61171c023218_3 );
        }
        cache_frame_06dcdb711788f30b999a61171c023218_3 = NULL;

        assertFrameObject( frame_06dcdb711788f30b999a61171c023218_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_8;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;

                goto try_except_handler_8;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_129, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;

                goto try_except_handler_8;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_4 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_7 = const_str_plain_AnonymousArguments;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_7 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
            tmp_tuple_element_7 = locals_jedi$evaluate$arguments_129;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 129;
            tmp_assign_source_38 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_38;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_37 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_37 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_jedi$evaluate$arguments_129 );
        locals_jedi$evaluate$arguments_129 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$arguments_129 );
        locals_jedi$evaluate$arguments_129 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 129;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_AnonymousArguments, tmp_assign_source_37 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_tuple_element_8;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_AbstractArguments );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AbstractArguments );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AbstractArguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto try_except_handler_9;
        }

        tmp_tuple_element_8 = tmp_mvar_value_4;
        tmp_assign_source_39 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_assign_source_39, 0, tmp_tuple_element_8 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_40 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_14;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        tmp_condition_result_14 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_42 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_42;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_9;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_9 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_10 = tmp_class_creation_3__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_9;
            }
            tmp_tuple_element_9 = const_str_plain_TreeArguments;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_9 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 142;
            tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_43;
        }
        {
            nuitka_bool tmp_condition_result_17;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_11 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_9;
            }
            tmp_condition_result_17 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_10;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_10 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_9;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_10 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 142;

                    goto try_except_handler_9;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_10 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;

                    goto try_except_handler_9;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 142;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_9;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_44;
            tmp_assign_source_44 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_44;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_45;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_jedi$evaluate$arguments_142 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_93e555a5b1643558b28cf6b2256158dc;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_plain_TreeArguments;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto try_except_handler_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_5f2059120328d2005b855a1561388c69_4, codeobj_5f2059120328d2005b855a1561388c69, module_jedi$evaluate$arguments, sizeof(void *) );
        frame_5f2059120328d2005b855a1561388c69_4 = cache_frame_5f2059120328d2005b855a1561388c69_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_5f2059120328d2005b855a1561388c69_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_5f2059120328d2005b855a1561388c69_4 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_5;
            tmp_defaults_5 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_5 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_11___init__( tmp_defaults_5 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_12__split(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain__split, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        {
            PyObject *tmp_defaults_6;
            tmp_defaults_6 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_6 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_13_unpack( tmp_defaults_6 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain_unpack, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_14_as_tree_tuple_objects(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain_as_tree_tuple_objects, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_15___repr__(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_16_get_calling_nodes(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain_get_calling_nodes, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 229;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5f2059120328d2005b855a1561388c69_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5f2059120328d2005b855a1561388c69_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_5f2059120328d2005b855a1561388c69_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_5f2059120328d2005b855a1561388c69_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_5f2059120328d2005b855a1561388c69_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_5f2059120328d2005b855a1561388c69_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_5f2059120328d2005b855a1561388c69_4 == cache_frame_5f2059120328d2005b855a1561388c69_4 )
        {
            Py_DECREF( frame_5f2059120328d2005b855a1561388c69_4 );
        }
        cache_frame_5f2059120328d2005b855a1561388c69_4 = NULL;

        assertFrameObject( frame_5f2059120328d2005b855a1561388c69_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_11;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_11;
            }
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_142, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_11;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_6 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_11 = const_str_plain_TreeArguments;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_11 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_11 );
            tmp_tuple_element_11 = locals_jedi$evaluate$arguments_142;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 142;
            tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 142;

                goto try_except_handler_11;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_46;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_45 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_45 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        Py_DECREF( locals_jedi$evaluate$arguments_142 );
        locals_jedi$evaluate$arguments_142 = NULL;
        goto try_return_handler_10;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$arguments_142 );
        locals_jedi$evaluate$arguments_142 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_10;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 142;
        goto try_except_handler_9;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_TreeArguments, tmp_assign_source_45 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_tuple_element_12;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_AbstractArguments );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AbstractArguments );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AbstractArguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 266;

            goto try_except_handler_12;
        }

        tmp_tuple_element_12 = tmp_mvar_value_5;
        tmp_assign_source_47 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_12 );
        PyTuple_SET_ITEM( tmp_assign_source_47, 0, tmp_tuple_element_12 );
        assert( tmp_class_creation_4__bases_orig == NULL );
        tmp_class_creation_4__bases_orig = tmp_assign_source_47;
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_dircall_arg1_4;
        CHECK_OBJECT( tmp_class_creation_4__bases_orig );
        tmp_dircall_arg1_4 = tmp_class_creation_4__bases_orig;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_48 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_48;
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_49;
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_19;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_20;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        tmp_condition_result_20 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_4 = tmp_class_creation_4__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_8:;
        condexpr_end_7:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_50 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_50;
    }
    {
        nuitka_bool tmp_condition_result_21;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_12;
        }
        branch_no_13:;
    }
    {
        nuitka_bool tmp_condition_result_22;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_13 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___prepare__ );
        tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_51;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_14;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_14 = tmp_class_creation_4__metaclass;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___prepare__ );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;

                goto try_except_handler_12;
            }
            tmp_tuple_element_13 = const_str_plain_ValuesArguments;
            tmp_args_name_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_13 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_4__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 266;
            tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;

                goto try_except_handler_12;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_51;
        }
        {
            nuitka_bool tmp_condition_result_23;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_15 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;

                goto try_except_handler_12;
            }
            tmp_condition_result_23 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_14;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_16;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_14 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 266;

                    goto try_except_handler_12;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_14 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_16 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_16 == NULL) );
                tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_16 );
                if ( tmp_tuple_element_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 266;

                    goto try_except_handler_12;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_14 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 266;

                    goto try_except_handler_12;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 266;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_12;
            }
            branch_no_15:;
        }
        goto branch_end_14;
        branch_no_14:;
        {
            PyObject *tmp_assign_source_52;
            tmp_assign_source_52 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_52;
        }
        branch_end_14:;
    }
    {
        PyObject *tmp_assign_source_53;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_jedi$evaluate$arguments_266 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_93e555a5b1643558b28cf6b2256158dc;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_266, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_14;
        }
        tmp_dictset_value = const_str_plain_ValuesArguments;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_266, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto try_except_handler_14;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_c260aa4eb415ead534da046b6a064a61_5, codeobj_c260aa4eb415ead534da046b6a064a61, module_jedi$evaluate$arguments, sizeof(void *) );
        frame_c260aa4eb415ead534da046b6a064a61_5 = cache_frame_c260aa4eb415ead534da046b6a064a61_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_c260aa4eb415ead534da046b6a064a61_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_c260aa4eb415ead534da046b6a064a61_5 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_17___init__(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_266, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 267;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        {
            PyObject *tmp_defaults_7;
            tmp_defaults_7 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_7 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_18_unpack( tmp_defaults_7 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_266, const_str_plain_unpack, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 270;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_19___repr__(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_266, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c260aa4eb415ead534da046b6a064a61_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c260aa4eb415ead534da046b6a064a61_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_c260aa4eb415ead534da046b6a064a61_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_c260aa4eb415ead534da046b6a064a61_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_c260aa4eb415ead534da046b6a064a61_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_c260aa4eb415ead534da046b6a064a61_5,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_c260aa4eb415ead534da046b6a064a61_5 == cache_frame_c260aa4eb415ead534da046b6a064a61_5 )
        {
            Py_DECREF( frame_c260aa4eb415ead534da046b6a064a61_5 );
        }
        cache_frame_c260aa4eb415ead534da046b6a064a61_5 = NULL;

        assertFrameObject( frame_c260aa4eb415ead534da046b6a064a61_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_14;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_24;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_4 = tmp_class_creation_4__bases;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_compexpr_right_4 = tmp_class_creation_4__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;

                goto try_except_handler_14;
            }
            tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_dictset_value = tmp_class_creation_4__bases_orig;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$arguments_266, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;

                goto try_except_handler_14;
            }
            branch_no_16:;
        }
        {
            PyObject *tmp_assign_source_54;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_15;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_8 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_15 = const_str_plain_ValuesArguments;
            tmp_args_name_8 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_15 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_15 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_15 );
            tmp_tuple_element_15 = locals_jedi$evaluate$arguments_266;
            Py_INCREF( tmp_tuple_element_15 );
            PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_15 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame.f_lineno = 266;
            tmp_assign_source_54 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_8, tmp_kw_name_8 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_54 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 266;

                goto try_except_handler_14;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_54;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_53 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_53 );
        goto try_return_handler_14;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        Py_DECREF( locals_jedi$evaluate$arguments_266 );
        locals_jedi$evaluate$arguments_266 = NULL;
        goto try_return_handler_13;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$arguments_266 );
        locals_jedi$evaluate$arguments_266 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto try_except_handler_13;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$arguments );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 266;
        goto try_except_handler_12;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain_ValuesArguments, tmp_assign_source_53 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c03f1e1d5ff3897f482f9b6f063fc57d );
#endif
    popFrameStack();

    assertFrameObject( frame_c03f1e1d5ff3897f482f9b6f063fc57d );

    goto frame_no_exception_5;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c03f1e1d5ff3897f482f9b6f063fc57d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c03f1e1d5ff3897f482f9b6f063fc57d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c03f1e1d5ff3897f482f9b6f063fc57d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c03f1e1d5ff3897f482f9b6f063fc57d, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases_orig );
    Py_DECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_defaults_8;
        tmp_defaults_8 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_8 );
        tmp_assign_source_55 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_20__iterate_star_args( tmp_defaults_8 );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__iterate_star_args, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = MAKE_FUNCTION_jedi$evaluate$arguments$$$function_21__star_star_dict(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$arguments, (Nuitka_StringObject *)const_str_plain__star_star_dict, tmp_assign_source_56 );
    }

    return MOD_RETURN_VALUE( module_jedi$evaluate$arguments );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
