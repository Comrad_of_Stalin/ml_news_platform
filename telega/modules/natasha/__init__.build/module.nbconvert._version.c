/* Generated code for Python module 'nbconvert._version'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbconvert$_version" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbconvert$_version;
PyDictObject *moduledict_nbconvert$_version;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_str_plain_prefix_str_plain_pre_input_tuple;
static PyObject *const_str_plain_out_version;
static PyObject *const_str_digest_a80d60c0f34651df18c3ebec75b5d058;
static PyObject *const_tuple_str_digest_739668f090bfd9222f6cf24461fa91b9_tuple;
static PyObject *const_str_digest_3c700c4411163d2e8e93f7a0fe32818c;
extern PyObject *const_str_plain_match;
extern PyObject *const_str_plain_b;
static PyObject *const_tuple_str_plain_version_str_plain_re_tuple;
extern PyObject *const_str_plain___spec__;
static PyObject *const_xrange_0_10;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain_map;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_int_pos_5_int_pos_5_int_0_tuple;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_0;
static PyObject *const_str_digest_7a81646a3031478db9eb0e30396e3a3a;
extern PyObject *const_str_plain___version__;
static PyObject *const_str_plain_pre_seg;
extern PyObject *const_int_0;
static PyObject *const_list_str_plain_a_str_plain_b_str_plain_rc_list;
static PyObject *const_str_plain_dev_info;
static PyObject *const_str_digest_8cc4aae7ec0e4b177d591ac9a475ba6e;
extern PyObject *const_str_plain_prefix;
static PyObject *const_str_plain_pep440_err;
static PyObject *const_str_plain__magic_pre;
extern PyObject *const_str_plain_a;
extern PyObject *const_str_plain_rc;
static PyObject *const_str_digest_aeba953610f62e7d5ba00f74c940cfa0;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_release_seg;
extern PyObject *const_str_chr_33;
static PyObject *const_tuple_str_plain_n_str_plain_dev_seg_tuple;
extern PyObject *const_str_digest_739668f090bfd9222f6cf24461fa91b9;
static PyObject *const_str_plain_epoch;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_plain_None;
extern PyObject *const_tuple_str_dot_tuple;
static PyObject *const_tuple_str_plain_dev_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_epoch_seg;
extern PyObject *const_str_plain_version;
extern PyObject *const_int_pos_5;
static PyObject *const_str_digest_dac9b966bd397e2934f1949cab48e783;
static PyObject *const_str_digest_c50ccba4a98582d38da9c7810207c859;
static PyObject *const_str_plain_release_info;
extern PyObject *const_str_plain_dev;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_6417b5e3700fbe69854c25a87c768a1a;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_empty;
static PyObject *const_tuple_none_str_empty_str_empty_tuple;
static PyObject *const_str_plain_pre_input;
static PyObject *const_str_plain_dev_input;
extern PyObject *const_str_plain_endswith;
extern PyObject *const_int_pos_10;
static PyObject *const_str_plain_dev_seg;
static PyObject *const_str_digest_0a5943f42ffb829b470d63b41795c813;
static PyObject *const_str_plain_create_valid_version;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_is_canonical;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_plain_pre_info;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_prefix_str_plain_pre_input_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_prefix_str_plain_pre_input_tuple, 0, const_str_plain_prefix ); Py_INCREF( const_str_plain_prefix );
    const_str_plain_pre_input = UNSTREAM_STRING_ASCII( &constant_bin[ 2724750 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_prefix_str_plain_pre_input_tuple, 1, const_str_plain_pre_input ); Py_INCREF( const_str_plain_pre_input );
    const_str_plain_out_version = UNSTREAM_STRING_ASCII( &constant_bin[ 2724759 ], 11, 1 );
    const_str_digest_a80d60c0f34651df18c3ebec75b5d058 = UNSTREAM_STRING_ASCII( &constant_bin[ 2724770 ], 98, 0 );
    const_tuple_str_digest_739668f090bfd9222f6cf24461fa91b9_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_739668f090bfd9222f6cf24461fa91b9_tuple, 0, const_str_digest_739668f090bfd9222f6cf24461fa91b9 ); Py_INCREF( const_str_digest_739668f090bfd9222f6cf24461fa91b9 );
    const_str_digest_3c700c4411163d2e8e93f7a0fe32818c = UNSTREAM_STRING_ASCII( &constant_bin[ 2724868 ], 21, 0 );
    const_tuple_str_plain_version_str_plain_re_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_version_str_plain_re_tuple, 0, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    PyTuple_SET_ITEM( const_tuple_str_plain_version_str_plain_re_tuple, 1, const_str_plain_re ); Py_INCREF( const_str_plain_re );
    const_xrange_0_10 = BUILTIN_XRANGE3( const_int_0, const_int_pos_10, const_int_pos_1 );
    const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple = PyTuple_New( 13 );
    const_str_plain_release_info = UNSTREAM_STRING_ASCII( &constant_bin[ 2724889 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 0, const_str_plain_release_info ); Py_INCREF( const_str_plain_release_info );
    const_str_plain_epoch = UNSTREAM_STRING_ASCII( &constant_bin[ 635200 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 1, const_str_plain_epoch ); Py_INCREF( const_str_plain_epoch );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 2, const_str_plain_pre_input ); Py_INCREF( const_str_plain_pre_input );
    const_str_plain_dev_input = UNSTREAM_STRING_ASCII( &constant_bin[ 2724901 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 3, const_str_plain_dev_input ); Py_INCREF( const_str_plain_dev_input );
    const_str_plain_pep440_err = UNSTREAM_STRING_ASCII( &constant_bin[ 2724910 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 4, const_str_plain_pep440_err ); Py_INCREF( const_str_plain_pep440_err );
    const_str_plain_epoch_seg = UNSTREAM_STRING_ASCII( &constant_bin[ 2724920 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 5, const_str_plain_epoch_seg ); Py_INCREF( const_str_plain_epoch_seg );
    const_str_plain_release_seg = UNSTREAM_STRING_ASCII( &constant_bin[ 2724929 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 6, const_str_plain_release_seg ); Py_INCREF( const_str_plain_release_seg );
    const_str_plain__magic_pre = UNSTREAM_STRING_ASCII( &constant_bin[ 2724940 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 7, const_str_plain__magic_pre ); Py_INCREF( const_str_plain__magic_pre );
    const_str_plain_pre_seg = UNSTREAM_STRING_ASCII( &constant_bin[ 2724950 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 8, const_str_plain_pre_seg ); Py_INCREF( const_str_plain_pre_seg );
    const_str_plain_dev_seg = UNSTREAM_STRING_ASCII( &constant_bin[ 2724957 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 9, const_str_plain_dev_seg ); Py_INCREF( const_str_plain_dev_seg );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 10, const_str_plain_out_version ); Py_INCREF( const_str_plain_out_version );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 11, const_str_plain_re ); Py_INCREF( const_str_plain_re );
    const_str_plain_is_canonical = UNSTREAM_STRING_ASCII( &constant_bin[ 2724964 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 12, const_str_plain_is_canonical ); Py_INCREF( const_str_plain_is_canonical );
    const_tuple_int_pos_5_int_pos_5_int_0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_int_pos_5_int_pos_5_int_0_tuple, 0, const_int_pos_5 ); Py_INCREF( const_int_pos_5 );
    PyTuple_SET_ITEM( const_tuple_int_pos_5_int_pos_5_int_0_tuple, 1, const_int_pos_5 ); Py_INCREF( const_int_pos_5 );
    PyTuple_SET_ITEM( const_tuple_int_pos_5_int_pos_5_int_0_tuple, 2, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_digest_7a81646a3031478db9eb0e30396e3a3a = UNSTREAM_STRING_ASCII( &constant_bin[ 2724976 ], 18, 0 );
    const_list_str_plain_a_str_plain_b_str_plain_rc_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_a_str_plain_b_str_plain_rc_list, 0, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyList_SET_ITEM( const_list_str_plain_a_str_plain_b_str_plain_rc_list, 1, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyList_SET_ITEM( const_list_str_plain_a_str_plain_b_str_plain_rc_list, 2, const_str_plain_rc ); Py_INCREF( const_str_plain_rc );
    const_str_plain_dev_info = UNSTREAM_STRING_ASCII( &constant_bin[ 2724994 ], 8, 1 );
    const_str_digest_8cc4aae7ec0e4b177d591ac9a475ba6e = UNSTREAM_STRING_ASCII( &constant_bin[ 2725002 ], 27, 0 );
    const_str_digest_aeba953610f62e7d5ba00f74c940cfa0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2725029 ], 42, 0 );
    const_tuple_str_plain_n_str_plain_dev_seg_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_n_str_plain_dev_seg_tuple, 0, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_str_plain_n_str_plain_dev_seg_tuple, 1, const_str_plain_dev_seg ); Py_INCREF( const_str_plain_dev_seg );
    const_tuple_str_plain_dev_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dev_tuple, 0, const_str_plain_dev ); Py_INCREF( const_str_plain_dev );
    const_str_digest_dac9b966bd397e2934f1949cab48e783 = UNSTREAM_STRING_ASCII( &constant_bin[ 2725071 ], 97, 0 );
    const_str_digest_c50ccba4a98582d38da9c7810207c859 = UNSTREAM_STRING_ASCII( &constant_bin[ 2724770 ], 60, 0 );
    const_str_digest_6417b5e3700fbe69854c25a87c768a1a = UNSTREAM_STRING_ASCII( &constant_bin[ 2725168 ], 295, 0 );
    const_tuple_none_str_empty_str_empty_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_none_str_empty_str_empty_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_str_empty_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_none_str_empty_str_empty_tuple, 2, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_digest_0a5943f42ffb829b470d63b41795c813 = UNSTREAM_STRING_ASCII( &constant_bin[ 2725463 ], 107, 0 );
    const_str_plain_create_valid_version = UNSTREAM_STRING_ASCII( &constant_bin[ 2725029 ], 20, 1 );
    const_str_plain_pre_info = UNSTREAM_STRING_ASCII( &constant_bin[ 2725570 ], 8, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbconvert$_version( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_dee98024233b8b37600f71ba1ad36d2f;
static PyCodeObject *codeobj_9a662e838f119a97db4c76f90eb3ca96;
static PyCodeObject *codeobj_c29cf356d6cf149c296423c4a3b99c36;
static PyCodeObject *codeobj_2343100da356a1ee8d1467a179dadd77;
static PyCodeObject *codeobj_6fff53e0e144d0c92accb54f29c7d873;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_3c700c4411163d2e8e93f7a0fe32818c );
    codeobj_dee98024233b8b37600f71ba1ad36d2f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 43, const_tuple_str_plain_n_str_plain_dev_seg_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a662e838f119a97db4c76f90eb3ca96 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 29, const_tuple_str_plain_prefix_str_plain_pre_input_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c29cf356d6cf149c296423c4a3b99c36 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8cc4aae7ec0e4b177d591ac9a475ba6e, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_2343100da356a1ee8d1467a179dadd77 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_valid_version, 5, const_tuple_53a7aecac0d1b8ec3362a432c3e8cad4_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6fff53e0e144d0c92accb54f29c7d873 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_canonical, 50, const_tuple_str_plain_version_str_plain_re_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_nbconvert$_version$$$function_1_create_valid_version( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical(  );


// The module function definitions.
static PyObject *impl_nbconvert$_version$$$function_1_create_valid_version( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_release_info = python_pars[ 0 ];
    PyObject *par_epoch = python_pars[ 1 ];
    PyObject *par_pre_input = python_pars[ 2 ];
    PyObject *par_dev_input = python_pars[ 3 ];
    PyObject *var_epoch_seg = NULL;
    PyObject *var_release_seg = NULL;
    PyObject *var__magic_pre = NULL;
    PyObject *var_pre_seg = NULL;
    PyObject *var_dev_seg = NULL;
    PyObject *var_out_version = NULL;
    struct Nuitka_CellObject *var_re = PyCell_EMPTY();
    PyObject *var_is_canonical = NULL;
    PyObject *outline_0_var_prefix = NULL;
    PyObject *outline_1_var_n = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_2343100da356a1ee8d1467a179dadd77;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_9a662e838f119a97db4c76f90eb3ca96_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_9a662e838f119a97db4c76f90eb3ca96_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_dee98024233b8b37600f71ba1ad36d2f_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_dee98024233b8b37600f71ba1ad36d2f_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_2343100da356a1ee8d1467a179dadd77 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2343100da356a1ee8d1467a179dadd77, codeobj_2343100da356a1ee8d1467a179dadd77, module_nbconvert$_version, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2343100da356a1ee8d1467a179dadd77 = cache_frame_2343100da356a1ee8d1467a179dadd77;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2343100da356a1ee8d1467a179dadd77 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2343100da356a1ee8d1467a179dadd77 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_epoch );
        tmp_compexpr_left_1 = par_epoch;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( par_epoch );
            tmp_unicode_arg_1 = par_epoch;
            tmp_left_name_1 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = const_str_chr_33;
            tmp_assign_source_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            assert( var_epoch_seg == NULL );
            var_epoch_seg = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = const_str_empty;
            assert( var_epoch_seg == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_epoch_seg = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_source_name_1 = const_str_dot;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        tmp_called_name_2 = (PyObject *)&PyMap_Type;
        tmp_args_element_name_2 = (PyObject *)&PyUnicode_Type;
        CHECK_OBJECT( par_release_info );
        tmp_args_element_name_3 = par_release_info;
        frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 26;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 26;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 26;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        assert( var_release_seg == NULL );
        var_release_seg = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = LIST_COPY( const_list_str_plain_a_str_plain_b_str_plain_rc_list );
        assert( var__magic_pre == NULL );
        var__magic_pre = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_any_arg_1;
        CHECK_OBJECT( par_pre_input );
        tmp_compexpr_left_2 = par_pre_input;
        tmp_compexpr_right_2 = const_str_empty;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var__magic_pre );
            tmp_iter_arg_1 = var__magic_pre;
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_1 = "ooooNooooooco";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_9a662e838f119a97db4c76f90eb3ca96_2, codeobj_9a662e838f119a97db4c76f90eb3ca96, module_nbconvert$_version, sizeof(void *)+sizeof(void *) );
        frame_9a662e838f119a97db4c76f90eb3ca96_2 = cache_frame_9a662e838f119a97db4c76f90eb3ca96_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_9a662e838f119a97db4c76f90eb3ca96_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_9a662e838f119a97db4c76f90eb3ca96_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 29;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_8 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_prefix;
                outline_0_var_prefix = tmp_assign_source_8;
                Py_INCREF( outline_0_var_prefix );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( par_pre_input );
            tmp_called_instance_1 = par_pre_input;
            CHECK_OBJECT( outline_0_var_prefix );
            tmp_args_element_name_4 = outline_0_var_prefix;
            frame_9a662e838f119a97db4c76f90eb3ca96_2->m_frame.f_lineno = 29;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_append_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, call_args );
            }

            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_any_arg_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_any_arg_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_9a662e838f119a97db4c76f90eb3ca96_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_9a662e838f119a97db4c76f90eb3ca96_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_9a662e838f119a97db4c76f90eb3ca96_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_9a662e838f119a97db4c76f90eb3ca96_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_9a662e838f119a97db4c76f90eb3ca96_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_9a662e838f119a97db4c76f90eb3ca96_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_9a662e838f119a97db4c76f90eb3ca96_2,
            type_description_2,
            outline_0_var_prefix,
            par_pre_input
        );


        // Release cached frame.
        if ( frame_9a662e838f119a97db4c76f90eb3ca96_2 == cache_frame_9a662e838f119a97db4c76f90eb3ca96_2 )
        {
            Py_DECREF( frame_9a662e838f119a97db4c76f90eb3ca96_2 );
        }
        cache_frame_9a662e838f119a97db4c76f90eb3ca96_2 = NULL;

        assertFrameObject( frame_9a662e838f119a97db4c76f90eb3ca96_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooooNooooooco";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_prefix );
        outline_0_var_prefix = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_prefix );
        outline_0_var_prefix = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
        return NULL;
        outline_exception_1:;
        exception_lineno = 29;
        goto frame_exception_exit_1;
        outline_result_1:;
        tmp_operand_name_1 = BUILTIN_ANY( tmp_any_arg_1 );
        Py_DECREF( tmp_any_arg_1 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_dac9b966bd397e2934f1949cab48e783;
            frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 30;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 30;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( par_pre_input );
            tmp_assign_source_9 = par_pre_input;
            assert( var_pre_seg == NULL );
            Py_INCREF( tmp_assign_source_9 );
            var_pre_seg = tmp_assign_source_9;
        }
        branch_end_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( par_dev_input );
        tmp_compexpr_left_3 = par_dev_input;
        tmp_compexpr_right_3 = const_str_empty;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( par_dev_input );
            tmp_assign_source_10 = par_dev_input;
            assert( var_dev_seg == NULL );
            Py_INCREF( tmp_assign_source_10 );
            var_dev_seg = tmp_assign_source_10;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            int tmp_and_left_truth_2;
            nuitka_bool tmp_and_left_value_2;
            nuitka_bool tmp_and_right_value_2;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_dev_input );
            tmp_called_instance_2 = par_dev_input;
            frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 36;
            tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

            if ( tmp_operand_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            CHECK_OBJECT( par_dev_input );
            tmp_called_instance_3 = par_dev_input;
            frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 36;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_dev_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 36;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            tmp_condition_result_4 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_condition_result_4 = tmp_and_left_value_2;
            and_end_2:;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_11;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_list_element_1;
                tmp_called_instance_4 = const_str_empty;
                tmp_list_element_1 = const_str_dot;
                tmp_args_element_name_5 = PyList_New( 2 );
                Py_INCREF( tmp_list_element_1 );
                PyList_SET_ITEM( tmp_args_element_name_5, 0, tmp_list_element_1 );
                CHECK_OBJECT( par_dev_input );
                tmp_list_element_1 = par_dev_input;
                Py_INCREF( tmp_list_element_1 );
                PyList_SET_ITEM( tmp_args_element_name_5, 1, tmp_list_element_1 );
                frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 37;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5 };
                    tmp_assign_source_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_join, call_args );
                }

                Py_DECREF( tmp_args_element_name_5 );
                if ( tmp_assign_source_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 37;
                    type_description_1 = "ooooNooooooco";
                    goto frame_exception_exit_1;
                }
                assert( var_dev_seg == NULL );
                var_dev_seg = tmp_assign_source_11;
            }
            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_called_instance_5;
                PyObject *tmp_call_result_2;
                int tmp_truth_name_2;
                CHECK_OBJECT( par_dev_input );
                tmp_called_instance_5 = par_dev_input;
                frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 38;
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_739668f090bfd9222f6cf24461fa91b9_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 38;
                    type_description_1 = "ooooNooooooco";
                    goto frame_exception_exit_1;
                }
                tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_2 );

                    exception_lineno = 38;
                    type_description_1 = "ooooNooooooco";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_2 );
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_12;
                    CHECK_OBJECT( par_dev_input );
                    tmp_assign_source_12 = par_dev_input;
                    assert( var_dev_seg == NULL );
                    Py_INCREF( tmp_assign_source_12 );
                    var_dev_seg = tmp_assign_source_12;
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    nuitka_bool tmp_condition_result_6;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    CHECK_OBJECT( par_dev_input );
                    tmp_compexpr_left_4 = par_dev_input;
                    tmp_compexpr_right_4 = const_str_empty;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 40;
                        type_description_1 = "ooooNooooooco";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_raise_type_2;
                        PyObject *tmp_make_exception_arg_2;
                        tmp_make_exception_arg_2 = const_str_digest_a80d60c0f34651df18c3ebec75b5d058;
                        frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 41;
                        {
                            PyObject *call_args[] = { tmp_make_exception_arg_2 };
                            tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                        }

                        assert( !(tmp_raise_type_2 == NULL) );
                        exception_type = tmp_raise_type_2;
                        exception_lineno = 41;
                        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooNooooooco";
                        goto frame_exception_exit_1;
                    }
                    branch_no_6:;
                }
                branch_end_5:;
            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    {
        nuitka_bool tmp_condition_result_7;
        int tmp_and_left_truth_3;
        nuitka_bool tmp_and_left_value_3;
        nuitka_bool tmp_and_right_value_3;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_any_arg_2;
        CHECK_OBJECT( par_dev_input );
        tmp_compexpr_left_5 = par_dev_input;
        tmp_compexpr_right_5 = const_str_empty;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_3 == 1 )
        {
            goto and_right_3;
        }
        else
        {
            goto and_left_3;
        }
        and_right_3:;
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_iter_arg_2;
            tmp_iter_arg_2 = const_xrange_0_10;
            tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_2 );
            assert( !(tmp_assign_source_13 == NULL) );
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_13;
        }
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_14;
        }
        // Tried code:
        MAKE_OR_REUSE_FRAME( cache_frame_dee98024233b8b37600f71ba1ad36d2f_3, codeobj_dee98024233b8b37600f71ba1ad36d2f, module_nbconvert$_version, sizeof(void *)+sizeof(void *) );
        frame_dee98024233b8b37600f71ba1ad36d2f_3 = cache_frame_dee98024233b8b37600f71ba1ad36d2f_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_dee98024233b8b37600f71ba1ad36d2f_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_dee98024233b8b37600f71ba1ad36d2f_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_2 = tmp_listcomp_2__$0;
            tmp_assign_source_15 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 43;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_16 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_n;
                outline_1_var_n = tmp_assign_source_16;
                Py_INCREF( outline_1_var_n );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_2;
            PyObject *tmp_append_value_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_unicode_arg_2;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_append_list_2 = tmp_listcomp_2__contraction;
            if ( var_dev_seg == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "dev_seg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }

            tmp_source_name_2 = var_dev_seg;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_endswith );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( outline_1_var_n );
            tmp_unicode_arg_2 = outline_1_var_n;
            tmp_args_element_name_6 = PyObject_Unicode( tmp_unicode_arg_2 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 43;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            frame_dee98024233b8b37600f71ba1ad36d2f_3->m_frame.f_lineno = 43;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_append_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_append_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            assert( PyList_Check( tmp_append_list_2 ) );
            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
            Py_DECREF( tmp_append_value_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_2 = "oo";
            goto try_except_handler_5;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_any_arg_2 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_any_arg_2 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_dee98024233b8b37600f71ba1ad36d2f_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_dee98024233b8b37600f71ba1ad36d2f_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_4;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_dee98024233b8b37600f71ba1ad36d2f_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_dee98024233b8b37600f71ba1ad36d2f_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_dee98024233b8b37600f71ba1ad36d2f_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_dee98024233b8b37600f71ba1ad36d2f_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_dee98024233b8b37600f71ba1ad36d2f_3,
            type_description_2,
            outline_1_var_n,
            var_dev_seg
        );


        // Release cached frame.
        if ( frame_dee98024233b8b37600f71ba1ad36d2f_3 == cache_frame_dee98024233b8b37600f71ba1ad36d2f_3 )
        {
            Py_DECREF( frame_dee98024233b8b37600f71ba1ad36d2f_3 );
        }
        cache_frame_dee98024233b8b37600f71ba1ad36d2f_3 = NULL;

        assertFrameObject( frame_dee98024233b8b37600f71ba1ad36d2f_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "ooooNooooooco";
        goto try_except_handler_4;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( outline_1_var_n );
        outline_1_var_n = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_n );
        outline_1_var_n = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
        return NULL;
        outline_exception_2:;
        exception_lineno = 43;
        goto frame_exception_exit_1;
        outline_result_2:;
        tmp_operand_name_3 = BUILTIN_ANY( tmp_any_arg_2 );
        Py_DECREF( tmp_any_arg_2 );
        if ( tmp_operand_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        Py_DECREF( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_7 = tmp_and_right_value_3;
        goto and_end_3;
        and_left_3:;
        tmp_condition_result_7 = tmp_and_left_value_3;
        and_end_3:;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_list_element_2;
            tmp_source_name_3 = const_str_empty;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
            assert( !(tmp_called_name_4 == NULL) );
            if ( var_dev_seg == NULL )
            {
                Py_DECREF( tmp_called_name_4 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "dev_seg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 44;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }

            tmp_list_element_2 = var_dev_seg;
            tmp_args_element_name_7 = PyList_New( 2 );
            Py_INCREF( tmp_list_element_2 );
            PyList_SET_ITEM( tmp_args_element_name_7, 0, tmp_list_element_2 );
            tmp_list_element_2 = const_str_plain_0;
            Py_INCREF( tmp_list_element_2 );
            PyList_SET_ITEM( tmp_args_element_name_7, 1, tmp_list_element_2 );
            frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 44;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;
                type_description_1 = "ooooNooooooco";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_dev_seg;
                var_dev_seg = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        branch_no_7:;
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_list_element_3;
        tmp_source_name_4 = const_str_empty;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_join );
        assert( !(tmp_called_name_5 == NULL) );
        CHECK_OBJECT( var_epoch_seg );
        tmp_list_element_3 = var_epoch_seg;
        tmp_args_element_name_8 = PyList_New( 4 );
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_args_element_name_8, 0, tmp_list_element_3 );
        CHECK_OBJECT( var_release_seg );
        tmp_list_element_3 = var_release_seg;
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_args_element_name_8, 1, tmp_list_element_3 );
        if ( var_pre_seg == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_8 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "pre_seg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }

        tmp_list_element_3 = var_pre_seg;
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_args_element_name_8, 2, tmp_list_element_3 );
        if ( var_dev_seg == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_8 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "dev_seg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }

        tmp_list_element_3 = var_dev_seg;
        Py_INCREF( tmp_list_element_3 );
        PyList_SET_ITEM( tmp_args_element_name_8, 3, tmp_list_element_3 );
        frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        assert( var_out_version == NULL );
        var_out_version = tmp_assign_source_18;
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_nbconvert$_version;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 49;
        tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_re ) == NULL );
        PyCell_SET( var_re, tmp_assign_source_19 );

    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_20)->m_closure[0] = var_re;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_20)->m_closure[0] );


        assert( var_is_canonical == NULL );
        var_is_canonical = tmp_assign_source_20;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_called_name_6;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_9;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_is_canonical );
        tmp_called_name_6 = var_is_canonical;
        CHECK_OBJECT( var_out_version );
        tmp_args_element_name_9 = var_out_version;
        frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_3 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_3 );

            exception_lineno = 57;
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_3 );
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        CHECK_OBJECT( var_out_version );
        tmp_return_value = var_out_version;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_8;
        branch_no_8:;
        {
            PyObject *tmp_raise_type_3;
            PyObject *tmp_make_exception_arg_3;
            tmp_make_exception_arg_3 = const_str_digest_c50ccba4a98582d38da9c7810207c859;
            frame_2343100da356a1ee8d1467a179dadd77->m_frame.f_lineno = 60;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_3 };
                tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_3 == NULL) );
            exception_type = tmp_raise_type_3;
            exception_lineno = 60;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooNooooooco";
            goto frame_exception_exit_1;
        }
        branch_end_8:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2343100da356a1ee8d1467a179dadd77 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2343100da356a1ee8d1467a179dadd77 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2343100da356a1ee8d1467a179dadd77 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2343100da356a1ee8d1467a179dadd77, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2343100da356a1ee8d1467a179dadd77->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2343100da356a1ee8d1467a179dadd77, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2343100da356a1ee8d1467a179dadd77,
        type_description_1,
        par_release_info,
        par_epoch,
        par_pre_input,
        par_dev_input,
        NULL,
        var_epoch_seg,
        var_release_seg,
        var__magic_pre,
        var_pre_seg,
        var_dev_seg,
        var_out_version,
        var_re,
        var_is_canonical
    );


    // Release cached frame.
    if ( frame_2343100da356a1ee8d1467a179dadd77 == cache_frame_2343100da356a1ee8d1467a179dadd77 )
    {
        Py_DECREF( frame_2343100da356a1ee8d1467a179dadd77 );
    }
    cache_frame_2343100da356a1ee8d1467a179dadd77 = NULL;

    assertFrameObject( frame_2343100da356a1ee8d1467a179dadd77 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_release_info );
    Py_DECREF( par_release_info );
    par_release_info = NULL;

    CHECK_OBJECT( (PyObject *)par_epoch );
    Py_DECREF( par_epoch );
    par_epoch = NULL;

    CHECK_OBJECT( (PyObject *)par_pre_input );
    Py_DECREF( par_pre_input );
    par_pre_input = NULL;

    CHECK_OBJECT( (PyObject *)par_dev_input );
    Py_DECREF( par_dev_input );
    par_dev_input = NULL;

    CHECK_OBJECT( (PyObject *)var_epoch_seg );
    Py_DECREF( var_epoch_seg );
    var_epoch_seg = NULL;

    CHECK_OBJECT( (PyObject *)var_release_seg );
    Py_DECREF( var_release_seg );
    var_release_seg = NULL;

    CHECK_OBJECT( (PyObject *)var__magic_pre );
    Py_DECREF( var__magic_pre );
    var__magic_pre = NULL;

    Py_XDECREF( var_pre_seg );
    var_pre_seg = NULL;

    Py_XDECREF( var_dev_seg );
    var_dev_seg = NULL;

    CHECK_OBJECT( (PyObject *)var_out_version );
    Py_DECREF( var_out_version );
    var_out_version = NULL;

    CHECK_OBJECT( (PyObject *)var_re );
    Py_DECREF( var_re );
    var_re = NULL;

    CHECK_OBJECT( (PyObject *)var_is_canonical );
    Py_DECREF( var_is_canonical );
    var_is_canonical = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_release_info );
    Py_DECREF( par_release_info );
    par_release_info = NULL;

    CHECK_OBJECT( (PyObject *)par_epoch );
    Py_DECREF( par_epoch );
    par_epoch = NULL;

    CHECK_OBJECT( (PyObject *)par_pre_input );
    Py_DECREF( par_pre_input );
    par_pre_input = NULL;

    CHECK_OBJECT( (PyObject *)par_dev_input );
    Py_DECREF( par_dev_input );
    par_dev_input = NULL;

    Py_XDECREF( var_epoch_seg );
    var_epoch_seg = NULL;

    Py_XDECREF( var_release_seg );
    var_release_seg = NULL;

    Py_XDECREF( var__magic_pre );
    var__magic_pre = NULL;

    Py_XDECREF( var_pre_seg );
    var_pre_seg = NULL;

    Py_XDECREF( var_dev_seg );
    var_dev_seg = NULL;

    Py_XDECREF( var_out_version );
    var_out_version = NULL;

    CHECK_OBJECT( (PyObject *)var_re );
    Py_DECREF( var_re );
    var_re = NULL;

    Py_XDECREF( var_is_canonical );
    var_is_canonical = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_version = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6fff53e0e144d0c92accb54f29c7d873;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6fff53e0e144d0c92accb54f29c7d873 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6fff53e0e144d0c92accb54f29c7d873, codeobj_6fff53e0e144d0c92accb54f29c7d873, module_nbconvert$_version, sizeof(void *)+sizeof(void *) );
    frame_6fff53e0e144d0c92accb54f29c7d873 = cache_frame_6fff53e0e144d0c92accb54f29c7d873;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6fff53e0e144d0c92accb54f29c7d873 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6fff53e0e144d0c92accb54f29c7d873 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        tmp_args_element_name_1 = const_str_digest_0a5943f42ffb829b470d63b41795c813;
        CHECK_OBJECT( par_version );
        tmp_args_element_name_2 = par_version;
        frame_6fff53e0e144d0c92accb54f29c7d873->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_compexpr_left_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_match, call_args );
        }

        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_return_value = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? Py_True : Py_False;
        Py_DECREF( tmp_compexpr_left_1 );
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fff53e0e144d0c92accb54f29c7d873 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fff53e0e144d0c92accb54f29c7d873 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fff53e0e144d0c92accb54f29c7d873 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6fff53e0e144d0c92accb54f29c7d873, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6fff53e0e144d0c92accb54f29c7d873->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6fff53e0e144d0c92accb54f29c7d873, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6fff53e0e144d0c92accb54f29c7d873,
        type_description_1,
        par_version,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_6fff53e0e144d0c92accb54f29c7d873 == cache_frame_6fff53e0e144d0c92accb54f29c7d873 )
    {
        Py_DECREF( frame_6fff53e0e144d0c92accb54f29c7d873 );
    }
    cache_frame_6fff53e0e144d0c92accb54f29c7d873 = NULL;

    assertFrameObject( frame_6fff53e0e144d0c92accb54f29c7d873 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_nbconvert$_version$$$function_1_create_valid_version( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$_version$$$function_1_create_valid_version,
        const_str_plain_create_valid_version,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2343100da356a1ee8d1467a179dadd77,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$_version,
        const_str_digest_6417b5e3700fbe69854c25a87c768a1a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbconvert$_version$$$function_1_create_valid_version$$$function_1_is_canonical,
        const_str_plain_is_canonical,
#if PYTHON_VERSION >= 300
        const_str_digest_aeba953610f62e7d5ba00f74c940cfa0,
#endif
        codeobj_6fff53e0e144d0c92accb54f29c7d873,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbconvert$_version,
        NULL,
        1
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbconvert$_version =
{
    PyModuleDef_HEAD_INIT,
    "nbconvert._version",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbconvert$_version)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbconvert$_version)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbconvert$_version );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbconvert._version: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert._version: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbconvert._version: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbconvert$_version" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbconvert$_version = Py_InitModule4(
        "nbconvert._version",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbconvert$_version = PyModule_Create( &mdef_nbconvert$_version );
#endif

    moduledict_nbconvert$_version = MODULE_DICT( module_nbconvert$_version );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbconvert$_version,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbconvert$_version,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$_version,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbconvert$_version,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbconvert$_version );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_7a81646a3031478db9eb0e30396e3a3a, module_nbconvert$_version );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_c29cf356d6cf149c296423c4a3b99c36;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c29cf356d6cf149c296423c4a3b99c36 = MAKE_MODULE_FRAME( codeobj_c29cf356d6cf149c296423c4a3b99c36, module_nbconvert$_version );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c29cf356d6cf149c296423c4a3b99c36 );
    assert( Py_REFCNT( frame_c29cf356d6cf149c296423c4a3b99c36 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = const_tuple_int_pos_5_int_pos_5_int_0_tuple;
        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_version_info, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = const_str_empty;
        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_pre_info, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = const_str_empty;
        UPDATE_STRING_DICT0( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_dev_info, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_str_empty_str_empty_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_7 = MAKE_FUNCTION_nbconvert$_version$$$function_1_create_valid_version( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_create_valid_version, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_create_valid_version );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_valid_version );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_version_info );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_version_info );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_tuple_element_1 = tmp_mvar_value_4;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_pre_input;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_pre_info );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pre_info );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_dict_value_1 = tmp_mvar_value_5;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_dev_input;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain_dev_info );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dev_info );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_dict_value_2 = tmp_mvar_value_6;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_c29cf356d6cf149c296423c4a3b99c36->m_frame.f_lineno = 63;
        tmp_assign_source_8 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbconvert$_version, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_8 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c29cf356d6cf149c296423c4a3b99c36 );
#endif
    popFrameStack();

    assertFrameObject( frame_c29cf356d6cf149c296423c4a3b99c36 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c29cf356d6cf149c296423c4a3b99c36 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c29cf356d6cf149c296423c4a3b99c36, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c29cf356d6cf149c296423c4a3b99c36->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c29cf356d6cf149c296423c4a3b99c36, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_nbconvert$_version );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
