/* Generated code for Python module 'matplotlib.texmanager'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$texmanager" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$texmanager;
PyDictObject *moduledict_matplotlib$texmanager;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_dict;
extern PyObject *const_str_plain___name__;
static PyObject *const_tuple_5be502270cb646a1dc150e71300691a3_tuple;
static PyObject *const_str_digest_e01874232ccefaf0d37dc214b5be49b0;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain_points_to_pixels;
extern PyObject *const_str_plain_encode;
static PyObject *const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple;
extern PyObject *const_str_plain_err;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_n_tuple;
static PyObject *const_str_digest_f5b3e2da1ea2a856c2d58edc746b8ee9;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_chr_42;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_digest_95b020fe417b0958dcb803463bbe8067;
static PyObject *const_tuple_str_plain_dvi_str_plain_tex_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_changed_str_plain_k_tuple;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_make_dvi;
extern PyObject *const_str_digest_80d71dc37939494333d50379f9639d3c;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_matplotlib;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_str;
static PyObject *const_tuple_str_plain_l1_str_plain_dpi_fraction_tuple;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_hexdigest;
extern PyObject *const_str_plain_Z;
extern PyObject *const_str_plain_hashlib;
extern PyObject *const_float_1_0;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_chr_45;
extern PyObject *const_str_plain_font;
extern PyObject *const_tuple_str_plain__png_tuple;
extern PyObject *const_str_plain_TexManager;
static PyObject *const_str_plain_texfile;
static PyObject *const_tuple_str_plain_cls_str_plain_self_tuple;
static PyObject *const_str_plain__fontconfig;
static PyObject *const_str_digest_73298c22917b298b198faf9bb5b452f4;
extern PyObject *const_str_plain_rcParams;
extern PyObject *const_str_plain_rgb;
extern PyObject *const_str_plain_changed;
extern PyObject *const_str_plain_endswith;
static PyObject *const_str_digest_9d50db30dcf25d7bb8b27b5444f5991e;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_descent;
extern PyObject *const_str_plain_l1;
static PyObject *const_str_digest_a8dc9ad548bfe95b26e3c1ba965ee0a8;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain___new__;
static PyObject *const_str_plain__run_checked_subprocess;
extern PyObject *const_str_plain_par;
extern PyObject *const_int_0;
static PyObject *const_str_digest_2505020aad631a36514c89cc34a6693f;
static PyObject *const_str_digest_26938f8f813d1b6246fc351d3a93f5b3;
extern PyObject *const_float_1_25;
extern PyObject *const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
static PyObject *const_tuple_str_digest_8627bf73710bb4a0fb532b132d20b519_tuple;
static PyObject *const_str_digest_d58ac6888e0c113ae7f41e8955b8f6cf;
static PyObject *const_str_plain_make_tex;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain__log;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_356e9ba5a3f3236c7dfc259adeb0d143;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_tuple_int_0_int_0_int_0_tuple;
extern PyObject *const_str_plain_functools;
static PyObject *const_str_digest_9b7324678882942a7030b665c2148e26;
static PyObject *const_str_plain_texcache;
static PyObject *const_str_digest_984c8f5dae87f53dab68d11cd420a5d3;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_dpi;
extern PyObject *const_str_plain_replace;
static PyObject *const_str_digest_8032213fa23e3682f7bc08f590cd931b;
static PyObject *const_str_plain_font_info;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain__png;
extern PyObject *const_str_plain_cursive;
static PyObject *const_str_plain_pngfile;
extern PyObject *const_str_plain_groups;
extern PyObject *const_str_plain_pathlib;
extern PyObject *const_str_plain_write;
static PyObject *const_str_plain_make_png;
static PyObject *const_str_plain_custom_preamble;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_exist_ok;
static PyObject *const_str_plain_basefile;
extern PyObject *const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d;
static PyObject *const_str_plain_dpi_fraction;
extern PyObject *const_str_plain_prog;
static PyObject *const_str_digest_e686bff235b85bb42d2e91e5388e6e2d;
extern PyObject *const_str_digest_e0f88f933d7ade7a3f12c06b501c8f8c;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_lru_cache;
extern PyObject *const_str_plain_baseline;
static PyObject *const_tuple_str_plain_cmtt_str_empty_tuple;
static PyObject *const_str_digest_bce359f7716234fe24f45cb0ed5dadcb;
static PyObject *const_str_plain_preamble_bytes;
extern PyObject *const_tuple_str_plain_utf8_tuple;
extern PyObject *const_str_plain_strip;
extern PyObject *const_str_plain_check_output;
static PyObject *const_str_digest_e7ab4de4b66d3e69ece19096c702273c;
extern PyObject *const_str_plain_CalledProcessError;
static PyObject *const_tuple_tuple_str_plain_dvi_str_plain_tex_tuple_tuple;
extern PyObject *const_str_digest_0ef268d7244e784284aed7369564edb4;
extern PyObject *const_str_plain_read;
extern PyObject *const_str_plain_lower;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_wb;
extern PyObject *const_str_plain_page;
static PyObject *const_str_plain_unicode_preamble;
static PyObject *const_dict_116370359a70a271fa1bb0c2b4992e02;
static PyObject *const_str_plain_get_font_preamble;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_monospace;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_plain_depth;
extern PyObject *const_str_plain_tight;
extern PyObject *const_str_plain_g;
extern PyObject *const_str_plain_alpha;
extern PyObject *const_str_plain_l;
extern PyObject *const_tuple_float_1_0_tuple;
static PyObject *const_str_plain_cmss;
static PyObject *const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple;
static PyObject *const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple;
static PyObject *const_str_digest_81c020872fc7de5ff77312e7e1dc5fc3;
extern PyObject *const_str_plain_decode;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_plain__rc_cache_keys;
static PyObject *const_str_digest_3f89837fd54cf40b4db7d36767a99d84;
static PyObject *const_tuple_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple_tuple;
static PyObject *const_tuple_str_plain_cmss_str_empty_tuple;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain__re_vbox;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_5ccbf34487b50d11e4ba2bd929cdfe91;
static PyObject *const_str_plain_fontcmd;
static PyObject *const_str_digest_2efa96a3fdddfeecd57738931a82445a;
extern PyObject *const_str_plain_cwd;
static PyObject *const_str_digest_2857a7d64c1eed6bf6176547b308f9e2;
extern PyObject *const_tuple_str_chr_45_str_plain___tuple;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_plain_copy;
extern PyObject *const_str_plain_key;
static PyObject *const_str_digest_a3aa3a290fd512f365474a19a5c42e93;
extern PyObject *const_str_digest_b26d3cd4cb167eb4277cbd83e0c608a6;
static PyObject *const_str_plain_get_font_config;
static PyObject *const_tuple_str_plain_unicode_escape_tuple;
extern PyObject *const_str_plain_md5;
static PyObject *const_tuple_slice_none_none_none_slice_none_none_none_int_neg_1_tuple;
static PyObject *const_tuple_str_plain_par_str_plain_self_tuple;
extern PyObject *const_str_plain_fname;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_a6ece34cc8b11b0923a994a726b60f2f;
extern PyObject *const_str_plain_ascii;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_empty;
static PyObject *const_str_plain_font_family_attr;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_str_digest_cdf16115e7b70895f4896e5476e5d9bb;
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
extern PyObject *const_str_plain_getLogger;
extern PyObject *const_str_plain_Path;
static PyObject *const_str_digest_c0052235910e9a252f07af6a9b689c4d;
extern PyObject *const_str_plain_fontsize;
static PyObject *const_tuple_str_digest_984c8f5dae87f53dab68d11cd420a5d3_tuple;
extern PyObject *const_str_plain_mkdir;
static PyObject *const_tuple_str_digest_2927227e39724474c42d53ec57cf54f3_tuple;
static PyObject *const_tuple_b21610e59f492cda989c541866b8c37a_tuple;
static PyObject *const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple;
extern PyObject *const_str_plain_parents;
static PyObject *const_tuple_str_plain_pzc_str_digest_647a68f81e76836af6331dea2d40b163_tuple;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_get_cachedir;
extern PyObject *const_str_digest_876c9648523deea8f3845ee2dd49a0d1;
static PyObject *const_str_digest_71e8eace702b793e0ab622519bafb6c8;
extern PyObject *const_str_plain_STDOUT;
static PyObject *const_str_digest_efd982e9c319b0d2cd1e1a07ed784dab;
static PyObject *const_str_digest_7f30043b9a2bb91b0e84ad3903ac0a42;
static PyObject *const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple;
extern PyObject *const_str_plain_Dvi;
static PyObject *const_str_digest_50ef9b639fb224b7409dfaccdceb4fac;
extern PyObject *const_str_plain_dviread;
static PyObject *const_str_plain_cmtt;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_re;
static PyObject *const_str_plain__rc_cache;
extern PyObject *const_str_plain_ff;
static PyObject *const_str_digest_2824195fb82d23cb05c0a1f71a0e4d3c;
extern PyObject *const_tuple_str_plain_ascii_tuple;
extern PyObject *const_str_plain_s;
static PyObject *const_str_plain_make_dvi_preview;
extern PyObject *const_str_plain_get_grey;
static PyObject *const_str_plain_font_families;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_digest_d4431b0426576a07f19cda03326c253a;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple;
extern PyObject *const_str_plain_height;
static PyObject *const_str_digest_9054ff3e7037d8914f9ab82584551053;
extern PyObject *const_str_plain_subprocess;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_space;
static PyObject *const_str_digest_b038421a02d77f9b5d274e8555649244;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_fh;
static PyObject *const_str_digest_1bebc29034a166ca514e2b054466afb6;
static PyObject *const_str_digest_03b8dda3ffebd96a6b130e1cb60f2f27;
static PyObject *const_str_plain_cachedir;
extern PyObject *const_str_plain_compile;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_remove;
static PyObject *const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple;
static PyObject *const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple;
extern PyObject *const_str_digest_2927227e39724474c42d53ec57cf54f3;
static PyObject *const_str_digest_39d9f01d64225c607852a9c6459f5a6e;
extern PyObject *const_str_plain_renderer;
static PyObject *const_str_digest_60514009de511c7aafdeb720b2a0e9a0;
static PyObject *const_str_digest_847e8a9ba2def92a0a16c231c8318eed;
static PyObject *const_str_digest_ef341ce22ae49aada81d48bddaa4ddcf;
extern PyObject *const_int_pos_72;
static PyObject *const_str_plain_font_family;
extern PyObject *const_str_plain_exists;
extern PyObject *const_str_digest_ba1d2497b5193e6a3b8923fd050980bc;
static PyObject *const_str_plain_get_custom_preamble;
static PyObject *const_str_plain_get_rgba;
static PyObject *const_tuple_str_plain_cmr_str_empty_tuple;
extern PyObject *const_str_plain_serif;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain__;
static PyObject *const_list_str_digest_6cdd425fa9be1d305df640c6875d0373_list;
extern PyObject *const_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_digest_626bcc2c1f36a7ec05dcd93c51f76c6d;
extern PyObject *const_str_plain_debug;
static PyObject *const_str_digest_63dc86e1658320ba5eb6595f23eb3c31;
extern PyObject *const_str_plain_utf8;
static PyObject *const_str_digest_aa245a491a278bdf2c7c38c3cead3cf7;
static PyObject *const_str_digest_c396522b4d8d8b219cca9cbe512ef24e;
extern PyObject *const_str_plain_cmd;
static PyObject *const_str_plain_make_tex_preview;
static PyObject *const_str_digest_8c92326123525d178b015f645b1acaae;
extern PyObject *const_str_plain_X;
extern PyObject *const_str_plain_get_text_width_height_descent;
extern PyObject *const_str_plain_tex;
extern PyObject *const_tuple_str_plain_Path_tuple;
static PyObject *const_str_digest_647a68f81e76836af6331dea2d40b163;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_np;
static PyObject *const_str_digest_94ba037759435a553bb5b8a7861af2ff;
static PyObject *const_str_digest_3d457918981054c271396c44cb4a3da1;
static PyObject *const_str_plain_sans_serif;
extern PyObject *const_str_plain_logging;
extern PyObject *const_dict_b38ee67c68e7540ea798119f786298f3;
static PyObject *const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple;
static PyObject *const_str_digest_e70954d4944ace0df22768dd3f0efd4d;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_plain_baselinefile;
extern PyObject *const_str_plain_report;
extern PyObject *const_str_plain___exit__;
static PyObject *const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple;
extern PyObject *const_str_plain_mpl;
static PyObject *const_str_plain__font_preamble;
static PyObject *const_str_digest_5932a378365063a5c92684feca287d5b;
extern PyObject *const_str_plain___enter__;
static PyObject *const_str_plain_get_basefile;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_command;
extern PyObject *const_str_plain_glob;
extern PyObject *const_str_plain_dvi;
static PyObject *const_tuple_21406069b7d27573eca3b42f76211dc4_tuple;
static PyObject *const_str_digest_78655ce5339fe407fc985b9e53ac5deb;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_104f56ee163f5d38519bdcac501cbdc4;
static PyObject *const_str_digest_e46247e37fbcd9702f3c7e8a8462587a;
static PyObject *const_str_digest_f97abf7a72150bda6101f6034f894d8f;
static PyObject *const_str_plain_grey_arrayd;
extern PyObject *const_str_plain_fromkeys;
static PyObject *const_str_plain_cmr;
extern PyObject *const_str_plain_Transparent;
static PyObject *const_str_plain_pzc;
static PyObject *const_str_digest_c719558650e7b9f8cfadda9ccf4a4599;
extern PyObject *const_str_plain__lock_path;
extern PyObject *const_str_plain_deepcopy;
extern PyObject *const_str_plain_stderr;
extern PyObject *const_str_plain_cbook;
extern PyObject *const_str_plain_r;
static PyObject *const_str_plain__reinit;
static PyObject *const_str_plain_rgba_arrayd;
static PyObject *const_str_digest_ce668e8d3c993b7e8fec3358e49f9674;
extern PyObject *const_str_digest_d92188225719de74d2661c9698316d93;
extern PyObject *const_str_plain_output;
static PyObject *const_tuple_none_none_tuple_int_0_int_0_int_0_tuple_tuple;
static PyObject *const_str_digest_f4eb7fe84e890bc3248e2b3093eec403;
static PyObject *const_dict_ea2002d9fcd546c394f3adbf9d8486c0;
static PyObject *const_tuple_str_plain_cbook_str_plain_dviread_str_plain_rcParams_tuple;
extern PyObject *const_str_plain_unicode_escape;
extern PyObject *const_str_digest_f8c0bb727c0c12a1906ec4a7e8c1e51e;
extern PyObject *const_str_plain_search;
extern PyObject *const_str_digest_19937c07a05eb95bf3c8bd72bc09f99c;
static PyObject *const_str_digest_36a9662c4ab6c494066670b73f711ba4;
static PyObject *const_str_digest_a8836377e124dc254c743ec614df91c2;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_latex;
extern PyObject *const_str_plain_dstack;
static PyObject *const_str_digest_e0e3f24a7bc88427dc66fa01ae4dbc38;
static PyObject *const_list_str_digest_60514009de511c7aafdeb720b2a0e9a0_list;
static PyObject *const_str_digest_6cdd425fa9be1d305df640c6875d0373;
extern PyObject *const_tuple_type_object_tuple;
static PyObject *const_str_plain_fontconfig;
extern PyObject *const_str_plain_exc;
extern PyObject *const_str_plain_width;
static PyObject *const_str_digest_8627bf73710bb4a0fb532b132d20b519;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_4bf30039f375800a1d643e90e574e5ea;
extern PyObject *const_slice_none_none_none;
static PyObject *const_str_digest_bdd73ad7b276f29fb1abd526d3a891e9;
static PyObject *const_str_plain_dvifile;
static PyObject *const_str_digest_971422009bd62b86f8dba405db5419f3;
extern PyObject *const_str_plain_read_png;
extern PyObject *const_str_plain_dvipng;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_5be502270cb646a1dc150e71300691a3_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_5be502270cb646a1dc150e71300691a3_tuple, 0, const_str_digest_f8c0bb727c0c12a1906ec4a7e8c1e51e ); Py_INCREF( const_str_digest_f8c0bb727c0c12a1906ec4a7e8c1e51e );
    PyTuple_SET_ITEM( const_tuple_5be502270cb646a1dc150e71300691a3_tuple, 1, const_str_digest_cdf16115e7b70895f4896e5476e5d9bb ); Py_INCREF( const_str_digest_cdf16115e7b70895f4896e5476e5d9bb );
    PyTuple_SET_ITEM( const_tuple_5be502270cb646a1dc150e71300691a3_tuple, 2, const_str_digest_95b020fe417b0958dcb803463bbe8067 ); Py_INCREF( const_str_digest_95b020fe417b0958dcb803463bbe8067 );
    PyTuple_SET_ITEM( const_tuple_5be502270cb646a1dc150e71300691a3_tuple, 3, const_str_digest_e0f88f933d7ade7a3f12c06b501c8f8c ); Py_INCREF( const_str_digest_e0f88f933d7ade7a3f12c06b501c8f8c );
    const_str_digest_e01874232ccefaf0d37dc214b5be49b0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497306 ], 9, 0 );
    const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 3, const_str_plain_dpi ); Py_INCREF( const_str_plain_dpi );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 4, const_str_plain__png ); Py_INCREF( const_str_plain__png );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 5, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 6, const_str_plain_alpha ); Py_INCREF( const_str_plain_alpha );
    const_str_plain_pngfile = UNSTREAM_STRING_ASCII( &constant_bin[ 2497315 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 7, const_str_plain_pngfile ); Py_INCREF( const_str_plain_pngfile );
    PyTuple_SET_ITEM( const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 8, const_str_plain_X ); Py_INCREF( const_str_plain_X );
    const_str_digest_f5b3e2da1ea2a856c2d58edc746b8ee9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497322 ], 19, 0 );
    const_tuple_str_plain_dvi_str_plain_tex_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dvi_str_plain_tex_tuple, 0, const_str_plain_dvi ); Py_INCREF( const_str_plain_dvi );
    PyTuple_SET_ITEM( const_tuple_str_plain_dvi_str_plain_tex_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    const_tuple_str_plain_self_str_plain_changed_str_plain_k_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_changed_str_plain_k_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_changed_str_plain_k_tuple, 1, const_str_plain_changed ); Py_INCREF( const_str_plain_changed );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_changed_str_plain_k_tuple, 2, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    const_tuple_str_plain_l1_str_plain_dpi_fraction_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_l1_str_plain_dpi_fraction_tuple, 0, const_str_plain_l1 ); Py_INCREF( const_str_plain_l1 );
    const_str_plain_dpi_fraction = UNSTREAM_STRING_ASCII( &constant_bin[ 2497341 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_l1_str_plain_dpi_fraction_tuple, 1, const_str_plain_dpi_fraction ); Py_INCREF( const_str_plain_dpi_fraction );
    const_str_plain_texfile = UNSTREAM_STRING_ASCII( &constant_bin[ 2497353 ], 7, 1 );
    const_tuple_str_plain_cls_str_plain_self_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_self_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_self_tuple, 1, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain__fontconfig = UNSTREAM_STRING_ASCII( &constant_bin[ 2104168 ], 11, 1 );
    const_str_digest_73298c22917b298b198faf9bb5b452f4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497360 ], 15, 0 );
    const_str_digest_9d50db30dcf25d7bb8b27b5444f5991e = UNSTREAM_STRING_ASCII( &constant_bin[ 2497375 ], 19, 0 );
    const_str_digest_a8dc9ad548bfe95b26e3c1ba965ee0a8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497394 ], 19, 0 );
    const_str_plain__run_checked_subprocess = UNSTREAM_STRING_ASCII( &constant_bin[ 2497413 ], 23, 1 );
    const_str_digest_2505020aad631a36514c89cc34a6693f = UNSTREAM_STRING_ASCII( &constant_bin[ 2497436 ], 14, 0 );
    const_str_digest_26938f8f813d1b6246fc351d3a93f5b3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497450 ], 14, 0 );
    const_tuple_str_digest_8627bf73710bb4a0fb532b132d20b519_tuple = PyTuple_New( 1 );
    const_str_digest_8627bf73710bb4a0fb532b132d20b519 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497464 ], 87, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_8627bf73710bb4a0fb532b132d20b519_tuple, 0, const_str_digest_8627bf73710bb4a0fb532b132d20b519 ); Py_INCREF( const_str_digest_8627bf73710bb4a0fb532b132d20b519 );
    const_str_digest_d58ac6888e0c113ae7f41e8955b8f6cf = UNSTREAM_STRING_ASCII( &constant_bin[ 2497551 ], 30, 0 );
    const_str_plain_make_tex = UNSTREAM_STRING_ASCII( &constant_bin[ 2497581 ], 8, 1 );
    const_str_digest_356e9ba5a3f3236c7dfc259adeb0d143 = UNSTREAM_STRING_ASCII( &constant_bin[ 2497589 ], 722, 0 );
    const_str_digest_9b7324678882942a7030b665c2148e26 = UNSTREAM_STRING_ASCII( &constant_bin[ 2498311 ], 18, 0 );
    const_str_plain_texcache = UNSTREAM_STRING_ASCII( &constant_bin[ 2498329 ], 8, 1 );
    const_str_digest_984c8f5dae87f53dab68d11cd420a5d3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2498337 ], 51, 0 );
    const_str_digest_8032213fa23e3682f7bc08f590cd931b = UNSTREAM_STRING_ASCII( &constant_bin[ 2498388 ], 24, 0 );
    const_str_plain_font_info = UNSTREAM_STRING_ASCII( &constant_bin[ 2498412 ], 9, 1 );
    const_str_plain_make_png = UNSTREAM_STRING_ASCII( &constant_bin[ 2497386 ], 8, 1 );
    const_str_plain_custom_preamble = UNSTREAM_STRING_ASCII( &constant_bin[ 2497566 ], 15, 1 );
    const_str_plain_basefile = UNSTREAM_STRING_ASCII( &constant_bin[ 2498421 ], 8, 1 );
    const_str_digest_e686bff235b85bb42d2e91e5388e6e2d = UNSTREAM_STRING_ASCII( &constant_bin[ 2498429 ], 30, 0 );
    const_tuple_str_plain_cmtt_str_empty_tuple = PyTuple_New( 2 );
    const_str_plain_cmtt = UNSTREAM_STRING_ASCII( &constant_bin[ 1410403 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cmtt_str_empty_tuple, 0, const_str_plain_cmtt ); Py_INCREF( const_str_plain_cmtt );
    PyTuple_SET_ITEM( const_tuple_str_plain_cmtt_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_digest_bce359f7716234fe24f45cb0ed5dadcb = UNSTREAM_STRING_ASCII( &constant_bin[ 2498459 ], 26, 0 );
    const_str_plain_preamble_bytes = UNSTREAM_STRING_ASCII( &constant_bin[ 2498485 ], 14, 1 );
    const_str_digest_e7ab4de4b66d3e69ece19096c702273c = UNSTREAM_STRING_ASCII( &constant_bin[ 2498499 ], 6, 0 );
    const_tuple_tuple_str_plain_dvi_str_plain_tex_tuple_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_tuple_str_plain_dvi_str_plain_tex_tuple_tuple, 0, const_tuple_str_plain_dvi_str_plain_tex_tuple ); Py_INCREF( const_tuple_str_plain_dvi_str_plain_tex_tuple );
    const_str_plain_unicode_preamble = UNSTREAM_STRING_ASCII( &constant_bin[ 2498505 ], 16, 1 );
    const_dict_116370359a70a271fa1bb0c2b4992e02 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2498521 ], 611 );
    const_str_plain_get_font_preamble = UNSTREAM_STRING_ASCII( &constant_bin[ 2499132 ], 17, 1 );
    const_str_plain_cmss = UNSTREAM_STRING_ASCII( &constant_bin[ 2210633 ], 4, 1 );
    const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple, 0, const_str_plain_dvi ); Py_INCREF( const_str_plain_dvi );
    PyTuple_SET_ITEM( const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple, 2, const_str_plain_baseline ); Py_INCREF( const_str_plain_baseline );
    const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 3, const_str_plain_basefile ); Py_INCREF( const_str_plain_basefile );
    const_str_plain_dvifile = UNSTREAM_STRING_ASCII( &constant_bin[ 2499149 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 4, const_str_plain_dvifile ); Py_INCREF( const_str_plain_dvifile );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 5, const_str_plain_texfile ); Py_INCREF( const_str_plain_texfile );
    PyTuple_SET_ITEM( const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 6, const_str_plain_fname ); Py_INCREF( const_str_plain_fname );
    const_str_digest_81c020872fc7de5ff77312e7e1dc5fc3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499156 ], 237, 0 );
    const_str_plain__rc_cache_keys = UNSTREAM_STRING_ASCII( &constant_bin[ 2499393 ], 14, 1 );
    const_str_digest_3f89837fd54cf40b4db7d36767a99d84 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499407 ], 40, 0 );
    const_tuple_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple_tuple, 0, const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple ); Py_INCREF( const_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple );
    const_tuple_str_plain_cmss_str_empty_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cmss_str_empty_tuple, 0, const_str_plain_cmss ); Py_INCREF( const_str_plain_cmss );
    PyTuple_SET_ITEM( const_tuple_str_plain_cmss_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_plain__re_vbox = UNSTREAM_STRING_ASCII( &constant_bin[ 2499447 ], 8, 1 );
    const_str_digest_5ccbf34487b50d11e4ba2bd929cdfe91 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499455 ], 27, 0 );
    const_str_plain_fontcmd = UNSTREAM_STRING_ASCII( &constant_bin[ 2499482 ], 7, 1 );
    const_str_digest_2efa96a3fdddfeecd57738931a82445a = UNSTREAM_STRING_ASCII( &constant_bin[ 2499489 ], 21, 0 );
    const_str_digest_2857a7d64c1eed6bf6176547b308f9e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499510 ], 24, 0 );
    const_str_digest_a3aa3a290fd512f365474a19a5c42e93 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499534 ], 66, 0 );
    const_str_plain_get_font_config = UNSTREAM_STRING_ASCII( &constant_bin[ 2499600 ], 15, 1 );
    const_tuple_str_plain_unicode_escape_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_unicode_escape_tuple, 0, const_str_plain_unicode_escape ); Py_INCREF( const_str_plain_unicode_escape );
    const_tuple_slice_none_none_none_slice_none_none_none_int_neg_1_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_slice_none_none_none_slice_none_none_none_int_neg_1_tuple, 0, const_slice_none_none_none ); Py_INCREF( const_slice_none_none_none );
    PyTuple_SET_ITEM( const_tuple_slice_none_none_none_slice_none_none_none_int_neg_1_tuple, 1, const_slice_none_none_none ); Py_INCREF( const_slice_none_none_none );
    PyTuple_SET_ITEM( const_tuple_slice_none_none_none_slice_none_none_none_int_neg_1_tuple, 2, const_int_neg_1 ); Py_INCREF( const_int_neg_1 );
    const_tuple_str_plain_par_str_plain_self_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_par_str_plain_self_tuple, 0, const_str_plain_par ); Py_INCREF( const_str_plain_par );
    PyTuple_SET_ITEM( const_tuple_str_plain_par_str_plain_self_tuple, 1, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_digest_a6ece34cc8b11b0923a994a726b60f2f = UNSTREAM_STRING_ASCII( &constant_bin[ 2499615 ], 34, 0 );
    const_str_plain_font_family_attr = UNSTREAM_STRING_ASCII( &constant_bin[ 2499649 ], 16, 1 );
    const_str_digest_c0052235910e9a252f07af6a9b689c4d = UNSTREAM_STRING_ASCII( &constant_bin[ 2499665 ], 88, 0 );
    const_tuple_str_digest_984c8f5dae87f53dab68d11cd420a5d3_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_984c8f5dae87f53dab68d11cd420a5d3_tuple, 0, const_str_digest_984c8f5dae87f53dab68d11cd420a5d3 ); Py_INCREF( const_str_digest_984c8f5dae87f53dab68d11cd420a5d3 );
    const_tuple_str_digest_2927227e39724474c42d53ec57cf54f3_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_2927227e39724474c42d53ec57cf54f3_tuple, 0, const_str_digest_2927227e39724474c42d53ec57cf54f3 ); Py_INCREF( const_str_digest_2927227e39724474c42d53ec57cf54f3 );
    const_tuple_b21610e59f492cda989c541866b8c37a_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b21610e59f492cda989c541866b8c37a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_b21610e59f492cda989c541866b8c37a_tuple, 1, const_str_plain_command ); Py_INCREF( const_str_plain_command );
    PyTuple_SET_ITEM( const_tuple_b21610e59f492cda989c541866b8c37a_tuple, 2, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_b21610e59f492cda989c541866b8c37a_tuple, 3, const_str_plain_report ); Py_INCREF( const_str_plain_report );
    PyTuple_SET_ITEM( const_tuple_b21610e59f492cda989c541866b8c37a_tuple, 4, const_str_plain_exc ); Py_INCREF( const_str_plain_exc );
    const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 1, const_str_plain_ff ); Py_INCREF( const_str_plain_ff );
    const_str_plain_fontconfig = UNSTREAM_STRING_ASCII( &constant_bin[ 2104169 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 2, const_str_plain_fontconfig ); Py_INCREF( const_str_plain_fontconfig );
    const_str_plain_font_family = UNSTREAM_STRING_ASCII( &constant_bin[ 2104259 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 3, const_str_plain_font_family ); Py_INCREF( const_str_plain_font_family );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 4, const_str_plain_font_family_attr ); Py_INCREF( const_str_plain_font_family_attr );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 5, const_str_plain_font ); Py_INCREF( const_str_plain_font );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 6, const_str_plain_preamble_bytes ); Py_INCREF( const_str_plain_preamble_bytes );
    PyTuple_SET_ITEM( const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 7, const_str_plain_cmd ); Py_INCREF( const_str_plain_cmd );
    const_tuple_str_plain_pzc_str_digest_647a68f81e76836af6331dea2d40b163_tuple = PyTuple_New( 2 );
    const_str_plain_pzc = UNSTREAM_STRING_ASCII( &constant_bin[ 2222167 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pzc_str_digest_647a68f81e76836af6331dea2d40b163_tuple, 0, const_str_plain_pzc ); Py_INCREF( const_str_plain_pzc );
    const_str_digest_647a68f81e76836af6331dea2d40b163 = UNSTREAM_STRING_ASCII( &constant_bin[ 2498734 ], 21, 0 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pzc_str_digest_647a68f81e76836af6331dea2d40b163_tuple, 1, const_str_digest_647a68f81e76836af6331dea2d40b163 ); Py_INCREF( const_str_digest_647a68f81e76836af6331dea2d40b163 );
    const_str_digest_71e8eace702b793e0ab622519bafb6c8 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499753 ], 158, 0 );
    const_str_digest_efd982e9c319b0d2cd1e1a07ed784dab = UNSTREAM_STRING_ASCII( &constant_bin[ 2499911 ], 85, 0 );
    const_str_digest_7f30043b9a2bb91b0e84ad3903ac0a42 = UNSTREAM_STRING_ASCII( &constant_bin[ 2499996 ], 25, 0 );
    const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple, 3, const_str_plain_dpi ); Py_INCREF( const_str_plain_dpi );
    PyTuple_SET_ITEM( const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple, 4, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    const_str_digest_50ef9b639fb224b7409dfaccdceb4fac = UNSTREAM_STRING_ASCII( &constant_bin[ 2500021 ], 226, 0 );
    const_str_plain__rc_cache = UNSTREAM_STRING_ASCII( &constant_bin[ 2499393 ], 9, 1 );
    const_str_digest_2824195fb82d23cb05c0a1f71a0e4d3c = UNSTREAM_STRING_ASCII( &constant_bin[ 2500247 ], 62, 0 );
    const_str_plain_make_dvi_preview = UNSTREAM_STRING_ASCII( &constant_bin[ 2500309 ], 16, 1 );
    const_str_plain_font_families = UNSTREAM_STRING_ASCII( &constant_bin[ 2500325 ], 13, 1 );
    const_str_digest_d4431b0426576a07f19cda03326c253a = UNSTREAM_STRING_ASCII( &constant_bin[ 2500338 ], 27, 0 );
    const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 3, const_str_plain_dpi ); Py_INCREF( const_str_plain_dpi );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 4, const_str_plain_basefile ); Py_INCREF( const_str_plain_basefile );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 5, const_str_plain_pngfile ); Py_INCREF( const_str_plain_pngfile );
    PyTuple_SET_ITEM( const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 6, const_str_plain_dvifile ); Py_INCREF( const_str_plain_dvifile );
    const_str_digest_9054ff3e7037d8914f9ab82584551053 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500365 ], 81, 0 );
    const_str_digest_b038421a02d77f9b5d274e8555649244 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500446 ], 26, 0 );
    const_str_digest_1bebc29034a166ca514e2b054466afb6 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500472 ], 112, 0 );
    const_str_digest_03b8dda3ffebd96a6b130e1cb60f2f27 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500584 ], 45, 0 );
    const_str_plain_cachedir = UNSTREAM_STRING_ASCII( &constant_bin[ 2500629 ], 8, 1 );
    const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 3, const_str_plain_basefile ); Py_INCREF( const_str_plain_basefile );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 4, const_str_plain_texfile ); Py_INCREF( const_str_plain_texfile );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 5, const_str_plain_custom_preamble ); Py_INCREF( const_str_plain_custom_preamble );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 6, const_str_plain_fontcmd ); Py_INCREF( const_str_plain_fontcmd );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 7, const_str_plain_unicode_preamble ); Py_INCREF( const_str_plain_unicode_preamble );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 8, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 9, const_str_plain_fh ); Py_INCREF( const_str_plain_fh );
    PyTuple_SET_ITEM( const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 10, const_str_plain_err ); Py_INCREF( const_str_plain_err );
    const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple, 0, const_str_plain_serif ); Py_INCREF( const_str_plain_serif );
    PyTuple_SET_ITEM( const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple, 1, const_str_digest_19937c07a05eb95bf3c8bd72bc09f99c ); Py_INCREF( const_str_digest_19937c07a05eb95bf3c8bd72bc09f99c );
    PyTuple_SET_ITEM( const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple, 2, const_str_plain_cursive ); Py_INCREF( const_str_plain_cursive );
    PyTuple_SET_ITEM( const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple, 3, const_str_plain_monospace ); Py_INCREF( const_str_plain_monospace );
    const_str_digest_39d9f01d64225c607852a9c6459f5a6e = UNSTREAM_STRING_ASCII( &constant_bin[ 2500637 ], 56, 0 );
    const_str_digest_60514009de511c7aafdeb720b2a0e9a0 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500693 ], 21, 0 );
    const_str_digest_847e8a9ba2def92a0a16c231c8318eed = UNSTREAM_STRING_ASCII( &constant_bin[ 2500714 ], 19, 0 );
    const_str_digest_ef341ce22ae49aada81d48bddaa4ddcf = UNSTREAM_STRING_ASCII( &constant_bin[ 2500733 ], 20, 0 );
    const_str_plain_get_custom_preamble = UNSTREAM_STRING_ASCII( &constant_bin[ 2497562 ], 19, 1 );
    const_str_plain_get_rgba = UNSTREAM_STRING_ASCII( &constant_bin[ 2497405 ], 8, 1 );
    const_tuple_str_plain_cmr_str_empty_tuple = PyTuple_New( 2 );
    const_str_plain_cmr = UNSTREAM_STRING_ASCII( &constant_bin[ 1406938 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cmr_str_empty_tuple, 0, const_str_plain_cmr ); Py_INCREF( const_str_plain_cmr );
    PyTuple_SET_ITEM( const_tuple_str_plain_cmr_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_list_str_digest_6cdd425fa9be1d305df640c6875d0373_list = PyList_New( 1 );
    const_str_digest_6cdd425fa9be1d305df640c6875d0373 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500753 ], 20, 0 );
    PyList_SET_ITEM( const_list_str_digest_6cdd425fa9be1d305df640c6875d0373_list, 0, const_str_digest_6cdd425fa9be1d305df640c6875d0373 ); Py_INCREF( const_str_digest_6cdd425fa9be1d305df640c6875d0373 );
    const_str_digest_626bcc2c1f36a7ec05dcd93c51f76c6d = UNSTREAM_STRING_ASCII( &constant_bin[ 2500773 ], 183, 0 );
    const_str_digest_63dc86e1658320ba5eb6595f23eb3c31 = UNSTREAM_STRING_ASCII( &constant_bin[ 2500956 ], 63, 0 );
    const_str_digest_aa245a491a278bdf2c7c38c3cead3cf7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501019 ], 114, 0 );
    const_str_digest_c396522b4d8d8b219cca9cbe512ef24e = UNSTREAM_STRING_ASCII( &constant_bin[ 2499455 ], 19, 0 );
    const_str_plain_make_tex_preview = UNSTREAM_STRING_ASCII( &constant_bin[ 2499244 ], 16, 1 );
    const_str_digest_8c92326123525d178b015f645b1acaae = UNSTREAM_STRING_ASCII( &constant_bin[ 1385864 ], 2, 0 );
    const_str_digest_94ba037759435a553bb5b8a7861af2ff = UNSTREAM_STRING_ASCII( &constant_bin[ 2501133 ], 11, 0 );
    const_str_digest_3d457918981054c271396c44cb4a3da1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501144 ], 23, 0 );
    const_str_plain_sans_serif = UNSTREAM_STRING_ASCII( &constant_bin[ 2501167 ], 10, 1 );
    const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 3, const_str_plain_renderer ); Py_INCREF( const_str_plain_renderer );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 4, const_str_plain_dpi_fraction ); Py_INCREF( const_str_plain_dpi_fraction );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 5, const_str_plain_basefile ); Py_INCREF( const_str_plain_basefile );
    const_str_plain_baselinefile = UNSTREAM_STRING_ASCII( &constant_bin[ 2501177 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 6, const_str_plain_baselinefile ); Py_INCREF( const_str_plain_baselinefile );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 7, const_str_plain_dvifile ); Py_INCREF( const_str_plain_dvifile );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 8, const_str_plain_fh ); Py_INCREF( const_str_plain_fh );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 9, const_str_plain_l ); Py_INCREF( const_str_plain_l );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 10, const_str_plain_height ); Py_INCREF( const_str_plain_height );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 11, const_str_plain_depth ); Py_INCREF( const_str_plain_depth );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 12, const_str_plain_width ); Py_INCREF( const_str_plain_width );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 13, const_str_plain_dvi ); Py_INCREF( const_str_plain_dvi );
    PyTuple_SET_ITEM( const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 14, const_str_plain_page ); Py_INCREF( const_str_plain_page );
    const_str_digest_e70954d4944ace0df22768dd3f0efd4d = UNSTREAM_STRING_ASCII( &constant_bin[ 2501189 ], 28, 0 );
    const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 3, const_str_plain_basefile ); Py_INCREF( const_str_plain_basefile );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 4, const_str_plain_dvifile ); Py_INCREF( const_str_plain_dvifile );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 5, const_str_plain_baselinefile ); Py_INCREF( const_str_plain_baselinefile );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 6, const_str_plain_texfile ); Py_INCREF( const_str_plain_texfile );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 7, const_str_plain_report ); Py_INCREF( const_str_plain_report );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 8, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 9, const_str_plain_fh ); Py_INCREF( const_str_plain_fh );
    PyTuple_SET_ITEM( const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 10, const_str_plain_fname ); Py_INCREF( const_str_plain_fname );
    const_str_plain__font_preamble = UNSTREAM_STRING_ASCII( &constant_bin[ 2499135 ], 14, 1 );
    const_str_digest_5932a378365063a5c92684feca287d5b = UNSTREAM_STRING_ASCII( &constant_bin[ 2501217 ], 38, 0 );
    const_str_plain_get_basefile = UNSTREAM_STRING_ASCII( &constant_bin[ 2501155 ], 12, 1 );
    const_tuple_21406069b7d27573eca3b42f76211dc4_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 1, const_str_plain_tex ); Py_INCREF( const_str_plain_tex );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 2, const_str_plain_fontsize ); Py_INCREF( const_str_plain_fontsize );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 3, const_str_plain_dpi ); Py_INCREF( const_str_plain_dpi );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 4, const_str_plain_rgb ); Py_INCREF( const_str_plain_rgb );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 5, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 6, const_str_plain_g ); Py_INCREF( const_str_plain_g );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 7, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 8, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 9, const_str_plain_Z ); Py_INCREF( const_str_plain_Z );
    PyTuple_SET_ITEM( const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 10, const_str_plain_alpha ); Py_INCREF( const_str_plain_alpha );
    const_str_digest_78655ce5339fe407fc985b9e53ac5deb = UNSTREAM_STRING_ASCII( &constant_bin[ 2501255 ], 109, 0 );
    const_str_digest_104f56ee163f5d38519bdcac501cbdc4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501364 ], 433, 0 );
    const_str_digest_e46247e37fbcd9702f3c7e8a8462587a = UNSTREAM_STRING_ASCII( &constant_bin[ 2501797 ], 14, 0 );
    const_str_digest_f97abf7a72150bda6101f6034f894d8f = UNSTREAM_STRING_ASCII( &constant_bin[ 2501811 ], 26, 0 );
    const_str_plain_grey_arrayd = UNSTREAM_STRING_ASCII( &constant_bin[ 2501837 ], 11, 1 );
    const_str_digest_c719558650e7b9f8cfadda9ccf4a4599 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501848 ], 28, 0 );
    const_str_plain__reinit = UNSTREAM_STRING_ASCII( &constant_bin[ 2501876 ], 7, 1 );
    const_str_plain_rgba_arrayd = UNSTREAM_STRING_ASCII( &constant_bin[ 2501883 ], 11, 1 );
    const_str_digest_ce668e8d3c993b7e8fec3358e49f9674 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501894 ], 18, 0 );
    const_tuple_none_none_tuple_int_0_int_0_int_0_tuple_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_none_none_tuple_int_0_int_0_int_0_tuple_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_tuple_int_0_int_0_int_0_tuple_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_tuple_int_0_int_0_int_0_tuple_tuple, 2, const_tuple_int_0_int_0_int_0_tuple ); Py_INCREF( const_tuple_int_0_int_0_int_0_tuple );
    const_str_digest_f4eb7fe84e890bc3248e2b3093eec403 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501912 ], 60, 0 );
    const_dict_ea2002d9fcd546c394f3adbf9d8486c0 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_ea2002d9fcd546c394f3adbf9d8486c0, const_str_digest_19937c07a05eb95bf3c8bd72bc09f99c, const_str_digest_2505020aad631a36514c89cc34a6693f );
    const_str_digest_4bf30039f375800a1d643e90e574e5ea = UNSTREAM_STRING_ASCII( &constant_bin[ 2501972 ], 14, 0 );
    PyDict_SetItem( const_dict_ea2002d9fcd546c394f3adbf9d8486c0, const_str_plain_monospace, const_str_digest_4bf30039f375800a1d643e90e574e5ea );
    assert( PyDict_Size( const_dict_ea2002d9fcd546c394f3adbf9d8486c0 ) == 2 );
    const_tuple_str_plain_cbook_str_plain_dviread_str_plain_rcParams_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cbook_str_plain_dviread_str_plain_rcParams_tuple, 0, const_str_plain_cbook ); Py_INCREF( const_str_plain_cbook );
    PyTuple_SET_ITEM( const_tuple_str_plain_cbook_str_plain_dviread_str_plain_rcParams_tuple, 1, const_str_plain_dviread ); Py_INCREF( const_str_plain_dviread );
    PyTuple_SET_ITEM( const_tuple_str_plain_cbook_str_plain_dviread_str_plain_rcParams_tuple, 2, const_str_plain_rcParams ); Py_INCREF( const_str_plain_rcParams );
    const_str_digest_36a9662c4ab6c494066670b73f711ba4 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501986 ], 6, 0 );
    const_str_digest_a8836377e124dc254c743ec614df91c2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2501992 ], 118, 0 );
    const_str_digest_e0e3f24a7bc88427dc66fa01ae4dbc38 = UNSTREAM_STRING_ASCII( &constant_bin[ 2502110 ], 85, 0 );
    const_list_str_digest_60514009de511c7aafdeb720b2a0e9a0_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_digest_60514009de511c7aafdeb720b2a0e9a0_list, 0, const_str_digest_60514009de511c7aafdeb720b2a0e9a0 ); Py_INCREF( const_str_digest_60514009de511c7aafdeb720b2a0e9a0 );
    const_str_digest_bdd73ad7b276f29fb1abd526d3a891e9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2502195 ], 30, 0 );
    const_str_digest_971422009bd62b86f8dba405db5419f3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2502225 ], 6, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$texmanager( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_1ef366a7d2522946669bd5ca67fe8337;
static PyCodeObject *codeobj_ae58b6fc41a0c249b54f756d4ee3aea3;
static PyCodeObject *codeobj_1b0a9b4723c3eac9894ba724d9fc6914;
static PyCodeObject *codeobj_09f11e489fd7ceaa2d038df5e5839b15;
static PyCodeObject *codeobj_458668bb1305aeecb0f754db4460bf6e;
static PyCodeObject *codeobj_be8f5e3a76741c2be18baa713ab03908;
static PyCodeObject *codeobj_82d31c90b2bc44bd8956a140016c659c;
static PyCodeObject *codeobj_dbb6c692af222223e58ef7650e8caadb;
static PyCodeObject *codeobj_2e2900f8e1990fc4a076565895012bd6;
static PyCodeObject *codeobj_95ed78ef04bd33c0f4c97cdc4592a6b5;
static PyCodeObject *codeobj_4ea70b512bc8b1d3be40b3088ed97add;
static PyCodeObject *codeobj_0ab86185c88ea08441b3fc271fd99bc0;
static PyCodeObject *codeobj_3d75e1bc56f161ee4c8854b18e8a19b3;
static PyCodeObject *codeobj_b5aa252c1f16d06c1f569f2ce0538991;
static PyCodeObject *codeobj_ce63371f722df7668d792b93c463f329;
static PyCodeObject *codeobj_cdb8867686ac28d342037e078516a799;
static PyCodeObject *codeobj_afe1565802bff734eb9df1a7ea37adbb;
static PyCodeObject *codeobj_7054fbef09f51b9f42f20d726b61666e;
static PyCodeObject *codeobj_95eb3e49b1f99b994dae5684beda067e;
static PyCodeObject *codeobj_1d5ceb1ed1a260a11fe98e392e5ba5c5;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_8032213fa23e3682f7bc08f590cd931b );
    codeobj_1ef366a7d2522946669bd5ca67fe8337 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 97, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_n_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ae58b6fc41a0c249b54f756d4ee3aea3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 443, const_tuple_str_plain_l1_str_plain_dpi_fraction_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1b0a9b4723c3eac9894ba724d9fc6914 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 168, const_tuple_str_plain_par_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_09f11e489fd7ceaa2d038df5e5839b15 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_bdd73ad7b276f29fb1abd526d3a891e9, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_458668bb1305aeecb0f754db4460bf6e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_TexManager, 49, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_be8f5e3a76741c2be18baa713ab03908 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___new__, 99, const_tuple_str_plain_cls_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_82d31c90b2bc44bd8956a140016c659c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__reinit, 105, const_tuple_965ae09b7bfb18a49204ce84e2206a0c_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dbb6c692af222223e58ef7650e8caadb = MAKE_CODEOBJ( module_filename_obj, const_str_plain__run_checked_subprocess, 299, const_tuple_b21610e59f492cda989c541866b8c37a_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2e2900f8e1990fc4a076565895012bd6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_basefile, 155, const_tuple_d7cacdfdc027a855d4f550b540ba5d41_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_95ed78ef04bd33c0f4c97cdc4592a6b5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_custom_preamble, 188, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4ea70b512bc8b1d3be40b3088ed97add = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_font_config, 164, const_tuple_str_plain_self_str_plain_changed_str_plain_k_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0ab86185c88ea08441b3fc271fd99bc0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_font_preamble, 182, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3d75e1bc56f161ee4c8854b18e8a19b3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_grey, 398, const_tuple_9ca55e263e74be31bc540a6010a59c64_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b5aa252c1f16d06c1f569f2ce0538991 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_rgba, 409, const_tuple_21406069b7d27573eca3b42f76211dc4_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ce63371f722df7668d792b93c463f329 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_text_width_height_descent, 426, const_tuple_22f8d7a4a186e5ea45ef3594974ec827_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cdb8867686ac28d342037e078516a799 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_dvi, 321, const_tuple_5570a48682a6dbbe6fb326b3d485487f_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_afe1565802bff734eb9df1a7ea37adbb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_dvi_preview, 348, const_tuple_9aa12ee9fc7c0420f2ea359b055b55f9_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7054fbef09f51b9f42f20d726b61666e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_png, 382, const_tuple_2f0dc0641865c68baa7281e8186b75e1_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_95eb3e49b1f99b994dae5684beda067e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_tex, 192, const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1d5ceb1ed1a260a11fe98e392e5ba5c5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_tex_preview, 240, const_tuple_b265cd90d9d6c3f0a7fe5e0e75c65631_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *matplotlib$texmanager$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_10_make_dvi(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_11_make_dvi_preview(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_12_make_png(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_13_get_grey( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_14_get_rgba( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_15_get_text_width_height_descent( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_1___new__(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_2__reinit(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_3_get_basefile( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_4_get_font_config(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_5_get_font_preamble(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_6_get_custom_preamble(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_7_make_tex(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_8_make_tex_preview(  );


static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_9__run_checked_subprocess(  );


// The module function definitions.

struct matplotlib$texmanager$$$genexpr_1_genexpr_locals {
    PyObject *var_n;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *matplotlib$texmanager$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$texmanager$$$genexpr_1_genexpr_locals *generator_heap = (struct matplotlib$texmanager$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_n = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_1ef366a7d2522946669bd5ca67fe8337, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 97;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_n;
            generator_heap->var_n = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_n );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_left_name_1 = const_str_digest_80d71dc37939494333d50379f9639d3c;
        CHECK_OBJECT( generator_heap->var_n );
        tmp_right_name_1 = generator_heap->var_n;
        tmp_expression_name_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 97;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 97;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 97;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_n
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_n );
    generator_heap->var_n = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_n );
    generator_heap->var_n = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$texmanager$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$texmanager$$$genexpr_1_genexpr_context,
        module_matplotlib$texmanager,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_ef341ce22ae49aada81d48bddaa4ddcf,
#endif
        codeobj_1ef366a7d2522946669bd5ca67fe8337,
        1,
        sizeof(struct matplotlib$texmanager$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_matplotlib$texmanager$$$function_1___new__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *var_self = NULL;
    struct Nuitka_FrameObject *frame_be8f5e3a76741c2be18baa713ab03908;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_be8f5e3a76741c2be18baa713ab03908 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_be8f5e3a76741c2be18baa713ab03908, codeobj_be8f5e3a76741c2be18baa713ab03908, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *) );
    frame_be8f5e3a76741c2be18baa713ab03908 = cache_frame_be8f5e3a76741c2be18baa713ab03908;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_be8f5e3a76741c2be18baa713ab03908 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_be8f5e3a76741c2be18baa713ab03908 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        tmp_called_instance_1 = (PyObject *)&PyBaseObject_Type;
        CHECK_OBJECT( par_cls );
        tmp_args_element_name_1 = par_cls;
        frame_be8f5e3a76741c2be18baa713ab03908->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain___new__, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_self == NULL );
        var_self = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_self );
        tmp_called_instance_2 = var_self;
        frame_be8f5e3a76741c2be18baa713ab03908->m_frame.f_lineno = 102;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain__reinit );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be8f5e3a76741c2be18baa713ab03908 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_be8f5e3a76741c2be18baa713ab03908 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_be8f5e3a76741c2be18baa713ab03908, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_be8f5e3a76741c2be18baa713ab03908->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_be8f5e3a76741c2be18baa713ab03908, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_be8f5e3a76741c2be18baa713ab03908,
        type_description_1,
        par_cls,
        var_self
    );


    // Release cached frame.
    if ( frame_be8f5e3a76741c2be18baa713ab03908 == cache_frame_be8f5e3a76741c2be18baa713ab03908 )
    {
        Py_DECREF( frame_be8f5e3a76741c2be18baa713ab03908 );
    }
    cache_frame_be8f5e3a76741c2be18baa713ab03908 = NULL;

    assertFrameObject( frame_be8f5e3a76741c2be18baa713ab03908 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_self );
    tmp_return_value = var_self;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_1___new__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_self );
    Py_DECREF( var_self );
    var_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( var_self );
    var_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_1___new__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_2__reinit( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_ff = NULL;
    PyObject *var_fontconfig = NULL;
    PyObject *var_font_family = NULL;
    PyObject *var_font_family_attr = NULL;
    PyObject *var_font = NULL;
    PyObject *var_preamble_bytes = NULL;
    PyObject *var_cmd = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__break_indicator = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    struct Nuitka_FrameObject *frame_82d31c90b2bc44bd8956a140016c659c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_82d31c90b2bc44bd8956a140016c659c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_82d31c90b2bc44bd8956a140016c659c, codeobj_82d31c90b2bc44bd8956a140016c659c, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_82d31c90b2bc44bd8956a140016c659c = cache_frame_82d31c90b2bc44bd8956a140016c659c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_82d31c90b2bc44bd8956a140016c659c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_82d31c90b2bc44bd8956a140016c659c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_texcache );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_a3aa3a290fd512f365474a19a5c42e93;
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 107;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 107;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_Path );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Path );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Path" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_texcache );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_source_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_mkdir );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_b38ee67c68e7540ea798119f786298f3 );
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 110;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_2;
        tmp_subscript_name_1 = const_str_digest_e0f88f933d7ade7a3f12c06b501c8f8c;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_ff == NULL );
        var_ff = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_ff );
        tmp_len_arg_1 = var_ff;
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_ff );
        tmp_subscribed_name_2 = var_ff;
        tmp_subscript_name_2 = const_int_0;
        tmp_called_instance_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 112;
        tmp_compexpr_left_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_lower );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_compexpr_right_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_font_families );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_3 );

            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_ff );
            tmp_subscribed_name_3 = var_ff;
            tmp_subscript_name_3 = const_int_0;
            tmp_called_instance_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 113;
            tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_lower );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_font_family, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_and_left_truth_2;
            nuitka_bool tmp_and_left_value_2;
            nuitka_bool tmp_and_right_value_2;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_ff );
            tmp_isinstance_inst_1 = var_ff;
            tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 114;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            CHECK_OBJECT( var_ff );
            tmp_called_instance_3 = var_ff;
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 114;
            tmp_compexpr_left_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_lower );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 114;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_compexpr_right_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_font_families );
            if ( tmp_compexpr_right_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_4 );

                exception_lineno = 114;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            Py_DECREF( tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 114;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_3 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_condition_result_3 = tmp_and_left_value_2;
            and_end_2:;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assattr_name_2;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_assattr_target_2;
                CHECK_OBJECT( var_ff );
                tmp_called_instance_4 = var_ff;
                frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 115;
                tmp_assattr_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_lower );
                if ( tmp_assattr_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 115;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_assattr_target_2 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_font_family, tmp_assattr_name_2 );
                Py_DECREF( tmp_assattr_name_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 115;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_6;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_7;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_source_name_8;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 117;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_6 = tmp_mvar_value_3;
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_info );
                if ( tmp_called_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_2 = const_str_digest_c0052235910e9a252f07af6a9b689c4d;
                tmp_source_name_7 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_join );
                assert( !(tmp_called_name_4 == NULL) );
                CHECK_OBJECT( par_self );
                tmp_source_name_8 = par_self;
                tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_font_families );
                if ( tmp_args_element_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 119;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 119;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4 };
                    tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_4 );
                if ( tmp_args_element_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_3 );

                    exception_lineno = 119;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 117;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            {
                PyObject *tmp_assattr_name_3;
                PyObject *tmp_assattr_target_3;
                tmp_assattr_name_3 = const_str_plain_serif;
                CHECK_OBJECT( par_self );
                tmp_assattr_target_3 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_font_family, tmp_assattr_name_3 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 120;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_end_3:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_list_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_font_family );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_assign_source_2, 0, tmp_list_element_1 );
        assert( var_fontconfig == NULL );
        var_fontconfig = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( par_self );
        tmp_source_name_10 = par_self;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_font_families );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 123;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_font_family;
            var_font_family = tmp_assign_source_5;
            Py_INCREF( var_font_family );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_instance_5;
        CHECK_OBJECT( var_font_family );
        tmp_called_instance_5 = var_font_family;
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 124;
        tmp_assign_source_6 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_5, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_45_str_plain___tuple, 0 ) );

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_font_family_attr;
            var_font_family_attr = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = Py_False;
        {
            PyObject *old = tmp_for_loop_2__break_indicator;
            tmp_for_loop_2__break_indicator = tmp_assign_source_7;
            Py_INCREF( tmp_for_loop_2__break_indicator );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }

        tmp_subscribed_name_4 = tmp_mvar_value_4;
        tmp_left_name_1 = const_str_digest_80d71dc37939494333d50379f9639d3c;
        CHECK_OBJECT( var_font_family );
        tmp_right_name_1 = var_font_family;
        tmp_subscript_name_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_subscript_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_iter_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
        Py_DECREF( tmp_subscript_name_4 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_value_name_1;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_value_name_1 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_9 = ITERATOR_NEXT( tmp_value_name_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 125;
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        tmp_compexpr_left_5 = exception_keeper_type_1;
        tmp_compexpr_right_5 = PyExc_StopIteration;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            Py_DECREF( exception_keeper_type_1 );
            Py_XDECREF( exception_keeper_value_1 );
            Py_XDECREF( exception_keeper_tb_1 );

            exception_lineno = 125;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = Py_True;
            {
                PyObject *old = tmp_for_loop_2__break_indicator;
                assert( old != NULL );
                tmp_for_loop_2__break_indicator = tmp_assign_source_10;
                Py_INCREF( tmp_for_loop_2__break_indicator );
                Py_DECREF( old );
            }

        }
        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );
        goto loop_end_2;
        goto branch_end_4;
        branch_no_4:;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_4;
        branch_end_4:;
    }
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_11 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_font;
            var_font = tmp_assign_source_11;
            Py_INCREF( var_font );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_source_name_11;
        CHECK_OBJECT( var_font );
        tmp_called_instance_6 = var_font;
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 126;
        tmp_compexpr_left_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_lower );
        if ( tmp_compexpr_left_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_11 = par_self;
        tmp_compexpr_right_6 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_font_info );
        if ( tmp_compexpr_right_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_6 );

            exception_lineno = 126;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        Py_DECREF( tmp_compexpr_right_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_setattr_target_1;
            PyObject *tmp_setattr_attr_1;
            PyObject *tmp_setattr_value_1;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_source_name_12;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_called_instance_7;
            PyObject *tmp_capi_result_1;
            CHECK_OBJECT( par_self );
            tmp_setattr_target_1 = par_self;
            CHECK_OBJECT( var_font_family_attr );
            tmp_setattr_attr_1 = var_font_family_attr;
            CHECK_OBJECT( par_self );
            tmp_source_name_12 = par_self;
            tmp_subscribed_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_font_info );
            if ( tmp_subscribed_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( var_font );
            tmp_called_instance_7 = var_font;
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 128;
            tmp_subscript_name_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_lower );
            if ( tmp_subscript_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscribed_name_5 );

                exception_lineno = 128;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            tmp_setattr_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            Py_DECREF( tmp_subscribed_name_5 );
            Py_DECREF( tmp_subscript_name_5 );
            if ( tmp_setattr_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            tmp_capi_result_1 = BUILTIN_SETATTR( tmp_setattr_target_1, tmp_setattr_attr_1, tmp_setattr_value_1 );
            Py_DECREF( tmp_setattr_value_1 );
            if ( tmp_capi_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 127;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
        }
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_13;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_source_name_14;
            PyObject *tmp_subscript_name_6;
            PyObject *tmp_called_instance_8;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 129;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }

            tmp_source_name_13 = tmp_mvar_value_5;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_debug );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            tmp_args_element_name_5 = const_str_digest_e686bff235b85bb42d2e91e5388e6e2d;
            CHECK_OBJECT( var_font_family );
            tmp_args_element_name_6 = var_font_family;
            CHECK_OBJECT( var_font );
            tmp_args_element_name_7 = var_font;
            CHECK_OBJECT( par_self );
            tmp_source_name_14 = par_self;
            tmp_subscribed_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_font_info );
            if ( tmp_subscribed_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 130;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( var_font );
            tmp_called_instance_8 = var_font;
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 130;
            tmp_subscript_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_lower );
            if ( tmp_subscript_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );
                Py_DECREF( tmp_subscribed_name_6 );

                exception_lineno = 130;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            tmp_args_element_name_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
            Py_DECREF( tmp_subscribed_name_6 );
            Py_DECREF( tmp_subscript_name_6 );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 130;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 129;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        goto loop_end_2;
        goto branch_end_5;
        branch_no_5:;
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_args_element_name_10;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 133;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }

            tmp_called_instance_9 = tmp_mvar_value_6;
            tmp_args_element_name_9 = const_str_digest_5932a378365063a5c92684feca287d5b;
            CHECK_OBJECT( var_font_family );
            tmp_args_element_name_10 = var_font_family;
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 133;
            {
                PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_9, const_str_plain_debug, call_args );
            }

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_end_5:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 125;
        type_description_1 = "oooooooo";
        goto try_except_handler_4;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_6;
        nuitka_bool tmp_compexpr_left_7;
        nuitka_bool tmp_compexpr_right_7;
        int tmp_truth_name_1;
        CHECK_OBJECT( tmp_for_loop_2__break_indicator );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_for_loop_2__break_indicator );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_left_7 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_compexpr_right_7 = NUITKA_BOOL_TRUE;
        tmp_condition_result_6 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_called_instance_10;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_args_element_name_12;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 136;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }

            tmp_called_instance_10 = tmp_mvar_value_7;
            tmp_args_element_name_11 = const_str_digest_9054ff3e7037d8914f9ab82584551053;
            CHECK_OBJECT( var_font_family );
            tmp_args_element_name_12 = var_font_family;
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 136;
            {
                PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
                tmp_call_result_5 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_10, const_str_plain_info, call_args );
            }

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 136;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        {
            PyObject *tmp_setattr_target_2;
            PyObject *tmp_setattr_attr_2;
            PyObject *tmp_setattr_value_2;
            PyObject *tmp_subscribed_name_7;
            PyObject *tmp_source_name_15;
            PyObject *tmp_subscript_name_7;
            PyObject *tmp_capi_result_2;
            CHECK_OBJECT( par_self );
            tmp_setattr_target_2 = par_self;
            CHECK_OBJECT( var_font_family_attr );
            tmp_setattr_attr_2 = var_font_family_attr;
            CHECK_OBJECT( par_self );
            tmp_source_name_15 = par_self;
            tmp_subscribed_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_font_info );
            if ( tmp_subscribed_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_font_family );
            tmp_subscript_name_7 = var_font_family;
            tmp_setattr_value_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
            Py_DECREF( tmp_subscribed_name_7 );
            if ( tmp_setattr_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            tmp_capi_result_2 = BUILTIN_SETATTR( tmp_setattr_target_2, tmp_setattr_attr_2, tmp_setattr_value_2 );
            Py_DECREF( tmp_setattr_value_2 );
            if ( tmp_capi_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
        }
        branch_no_6:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__break_indicator );
    tmp_for_loop_2__break_indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__break_indicator );
    Py_DECREF( tmp_for_loop_2__break_indicator );
    tmp_for_loop_2__break_indicator = NULL;

    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_16;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_subscript_name_8;
        CHECK_OBJECT( var_fontconfig );
        tmp_source_name_16 = var_fontconfig;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_append );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_self );
        tmp_getattr_target_1 = par_self;
        CHECK_OBJECT( var_font_family_attr );
        tmp_getattr_attr_1 = var_font_family_attr;
        tmp_subscribed_name_8 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_subscribed_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 139;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_subscript_name_8 = const_int_0;
        tmp_args_element_name_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 0 );
        Py_DECREF( tmp_subscribed_name_8 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 139;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 139;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 123;
        type_description_1 = "oooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_instance_11;
        PyObject *tmp_called_instance_12;
        CHECK_OBJECT( par_self );
        tmp_called_instance_12 = par_self;
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 143;
        tmp_called_instance_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_get_custom_preamble );
        if ( tmp_called_instance_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 143;
        tmp_assign_source_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_11 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_preamble_bytes == NULL );
        var_preamble_bytes = tmp_assign_source_12;
    }
    {
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_17;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_instance_13;
        PyObject *tmp_called_instance_14;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_15;
        CHECK_OBJECT( var_fontconfig );
        tmp_source_name_17 = var_fontconfig;
        tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_append );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_hashlib );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_hashlib );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "hashlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_14 = tmp_mvar_value_8;
        CHECK_OBJECT( var_preamble_bytes );
        tmp_args_element_name_15 = var_preamble_bytes;
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_called_instance_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_14, const_str_plain_md5, call_args );
        }

        if ( tmp_called_instance_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 144;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 144;
        tmp_args_element_name_14 = CALL_METHOD_NO_ARGS( tmp_called_instance_13, const_str_plain_hexdigest );
        Py_DECREF( tmp_called_instance_13 );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 144;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_called_instance_15;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_assattr_target_4;
        tmp_called_instance_15 = const_str_empty;
        CHECK_OBJECT( var_fontconfig );
        tmp_args_element_name_16 = var_fontconfig;
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 145;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_assattr_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_15, const_str_plain_join, call_args );
        }

        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__fontconfig, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_list_element_2;
        PyObject *tmp_subscribed_name_9;
        PyObject *tmp_source_name_18;
        PyObject *tmp_subscript_name_9;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_source_name_19;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_subscribed_name_11;
        PyObject *tmp_source_name_20;
        PyObject *tmp_subscript_name_11;
        CHECK_OBJECT( par_self );
        tmp_source_name_18 = par_self;
        tmp_subscribed_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_serif );
        if ( tmp_subscribed_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_9 = const_int_pos_1;
        tmp_list_element_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_9, tmp_subscript_name_9, 1 );
        Py_DECREF( tmp_subscribed_name_9 );
        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_assign_source_13, 0, tmp_list_element_2 );
        CHECK_OBJECT( par_self );
        tmp_source_name_19 = par_self;
        tmp_subscribed_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_sans_serif );
        if ( tmp_subscribed_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_13 );

            exception_lineno = 149;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_10 = const_int_pos_1;
        tmp_list_element_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_10, tmp_subscript_name_10, 1 );
        Py_DECREF( tmp_subscribed_name_10 );
        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_13 );

            exception_lineno = 149;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_13, 1, tmp_list_element_2 );
        CHECK_OBJECT( par_self );
        tmp_source_name_20 = par_self;
        tmp_subscribed_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_monospace );
        if ( tmp_subscribed_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_13 );

            exception_lineno = 149;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_11 = const_int_pos_1;
        tmp_list_element_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_11, tmp_subscript_name_11, 1 );
        Py_DECREF( tmp_subscribed_name_11 );
        if ( tmp_list_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_13 );

            exception_lineno = 149;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_13, 2, tmp_list_element_2 );
        assert( var_cmd == NULL );
        var_cmd = tmp_assign_source_13;
    }
    {
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        PyObject *tmp_source_name_21;
        CHECK_OBJECT( par_self );
        tmp_source_name_21 = par_self;
        tmp_compexpr_left_8 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_font_family );
        if ( tmp_compexpr_left_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_8 = const_str_plain_cursive;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
        Py_DECREF( tmp_compexpr_left_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_source_name_22;
            PyObject *tmp_call_result_8;
            PyObject *tmp_args_element_name_17;
            PyObject *tmp_subscribed_name_12;
            PyObject *tmp_source_name_23;
            PyObject *tmp_subscript_name_12;
            CHECK_OBJECT( var_cmd );
            tmp_source_name_22 = var_cmd;
            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_append );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_23 = par_self;
            tmp_subscribed_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_cursive );
            if ( tmp_subscribed_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_8 );

                exception_lineno = 151;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_subscript_name_12 = const_int_pos_1;
            tmp_args_element_name_17 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_12, tmp_subscript_name_12, 1 );
            Py_DECREF( tmp_subscribed_name_12 );
            if ( tmp_args_element_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_8 );

                exception_lineno = 151;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 151;
            {
                PyObject *call_args[] = { tmp_args_element_name_17 };
                tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_17 );
            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_no_7:;
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_24;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_assattr_target_5;
        tmp_source_name_24 = const_str_newline;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_join );
        assert( !(tmp_called_name_9 == NULL) );
        tmp_left_name_3 = LIST_COPY( const_list_str_digest_6cdd425fa9be1d305df640c6875d0373_list );
        CHECK_OBJECT( var_cmd );
        tmp_right_name_2 = var_cmd;
        tmp_left_name_2 = BINARY_OPERATION_ADD_LIST_LIST( tmp_left_name_3, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_3 );
        assert( !(tmp_left_name_2 == NULL) );
        tmp_right_name_3 = LIST_COPY( const_list_str_digest_60514009de511c7aafdeb720b2a0e9a0_list );
        tmp_args_element_name_18 = BINARY_OPERATION_ADD_LIST_LIST( tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_3 );
        assert( !(tmp_args_element_name_18 == NULL) );
        frame_82d31c90b2bc44bd8956a140016c659c->m_frame.f_lineno = 152;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_assattr_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_assattr_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__font_preamble, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_82d31c90b2bc44bd8956a140016c659c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_82d31c90b2bc44bd8956a140016c659c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_82d31c90b2bc44bd8956a140016c659c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_82d31c90b2bc44bd8956a140016c659c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_82d31c90b2bc44bd8956a140016c659c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_82d31c90b2bc44bd8956a140016c659c,
        type_description_1,
        par_self,
        var_ff,
        var_fontconfig,
        var_font_family,
        var_font_family_attr,
        var_font,
        var_preamble_bytes,
        var_cmd
    );


    // Release cached frame.
    if ( frame_82d31c90b2bc44bd8956a140016c659c == cache_frame_82d31c90b2bc44bd8956a140016c659c )
    {
        Py_DECREF( frame_82d31c90b2bc44bd8956a140016c659c );
    }
    cache_frame_82d31c90b2bc44bd8956a140016c659c = NULL;

    assertFrameObject( frame_82d31c90b2bc44bd8956a140016c659c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_2__reinit );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_ff );
    Py_DECREF( var_ff );
    var_ff = NULL;

    CHECK_OBJECT( (PyObject *)var_fontconfig );
    Py_DECREF( var_fontconfig );
    var_fontconfig = NULL;

    Py_XDECREF( var_font_family );
    var_font_family = NULL;

    Py_XDECREF( var_font_family_attr );
    var_font_family_attr = NULL;

    Py_XDECREF( var_font );
    var_font = NULL;

    CHECK_OBJECT( (PyObject *)var_preamble_bytes );
    Py_DECREF( var_preamble_bytes );
    var_preamble_bytes = NULL;

    CHECK_OBJECT( (PyObject *)var_cmd );
    Py_DECREF( var_cmd );
    var_cmd = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_ff );
    var_ff = NULL;

    Py_XDECREF( var_fontconfig );
    var_fontconfig = NULL;

    Py_XDECREF( var_font_family );
    var_font_family = NULL;

    Py_XDECREF( var_font_family_attr );
    var_font_family_attr = NULL;

    Py_XDECREF( var_font );
    var_font = NULL;

    Py_XDECREF( var_preamble_bytes );
    var_preamble_bytes = NULL;

    Py_XDECREF( var_cmd );
    var_cmd = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_2__reinit );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_3_get_basefile( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *par_dpi = python_pars[ 3 ];
    PyObject *var_s = NULL;
    struct Nuitka_FrameObject *frame_2e2900f8e1990fc4a076565895012bd6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_2e2900f8e1990fc4a076565895012bd6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2e2900f8e1990fc4a076565895012bd6, codeobj_2e2900f8e1990fc4a076565895012bd6, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2e2900f8e1990fc4a076565895012bd6 = cache_frame_2e2900f8e1990fc4a076565895012bd6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2e2900f8e1990fc4a076565895012bd6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2e2900f8e1990fc4a076565895012bd6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_unicode_arg_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        tmp_source_name_1 = const_str_empty;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        CHECK_OBJECT( par_tex );
        tmp_list_element_1 = par_tex;
        tmp_args_element_name_1 = PyList_New( 5 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_1, 0, tmp_list_element_1 );
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 159;
        tmp_list_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_font_config );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 159;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_1, 1, tmp_list_element_1 );
        tmp_left_name_1 = const_str_digest_8c92326123525d178b015f645b1acaae;
        CHECK_OBJECT( par_fontsize );
        tmp_right_name_1 = par_fontsize;
        tmp_list_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 159;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_1, 2, tmp_list_element_1 );
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 160;
        tmp_list_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_custom_preamble );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 160;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_1, 3, tmp_list_element_1 );
        CHECK_OBJECT( par_dpi );
        tmp_or_left_value_1 = par_dpi;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 160;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = const_str_empty;
        tmp_unicode_arg_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_unicode_arg_1 = tmp_or_left_value_1;
        or_end_1:;
        tmp_list_element_1 = PyObject_Unicode( tmp_unicode_arg_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 160;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_1, 4, tmp_list_element_1 );
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 159;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_s == NULL );
        var_s = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_instance_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_path );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_join );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_texcache );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_hashlib );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_hashlib );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "hashlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_2;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_md5 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_s );
        tmp_called_instance_4 = var_s;
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 162;
        tmp_args_element_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_called_instance_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 162;
        tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_hexdigest );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 162;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_2e2900f8e1990fc4a076565895012bd6->m_frame.f_lineno = 161;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e2900f8e1990fc4a076565895012bd6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e2900f8e1990fc4a076565895012bd6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e2900f8e1990fc4a076565895012bd6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2e2900f8e1990fc4a076565895012bd6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2e2900f8e1990fc4a076565895012bd6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2e2900f8e1990fc4a076565895012bd6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2e2900f8e1990fc4a076565895012bd6,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        par_dpi,
        var_s
    );


    // Release cached frame.
    if ( frame_2e2900f8e1990fc4a076565895012bd6 == cache_frame_2e2900f8e1990fc4a076565895012bd6 )
    {
        Py_DECREF( frame_2e2900f8e1990fc4a076565895012bd6 );
    }
    cache_frame_2e2900f8e1990fc4a076565895012bd6 = NULL;

    assertFrameObject( frame_2e2900f8e1990fc4a076565895012bd6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_3_get_basefile );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var_s );
    Py_DECREF( var_s );
    var_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_3_get_basefile );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_4_get_font_config( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_changed = NULL;
    PyObject *var_k = NULL;
    PyObject *outline_0_var_par = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_4ea70b512bc8b1d3be40b3088ed97add;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    struct Nuitka_FrameObject *frame_1b0a9b4723c3eac9894ba724d9fc6914_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_1b0a9b4723c3eac9894ba724d9fc6914_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_4ea70b512bc8b1d3be40b3088ed97add = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4ea70b512bc8b1d3be40b3088ed97add, codeobj_4ea70b512bc8b1d3be40b3088ed97add, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4ea70b512bc8b1d3be40b3088ed97add = cache_frame_4ea70b512bc8b1d3be40b3088ed97add;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4ea70b512bc8b1d3be40b3088ed97add );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4ea70b512bc8b1d3be40b3088ed97add ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__rc_cache );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_assattr_target_1;
            tmp_source_name_2 = (PyObject *)&PyDict_Type;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fromkeys );
            assert( !(tmp_called_name_1 == NULL) );
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__rc_cache_keys );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 167;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 167;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__rc_cache, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__rc_cache_keys );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_1b0a9b4723c3eac9894ba724d9fc6914_2, codeobj_1b0a9b4723c3eac9894ba724d9fc6914, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *) );
        frame_1b0a9b4723c3eac9894ba724d9fc6914_2 = cache_frame_1b0a9b4723c3eac9894ba724d9fc6914_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 168;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_par;
                outline_0_var_par = tmp_assign_source_5;
                Py_INCREF( outline_0_var_par );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_subscript_name_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 169;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( outline_0_var_par );
            tmp_subscript_name_1 = outline_0_var_par;
            tmp_compexpr_left_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__rc_cache );
            if ( tmp_subscribed_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_2 );

                exception_lineno = 169;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( outline_0_var_par );
            tmp_subscript_name_2 = outline_0_var_par;
            tmp_compexpr_right_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscribed_name_2 );
            if ( tmp_compexpr_right_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_2 );

                exception_lineno = 169;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 169;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_par );
                tmp_append_value_1 = outline_0_var_par;
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
            }
            branch_no_2:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_4_get_font_config );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_1b0a9b4723c3eac9894ba724d9fc6914_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_1b0a9b4723c3eac9894ba724d9fc6914_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_1b0a9b4723c3eac9894ba724d9fc6914_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_1b0a9b4723c3eac9894ba724d9fc6914_2,
            type_description_2,
            outline_0_var_par,
            par_self
        );


        // Release cached frame.
        if ( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 == cache_frame_1b0a9b4723c3eac9894ba724d9fc6914_2 )
        {
            Py_DECREF( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 );
        }
        cache_frame_1b0a9b4723c3eac9894ba724d9fc6914_2 = NULL;

        assertFrameObject( frame_1b0a9b4723c3eac9894ba724d9fc6914_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_4_get_font_config );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_par );
        outline_0_var_par = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_par );
        outline_0_var_par = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_4_get_font_config );
        return NULL;
        outline_exception_1:;
        exception_lineno = 168;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_changed == NULL );
        var_changed = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_changed );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_changed );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 171;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            tmp_args_element_name_2 = const_str_digest_f97abf7a72150bda6101f6034f894d8f;
            CHECK_OBJECT( var_changed );
            tmp_args_element_name_3 = var_changed;
            frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 171;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_debug, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_2;
            CHECK_OBJECT( var_changed );
            tmp_iter_arg_2 = var_changed;
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_6;
        }
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_2 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooo";
                    exception_lineno = 172;
                    goto try_except_handler_4;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_8 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_k;
                var_k = tmp_assign_source_8;
                Py_INCREF( var_k );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_subscript_name_4;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 173;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_source_name_6 = tmp_mvar_value_3;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_debug );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 173;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            tmp_args_element_name_4 = const_str_digest_2efa96a3fdddfeecd57738931a82445a;
            CHECK_OBJECT( var_k );
            tmp_args_element_name_5 = var_k;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__rc_cache );
            if ( tmp_subscribed_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 174;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( var_k );
            tmp_subscript_name_3 = var_k;
            tmp_args_element_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 174;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_6 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 174;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_subscribed_name_4 = tmp_mvar_value_4;
            CHECK_OBJECT( var_k );
            tmp_subscript_name_4 = var_k;
            tmp_args_element_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            if ( tmp_args_element_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_6 );

                exception_lineno = 174;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 173;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_6 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 173;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_8;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_source_name_9;
            PyObject *tmp_ass_subscript_1;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_copy );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_copy );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "copy" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_source_name_8 = tmp_mvar_value_5;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_deepcopy );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
            }

            if ( tmp_mvar_value_6 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_subscribed_name_5 = tmp_mvar_value_6;
            CHECK_OBJECT( var_k );
            tmp_subscript_name_5 = var_k;
            tmp_args_element_name_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 176;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_ass_subvalue_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_9 = par_self;
            tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__rc_cache );
            if ( tmp_ass_subscribed_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_ass_subvalue_1 );

                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( var_k );
            tmp_ass_subscript_1 = var_k;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscribed_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }
        goto loop_start_2;
        loop_end_2:;
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_10;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_source_name_11;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 177;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_10 = tmp_mvar_value_7;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_debug );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 177;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_9 = const_str_digest_bce359f7716234fe24f45cb0ed5dadcb;
            CHECK_OBJECT( par_self );
            tmp_source_name_11 = par_self;
            tmp_args_element_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__fontconfig );
            if ( tmp_args_element_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 177;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 177;
            {
                PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 177;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_2 = par_self;
            frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 178;
            tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain__reinit );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 178;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_source_name_13;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_8;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_debug );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_11 = const_str_digest_26938f8f813d1b6246fc351d3a93f5b3;
        CHECK_OBJECT( par_self );
        tmp_source_name_13 = par_self;
        tmp_args_element_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain__fontconfig );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 179;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame.f_lineno = 179;
        {
            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 179;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_source_name_14;
        CHECK_OBJECT( par_self );
        tmp_source_name_14 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain__fontconfig );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ea70b512bc8b1d3be40b3088ed97add );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ea70b512bc8b1d3be40b3088ed97add );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ea70b512bc8b1d3be40b3088ed97add );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4ea70b512bc8b1d3be40b3088ed97add, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4ea70b512bc8b1d3be40b3088ed97add->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4ea70b512bc8b1d3be40b3088ed97add, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4ea70b512bc8b1d3be40b3088ed97add,
        type_description_1,
        par_self,
        var_changed,
        var_k
    );


    // Release cached frame.
    if ( frame_4ea70b512bc8b1d3be40b3088ed97add == cache_frame_4ea70b512bc8b1d3be40b3088ed97add )
    {
        Py_DECREF( frame_4ea70b512bc8b1d3be40b3088ed97add );
    }
    cache_frame_4ea70b512bc8b1d3be40b3088ed97add = NULL;

    assertFrameObject( frame_4ea70b512bc8b1d3be40b3088ed97add );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_4_get_font_config );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_changed );
    Py_DECREF( var_changed );
    var_changed = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_changed );
    var_changed = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_4_get_font_config );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_5_get_font_preamble( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0ab86185c88ea08441b3fc271fd99bc0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0ab86185c88ea08441b3fc271fd99bc0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0ab86185c88ea08441b3fc271fd99bc0, codeobj_0ab86185c88ea08441b3fc271fd99bc0, module_matplotlib$texmanager, sizeof(void *) );
    frame_0ab86185c88ea08441b3fc271fd99bc0 = cache_frame_0ab86185c88ea08441b3fc271fd99bc0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0ab86185c88ea08441b3fc271fd99bc0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0ab86185c88ea08441b3fc271fd99bc0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__font_preamble );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ab86185c88ea08441b3fc271fd99bc0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ab86185c88ea08441b3fc271fd99bc0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ab86185c88ea08441b3fc271fd99bc0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0ab86185c88ea08441b3fc271fd99bc0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0ab86185c88ea08441b3fc271fd99bc0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0ab86185c88ea08441b3fc271fd99bc0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0ab86185c88ea08441b3fc271fd99bc0,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_0ab86185c88ea08441b3fc271fd99bc0 == cache_frame_0ab86185c88ea08441b3fc271fd99bc0 )
    {
        Py_DECREF( frame_0ab86185c88ea08441b3fc271fd99bc0 );
    }
    cache_frame_0ab86185c88ea08441b3fc271fd99bc0 = NULL;

    assertFrameObject( frame_0ab86185c88ea08441b3fc271fd99bc0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_5_get_font_preamble );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_5_get_font_preamble );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_6_get_custom_preamble( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_95ed78ef04bd33c0f4c97cdc4592a6b5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_95ed78ef04bd33c0f4c97cdc4592a6b5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_95ed78ef04bd33c0f4c97cdc4592a6b5, codeobj_95ed78ef04bd33c0f4c97cdc4592a6b5, module_matplotlib$texmanager, sizeof(void *) );
    frame_95ed78ef04bd33c0f4c97cdc4592a6b5 = cache_frame_95ed78ef04bd33c0f4c97cdc4592a6b5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 190;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        tmp_subscript_name_1 = const_str_digest_f8c0bb727c0c12a1906ec4a7e8c1e51e;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_95ed78ef04bd33c0f4c97cdc4592a6b5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_95ed78ef04bd33c0f4c97cdc4592a6b5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_95ed78ef04bd33c0f4c97cdc4592a6b5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_95ed78ef04bd33c0f4c97cdc4592a6b5,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 == cache_frame_95ed78ef04bd33c0f4c97cdc4592a6b5 )
    {
        Py_DECREF( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 );
    }
    cache_frame_95ed78ef04bd33c0f4c97cdc4592a6b5 = NULL;

    assertFrameObject( frame_95ed78ef04bd33c0f4c97cdc4592a6b5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_6_get_custom_preamble );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_6_get_custom_preamble );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_7_make_tex( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *var_basefile = NULL;
    PyObject *var_texfile = NULL;
    PyObject *var_custom_preamble = NULL;
    PyObject *var_fontcmd = NULL;
    PyObject *var_unicode_preamble = NULL;
    PyObject *var_s = NULL;
    PyObject *var_fh = NULL;
    PyObject *var_err = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_95eb3e49b1f99b994dae5684beda067e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_95eb3e49b1f99b994dae5684beda067e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_95eb3e49b1f99b994dae5684beda067e, codeobj_95eb3e49b1f99b994dae5684beda067e, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_95eb3e49b1f99b994dae5684beda067e = cache_frame_95eb3e49b1f99b994dae5684beda067e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_95eb3e49b1f99b994dae5684beda067e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_95eb3e49b1f99b994dae5684beda067e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_tex );
        tmp_args_element_name_1 = par_tex;
        CHECK_OBJECT( par_fontsize );
        tmp_args_element_name_2 = par_fontsize;
        frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 198;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get_basefile, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_basefile == NULL );
        var_basefile = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_36a9662c4ab6c494066670b73f711ba4;
        CHECK_OBJECT( var_basefile );
        tmp_right_name_1 = var_basefile;
        tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_texfile == NULL );
        var_texfile = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 200;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_custom_preamble );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_custom_preamble == NULL );
        var_custom_preamble = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_4;
        tmp_source_name_1 = PyDict_Copy( const_dict_ea2002d9fcd546c394f3adbf9d8486c0 );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
        Py_DECREF( tmp_source_name_1 );
        assert( !(tmp_called_name_1 == NULL) );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_font_family );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 202;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = const_str_digest_e46247e37fbcd9702f3c7e8a8462587a;
        frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 201;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_fontcmd == NULL );
        var_fontcmd = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var_fontcmd );
        tmp_left_name_2 = var_fontcmd;
        CHECK_OBJECT( par_tex );
        tmp_right_name_2 = par_tex;
        tmp_assign_source_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_tex;
            assert( old != NULL );
            par_tex = tmp_assign_source_5;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_result_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 206;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        tmp_subscript_name_1 = const_str_digest_cdf16115e7b70895f4896e5476e5d9bb;
        tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_subscript_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_1 );

            exception_lineno = 206;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = const_str_digest_e70954d4944ace0df22768dd3f0efd4d;
            assert( var_unicode_preamble == NULL );
            Py_INCREF( tmp_assign_source_6 );
            var_unicode_preamble = tmp_assign_source_6;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = const_str_empty;
            assert( var_unicode_preamble == NULL );
            Py_INCREF( tmp_assign_source_7 );
            var_unicode_preamble = tmp_assign_source_7;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        tmp_left_name_3 = const_str_digest_626bcc2c1f36a7ec05dcd93c51f76c6d;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__font_preamble );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = PyTuple_New( 6 );
        PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_unicode_preamble );
        tmp_tuple_element_1 = var_unicode_preamble;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_custom_preamble );
        tmp_tuple_element_1 = var_custom_preamble;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_fontsize );
        tmp_tuple_element_1 = par_fontsize;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_fontsize );
        tmp_left_name_4 = par_fontsize;
        tmp_right_name_4 = const_float_1_25;
        tmp_tuple_element_1 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_3 );

            exception_lineno = 223;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_3, 4, tmp_tuple_element_1 );
        CHECK_OBJECT( par_tex );
        tmp_tuple_element_1 = par_tex;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 5, tmp_tuple_element_1 );
        tmp_assign_source_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_s == NULL );
        var_s = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_open_filename_1;
        PyObject *tmp_open_mode_1;
        CHECK_OBJECT( var_texfile );
        tmp_open_filename_1 = var_texfile;
        tmp_open_mode_1 = const_str_plain_wb;
        tmp_assign_source_9 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_4 = tmp_with_1__source;
        tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_4, const_str_plain___enter__ );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 224;
        tmp_assign_source_10 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        Py_DECREF( tmp_called_name_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_5 = tmp_with_1__source;
        tmp_assign_source_11 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___exit__ );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_11;
    }
    {
        nuitka_bool tmp_assign_source_12;
        tmp_assign_source_12 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_13 = tmp_with_1__enter;
        assert( var_fh == NULL );
        Py_INCREF( tmp_assign_source_13 );
        var_fh = tmp_assign_source_13;
    }
    // Tried code:
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_subscript_result_2;
        int tmp_truth_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 225;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_2;
        tmp_subscript_name_2 = const_str_digest_cdf16115e7b70895f4896e5476e5d9bb;
        tmp_subscript_result_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_subscript_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 225;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_subscript_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_2 );

            exception_lineno = 225;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_instance_3;
            CHECK_OBJECT( var_fh );
            tmp_source_name_6 = var_fh;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_write );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( var_s );
            tmp_called_instance_3 = var_s;
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 226;
            tmp_args_element_name_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_utf8_tuple, 0 ) );

            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 226;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 226;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_2;
        branch_no_2:;
        // Tried code:
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( var_fh );
            tmp_source_name_7 = var_fh;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_write );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 229;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_s );
            tmp_called_instance_4 = var_s;
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 229;
            tmp_args_element_name_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_ascii_tuple, 0 ) );

            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 229;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 229;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 229;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_95eb3e49b1f99b994dae5684beda067e, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_95eb3e49b1f99b994dae5684beda067e, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_1 = PyExc_UnicodeEncodeError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 230;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_14;
                tmp_assign_source_14 = EXC_VALUE(PyThreadState_GET());
                assert( var_err == NULL );
                Py_INCREF( tmp_assign_source_14 );
                var_err = tmp_assign_source_14;
            }
            // Tried code:
            {
                PyObject *tmp_called_instance_5;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_call_result_3;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 231;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_7;
                }

                tmp_called_instance_5 = tmp_mvar_value_3;
                frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 231;
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_info, &PyTuple_GET_ITEM( const_tuple_str_digest_8627bf73710bb4a0fb532b132d20b519_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 231;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_7;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 233;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_95eb3e49b1f99b994dae5684beda067e->m_frame) frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_7;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_7_make_tex );
            return NULL;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( var_err );
            var_err = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_6;
            // End of try:
            goto branch_end_3;
            branch_no_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 228;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_95eb3e49b1f99b994dae5684beda067e->m_frame) frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_6;
            branch_end_3:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_7_make_tex );
        return NULL;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // End of try:
        try_end_1:;
        branch_end_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_4 == NULL )
    {
        exception_keeper_tb_4 = MAKE_TRACEBACK( frame_95eb3e49b1f99b994dae5684beda067e, exception_keeper_lineno_4 );
    }
    else if ( exception_keeper_lineno_4 != 0 )
    {
        exception_keeper_tb_4 = ADD_TRACEBACK( exception_keeper_tb_4, frame_95eb3e49b1f99b994dae5684beda067e, exception_keeper_lineno_4 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    PyException_SetTraceback( exception_keeper_value_4, (PyObject *)exception_keeper_tb_4 );
    PUBLISH_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_8;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            nuitka_bool tmp_assign_source_15;
            tmp_assign_source_15 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_15;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            tmp_args_element_name_7 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_8 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_9 = EXC_TRACEBACK(PyThreadState_GET());
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 228;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_8;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 228;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_95eb3e49b1f99b994dae5684beda067e->m_frame) frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_8;
            branch_no_5:;
        }
        goto branch_end_4;
        branch_no_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 224;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_95eb3e49b1f99b994dae5684beda067e->m_frame) frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
        branch_end_4:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_7_make_tex );
    return NULL;
    // End of try:
    try_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_6;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_6 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_6 = tmp_with_1__exit;
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 228;
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_6 );
                Py_XDECREF( exception_keeper_value_6 );
                Py_XDECREF( exception_keeper_tb_6 );

                exception_lineno = 228;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_6:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    {
        nuitka_bool tmp_condition_result_7;
        nuitka_bool tmp_compexpr_left_4;
        nuitka_bool tmp_compexpr_right_4;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_4 = tmp_with_1__indicator;
        tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
        tmp_condition_result_7 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_7 = tmp_with_1__exit;
            frame_95eb3e49b1f99b994dae5684beda067e->m_frame.f_lineno = 228;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 228;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_7:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_95eb3e49b1f99b994dae5684beda067e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_95eb3e49b1f99b994dae5684beda067e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_95eb3e49b1f99b994dae5684beda067e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_95eb3e49b1f99b994dae5684beda067e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_95eb3e49b1f99b994dae5684beda067e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_95eb3e49b1f99b994dae5684beda067e,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        var_basefile,
        var_texfile,
        var_custom_preamble,
        var_fontcmd,
        var_unicode_preamble,
        var_s,
        var_fh,
        var_err
    );


    // Release cached frame.
    if ( frame_95eb3e49b1f99b994dae5684beda067e == cache_frame_95eb3e49b1f99b994dae5684beda067e )
    {
        Py_DECREF( frame_95eb3e49b1f99b994dae5684beda067e );
    }
    cache_frame_95eb3e49b1f99b994dae5684beda067e = NULL;

    assertFrameObject( frame_95eb3e49b1f99b994dae5684beda067e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    CHECK_OBJECT( var_texfile );
    tmp_return_value = var_texfile;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_7_make_tex );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)var_basefile );
    Py_DECREF( var_basefile );
    var_basefile = NULL;

    CHECK_OBJECT( (PyObject *)var_texfile );
    Py_DECREF( var_texfile );
    var_texfile = NULL;

    CHECK_OBJECT( (PyObject *)var_custom_preamble );
    Py_DECREF( var_custom_preamble );
    var_custom_preamble = NULL;

    CHECK_OBJECT( (PyObject *)var_fontcmd );
    Py_DECREF( var_fontcmd );
    var_fontcmd = NULL;

    CHECK_OBJECT( (PyObject *)var_unicode_preamble );
    Py_DECREF( var_unicode_preamble );
    var_unicode_preamble = NULL;

    CHECK_OBJECT( (PyObject *)var_s );
    Py_DECREF( var_s );
    var_s = NULL;

    CHECK_OBJECT( (PyObject *)var_fh );
    Py_DECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_texfile );
    var_texfile = NULL;

    Py_XDECREF( var_custom_preamble );
    var_custom_preamble = NULL;

    Py_XDECREF( var_fontcmd );
    var_fontcmd = NULL;

    Py_XDECREF( var_unicode_preamble );
    var_unicode_preamble = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_7_make_tex );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_8_make_tex_preview( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *var_basefile = NULL;
    PyObject *var_texfile = NULL;
    PyObject *var_custom_preamble = NULL;
    PyObject *var_fontcmd = NULL;
    PyObject *var_unicode_preamble = NULL;
    PyObject *var_s = NULL;
    PyObject *var_fh = NULL;
    PyObject *var_err = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_1d5ceb1ed1a260a11fe98e392e5ba5c5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, codeobj_1d5ceb1ed1a260a11fe98e392e5ba5c5, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 = cache_frame_1d5ceb1ed1a260a11fe98e392e5ba5c5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_tex );
        tmp_args_element_name_1 = par_tex;
        CHECK_OBJECT( par_fontsize );
        tmp_args_element_name_2 = par_fontsize;
        frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 249;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get_basefile, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_basefile == NULL );
        var_basefile = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_36a9662c4ab6c494066670b73f711ba4;
        CHECK_OBJECT( var_basefile );
        tmp_right_name_1 = var_basefile;
        tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_texfile == NULL );
        var_texfile = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 251;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_custom_preamble );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_custom_preamble == NULL );
        var_custom_preamble = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_4;
        tmp_source_name_1 = PyDict_Copy( const_dict_ea2002d9fcd546c394f3adbf9d8486c0 );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
        Py_DECREF( tmp_source_name_1 );
        assert( !(tmp_called_name_1 == NULL) );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_font_family );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 253;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = const_str_digest_e46247e37fbcd9702f3c7e8a8462587a;
        frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 252;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_fontcmd == NULL );
        var_fontcmd = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( var_fontcmd );
        tmp_left_name_2 = var_fontcmd;
        CHECK_OBJECT( par_tex );
        tmp_right_name_2 = par_tex;
        tmp_assign_source_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_tex;
            assert( old != NULL );
            par_tex = tmp_assign_source_5;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_result_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 257;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        tmp_subscript_name_1 = const_str_digest_cdf16115e7b70895f4896e5476e5d9bb;
        tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_subscript_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_1 );

            exception_lineno = 257;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = const_str_digest_e70954d4944ace0df22768dd3f0efd4d;
            assert( var_unicode_preamble == NULL );
            Py_INCREF( tmp_assign_source_6 );
            var_unicode_preamble = tmp_assign_source_6;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = const_str_empty;
            assert( var_unicode_preamble == NULL );
            Py_INCREF( tmp_assign_source_7 );
            var_unicode_preamble = tmp_assign_source_7;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_right_name_4;
        tmp_left_name_3 = const_str_digest_104f56ee163f5d38519bdcac501cbdc4;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__font_preamble );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 284;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = PyTuple_New( 6 );
        PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_unicode_preamble );
        tmp_tuple_element_1 = var_unicode_preamble;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_custom_preamble );
        tmp_tuple_element_1 = var_custom_preamble;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_fontsize );
        tmp_tuple_element_1 = par_fontsize;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_fontsize );
        tmp_left_name_4 = par_fontsize;
        tmp_right_name_4 = const_float_1_25;
        tmp_tuple_element_1 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_4, tmp_right_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_3 );

            exception_lineno = 285;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_3, 4, tmp_tuple_element_1 );
        CHECK_OBJECT( par_tex );
        tmp_tuple_element_1 = par_tex;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 5, tmp_tuple_element_1 );
        tmp_assign_source_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 284;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_s == NULL );
        var_s = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_open_filename_1;
        PyObject *tmp_open_mode_1;
        CHECK_OBJECT( var_texfile );
        tmp_open_filename_1 = var_texfile;
        tmp_open_mode_1 = const_str_plain_wb;
        tmp_assign_source_9 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_4 = tmp_with_1__source;
        tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_4, const_str_plain___enter__ );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 286;
        tmp_assign_source_10 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        Py_DECREF( tmp_called_name_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_5 = tmp_with_1__source;
        tmp_assign_source_11 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___exit__ );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_11;
    }
    {
        nuitka_bool tmp_assign_source_12;
        tmp_assign_source_12 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_13 = tmp_with_1__enter;
        assert( var_fh == NULL );
        Py_INCREF( tmp_assign_source_13 );
        var_fh = tmp_assign_source_13;
    }
    // Tried code:
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_subscript_result_2;
        int tmp_truth_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 287;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }

        tmp_subscribed_name_2 = tmp_mvar_value_2;
        tmp_subscript_name_2 = const_str_digest_cdf16115e7b70895f4896e5476e5d9bb;
        tmp_subscript_result_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_subscript_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_subscript_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_2 );

            exception_lineno = 287;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_instance_3;
            CHECK_OBJECT( var_fh );
            tmp_source_name_6 = var_fh;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_write );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 288;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( var_s );
            tmp_called_instance_3 = var_s;
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 288;
            tmp_args_element_name_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_utf8_tuple, 0 ) );

            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 288;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 288;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 288;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_2;
        branch_no_2:;
        // Tried code:
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_called_instance_4;
            CHECK_OBJECT( var_fh );
            tmp_source_name_7 = var_fh;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_write );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 291;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_s );
            tmp_called_instance_4 = var_s;
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 291;
            tmp_args_element_name_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_ascii_tuple, 0 ) );

            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 291;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 291;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 291;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_1 = PyExc_UnicodeEncodeError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 292;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_14;
                tmp_assign_source_14 = EXC_VALUE(PyThreadState_GET());
                assert( var_err == NULL );
                Py_INCREF( tmp_assign_source_14 );
                var_err = tmp_assign_source_14;
            }
            // Tried code:
            {
                PyObject *tmp_called_instance_5;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_call_result_3;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 293;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_7;
                }

                tmp_called_instance_5 = tmp_mvar_value_3;
                frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 293;
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_info, &PyTuple_GET_ITEM( const_tuple_str_digest_8627bf73710bb4a0fb532b132d20b519_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 293;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_7;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 295;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame) frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_7;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_8_make_tex_preview );
            return NULL;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( var_err );
            var_err = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_6;
            // End of try:
            goto branch_end_3;
            branch_no_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 290;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame) frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_6;
            branch_end_3:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_8_make_tex_preview );
        return NULL;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // End of try:
        try_end_1:;
        branch_end_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_4 == NULL )
    {
        exception_keeper_tb_4 = MAKE_TRACEBACK( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, exception_keeper_lineno_4 );
    }
    else if ( exception_keeper_lineno_4 != 0 )
    {
        exception_keeper_tb_4 = ADD_TRACEBACK( exception_keeper_tb_4, frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, exception_keeper_lineno_4 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    PyException_SetTraceback( exception_keeper_value_4, (PyObject *)exception_keeper_tb_4 );
    PUBLISH_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_8;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            nuitka_bool tmp_assign_source_15;
            tmp_assign_source_15 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_15;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            tmp_args_element_name_7 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_8 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_9 = EXC_TRACEBACK(PyThreadState_GET());
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 290;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 290;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_8;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 290;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 290;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame) frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_8;
            branch_no_5:;
        }
        goto branch_end_4;
        branch_no_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 286;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame) frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
        branch_end_4:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_8_make_tex_preview );
    return NULL;
    // End of try:
    try_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_6;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_6 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_6 = tmp_with_1__exit;
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 290;
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_6 );
                Py_XDECREF( exception_keeper_value_6 );
                Py_XDECREF( exception_keeper_tb_6 );

                exception_lineno = 290;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_6:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    {
        nuitka_bool tmp_condition_result_7;
        nuitka_bool tmp_compexpr_left_4;
        nuitka_bool tmp_compexpr_right_4;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_4 = tmp_with_1__indicator;
        tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
        tmp_condition_result_7 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_7 = tmp_with_1__exit;
            frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame.f_lineno = 290;
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 290;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_7:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1d5ceb1ed1a260a11fe98e392e5ba5c5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1d5ceb1ed1a260a11fe98e392e5ba5c5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1d5ceb1ed1a260a11fe98e392e5ba5c5,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        var_basefile,
        var_texfile,
        var_custom_preamble,
        var_fontcmd,
        var_unicode_preamble,
        var_s,
        var_fh,
        var_err
    );


    // Release cached frame.
    if ( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 == cache_frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 )
    {
        Py_DECREF( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 );
    }
    cache_frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 = NULL;

    assertFrameObject( frame_1d5ceb1ed1a260a11fe98e392e5ba5c5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    CHECK_OBJECT( var_texfile );
    tmp_return_value = var_texfile;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_8_make_tex_preview );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)var_basefile );
    Py_DECREF( var_basefile );
    var_basefile = NULL;

    CHECK_OBJECT( (PyObject *)var_texfile );
    Py_DECREF( var_texfile );
    var_texfile = NULL;

    CHECK_OBJECT( (PyObject *)var_custom_preamble );
    Py_DECREF( var_custom_preamble );
    var_custom_preamble = NULL;

    CHECK_OBJECT( (PyObject *)var_fontcmd );
    Py_DECREF( var_fontcmd );
    var_fontcmd = NULL;

    CHECK_OBJECT( (PyObject *)var_unicode_preamble );
    Py_DECREF( var_unicode_preamble );
    var_unicode_preamble = NULL;

    CHECK_OBJECT( (PyObject *)var_s );
    Py_DECREF( var_s );
    var_s = NULL;

    CHECK_OBJECT( (PyObject *)var_fh );
    Py_DECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_texfile );
    var_texfile = NULL;

    Py_XDECREF( var_custom_preamble );
    var_custom_preamble = NULL;

    Py_XDECREF( var_fontcmd );
    var_fontcmd = NULL;

    Py_XDECREF( var_unicode_preamble );
    var_unicode_preamble = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_err );
    var_err = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_8_make_tex_preview );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_9__run_checked_subprocess( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_command = python_pars[ 1 ];
    PyObject *par_tex = python_pars[ 2 ];
    PyObject *var_report = NULL;
    PyObject *var_exc = NULL;
    struct Nuitka_FrameObject *frame_dbb6c692af222223e58ef7650e8caadb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_dbb6c692af222223e58ef7650e8caadb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dbb6c692af222223e58ef7650e8caadb, codeobj_dbb6c692af222223e58ef7650e8caadb, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_dbb6c692af222223e58ef7650e8caadb = cache_frame_dbb6c692af222223e58ef7650e8caadb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dbb6c692af222223e58ef7650e8caadb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dbb6c692af222223e58ef7650e8caadb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 300;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_command );
        tmp_args_element_name_1 = par_command;
        frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 300;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_debug, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 302;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_check_output );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 302;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_command );
        tmp_tuple_element_1 = par_command;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_cwd;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_texcache );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 303;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_stderr;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 304;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_STDOUT );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 304;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 302;
        tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 302;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        assert( var_report == NULL );
        var_report = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_dbb6c692af222223e58ef7650e8caadb, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_dbb6c692af222223e58ef7650e8caadb, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_FileNotFoundError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 305;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = EXC_VALUE(PyThreadState_GET());
            assert( var_exc == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_exc = tmp_assign_source_2;
        }
        // Tried code:
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_raise_cause_1;
            tmp_source_name_4 = const_str_digest_63dc86e1658320ba5eb6595f23eb3c31;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_format );
            assert( !(tmp_called_name_2 == NULL) );
            CHECK_OBJECT( par_command );
            tmp_subscribed_name_1 = par_command;
            tmp_subscript_name_1 = const_int_0;
            tmp_args_element_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 308;
                type_description_1 = "ooooo";
                goto try_except_handler_4;
            }
            frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 307;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_make_exception_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 307;
                type_description_1 = "ooooo";
                goto try_except_handler_4;
            }
            frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 306;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            CHECK_OBJECT( var_exc );
            tmp_raise_cause_1 = var_exc;
            exception_type = tmp_raise_type_1;
            exception_value = NULL;
            Py_INCREF( tmp_raise_cause_1 );
            exception_lineno = 308;
            RAISE_EXCEPTION_WITH_CAUSE( &exception_type, &exception_value, &exception_tb, tmp_raise_cause_1 );
            type_description_1 = "ooooo";
            goto try_except_handler_4;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_9__run_checked_subprocess );
        return NULL;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_exc );
        var_exc = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_4;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_subprocess );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 309;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_5 = tmp_mvar_value_4;
            tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_CalledProcessError );
            if ( tmp_compexpr_right_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 309;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 309;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                tmp_assign_source_3 = EXC_VALUE(PyThreadState_GET());
                assert( var_exc == NULL );
                Py_INCREF( tmp_assign_source_3 );
                var_exc = tmp_assign_source_3;
            }
            // Tried code:
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_6;
                PyObject *tmp_kw_name_2;
                PyObject *tmp_dict_key_3;
                PyObject *tmp_dict_value_3;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_dict_key_4;
                PyObject *tmp_dict_value_4;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_dict_key_5;
                PyObject *tmp_dict_value_5;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_source_name_7;
                PyObject *tmp_raise_cause_2;
                tmp_source_name_6 = const_str_digest_aa245a491a278bdf2c7c38c3cead3cf7;
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_format );
                assert( !(tmp_called_name_3 == NULL) );
                tmp_dict_key_3 = const_str_plain_prog;
                CHECK_OBJECT( par_command );
                tmp_subscribed_name_2 = par_command;
                tmp_subscript_name_2 = const_int_0;
                tmp_dict_value_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
                if ( tmp_dict_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_3 );

                    exception_lineno = 315;
                    type_description_1 = "ooooo";
                    goto try_except_handler_5;
                }
                tmp_kw_name_2 = _PyDict_NewPresized( 3 );
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
                Py_DECREF( tmp_dict_value_3 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_4 = const_str_plain_tex;
                CHECK_OBJECT( par_tex );
                tmp_called_instance_2 = par_tex;
                frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 316;
                tmp_dict_value_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_plain_unicode_escape_tuple, 0 ) );

                if ( tmp_dict_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_kw_name_2 );

                    exception_lineno = 316;
                    type_description_1 = "ooooo";
                    goto try_except_handler_5;
                }
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
                Py_DECREF( tmp_dict_value_4 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_5 = const_str_plain_exc;
                CHECK_OBJECT( var_exc );
                tmp_source_name_7 = var_exc;
                tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_output );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_kw_name_2 );

                    exception_lineno = 317;
                    type_description_1 = "ooooo";
                    goto try_except_handler_5;
                }
                frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 317;
                tmp_dict_value_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_dict_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_kw_name_2 );

                    exception_lineno = 317;
                    type_description_1 = "ooooo";
                    goto try_except_handler_5;
                }
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_5, tmp_dict_value_5 );
                Py_DECREF( tmp_dict_value_5 );
                assert( !(tmp_res != 0) );
                frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 311;
                tmp_make_exception_arg_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_kw_name_2 );
                if ( tmp_make_exception_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 311;
                    type_description_1 = "ooooo";
                    goto try_except_handler_5;
                }
                frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 310;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_2 );
                assert( !(tmp_raise_type_2 == NULL) );
                CHECK_OBJECT( var_exc );
                tmp_raise_cause_2 = var_exc;
                exception_type = tmp_raise_type_2;
                exception_value = NULL;
                Py_INCREF( tmp_raise_cause_2 );
                exception_lineno = 317;
                RAISE_EXCEPTION_WITH_CAUSE( &exception_type, &exception_value, &exception_tb, tmp_raise_cause_2 );
                type_description_1 = "ooooo";
                goto try_except_handler_5;
            }
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_9__run_checked_subprocess );
            return NULL;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( var_exc );
            var_exc = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto try_except_handler_3;
            // End of try:
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 301;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_dbb6c692af222223e58ef7650e8caadb->m_frame) frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
            branch_end_2:;
        }
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_9__run_checked_subprocess );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__log );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_log" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 318;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_5;
        CHECK_OBJECT( var_report );
        tmp_args_element_name_3 = var_report;
        frame_dbb6c692af222223e58ef7650e8caadb->m_frame.f_lineno = 318;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_debug, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dbb6c692af222223e58ef7650e8caadb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dbb6c692af222223e58ef7650e8caadb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dbb6c692af222223e58ef7650e8caadb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dbb6c692af222223e58ef7650e8caadb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dbb6c692af222223e58ef7650e8caadb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dbb6c692af222223e58ef7650e8caadb,
        type_description_1,
        par_self,
        par_command,
        par_tex,
        var_report,
        var_exc
    );


    // Release cached frame.
    if ( frame_dbb6c692af222223e58ef7650e8caadb == cache_frame_dbb6c692af222223e58ef7650e8caadb )
    {
        Py_DECREF( frame_dbb6c692af222223e58ef7650e8caadb );
    }
    cache_frame_dbb6c692af222223e58ef7650e8caadb = NULL;

    assertFrameObject( frame_dbb6c692af222223e58ef7650e8caadb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_report );
    tmp_return_value = var_report;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_9__run_checked_subprocess );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)var_report );
    Py_DECREF( var_report );
    var_report = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_command );
    Py_DECREF( par_command );
    par_command = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    Py_XDECREF( var_report );
    var_report = NULL;

    Py_XDECREF( var_exc );
    var_exc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_9__run_checked_subprocess );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_10_make_dvi( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *var_basefile = NULL;
    PyObject *var_dvifile = NULL;
    PyObject *var_texfile = NULL;
    PyObject *var_fname = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_cdb8867686ac28d342037e078516a799;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_cdb8867686ac28d342037e078516a799 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cdb8867686ac28d342037e078516a799, codeobj_cdb8867686ac28d342037e078516a799, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cdb8867686ac28d342037e078516a799 = cache_frame_cdb8867686ac28d342037e078516a799;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cdb8867686ac28d342037e078516a799 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cdb8867686ac28d342037e078516a799 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_result_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 328;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        tmp_subscript_name_1 = const_str_digest_95b020fe417b0958dcb803463bbe8067;
        tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_subscript_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_1 );

            exception_lineno = 328;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_called_instance_1 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_1 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_2 = par_fontsize;
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 329;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_make_dvi_preview, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 329;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_tex );
        tmp_args_element_name_3 = par_tex;
        CHECK_OBJECT( par_fontsize );
        tmp_args_element_name_4 = par_fontsize;
        frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 331;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get_basefile, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 331;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_basefile == NULL );
        var_basefile = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_971422009bd62b86f8dba405db5419f3;
        CHECK_OBJECT( var_basefile );
        tmp_right_name_1 = var_basefile;
        tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 332;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_dvifile == NULL );
        var_dvifile = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 333;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_dvifile );
        tmp_args_element_name_5 = var_dvifile;
        frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 333;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_exists, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 333;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( par_self );
            tmp_called_instance_4 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_6 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_7 = par_fontsize;
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 334;
            {
                PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                tmp_assign_source_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_make_tex, call_args );
            }

            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 334;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_texfile == NULL );
            var_texfile = tmp_assign_source_3;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_8;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_cbook );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 335;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }

            tmp_called_instance_5 = tmp_mvar_value_3;
            CHECK_OBJECT( var_texfile );
            tmp_args_element_name_8 = var_texfile;
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 335;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain__lock_path, call_args );
            }

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 335;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__source == NULL );
            tmp_with_1__source = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_2 = tmp_with_1__source;
            tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 335;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 335;
            tmp_assign_source_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 335;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__enter == NULL );
            tmp_with_1__enter = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_3 = tmp_with_1__source;
            tmp_assign_source_6 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 335;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__exit == NULL );
            tmp_with_1__exit = tmp_assign_source_6;
        }
        {
            nuitka_bool tmp_assign_source_7;
            tmp_assign_source_7 = NUITKA_BOOL_TRUE;
            tmp_with_1__indicator = tmp_assign_source_7;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_called_instance_6;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_list_element_1;
            PyObject *tmp_args_element_name_10;
            CHECK_OBJECT( par_self );
            tmp_called_instance_6 = par_self;
            tmp_list_element_1 = const_str_plain_latex;
            tmp_args_element_name_9 = PyList_New( 4 );
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_9, 0, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_2857a7d64c1eed6bf6176547b308f9e2;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_9, 1, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_73298c22917b298b198faf9bb5b452f4;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_9, 2, tmp_list_element_1 );
            CHECK_OBJECT( var_texfile );
            tmp_list_element_1 = var_texfile;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_9, 3, tmp_list_element_1 );
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_10 = par_tex;
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 336;
            {
                PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain__run_checked_subprocess, call_args );
            }

            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 336;
                type_description_1 = "ooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_cdb8867686ac28d342037e078516a799, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_cdb8867686ac28d342037e078516a799, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_1 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 335;
                type_description_1 = "ooooooo";
                goto try_except_handler_5;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                nuitka_bool tmp_assign_source_8;
                tmp_assign_source_8 = NUITKA_BOOL_FALSE;
                tmp_with_1__indicator = tmp_assign_source_8;
            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_element_name_11;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_args_element_name_13;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_2 = tmp_with_1__exit;
                tmp_args_element_name_11 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_12 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_13 = EXC_TRACEBACK(PyThreadState_GET());
                frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 338;
                {
                    PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13 };
                    tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
                }

                if ( tmp_operand_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 338;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_5;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                Py_DECREF( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 338;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_5;
                }
                tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 338;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_cdb8867686ac28d342037e078516a799->m_frame) frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooo";
                goto try_except_handler_5;
                branch_no_4:;
            }
            goto branch_end_3;
            branch_no_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 335;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_cdb8867686ac28d342037e078516a799->m_frame) frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooo";
            goto try_except_handler_5;
            branch_end_3:;
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_1;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_10_make_dvi );
        return NULL;
        // End of try:
        try_end_1:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_5;
            nuitka_bool tmp_compexpr_left_2;
            nuitka_bool tmp_compexpr_right_2;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_2 = tmp_with_1__indicator;
            tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
            tmp_condition_result_5 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_name_3;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_3 = tmp_with_1__exit;
                frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 338;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_3 );
                    Py_XDECREF( exception_keeper_value_3 );
                    Py_XDECREF( exception_keeper_tb_3 );

                    exception_lineno = 338;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_5:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_2;
        // End of try:
        try_end_3:;
        {
            nuitka_bool tmp_condition_result_6;
            nuitka_bool tmp_compexpr_left_3;
            nuitka_bool tmp_compexpr_right_3;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_3 = tmp_with_1__indicator;
            tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
            tmp_condition_result_6 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_4 = tmp_with_1__exit;
                frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 338;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 338;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_6:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_14;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_glob );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_glob );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "glob" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 339;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_4;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_glob );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 339;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_basefile );
            tmp_left_name_2 = var_basefile;
            tmp_right_name_2 = const_str_chr_42;
            tmp_args_element_name_14 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_2 );
            if ( tmp_args_element_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 339;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 339;
            {
                PyObject *call_args[] = { tmp_args_element_name_14 };
                tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_14 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 339;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 339;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_9;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooo";
                    exception_lineno = 339;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_11;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_11 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_fname;
                var_fname = tmp_assign_source_11;
                Py_INCREF( var_fname );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_called_instance_7;
            CHECK_OBJECT( var_fname );
            tmp_called_instance_7 = var_fname;
            frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 340;
            tmp_operand_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_tuple_str_plain_dvi_str_plain_tex_tuple_tuple, 0 ) );

            if ( tmp_operand_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 340;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            Py_DECREF( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 340;
                type_description_1 = "ooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            // Tried code:
            {
                PyObject *tmp_called_instance_8;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_call_result_4;
                PyObject *tmp_args_element_name_15;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 342;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_7;
                }

                tmp_called_instance_8 = tmp_mvar_value_5;
                CHECK_OBJECT( var_fname );
                tmp_args_element_name_15 = var_fname;
                frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = 342;
                {
                    PyObject *call_args[] = { tmp_args_element_name_15 };
                    tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_remove, call_args );
                }

                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 342;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_7;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            goto try_end_5;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_2 );
            exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_2 );
            exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_2 );

            if ( exception_keeper_tb_5 == NULL )
            {
                exception_keeper_tb_5 = MAKE_TRACEBACK( frame_cdb8867686ac28d342037e078516a799, exception_keeper_lineno_5 );
            }
            else if ( exception_keeper_lineno_5 != 0 )
            {
                exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_cdb8867686ac28d342037e078516a799, exception_keeper_lineno_5 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
            PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
            PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_operand_name_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_4 = PyExc_OSError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 343;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_8;
                }
                tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 343;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_8;
                }
                tmp_condition_result_8 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 341;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_cdb8867686ac28d342037e078516a799->m_frame) frame_cdb8867686ac28d342037e078516a799->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooo";
                goto try_except_handler_8;
                branch_no_8:;
            }
            goto try_end_6;
            // Exception handler code:
            try_except_handler_8:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto try_except_handler_6;
            // End of try:
            try_end_6:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
            goto try_end_5;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_10_make_dvi );
            return NULL;
            // End of try:
            try_end_5:;
            branch_no_7:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 339;
            type_description_1 = "ooooooo";
            goto try_except_handler_6;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_7;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_1;
        // End of try:
        try_end_7:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cdb8867686ac28d342037e078516a799 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cdb8867686ac28d342037e078516a799 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cdb8867686ac28d342037e078516a799 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cdb8867686ac28d342037e078516a799, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cdb8867686ac28d342037e078516a799->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cdb8867686ac28d342037e078516a799, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cdb8867686ac28d342037e078516a799,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        var_basefile,
        var_dvifile,
        var_texfile,
        var_fname
    );


    // Release cached frame.
    if ( frame_cdb8867686ac28d342037e078516a799 == cache_frame_cdb8867686ac28d342037e078516a799 )
    {
        Py_DECREF( frame_cdb8867686ac28d342037e078516a799 );
    }
    cache_frame_cdb8867686ac28d342037e078516a799 = NULL;

    assertFrameObject( frame_cdb8867686ac28d342037e078516a799 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_dvifile );
    tmp_return_value = var_dvifile;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_10_make_dvi );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    Py_XDECREF( var_texfile );
    var_texfile = NULL;

    Py_XDECREF( var_fname );
    var_fname = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    Py_XDECREF( var_texfile );
    var_texfile = NULL;

    Py_XDECREF( var_fname );
    var_fname = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_10_make_dvi );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_11_make_dvi_preview( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *var_basefile = NULL;
    PyObject *var_dvifile = NULL;
    PyObject *var_baselinefile = NULL;
    PyObject *var_texfile = NULL;
    PyObject *var_report = NULL;
    PyObject *var_m = NULL;
    PyObject *var_fh = NULL;
    PyObject *var_fname = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_afe1565802bff734eb9df1a7ea37adbb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_afe1565802bff734eb9df1a7ea37adbb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_afe1565802bff734eb9df1a7ea37adbb, codeobj_afe1565802bff734eb9df1a7ea37adbb, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_afe1565802bff734eb9df1a7ea37adbb = cache_frame_afe1565802bff734eb9df1a7ea37adbb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_afe1565802bff734eb9df1a7ea37adbb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_afe1565802bff734eb9df1a7ea37adbb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_tex );
        tmp_args_element_name_1 = par_tex;
        CHECK_OBJECT( par_fontsize );
        tmp_args_element_name_2 = par_fontsize;
        frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 357;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get_basefile, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_basefile == NULL );
        var_basefile = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_971422009bd62b86f8dba405db5419f3;
        CHECK_OBJECT( var_basefile );
        tmp_right_name_1 = var_basefile;
        tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_dvifile == NULL );
        var_dvifile = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_str_digest_94ba037759435a553bb5b8a7861af2ff;
        CHECK_OBJECT( var_basefile );
        tmp_right_name_2 = var_basefile;
        tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_baselinefile == NULL );
        var_baselinefile = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_dvifile );
        tmp_args_element_name_3 = var_dvifile;
        frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 361;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_exists, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_baselinefile );
        tmp_args_element_name_4 = var_baselinefile;
        frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 361;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_exists, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( par_self );
            tmp_called_instance_4 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_5 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_6 = par_fontsize;
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 362;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_make_tex_preview, call_args );
            }

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 362;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_texfile == NULL );
            var_texfile = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_list_element_1;
            PyObject *tmp_args_element_name_8;
            CHECK_OBJECT( par_self );
            tmp_called_instance_5 = par_self;
            tmp_list_element_1 = const_str_plain_latex;
            tmp_args_element_name_7 = PyList_New( 4 );
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 0, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_2857a7d64c1eed6bf6176547b308f9e2;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 1, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_73298c22917b298b198faf9bb5b452f4;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 2, tmp_list_element_1 );
            CHECK_OBJECT( var_texfile );
            tmp_list_element_1 = var_texfile;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 3, tmp_list_element_1 );
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_8 = par_tex;
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 363;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_assign_source_5 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_5, const_str_plain__run_checked_subprocess, call_args );
            }

            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 363;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_report == NULL );
            var_report = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_called_instance_6;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_TexManager );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TexManager );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TexManager" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 369;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_3;
            tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__re_vbox );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 369;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_search );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 369;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_report );
            tmp_called_instance_6 = var_report;
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 369;
            tmp_args_element_name_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

            if ( tmp_args_element_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 369;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 369;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 369;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_m == NULL );
            var_m = tmp_assign_source_6;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_open_filename_1;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_open_mode_1;
            CHECK_OBJECT( var_basefile );
            tmp_left_name_3 = var_basefile;
            tmp_right_name_3 = const_str_digest_d92188225719de74d2661c9698316d93;
            tmp_open_filename_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_open_filename_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 370;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            tmp_open_mode_1 = const_str_plain_w;
            tmp_assign_source_7 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
            Py_DECREF( tmp_open_filename_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 370;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__source == NULL );
            tmp_with_1__source = tmp_assign_source_7;
        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_5 = tmp_with_1__source;
            tmp_called_name_2 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___enter__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 370;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 370;
            tmp_assign_source_8 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 370;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__enter == NULL );
            tmp_with_1__enter = tmp_assign_source_8;
        }
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_6 = tmp_with_1__source;
            tmp_assign_source_9 = LOOKUP_SPECIAL( tmp_source_name_6, const_str_plain___exit__ );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 370;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__exit == NULL );
            tmp_with_1__exit = tmp_assign_source_9;
        }
        {
            nuitka_bool tmp_assign_source_10;
            tmp_assign_source_10 = NUITKA_BOOL_TRUE;
            tmp_with_1__indicator = tmp_assign_source_10;
        }
        {
            PyObject *tmp_assign_source_11;
            CHECK_OBJECT( tmp_with_1__enter );
            tmp_assign_source_11 = tmp_with_1__enter;
            assert( var_fh == NULL );
            Py_INCREF( tmp_assign_source_11 );
            var_fh = tmp_assign_source_11;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_8;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_called_instance_7;
            CHECK_OBJECT( var_fh );
            tmp_source_name_7 = var_fh;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_write );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 371;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            tmp_source_name_8 = const_str_space;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_join );
            assert( !(tmp_called_name_4 == NULL) );
            CHECK_OBJECT( var_m );
            tmp_called_instance_7 = var_m;
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 371;
            tmp_args_element_name_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_groups );
            if ( tmp_args_element_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 371;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 371;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_args_element_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 371;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 371;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 371;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_afe1565802bff734eb9df1a7ea37adbb, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_afe1565802bff734eb9df1a7ea37adbb, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_1 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 370;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                nuitka_bool tmp_assign_source_12;
                tmp_assign_source_12 = NUITKA_BOOL_FALSE;
                tmp_with_1__indicator = tmp_assign_source_12;
            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_operand_name_3;
                PyObject *tmp_called_name_5;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_args_element_name_13;
                PyObject *tmp_args_element_name_14;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_5 = tmp_with_1__exit;
                tmp_args_element_name_12 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_13 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_14 = EXC_TRACEBACK(PyThreadState_GET());
                frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 371;
                {
                    PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
                    tmp_operand_name_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
                }

                if ( tmp_operand_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 371;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_5;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                Py_DECREF( tmp_operand_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 371;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_5;
                }
                tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 371;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_afe1565802bff734eb9df1a7ea37adbb->m_frame) frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_5;
                branch_no_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 370;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_afe1565802bff734eb9df1a7ea37adbb->m_frame) frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_5;
            branch_end_2:;
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_1;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_11_make_dvi_preview );
        return NULL;
        // End of try:
        try_end_1:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_4;
            nuitka_bool tmp_compexpr_left_2;
            nuitka_bool tmp_compexpr_right_2;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_2 = tmp_with_1__indicator;
            tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
            tmp_condition_result_4 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_6;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_6 = tmp_with_1__exit;
                frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 371;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_3 );
                    Py_XDECREF( exception_keeper_value_3 );
                    Py_XDECREF( exception_keeper_tb_3 );

                    exception_lineno = 371;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_4:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_2;
        // End of try:
        try_end_3:;
        {
            nuitka_bool tmp_condition_result_5;
            nuitka_bool tmp_compexpr_left_3;
            nuitka_bool tmp_compexpr_right_3;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_3 = tmp_with_1__indicator;
            tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
            tmp_condition_result_5 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_name_7;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_7 = tmp_with_1__exit;
                frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 371;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 371;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_5:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_name_8;
            PyObject *tmp_source_name_9;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_glob );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_glob );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "glob" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 373;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_9 = tmp_mvar_value_4;
            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_glob );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 373;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_basefile );
            tmp_left_name_4 = var_basefile;
            tmp_right_name_4 = const_str_chr_42;
            tmp_args_element_name_15 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_4, tmp_right_name_4 );
            if ( tmp_args_element_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_8 );

                exception_lineno = 373;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 373;
            {
                PyObject *call_args[] = { tmp_args_element_name_15 };
                tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 373;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 373;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_13;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_14;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_14 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooooo";
                    exception_lineno = 373;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_14;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_15 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_fname;
                var_fname = tmp_assign_source_15;
                Py_INCREF( var_fname );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_called_instance_8;
            CHECK_OBJECT( var_fname );
            tmp_called_instance_8 = var_fname;
            frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 374;
            tmp_operand_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_tuple_str_plain_dvi_str_plain_tex_str_plain_baseline_tuple_tuple, 0 ) );

            if ( tmp_operand_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_6;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            Py_DECREF( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            // Tried code:
            {
                PyObject *tmp_called_instance_9;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_call_result_4;
                PyObject *tmp_args_element_name_16;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 376;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_7;
                }

                tmp_called_instance_9 = tmp_mvar_value_5;
                CHECK_OBJECT( var_fname );
                tmp_args_element_name_16 = var_fname;
                frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = 376;
                {
                    PyObject *call_args[] = { tmp_args_element_name_16 };
                    tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_remove, call_args );
                }

                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 376;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_7;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            goto try_end_5;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_2 );
            exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_2 );
            exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_2 );

            if ( exception_keeper_tb_5 == NULL )
            {
                exception_keeper_tb_5 = MAKE_TRACEBACK( frame_afe1565802bff734eb9df1a7ea37adbb, exception_keeper_lineno_5 );
            }
            else if ( exception_keeper_lineno_5 != 0 )
            {
                exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_afe1565802bff734eb9df1a7ea37adbb, exception_keeper_lineno_5 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
            PyException_SetTraceback( exception_keeper_value_5, (PyObject *)exception_keeper_tb_5 );
            PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_operand_name_5;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_4 = PyExc_OSError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 377;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_8;
                }
                tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 377;
                    type_description_1 = "ooooooooooo";
                    goto try_except_handler_8;
                }
                tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 375;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_afe1565802bff734eb9df1a7ea37adbb->m_frame) frame_afe1565802bff734eb9df1a7ea37adbb->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooooo";
                goto try_except_handler_8;
                branch_no_7:;
            }
            goto try_end_6;
            // Exception handler code:
            try_except_handler_8:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto try_except_handler_6;
            // End of try:
            try_end_6:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
            goto try_end_5;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_11_make_dvi_preview );
            return NULL;
            // End of try:
            try_end_5:;
            branch_no_6:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 373;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_6;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_7;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_1;
        // End of try:
        try_end_7:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_afe1565802bff734eb9df1a7ea37adbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_afe1565802bff734eb9df1a7ea37adbb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_afe1565802bff734eb9df1a7ea37adbb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_afe1565802bff734eb9df1a7ea37adbb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_afe1565802bff734eb9df1a7ea37adbb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_afe1565802bff734eb9df1a7ea37adbb,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        var_basefile,
        var_dvifile,
        var_baselinefile,
        var_texfile,
        var_report,
        var_m,
        var_fh,
        var_fname
    );


    // Release cached frame.
    if ( frame_afe1565802bff734eb9df1a7ea37adbb == cache_frame_afe1565802bff734eb9df1a7ea37adbb )
    {
        Py_DECREF( frame_afe1565802bff734eb9df1a7ea37adbb );
    }
    cache_frame_afe1565802bff734eb9df1a7ea37adbb = NULL;

    assertFrameObject( frame_afe1565802bff734eb9df1a7ea37adbb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_dvifile );
    tmp_return_value = var_dvifile;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_11_make_dvi_preview );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)var_basefile );
    Py_DECREF( var_basefile );
    var_basefile = NULL;

    CHECK_OBJECT( (PyObject *)var_dvifile );
    Py_DECREF( var_dvifile );
    var_dvifile = NULL;

    CHECK_OBJECT( (PyObject *)var_baselinefile );
    Py_DECREF( var_baselinefile );
    var_baselinefile = NULL;

    Py_XDECREF( var_texfile );
    var_texfile = NULL;

    Py_XDECREF( var_report );
    var_report = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_fname );
    var_fname = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    Py_XDECREF( var_baselinefile );
    var_baselinefile = NULL;

    Py_XDECREF( var_texfile );
    var_texfile = NULL;

    Py_XDECREF( var_report );
    var_report = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_fname );
    var_fname = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_11_make_dvi_preview );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_12_make_png( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *par_dpi = python_pars[ 3 ];
    PyObject *var_basefile = NULL;
    PyObject *var_pngfile = NULL;
    PyObject *var_dvifile = NULL;
    struct Nuitka_FrameObject *frame_7054fbef09f51b9f42f20d726b61666e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_7054fbef09f51b9f42f20d726b61666e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7054fbef09f51b9f42f20d726b61666e, codeobj_7054fbef09f51b9f42f20d726b61666e, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7054fbef09f51b9f42f20d726b61666e = cache_frame_7054fbef09f51b9f42f20d726b61666e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7054fbef09f51b9f42f20d726b61666e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7054fbef09f51b9f42f20d726b61666e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_tex );
        tmp_args_element_name_1 = par_tex;
        CHECK_OBJECT( par_fontsize );
        tmp_args_element_name_2 = par_fontsize;
        CHECK_OBJECT( par_dpi );
        tmp_args_element_name_3 = par_dpi;
        frame_7054fbef09f51b9f42f20d726b61666e->m_frame.f_lineno = 388;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_get_basefile, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_basefile == NULL );
        var_basefile = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_digest_e7ab4de4b66d3e69ece19096c702273c;
        CHECK_OBJECT( var_basefile );
        tmp_right_name_1 = var_basefile;
        tmp_assign_source_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 389;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_pngfile == NULL );
        var_pngfile = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 391;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_pngfile );
        tmp_args_element_name_4 = var_pngfile;
        frame_7054fbef09f51b9f42f20d726b61666e->m_frame.f_lineno = 391;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_exists, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( par_self );
            tmp_called_instance_3 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_5 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_6 = par_fontsize;
            frame_7054fbef09f51b9f42f20d726b61666e->m_frame.f_lineno = 392;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_assign_source_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_make_dvi, call_args );
            }

            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 392;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_dvifile == NULL );
            var_dvifile = tmp_assign_source_3;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_list_element_1;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_args_element_name_8;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__run_checked_subprocess );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 393;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_list_element_1 = const_str_plain_dvipng;
            tmp_args_element_name_7 = PyList_New( 10 );
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 0, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_b26d3cd4cb167eb4277cbd83e0c608a6;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 1, tmp_list_element_1 );
            tmp_list_element_1 = const_str_plain_Transparent;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 2, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 3, tmp_list_element_1 );
            CHECK_OBJECT( par_dpi );
            tmp_unicode_arg_1 = par_dpi;
            tmp_list_element_1 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_7 );

                exception_lineno = 394;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            PyList_SET_ITEM( tmp_args_element_name_7, 4, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_ba1d2497b5193e6a3b8923fd050980bc;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 5, tmp_list_element_1 );
            tmp_list_element_1 = const_str_plain_tight;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 6, tmp_list_element_1 );
            tmp_list_element_1 = const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 7, tmp_list_element_1 );
            CHECK_OBJECT( var_pngfile );
            tmp_list_element_1 = var_pngfile;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 8, tmp_list_element_1 );
            CHECK_OBJECT( var_dvifile );
            tmp_list_element_1 = var_dvifile;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_7, 9, tmp_list_element_1 );
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_8 = par_tex;
            frame_7054fbef09f51b9f42f20d726b61666e->m_frame.f_lineno = 393;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 393;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7054fbef09f51b9f42f20d726b61666e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7054fbef09f51b9f42f20d726b61666e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7054fbef09f51b9f42f20d726b61666e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7054fbef09f51b9f42f20d726b61666e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7054fbef09f51b9f42f20d726b61666e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7054fbef09f51b9f42f20d726b61666e,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        par_dpi,
        var_basefile,
        var_pngfile,
        var_dvifile
    );


    // Release cached frame.
    if ( frame_7054fbef09f51b9f42f20d726b61666e == cache_frame_7054fbef09f51b9f42f20d726b61666e )
    {
        Py_DECREF( frame_7054fbef09f51b9f42f20d726b61666e );
    }
    cache_frame_7054fbef09f51b9f42f20d726b61666e = NULL;

    assertFrameObject( frame_7054fbef09f51b9f42f20d726b61666e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_pngfile );
    tmp_return_value = var_pngfile;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_12_make_png );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var_basefile );
    Py_DECREF( var_basefile );
    var_basefile = NULL;

    CHECK_OBJECT( (PyObject *)var_pngfile );
    Py_DECREF( var_pngfile );
    var_pngfile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_pngfile );
    var_pngfile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_12_make_png );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_13_get_grey( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *par_dpi = python_pars[ 3 ];
    PyObject *var__png = NULL;
    PyObject *var_key = NULL;
    PyObject *var_alpha = NULL;
    PyObject *var_pngfile = NULL;
    PyObject *var_X = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    struct Nuitka_FrameObject *frame_3d75e1bc56f161ee4c8854b18e8a19b3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_3d75e1bc56f161ee4c8854b18e8a19b3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3d75e1bc56f161ee4c8854b18e8a19b3, codeobj_3d75e1bc56f161ee4c8854b18e8a19b3, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3d75e1bc56f161ee4c8854b18e8a19b3 = cache_frame_3d75e1bc56f161ee4c8854b18e8a19b3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3d75e1bc56f161ee4c8854b18e8a19b3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3d75e1bc56f161ee4c8854b18e8a19b3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_matplotlib;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain__png_tuple;
        tmp_level_name_1 = const_int_0;
        frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame.f_lineno = 400;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 400;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__png );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 400;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var__png == NULL );
        var__png = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_tex );
        tmp_tuple_element_1 = par_tex;
        tmp_assign_source_2 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_2, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame.f_lineno = 401;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_font_config );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_2 );

            exception_lineno = 401;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_assign_source_2, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_fontsize );
        tmp_tuple_element_1 = par_fontsize;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_2, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_dpi );
        tmp_tuple_element_1 = par_dpi;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_2, 3, tmp_tuple_element_1 );
        assert( var_key == NULL );
        var_key = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_grey_arrayd );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 402;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_key );
        tmp_args_element_name_1 = var_key;
        frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame.f_lineno = 402;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 402;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_alpha == NULL );
        var_alpha = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_alpha );
        tmp_compexpr_left_1 = var_alpha;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_3 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_2 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_3 = par_fontsize;
            CHECK_OBJECT( par_dpi );
            tmp_args_element_name_4 = par_dpi;
            frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame.f_lineno = 404;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_assign_source_4 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_3, const_str_plain_make_png, call_args );
            }

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 404;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_pngfile == NULL );
            var_pngfile = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( var__png );
            tmp_source_name_2 = var__png;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_read_png );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_1;
            tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_path );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_texcache );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_pngfile );
            tmp_args_element_name_7 = var_pngfile;
            frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame.f_lineno = 405;
            {
                PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame.f_lineno = 405;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 405;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_X == NULL );
            var_X = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_X );
            tmp_subscribed_name_1 = var_X;
            tmp_subscript_name_1 = const_tuple_slice_none_none_none_slice_none_none_none_int_neg_1_tuple;
            tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 406;
                type_description_1 = "ooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_assign_unpack_1__assign_source == NULL );
            tmp_assign_unpack_1__assign_source = tmp_assign_source_6;
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_ass_subvalue_1 = tmp_assign_unpack_1__assign_source;
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_grey_arrayd );
            if ( tmp_ass_subscribed_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 406;
                type_description_1 = "ooooooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( var_key );
            tmp_ass_subscript_1 = var_key;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscribed_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 406;
                type_description_1 = "ooooooooo";
                goto try_except_handler_2;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
            tmp_assign_source_7 = tmp_assign_unpack_1__assign_source;
            {
                PyObject *old = var_alpha;
                assert( old != NULL );
                var_alpha = tmp_assign_source_7;
                Py_INCREF( var_alpha );
                Py_DECREF( old );
            }

        }
        CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
        Py_DECREF( tmp_assign_unpack_1__assign_source );
        tmp_assign_unpack_1__assign_source = NULL;

        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d75e1bc56f161ee4c8854b18e8a19b3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d75e1bc56f161ee4c8854b18e8a19b3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3d75e1bc56f161ee4c8854b18e8a19b3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3d75e1bc56f161ee4c8854b18e8a19b3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3d75e1bc56f161ee4c8854b18e8a19b3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3d75e1bc56f161ee4c8854b18e8a19b3,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        par_dpi,
        var__png,
        var_key,
        var_alpha,
        var_pngfile,
        var_X
    );


    // Release cached frame.
    if ( frame_3d75e1bc56f161ee4c8854b18e8a19b3 == cache_frame_3d75e1bc56f161ee4c8854b18e8a19b3 )
    {
        Py_DECREF( frame_3d75e1bc56f161ee4c8854b18e8a19b3 );
    }
    cache_frame_3d75e1bc56f161ee4c8854b18e8a19b3 = NULL;

    assertFrameObject( frame_3d75e1bc56f161ee4c8854b18e8a19b3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_alpha );
    tmp_return_value = var_alpha;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_13_get_grey );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var__png );
    Py_DECREF( var__png );
    var__png = NULL;

    CHECK_OBJECT( (PyObject *)var_key );
    Py_DECREF( var_key );
    var_key = NULL;

    CHECK_OBJECT( (PyObject *)var_alpha );
    Py_DECREF( var_alpha );
    var_alpha = NULL;

    Py_XDECREF( var_pngfile );
    var_pngfile = NULL;

    Py_XDECREF( var_X );
    var_X = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    Py_XDECREF( var__png );
    var__png = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_alpha );
    var_alpha = NULL;

    Py_XDECREF( var_pngfile );
    var_pngfile = NULL;

    Py_XDECREF( var_X );
    var_X = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_13_get_grey );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_14_get_rgba( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *par_dpi = python_pars[ 3 ];
    PyObject *par_rgb = python_pars[ 4 ];
    PyObject *var_r = NULL;
    PyObject *var_g = NULL;
    PyObject *var_b = NULL;
    PyObject *var_key = NULL;
    PyObject *var_Z = NULL;
    PyObject *var_alpha = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_b5aa252c1f16d06c1f569f2ce0538991;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_b5aa252c1f16d06c1f569f2ce0538991 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b5aa252c1f16d06c1f569f2ce0538991, codeobj_b5aa252c1f16d06c1f569f2ce0538991, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b5aa252c1f16d06c1f569f2ce0538991 = cache_frame_b5aa252c1f16d06c1f569f2ce0538991;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b5aa252c1f16d06c1f569f2ce0538991 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b5aa252c1f16d06c1f569f2ce0538991 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_fontsize );
        tmp_operand_name_1 = par_fontsize;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 411;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_subscript_name_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 412;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = tmp_mvar_value_1;
            tmp_subscript_name_1 = const_str_digest_876c9648523deea8f3845ee2dd49a0d1;
            tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 412;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_fontsize;
                assert( old != NULL );
                par_fontsize = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( par_dpi );
        tmp_operand_name_2 = par_dpi;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 413;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_subscript_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 414;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_2 = tmp_mvar_value_2;
            tmp_subscript_name_2 = const_str_digest_0ef268d7244e784284aed7369564edb4;
            tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 414;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_dpi;
                assert( old != NULL );
                par_dpi = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_rgb );
        tmp_iter_arg_1 = par_rgb;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 415;
            type_description_1 = "ooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooo";
            exception_lineno = 415;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooo";
            exception_lineno = 415;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooo";
            exception_lineno = 415;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_3 == NULL );
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_6;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooo";
                    exception_lineno = 415;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooo";
            exception_lineno = 415;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        assert( var_r == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_r = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        assert( var_g == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_g = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_3;
        assert( var_b == NULL );
        Py_INCREF( tmp_assign_source_9 );
        var_b = tmp_assign_source_9;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_tuple_arg_1;
        CHECK_OBJECT( par_tex );
        tmp_tuple_element_1 = par_tex;
        tmp_assign_source_10 = PyTuple_New( 5 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_10, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_b5aa252c1f16d06c1f569f2ce0538991->m_frame.f_lineno = 416;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_font_config );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_10 );

            exception_lineno = 416;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_assign_source_10, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_fontsize );
        tmp_tuple_element_1 = par_fontsize;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_10, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_dpi );
        tmp_tuple_element_1 = par_dpi;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_10, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_rgb );
        tmp_tuple_arg_1 = par_rgb;
        tmp_tuple_element_1 = PySequence_Tuple( tmp_tuple_arg_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_10 );

            exception_lineno = 416;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_assign_source_10, 4, tmp_tuple_element_1 );
        assert( var_key == NULL );
        var_key = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_rgba_arrayd );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 417;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_key );
        tmp_args_element_name_1 = var_key;
        frame_b5aa252c1f16d06c1f569f2ce0538991->m_frame.f_lineno = 417;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 417;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_Z == NULL );
        var_Z = tmp_assign_source_11;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_Z );
        tmp_compexpr_left_1 = var_Z;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( par_self );
            tmp_called_instance_3 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_2 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_3 = par_fontsize;
            CHECK_OBJECT( par_dpi );
            tmp_args_element_name_4 = par_dpi;
            frame_b5aa252c1f16d06c1f569f2ce0538991->m_frame.f_lineno = 420;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_assign_source_12 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_3, const_str_plain_get_grey, call_args );
            }

            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 420;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_alpha == NULL );
            var_alpha = tmp_assign_source_12;
        }
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_list_element_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_np );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 421;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_4 = tmp_mvar_value_3;
            CHECK_OBJECT( var_r );
            tmp_list_element_1 = var_r;
            tmp_args_element_name_5 = PyList_New( 4 );
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_5, 0, tmp_list_element_1 );
            CHECK_OBJECT( var_g );
            tmp_list_element_1 = var_g;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_5, 1, tmp_list_element_1 );
            CHECK_OBJECT( var_b );
            tmp_list_element_1 = var_b;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_5, 2, tmp_list_element_1 );
            CHECK_OBJECT( var_alpha );
            tmp_list_element_1 = var_alpha;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_5, 3, tmp_list_element_1 );
            frame_b5aa252c1f16d06c1f569f2ce0538991->m_frame.f_lineno = 421;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_assign_source_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_dstack, call_args );
            }

            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 421;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_Z;
                assert( old != NULL );
                var_Z = tmp_assign_source_13;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( var_Z );
            tmp_ass_subvalue_1 = var_Z;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_rgba_arrayd );
            if ( tmp_ass_subscribed_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 422;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_key );
            tmp_ass_subscript_1 = var_key;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscribed_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 422;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_3:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b5aa252c1f16d06c1f569f2ce0538991 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b5aa252c1f16d06c1f569f2ce0538991 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b5aa252c1f16d06c1f569f2ce0538991, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b5aa252c1f16d06c1f569f2ce0538991->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b5aa252c1f16d06c1f569f2ce0538991, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b5aa252c1f16d06c1f569f2ce0538991,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        par_dpi,
        par_rgb,
        var_r,
        var_g,
        var_b,
        var_key,
        var_Z,
        var_alpha
    );


    // Release cached frame.
    if ( frame_b5aa252c1f16d06c1f569f2ce0538991 == cache_frame_b5aa252c1f16d06c1f569f2ce0538991 )
    {
        Py_DECREF( frame_b5aa252c1f16d06c1f569f2ce0538991 );
    }
    cache_frame_b5aa252c1f16d06c1f569f2ce0538991 = NULL;

    assertFrameObject( frame_b5aa252c1f16d06c1f569f2ce0538991 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_Z );
    tmp_return_value = var_Z;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_14_get_rgba );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_dpi );
    Py_DECREF( par_dpi );
    par_dpi = NULL;

    CHECK_OBJECT( (PyObject *)par_rgb );
    Py_DECREF( par_rgb );
    par_rgb = NULL;

    CHECK_OBJECT( (PyObject *)var_r );
    Py_DECREF( var_r );
    var_r = NULL;

    CHECK_OBJECT( (PyObject *)var_g );
    Py_DECREF( var_g );
    var_g = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    CHECK_OBJECT( (PyObject *)var_key );
    Py_DECREF( var_key );
    var_key = NULL;

    CHECK_OBJECT( (PyObject *)var_Z );
    Py_DECREF( var_Z );
    var_Z = NULL;

    Py_XDECREF( var_alpha );
    var_alpha = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    Py_XDECREF( par_fontsize );
    par_fontsize = NULL;

    Py_XDECREF( par_dpi );
    par_dpi = NULL;

    CHECK_OBJECT( (PyObject *)par_rgb );
    Py_DECREF( par_rgb );
    par_rgb = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    Py_XDECREF( var_g );
    var_g = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_Z );
    var_Z = NULL;

    Py_XDECREF( var_alpha );
    var_alpha = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_14_get_rgba );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$texmanager$$$function_15_get_text_width_height_descent( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_tex = python_pars[ 1 ];
    PyObject *par_fontsize = python_pars[ 2 ];
    PyObject *par_renderer = python_pars[ 3 ];
    PyObject *var_dpi_fraction = NULL;
    PyObject *var_basefile = NULL;
    PyObject *var_baselinefile = NULL;
    PyObject *var_dvifile = NULL;
    PyObject *var_fh = NULL;
    PyObject *var_l = NULL;
    PyObject *var_height = NULL;
    PyObject *var_depth = NULL;
    PyObject *var_width = NULL;
    PyObject *var_dvi = NULL;
    PyObject *var_page = NULL;
    PyObject *outline_0_var_l1 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    PyObject *tmp_with_2__enter = NULL;
    PyObject *tmp_with_2__exit = NULL;
    nuitka_bool tmp_with_2__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_2__source = NULL;
    struct Nuitka_FrameObject *frame_ce63371f722df7668d792b93c463f329;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    struct Nuitka_FrameObject *frame_ae58b6fc41a0c249b54f756d4ee3aea3_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    static struct Nuitka_FrameObject *cache_frame_ce63371f722df7668d792b93c463f329 = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ce63371f722df7668d792b93c463f329, codeobj_ce63371f722df7668d792b93c463f329, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ce63371f722df7668d792b93c463f329 = cache_frame_ce63371f722df7668d792b93c463f329;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ce63371f722df7668d792b93c463f329 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ce63371f722df7668d792b93c463f329 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_tex );
        tmp_called_instance_1 = par_tex;
        frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 428;
        tmp_compexpr_left_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_strip );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 428;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_str_empty;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 428;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_tuple_int_0_int_0_int_0_tuple;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_renderer );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_renderer );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 431;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_renderer );
        tmp_called_instance_2 = par_renderer;
        frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 431;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_points_to_pixels, &PyTuple_GET_ITEM( const_tuple_float_1_0_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 431;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_assign_source_1 = const_int_pos_1;
        Py_INCREF( tmp_assign_source_1 );
        condexpr_end_1:;
        assert( var_dpi_fraction == NULL );
        var_dpi_fraction = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_result_1;
        int tmp_truth_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rcParams );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rcParams" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 433;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        tmp_subscript_name_1 = const_str_digest_95b020fe417b0958dcb803463bbe8067;
        tmp_subscript_result_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_subscript_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 433;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_subscript_result_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_subscript_result_1 );

            exception_lineno = 433;
            type_description_1 = "ooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_subscript_result_1 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_called_instance_3 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_1 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_2 = par_fontsize;
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 435;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_get_basefile, call_args );
            }

            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 435;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_basefile == NULL );
            var_basefile = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_94ba037759435a553bb5b8a7861af2ff;
            CHECK_OBJECT( var_basefile );
            tmp_right_name_1 = var_basefile;
            tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 436;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_baselinefile == NULL );
            var_baselinefile = tmp_assign_source_3;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 438;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 438;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_baselinefile );
            tmp_args_element_name_3 = var_baselinefile;
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 438;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_exists, call_args );
            }

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 438;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 438;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_called_instance_5;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                CHECK_OBJECT( par_self );
                tmp_called_instance_5 = par_self;
                CHECK_OBJECT( par_tex );
                tmp_args_element_name_4 = par_tex;
                CHECK_OBJECT( par_fontsize );
                tmp_args_element_name_5 = par_fontsize;
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 439;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                    tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_5, const_str_plain_make_dvi_preview, call_args );
                }

                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 439;
                    type_description_1 = "ooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_dvifile == NULL );
                var_dvifile = tmp_assign_source_4;
            }
            branch_no_3:;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_open_filename_1;
            CHECK_OBJECT( var_baselinefile );
            tmp_open_filename_1 = var_baselinefile;
            tmp_assign_source_5 = BUILTIN_OPEN( tmp_open_filename_1, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 441;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__source == NULL );
            tmp_with_1__source = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_2 = tmp_with_1__source;
            tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 441;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_2;
            }
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 441;
            tmp_assign_source_6 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 441;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__enter == NULL );
            tmp_with_1__enter = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_with_1__source );
            tmp_source_name_3 = tmp_with_1__source;
            tmp_assign_source_7 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 441;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_with_1__exit == NULL );
            tmp_with_1__exit = tmp_assign_source_7;
        }
        {
            nuitka_bool tmp_assign_source_8;
            tmp_assign_source_8 = NUITKA_BOOL_TRUE;
            tmp_with_1__indicator = tmp_assign_source_8;
        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_with_1__enter );
            tmp_assign_source_9 = tmp_with_1__enter;
            assert( var_fh == NULL );
            Py_INCREF( tmp_assign_source_9 );
            var_fh = tmp_assign_source_9;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_called_instance_6;
            PyObject *tmp_called_instance_7;
            CHECK_OBJECT( var_fh );
            tmp_called_instance_7 = var_fh;
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 442;
            tmp_called_instance_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_read );
            if ( tmp_called_instance_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 442;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_4;
            }
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 442;
            tmp_assign_source_10 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_split );
            Py_DECREF( tmp_called_instance_6 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 442;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_4;
            }
            assert( var_l == NULL );
            var_l = tmp_assign_source_10;
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_ce63371f722df7668d792b93c463f329, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_ce63371f722df7668d792b93c463f329, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 441;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                nuitka_bool tmp_assign_source_11;
                tmp_assign_source_11 = NUITKA_BOOL_FALSE;
                tmp_with_1__indicator = tmp_assign_source_11;
            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_operand_name_2;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_args_element_name_7;
                PyObject *tmp_args_element_name_8;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_2 = tmp_with_1__exit;
                tmp_args_element_name_6 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_7 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_8 = EXC_TRACEBACK(PyThreadState_GET());
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 442;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                    tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
                }

                if ( tmp_operand_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 442;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_5;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                Py_DECREF( tmp_operand_name_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 442;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_5;
                }
                tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 442;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_ce63371f722df7668d792b93c463f329->m_frame) frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_5;
                branch_no_5:;
            }
            goto branch_end_4;
            branch_no_4:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 441;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ce63371f722df7668d792b93c463f329->m_frame) frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_5;
            branch_end_4:;
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        try_end_2:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_1;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
        return NULL;
        // End of try:
        try_end_1:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_7;
            nuitka_bool tmp_compexpr_left_3;
            nuitka_bool tmp_compexpr_right_3;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_3 = tmp_with_1__indicator;
            tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
            tmp_condition_result_7 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_called_name_3;
                PyObject *tmp_call_result_1;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_3 = tmp_with_1__exit;
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 442;
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_3 );
                    Py_XDECREF( exception_keeper_value_3 );
                    Py_XDECREF( exception_keeper_tb_3 );

                    exception_lineno = 442;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            branch_no_6:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_2;
        // End of try:
        try_end_3:;
        {
            nuitka_bool tmp_condition_result_8;
            nuitka_bool tmp_compexpr_left_4;
            nuitka_bool tmp_compexpr_right_4;
            assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_4 = tmp_with_1__indicator;
            tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
            tmp_condition_result_8 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( tmp_with_1__exit );
                tmp_called_name_4 = tmp_with_1__exit;
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 442;
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 442;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_no_7:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        Py_XDECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        Py_XDECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_with_1__source );
        Py_DECREF( tmp_with_1__source );
        tmp_with_1__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
        Py_DECREF( tmp_with_1__enter );
        tmp_with_1__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
        Py_DECREF( tmp_with_1__exit );
        tmp_with_1__exit = NULL;

        // Tried code:
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_iter_arg_1;
            // Tried code:
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_iter_arg_2;
                if ( var_l == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "l" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 443;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_7;
                }

                tmp_iter_arg_2 = var_l;
                tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 443;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_7;
                }
                assert( tmp_listcomp_1__$0 == NULL );
                tmp_listcomp_1__$0 = tmp_assign_source_13;
            }
            {
                PyObject *tmp_assign_source_14;
                tmp_assign_source_14 = PyList_New( 0 );
                assert( tmp_listcomp_1__contraction == NULL );
                tmp_listcomp_1__contraction = tmp_assign_source_14;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_ae58b6fc41a0c249b54f756d4ee3aea3_2, codeobj_ae58b6fc41a0c249b54f756d4ee3aea3, module_matplotlib$texmanager, sizeof(void *)+sizeof(void *) );
            frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 = cache_frame_ae58b6fc41a0c249b54f756d4ee3aea3_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_15;
                CHECK_OBJECT( tmp_listcomp_1__$0 );
                tmp_next_source_1 = tmp_listcomp_1__$0;
                tmp_assign_source_15 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_15 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "oo";
                        exception_lineno = 443;
                        goto try_except_handler_8;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_0;
                    tmp_listcomp_1__iter_value_0 = tmp_assign_source_15;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_16;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                tmp_assign_source_16 = tmp_listcomp_1__iter_value_0;
                {
                    PyObject *old = outline_0_var_l1;
                    outline_0_var_l1 = tmp_assign_source_16;
                    Py_INCREF( outline_0_var_l1 );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_left_name_2;
                PyObject *tmp_float_arg_1;
                PyObject *tmp_right_name_2;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_l1 );
                tmp_float_arg_1 = outline_0_var_l1;
                tmp_left_name_2 = TO_FLOAT( tmp_float_arg_1 );
                if ( tmp_left_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 443;
                    type_description_2 = "oo";
                    goto try_except_handler_8;
                }
                CHECK_OBJECT( var_dpi_fraction );
                tmp_right_name_2 = var_dpi_fraction;
                tmp_append_value_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_left_name_2 );
                if ( tmp_append_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 443;
                    type_description_2 = "oo";
                    goto try_except_handler_8;
                }
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 443;
                    type_description_2 = "oo";
                    goto try_except_handler_8;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 443;
                type_description_2 = "oo";
                goto try_except_handler_8;
            }
            goto loop_start_1;
            loop_end_1:;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_iter_arg_1 = tmp_listcomp_1__contraction;
            Py_INCREF( tmp_iter_arg_1 );
            goto try_return_handler_8;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
            return NULL;
            // Return handler code:
            try_return_handler_8:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_8:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_7;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_ae58b6fc41a0c249b54f756d4ee3aea3_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_ae58b6fc41a0c249b54f756d4ee3aea3_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_ae58b6fc41a0c249b54f756d4ee3aea3_2,
                type_description_2,
                outline_0_var_l1,
                var_dpi_fraction
            );


            // Release cached frame.
            if ( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 == cache_frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 )
            {
                Py_DECREF( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 );
            }
            cache_frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 = NULL;

            assertFrameObject( frame_ae58b6fc41a0c249b54f756d4ee3aea3_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_7;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
            return NULL;
            // Return handler code:
            try_return_handler_7:;
            Py_XDECREF( outline_0_var_l1 );
            outline_0_var_l1 = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_0_var_l1 );
            outline_0_var_l1 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
            return NULL;
            outline_exception_1:;
            exception_lineno = 443;
            goto try_except_handler_6;
            outline_result_1:;
            tmp_assign_source_12 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 443;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_6;
            }
            assert( tmp_tuple_unpack_1__source_iter == NULL );
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_12;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
            if ( tmp_assign_source_17 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooo";
                exception_lineno = 443;
                goto try_except_handler_9;
            }
            assert( tmp_tuple_unpack_1__element_1 == NULL );
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_17;
        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
            if ( tmp_assign_source_18 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooo";
                exception_lineno = 443;
                goto try_except_handler_9;
            }
            assert( tmp_tuple_unpack_1__element_2 == NULL );
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_18;
        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
            if ( tmp_assign_source_19 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooo";
                exception_lineno = 443;
                goto try_except_handler_9;
            }
            assert( tmp_tuple_unpack_1__element_3 == NULL );
            tmp_tuple_unpack_1__element_3 = tmp_assign_source_19;
        }
        {
            PyObject *tmp_iterator_name_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooo";
                        exception_lineno = 443;
                        goto try_except_handler_9;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooooo";
                exception_lineno = 443;
                goto try_except_handler_9;
            }
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_6;
        // End of try:
        try_end_5:;
        goto try_end_6;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        Py_XDECREF( tmp_tuple_unpack_1__element_3 );
        tmp_tuple_unpack_1__element_3 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto frame_exception_exit_1;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_20;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_20 = tmp_tuple_unpack_1__element_1;
            assert( var_height == NULL );
            Py_INCREF( tmp_assign_source_20 );
            var_height = tmp_assign_source_20;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_21;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_21 = tmp_tuple_unpack_1__element_2;
            assert( var_depth == NULL );
            Py_INCREF( tmp_assign_source_21 );
            var_depth = tmp_assign_source_21;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        {
            PyObject *tmp_assign_source_22;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
            tmp_assign_source_22 = tmp_tuple_unpack_1__element_3;
            assert( var_width == NULL );
            Py_INCREF( tmp_assign_source_22 );
            var_width = tmp_assign_source_22;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_3 );
        tmp_tuple_unpack_1__element_3 = NULL;

        {
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            CHECK_OBJECT( var_width );
            tmp_tuple_element_1 = var_width;
            tmp_return_value = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_height );
            tmp_left_name_3 = var_height;
            CHECK_OBJECT( var_depth );
            tmp_right_name_3 = var_depth;
            tmp_tuple_element_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 444;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( var_depth );
            tmp_tuple_element_1 = var_depth;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
            goto frame_return_exit_1;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_instance_8;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_args_element_name_10;
            CHECK_OBJECT( par_self );
            tmp_called_instance_8 = par_self;
            CHECK_OBJECT( par_tex );
            tmp_args_element_name_9 = par_tex;
            CHECK_OBJECT( par_fontsize );
            tmp_args_element_name_10 = par_fontsize;
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 448;
            {
                PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
                tmp_assign_source_23 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_8, const_str_plain_make_dvi, call_args );
            }

            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 448;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_dvifile == NULL );
            var_dvifile = tmp_assign_source_23;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_dviread );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dviread );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dviread" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }

            tmp_source_name_4 = tmp_mvar_value_3;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_Dvi );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            CHECK_OBJECT( var_dvifile );
            tmp_args_element_name_11 = var_dvifile;
            tmp_left_name_4 = const_int_pos_72;
            CHECK_OBJECT( var_dpi_fraction );
            tmp_right_name_4 = var_dpi_fraction;
            tmp_args_element_name_12 = BINARY_OPERATION_MUL_LONG_OBJECT( tmp_left_name_4, tmp_right_name_4 );
            if ( tmp_args_element_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 449;
            {
                PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
                tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            assert( tmp_with_2__source == NULL );
            tmp_with_2__source = tmp_assign_source_24;
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_called_name_6;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_with_2__source );
            tmp_source_name_5 = tmp_with_2__source;
            tmp_called_name_6 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___enter__ );
            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 449;
            tmp_assign_source_25 = CALL_FUNCTION_NO_ARGS( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_6 );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            assert( tmp_with_2__enter == NULL );
            tmp_with_2__enter = tmp_assign_source_25;
        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( tmp_with_2__source );
            tmp_source_name_6 = tmp_with_2__source;
            tmp_assign_source_26 = LOOKUP_SPECIAL( tmp_source_name_6, const_str_plain___exit__ );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_10;
            }
            assert( tmp_with_2__exit == NULL );
            tmp_with_2__exit = tmp_assign_source_26;
        }
        {
            nuitka_bool tmp_assign_source_27;
            tmp_assign_source_27 = NUITKA_BOOL_TRUE;
            tmp_with_2__indicator = tmp_assign_source_27;
        }
        {
            PyObject *tmp_assign_source_28;
            CHECK_OBJECT( tmp_with_2__enter );
            tmp_assign_source_28 = tmp_with_2__enter;
            assert( var_dvi == NULL );
            Py_INCREF( tmp_assign_source_28 );
            var_dvi = tmp_assign_source_28;
        }
        // Tried code:
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_iter_arg_3;
            CHECK_OBJECT( var_dvi );
            tmp_iter_arg_3 = var_dvi;
            tmp_assign_source_29 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 450;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_13;
            }
            assert( tmp_tuple_unpack_2__source_iter == NULL );
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_29;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_4, 0, 1 );
            if ( tmp_assign_source_30 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooooooooo";
                exception_lineno = 450;
                goto try_except_handler_14;
            }
            assert( tmp_tuple_unpack_2__element_1 == NULL );
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_30;
        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "ooooooooooooooo";
                        exception_lineno = 450;
                        goto try_except_handler_14;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 1)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooooo";
                exception_lineno = 450;
                goto try_except_handler_14;
            }
        }
        goto try_end_7;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_13;
        // End of try:
        try_end_7:;
        goto try_end_8;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto try_except_handler_12;
        // End of try:
        try_end_8:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_31;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_31 = tmp_tuple_unpack_2__element_1;
            assert( var_page == NULL );
            Py_INCREF( tmp_assign_source_31 );
            var_page = tmp_assign_source_31;
        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        goto try_end_9;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_2 );
        exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_2 );
        exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_2 );

        if ( exception_keeper_tb_11 == NULL )
        {
            exception_keeper_tb_11 = MAKE_TRACEBACK( frame_ce63371f722df7668d792b93c463f329, exception_keeper_lineno_11 );
        }
        else if ( exception_keeper_lineno_11 != 0 )
        {
            exception_keeper_tb_11 = ADD_TRACEBACK( exception_keeper_tb_11, frame_ce63371f722df7668d792b93c463f329, exception_keeper_lineno_11 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_11, &exception_keeper_value_11, &exception_keeper_tb_11 );
        PyException_SetTraceback( exception_keeper_value_11, (PyObject *)exception_keeper_tb_11 );
        PUBLISH_EXCEPTION( &exception_keeper_type_11, &exception_keeper_value_11, &exception_keeper_tb_11 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            tmp_compexpr_left_5 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_5 = PyExc_BaseException;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 449;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_15;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                nuitka_bool tmp_assign_source_32;
                tmp_assign_source_32 = NUITKA_BOOL_FALSE;
                tmp_with_2__indicator = tmp_assign_source_32;
            }
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_operand_name_3;
                PyObject *tmp_called_name_7;
                PyObject *tmp_args_element_name_13;
                PyObject *tmp_args_element_name_14;
                PyObject *tmp_args_element_name_15;
                CHECK_OBJECT( tmp_with_2__exit );
                tmp_called_name_7 = tmp_with_2__exit;
                tmp_args_element_name_13 = EXC_TYPE(PyThreadState_GET());
                tmp_args_element_name_14 = EXC_VALUE(PyThreadState_GET());
                tmp_args_element_name_15 = EXC_TRACEBACK(PyThreadState_GET());
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 450;
                {
                    PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15 };
                    tmp_operand_name_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
                }

                if ( tmp_operand_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 450;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_15;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                Py_DECREF( tmp_operand_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 450;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_15;
                }
                tmp_condition_result_10 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 450;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_ce63371f722df7668d792b93c463f329->m_frame) frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooooooooo";
                goto try_except_handler_15;
                branch_no_9:;
            }
            goto branch_end_8;
            branch_no_8:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 449;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ce63371f722df7668d792b93c463f329->m_frame) frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "ooooooooooooooo";
            goto try_except_handler_15;
            branch_end_8:;
        }
        goto try_end_10;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto try_except_handler_11;
        // End of try:
        try_end_10:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        goto try_end_9;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
        return NULL;
        // End of try:
        try_end_9:;
        goto try_end_11;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_condition_result_11;
            nuitka_bool tmp_compexpr_left_6;
            nuitka_bool tmp_compexpr_right_6;
            assert( tmp_with_2__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_6 = tmp_with_2__indicator;
            tmp_compexpr_right_6 = NUITKA_BOOL_TRUE;
            tmp_condition_result_11 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_10;
            }
            else
            {
                goto branch_no_10;
            }
            branch_yes_10:;
            {
                PyObject *tmp_called_name_8;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( tmp_with_2__exit );
                tmp_called_name_8 = tmp_with_2__exit;
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 450;
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_13 );
                    Py_XDECREF( exception_keeper_value_13 );
                    Py_XDECREF( exception_keeper_tb_13 );

                    exception_lineno = 450;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_10;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            branch_no_10:;
        }
        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto try_except_handler_10;
        // End of try:
        try_end_11:;
        {
            nuitka_bool tmp_condition_result_12;
            nuitka_bool tmp_compexpr_left_7;
            nuitka_bool tmp_compexpr_right_7;
            assert( tmp_with_2__indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_7 = tmp_with_2__indicator;
            tmp_compexpr_right_7 = NUITKA_BOOL_TRUE;
            tmp_condition_result_12 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_called_name_9;
                PyObject *tmp_call_result_4;
                CHECK_OBJECT( tmp_with_2__exit );
                tmp_called_name_9 = tmp_with_2__exit;
                frame_ce63371f722df7668d792b93c463f329->m_frame.f_lineno = 450;
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 450;
                    type_description_1 = "ooooooooooooooo";
                    goto try_except_handler_10;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            branch_no_11:;
        }
        goto try_end_12;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_with_2__source );
        tmp_with_2__source = NULL;

        Py_XDECREF( tmp_with_2__enter );
        tmp_with_2__enter = NULL;

        Py_XDECREF( tmp_with_2__exit );
        tmp_with_2__exit = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto frame_exception_exit_1;
        // End of try:
        try_end_12:;
        CHECK_OBJECT( (PyObject *)tmp_with_2__source );
        Py_DECREF( tmp_with_2__source );
        tmp_with_2__source = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_2__enter );
        Py_DECREF( tmp_with_2__enter );
        tmp_with_2__enter = NULL;

        CHECK_OBJECT( (PyObject *)tmp_with_2__exit );
        Py_DECREF( tmp_with_2__exit );
        tmp_with_2__exit = NULL;

        {
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_source_name_7;
            PyObject *tmp_left_name_5;
            PyObject *tmp_source_name_8;
            PyObject *tmp_right_name_5;
            PyObject *tmp_source_name_9;
            PyObject *tmp_source_name_10;
            if ( var_page == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "page" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_7 = var_page;
            tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_width );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_return_value = PyTuple_New( 3 );
            PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
            if ( var_page == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "page" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_8 = var_page;
            tmp_left_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_height );
            if ( tmp_left_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_page == NULL )
            {
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_left_name_5 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "page" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_9 = var_page;
            tmp_right_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_descent );
            if ( tmp_right_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );
                Py_DECREF( tmp_left_name_5 );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_tuple_element_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_5 );
            Py_DECREF( tmp_left_name_5 );
            Py_DECREF( tmp_right_name_5 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
            if ( var_page == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "page" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_10 = var_page;
            tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_descent );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_return_value );

                exception_lineno = 452;
                type_description_1 = "ooooooooooooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_2 );
            goto frame_return_exit_1;
        }
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce63371f722df7668d792b93c463f329 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce63371f722df7668d792b93c463f329 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce63371f722df7668d792b93c463f329 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ce63371f722df7668d792b93c463f329, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ce63371f722df7668d792b93c463f329->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ce63371f722df7668d792b93c463f329, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ce63371f722df7668d792b93c463f329,
        type_description_1,
        par_self,
        par_tex,
        par_fontsize,
        par_renderer,
        var_dpi_fraction,
        var_basefile,
        var_baselinefile,
        var_dvifile,
        var_fh,
        var_l,
        var_height,
        var_depth,
        var_width,
        var_dvi,
        var_page
    );


    // Release cached frame.
    if ( frame_ce63371f722df7668d792b93c463f329 == cache_frame_ce63371f722df7668d792b93c463f329 )
    {
        Py_DECREF( frame_ce63371f722df7668d792b93c463f329 );
    }
    cache_frame_ce63371f722df7668d792b93c463f329 = NULL;

    assertFrameObject( frame_ce63371f722df7668d792b93c463f329 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    Py_XDECREF( var_dpi_fraction );
    var_dpi_fraction = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_baselinefile );
    var_baselinefile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    Py_XDECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_l );
    var_l = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_depth );
    var_depth = NULL;

    Py_XDECREF( var_width );
    var_width = NULL;

    Py_XDECREF( var_dvi );
    var_dvi = NULL;

    Py_XDECREF( var_page );
    var_page = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_tex );
    Py_DECREF( par_tex );
    par_tex = NULL;

    CHECK_OBJECT( (PyObject *)par_fontsize );
    Py_DECREF( par_fontsize );
    par_fontsize = NULL;

    CHECK_OBJECT( (PyObject *)par_renderer );
    Py_DECREF( par_renderer );
    par_renderer = NULL;

    Py_XDECREF( var_dpi_fraction );
    var_dpi_fraction = NULL;

    Py_XDECREF( var_basefile );
    var_basefile = NULL;

    Py_XDECREF( var_baselinefile );
    var_baselinefile = NULL;

    Py_XDECREF( var_dvifile );
    var_dvifile = NULL;

    Py_XDECREF( var_fh );
    var_fh = NULL;

    Py_XDECREF( var_l );
    var_l = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_depth );
    var_depth = NULL;

    Py_XDECREF( var_width );
    var_width = NULL;

    Py_XDECREF( var_dvi );
    var_dvi = NULL;

    Py_XDECREF( var_page );
    var_page = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$texmanager$$$function_15_get_text_width_height_descent );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_10_make_dvi(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_10_make_dvi,
        const_str_plain_make_dvi,
#if PYTHON_VERSION >= 300
        const_str_digest_f5b3e2da1ea2a856c2d58edc746b8ee9,
#endif
        codeobj_cdb8867686ac28d342037e078516a799,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_78655ce5339fe407fc985b9e53ac5deb,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_11_make_dvi_preview(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_11_make_dvi_preview,
        const_str_plain_make_dvi_preview,
#if PYTHON_VERSION >= 300
        const_str_digest_d4431b0426576a07f19cda03326c253a,
#endif
        codeobj_afe1565802bff734eb9df1a7ea37adbb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_81c020872fc7de5ff77312e7e1dc5fc3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_12_make_png(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_12_make_png,
        const_str_plain_make_png,
#if PYTHON_VERSION >= 300
        const_str_digest_9d50db30dcf25d7bb8b27b5444f5991e,
#endif
        codeobj_7054fbef09f51b9f42f20d726b61666e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_1bebc29034a166ca514e2b054466afb6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_13_get_grey( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_13_get_grey,
        const_str_plain_get_grey,
#if PYTHON_VERSION >= 300
        const_str_digest_847e8a9ba2def92a0a16c231c8318eed,
#endif
        codeobj_3d75e1bc56f161ee4c8854b18e8a19b3,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_7f30043b9a2bb91b0e84ad3903ac0a42,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_14_get_rgba( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_14_get_rgba,
        const_str_plain_get_rgba,
#if PYTHON_VERSION >= 300
        const_str_digest_a8dc9ad548bfe95b26e3c1ba965ee0a8,
#endif
        codeobj_b5aa252c1f16d06c1f569f2ce0538991,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_f4eb7fe84e890bc3248e2b3093eec403,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_15_get_text_width_height_descent( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_15_get_text_width_height_descent,
        const_str_plain_get_text_width_height_descent,
#if PYTHON_VERSION >= 300
        const_str_digest_3f89837fd54cf40b4db7d36767a99d84,
#endif
        codeobj_ce63371f722df7668d792b93c463f329,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_03b8dda3ffebd96a6b130e1cb60f2f27,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_1___new__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_1___new__,
        const_str_plain___new__,
#if PYTHON_VERSION >= 300
        const_str_digest_9b7324678882942a7030b665c2148e26,
#endif
        codeobj_be8f5e3a76741c2be18baa713ab03908,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_2__reinit(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_2__reinit,
        const_str_plain__reinit,
#if PYTHON_VERSION >= 300
        const_str_digest_ce668e8d3c993b7e8fec3358e49f9674,
#endif
        codeobj_82d31c90b2bc44bd8956a140016c659c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_3_get_basefile( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_3_get_basefile,
        const_str_plain_get_basefile,
#if PYTHON_VERSION >= 300
        const_str_digest_3d457918981054c271396c44cb4a3da1,
#endif
        codeobj_2e2900f8e1990fc4a076565895012bd6,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_efd982e9c319b0d2cd1e1a07ed784dab,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_4_get_font_config(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_4_get_font_config,
        const_str_plain_get_font_config,
#if PYTHON_VERSION >= 300
        const_str_digest_b038421a02d77f9b5d274e8555649244,
#endif
        codeobj_4ea70b512bc8b1d3be40b3088ed97add,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_39d9f01d64225c607852a9c6459f5a6e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_5_get_font_preamble(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_5_get_font_preamble,
        const_str_plain_get_font_preamble,
#if PYTHON_VERSION >= 300
        const_str_digest_c719558650e7b9f8cfadda9ccf4a4599,
#endif
        codeobj_0ab86185c88ea08441b3fc271fd99bc0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_e0e3f24a7bc88427dc66fa01ae4dbc38,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_6_get_custom_preamble(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_6_get_custom_preamble,
        const_str_plain_get_custom_preamble,
#if PYTHON_VERSION >= 300
        const_str_digest_d58ac6888e0c113ae7f41e8955b8f6cf,
#endif
        codeobj_95ed78ef04bd33c0f4c97cdc4592a6b5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_2824195fb82d23cb05c0a1f71a0e4d3c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_7_make_tex(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_7_make_tex,
        const_str_plain_make_tex,
#if PYTHON_VERSION >= 300
        const_str_digest_c396522b4d8d8b219cca9cbe512ef24e,
#endif
        codeobj_95eb3e49b1f99b994dae5684beda067e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_a8836377e124dc254c743ec614df91c2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_8_make_tex_preview(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_8_make_tex_preview,
        const_str_plain_make_tex_preview,
#if PYTHON_VERSION >= 300
        const_str_digest_5ccbf34487b50d11e4ba2bd929cdfe91,
#endif
        codeobj_1d5ceb1ed1a260a11fe98e392e5ba5c5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        const_str_digest_50ef9b639fb224b7409dfaccdceb4fac,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$texmanager$$$function_9__run_checked_subprocess(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$texmanager$$$function_9__run_checked_subprocess,
        const_str_plain__run_checked_subprocess,
#if PYTHON_VERSION >= 300
        const_str_digest_a6ece34cc8b11b0923a994a726b60f2f,
#endif
        codeobj_dbb6c692af222223e58ef7650e8caadb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$texmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$texmanager =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.texmanager",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$texmanager)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$texmanager)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$texmanager );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.texmanager: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.texmanager: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.texmanager: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$texmanager" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$texmanager = Py_InitModule4(
        "matplotlib.texmanager",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$texmanager = PyModule_Create( &mdef_matplotlib$texmanager );
#endif

    moduledict_matplotlib$texmanager = MODULE_DICT( module_matplotlib$texmanager );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$texmanager,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$texmanager,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$texmanager,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$texmanager,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$texmanager );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_2927227e39724474c42d53ec57cf54f3, module_matplotlib$texmanager );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_TexManager$genexpr_1__$0 = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_09f11e489fd7ceaa2d038df5e5839b15;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_matplotlib$texmanager_49 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_458668bb1305aeecb0f754db4460bf6e_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_458668bb1305aeecb0f754db4460bf6e_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_356e9ba5a3f3236c7dfc259adeb0d143;
        UPDATE_STRING_DICT0( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_09f11e489fd7ceaa2d038df5e5839b15 = MAKE_MODULE_FRAME( codeobj_09f11e489fd7ceaa2d038df5e5839b15, module_matplotlib$texmanager );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_09f11e489fd7ceaa2d038df5e5839b15 );
    assert( Py_REFCNT( frame_09f11e489fd7ceaa2d038df5e5839b15 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_copy;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 31;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_copy, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_functools;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 32;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_glob;
        tmp_globals_name_3 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 33;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_glob, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_hashlib;
        tmp_globals_name_4 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 34;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_hashlib, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_logging;
        tmp_globals_name_5 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 35;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_logging, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_os;
        tmp_globals_name_6 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 36;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_pathlib;
        tmp_globals_name_7 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_Path_tuple;
        tmp_level_name_7 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 37;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Path );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_Path, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_re;
        tmp_globals_name_8 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = Py_None;
        tmp_level_name_8 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 38;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_subprocess;
        tmp_globals_name_9 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = Py_None;
        tmp_level_name_9 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 39;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_subprocess, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_plain_numpy;
        tmp_globals_name_10 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = Py_None;
        tmp_level_name_10 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 41;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_np, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_plain_matplotlib;
        tmp_globals_name_11 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = Py_None;
        tmp_level_name_11 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 43;
        tmp_assign_source_14 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_mpl, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_plain_matplotlib;
        tmp_globals_name_12 = (PyObject *)moduledict_matplotlib$texmanager;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_cbook_str_plain_dviread_str_plain_rcParams_tuple;
        tmp_level_name_12 = const_int_0;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 44;
        tmp_assign_source_15 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_15;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_cbook );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_cbook, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_dviread );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_dviread, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_rcParams );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_rcParams, tmp_assign_source_18 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_logging );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_logging );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "logging" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 46;
        tmp_assign_source_19 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_getLogger, &PyTuple_GET_ITEM( const_tuple_str_digest_2927227e39724474c42d53ec57cf54f3_tuple, 0 ) );

        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain__log, tmp_assign_source_19 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_20 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_21;
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_22 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_22;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_2;
            }
            tmp_tuple_element_1 = const_str_plain_TexManager;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 49;
            tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_23;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 49;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 49;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_24;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_25;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_matplotlib$texmanager_49 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_2927227e39724474c42d53ec57cf54f3;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_71e8eace702b793e0ab622519bafb6c8;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_TexManager;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_458668bb1305aeecb0f754db4460bf6e_2, codeobj_458668bb1305aeecb0f754db4460bf6e, module_matplotlib$texmanager, sizeof(void *) );
        frame_458668bb1305aeecb0f754db4460bf6e_2 = cache_frame_458668bb1305aeecb0f754db4460bf6e_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_458668bb1305aeecb0f754db4460bf6e_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_458668bb1305aeecb0f754db4460bf6e_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_4;
            tmp_called_instance_2 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_mpl );

            if ( tmp_called_instance_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_mpl );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_mpl );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "mpl" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 56;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_instance_2 = tmp_mvar_value_4;
                Py_INCREF( tmp_called_instance_2 );
                }
            }

            frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 56;
            tmp_dictset_value = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_cachedir );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_cachedir, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_cachedir );

            if ( tmp_compexpr_left_1 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cachedir" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 57;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_6 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_5;
                PyObject *tmp_source_name_6;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                tmp_source_name_6 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_os );

                if ( tmp_source_name_6 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_os );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 58;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_source_name_6 = tmp_mvar_value_5;
                    Py_INCREF( tmp_source_name_6 );
                    }
                }

                tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_path );
                Py_DECREF( tmp_source_name_6 );
                if ( tmp_source_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_join );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_args_element_name_1 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_cachedir );

                if ( tmp_args_element_name_1 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                    Py_DECREF( tmp_called_name_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cachedir" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 58;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                if ( tmp_args_element_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    exception_lineno = 58;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_args_element_name_2 = const_str_digest_e01874232ccefaf0d37dc214b5be49b0;
                frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 58;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_1 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_texcache, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
            }
            {
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_7;
                PyObject *tmp_called_name_4;
                PyObject *tmp_mvar_value_6;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_call_result_1;
                PyObject *tmp_kw_name_2;
                tmp_called_name_4 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_Path );

                if ( tmp_called_name_4 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_Path );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Path );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Path" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 59;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_called_name_4 = tmp_mvar_value_6;
                    Py_INCREF( tmp_called_name_4 );
                    }
                }

                tmp_args_element_name_3 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_texcache );

                if ( tmp_args_element_name_3 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "texcache" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 59;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                if ( tmp_args_element_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 59;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 59;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_source_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_source_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 59;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_mkdir );
                Py_DECREF( tmp_source_name_7 );
                if ( tmp_called_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 59;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_kw_name_2 = PyDict_Copy( const_dict_b38ee67c68e7540ea798119f786298f3 );
                frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 59;
                tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_kw_name_2 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 59;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            goto branch_end_4;
            branch_no_4:;
            tmp_dictset_value = Py_None;
            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_texcache, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            branch_end_4:;
        }
        tmp_dictset_value = PyDict_New();
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_rgba_arrayd, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = PyDict_New();
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_grey_arrayd, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_tuple_str_plain_cmr_str_empty_tuple;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_serif, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_tuple_str_plain_cmss_str_empty_tuple;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_sans_serif, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_tuple_str_plain_cmtt_str_empty_tuple;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_monospace, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_tuple_str_plain_pzc_str_digest_647a68f81e76836af6331dea2d40b163_tuple;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_cursive, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_str_plain_serif;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_font_family, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_tuple_02dd58a4a9cce7c6bd0b1b2760bd591a_tuple;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_font_families, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = PyDict_Copy( const_dict_116370359a70a271fa1bb0c2b4992e02 );
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_font_info, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain__rc_cache, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_tuple_arg_1;
            tmp_left_name_2 = const_tuple_5be502270cb646a1dc150e71300691a3_tuple;
            tmp_res = MAPPING_HAS_ITEM( locals_matplotlib$texmanager_49, const_str_plain_tuple );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_5 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_tuple );

            if ( tmp_called_name_5 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tuple" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            {
                PyObject *tmp_assign_source_26;
                PyObject *tmp_iter_arg_1;
                tmp_iter_arg_1 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_font_families );

                if ( tmp_iter_arg_1 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "font_families" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_26 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                assert( tmp_TexManager$genexpr_1__$0 == NULL );
                tmp_TexManager$genexpr_1__$0 = tmp_assign_source_26;
            }
            // Tried code:
            tmp_args_element_name_4 = matplotlib$texmanager$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_4)->m_closure[0] = PyCell_NEW0( tmp_TexManager$genexpr_1__$0 );


            goto try_return_handler_5;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_5:;
            CHECK_OBJECT( (PyObject *)tmp_TexManager$genexpr_1__$0 );
            Py_DECREF( tmp_TexManager$genexpr_1__$0 );
            tmp_TexManager$genexpr_1__$0 = NULL;

            goto outline_result_2;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_TexManager$genexpr_1__$0 );
            Py_DECREF( tmp_TexManager$genexpr_1__$0 );
            tmp_TexManager$genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
            return MOD_RETURN_VALUE( NULL );
            outline_result_2:;
            frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 97;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_right_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            {
                PyObject *tmp_assign_source_27;
                PyObject *tmp_iter_arg_2;
                tmp_iter_arg_2 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_font_families );

                if ( tmp_iter_arg_2 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "font_families" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                if ( tmp_iter_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                tmp_assign_source_27 = MAKE_ITERATOR( tmp_iter_arg_2 );
                Py_DECREF( tmp_iter_arg_2 );
                if ( tmp_assign_source_27 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 97;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                assert( tmp_TexManager$genexpr_1__$0 == NULL );
                tmp_TexManager$genexpr_1__$0 = tmp_assign_source_27;
            }
            // Tried code:
            tmp_tuple_arg_1 = matplotlib$texmanager$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_tuple_arg_1)->m_closure[0] = PyCell_NEW0( tmp_TexManager$genexpr_1__$0 );


            goto try_return_handler_6;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_6:;
            CHECK_OBJECT( (PyObject *)tmp_TexManager$genexpr_1__$0 );
            Py_DECREF( tmp_TexManager$genexpr_1__$0 );
            tmp_TexManager$genexpr_1__$0 = NULL;

            goto outline_result_3;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_TexManager$genexpr_1__$0 );
            Py_DECREF( tmp_TexManager$genexpr_1__$0 );
            tmp_TexManager$genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
            return MOD_RETURN_VALUE( NULL );
            outline_result_3:;
            tmp_right_name_2 = PySequence_Tuple( tmp_tuple_arg_1 );
            Py_DECREF( tmp_tuple_arg_1 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_dictset_value = BINARY_OPERATION_ADD_TUPLE_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 96;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain__rc_cache_keys, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 95;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_args_element_name_5;
            tmp_called_instance_3 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_functools );

            if ( tmp_called_instance_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_functools );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 99;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_instance_3 = tmp_mvar_value_7;
                Py_INCREF( tmp_called_instance_3 );
                }
            }

            frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 99;
            tmp_called_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_lru_cache );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_matplotlib$texmanager$$$function_1___new__(  );



            frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 99;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain___new__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_2__reinit(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain__reinit, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_3_get_basefile( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_basefile, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_4_get_font_config(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_font_config, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_5_get_font_preamble(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_font_preamble, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_6_get_custom_preamble(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_custom_preamble, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_7_make_tex(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_make_tex, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_mvar_value_8;
            tmp_called_instance_4 = PyObject_GetItem( locals_matplotlib$texmanager_49, const_str_plain_re );

            if ( tmp_called_instance_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_re );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 237;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_instance_4 = tmp_mvar_value_8;
                Py_INCREF( tmp_called_instance_4 );
                }
            }

            frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame.f_lineno = 237;
            tmp_dictset_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_984c8f5dae87f53dab68d11cd420a5d3_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain__re_vbox, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_8_make_tex_preview(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_make_tex_preview, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_9__run_checked_subprocess(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain__run_checked_subprocess, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_10_make_dvi(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_make_dvi, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 321;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_11_make_dvi_preview(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_make_dvi_preview, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 348;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_12_make_png(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_make_png, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 382;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_none_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_13_get_grey( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_grey, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 398;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_3;
            tmp_defaults_3 = const_tuple_none_none_tuple_int_0_int_0_int_0_tuple_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_14_get_rgba( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_rgba, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 409;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_4;
            tmp_defaults_4 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_4 );
            tmp_dictset_value = MAKE_FUNCTION_matplotlib$texmanager$$$function_15_get_text_width_height_descent( tmp_defaults_4 );



            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain_get_text_width_height_descent, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 426;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_458668bb1305aeecb0f754db4460bf6e_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_458668bb1305aeecb0f754db4460bf6e_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_458668bb1305aeecb0f754db4460bf6e_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_458668bb1305aeecb0f754db4460bf6e_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_458668bb1305aeecb0f754db4460bf6e_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_458668bb1305aeecb0f754db4460bf6e_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_458668bb1305aeecb0f754db4460bf6e_2 == cache_frame_458668bb1305aeecb0f754db4460bf6e_2 )
        {
            Py_DECREF( frame_458668bb1305aeecb0f754db4460bf6e_2 );
        }
        cache_frame_458668bb1305aeecb0f754db4460bf6e_2 = NULL;

        assertFrameObject( frame_458668bb1305aeecb0f754db4460bf6e_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_2 = tmp_class_creation_1__bases;
            tmp_compexpr_right_2 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_4;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_matplotlib$texmanager_49, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_4;
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_7 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_TexManager;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_matplotlib$texmanager_49;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_1__class_decl_dict;
            frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame.f_lineno = 49;
            tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_2, tmp_kw_name_3 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_28;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_25 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_25 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_matplotlib$texmanager_49 );
        locals_matplotlib$texmanager_49 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$texmanager_49 );
        locals_matplotlib$texmanager_49 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$texmanager );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 49;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$texmanager, (Nuitka_StringObject *)const_str_plain_TexManager, tmp_assign_source_25 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_09f11e489fd7ceaa2d038df5e5839b15 );
#endif
    popFrameStack();

    assertFrameObject( frame_09f11e489fd7ceaa2d038df5e5839b15 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_09f11e489fd7ceaa2d038df5e5839b15 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_09f11e489fd7ceaa2d038df5e5839b15, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_09f11e489fd7ceaa2d038df5e5839b15->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_09f11e489fd7ceaa2d038df5e5839b15, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;


    return MOD_RETURN_VALUE( module_matplotlib$texmanager );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
