/* Generated code for Python module 'jedi.evaluate.stdlib'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jedi$evaluate$stdlib" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jedi$evaluate$stdlib;
PyDictObject *moduledict_jedi$evaluate$stdlib;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain___name__;
static PyObject *const_tuple_337c362010bc06f6b8ae08f820125e79_tuple;
extern PyObject *const_str_plain_builtins;
extern PyObject *const_str_plain_infer;
static PyObject *const_str_plain_builtins_super;
static PyObject *const_str_digest_bc33aa076dc4474ec3baf1b192e9fc32;
static PyObject *const_tuple_9399f4707b0ac471391b55085d6d0858_tuple;
extern PyObject *const_str_plain_None;
static PyObject *const_tuple_6d103a741b243e268a279146e9f9d03c_tuple;
extern PyObject *const_str_plain_next;
static PyObject *const_tuple_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0_str_empty_tuple;
static PyObject *const_str_digest_1db8cd3ef2f7d1f4e6a2e71b84ba2dd3;
extern PyObject *const_str_plain_iter_classdefs;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_7ea303fd6c9d57c037274ff98ba3dd3f;
static PyObject *const_tuple_str_plain__field_template_tuple;
static PyObject *const_str_plain__repr_template;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_tuple_b40d30098df559b8e31032c3471a605b_tuple;
extern PyObject *const_str_plain_builtin_from_name;
extern PyObject *const_str_plain_BoundMethod;
extern PyObject *const_str_plain_string_name;
extern PyObject *const_str_plain_bases;
static PyObject *const_tuple_1ea377787bdea67875561c820b9b00dc_tuple;
static PyObject *const_tuple_53878258122763aeac3edebbdb8d16b2_tuple;
extern PyObject *const_str_digest_74a64e2326d0e94475afe0623d07b03d;
extern PyObject *const_str_plain_load;
static PyObject *const_tuple_2486b681f5e751c4123ea32d016a9298_tuple;
extern PyObject *const_str_plain_cls_or_tup;
extern PyObject *const_str_plain_get_safe_value;
static PyObject *const_tuple_str_chr_39_str_empty_tuple;
extern PyObject *const_str_plain_objects;
extern PyObject *const_str_plain_major;
extern PyObject *const_str_plain_reversed;
extern PyObject *const_str_plain_name;
static PyObject *const_str_plain_sequences;
static PyObject *const_tuple_str_plain_evaluator_str_plain_firsts_tuple;
extern PyObject *const_str_plain_from_iterable;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_pop;
extern PyObject *const_int_0;
static PyObject *const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple;
extern PyObject *const_str_plain_code;
extern PyObject *const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
static PyObject *const_str_digest_c31205f7b9f388d028503e160aac47b4;
extern PyObject *const_str_plain_ValuesArguments;
extern PyObject *const_str_plain_defaults;
static PyObject *const_tuple_str_digest_568d907a1a4a83fadc4155df91d2cd9a_tuple;
extern PyObject *const_str_plain_type;
static PyObject *const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple;
extern PyObject *const_str_plain_color;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_LazyTreeContext;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_tuple_str_digest_bc33aa076dc4474ec3baf1b192e9fc32_tuple;
static PyObject *const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple;
extern PyObject *const_str_plain_getattr;
extern PyObject *const_str_plain_CompiledInstance;
extern PyObject *const_tuple_str_plain_compiled_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain_module_name;
static PyObject *const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple;
static PyObject *const_str_plain_SuperInstance;
extern PyObject *const_str_plain_jedi;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_include_self_names;
extern PyObject *const_str_plain_module;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_format;
extern PyObject *const_tuple_str_chr_44_str_space_tuple;
static PyObject *const_tuple_str_digest_dcc5f9e29092af34f59364b2136f9ebf_tuple;
static PyObject *const_str_digest_f374c4f59bafcb2c22d42a0e88e08b1a;
extern PyObject *const_str_plain_index;
static PyObject *const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple;
extern PyObject *const_str_plain_string;
static PyObject *const_str_digest_dfb80871e427217dfe761603c12c346d;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_568d907a1a4a83fadc4155df91d2cd9a;
extern PyObject *const_str_plain_unpack;
extern PyObject *const_str_plain_parso;
static PyObject *const_tuple_str_plain_func_str_plain_wrapper_str_plain_string_tuple;
extern PyObject *const_str_digest_4b6ae0a4a7c5ef73221de5bb2b8ff0e4;
extern PyObject *const_str_plain_cn;
extern PyObject *const_str_plain_iterators;
extern PyObject *const_str_plain_filter;
extern PyObject *const_str_plain_MAGENTA;
extern PyObject *const_str_plain_func;
static PyObject *const_str_plain_argument_clinic;
static PyObject *const_str_digest_4e3cbccd16e9b40484f60435e970bec1;
extern PyObject *const_str_plain_builtins_module;
extern PyObject *const_str_plain_names;
static PyObject *const_tuple_str_digest_71d72ea4a426b505f2886f81f114ef20_tuple;
extern PyObject *const_str_plain_wrapper;
static PyObject *const_str_plain_repr_fmt;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_tuple_ca55646d5a4f44b7976271ef44596511_tuple;
extern PyObject *const_str_plain_FunctionExecutionContext;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_plain__field_template;
static PyObject *const_str_digest_e3bbab13d8c6c6a5fae7b76afdeef3aa;
extern PyObject *const_str_plain_n;
static PyObject *const_str_plain_py_mro;
extern PyObject *const_str_chr_39;
extern PyObject *const_tuple_str_plain_arguments_tuple;
extern PyObject *const_str_plain_collections;
extern PyObject *const_str_plain_message;
static PyObject *const_str_plain__implemented;
extern PyObject *const_str_plain_context;
static PyObject *const_str_plain_builtins_type;
static PyObject *const_tuple_4c97001935895517801cfa58e915a217_tuple;
extern PyObject *const_str_plain_enumerate;
static PyObject *const_str_digest_86d2373b6d1e6c0afaec8f7b162c3e3a;
extern PyObject *const_str_plain_execute;
extern PyObject *const_str_plain_grammar;
static PyObject *const_tuple_02831cdb2915c664588c38506bed65cc_tuple;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_digest_36e46c252a3e3bf20afff1d05096f2ab;
extern PyObject *const_str_plain_mro;
static PyObject *const_str_plain_collections_namedtuple;
static PyObject *const_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_iterate;
extern PyObject *const_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_4783aaea9f206425e10f79f8d9442dde;
extern PyObject *const_str_plain_warning;
static PyObject *const_str_plain_want_arguments;
extern PyObject *const_str_plain_base;
extern PyObject *const_str_plain_namedtuple;
static PyObject *const_str_plain_su;
extern PyObject *const_str_plain_v;
static PyObject *const_str_digest_1b1724c41b1bbb89d30a46b326b95dad;
static PyObject *const_tuple_str_plain__class_template_tuple;
static PyObject *const_str_plain_get_var;
extern PyObject *const_str_digest_785ebe15053a4fa731c98e296de8edd7;
extern PyObject *const_str_plain_isinstance;
static PyObject *const_str_digest_571dc04c354a474fb7f3309327d95d7a;
extern PyObject *const_str_plain___next__;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_plain_copy;
extern PyObject *const_str_plain_code_lines;
extern PyObject *const_str_plain_key;
static PyObject *const_str_plain_field_defs;
static PyObject *const_str_plain_num_fields;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_types;
extern PyObject *const_str_plain_parse;
static PyObject *const_str_digest_d24bac973c171b9040e8ff82c6c4078c;
extern PyObject *const_str_plain_loads;
extern PyObject *const_str_plain_evaluator;
extern PyObject *const_str_plain_has_location;
static PyObject *const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple;
extern PyObject *const_str_empty;
static PyObject *const_tuple_str_digest_c19157e8112b641864481c8e6f9ac442_tuple;
extern PyObject *const_str_plain_py__mro__;
extern PyObject *const_str_plain_force_unicode;
extern PyObject *const_str_plain_seq;
extern PyObject *const_str_plain_result;
static PyObject *const_str_plain_rev;
extern PyObject *const_str_plain_NotInStdLib;
extern PyObject *const_str_plain_analysis;
extern PyObject *const_tuple_str_plain_iterable_tuple;
extern PyObject *const_tuple_str_plain_LazyTreeContext_tuple;
extern PyObject *const_str_plain_iterable;
extern PyObject *const_str_plain_json;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_classes;
static PyObject *const_str_plain_collections_context;
extern PyObject *const_str_digest_3e1604d04ce104740ab4a0cd2cf306db;
extern PyObject *const_str_plain_from_sets;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_25b544778da07cbf7f7f7816b8dd5745;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_add;
extern PyObject *const_str_plain_is_class;
extern PyObject *const_str_plain_execute_evaluated;
static PyObject *const_str_plain_want_context;
extern PyObject *const_str_plain__context;
extern PyObject *const_str_plain_node;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_digest_ee4849b262684aea622e900a24a8a0bd;
static PyObject *const_str_digest_c53d2a4a26885939c8baa5017b23583f;
extern PyObject *const_str_plain_typename;
extern PyObject *const_str_plain_environment;
extern PyObject *const_tuple_str_plain_analysis_tuple;
extern PyObject *const_str_plain_NO_CONTEXTS;
extern PyObject *const_str_plain_var_args;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_space;
extern PyObject *const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
static PyObject *const_tuple_str_digest_c31205f7b9f388d028503e160aac47b4_tuple;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain__fields;
extern PyObject *const_str_digest_93e555a5b1643558b28cf6b2256158dc;
static PyObject *const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple;
extern PyObject *const_str_plain_list;
extern PyObject *const_dict_c96b872716a13ad444d819f9c4f633b7;
extern PyObject *const_str_plain_keep_arguments_param;
static PyObject *const_tuple_str_plain_lazy_context_str_plain_v_tuple;
static PyObject *const_tuple_type_LookupError_tuple;
static PyObject *const_str_digest_8034a5ea6d3f98c35e9cf5a85a9b128c;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_plain_lazy_context;
static PyObject *const_str_plain_generated_class;
static PyObject *const_dict_de706df0108fd8fcd92a5da8a4cc94aa;
extern PyObject *const_str_plain_py__class__;
extern PyObject *const_str_plain_iterator;
static PyObject *const_str_digest_71d72ea4a426b505f2886f81f114ef20;
extern PyObject *const_dict_fc2d0675235f4f6c6fd56070d077c5e5;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_dbg;
extern PyObject *const_str_plain_debug;
static PyObject *const_str_plain__follow_param;
static PyObject *const_dict_0ef631920779735f6d8ddb2e395f99d0;
static PyObject *const_tuple_str_digest_4e3cbccd16e9b40484f60435e970bec1_tuple;
extern PyObject *const_str_plain_compiled;
extern PyObject *const_str_plain_split_lines;
extern PyObject *const_str_plain_version_info;
static PyObject *const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple;
extern PyObject *const_str_plain_get;
static PyObject *const_str_digest_042c753535689c6a18265d88587540e6;
static PyObject *const_tuple_a9471d258cf3258db6320fde70507329_tuple;
extern PyObject *const_str_plain_dicts;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_str_plain_builtins_isinstance;
extern PyObject *const_tuple_str_plain_force_unicode_tuple;
extern PyObject *const_tuple_false_false_false_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_str_plain__return_first_param;
static PyObject *const_tuple_a122947201318f9eb69dff98cf405b45_tuple;
extern PyObject *const_str_plain_obj_name;
extern PyObject *const_str_plain_args;
extern PyObject *const_slice_int_pos_1_int_neg_1_none;
extern PyObject *const_str_plain_get_filters;
extern PyObject *const_str_plain_instance;
extern PyObject *const_str_plain_InstanceArguments;
static PyObject *const_str_digest_aa9336f5c47581fe04ea84da97f82efe;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_super;
static PyObject *const_str_digest_f4f9c2a9113d5b51fa01e3be21e1a4ea;
static PyObject *const_str_plain_builtins_getattr;
static PyObject *const_str_digest_ec4e4044dc14fa3687c4a45457ca7489;
extern PyObject *const_str_chr_44;
extern PyObject *const_str_plain_arguments;
static PyObject *const_str_plain_bool_results;
extern PyObject *const_str_plain_FakeSequence;
static PyObject *const_tuple_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a_tuple;
static PyObject *const_str_plain_firsts;
extern PyObject *const_tuple_str_plain_is_string_tuple;
extern PyObject *const_tuple_str_plain_debug_tuple;
extern PyObject *const_str_plain_deepcopy;
extern PyObject *const_str_plain_py__bases__;
static PyObject *const_str_plain__class_template_set;
extern PyObject *const_str_plain_py__iter__;
static PyObject *const_str_plain__NAMEDTUPLE_INIT;
extern PyObject *const_str_plain_ordered;
static PyObject *const_str_digest_723974f56302e163bbe2d0cac1cc3598;
extern PyObject *const_str_plain_get_root_context;
static PyObject *const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple;
static PyObject *const_dict_f0ba030d13b5b64a31319be3eaa33cee;
extern PyObject *const_str_plain_ContextualizedNode;
extern PyObject *const_str_plain_ModuleContext;
extern PyObject *const_str_plain_CompiledObject;
static PyObject *const_str_digest_dcc5f9e29092af34f59364b2136f9ebf;
static PyObject *const_str_digest_9c5b414e40bcc6854c5f0b647dc1b864;
static PyObject *const_dict_d1013312da29be8dcbc7a3855329c0aa;
extern PyObject *const_str_plain_Sequence;
extern PyObject *const_str_plain_ClassContext;
static PyObject *const_set_3ba3dcb5a42392616e5741230a6a5b3b;
extern PyObject *const_str_plain_repack_with_argument_clinic;
static PyObject *const_str_plain_mro_func;
extern PyObject *const_str_plain_context_set;
extern PyObject *const_str_plain_o;
static PyObject *const_tuple_str_plain_name_str_plain_x_str_plain_collections_context_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_digest_4ea3c739c0994e1c59a1638cbf9bd81c;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_arg_list;
static PyObject *const_str_plain_field_names;
extern PyObject *const_str_plain_ContextSet;
extern PyObject *const_tuple_9bdc0c05ddc9014d7f1e078f74d8ea62_tuple;
static PyObject *const_str_plain__class_template;
extern PyObject *const_str_plain_AbstractInstanceContext;
static PyObject *const_str_plain_want_obj;
static PyObject *const_dict_31468f992b2dfacc6e9a4f2c8c95b093;
extern PyObject *const_str_plain_is_string;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain_fields;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_keepends;
static PyObject *const_str_plain_builtins_next;
static PyObject *const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple;
extern PyObject *const_int_pos_2;
static PyObject *const_str_plain_builtins_reversed;
extern PyObject *const_str_plain_obj;
static PyObject *const_tuple_str_plain__repr_template_tuple;
extern PyObject *const_str_plain_parent_context;
extern PyObject *const_str_plain_py__getattribute__;
static PyObject *const_str_digest_c19157e8112b641864481c8e6f9ac442;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_337c362010bc06f6b8ae08f820125e79_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_337c362010bc06f6b8ae08f820125e79_tuple, 0, const_str_plain_ValuesArguments ); Py_INCREF( const_str_plain_ValuesArguments );
    PyTuple_SET_ITEM( const_tuple_337c362010bc06f6b8ae08f820125e79_tuple, 1, const_str_plain_repack_with_argument_clinic ); Py_INCREF( const_str_plain_repack_with_argument_clinic );
    const_str_plain_builtins_super = UNSTREAM_STRING_ASCII( &constant_bin[ 1016912 ], 14, 1 );
    const_str_digest_bc33aa076dc4474ec3baf1b192e9fc32 = UNSTREAM_STRING_ASCII( &constant_bin[ 1016926 ], 12, 0 );
    const_tuple_9399f4707b0ac471391b55085d6d0858_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_9399f4707b0ac471391b55085d6d0858_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_9399f4707b0ac471391b55085d6d0858_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_get_var = UNSTREAM_STRING_ASCII( &constant_bin[ 1016938 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_9399f4707b0ac471391b55085d6d0858_tuple, 2, const_str_plain_get_var ); Py_INCREF( const_str_plain_get_var );
    const_tuple_6d103a741b243e268a279146e9f9d03c_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_6d103a741b243e268a279146e9f9d03c_tuple, 0, const_str_plain_ContextualizedNode ); Py_INCREF( const_str_plain_ContextualizedNode );
    PyTuple_SET_ITEM( const_tuple_6d103a741b243e268a279146e9f9d03c_tuple, 1, const_str_plain_NO_CONTEXTS ); Py_INCREF( const_str_plain_NO_CONTEXTS );
    PyTuple_SET_ITEM( const_tuple_6d103a741b243e268a279146e9f9d03c_tuple, 2, const_str_plain_ContextSet ); Py_INCREF( const_str_plain_ContextSet );
    const_tuple_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0_str_empty_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0_str_empty_tuple, 0, const_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0 ); Py_INCREF( const_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_digest_1db8cd3ef2f7d1f4e6a2e71b84ba2dd3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1016945 ], 76, 0 );
    const_str_digest_7ea303fd6c9d57c037274ff98ba3dd3f = UNSTREAM_STRING_ASCII( &constant_bin[ 1017021 ], 161, 0 );
    const_tuple_str_plain__field_template_tuple = PyTuple_New( 1 );
    const_str_plain__field_template = UNSTREAM_STRING_ASCII( &constant_bin[ 1017182 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__field_template_tuple, 0, const_str_plain__field_template ); Py_INCREF( const_str_plain__field_template );
    const_str_plain__repr_template = UNSTREAM_STRING_ASCII( &constant_bin[ 1017197 ], 14, 1 );
    const_tuple_b40d30098df559b8e31032c3471a605b_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 1, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 2, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 3, const_str_plain_obj_name ); Py_INCREF( const_str_plain_obj_name );
    PyTuple_SET_ITEM( const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 4, const_str_plain_module_name ); Py_INCREF( const_str_plain_module_name );
    PyTuple_SET_ITEM( const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 5, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_tuple_1ea377787bdea67875561c820b9b00dc_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 1, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 2, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 4, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 5, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_str_plain_want_context = UNSTREAM_STRING_ASCII( &constant_bin[ 1017211 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 6, const_str_plain_want_context ); Py_INCREF( const_str_plain_want_context );
    const_str_plain_want_obj = UNSTREAM_STRING_ASCII( &constant_bin[ 1017223 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 7, const_str_plain_want_obj ); Py_INCREF( const_str_plain_want_obj );
    const_str_plain_want_arguments = UNSTREAM_STRING_ASCII( &constant_bin[ 1017231 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 8, const_str_plain_want_arguments ); Py_INCREF( const_str_plain_want_arguments );
    PyTuple_SET_ITEM( const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 9, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_tuple_53878258122763aeac3edebbdb8d16b2_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_53878258122763aeac3edebbdb8d16b2_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_53878258122763aeac3edebbdb8d16b2_tuple, 1, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_53878258122763aeac3edebbdb8d16b2_tuple, 2, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    const_tuple_2486b681f5e751c4123ea32d016a9298_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_2486b681f5e751c4123ea32d016a9298_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_2486b681f5e751c4123ea32d016a9298_tuple, 1, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_2486b681f5e751c4123ea32d016a9298_tuple, 2, const_str_plain_mro ); Py_INCREF( const_str_plain_mro );
    const_tuple_str_chr_39_str_empty_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_chr_39_str_empty_tuple, 0, const_str_chr_39 ); Py_INCREF( const_str_chr_39 );
    PyTuple_SET_ITEM( const_tuple_str_chr_39_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_plain_sequences = UNSTREAM_STRING_ASCII( &constant_bin[ 6577 ], 9, 1 );
    const_tuple_str_plain_evaluator_str_plain_firsts_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_evaluator_str_plain_firsts_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    const_str_plain_firsts = UNSTREAM_STRING_ASCII( &constant_bin[ 1017245 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_evaluator_str_plain_firsts_tuple, 1, const_str_plain_firsts ); Py_INCREF( const_str_plain_firsts );
    const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple, 1, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple, 2, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    const_str_digest_c31205f7b9f388d028503e160aac47b4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1017251 ], 26, 0 );
    const_tuple_str_digest_568d907a1a4a83fadc4155df91d2cd9a_tuple = PyTuple_New( 1 );
    const_str_digest_568d907a1a4a83fadc4155df91d2cd9a = UNSTREAM_STRING_ASCII( &constant_bin[ 1017277 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_568d907a1a4a83fadc4155df91d2cd9a_tuple, 0, const_str_digest_568d907a1a4a83fadc4155df91d2cd9a ); Py_INCREF( const_str_digest_568d907a1a4a83fadc4155df91d2cd9a );
    const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple, 1, const_str_plain_objects ); Py_INCREF( const_str_plain_objects );
    PyTuple_SET_ITEM( const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple, 2, const_str_plain_bases ); Py_INCREF( const_str_plain_bases );
    PyTuple_SET_ITEM( const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple, 3, const_str_plain_dicts ); Py_INCREF( const_str_plain_dicts );
    const_tuple_str_digest_bc33aa076dc4474ec3baf1b192e9fc32_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_bc33aa076dc4474ec3baf1b192e9fc32_tuple, 0, const_str_digest_bc33aa076dc4474ec3baf1b192e9fc32 ); Py_INCREF( const_str_digest_bc33aa076dc4474ec3baf1b192e9fc32 );
    const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple, 1, const_str_plain_index ); Py_INCREF( const_str_plain_index );
    PyTuple_SET_ITEM( const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple, 3, const_str_plain_get_var ); Py_INCREF( const_str_plain_get_var );
    const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 1, const_str_plain_sequences ); Py_INCREF( const_str_plain_sequences );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 2, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 3, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 4, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 5, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 6, const_str_plain_cn ); Py_INCREF( const_str_plain_cn );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 7, const_str_plain_ordered ); Py_INCREF( const_str_plain_ordered );
    const_str_plain_rev = UNSTREAM_STRING_ASCII( &constant_bin[ 4062 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 8, const_str_plain_rev ); Py_INCREF( const_str_plain_rev );
    PyTuple_SET_ITEM( const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 9, const_str_plain_seq ); Py_INCREF( const_str_plain_seq );
    const_str_plain_SuperInstance = UNSTREAM_STRING_ASCII( &constant_bin[ 1017301 ], 13, 1 );
    const_tuple_str_digest_dcc5f9e29092af34f59364b2136f9ebf_tuple = PyTuple_New( 1 );
    const_str_digest_dcc5f9e29092af34f59364b2136f9ebf = UNSTREAM_STRING_ASCII( &constant_bin[ 1017314 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_dcc5f9e29092af34f59364b2136f9ebf_tuple, 0, const_str_digest_dcc5f9e29092af34f59364b2136f9ebf ); Py_INCREF( const_str_digest_dcc5f9e29092af34f59364b2136f9ebf );
    const_str_digest_f374c4f59bafcb2c22d42a0e88e08b1a = UNSTREAM_STRING_ASCII( &constant_bin[ 1017322 ], 16, 0 );
    const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 1, const_str_plain_objects ); Py_INCREF( const_str_plain_objects );
    PyTuple_SET_ITEM( const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 2, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 3, const_str_plain_defaults ); Py_INCREF( const_str_plain_defaults );
    PyTuple_SET_ITEM( const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 4, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 5, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_digest_dfb80871e427217dfe761603c12c346d = UNSTREAM_STRING_ASCII( &constant_bin[ 1017338 ], 23, 0 );
    const_tuple_str_plain_func_str_plain_wrapper_str_plain_string_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_func_str_plain_wrapper_str_plain_string_tuple, 0, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_str_plain_func_str_plain_wrapper_str_plain_string_tuple, 1, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    PyTuple_SET_ITEM( const_tuple_str_plain_func_str_plain_wrapper_str_plain_string_tuple, 2, const_str_plain_string ); Py_INCREF( const_str_plain_string );
    const_str_plain_argument_clinic = UNSTREAM_STRING_ASCII( &constant_bin[ 964893 ], 15, 1 );
    const_str_digest_4e3cbccd16e9b40484f60435e970bec1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1017361 ], 26, 0 );
    const_tuple_str_digest_71d72ea4a426b505f2886f81f114ef20_tuple = PyTuple_New( 1 );
    const_str_digest_71d72ea4a426b505f2886f81f114ef20 = UNSTREAM_STRING_ASCII( &constant_bin[ 1017387 ], 22, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_71d72ea4a426b505f2886f81f114ef20_tuple, 0, const_str_digest_71d72ea4a426b505f2886f81f114ef20 ); Py_INCREF( const_str_digest_71d72ea4a426b505f2886f81f114ef20 );
    const_str_plain_repr_fmt = UNSTREAM_STRING_ASCII( &constant_bin[ 1017409 ], 8, 1 );
    const_tuple_ca55646d5a4f44b7976271ef44596511_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_ca55646d5a4f44b7976271ef44596511_tuple, 0, const_str_plain_ClassContext ); Py_INCREF( const_str_plain_ClassContext );
    PyTuple_SET_ITEM( const_tuple_ca55646d5a4f44b7976271ef44596511_tuple, 1, const_str_plain_ModuleContext ); Py_INCREF( const_str_plain_ModuleContext );
    PyTuple_SET_ITEM( const_tuple_ca55646d5a4f44b7976271ef44596511_tuple, 2, const_str_plain_FunctionExecutionContext ); Py_INCREF( const_str_plain_FunctionExecutionContext );
    const_str_digest_e3bbab13d8c6c6a5fae7b76afdeef3aa = UNSTREAM_STRING_ASCII( &constant_bin[ 1017417 ], 22, 0 );
    const_str_plain_py_mro = UNSTREAM_STRING_ASCII( &constant_bin[ 1017439 ], 6, 1 );
    const_str_plain__implemented = UNSTREAM_STRING_ASCII( &constant_bin[ 546790 ], 12, 1 );
    const_str_plain_builtins_type = UNSTREAM_STRING_ASCII( &constant_bin[ 1017445 ], 13, 1 );
    const_tuple_4c97001935895517801cfa58e915a217_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_4c97001935895517801cfa58e915a217_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_4c97001935895517801cfa58e915a217_tuple, 1, const_str_plain_iterators ); Py_INCREF( const_str_plain_iterators );
    PyTuple_SET_ITEM( const_tuple_4c97001935895517801cfa58e915a217_tuple, 2, const_str_plain_defaults ); Py_INCREF( const_str_plain_defaults );
    PyTuple_SET_ITEM( const_tuple_4c97001935895517801cfa58e915a217_tuple, 3, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_4c97001935895517801cfa58e915a217_tuple, 4, const_str_plain_context_set ); Py_INCREF( const_str_plain_context_set );
    PyTuple_SET_ITEM( const_tuple_4c97001935895517801cfa58e915a217_tuple, 5, const_str_plain_iterator ); Py_INCREF( const_str_plain_iterator );
    const_str_digest_86d2373b6d1e6c0afaec8f7b162c3e3a = UNSTREAM_STRING_ASCII( &constant_bin[ 1017458 ], 43, 0 );
    const_tuple_02831cdb2915c664588c38506bed65cc_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_02831cdb2915c664588c38506bed65cc_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_02831cdb2915c664588c38506bed65cc_tuple, 1, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_02831cdb2915c664588c38506bed65cc_tuple, 2, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    const_str_plain_su = UNSTREAM_STRING_ASCII( &constant_bin[ 1259 ], 2, 1 );
    PyTuple_SET_ITEM( const_tuple_02831cdb2915c664588c38506bed65cc_tuple, 3, const_str_plain_su ); Py_INCREF( const_str_plain_su );
    PyTuple_SET_ITEM( const_tuple_02831cdb2915c664588c38506bed65cc_tuple, 4, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_plain_collections_namedtuple = UNSTREAM_STRING_ASCII( &constant_bin[ 1017501 ], 22, 1 );
    const_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a = UNSTREAM_STRING_ASCII( &constant_bin[ 1017523 ], 16, 0 );
    const_str_digest_4783aaea9f206425e10f79f8d9442dde = UNSTREAM_STRING_ASCII( &constant_bin[ 1017539 ], 91, 0 );
    const_str_digest_1b1724c41b1bbb89d30a46b326b95dad = UNSTREAM_STRING_ASCII( &constant_bin[ 1017630 ], 29, 0 );
    const_tuple_str_plain__class_template_tuple = PyTuple_New( 1 );
    const_str_plain__class_template = UNSTREAM_STRING_ASCII( &constant_bin[ 120580 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__class_template_tuple, 0, const_str_plain__class_template ); Py_INCREF( const_str_plain__class_template );
    const_str_digest_571dc04c354a474fb7f3309327d95d7a = UNSTREAM_STRING_ASCII( &constant_bin[ 1017659 ], 15, 0 );
    const_str_plain_field_defs = UNSTREAM_STRING_ASCII( &constant_bin[ 1017674 ], 10, 1 );
    const_str_plain_num_fields = UNSTREAM_STRING_ASCII( &constant_bin[ 1017684 ], 10, 1 );
    const_str_digest_d24bac973c171b9040e8ff82c6c4078c = UNSTREAM_STRING_ASCII( &constant_bin[ 1017694 ], 265, 0 );
    const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple = PyTuple_New( 14 );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 1, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 2, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    const_str_plain_collections_context = UNSTREAM_STRING_ASCII( &constant_bin[ 1017959 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 3, const_str_plain_collections_context ); Py_INCREF( const_str_plain_collections_context );
    const_str_plain__class_template_set = UNSTREAM_STRING_ASCII( &constant_bin[ 1017978 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 4, const_str_plain__class_template_set ); Py_INCREF( const_str_plain__class_template_set );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 5, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 6, const_str_plain__fields ); Py_INCREF( const_str_plain__fields );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 7, const_str_plain_fields ); Py_INCREF( const_str_plain_fields );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 8, const_str_plain_get_var ); Py_INCREF( const_str_plain_get_var );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 9, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 10, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 11, const_str_plain_module ); Py_INCREF( const_str_plain_module );
    const_str_plain_generated_class = UNSTREAM_STRING_ASCII( &constant_bin[ 1017997 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 12, const_str_plain_generated_class ); Py_INCREF( const_str_plain_generated_class );
    PyTuple_SET_ITEM( const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 13, const_str_plain_parent_context ); Py_INCREF( const_str_plain_parent_context );
    const_tuple_str_digest_c19157e8112b641864481c8e6f9ac442_tuple = PyTuple_New( 1 );
    const_str_digest_c19157e8112b641864481c8e6f9ac442 = UNSTREAM_STRING_ASCII( &constant_bin[ 1018012 ], 11, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c19157e8112b641864481c8e6f9ac442_tuple, 0, const_str_digest_c19157e8112b641864481c8e6f9ac442 ); Py_INCREF( const_str_digest_c19157e8112b641864481c8e6f9ac442 );
    const_str_digest_ee4849b262684aea622e900a24a8a0bd = UNSTREAM_STRING_ASCII( &constant_bin[ 1018023 ], 32, 0 );
    const_str_digest_c53d2a4a26885939c8baa5017b23583f = UNSTREAM_STRING_ASCII( &constant_bin[ 1018055 ], 153, 0 );
    const_tuple_str_digest_c31205f7b9f388d028503e160aac47b4_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c31205f7b9f388d028503e160aac47b4_tuple, 0, const_str_digest_c31205f7b9f388d028503e160aac47b4 ); Py_INCREF( const_str_digest_c31205f7b9f388d028503e160aac47b4 );
    const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 1, const_str_plain_objects ); Py_INCREF( const_str_plain_objects );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 2, const_str_plain_types ); Py_INCREF( const_str_plain_types );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 3, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    const_str_plain_bool_results = UNSTREAM_STRING_ASCII( &constant_bin[ 1018208 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 4, const_str_plain_bool_results ); Py_INCREF( const_str_plain_bool_results );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 5, const_str_plain_o ); Py_INCREF( const_str_plain_o );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 6, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    const_str_plain_mro_func = UNSTREAM_STRING_ASCII( &constant_bin[ 1018220 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 7, const_str_plain_mro_func ); Py_INCREF( const_str_plain_mro_func );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 8, const_str_plain_mro ); Py_INCREF( const_str_plain_mro );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 9, const_str_plain_cls_or_tup ); Py_INCREF( const_str_plain_cls_or_tup );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 10, const_str_plain_classes ); Py_INCREF( const_str_plain_classes );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 11, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 12, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 13, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    PyTuple_SET_ITEM( const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 14, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    const_tuple_str_plain_lazy_context_str_plain_v_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_lazy_context_str_plain_v_tuple, 0, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    PyTuple_SET_ITEM( const_tuple_str_plain_lazy_context_str_plain_v_tuple, 1, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    const_tuple_type_LookupError_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_type_LookupError_tuple, 0, (PyObject *)PyExc_LookupError ); Py_INCREF( (PyObject *)PyExc_LookupError );
    const_str_digest_8034a5ea6d3f98c35e9cf5a85a9b128c = UNSTREAM_STRING_ASCII( &constant_bin[ 1018228 ], 38, 0 );
    const_dict_de706df0108fd8fcd92a5da8a4cc94aa = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_de706df0108fd8fcd92a5da8a4cc94aa, const_str_plain_want_arguments, Py_True );
    assert( PyDict_Size( const_dict_de706df0108fd8fcd92a5da8a4cc94aa ) == 1 );
    const_str_plain__follow_param = UNSTREAM_STRING_ASCII( &constant_bin[ 1018266 ], 13, 1 );
    const_dict_0ef631920779735f6d8ddb2e395f99d0 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_0ef631920779735f6d8ddb2e395f99d0, const_str_plain_want_context, Py_True );
    assert( PyDict_Size( const_dict_0ef631920779735f6d8ddb2e395f99d0 ) == 1 );
    const_tuple_str_digest_4e3cbccd16e9b40484f60435e970bec1_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4e3cbccd16e9b40484f60435e970bec1_tuple, 0, const_str_digest_4e3cbccd16e9b40484f60435e970bec1 ); Py_INCREF( const_str_digest_4e3cbccd16e9b40484f60435e970bec1 );
    const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple, 1, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    PyTuple_SET_ITEM( const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple, 2, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple, 3, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_digest_042c753535689c6a18265d88587540e6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1018279 ], 434, 0 );
    const_tuple_a9471d258cf3258db6320fde70507329_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_a9471d258cf3258db6320fde70507329_tuple, 0, const_str_plain_string ); Py_INCREF( const_str_plain_string );
    PyTuple_SET_ITEM( const_tuple_a9471d258cf3258db6320fde70507329_tuple, 1, const_str_plain_want_obj ); Py_INCREF( const_str_plain_want_obj );
    PyTuple_SET_ITEM( const_tuple_a9471d258cf3258db6320fde70507329_tuple, 2, const_str_plain_want_context ); Py_INCREF( const_str_plain_want_context );
    PyTuple_SET_ITEM( const_tuple_a9471d258cf3258db6320fde70507329_tuple, 3, const_str_plain_want_arguments ); Py_INCREF( const_str_plain_want_arguments );
    PyTuple_SET_ITEM( const_tuple_a9471d258cf3258db6320fde70507329_tuple, 4, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    const_str_plain_builtins_isinstance = UNSTREAM_STRING_ASCII( &constant_bin[ 1018228 ], 19, 1 );
    const_str_plain__return_first_param = UNSTREAM_STRING_ASCII( &constant_bin[ 1018713 ], 19, 1 );
    const_tuple_a122947201318f9eb69dff98cf405b45_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_a122947201318f9eb69dff98cf405b45_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_a122947201318f9eb69dff98cf405b45_tuple, 1, const_str_plain_types ); Py_INCREF( const_str_plain_types );
    PyTuple_SET_ITEM( const_tuple_a122947201318f9eb69dff98cf405b45_tuple, 2, const_str_plain_objects ); Py_INCREF( const_str_plain_objects );
    PyTuple_SET_ITEM( const_tuple_a122947201318f9eb69dff98cf405b45_tuple, 3, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_a122947201318f9eb69dff98cf405b45_tuple, 4, const_str_plain_su ); Py_INCREF( const_str_plain_su );
    const_str_digest_aa9336f5c47581fe04ea84da97f82efe = UNSTREAM_STRING_ASCII( &constant_bin[ 1018732 ], 45, 0 );
    const_str_digest_f4f9c2a9113d5b51fa01e3be21e1a4ea = UNSTREAM_STRING_ASCII( &constant_bin[ 1017458 ], 26, 0 );
    const_str_plain_builtins_getattr = UNSTREAM_STRING_ASCII( &constant_bin[ 1018777 ], 16, 1 );
    const_str_digest_ec4e4044dc14fa3687c4a45457ca7489 = UNSTREAM_STRING_ASCII( &constant_bin[ 1018793 ], 41, 0 );
    const_tuple_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a_tuple, 0, const_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a ); Py_INCREF( const_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a );
    const_str_plain__NAMEDTUPLE_INIT = UNSTREAM_STRING_ASCII( &constant_bin[ 1018834 ], 16, 1 );
    const_str_digest_723974f56302e163bbe2d0cac1cc3598 = UNSTREAM_STRING_ASCII( &constant_bin[ 1018850 ], 39, 0 );
    const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple, 0, const_str_plain_AbstractInstanceContext ); Py_INCREF( const_str_plain_AbstractInstanceContext );
    PyTuple_SET_ITEM( const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple, 1, const_str_plain_CompiledInstance ); Py_INCREF( const_str_plain_CompiledInstance );
    PyTuple_SET_ITEM( const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple, 2, const_str_plain_BoundMethod ); Py_INCREF( const_str_plain_BoundMethod );
    PyTuple_SET_ITEM( const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple, 3, const_str_plain_InstanceArguments ); Py_INCREF( const_str_plain_InstanceArguments );
    const_dict_f0ba030d13b5b64a31319be3eaa33cee = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_f0ba030d13b5b64a31319be3eaa33cee, const_str_plain_include_self_names, Py_True );
    assert( PyDict_Size( const_dict_f0ba030d13b5b64a31319be3eaa33cee ) == 1 );
    const_str_digest_9c5b414e40bcc6854c5f0b647dc1b864 = UNSTREAM_STRING_ASCII( &constant_bin[ 1017638 ], 20, 0 );
    const_dict_d1013312da29be8dcbc7a3855329c0aa = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_d1013312da29be8dcbc7a3855329c0aa, const_str_plain_keep_arguments_param, Py_True );
    assert( PyDict_Size( const_dict_d1013312da29be8dcbc7a3855329c0aa ) == 1 );
    const_set_3ba3dcb5a42392616e5741230a6a5b3b = PySet_New( NULL );
    PySet_Add( const_set_3ba3dcb5a42392616e5741230a6a5b3b, Py_False );
    PySet_Add( const_set_3ba3dcb5a42392616e5741230a6a5b3b, Py_True );
    assert( PySet_Size( const_set_3ba3dcb5a42392616e5741230a6a5b3b ) == 2 );
    const_tuple_str_plain_name_str_plain_x_str_plain_collections_context_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_x_str_plain_collections_context_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_x_str_plain_collections_context_tuple, 1, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_x_str_plain_collections_context_tuple, 2, const_str_plain_collections_context ); Py_INCREF( const_str_plain_collections_context );
    const_str_plain_field_names = UNSTREAM_STRING_ASCII( &constant_bin[ 1018889 ], 11, 1 );
    const_dict_31468f992b2dfacc6e9a4f2c8c95b093 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_31468f992b2dfacc6e9a4f2c8c95b093, const_str_plain_want_obj, Py_True );
    PyDict_SetItem( const_dict_31468f992b2dfacc6e9a4f2c8c95b093, const_str_plain_want_arguments, Py_True );
    assert( PyDict_Size( const_dict_31468f992b2dfacc6e9a4f2c8c95b093 ) == 2 );
    const_str_plain_builtins_next = UNSTREAM_STRING_ASCII( &constant_bin[ 1018023 ], 13, 1 );
    const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple, 0, const_str_plain_evaluator ); Py_INCREF( const_str_plain_evaluator );
    PyTuple_SET_ITEM( const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple, 1, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple, 2, const_str_plain_index ); Py_INCREF( const_str_plain_index );
    PyTuple_SET_ITEM( const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple, 3, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple, 4, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    const_str_plain_builtins_reversed = UNSTREAM_STRING_ASCII( &constant_bin[ 1018900 ], 17, 1 );
    const_tuple_str_plain__repr_template_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__repr_template_tuple, 0, const_str_plain__repr_template ); Py_INCREF( const_str_plain__repr_template );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jedi$evaluate$stdlib( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_3a7f24964d09701b11f51b7e3c58ac45;
static PyCodeObject *codeobj_e4b04af887187ee4db77339b3d688e42;
static PyCodeObject *codeobj_d85c6c078181634647036533eb58afe7;
static PyCodeObject *codeobj_ed90bf1b92b2123d2a7dac4520e6cd1f;
static PyCodeObject *codeobj_7647e9bb1aa506fe6713ef5d2fb379b3;
static PyCodeObject *codeobj_31c72a8b54b17b3482e4174e58324ed0;
static PyCodeObject *codeobj_95a96bf5ce3d7c92f0e63f7753303d2a;
static PyCodeObject *codeobj_50a2e40e3a8979dde27972d8018bbcbe;
static PyCodeObject *codeobj_59a08b6925cbed0800609fcab58b69b6;
static PyCodeObject *codeobj_4e96a1be1e7dd300ccb16a03a7fd2fad;
static PyCodeObject *codeobj_24f155bac913fc03dc2184a111ba1b11;
static PyCodeObject *codeobj_4bc33372de037fb8e6151bb35b89b75c;
static PyCodeObject *codeobj_59f7f26d1c9aa31b00f6ea05ecfe0310;
static PyCodeObject *codeobj_98835c9bd577208e246a74f7d6f0dbcd;
static PyCodeObject *codeobj_0b2afb1033a8457e09941ca11951de5a;
static PyCodeObject *codeobj_7ff73fcb8cd300de4e8347620b2c0d61;
static PyCodeObject *codeobj_983ebaf86279bbca491414011c2389dc;
static PyCodeObject *codeobj_f4123a9e37aacd5ea9bbe93291fa27b0;
static PyCodeObject *codeobj_7f8ec4eec6d1fff5c9fd1c15a0bd31f5;
static PyCodeObject *codeobj_32beddd2d5247d984511acc86158d32e;
static PyCodeObject *codeobj_814fb51d36825a2e958c0290e8e70d23;
static PyCodeObject *codeobj_99c9beedea81ff81c3aad55f957ebe37;
static PyCodeObject *codeobj_bc25a2a819f7291ec7c39683820b6f0c;
static PyCodeObject *codeobj_d109c42f3d5aa99af2eb7e9298d90e7b;
static PyCodeObject *codeobj_7657bae6b5fa40223a88efbcdede2d9f;
static PyCodeObject *codeobj_44d0aa66af007c065af3399f112902ff;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_dfb80871e427217dfe761603c12c346d );
    codeobj_3a7f24964d09701b11f51b7e3c58ac45 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 234, const_tuple_53878258122763aeac3edebbdb8d16b2_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e4b04af887187ee4db77339b3d688e42 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 223, const_tuple_2486b681f5e751c4123ea32d016a9298_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d85c6c078181634647036533eb58afe7 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 126, const_tuple_638b8b6a5a423908e9f1ec4955cbff9f_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ed90bf1b92b2123d2a7dac4520e6cd1f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 283, const_tuple_af64316a582718a7c5c53bd96a2e2aa9_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7647e9bb1aa506fe6713ef5d2fb379b3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 220, const_tuple_9bdc0c05ddc9014d7f1e078f74d8ea62_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_31c72a8b54b17b3482e4174e58324ed0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 282, const_tuple_9399f4707b0ac471391b55085d6d0858_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_95a96bf5ce3d7c92f0e63f7753303d2a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 315, const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_50a2e40e3a8979dde27972d8018bbcbe = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 316, const_tuple_str_plain_evaluator_str_plain_obj_str_plain_arguments_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_59a08b6925cbed0800609fcab58b69b6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 263, const_tuple_str_plain_lazy_context_str_plain_v_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4e96a1be1e7dd300ccb16a03a7fd2fad = MAKE_CODEOBJ( module_filename_obj, const_str_digest_1b1724c41b1bbb89d30a46b326b95dad, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_24f155bac913fc03dc2184a111ba1b11 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SuperInstance, 157, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_4bc33372de037fb8e6151bb35b89b75c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 159, const_tuple_02831cdb2915c664588c38506bed65cc_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_59f7f26d1c9aa31b00f6ea05ecfe0310 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__follow_param, 76, const_tuple_83e6e0a7cc296016d9b44fb3c510e6dd_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_98835c9bd577208e246a74f7d6f0dbcd = MAKE_CODEOBJ( module_filename_obj, const_str_plain__return_first_param, 297, const_tuple_str_plain_evaluator_str_plain_firsts_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0b2afb1033a8457e09941ca11951de5a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_argument_clinic, 85, const_tuple_a9471d258cf3258db6320fde70507329_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7ff73fcb8cd300de4e8347620b2c0d61 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_getattr, 135, const_tuple_ff0f4e6c038f23073c0ba29f95394ca3_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_983ebaf86279bbca491414011c2389dc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_isinstance, 197, const_tuple_ea8c24d3f9421f720e640ca5ff71df28_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f4123a9e37aacd5ea9bbe93291fa27b0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_next, 111, const_tuple_4c97001935895517801cfa58e915a217_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_reversed, 175, const_tuple_f181e0c999daf1a6c0b9bf55fb7d33e6_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_32beddd2d5247d984511acc86158d32e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_super, 164, const_tuple_a122947201318f9eb69dff98cf405b45_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_814fb51d36825a2e958c0290e8e70d23 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_type, 148, const_tuple_ab261c2d1a9fec24cdd4f13d50388e38_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_99c9beedea81ff81c3aad55f957ebe37 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_collections_namedtuple, 239, const_tuple_e1b1b2c86f5a6edf14436821abbc960e_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bc25a2a819f7291ec7c39683820b6f0c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_execute, 50, const_tuple_b40d30098df559b8e31032c3471a605b_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d109c42f3d5aa99af2eb7e9298d90e7b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_f, 90, const_tuple_str_plain_func_str_plain_wrapper_str_plain_string_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_7657bae6b5fa40223a88efbcdede2d9f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_var, 270, const_tuple_str_plain_name_str_plain_x_str_plain_collections_context_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_44d0aa66af007c065af3399f112902ff = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 91, const_tuple_1ea377787bdea67875561c820b9b00dc_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
}

// The module function declarations.
static PyObject *jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_maker( void );


static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_maker( void );


static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_maker( void );


static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_maker( void );


static PyObject *jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_maker( void );


static PyObject *jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_10_builtins_isinstance(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_11_collections_namedtuple(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_12__return_first_param(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_13_lambda(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_14_lambda(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_1_execute(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_2__follow_param(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_4_builtins_next(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_5_builtins_getattr( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_6_builtins_type(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_7___init__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_8_builtins_super(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_9_builtins_reversed(  );


// The module function definitions.
static PyObject *impl_jedi$evaluate$stdlib$$$function_1_execute( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_obj = python_pars[ 1 ];
    PyObject *par_arguments = python_pars[ 2 ];
    PyObject *var_obj_name = NULL;
    PyObject *var_module_name = NULL;
    PyObject *var_func = NULL;
    nuitka_bool tmp_try_except_1__unhandled_indicator = NUITKA_BOOL_UNASSIGNED;
    nuitka_bool tmp_try_except_2__unhandled_indicator = NUITKA_BOOL_UNASSIGNED;
    struct Nuitka_FrameObject *frame_bc25a2a819f7291ec7c39683820b6f0c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_bc25a2a819f7291ec7c39683820b6f0c = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bc25a2a819f7291ec7c39683820b6f0c, codeobj_bc25a2a819f7291ec7c39683820b6f0c, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bc25a2a819f7291ec7c39683820b6f0c = cache_frame_bc25a2a819f7291ec7c39683820b6f0c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bc25a2a819f7291ec7c39683820b6f0c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bc25a2a819f7291ec7c39683820b6f0c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_obj );
        tmp_isinstance_inst_1 = par_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_BoundMethod );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BoundMethod );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BoundMethod" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NotInStdLib );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NotInStdLib );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NotInStdLib" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 52;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_2;
            frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame.f_lineno = 52;
            tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 52;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_assign_source_1;
        tmp_assign_source_1 = NUITKA_BOOL_TRUE;
        tmp_try_except_2__unhandled_indicator = tmp_assign_source_1;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_obj );
        tmp_source_name_2 = par_obj;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_string_name );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        assert( var_obj_name == NULL );
        var_obj_name = tmp_assign_source_2;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_assign_source_3;
        tmp_assign_source_3 = NUITKA_BOOL_FALSE;
        tmp_try_except_2__unhandled_indicator = tmp_assign_source_3;
    }
    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_bc25a2a819f7291ec7c39683820b6f0c, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_bc25a2a819f7291ec7c39683820b6f0c, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "oooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 54;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame) frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooo";
        goto try_except_handler_4;
        branch_no_2:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_1_execute );
    return NULL;
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_try_except_2__unhandled_indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_try_except_2__unhandled_indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_obj );
            tmp_source_name_3 = par_obj;
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_parent_context );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( par_evaluator );
            tmp_source_name_4 = par_evaluator;
            tmp_compexpr_right_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_builtins_module );
            if ( tmp_compexpr_right_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_3 );

                exception_lineno = 59;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            Py_DECREF( tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_4;
                tmp_assign_source_4 = const_str_plain_builtins;
                assert( var_module_name == NULL );
                Py_INCREF( tmp_assign_source_4 );
                var_module_name = tmp_assign_source_4;
            }
            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_isinstance_inst_2;
                PyObject *tmp_isinstance_cls_2;
                PyObject *tmp_source_name_5;
                PyObject *tmp_mvar_value_3;
                CHECK_OBJECT( par_obj );
                tmp_source_name_5 = par_obj;
                tmp_isinstance_inst_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_parent_context );
                if ( tmp_isinstance_inst_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 61;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ModuleContext );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ModuleContext );
                }

                if ( tmp_mvar_value_3 == NULL )
                {
                    Py_DECREF( tmp_isinstance_inst_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ModuleContext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 61;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }

                tmp_isinstance_cls_2 = tmp_mvar_value_3;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
                Py_DECREF( tmp_isinstance_inst_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 61;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_5;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_source_name_8;
                    CHECK_OBJECT( par_obj );
                    tmp_source_name_8 = par_obj;
                    tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_parent_context );
                    if ( tmp_source_name_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 62;
                        type_description_1 = "oooooo";
                        goto try_except_handler_2;
                    }
                    tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_name );
                    Py_DECREF( tmp_source_name_7 );
                    if ( tmp_source_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 62;
                        type_description_1 = "oooooo";
                        goto try_except_handler_2;
                    }
                    tmp_assign_source_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_string_name );
                    Py_DECREF( tmp_source_name_6 );
                    if ( tmp_assign_source_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 62;
                        type_description_1 = "oooooo";
                        goto try_except_handler_2;
                    }
                    assert( var_module_name == NULL );
                    var_module_name = tmp_assign_source_5;
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    PyObject *tmp_assign_source_6;
                    tmp_assign_source_6 = const_str_empty;
                    assert( var_module_name == NULL );
                    Py_INCREF( tmp_assign_source_6 );
                    var_module_name = tmp_assign_source_6;
                }
                branch_end_5:;
            }
            branch_end_4:;
        }
        {
            nuitka_bool tmp_assign_source_7;
            tmp_assign_source_7 = NUITKA_BOOL_TRUE;
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_7;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_subscript_name_2;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__implemented );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__implemented );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_implemented" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 68;
                type_description_1 = "oooooo";
                goto try_except_handler_6;
            }

            tmp_subscribed_name_2 = tmp_mvar_value_4;
            if ( var_module_name == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "module_name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 68;
                type_description_1 = "oooooo";
                goto try_except_handler_6;
            }

            tmp_subscript_name_1 = var_module_name;
            tmp_subscribed_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_1 );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "oooooo";
                goto try_except_handler_6;
            }
            if ( var_obj_name == NULL )
            {
                Py_DECREF( tmp_subscribed_name_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "obj_name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 68;
                type_description_1 = "oooooo";
                goto try_except_handler_6;
            }

            tmp_subscript_name_2 = var_obj_name;
            tmp_assign_source_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "oooooo";
                goto try_except_handler_6;
            }
            assert( var_func == NULL );
            var_func = tmp_assign_source_8;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        {
            nuitka_bool tmp_assign_source_9;
            tmp_assign_source_9 = NUITKA_BOOL_FALSE;
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_9;
        }
        // Preserve existing published exception.
        exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_2 );
        exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_2 );
        exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_2 );

        if ( exception_keeper_tb_3 == NULL )
        {
            exception_keeper_tb_3 = MAKE_TRACEBACK( frame_bc25a2a819f7291ec7c39683820b6f0c, exception_keeper_lineno_3 );
        }
        else if ( exception_keeper_lineno_3 != 0 )
        {
            exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_bc25a2a819f7291ec7c39683820b6f0c, exception_keeper_lineno_3 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
        PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_4 = PyExc_KeyError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "oooooo";
                goto try_except_handler_7;
            }
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "oooooo";
                goto try_except_handler_7;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 67;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame) frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooo";
            goto try_except_handler_7;
            branch_no_6:;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        try_end_4:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        goto try_end_3;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_1_execute );
        return NULL;
        // End of try:
        try_end_3:;
        {
            nuitka_bool tmp_condition_result_7;
            nuitka_bool tmp_compexpr_left_5;
            nuitka_bool tmp_compexpr_right_5;
            assert( tmp_try_except_1__unhandled_indicator != NUITKA_BOOL_UNASSIGNED);
            tmp_compexpr_left_5 = tmp_try_except_1__unhandled_indicator;
            tmp_compexpr_right_5 = NUITKA_BOOL_TRUE;
            tmp_condition_result_7 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                if ( var_func == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "func" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 72;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }

                tmp_called_name_2 = var_func;
                CHECK_OBJECT( par_evaluator );
                tmp_tuple_element_1 = par_evaluator;
                tmp_args_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                CHECK_OBJECT( par_obj );
                tmp_tuple_element_1 = par_obj;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
                tmp_dict_key_1 = const_str_plain_arguments;
                CHECK_OBJECT( par_arguments );
                tmp_dict_value_1 = par_arguments;
                tmp_kw_name_1 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame.f_lineno = 72;
                tmp_return_value = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 72;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                goto try_return_handler_5;
            }
            branch_no_7:;
        }
        goto try_end_5;
        // Return handler code:
        try_return_handler_5:;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_2;
        // End of try:
        try_end_5:;
        branch_no_3:;
    }
    goto try_end_6;
    // Return handler code:
    try_return_handler_2:;
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    {
        PyObject *tmp_raise_type_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NotInStdLib );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NotInStdLib );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NotInStdLib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 73;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_5;
        frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame.f_lineno = 73;
        tmp_raise_type_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_raise_type_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        exception_type = tmp_raise_type_2;
        exception_lineno = 73;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc25a2a819f7291ec7c39683820b6f0c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc25a2a819f7291ec7c39683820b6f0c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc25a2a819f7291ec7c39683820b6f0c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bc25a2a819f7291ec7c39683820b6f0c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bc25a2a819f7291ec7c39683820b6f0c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bc25a2a819f7291ec7c39683820b6f0c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bc25a2a819f7291ec7c39683820b6f0c,
        type_description_1,
        par_evaluator,
        par_obj,
        par_arguments,
        var_obj_name,
        var_module_name,
        var_func
    );


    // Release cached frame.
    if ( frame_bc25a2a819f7291ec7c39683820b6f0c == cache_frame_bc25a2a819f7291ec7c39683820b6f0c )
    {
        Py_DECREF( frame_bc25a2a819f7291ec7c39683820b6f0c );
    }
    cache_frame_bc25a2a819f7291ec7c39683820b6f0c = NULL;

    assertFrameObject( frame_bc25a2a819f7291ec7c39683820b6f0c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_1_execute );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    Py_XDECREF( var_obj_name );
    var_obj_name = NULL;

    Py_XDECREF( var_module_name );
    var_module_name = NULL;

    Py_XDECREF( var_func );
    var_func = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    Py_XDECREF( var_obj_name );
    var_obj_name = NULL;

    Py_XDECREF( var_module_name );
    var_module_name = NULL;

    Py_XDECREF( var_func );
    var_func = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_1_execute );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_2__follow_param( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_arguments = python_pars[ 1 ];
    PyObject *par_index = python_pars[ 2 ];
    PyObject *var_key = NULL;
    PyObject *var_lazy_context = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_59f7f26d1c9aa31b00f6ea05ecfe0310;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_59f7f26d1c9aa31b00f6ea05ecfe0310 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_59f7f26d1c9aa31b00f6ea05ecfe0310, codeobj_59f7f26d1c9aa31b00f6ea05ecfe0310, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_59f7f26d1c9aa31b00f6ea05ecfe0310 = cache_frame_59f7f26d1c9aa31b00f6ea05ecfe0310;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_arguments );
        tmp_called_instance_1 = par_arguments;
        frame_59f7f26d1c9aa31b00f6ea05ecfe0310->m_frame.f_lineno = 78;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_unpack );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_subscribed_name_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        CHECK_OBJECT( par_index );
        tmp_subscript_name_1 = par_index;
        tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 78;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 78;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 78;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 78;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_59f7f26d1c9aa31b00f6ea05ecfe0310, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_59f7f26d1c9aa31b00f6ea05ecfe0310, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_IndexError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 80;
                type_description_1 = "ooooo";
                goto try_except_handler_5;
            }

            tmp_return_value = tmp_mvar_value_1;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_5;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 77;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_59f7f26d1c9aa31b00f6ea05ecfe0310->m_frame) frame_59f7f26d1c9aa31b00f6ea05ecfe0310->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_2__follow_param );
    return NULL;
    // Return handler code:
    try_return_handler_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_key == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_key = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_lazy_context == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_lazy_context = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_lazy_context );
        tmp_called_instance_2 = var_lazy_context;
        frame_59f7f26d1c9aa31b00f6ea05ecfe0310->m_frame.f_lineno = 82;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_infer );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_59f7f26d1c9aa31b00f6ea05ecfe0310, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_59f7f26d1c9aa31b00f6ea05ecfe0310->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_59f7f26d1c9aa31b00f6ea05ecfe0310, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_59f7f26d1c9aa31b00f6ea05ecfe0310,
        type_description_1,
        par_evaluator,
        par_arguments,
        par_index,
        var_key,
        var_lazy_context
    );


    // Release cached frame.
    if ( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 == cache_frame_59f7f26d1c9aa31b00f6ea05ecfe0310 )
    {
        Py_DECREF( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 );
    }
    cache_frame_59f7f26d1c9aa31b00f6ea05ecfe0310 = NULL;

    assertFrameObject( frame_59f7f26d1c9aa31b00f6ea05ecfe0310 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_2__follow_param );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_index );
    Py_DECREF( par_index );
    par_index = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)par_index );
    Py_DECREF( par_index );
    par_index = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_2__follow_param );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_3_argument_clinic( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_string = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_want_obj = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_want_context = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_want_arguments = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *var_f = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_string;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] = par_want_arguments;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[2] = par_want_context;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[3] = par_want_obj;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[3] );


        assert( var_f == NULL );
        var_f = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_f );
    tmp_return_value = var_f;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_3_argument_clinic );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    CHECK_OBJECT( (PyObject *)par_want_obj );
    Py_DECREF( par_want_obj );
    par_want_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_want_context );
    Py_DECREF( par_want_context );
    par_want_context = NULL;

    CHECK_OBJECT( (PyObject *)par_want_arguments );
    Py_DECREF( par_want_arguments );
    par_want_arguments = NULL;

    CHECK_OBJECT( (PyObject *)var_f );
    Py_DECREF( var_f );
    var_f = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_3_argument_clinic );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_func = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_wrapper = NULL;
    struct Nuitka_FrameObject *frame_d109c42f3d5aa99af2eb7e9298d90e7b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d109c42f3d5aa99af2eb7e9298d90e7b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d109c42f3d5aa99af2eb7e9298d90e7b, codeobj_d109c42f3d5aa99af2eb7e9298d90e7b, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d109c42f3d5aa99af2eb7e9298d90e7b = cache_frame_d109c42f3d5aa99af2eb7e9298d90e7b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d109c42f3d5aa99af2eb7e9298d90e7b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d109c42f3d5aa99af2eb7e9298d90e7b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_repack_with_argument_clinic );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_repack_with_argument_clinic );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "repack_with_argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "string" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = PyCell_GET( self->m_closure[0] );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_d1013312da29be8dcbc7a3855329c0aa );
        frame_d109c42f3d5aa99af2eb7e9298d90e7b->m_frame.f_lineno = 91;
        tmp_called_name_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] = par_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[1] = self->m_closure[1];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[2] = self->m_closure[2];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[3] = self->m_closure[3];
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_1)->m_closure[3] );


        frame_d109c42f3d5aa99af2eb7e9298d90e7b->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "coc";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d109c42f3d5aa99af2eb7e9298d90e7b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d109c42f3d5aa99af2eb7e9298d90e7b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d109c42f3d5aa99af2eb7e9298d90e7b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d109c42f3d5aa99af2eb7e9298d90e7b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d109c42f3d5aa99af2eb7e9298d90e7b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d109c42f3d5aa99af2eb7e9298d90e7b,
        type_description_1,
        par_func,
        var_wrapper,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_d109c42f3d5aa99af2eb7e9298d90e7b == cache_frame_d109c42f3d5aa99af2eb7e9298d90e7b )
    {
        Py_DECREF( frame_d109c42f3d5aa99af2eb7e9298d90e7b );
    }
    cache_frame_d109c42f3d5aa99af2eb7e9298d90e7b = NULL;

    assertFrameObject( frame_d109c42f3d5aa99af2eb7e9298d90e7b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapper );
    Py_DECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_obj = python_pars[ 1 ];
    PyObject *par_args = python_pars[ 2 ];
    PyObject *par_kwargs = python_pars[ 3 ];
    PyObject *var_arguments = NULL;
    PyObject *var_result = NULL;
    struct Nuitka_FrameObject *frame_44d0aa66af007c065af3399f112902ff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_44d0aa66af007c065af3399f112902ff = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_44d0aa66af007c065af3399f112902ff, codeobj_44d0aa66af007c065af3399f112902ff, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_44d0aa66af007c065af3399f112902ff = cache_frame_44d0aa66af007c065af3399f112902ff;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_44d0aa66af007c065af3399f112902ff );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_44d0aa66af007c065af3399f112902ff ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_kwargs );
        tmp_called_instance_1 = par_kwargs;
        frame_44d0aa66af007c065af3399f112902ff->m_frame.f_lineno = 93;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_arguments_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        assert( var_arguments == NULL );
        var_arguments = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( par_kwargs );
        tmp_operand_name_2 = par_kwargs;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res == 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 94;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 95;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_dbg );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_f374c4f59bafcb2c22d42a0e88e08b1a;
        CHECK_OBJECT( par_obj );
        tmp_right_name_1 = par_obj;
        tmp_tuple_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 95;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_c96b872716a13ad444d819f9c4f633b7 );
        frame_44d0aa66af007c065af3399f112902ff->m_frame.f_lineno = 95;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_2 = tmp_mvar_value_2;
        assert( var_result == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_result = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "want_context" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[2] ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( var_arguments );
            tmp_source_name_2 = var_arguments;
            tmp_ass_subvalue_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_context );
            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "oooooocccc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_1 = par_kwargs;
            tmp_ass_subscript_1 = const_str_plain_context;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "oooooocccc";
                goto frame_exception_exit_1;
            }
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_2;
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "want_obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_2 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[3] ) );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_ass_subscript_2;
            CHECK_OBJECT( par_obj );
            tmp_ass_subvalue_2 = par_obj;
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_2 = par_kwargs;
            tmp_ass_subscript_2 = const_str_plain_obj;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooooocccc";
                goto frame_exception_exit_1;
            }
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        int tmp_truth_name_3;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "want_arguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_3 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[1] ) );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_ass_subvalue_3;
            PyObject *tmp_ass_subscribed_3;
            PyObject *tmp_ass_subscript_3;
            CHECK_OBJECT( var_arguments );
            tmp_ass_subvalue_3 = var_arguments;
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_3 = par_kwargs;
            tmp_ass_subscript_3 = const_str_plain_arguments;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "oooooocccc";
                goto frame_exception_exit_1;
            }
        }
        branch_no_4:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_dircall_arg4_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 103;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_evaluator );
        tmp_tuple_element_2 = par_evaluator;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg3_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg4_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );
        Py_INCREF( tmp_dircall_arg4_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1, tmp_dircall_arg4_1};
            tmp_assign_source_3 = impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_result;
            assert( old != NULL );
            var_result = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_kw_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_debug );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_dbg );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_3 = const_str_digest_571dc04c354a474fb7f3309327d95d7a;
        tmp_args_name_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
        CHECK_OBJECT( var_result );
        tmp_tuple_element_3 = var_result;
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
        tmp_kw_name_2 = PyDict_Copy( const_dict_c96b872716a13ad444d819f9c4f633b7 );
        frame_44d0aa66af007c065af3399f112902ff->m_frame.f_lineno = 104;
        tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "oooooocccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_44d0aa66af007c065af3399f112902ff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_44d0aa66af007c065af3399f112902ff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_44d0aa66af007c065af3399f112902ff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_44d0aa66af007c065af3399f112902ff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_44d0aa66af007c065af3399f112902ff, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_44d0aa66af007c065af3399f112902ff,
        type_description_1,
        par_evaluator,
        par_obj,
        par_args,
        par_kwargs,
        var_arguments,
        var_result,
        self->m_closure[2],
        self->m_closure[3],
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_44d0aa66af007c065af3399f112902ff == cache_frame_44d0aa66af007c065af3399f112902ff )
    {
        Py_DECREF( frame_44d0aa66af007c065af3399f112902ff );
    }
    cache_frame_44d0aa66af007c065af3399f112902ff = NULL;

    assertFrameObject( frame_44d0aa66af007c065af3399f112902ff );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_result );
    tmp_return_value = var_result;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_arguments );
    Py_DECREF( var_arguments );
    var_arguments = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_arguments );
    var_arguments = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_4_builtins_next( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_iterators = python_pars[ 1 ];
    PyObject *par_defaults = python_pars[ 2 ];
    struct Nuitka_CellObject *var_name = PyCell_EMPTY();
    PyObject *var_context_set = NULL;
    PyObject *var_iterator = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_f4123a9e37aacd5ea9bbe93291fa27b0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f4123a9e37aacd5ea9bbe93291fa27b0 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f4123a9e37aacd5ea9bbe93291fa27b0, codeobj_f4123a9e37aacd5ea9bbe93291fa27b0, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f4123a9e37aacd5ea9bbe93291fa27b0 = cache_frame_f4123a9e37aacd5ea9bbe93291fa27b0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f4123a9e37aacd5ea9bbe93291fa27b0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f4123a9e37aacd5ea9bbe93291fa27b0 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_evaluator );
        tmp_source_name_3 = par_evaluator;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_environment );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version_info );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_major );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_2;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = const_str_plain_next;
            assert( PyCell_GET( var_name ) == NULL );
            Py_INCREF( tmp_assign_source_1 );
            PyCell_SET( var_name, tmp_assign_source_1 );

        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = const_str_plain___next__;
            assert( PyCell_GET( var_name ) == NULL );
            Py_INCREF( tmp_assign_source_2 );
            PyCell_SET( var_name, tmp_assign_source_2 );

        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_3 = tmp_mvar_value_1;
        assert( var_context_set == NULL );
        Py_INCREF( tmp_assign_source_3 );
        var_context_set = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_iterators );
        tmp_iter_arg_1 = par_iterators;
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooocoo";
                exception_lineno = 123;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_6 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_iterator;
            var_iterator = tmp_assign_source_6;
            Py_INCREF( var_iterator );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_iterator );
        tmp_isinstance_inst_1 = var_iterator;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_AbstractInstanceContext );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AbstractInstanceContext );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AbstractInstanceContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "ooocoo";
            goto try_except_handler_2;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_2;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "ooocoo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 125;
                type_description_1 = "ooocoo";
                goto try_except_handler_2;
            }

            tmp_source_name_4 = tmp_mvar_value_3;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_from_sets );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "ooocoo";
                goto try_except_handler_2;
            }
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_iter_arg_2;
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_5;
                PyObject *tmp_kw_name_1;
                CHECK_OBJECT( var_iterator );
                tmp_source_name_5 = var_iterator;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_get_filters );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 127;
                    type_description_1 = "ooocoo";
                    goto try_except_handler_2;
                }
                tmp_kw_name_1 = PyDict_Copy( const_dict_f0ba030d13b5b64a31319be3eaa33cee );
                frame_f4123a9e37aacd5ea9bbe93291fa27b0->m_frame.f_lineno = 127;
                tmp_iter_arg_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_iter_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 127;
                    type_description_1 = "ooocoo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_2 );
                Py_DECREF( tmp_iter_arg_2 );
                if ( tmp_assign_source_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 126;
                    type_description_1 = "ooocoo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = tmp_genexpr_1__$0;
                    tmp_genexpr_1__$0 = tmp_assign_source_8;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            tmp_args_element_name_1 = jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );
            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[1] = var_name;
            Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[1] );


            goto try_return_handler_3;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_4_builtins_next );
            return NULL;
            // Return handler code:
            try_return_handler_3:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            goto outline_result_1;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_4_builtins_next );
            return NULL;
            outline_result_1:;
            frame_f4123a9e37aacd5ea9bbe93291fa27b0->m_frame.f_lineno = 125;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "ooocoo";
                goto try_except_handler_2;
            }
            frame_f4123a9e37aacd5ea9bbe93291fa27b0->m_frame.f_lineno = 125;
            tmp_assign_source_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_execute_evaluated );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "ooocoo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = var_context_set;
                var_context_set = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 123;
        type_description_1 = "ooocoo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        if ( var_context_set == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "context_set" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( var_context_set );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        if ( var_context_set == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "context_set" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "ooocoo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = var_context_set;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_3:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4123a9e37aacd5ea9bbe93291fa27b0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4123a9e37aacd5ea9bbe93291fa27b0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4123a9e37aacd5ea9bbe93291fa27b0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f4123a9e37aacd5ea9bbe93291fa27b0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f4123a9e37aacd5ea9bbe93291fa27b0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f4123a9e37aacd5ea9bbe93291fa27b0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f4123a9e37aacd5ea9bbe93291fa27b0,
        type_description_1,
        par_evaluator,
        par_iterators,
        par_defaults,
        var_name,
        var_context_set,
        var_iterator
    );


    // Release cached frame.
    if ( frame_f4123a9e37aacd5ea9bbe93291fa27b0 == cache_frame_f4123a9e37aacd5ea9bbe93291fa27b0 )
    {
        Py_DECREF( frame_f4123a9e37aacd5ea9bbe93291fa27b0 );
    }
    cache_frame_f4123a9e37aacd5ea9bbe93291fa27b0 = NULL;

    assertFrameObject( frame_f4123a9e37aacd5ea9bbe93291fa27b0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_defaults );
    tmp_return_value = par_defaults;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_4_builtins_next );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_iterators );
    Py_DECREF( par_iterators );
    par_iterators = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    CHECK_OBJECT( (PyObject *)var_name );
    Py_DECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_context_set );
    var_context_set = NULL;

    Py_XDECREF( var_iterator );
    var_iterator = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_iterators );
    Py_DECREF( par_iterators );
    par_iterators = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    CHECK_OBJECT( (PyObject *)var_name );
    Py_DECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_context_set );
    var_context_set = NULL;

    Py_XDECREF( var_iterator );
    var_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_4_builtins_next );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_locals {
    PyObject *var_filter;
    PyObject *var_n;
    PyObject *tmp_contraction_iter_0;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_iter_value_1;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_filter = NULL;
    generator_heap->var_n = NULL;
    generator_heap->tmp_contraction_iter_0 = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_iter_value_1 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_d85c6c078181634647036533eb58afe7, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nooc";
                generator_heap->exception_lineno = 126;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_1;
            generator_heap->tmp_iter_value_1 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_1 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_1;
        {
            PyObject *old = generator_heap->var_filter;
            generator_heap->var_filter = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_filter );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( generator_heap->var_filter );
        tmp_source_name_1 = generator_heap->var_filter;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 128;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 128;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = PyCell_GET( generator->m_closure[1] );
        generator->m_frame->m_frame.f_lineno = 128;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 128;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 126;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = generator_heap->tmp_contraction_iter_0;
            generator_heap->tmp_contraction_iter_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( generator_heap->tmp_contraction_iter_0 );
        tmp_next_source_2 = generator_heap->tmp_contraction_iter_0;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nooc";
                generator_heap->exception_lineno = 126;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_5 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_n;
            generator_heap->var_n = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_n );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_n );
        tmp_called_instance_1 = generator_heap->var_n;
        generator->m_frame->m_frame.f_lineno = 126;
        tmp_expression_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_infer );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 126;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 126;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 126;
        generator_heap->type_description_1 = "Nooc";
        goto try_except_handler_2;
    }
    goto loop_start_2;
    loop_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_contraction_iter_0 );
    Py_DECREF( generator_heap->tmp_contraction_iter_0 );
    generator_heap->tmp_contraction_iter_0 = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 126;
        generator_heap->type_description_1 = "Nooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->tmp_contraction_iter_0 );
    generator_heap->tmp_contraction_iter_0 = NULL;

    Py_XDECREF( generator_heap->tmp_iter_value_1 );
    generator_heap->tmp_iter_value_1 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_filter,
            generator_heap->var_n,
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_filter );
    generator_heap->var_filter = NULL;

    Py_XDECREF( generator_heap->var_n );
    generator_heap->var_n = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->tmp_contraction_iter_0 );
    generator_heap->tmp_contraction_iter_0 = NULL;

    Py_XDECREF( generator_heap->tmp_iter_value_1 );
    generator_heap->tmp_iter_value_1 = NULL;

    Py_XDECREF( generator_heap->var_filter );
    generator_heap->var_filter = NULL;

    Py_XDECREF( generator_heap->var_n );
    generator_heap->var_n = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_context,
        module_jedi$evaluate$stdlib,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_ee4849b262684aea622e900a24a8a0bd,
#endif
        codeobj_d85c6c078181634647036533eb58afe7,
        2,
        sizeof(struct jedi$evaluate$stdlib$$$function_4_builtins_next$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_5_builtins_getattr( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_objects = python_pars[ 1 ];
    PyObject *par_names = python_pars[ 2 ];
    PyObject *par_defaults = python_pars[ 3 ];
    PyObject *var_obj = NULL;
    PyObject *var_name = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    struct Nuitka_FrameObject *frame_7ff73fcb8cd300de4e8347620b2c0d61;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_7ff73fcb8cd300de4e8347620b2c0d61 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7ff73fcb8cd300de4e8347620b2c0d61, codeobj_7ff73fcb8cd300de4e8347620b2c0d61, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7ff73fcb8cd300de4e8347620b2c0d61 = cache_frame_7ff73fcb8cd300de4e8347620b2c0d61;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7ff73fcb8cd300de4e8347620b2c0d61 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7ff73fcb8cd300de4e8347620b2c0d61 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_objects );
        tmp_iter_arg_1 = par_objects;
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 138;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_obj;
            var_obj = tmp_assign_source_3;
            Py_INCREF( var_obj );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( par_names );
        tmp_iter_arg_2 = par_names;
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 139;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_6 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_name;
            var_name = tmp_assign_source_6;
            Py_INCREF( var_name );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_is_string );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_string );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_string" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_name );
        tmp_args_element_name_1 = var_name;
        frame_7ff73fcb8cd300de4e8347620b2c0d61->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 140;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( var_obj );
            tmp_source_name_1 = var_obj;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_py__getattribute__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_force_unicode );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_force_unicode );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "force_unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 141;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_3 = tmp_mvar_value_2;
            CHECK_OBJECT( var_name );
            tmp_called_instance_1 = var_name;
            frame_7ff73fcb8cd300de4e8347620b2c0d61->m_frame.f_lineno = 141;
            tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_safe_value );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 141;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }
            frame_7ff73fcb8cd300de4e8347620b2c0d61->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 141;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }
            frame_7ff73fcb8cd300de4e8347620b2c0d61->m_frame.f_lineno = 141;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }
            goto try_return_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_call_result_2;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_debug );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 143;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }

            tmp_called_instance_2 = tmp_mvar_value_3;
            frame_7ff73fcb8cd300de4e8347620b2c0d61->m_frame.f_lineno = 143;
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_warning, &PyTuple_GET_ITEM( const_tuple_str_digest_c31205f7b9f388d028503e160aac47b4_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto loop_start_2;
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 139;
        type_description_1 = "oooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_1;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__iter_value );
    Py_DECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    goto try_return_handler_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 138;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_4;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7ff73fcb8cd300de4e8347620b2c0d61 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7ff73fcb8cd300de4e8347620b2c0d61 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7ff73fcb8cd300de4e8347620b2c0d61 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7ff73fcb8cd300de4e8347620b2c0d61, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7ff73fcb8cd300de4e8347620b2c0d61->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7ff73fcb8cd300de4e8347620b2c0d61, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7ff73fcb8cd300de4e8347620b2c0d61,
        type_description_1,
        par_evaluator,
        par_objects,
        par_names,
        par_defaults,
        var_obj,
        var_name
    );


    // Release cached frame.
    if ( frame_7ff73fcb8cd300de4e8347620b2c0d61 == cache_frame_7ff73fcb8cd300de4e8347620b2c0d61 )
    {
        Py_DECREF( frame_7ff73fcb8cd300de4e8347620b2c0d61 );
    }
    cache_frame_7ff73fcb8cd300de4e8347620b2c0d61 = NULL;

    assertFrameObject( frame_7ff73fcb8cd300de4e8347620b2c0d61 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_5_builtins_getattr );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_names );
    Py_DECREF( par_names );
    par_names = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_names );
    Py_DECREF( par_names );
    par_names = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    Py_XDECREF( var_obj );
    var_obj = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_5_builtins_getattr );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_6_builtins_type( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_objects = python_pars[ 1 ];
    PyObject *par_bases = python_pars[ 2 ];
    PyObject *par_dicts = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_814fb51d36825a2e958c0290e8e70d23;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_814fb51d36825a2e958c0290e8e70d23 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_814fb51d36825a2e958c0290e8e70d23, codeobj_814fb51d36825a2e958c0290e8e70d23, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_814fb51d36825a2e958c0290e8e70d23 = cache_frame_814fb51d36825a2e958c0290e8e70d23;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_814fb51d36825a2e958c0290e8e70d23 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_814fb51d36825a2e958c0290e8e70d23 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        int tmp_truth_name_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_dicts );
        tmp_truth_name_2 = CHECK_IF_TRUE( par_dicts );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 152;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_return_value = tmp_mvar_value_1;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_objects );
            tmp_called_instance_1 = par_objects;
            frame_814fb51d36825a2e958c0290e8e70d23->m_frame.f_lineno = 154;
            tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_py__class__ );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_814fb51d36825a2e958c0290e8e70d23 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_814fb51d36825a2e958c0290e8e70d23 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_814fb51d36825a2e958c0290e8e70d23 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_814fb51d36825a2e958c0290e8e70d23, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_814fb51d36825a2e958c0290e8e70d23->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_814fb51d36825a2e958c0290e8e70d23, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_814fb51d36825a2e958c0290e8e70d23,
        type_description_1,
        par_evaluator,
        par_objects,
        par_bases,
        par_dicts
    );


    // Release cached frame.
    if ( frame_814fb51d36825a2e958c0290e8e70d23 == cache_frame_814fb51d36825a2e958c0290e8e70d23 )
    {
        Py_DECREF( frame_814fb51d36825a2e958c0290e8e70d23 );
    }
    cache_frame_814fb51d36825a2e958c0290e8e70d23 = NULL;

    assertFrameObject( frame_814fb51d36825a2e958c0290e8e70d23 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_6_builtins_type );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_bases );
    Py_DECREF( par_bases );
    par_bases = NULL;

    CHECK_OBJECT( (PyObject *)par_dicts );
    Py_DECREF( par_dicts );
    par_dicts = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_bases );
    Py_DECREF( par_bases );
    par_bases = NULL;

    CHECK_OBJECT( (PyObject *)par_dicts );
    Py_DECREF( par_dicts );
    par_dicts = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_6_builtins_type );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_7___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_evaluator = python_pars[ 1 ];
    PyObject *par_cls = python_pars[ 2 ];
    PyObject *var_su = NULL;
    struct Nuitka_FrameObject *frame_4bc33372de037fb8e6151bb35b89b75c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4bc33372de037fb8e6151bb35b89b75c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4bc33372de037fb8e6151bb35b89b75c, codeobj_4bc33372de037fb8e6151bb35b89b75c, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4bc33372de037fb8e6151bb35b89b75c = cache_frame_4bc33372de037fb8e6151bb35b89b75c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4bc33372de037fb8e6151bb35b89b75c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4bc33372de037fb8e6151bb35b89b75c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_cls );
        tmp_called_instance_1 = par_cls;
        frame_4bc33372de037fb8e6151bb35b89b75c->m_frame.f_lineno = 160;
        tmp_subscribed_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_py_mro );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_pos_1;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        assert( var_su == NULL );
        var_su = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_evaluator );
        tmp_args_element_name_1 = par_evaluator;
        CHECK_OBJECT( var_su );
        tmp_and_left_value_1 = var_su;
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_su );
        tmp_subscribed_name_2 = var_su;
        tmp_subscript_name_2 = const_int_0;
        tmp_and_right_value_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_and_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        Py_INCREF( tmp_and_left_value_1 );
        tmp_or_left_value_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        CHECK_OBJECT( par_self );
        tmp_or_right_value_1 = par_self;
        Py_INCREF( tmp_or_right_value_1 );
        tmp_args_element_name_2 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_args_element_name_2 = tmp_or_left_value_1;
        or_end_1:;
        frame_4bc33372de037fb8e6151bb35b89b75c->m_frame.f_lineno = 161;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4bc33372de037fb8e6151bb35b89b75c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4bc33372de037fb8e6151bb35b89b75c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4bc33372de037fb8e6151bb35b89b75c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4bc33372de037fb8e6151bb35b89b75c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4bc33372de037fb8e6151bb35b89b75c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4bc33372de037fb8e6151bb35b89b75c,
        type_description_1,
        par_self,
        par_evaluator,
        par_cls,
        var_su,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_4bc33372de037fb8e6151bb35b89b75c == cache_frame_4bc33372de037fb8e6151bb35b89b75c )
    {
        Py_DECREF( frame_4bc33372de037fb8e6151bb35b89b75c );
    }
    cache_frame_4bc33372de037fb8e6151bb35b89b75c = NULL;

    assertFrameObject( frame_4bc33372de037fb8e6151bb35b89b75c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_7___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_su );
    Py_DECREF( var_su );
    var_su = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( var_su );
    var_su = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_7___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_8_builtins_super( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_types = python_pars[ 1 ];
    PyObject *par_objects = python_pars[ 2 ];
    PyObject *par_context = python_pars[ 3 ];
    PyObject *var_su = NULL;
    struct Nuitka_FrameObject *frame_32beddd2d5247d984511acc86158d32e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_32beddd2d5247d984511acc86158d32e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_32beddd2d5247d984511acc86158d32e, codeobj_32beddd2d5247d984511acc86158d32e, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_32beddd2d5247d984511acc86158d32e = cache_frame_32beddd2d5247d984511acc86158d32e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_32beddd2d5247d984511acc86158d32e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_32beddd2d5247d984511acc86158d32e ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_context );
        tmp_isinstance_inst_1 = par_context;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_FunctionExecutionContext );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionExecutionContext );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionExecutionContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( par_context );
            tmp_source_name_1 = par_context;
            tmp_isinstance_inst_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_var_args );
            if ( tmp_isinstance_inst_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_InstanceArguments );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InstanceArguments );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_isinstance_inst_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InstanceArguments" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 168;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_2 = tmp_mvar_value_2;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            Py_DECREF( tmp_isinstance_inst_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_source_name_2;
                PyObject *tmp_source_name_3;
                CHECK_OBJECT( par_context );
                tmp_source_name_3 = par_context;
                tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_var_args );
                if ( tmp_source_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_instance );
                Py_DECREF( tmp_source_name_2 );
                if ( tmp_called_instance_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                frame_32beddd2d5247d984511acc86158d32e->m_frame.f_lineno = 169;
                tmp_called_instance_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_py__class__ );
                Py_DECREF( tmp_called_instance_2 );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                frame_32beddd2d5247d984511acc86158d32e->m_frame.f_lineno = 169;
                tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_py__bases__ );
                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 169;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_su == NULL );
                var_su = tmp_assign_source_1;
            }
            {
                PyObject *tmp_called_instance_3;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( var_su );
                tmp_subscribed_name_1 = var_su;
                tmp_subscript_name_1 = const_int_0;
                tmp_called_instance_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                if ( tmp_called_instance_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 170;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                frame_32beddd2d5247d984511acc86158d32e->m_frame.f_lineno = 170;
                tmp_called_instance_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_infer );
                Py_DECREF( tmp_called_instance_4 );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 170;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                frame_32beddd2d5247d984511acc86158d32e->m_frame.f_lineno = 170;
                tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_execute_evaluated );
                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 170;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_2:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_3;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32beddd2d5247d984511acc86158d32e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_32beddd2d5247d984511acc86158d32e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32beddd2d5247d984511acc86158d32e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_32beddd2d5247d984511acc86158d32e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_32beddd2d5247d984511acc86158d32e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_32beddd2d5247d984511acc86158d32e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_32beddd2d5247d984511acc86158d32e,
        type_description_1,
        par_evaluator,
        par_types,
        par_objects,
        par_context,
        var_su
    );


    // Release cached frame.
    if ( frame_32beddd2d5247d984511acc86158d32e == cache_frame_32beddd2d5247d984511acc86158d32e )
    {
        Py_DECREF( frame_32beddd2d5247d984511acc86158d32e );
    }
    cache_frame_32beddd2d5247d984511acc86158d32e = NULL;

    assertFrameObject( frame_32beddd2d5247d984511acc86158d32e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_8_builtins_super );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_types );
    Py_DECREF( par_types );
    par_types = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    Py_XDECREF( var_su );
    var_su = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_types );
    Py_DECREF( par_types );
    par_types = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    Py_XDECREF( var_su );
    var_su = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_8_builtins_super );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_9_builtins_reversed( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_sequences = python_pars[ 1 ];
    PyObject *par_obj = python_pars[ 2 ];
    PyObject *par_arguments = python_pars[ 3 ];
    PyObject *var_key = NULL;
    PyObject *var_lazy_context = NULL;
    PyObject *var_cn = NULL;
    PyObject *var_ordered = NULL;
    PyObject *var_rev = NULL;
    PyObject *var_seq = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5, codeobj_7f8ec4eec6d1fff5c9fd1c15a0bd31f5, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 = cache_frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_value_name_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_arguments );
        tmp_called_instance_1 = par_arguments;
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 180;
        tmp_value_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_unpack );
        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        tmp_iter_arg_1 = ITERATOR_NEXT( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooo";
            exception_lineno = 180;
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "oooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooo";
            exception_lineno = 180;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooo";
            exception_lineno = 180;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooo";
                    exception_lineno = 180;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooo";
            exception_lineno = 180;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_key == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_key = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_lazy_context == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_lazy_context = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = Py_None;
        assert( var_cn == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_cn = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( var_lazy_context );
        tmp_isinstance_inst_1 = var_lazy_context;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextualizedNode );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextualizedNode );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextualizedNode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 184;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_2;
            CHECK_OBJECT( var_lazy_context );
            tmp_source_name_1 = var_lazy_context;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__context );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_lazy_context );
            tmp_source_name_2 = var_lazy_context;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_data );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 184;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 184;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_cn;
                assert( old != NULL );
                var_cn = tmp_assign_source_7;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_sequences );
        tmp_called_instance_2 = par_sequences;
        CHECK_OBJECT( var_cn );
        tmp_args_element_name_3 = var_cn;
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 185;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_list_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_iterate, call_args );
        }

        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_ordered == NULL );
        var_ordered = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_list_arg_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_4;
        tmp_called_name_2 = (PyObject *)&PyReversed_Type;
        CHECK_OBJECT( var_ordered );
        tmp_args_element_name_4 = var_ordered;
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 187;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_list_arg_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_list_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = PySequence_List( tmp_list_arg_2 );
        Py_DECREF( tmp_list_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_rev == NULL );
        var_rev = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_iterable );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_3;
        CHECK_OBJECT( par_evaluator );
        tmp_args_element_name_5 = par_evaluator;
        tmp_args_element_name_6 = const_str_plain_list;
        CHECK_OBJECT( var_rev );
        tmp_args_element_name_7 = var_rev;
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 192;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_assign_source_10 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_3, const_str_plain_FakeSequence, call_args );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_seq == NULL );
        var_seq = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_9;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ValuesArguments );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ValuesArguments );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ValuesArguments" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_5;
        CHECK_OBJECT( var_seq );
        tmp_args_element_name_9 = var_seq;
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_8 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_args_element_name_8, 0, tmp_list_element_1 );
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_arguments;
            assert( old != NULL );
            par_arguments = tmp_assign_source_11;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_CompiledInstance );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompiledInstance );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompiledInstance" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_7;
        CHECK_OBJECT( par_evaluator );
        tmp_args_element_name_11 = par_evaluator;
        CHECK_OBJECT( par_evaluator );
        tmp_source_name_3 = par_evaluator;
        tmp_args_element_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_builtins_module );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_obj );
        tmp_args_element_name_13 = par_obj;
        CHECK_OBJECT( par_arguments );
        tmp_args_element_name_14 = par_arguments;
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 194;
        {
            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame.f_lineno = 194;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5,
        type_description_1,
        par_evaluator,
        par_sequences,
        par_obj,
        par_arguments,
        var_key,
        var_lazy_context,
        var_cn,
        var_ordered,
        var_rev,
        var_seq
    );


    // Release cached frame.
    if ( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 == cache_frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 )
    {
        Py_DECREF( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 );
    }
    cache_frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 = NULL;

    assertFrameObject( frame_7f8ec4eec6d1fff5c9fd1c15a0bd31f5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_9_builtins_reversed );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_sequences );
    Py_DECREF( par_sequences );
    par_sequences = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)var_key );
    Py_DECREF( var_key );
    var_key = NULL;

    CHECK_OBJECT( (PyObject *)var_lazy_context );
    Py_DECREF( var_lazy_context );
    var_lazy_context = NULL;

    CHECK_OBJECT( (PyObject *)var_cn );
    Py_DECREF( var_cn );
    var_cn = NULL;

    CHECK_OBJECT( (PyObject *)var_ordered );
    Py_DECREF( var_ordered );
    var_ordered = NULL;

    CHECK_OBJECT( (PyObject *)var_rev );
    Py_DECREF( var_rev );
    var_rev = NULL;

    CHECK_OBJECT( (PyObject *)var_seq );
    Py_DECREF( var_seq );
    var_seq = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_sequences );
    Py_DECREF( par_sequences );
    par_sequences = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    Py_XDECREF( var_cn );
    var_cn = NULL;

    Py_XDECREF( var_ordered );
    var_ordered = NULL;

    Py_XDECREF( var_rev );
    var_rev = NULL;

    Py_XDECREF( var_seq );
    var_seq = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_9_builtins_reversed );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_10_builtins_isinstance( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_evaluator = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_objects = python_pars[ 1 ];
    PyObject *par_types = python_pars[ 2 ];
    PyObject *par_arguments = python_pars[ 3 ];
    PyObject *var_bool_results = NULL;
    PyObject *var_o = NULL;
    PyObject *var_cls = NULL;
    PyObject *var_mro_func = NULL;
    struct Nuitka_CellObject *var_mro = PyCell_EMPTY();
    PyObject *var_cls_or_tup = NULL;
    PyObject *var_classes = NULL;
    PyObject *var__ = NULL;
    PyObject *var_lazy_context = NULL;
    PyObject *var_node = NULL;
    PyObject *var_message = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_genexpr_3__$0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_983ebaf86279bbca491414011c2389dc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_983ebaf86279bbca491414011c2389dc = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PySet_New( NULL );
        assert( var_bool_results == NULL );
        var_bool_results = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_983ebaf86279bbca491414011c2389dc, codeobj_983ebaf86279bbca491414011c2389dc, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_983ebaf86279bbca491414011c2389dc = cache_frame_983ebaf86279bbca491414011c2389dc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_983ebaf86279bbca491414011c2389dc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_983ebaf86279bbca491414011c2389dc ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_objects );
        tmp_iter_arg_1 = par_objects;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;
            type_description_1 = "cooooooocoooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "cooooooocoooooo";
                exception_lineno = 200;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_o;
            var_o = tmp_assign_source_4;
            Py_INCREF( var_o );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_o );
        tmp_called_instance_1 = var_o;
        frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 201;
        tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_py__class__ );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_cls;
            var_cls = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_cls );
        tmp_source_name_1 = var_cls;
        tmp_assign_source_6 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_py__mro__ );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = var_mro_func;
            var_mro_func = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_983ebaf86279bbca491414011c2389dc, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_983ebaf86279bbca491414011c2389dc, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = PySet_New( const_set_3ba3dcb5a42392616e5741230a6a5b3b );
            {
                PyObject *old = var_bool_results;
                assert( old != NULL );
                var_bool_results = tmp_assign_source_7;
                Py_DECREF( old );
            }

        }
        goto try_break_handler_4;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 202;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_983ebaf86279bbca491414011c2389dc->m_frame) frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "cooooooocoooooo";
        goto try_except_handler_4;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // try break handler code:
    try_break_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto loop_end_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_1;
        CHECK_OBJECT( var_mro_func );
        tmp_called_name_1 = var_mro_func;
        frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 211;
        tmp_assign_source_8 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = PyCell_GET( var_mro );
            PyCell_SET( var_mro, tmp_assign_source_8 );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( par_types );
        tmp_iter_arg_2 = par_types;
        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_9;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "cooooooocoooooo";
                exception_lineno = 213;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_11 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_cls_or_tup;
            var_cls_or_tup = tmp_assign_source_11;
            Py_INCREF( var_cls_or_tup );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_cls_or_tup );
        tmp_called_instance_2 = var_cls_or_tup;
        frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 214;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_is_class );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 214;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_5;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 214;
            type_description_1 = "cooooooocoooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_bool_results );
            tmp_source_name_2 = var_bool_results;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_add );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_cls_or_tup );
            tmp_compexpr_left_2 = var_cls_or_tup;
            CHECK_OBJECT( PyCell_GET( var_mro ) );
            tmp_compexpr_right_2 = PyCell_GET( var_mro );
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 215;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            tmp_args_element_name_1 = ( tmp_res == 1 ) ? Py_True : Py_False;
            frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 215;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_cls_or_tup );
            tmp_source_name_4 = var_cls_or_tup;
            tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_string_name );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            tmp_compexpr_right_3 = const_str_plain_tuple;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( var_cls_or_tup );
            tmp_called_instance_3 = var_cls_or_tup;
            frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 217;
            tmp_compexpr_left_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_root_context );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 217;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( PyCell_GET( par_evaluator ) );
            tmp_source_name_5 = PyCell_GET( par_evaluator );
            tmp_compexpr_right_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_builtins_module );
            if ( tmp_compexpr_right_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_4 );

                exception_lineno = 217;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            Py_DECREF( tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 217;
                type_description_1 = "cooooooocoooooo";
                goto try_except_handler_5;
            }
            tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_3 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_3 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_12;
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_6;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_2;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 219;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }

                tmp_source_name_6 = tmp_mvar_value_1;
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_from_sets );
                if ( tmp_called_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 219;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_iter_arg_3;
                    PyObject *tmp_called_instance_4;
                    CHECK_OBJECT( var_cls_or_tup );
                    tmp_called_instance_4 = var_cls_or_tup;
                    frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 221;
                    tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_iterate );
                    if ( tmp_iter_arg_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 221;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_3 );
                    Py_DECREF( tmp_iter_arg_3 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 220;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = tmp_genexpr_1__$0;
                        tmp_genexpr_1__$0 = tmp_assign_source_13;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                tmp_args_element_name_2 = jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_maker();

                ((struct Nuitka_GeneratorObject *)tmp_args_element_name_2)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


                goto try_return_handler_6;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
                return NULL;
                // Return handler code:
                try_return_handler_6:;
                CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                Py_DECREF( tmp_genexpr_1__$0 );
                tmp_genexpr_1__$0 = NULL;

                goto outline_result_1;
                // End of try:
                CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                Py_DECREF( tmp_genexpr_1__$0 );
                tmp_genexpr_1__$0 = NULL;

                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
                return NULL;
                outline_result_1:;
                frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 219;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_assign_source_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 219;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }
                {
                    PyObject *old = var_classes;
                    var_classes = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_7;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_any_arg_1;
                CHECK_OBJECT( var_bool_results );
                tmp_source_name_7 = var_bool_results;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_add );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_iter_arg_4;
                    CHECK_OBJECT( var_classes );
                    tmp_iter_arg_4 = var_classes;
                    tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_4 );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 223;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = tmp_genexpr_2__$0;
                        tmp_genexpr_2__$0 = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                tmp_any_arg_1 = jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_maker();

                ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_2__$0 );
                ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[1] = var_mro;
                Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[1] );


                goto try_return_handler_7;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
                return NULL;
                // Return handler code:
                try_return_handler_7:;
                CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
                Py_DECREF( tmp_genexpr_2__$0 );
                tmp_genexpr_2__$0 = NULL;

                goto outline_result_2;
                // End of try:
                CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
                Py_DECREF( tmp_genexpr_2__$0 );
                tmp_genexpr_2__$0 = NULL;

                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
                return NULL;
                outline_result_2:;
                tmp_args_element_name_3 = BUILTIN_ANY( tmp_any_arg_1 );
                Py_DECREF( tmp_any_arg_1 );
                if ( tmp_args_element_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 223;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }
                frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 223;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            goto branch_end_3;
            branch_no_3:;
            // Tried code:
            {
                PyObject *tmp_assign_source_15;
                PyObject *tmp_iter_arg_5;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_list_arg_1;
                PyObject *tmp_called_instance_5;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( par_arguments );
                tmp_called_instance_5 = par_arguments;
                frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 225;
                tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_unpack );
                if ( tmp_list_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 225;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_8;
                }
                tmp_subscribed_name_1 = PySequence_List( tmp_list_arg_1 );
                Py_DECREF( tmp_list_arg_1 );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 225;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_8;
                }
                tmp_subscript_name_1 = const_int_pos_1;
                tmp_iter_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
                Py_DECREF( tmp_subscribed_name_1 );
                if ( tmp_iter_arg_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 225;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_8;
                }
                tmp_assign_source_15 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_5 );
                Py_DECREF( tmp_iter_arg_5 );
                if ( tmp_assign_source_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 225;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_8;
                }
                {
                    PyObject *old = tmp_tuple_unpack_1__source_iter;
                    tmp_tuple_unpack_1__source_iter = tmp_assign_source_15;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_16;
                PyObject *tmp_unpack_1;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
                tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
                if ( tmp_assign_source_16 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "cooooooocoooooo";
                    exception_lineno = 225;
                    goto try_except_handler_9;
                }
                {
                    PyObject *old = tmp_tuple_unpack_1__element_1;
                    tmp_tuple_unpack_1__element_1 = tmp_assign_source_16;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_17;
                PyObject *tmp_unpack_2;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
                tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
                if ( tmp_assign_source_17 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "cooooooocoooooo";
                    exception_lineno = 225;
                    goto try_except_handler_9;
                }
                {
                    PyObject *old = tmp_tuple_unpack_1__element_2;
                    tmp_tuple_unpack_1__element_2 = tmp_assign_source_17;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_1;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "cooooooocoooooo";
                            exception_lineno = 225;
                            goto try_except_handler_9;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "cooooooocoooooo";
                    exception_lineno = 225;
                    goto try_except_handler_9;
                }
            }
            goto try_end_2;
            // Exception handler code:
            try_except_handler_9:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
            Py_DECREF( tmp_tuple_unpack_1__source_iter );
            tmp_tuple_unpack_1__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto try_except_handler_8;
            // End of try:
            try_end_2:;
            goto try_end_3;
            // Exception handler code:
            try_except_handler_8:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
            tmp_tuple_unpack_1__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
            tmp_tuple_unpack_1__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto try_except_handler_5;
            // End of try:
            try_end_3:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
            Py_DECREF( tmp_tuple_unpack_1__source_iter );
            tmp_tuple_unpack_1__source_iter = NULL;

            {
                PyObject *tmp_assign_source_18;
                CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
                tmp_assign_source_18 = tmp_tuple_unpack_1__element_1;
                {
                    PyObject *old = var__;
                    var__ = tmp_assign_source_18;
                    Py_INCREF( var__ );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
            tmp_tuple_unpack_1__element_1 = NULL;

            {
                PyObject *tmp_assign_source_19;
                CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
                tmp_assign_source_19 = tmp_tuple_unpack_1__element_2;
                {
                    PyObject *old = var_lazy_context;
                    var_lazy_context = tmp_assign_source_19;
                    Py_INCREF( var_lazy_context );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
            tmp_tuple_unpack_1__element_2 = NULL;

            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_isinstance_inst_1;
                PyObject *tmp_isinstance_cls_1;
                PyObject *tmp_mvar_value_2;
                CHECK_OBJECT( var_lazy_context );
                tmp_isinstance_inst_1 = var_lazy_context;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 226;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }

                tmp_isinstance_cls_1 = tmp_mvar_value_2;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 226;
                    type_description_1 = "cooooooocoooooo";
                    goto try_except_handler_5;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_20;
                    PyObject *tmp_source_name_8;
                    CHECK_OBJECT( var_lazy_context );
                    tmp_source_name_8 = var_lazy_context;
                    tmp_assign_source_20 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_data );
                    if ( tmp_assign_source_20 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 227;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = var_node;
                        var_node = tmp_assign_source_20;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_21;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    tmp_left_name_1 = const_str_digest_4783aaea9f206425e10f79f8d9442dde;
                    CHECK_OBJECT( var_cls_or_tup );
                    tmp_right_name_1 = var_cls_or_tup;
                    tmp_assign_source_21 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                    if ( tmp_assign_source_21 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 228;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = var_message;
                        var_message = tmp_assign_source_21;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_source_name_9;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_call_result_4;
                    PyObject *tmp_args_element_name_4;
                    PyObject *tmp_source_name_10;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_analysis );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 231;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }

                    tmp_source_name_9 = tmp_mvar_value_3;
                    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_add );
                    if ( tmp_called_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 231;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    CHECK_OBJECT( var_lazy_context );
                    tmp_source_name_10 = var_lazy_context;
                    tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain__context );
                    if ( tmp_args_element_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_5 );

                        exception_lineno = 231;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    tmp_args_element_name_5 = const_str_digest_25b544778da07cbf7f7f7816b8dd5745;
                    CHECK_OBJECT( var_node );
                    tmp_args_element_name_6 = var_node;
                    CHECK_OBJECT( var_message );
                    tmp_args_element_name_7 = var_message;
                    frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 231;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
                        tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_5, call_args );
                    }

                    Py_DECREF( tmp_called_name_5 );
                    Py_DECREF( tmp_args_element_name_4 );
                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 231;
                        type_description_1 = "cooooooocoooooo";
                        goto try_except_handler_5;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                branch_no_4:;
            }
            branch_end_3:;
        }
        branch_end_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 213;
        type_description_1 = "cooooooocoooooo";
        goto try_except_handler_5;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 200;
        type_description_1 = "cooooooocoooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;
            type_description_1 = "cooooooocoooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_4;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_from_iterable );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;
            type_description_1 = "cooooooocoooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_iter_arg_6;
            if ( var_bool_results == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "bool_results" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 235;
                type_description_1 = "cooooooocoooooo";
                goto frame_exception_exit_1;
            }

            tmp_iter_arg_6 = var_bool_results;
            tmp_assign_source_22 = MAKE_ITERATOR( tmp_iter_arg_6 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_1 = "cooooooocoooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_3__$0 == NULL );
            tmp_genexpr_3__$0 = tmp_assign_source_22;
        }
        // Tried code:
        tmp_args_element_name_8 = jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_8)->m_closure[0] = par_evaluator;
        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_8)->m_closure[0] );
        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_8)->m_closure[1] = PyCell_NEW0( tmp_genexpr_3__$0 );


        goto try_return_handler_10;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
        return NULL;
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_3__$0 );
        Py_DECREF( tmp_genexpr_3__$0 );
        tmp_genexpr_3__$0 = NULL;

        goto outline_result_3;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_3__$0 );
        Py_DECREF( tmp_genexpr_3__$0 );
        tmp_genexpr_3__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
        return NULL;
        outline_result_3:;
        frame_983ebaf86279bbca491414011c2389dc->m_frame.f_lineno = 233;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;
            type_description_1 = "cooooooocoooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_983ebaf86279bbca491414011c2389dc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_983ebaf86279bbca491414011c2389dc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_983ebaf86279bbca491414011c2389dc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_983ebaf86279bbca491414011c2389dc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_983ebaf86279bbca491414011c2389dc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_983ebaf86279bbca491414011c2389dc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_983ebaf86279bbca491414011c2389dc,
        type_description_1,
        par_evaluator,
        par_objects,
        par_types,
        par_arguments,
        var_bool_results,
        var_o,
        var_cls,
        var_mro_func,
        var_mro,
        var_cls_or_tup,
        var_classes,
        var__,
        var_lazy_context,
        var_node,
        var_message
    );


    // Release cached frame.
    if ( frame_983ebaf86279bbca491414011c2389dc == cache_frame_983ebaf86279bbca491414011c2389dc )
    {
        Py_DECREF( frame_983ebaf86279bbca491414011c2389dc );
    }
    cache_frame_983ebaf86279bbca491414011c2389dc = NULL;

    assertFrameObject( frame_983ebaf86279bbca491414011c2389dc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_types );
    Py_DECREF( par_types );
    par_types = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    Py_XDECREF( var_bool_results );
    var_bool_results = NULL;

    Py_XDECREF( var_o );
    var_o = NULL;

    Py_XDECREF( var_cls );
    var_cls = NULL;

    Py_XDECREF( var_mro_func );
    var_mro_func = NULL;

    CHECK_OBJECT( (PyObject *)var_mro );
    Py_DECREF( var_mro );
    var_mro = NULL;

    Py_XDECREF( var_cls_or_tup );
    var_cls_or_tup = NULL;

    Py_XDECREF( var_classes );
    var_classes = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_message );
    var_message = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_objects );
    Py_DECREF( par_objects );
    par_objects = NULL;

    CHECK_OBJECT( (PyObject *)par_types );
    Py_DECREF( par_types );
    par_types = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    Py_XDECREF( var_bool_results );
    var_bool_results = NULL;

    Py_XDECREF( var_o );
    var_o = NULL;

    Py_XDECREF( var_cls );
    var_cls = NULL;

    Py_XDECREF( var_mro_func );
    var_mro_func = NULL;

    CHECK_OBJECT( (PyObject *)var_mro );
    Py_DECREF( var_mro );
    var_mro = NULL;

    Py_XDECREF( var_cls_or_tup );
    var_cls_or_tup = NULL;

    Py_XDECREF( var_classes );
    var_classes = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_message );
    var_message = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_10_builtins_isinstance );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_locals {
    PyObject *var_lazy_context;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_lazy_context = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_7647e9bb1aa506fe6713ef5d2fb379b3, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 220;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_lazy_context;
            generator_heap->var_lazy_context = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_lazy_context );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_instance_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_lazy_context );
        tmp_called_instance_1 = generator_heap->var_lazy_context;
        generator->m_frame->m_frame.f_lineno = 220;
        tmp_expression_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_infer );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_instance_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 220;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_lazy_context
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_lazy_context );
    generator_heap->var_lazy_context = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_lazy_context );
    generator_heap->var_lazy_context = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_context,
        module_jedi$evaluate$stdlib,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_8034a5ea6d3f98c35e9cf5a85a9b128c,
#endif
        codeobj_7647e9bb1aa506fe6713ef5d2fb379b3,
        1,
        sizeof(struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_1_genexpr_locals)
    );
}



struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_locals {
    PyObject *var_cls;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_locals *generator_heap = (struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_cls = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_e4b04af887187ee4db77339b3d688e42, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noc";
                generator_heap->exception_lineno = 223;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_cls;
            generator_heap->var_cls = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_cls );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_cls );
        tmp_compexpr_left_1 = generator_heap->var_cls;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "mro" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 223;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_compexpr_right_1 = PyCell_GET( generator->m_closure[1] );
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 223;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ( generator_heap->tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_expression_name_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 223;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 223;
        generator_heap->type_description_1 = "Noc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_cls,
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_cls );
    generator_heap->var_cls = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_cls );
    generator_heap->var_cls = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_context,
        module_jedi$evaluate$stdlib,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_8034a5ea6d3f98c35e9cf5a85a9b128c,
#endif
        codeobj_e4b04af887187ee4db77339b3d688e42,
        2,
        sizeof(struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_2_genexpr_locals)
    );
}



struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_locals {
    PyObject *var_b;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_locals *generator_heap = (struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_b = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_3a7f24964d09701b11f51b7e3c58ac45, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[1] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[1] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noc";
                generator_heap->exception_lineno = 234;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_b;
            generator_heap->var_b = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_b );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_unicode_arg_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_compiled );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_compiled );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "compiled" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_builtin_from_name );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "evaluator" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_1 = PyCell_GET( generator->m_closure[0] );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_force_unicode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_force_unicode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "force_unicode" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( generator_heap->var_b );
        tmp_unicode_arg_1 = generator_heap->var_b;
        tmp_args_element_name_3 = PyObject_Unicode( tmp_unicode_arg_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 234;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_1 );

            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 234;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_unicode_arg_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_unicode_arg_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 234;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 234;
        generator_heap->type_description_1 = "Noc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_b,
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_b );
    generator_heap->var_b = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_b );
    generator_heap->var_b = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_context,
        module_jedi$evaluate$stdlib,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_8034a5ea6d3f98c35e9cf5a85a9b128c,
#endif
        codeobj_3a7f24964d09701b11f51b7e3c58ac45,
        2,
        sizeof(struct jedi$evaluate$stdlib$$$function_10_builtins_isinstance$$$genexpr_3_genexpr_locals)
    );
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_11_collections_namedtuple( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_obj = python_pars[ 1 ];
    PyObject *par_arguments = python_pars[ 2 ];
    struct Nuitka_CellObject *var_collections_context = PyCell_EMPTY();
    PyObject *var__class_template_set = NULL;
    PyObject *var_name = NULL;
    PyObject *var__fields = NULL;
    PyObject *var_fields = NULL;
    struct Nuitka_CellObject *var_get_var = PyCell_EMPTY();
    PyObject *var_base = NULL;
    PyObject *var_code = NULL;
    PyObject *var_module = NULL;
    PyObject *var_generated_class = NULL;
    PyObject *var_parent_context = NULL;
    PyObject *outline_0_var_lazy_context = NULL;
    PyObject *outline_0_var_v = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__contraction_iter_0 = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_1__iter_value_1 = NULL;
    struct Nuitka_FrameObject *frame_99c9beedea81ff81c3aad55f957ebe37;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_59a08b6925cbed0800609fcab58b69b6_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_59a08b6925cbed0800609fcab58b69b6_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_99c9beedea81ff81c3aad55f957ebe37 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_99c9beedea81ff81c3aad55f957ebe37, codeobj_99c9beedea81ff81c3aad55f957ebe37, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_99c9beedea81ff81c3aad55f957ebe37 = cache_frame_99c9beedea81ff81c3aad55f957ebe37;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_99c9beedea81ff81c3aad55f957ebe37 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_99c9beedea81ff81c3aad55f957ebe37 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_obj );
        tmp_source_name_1 = par_obj;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parent_context );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_collections_context ) == NULL );
        PyCell_SET( var_collections_context, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( PyCell_GET( var_collections_context ) );
        tmp_called_instance_1 = PyCell_GET( var_collections_context );
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 248;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_py__getattribute__, &PyTuple_GET_ITEM( const_tuple_str_plain__class_template_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var__class_template_set == NULL );
        var__class_template_set = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var__class_template_set );
        tmp_operand_name_1 = var__class_template_set;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 252;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }

            tmp_return_value = tmp_mvar_value_1;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__follow_param );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__follow_param );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_follow_param" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 257;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_evaluator );
        tmp_args_element_name_1 = par_evaluator;
        CHECK_OBJECT( par_arguments );
        tmp_args_element_name_2 = par_arguments;
        tmp_args_element_name_3 = const_int_0;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 257;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_list_arg_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_called_instance_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 257;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_safe_value );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var_name == NULL );
        var_name = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_list_arg_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_subscript_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__follow_param );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__follow_param );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_follow_param" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 258;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_evaluator );
        tmp_args_element_name_4 = par_evaluator;
        CHECK_OBJECT( par_arguments );
        tmp_args_element_name_5 = par_arguments;
        tmp_args_element_name_6 = const_int_pos_1;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 258;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_list_arg_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        if ( tmp_list_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_2 = PySequence_List( tmp_list_arg_2 );
        Py_DECREF( tmp_list_arg_2 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_assign_source_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var__fields == NULL );
        var__fields = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        CHECK_OBJECT( var__fields );
        tmp_isinstance_inst_1 = var__fields;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_compiled );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_compiled );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "compiled" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 259;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_CompiledObject );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_called_instance_5;
            CHECK_OBJECT( var__fields );
            tmp_called_instance_5 = var__fields;
            frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 260;
            tmp_called_instance_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_get_safe_value );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 260;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 260;
            tmp_called_instance_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_44_str_space_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 260;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 260;
            tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_split );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 260;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            assert( var_fields == NULL );
            var_fields = tmp_assign_source_5;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_5;
            CHECK_OBJECT( var__fields );
            tmp_isinstance_inst_2 = var__fields;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_iterable );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 261;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_5;
            tmp_isinstance_cls_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Sequence );
            if ( tmp_isinstance_cls_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 261;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            Py_DECREF( tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 261;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_6;
                // Tried code:
                {
                    PyObject *tmp_assign_source_7;
                    PyObject *tmp_iter_arg_1;
                    PyObject *tmp_called_instance_6;
                    CHECK_OBJECT( var__fields );
                    tmp_called_instance_6 = var__fields;
                    frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 264;
                    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_py__iter__ );
                    if ( tmp_iter_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 264;
                        type_description_1 = "ooocoooocooooo";
                        goto try_except_handler_2;
                    }
                    tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    Py_DECREF( tmp_iter_arg_1 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 263;
                        type_description_1 = "ooocoooocooooo";
                        goto try_except_handler_2;
                    }
                    assert( tmp_listcomp_1__$0 == NULL );
                    tmp_listcomp_1__$0 = tmp_assign_source_7;
                }
                {
                    PyObject *tmp_assign_source_8;
                    tmp_assign_source_8 = PyList_New( 0 );
                    assert( tmp_listcomp_1__contraction == NULL );
                    tmp_listcomp_1__contraction = tmp_assign_source_8;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_59a08b6925cbed0800609fcab58b69b6_2, codeobj_59a08b6925cbed0800609fcab58b69b6, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *) );
                frame_59a08b6925cbed0800609fcab58b69b6_2 = cache_frame_59a08b6925cbed0800609fcab58b69b6_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_59a08b6925cbed0800609fcab58b69b6_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_59a08b6925cbed0800609fcab58b69b6_2 ) == 2 ); // Frame stack

                // Framed code:
                // Tried code:
                loop_start_1:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_9;
                    CHECK_OBJECT( tmp_listcomp_1__$0 );
                    tmp_next_source_1 = tmp_listcomp_1__$0;
                    tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_9 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_1;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "oo";
                            exception_lineno = 263;
                            goto try_except_handler_3;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_1;
                        tmp_listcomp_1__iter_value_1 = tmp_assign_source_9;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_10;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_1 );
                    tmp_assign_source_10 = tmp_listcomp_1__iter_value_1;
                    {
                        PyObject *old = outline_0_var_lazy_context;
                        outline_0_var_lazy_context = tmp_assign_source_10;
                        Py_INCREF( outline_0_var_lazy_context );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_iter_arg_2;
                    PyObject *tmp_called_instance_7;
                    CHECK_OBJECT( outline_0_var_lazy_context );
                    tmp_called_instance_7 = outline_0_var_lazy_context;
                    frame_59a08b6925cbed0800609fcab58b69b6_2->m_frame.f_lineno = 265;
                    tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_infer );
                    if ( tmp_iter_arg_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 265;
                        type_description_2 = "oo";
                        goto try_except_handler_3;
                    }
                    tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_2 );
                    Py_DECREF( tmp_iter_arg_2 );
                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 263;
                        type_description_2 = "oo";
                        goto try_except_handler_3;
                    }
                    {
                        PyObject *old = tmp_listcomp_1__contraction_iter_0;
                        tmp_listcomp_1__contraction_iter_0 = tmp_assign_source_11;
                        Py_XDECREF( old );
                    }

                }
                loop_start_2:;
                {
                    PyObject *tmp_next_source_2;
                    PyObject *tmp_assign_source_12;
                    CHECK_OBJECT( tmp_listcomp_1__contraction_iter_0 );
                    tmp_next_source_2 = tmp_listcomp_1__contraction_iter_0;
                    tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_2 );
                    if ( tmp_assign_source_12 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_2;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "oo";
                            exception_lineno = 263;
                            goto try_except_handler_3;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_0;
                        tmp_listcomp_1__iter_value_0 = tmp_assign_source_12;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_13;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                    tmp_assign_source_13 = tmp_listcomp_1__iter_value_0;
                    {
                        PyObject *old = outline_0_var_v;
                        outline_0_var_v = tmp_assign_source_13;
                        Py_INCREF( outline_0_var_v );
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_call_result_1;
                    PyObject *tmp_args_element_name_7;
                    int tmp_truth_name_1;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_is_string );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_string );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_string" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 265;
                        type_description_2 = "oo";
                        goto try_except_handler_3;
                    }

                    tmp_called_name_3 = tmp_mvar_value_6;
                    CHECK_OBJECT( outline_0_var_v );
                    tmp_args_element_name_7 = outline_0_var_v;
                    frame_59a08b6925cbed0800609fcab58b69b6_2->m_frame.f_lineno = 265;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7 };
                        tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                    }

                    if ( tmp_call_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 265;
                        type_description_2 = "oo";
                        goto try_except_handler_3;
                    }
                    tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
                    if ( tmp_truth_name_1 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_call_result_1 );

                        exception_lineno = 265;
                        type_description_2 = "oo";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_4 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    Py_DECREF( tmp_call_result_1 );
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_append_list_1;
                        PyObject *tmp_append_value_1;
                        PyObject *tmp_called_instance_8;
                        CHECK_OBJECT( tmp_listcomp_1__contraction );
                        tmp_append_list_1 = tmp_listcomp_1__contraction;
                        CHECK_OBJECT( outline_0_var_v );
                        tmp_called_instance_8 = outline_0_var_v;
                        frame_59a08b6925cbed0800609fcab58b69b6_2->m_frame.f_lineno = 263;
                        tmp_append_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_get_safe_value );
                        if ( tmp_append_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 263;
                            type_description_2 = "oo";
                            goto try_except_handler_3;
                        }
                        assert( PyList_Check( tmp_append_list_1 ) );
                        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                        Py_DECREF( tmp_append_value_1 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 263;
                            type_description_2 = "oo";
                            goto try_except_handler_3;
                        }
                    }
                    branch_no_4:;
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 263;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
                goto loop_start_2;
                loop_end_2:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction_iter_0 );
                Py_DECREF( tmp_listcomp_1__contraction_iter_0 );
                tmp_listcomp_1__contraction_iter_0 = NULL;

                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 263;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
                goto loop_start_1;
                loop_end_1:;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_assign_source_6 = tmp_listcomp_1__contraction;
                Py_INCREF( tmp_assign_source_6 );
                goto try_return_handler_3;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
                return NULL;
                // Return handler code:
                try_return_handler_3:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                Py_XDECREF( tmp_listcomp_1__contraction_iter_0 );
                tmp_listcomp_1__contraction_iter_0 = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_1 );
                tmp_listcomp_1__iter_value_1 = NULL;

                goto frame_return_exit_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                Py_XDECREF( tmp_listcomp_1__contraction_iter_0 );
                tmp_listcomp_1__contraction_iter_0 = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_1 );
                tmp_listcomp_1__iter_value_1 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto frame_exception_exit_2;
                // End of try:

#if 0
                RESTORE_FRAME_EXCEPTION( frame_59a08b6925cbed0800609fcab58b69b6_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_return_exit_2:;
#if 0
                RESTORE_FRAME_EXCEPTION( frame_59a08b6925cbed0800609fcab58b69b6_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto try_return_handler_2;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_59a08b6925cbed0800609fcab58b69b6_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_59a08b6925cbed0800609fcab58b69b6_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_59a08b6925cbed0800609fcab58b69b6_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_59a08b6925cbed0800609fcab58b69b6_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_59a08b6925cbed0800609fcab58b69b6_2,
                    type_description_2,
                    outline_0_var_lazy_context,
                    outline_0_var_v
                );


                // Release cached frame.
                if ( frame_59a08b6925cbed0800609fcab58b69b6_2 == cache_frame_59a08b6925cbed0800609fcab58b69b6_2 )
                {
                    Py_DECREF( frame_59a08b6925cbed0800609fcab58b69b6_2 );
                }
                cache_frame_59a08b6925cbed0800609fcab58b69b6_2 = NULL;

                assertFrameObject( frame_59a08b6925cbed0800609fcab58b69b6_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;
                type_description_1 = "ooocoooocooooo";
                goto try_except_handler_2;
                skip_nested_handling_1:;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
                return NULL;
                // Return handler code:
                try_return_handler_2:;
                Py_XDECREF( outline_0_var_lazy_context );
                outline_0_var_lazy_context = NULL;

                Py_XDECREF( outline_0_var_v );
                outline_0_var_v = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( outline_0_var_lazy_context );
                outline_0_var_lazy_context = NULL;

                Py_XDECREF( outline_0_var_v );
                outline_0_var_v = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
                return NULL;
                outline_exception_1:;
                exception_lineno = 263;
                goto frame_exception_exit_1;
                outline_result_1:;
                assert( var_fields == NULL );
                var_fields = tmp_assign_source_6;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_mvar_value_7;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 268;
                    type_description_1 = "ooocoooocooooo";
                    goto frame_exception_exit_1;
                }

                tmp_return_value = tmp_mvar_value_7;
                Py_INCREF( tmp_return_value );
                goto frame_return_exit_1;
            }
            branch_end_3:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_14)->m_closure[0] = var_collections_context;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_14)->m_closure[0] );


        assert( PyCell_GET( var_get_var ) == NULL );
        PyCell_SET( var_get_var, tmp_assign_source_14 );

    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_called_instance_9;
        PyObject *tmp_value_name_1;
        PyObject *tmp_iter_arg_3;
        CHECK_OBJECT( var__class_template_set );
        tmp_iter_arg_3 = var__class_template_set;
        tmp_value_name_1 = MAKE_ITERATOR( tmp_iter_arg_3 );
        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_9 = ITERATOR_NEXT( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_called_instance_9 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooocoooocooooo";
            exception_lineno = 274;
            goto frame_exception_exit_1;
        }
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 274;
        tmp_assign_source_15 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_get_safe_value );
        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var_base == NULL );
        var_base = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_mvar_value_8;
        CHECK_OBJECT( var_base );
        tmp_left_name_1 = var_base;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__NAMEDTUPLE_INIT );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__NAMEDTUPLE_INIT );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_NAMEDTUPLE_INIT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 275;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_1 = tmp_mvar_value_8;
        tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = tmp_left_name_1;
        var_base = tmp_assign_source_16;

    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_tuple_arg_1;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_called_instance_10;
        PyObject *tmp_called_instance_11;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_tuple_arg_2;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_9;
        CHECK_OBJECT( var_base );
        tmp_source_name_4 = var_base;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_format );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_typename;
        CHECK_OBJECT( var_name );
        tmp_dict_value_1 = var_name;
        tmp_kw_name_1 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_field_names;
        if ( var_fields == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 279;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_arg_1 = var_fields;
        tmp_dict_value_2 = PySequence_Tuple( tmp_tuple_arg_1 );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 279;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_num_fields;
        if ( var_fields == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 280;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_1 = var_fields;
        tmp_dict_value_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 280;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_arg_list;
        if ( var_fields == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 281;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_arg_2 = var_fields;
        tmp_operand_name_2 = PySequence_Tuple( tmp_tuple_arg_2 );
        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 281;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_11 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_called_instance_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 281;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 281;
        tmp_called_instance_10 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_11, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_digest_155aefcd58caf4bb9bda5b4cdc3752d0_str_empty_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_11 );
        if ( tmp_called_instance_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 281;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 281;
        tmp_subscribed_name_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_10, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_39_str_empty_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_10 );
        if ( tmp_subscribed_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 281;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_3 = const_slice_int_pos_1_int_neg_1_none;
        tmp_dict_value_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscribed_name_3 );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 281;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_repr_fmt;
        tmp_source_name_5 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_join );
        assert( !(tmp_called_name_5 == NULL) );
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_iter_arg_4;
            if ( var_fields == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fields" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 282;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }

            tmp_iter_arg_4 = var_fields;
            tmp_assign_source_18 = MAKE_ITERATOR( tmp_iter_arg_4 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 282;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_18;
        }
        // Tried code:
        tmp_args_element_name_8 = jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_8)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );
        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_8)->m_closure[1] = var_get_var;
        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_8)->m_closure[1] );


        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_2;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
        return NULL;
        outline_result_2:;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 282;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_dict_value_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 282;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_field_defs;
        tmp_source_name_6 = const_str_newline;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_join );
        assert( !(tmp_called_name_6 == NULL) );
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_iter_arg_5;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_10;
            tmp_called_name_7 = (PyObject *)&PyEnum_Type;
            if ( var_fields == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fields" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 284;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_10 = var_fields;
            frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 284;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_iter_arg_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            if ( tmp_iter_arg_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 284;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_19 = MAKE_ITERATOR( tmp_iter_arg_5 );
            Py_DECREF( tmp_iter_arg_5 );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 283;
                type_description_1 = "ooocoooocooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_2__$0 == NULL );
            tmp_genexpr_2__$0 = tmp_assign_source_19;
        }
        // Tried code:
        tmp_args_element_name_9 = jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_9)->m_closure[0] = PyCell_NEW0( tmp_genexpr_2__$0 );
        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_9)->m_closure[1] = var_get_var;
        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_9)->m_closure[1] );


        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
        Py_DECREF( tmp_genexpr_2__$0 );
        tmp_genexpr_2__$0 = NULL;

        goto outline_result_3;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
        Py_DECREF( tmp_genexpr_2__$0 );
        tmp_genexpr_2__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
        return NULL;
        outline_result_3:;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 283;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_dict_value_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 283;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 277;
        tmp_assign_source_17 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var_code == NULL );
        var_code = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_called_instance_12;
        PyObject *tmp_source_name_7;
        PyObject *tmp_args_element_name_11;
        CHECK_OBJECT( par_evaluator );
        tmp_source_name_7 = par_evaluator;
        tmp_called_instance_12 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_grammar );
        if ( tmp_called_instance_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 288;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_code );
        tmp_args_element_name_11 = var_code;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 288;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_assign_source_20 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_12, const_str_plain_parse, call_args );
        }

        Py_DECREF( tmp_called_instance_12 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 288;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var_module == NULL );
        var_module = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_value_name_2;
        PyObject *tmp_called_instance_13;
        CHECK_OBJECT( var_module );
        tmp_called_instance_13 = var_module;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 289;
        tmp_value_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_13, const_str_plain_iter_classdefs );
        if ( tmp_value_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_21 = ITERATOR_NEXT( tmp_value_name_2 );
        Py_DECREF( tmp_value_name_2 );
        if ( tmp_assign_source_21 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooocoooocooooo";
            exception_lineno = 289;
            goto frame_exception_exit_1;
        }
        assert( var_generated_class == NULL );
        var_generated_class = tmp_assign_source_21;
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_3;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ModuleContext );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ModuleContext );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ModuleContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 290;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_9;
        CHECK_OBJECT( par_evaluator );
        tmp_tuple_element_1 = par_evaluator;
        tmp_args_name_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_module );
        tmp_tuple_element_1 = var_module;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = Py_None;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        tmp_dict_key_7 = const_str_plain_code_lines;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_parso );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parso );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parso" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 292;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_10;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_split_lines );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 292;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_code );
        tmp_tuple_element_2 = var_code;
        tmp_args_name_2 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
        tmp_kw_name_3 = PyDict_Copy( const_dict_fc2d0675235f4f6c6fd56070d077c5e5 );
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 292;
        tmp_dict_value_7 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_2, tmp_kw_name_3 );
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_dict_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 292;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 290;
        tmp_assign_source_22 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_1, tmp_kw_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 290;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        assert( var_parent_context == NULL );
        var_parent_context = tmp_assign_source_22;
    }
    {
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_args_element_name_15;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 294;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_11;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ClassContext );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ClassContext );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ClassContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 294;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_12;
        CHECK_OBJECT( par_evaluator );
        tmp_args_element_name_13 = par_evaluator;
        CHECK_OBJECT( var_parent_context );
        tmp_args_element_name_14 = var_parent_context;
        CHECK_OBJECT( var_generated_class );
        tmp_args_element_name_15 = var_generated_class;
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 294;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15 };
            tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, call_args );
        }

        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        frame_99c9beedea81ff81c3aad55f957ebe37->m_frame.f_lineno = 294;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "ooocoooocooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_99c9beedea81ff81c3aad55f957ebe37 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_99c9beedea81ff81c3aad55f957ebe37 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_99c9beedea81ff81c3aad55f957ebe37 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_99c9beedea81ff81c3aad55f957ebe37, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_99c9beedea81ff81c3aad55f957ebe37->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_99c9beedea81ff81c3aad55f957ebe37, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_99c9beedea81ff81c3aad55f957ebe37,
        type_description_1,
        par_evaluator,
        par_obj,
        par_arguments,
        var_collections_context,
        var__class_template_set,
        var_name,
        var__fields,
        var_fields,
        var_get_var,
        var_base,
        var_code,
        var_module,
        var_generated_class,
        var_parent_context
    );


    // Release cached frame.
    if ( frame_99c9beedea81ff81c3aad55f957ebe37 == cache_frame_99c9beedea81ff81c3aad55f957ebe37 )
    {
        Py_DECREF( frame_99c9beedea81ff81c3aad55f957ebe37 );
    }
    cache_frame_99c9beedea81ff81c3aad55f957ebe37 = NULL;

    assertFrameObject( frame_99c9beedea81ff81c3aad55f957ebe37 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)var_collections_context );
    Py_DECREF( var_collections_context );
    var_collections_context = NULL;

    CHECK_OBJECT( (PyObject *)var__class_template_set );
    Py_DECREF( var__class_template_set );
    var__class_template_set = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var__fields );
    var__fields = NULL;

    Py_XDECREF( var_fields );
    var_fields = NULL;

    CHECK_OBJECT( (PyObject *)var_get_var );
    Py_DECREF( var_get_var );
    var_get_var = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    Py_XDECREF( var_code );
    var_code = NULL;

    Py_XDECREF( var_module );
    var_module = NULL;

    Py_XDECREF( var_generated_class );
    var_generated_class = NULL;

    Py_XDECREF( var_parent_context );
    var_parent_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    CHECK_OBJECT( (PyObject *)var_collections_context );
    Py_DECREF( var_collections_context );
    var_collections_context = NULL;

    Py_XDECREF( var__class_template_set );
    var__class_template_set = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var__fields );
    var__fields = NULL;

    Py_XDECREF( var_fields );
    var_fields = NULL;

    CHECK_OBJECT( (PyObject *)var_get_var );
    Py_DECREF( var_get_var );
    var_get_var = NULL;

    Py_XDECREF( var_base );
    var_base = NULL;

    Py_XDECREF( var_code );
    var_code = NULL;

    Py_XDECREF( var_module );
    var_module = NULL;

    Py_XDECREF( var_generated_class );
    var_generated_class = NULL;

    Py_XDECREF( var_parent_context );
    var_parent_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *var_x = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_7657bae6b5fa40223a88efbcdede2d9f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7657bae6b5fa40223a88efbcdede2d9f = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7657bae6b5fa40223a88efbcdede2d9f, codeobj_7657bae6b5fa40223a88efbcdede2d9f, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7657bae6b5fa40223a88efbcdede2d9f = cache_frame_7657bae6b5fa40223a88efbcdede2d9f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7657bae6b5fa40223a88efbcdede2d9f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7657bae6b5fa40223a88efbcdede2d9f ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "collections_context" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_name );
        tmp_args_element_name_1 = par_name;
        frame_7657bae6b5fa40223a88efbcdede2d9f->m_frame.f_lineno = 271;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_py__getattribute__, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooc";
            exception_lineno = 271;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooc";
                    exception_lineno = 271;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 1)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooc";
            exception_lineno = 271;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_3 = tmp_tuple_unpack_1__element_1;
        assert( var_x == NULL );
        Py_INCREF( tmp_assign_source_3 );
        var_x = tmp_assign_source_3;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_x );
        tmp_called_instance_2 = var_x;
        frame_7657bae6b5fa40223a88efbcdede2d9f->m_frame.f_lineno = 272;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_safe_value );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7657bae6b5fa40223a88efbcdede2d9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7657bae6b5fa40223a88efbcdede2d9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7657bae6b5fa40223a88efbcdede2d9f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7657bae6b5fa40223a88efbcdede2d9f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7657bae6b5fa40223a88efbcdede2d9f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7657bae6b5fa40223a88efbcdede2d9f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7657bae6b5fa40223a88efbcdede2d9f,
        type_description_1,
        par_name,
        var_x,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_7657bae6b5fa40223a88efbcdede2d9f == cache_frame_7657bae6b5fa40223a88efbcdede2d9f )
    {
        Py_DECREF( frame_7657bae6b5fa40223a88efbcdede2d9f );
    }
    cache_frame_7657bae6b5fa40223a88efbcdede2d9f = NULL;

    assertFrameObject( frame_7657bae6b5fa40223a88efbcdede2d9f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_x );
    Py_DECREF( var_x );
    var_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_locals {
    PyObject *var_name;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_name = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_31c72a8b54b17b3482e4174e58324ed0, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noc";
                generator_heap->exception_lineno = 282;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_name;
            generator_heap->var_name = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_name );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_var" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = PyCell_GET( generator->m_closure[1] );
        generator->m_frame->m_frame.f_lineno = 282;
        tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain__repr_template_tuple, 0 ) );

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_dict_key_1 = const_str_plain_name;
        CHECK_OBJECT( generator_heap->var_name );
        tmp_dict_value_1 = generator_heap->var_name;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(generator_heap->tmp_res != 0) );
        generator->m_frame->m_frame.f_lineno = 282;
        tmp_expression_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 282;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 282;
        generator_heap->type_description_1 = "Noc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_name,
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_context,
        module_jedi$evaluate$stdlib,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_ec4e4044dc14fa3687c4a45457ca7489,
#endif
        codeobj_31c72a8b54b17b3482e4174e58324ed0,
        2,
        sizeof(struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_1_genexpr_locals)
    );
}



struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_locals {
    PyObject *var_index;
    PyObject *var_name;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_locals *generator_heap = (struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_index = NULL;
    generator_heap->var_name = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_ed90bf1b92b2123d2a7dac4520e6cd1f, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nooc";
                generator_heap->exception_lineno = 283;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 283;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Nooc";
            generator_heap->exception_lineno = 283;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Nooc";
            generator_heap->exception_lineno = 283;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Nooc";
                    generator_heap->exception_lineno = 283;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Nooc";
            generator_heap->exception_lineno = 283;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_index;
            generator_heap->var_index = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_index );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_name;
            generator_heap->var_name = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_name );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_var" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 283;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = PyCell_GET( generator->m_closure[1] );
        generator->m_frame->m_frame.f_lineno = 283;
        tmp_source_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain__field_template_tuple, 0 ) );

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 283;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 283;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        tmp_dict_key_1 = const_str_plain_index;
        CHECK_OBJECT( generator_heap->var_index );
        tmp_dict_value_1 = generator_heap->var_index;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(generator_heap->tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_name;
        CHECK_OBJECT( generator_heap->var_name );
        tmp_dict_value_2 = generator_heap->var_name;
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(generator_heap->tmp_res != 0) );
        generator->m_frame->m_frame.f_lineno = 283;
        tmp_expression_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 283;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 283;
            generator_heap->type_description_1 = "Nooc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 283;
        generator_heap->type_description_1 = "Nooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_index,
            generator_heap->var_name,
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_index );
    generator_heap->var_index = NULL;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_index );
    generator_heap->var_index = NULL;

    Py_XDECREF( generator_heap->var_name );
    generator_heap->var_name = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_context,
        module_jedi$evaluate$stdlib,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_ec4e4044dc14fa3687c4a45457ca7489,
#endif
        codeobj_ed90bf1b92b2123d2a7dac4520e6cd1f,
        2,
        sizeof(struct jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$genexpr_2_genexpr_locals)
    );
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_12__return_first_param( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_firsts = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    CHECK_OBJECT( par_firsts );
    tmp_return_value = par_firsts;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_12__return_first_param );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_firsts );
    Py_DECREF( par_firsts );
    par_firsts = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_12__return_first_param );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_13_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_obj = python_pars[ 1 ];
    PyObject *par_arguments = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_95a96bf5ce3d7c92f0e63f7753303d2a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_95a96bf5ce3d7c92f0e63f7753303d2a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_95a96bf5ce3d7c92f0e63f7753303d2a, codeobj_95a96bf5ce3d7c92f0e63f7753303d2a, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_95a96bf5ce3d7c92f0e63f7753303d2a = cache_frame_95a96bf5ce3d7c92f0e63f7753303d2a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_95a96bf5ce3d7c92f0e63f7753303d2a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_95a96bf5ce3d7c92f0e63f7753303d2a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 315;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_95a96bf5ce3d7c92f0e63f7753303d2a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_95a96bf5ce3d7c92f0e63f7753303d2a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_95a96bf5ce3d7c92f0e63f7753303d2a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_95a96bf5ce3d7c92f0e63f7753303d2a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_95a96bf5ce3d7c92f0e63f7753303d2a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_95a96bf5ce3d7c92f0e63f7753303d2a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_95a96bf5ce3d7c92f0e63f7753303d2a,
        type_description_1,
        par_evaluator,
        par_obj,
        par_arguments
    );


    // Release cached frame.
    if ( frame_95a96bf5ce3d7c92f0e63f7753303d2a == cache_frame_95a96bf5ce3d7c92f0e63f7753303d2a )
    {
        Py_DECREF( frame_95a96bf5ce3d7c92f0e63f7753303d2a );
    }
    cache_frame_95a96bf5ce3d7c92f0e63f7753303d2a = NULL;

    assertFrameObject( frame_95a96bf5ce3d7c92f0e63f7753303d2a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_13_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_13_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$stdlib$$$function_14_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_evaluator = python_pars[ 0 ];
    PyObject *par_obj = python_pars[ 1 ];
    PyObject *par_arguments = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_50a2e40e3a8979dde27972d8018bbcbe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_50a2e40e3a8979dde27972d8018bbcbe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_50a2e40e3a8979dde27972d8018bbcbe, codeobj_50a2e40e3a8979dde27972d8018bbcbe, module_jedi$evaluate$stdlib, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_50a2e40e3a8979dde27972d8018bbcbe = cache_frame_50a2e40e3a8979dde27972d8018bbcbe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_50a2e40e3a8979dde27972d8018bbcbe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_50a2e40e3a8979dde27972d8018bbcbe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 316;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50a2e40e3a8979dde27972d8018bbcbe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_50a2e40e3a8979dde27972d8018bbcbe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50a2e40e3a8979dde27972d8018bbcbe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_50a2e40e3a8979dde27972d8018bbcbe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_50a2e40e3a8979dde27972d8018bbcbe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_50a2e40e3a8979dde27972d8018bbcbe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_50a2e40e3a8979dde27972d8018bbcbe,
        type_description_1,
        par_evaluator,
        par_obj,
        par_arguments
    );


    // Release cached frame.
    if ( frame_50a2e40e3a8979dde27972d8018bbcbe == cache_frame_50a2e40e3a8979dde27972d8018bbcbe )
    {
        Py_DECREF( frame_50a2e40e3a8979dde27972d8018bbcbe );
    }
    cache_frame_50a2e40e3a8979dde27972d8018bbcbe = NULL;

    assertFrameObject( frame_50a2e40e3a8979dde27972d8018bbcbe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_14_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_evaluator );
    Py_DECREF( par_evaluator );
    par_evaluator = NULL;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_arguments );
    Py_DECREF( par_arguments );
    par_arguments = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib$$$function_14_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_10_builtins_isinstance(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_10_builtins_isinstance,
        const_str_plain_builtins_isinstance,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_983ebaf86279bbca491414011c2389dc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_11_collections_namedtuple(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_11_collections_namedtuple,
        const_str_plain_collections_namedtuple,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_99c9beedea81ff81c3aad55f957ebe37,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        const_str_digest_c53d2a4a26885939c8baa5017b23583f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_11_collections_namedtuple$$$function_1_get_var,
        const_str_plain_get_var,
#if PYTHON_VERSION >= 300
        const_str_digest_723974f56302e163bbe2d0cac1cc3598,
#endif
        codeobj_7657bae6b5fa40223a88efbcdede2d9f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_12__return_first_param(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_12__return_first_param,
        const_str_plain__return_first_param,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_98835c9bd577208e246a74f7d6f0dbcd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_13_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_13_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_95a96bf5ce3d7c92f0e63f7753303d2a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_14_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_14_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_50a2e40e3a8979dde27972d8018bbcbe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_1_execute(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_1_execute,
        const_str_plain_execute,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bc25a2a819f7291ec7c39683820b6f0c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_2__follow_param(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_2__follow_param,
        const_str_plain__follow_param,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_59f7f26d1c9aa31b00f6ea05ecfe0310,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_3_argument_clinic,
        const_str_plain_argument_clinic,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0b2afb1033a8457e09941ca11951de5a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        const_str_digest_1db8cd3ef2f7d1f4e6a2e71b84ba2dd3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f,
        const_str_plain_f,
#if PYTHON_VERSION >= 300
        const_str_digest_f4f9c2a9113d5b51fa01e3be21e1a4ea,
#endif
        codeobj_d109c42f3d5aa99af2eb7e9298d90e7b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_3_argument_clinic$$$function_1_f$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_86d2373b6d1e6c0afaec8f7b162c3e3a,
#endif
        codeobj_44d0aa66af007c065af3399f112902ff,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_4_builtins_next(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_4_builtins_next,
        const_str_plain_builtins_next,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f4123a9e37aacd5ea9bbe93291fa27b0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        const_str_digest_7ea303fd6c9d57c037274ff98ba3dd3f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_5_builtins_getattr( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_5_builtins_getattr,
        const_str_plain_builtins_getattr,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7ff73fcb8cd300de4e8347620b2c0d61,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_6_builtins_type(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_6_builtins_type,
        const_str_plain_builtins_type,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_814fb51d36825a2e958c0290e8e70d23,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_7___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_7___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_e3bbab13d8c6c6a5fae7b76afdeef3aa,
#endif
        codeobj_4bc33372de037fb8e6151bb35b89b75c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_8_builtins_super(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_8_builtins_super,
        const_str_plain_builtins_super,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_32beddd2d5247d984511acc86158d32e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_9_builtins_reversed(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$stdlib$$$function_9_builtins_reversed,
        const_str_plain_builtins_reversed,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7f8ec4eec6d1fff5c9fd1c15a0bd31f5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$stdlib,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jedi$evaluate$stdlib =
{
    PyModuleDef_HEAD_INIT,
    "jedi.evaluate.stdlib",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jedi$evaluate$stdlib)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jedi$evaluate$stdlib)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jedi$evaluate$stdlib );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.stdlib: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.stdlib: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.stdlib: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjedi$evaluate$stdlib" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jedi$evaluate$stdlib = Py_InitModule4(
        "jedi.evaluate.stdlib",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jedi$evaluate$stdlib = PyModule_Create( &mdef_jedi$evaluate$stdlib );
#endif

    moduledict_jedi$evaluate$stdlib = MODULE_DICT( module_jedi$evaluate$stdlib );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jedi$evaluate$stdlib,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jedi$evaluate$stdlib,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate$stdlib,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate$stdlib,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jedi$evaluate$stdlib );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_9c5b414e40bcc6854c5f0b647dc1b864, module_jedi$evaluate$stdlib );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    struct Nuitka_CellObject *outline_1_var___class__ = PyCell_EMPTY();
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    struct Nuitka_FrameObject *frame_4e96a1be1e7dd300ccb16a03a7fd2fad;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jedi$evaluate$stdlib_46 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *locals_jedi$evaluate$stdlib_157 = NULL;
    struct Nuitka_FrameObject *frame_24f155bac913fc03dc2184a111ba1b11_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_24f155bac913fc03dc2184a111ba1b11_2 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_042c753535689c6a18265d88587540e6;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_4e96a1be1e7dd300ccb16a03a7fd2fad = MAKE_MODULE_FRAME( codeobj_4e96a1be1e7dd300ccb16a03a7fd2fad, module_jedi$evaluate$stdlib );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_4e96a1be1e7dd300ccb16a03a7fd2fad );
    assert( Py_REFCNT( frame_4e96a1be1e7dd300ccb16a03a7fd2fad ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_parso;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 12;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_parso, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_4ea3c739c0994e1c59a1638cbf9bd81c;
        tmp_globals_name_2 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_force_unicode_tuple;
        tmp_level_name_2 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 14;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_force_unicode );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_force_unicode, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_jedi;
        tmp_globals_name_3 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_debug_tuple;
        tmp_level_name_3 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 15;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_debug );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_debug, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_93e555a5b1643558b28cf6b2256158dc;
        tmp_globals_name_4 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_337c362010bc06f6b8ae08f820125e79_tuple;
        tmp_level_name_4 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 16;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_7;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_ValuesArguments );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ValuesArguments, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_repack_with_argument_clinic );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_repack_with_argument_clinic, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_5 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_analysis_tuple;
        tmp_level_name_5 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 17;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_analysis );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_analysis, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_6 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_compiled_tuple;
        tmp_level_name_6 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 18;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_compiled );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_compiled, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_36e46c252a3e3bf20afff1d05096f2ab;
        tmp_globals_name_7 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_ebc97d80f46d087cb3f9a0cc3518654c_tuple;
        tmp_level_name_7 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 19;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_12;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_AbstractInstanceContext );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_AbstractInstanceContext, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_CompiledInstance );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_CompiledInstance, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_BoundMethod );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_BoundMethod, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_10 = tmp_import_from_2__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_InstanceArguments );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_InstanceArguments, tmp_assign_source_16 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_74a64e2326d0e94475afe0623d07b03d;
        tmp_globals_name_8 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_6d103a741b243e268a279146e9f9d03c_tuple;
        tmp_level_name_8 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 21;
        tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_17;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_11 = tmp_import_from_3__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_ContextualizedNode );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextualizedNode, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_12 = tmp_import_from_3__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_NO_CONTEXTS );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_13 = tmp_import_from_3__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_ContextSet );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ContextSet, tmp_assign_source_20 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_785ebe15053a4fa731c98e296de8edd7;
        tmp_globals_name_9 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_ca55646d5a4f44b7976271ef44596511_tuple;
        tmp_level_name_9 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 23;
        tmp_assign_source_21 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_21;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_14 = tmp_import_from_4__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_ClassContext );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ClassContext, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_15 = tmp_import_from_4__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_ModuleContext );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_ModuleContext, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_16 = tmp_import_from_4__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_FunctionExecutionContext );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_FunctionExecutionContext, tmp_assign_source_24 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_17;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_785ebe15053a4fa731c98e296de8edd7;
        tmp_globals_name_10 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_iterable_tuple;
        tmp_level_name_10 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 24;
        tmp_import_name_from_17 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_iterable );
        Py_DECREF( tmp_import_name_from_17 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_iterable, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_18;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_4b6ae0a4a7c5ef73221de5bb2b8ff0e4;
        tmp_globals_name_11 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_LazyTreeContext_tuple;
        tmp_level_name_11 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 25;
        tmp_import_name_from_18 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_LazyTreeContext );
        Py_DECREF( tmp_import_name_from_18 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_LazyTreeContext, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_19;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_3e1604d04ce104740ab4a0cd2cf306db;
        tmp_globals_name_12 = (PyObject *)moduledict_jedi$evaluate$stdlib;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_is_string_tuple;
        tmp_level_name_12 = const_int_0;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 26;
        tmp_import_name_from_19 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_import_name_from_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_is_string );
        Py_DECREF( tmp_import_name_from_19 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_is_string, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = const_str_digest_d24bac973c171b9040e8ff82c6c4078c;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__NAMEDTUPLE_INIT, tmp_assign_source_28 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_LookupError_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_29 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_29;
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_30;
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_31 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_31;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_5;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_5;
            }
            tmp_tuple_element_1 = const_str_plain_NotInStdLib;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 46;
            tmp_assign_source_32 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_32;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_5;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 46;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 46;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 46;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_33;
            tmp_assign_source_33 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_33;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_34;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jedi$evaluate$stdlib_46 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_9c5b414e40bcc6854c5f0b647dc1b864;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_46, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_NotInStdLib;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_46, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_7;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_LookupError_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_7;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_LookupError_tuple;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_46, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_7;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_NotInStdLib;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jedi$evaluate$stdlib_46;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 46;
            tmp_assign_source_35 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_7;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_35;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_34 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_34 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_jedi$evaluate$stdlib_46 );
        locals_jedi$evaluate$stdlib_46 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$stdlib_46 );
        locals_jedi$evaluate$stdlib_46 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 46;
        goto try_except_handler_5;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_NotInStdLib, tmp_assign_source_34 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_1_execute(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_execute, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        tmp_assign_source_37 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_2__follow_param(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__follow_param, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_false_false_false_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_38 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_3_argument_clinic( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_4 = tmp_mvar_value_3;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 111;
        tmp_called_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_digest_71d72ea4a426b505f2886f81f114ef20_tuple, 0 ) );

        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_4_builtins_next(  );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_next, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_called_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_defaults_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_4;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 135;
        tmp_called_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_digest_4e3cbccd16e9b40484f60435e970bec1_tuple, 0 ) );

        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;

            goto frame_exception_exit_1;
        }
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_args_element_name_2 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_5_builtins_getattr( tmp_defaults_2 );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 135;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_40 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_getattr, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_called_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_5;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 148;
        tmp_called_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_str_digest_568d907a1a4a83fadc4155df91d2cd9a_tuple, 0 ) );

        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_6_builtins_type(  );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 148;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_type, tmp_assign_source_41 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_AbstractInstanceContext );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AbstractInstanceContext );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AbstractInstanceContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;

            goto try_except_handler_8;
        }

        tmp_tuple_element_4 = tmp_mvar_value_6;
        tmp_assign_source_42 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assign_source_42, 0, tmp_tuple_element_4 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_42;
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_43 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_43;
    }
    {
        PyObject *tmp_assign_source_44;
        tmp_assign_source_44 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_44;
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_45 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_45;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_8;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_9;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_8;
            }
            tmp_tuple_element_5 = const_str_plain_SuperInstance;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 157;
            tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_46;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_8;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 157;

                    goto try_except_handler_8;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 157;

                    goto try_except_handler_8;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 157;

                    goto try_except_handler_8;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 157;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_8;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_47;
            tmp_assign_source_47 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_47;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_48;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_jedi$evaluate$stdlib_157 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_9c5b414e40bcc6854c5f0b647dc1b864;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_157, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_digest_aa9336f5c47581fe04ea84da97f82efe;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_157, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_plain_SuperInstance;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_157, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;

            goto try_except_handler_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_24f155bac913fc03dc2184a111ba1b11_2, codeobj_24f155bac913fc03dc2184a111ba1b11, module_jedi$evaluate$stdlib, sizeof(void *) );
        frame_24f155bac913fc03dc2184a111ba1b11_2 = cache_frame_24f155bac913fc03dc2184a111ba1b11_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_24f155bac913fc03dc2184a111ba1b11_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_24f155bac913fc03dc2184a111ba1b11_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_7___init__(  );

        ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_1_var___class__;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );


        tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_157, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_2 = "c";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_24f155bac913fc03dc2184a111ba1b11_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_24f155bac913fc03dc2184a111ba1b11_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_24f155bac913fc03dc2184a111ba1b11_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_24f155bac913fc03dc2184a111ba1b11_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_24f155bac913fc03dc2184a111ba1b11_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_24f155bac913fc03dc2184a111ba1b11_2,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_24f155bac913fc03dc2184a111ba1b11_2 == cache_frame_24f155bac913fc03dc2184a111ba1b11_2 )
        {
            Py_DECREF( frame_24f155bac913fc03dc2184a111ba1b11_2 );
        }
        cache_frame_24f155bac913fc03dc2184a111ba1b11_2 = NULL;

        assertFrameObject( frame_24f155bac913fc03dc2184a111ba1b11_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_10;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_10;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$stdlib_157, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_10;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_49;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_10 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_7 = const_str_plain_SuperInstance;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_7 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
            tmp_tuple_element_7 = locals_jedi$evaluate$stdlib_157;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 157;
            tmp_assign_source_49 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_49 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;

                goto try_except_handler_10;
            }
            {
                PyObject *old = PyCell_GET( outline_1_var___class__ );
                PyCell_SET( outline_1_var___class__, tmp_assign_source_49 );
                Py_XDECREF( old );
            }

        }
        CHECK_OBJECT( PyCell_GET( outline_1_var___class__ ) );
        tmp_assign_source_48 = PyCell_GET( outline_1_var___class__ );
        Py_INCREF( tmp_assign_source_48 );
        goto try_return_handler_10;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        Py_DECREF( locals_jedi$evaluate$stdlib_157 );
        locals_jedi$evaluate$stdlib_157 = NULL;
        goto try_return_handler_9;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$stdlib_157 );
        locals_jedi$evaluate$stdlib_157 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_9;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$stdlib );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 157;
        goto try_except_handler_8;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_SuperInstance, tmp_assign_source_48 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_called_name_11;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_name_5;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_7;
        tmp_args_name_5 = const_tuple_str_digest_6f8e18a5f8e1852adedd760bc2cdce2a_tuple;
        tmp_kw_name_5 = PyDict_Copy( const_dict_0ef631920779735f6d8ddb2e395f99d0 );
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 164;
        tmp_called_name_11 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_5, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_8_builtins_super(  );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_super, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_called_name_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_name_6;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_8;
        tmp_args_name_6 = const_tuple_str_digest_c19157e8112b641864481c8e6f9ac442_tuple;
        tmp_kw_name_6 = PyDict_Copy( const_dict_31468f992b2dfacc6e9a4f2c8c95b093 );
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 175;
        tmp_called_name_13 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_6, tmp_kw_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_called_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_5 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_9_builtins_reversed(  );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_51 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_reversed, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_called_name_15;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_name_7;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_9;
        tmp_args_name_7 = const_tuple_str_digest_bc33aa076dc4474ec3baf1b192e9fc32_tuple;
        tmp_kw_name_7 = PyDict_Copy( const_dict_de706df0108fd8fcd92a5da8a4cc94aa );
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 197;
        tmp_called_name_15 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_7, tmp_kw_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_called_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_6 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_10_builtins_isinstance(  );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 197;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_52 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_called_name_15 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_isinstance, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        tmp_assign_source_53 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_11_collections_namedtuple(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_collections_namedtuple, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_called_name_17;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_argument_clinic );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_argument_clinic );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "argument_clinic" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 297;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_10;
        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 297;
        tmp_called_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_digest_dcc5f9e29092af34f59364b2136f9ebf_tuple, 0 ) );

        if ( tmp_called_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_12__return_first_param(  );



        frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame.f_lineno = 297;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 297;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__return_first_param, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_mvar_value_18;
        tmp_dict_key_1 = const_str_plain_builtins;
        tmp_dict_key_2 = const_str_plain_getattr;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_getattr );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins_getattr );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins_getattr" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 304;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = tmp_mvar_value_11;
        tmp_dict_value_1 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_dict_value_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_type;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_type );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins_type );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_dict_value_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 305;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_3 = tmp_mvar_value_12;
        tmp_res = PyDict_SetItem( tmp_dict_value_1, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_super;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_super );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins_super );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_dict_value_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins_super" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 306;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_4 = tmp_mvar_value_13;
        tmp_res = PyDict_SetItem( tmp_dict_value_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_reversed;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_reversed );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins_reversed );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_dict_value_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins_reversed" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 307;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_5 = tmp_mvar_value_14;
        tmp_res = PyDict_SetItem( tmp_dict_value_1, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_isinstance;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_builtins_isinstance );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_builtins_isinstance );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_dict_value_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "builtins_isinstance" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 308;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_6 = tmp_mvar_value_15;
        tmp_res = PyDict_SetItem( tmp_dict_value_1, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_assign_source_55 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_assign_source_55, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_copy;
        tmp_dict_key_8 = const_str_plain_copy;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__return_first_param );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__return_first_param );
        }

        CHECK_OBJECT( tmp_mvar_value_16 );
        tmp_dict_value_8 = tmp_mvar_value_16;
        tmp_dict_value_7 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_dict_value_7, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_deepcopy;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__return_first_param );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__return_first_param );
        }

        CHECK_OBJECT( tmp_mvar_value_17 );
        tmp_dict_value_9 = tmp_mvar_value_17;
        tmp_res = PyDict_SetItem( tmp_dict_value_7, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_55, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_json;
        tmp_dict_key_11 = const_str_plain_load;
        tmp_dict_value_11 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_13_lambda(  );



        tmp_dict_value_10 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_dict_value_10, tmp_dict_key_11, tmp_dict_value_11 );
        Py_DECREF( tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_loads;
        tmp_dict_value_12 = MAKE_FUNCTION_jedi$evaluate$stdlib$$$function_14_lambda(  );



        tmp_res = PyDict_SetItem( tmp_dict_value_10, tmp_dict_key_12, tmp_dict_value_12 );
        Py_DECREF( tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_55, tmp_dict_key_10, tmp_dict_value_10 );
        Py_DECREF( tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_str_plain_collections;
        tmp_dict_key_14 = const_str_plain_namedtuple;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain_collections_namedtuple );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_collections_namedtuple );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_assign_source_55 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "collections_namedtuple" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 319;

            goto frame_exception_exit_1;
        }

        tmp_dict_value_14 = tmp_mvar_value_18;
        tmp_dict_value_13 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_dict_value_13, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_res = PyDict_SetItem( tmp_assign_source_55, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$stdlib, (Nuitka_StringObject *)const_str_plain__implemented, tmp_assign_source_55 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4e96a1be1e7dd300ccb16a03a7fd2fad );
#endif
    popFrameStack();

    assertFrameObject( frame_4e96a1be1e7dd300ccb16a03a7fd2fad );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4e96a1be1e7dd300ccb16a03a7fd2fad );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4e96a1be1e7dd300ccb16a03a7fd2fad, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4e96a1be1e7dd300ccb16a03a7fd2fad->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4e96a1be1e7dd300ccb16a03a7fd2fad, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_jedi$evaluate$stdlib );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
