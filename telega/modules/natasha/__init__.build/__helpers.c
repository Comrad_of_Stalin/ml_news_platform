// This file contains helper functions that are automatically created from
// templates.

#include "nuitka/prelude.h"

extern PyObject *callPythonFunction( PyObject *func, PyObject **args, int count );


PyObject *CALL_FUNCTION_WITH_ARGS1( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 1 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 1; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 1 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 1 * sizeof(PyObject *) );
            memcpy( python_pars + 1, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 1 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 1 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 1; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 1 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 1 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 1, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 1 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 1 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (1 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 1 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (1 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 1 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 1, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 1 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            1
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 1 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS2( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 2; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 2 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 2; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 2 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 2 * sizeof(PyObject *) );
            memcpy( python_pars + 2, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 2 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 2 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 2; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 2 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 2 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 2, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 2 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 2 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (2 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 2 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (2 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 2 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 2, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 2 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            2
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 2 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS3( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 3; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 3 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 3; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 3 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 3 * sizeof(PyObject *) );
            memcpy( python_pars + 3, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 3 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 3 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 3; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 3 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 3 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 3, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 3 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 3 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (3 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 3 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (3 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 3 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 3, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 3 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            3
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 3 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS4( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 4; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 4 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 4; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 4 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 4 * sizeof(PyObject *) );
            memcpy( python_pars + 4, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 4 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 4 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 4; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 4 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 4 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 4, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 4 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 4 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (4 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 4 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (4 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 4 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 4, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 4 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            4
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 4 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS5( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 5; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 5 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 5; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 5 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 5 * sizeof(PyObject *) );
            memcpy( python_pars + 5, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 5 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 5 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 5; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 5 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 5 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 5, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 5 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 5 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (5 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 5 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (5 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 5 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 5, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 5 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            5
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 5 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS6( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 6; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 6 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 6; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 6 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 6 * sizeof(PyObject *) );
            memcpy( python_pars + 6, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 6 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 6 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 6; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 6 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 6 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 6, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 6 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 6 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (6 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 6 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (6 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 6 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 6, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 6 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            6
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 6 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS7( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 7; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 7 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 7; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 7 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 7 * sizeof(PyObject *) );
            memcpy( python_pars + 7, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 7 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 7 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 7; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 7 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 7 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 7, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 7 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 7 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (7 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 7 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (7 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 7 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 7, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 7 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            7
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 7 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS8( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 8; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 8 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 8; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 8 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 8 * sizeof(PyObject *) );
            memcpy( python_pars + 8, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 8 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 8 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 8; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 8 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 8 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 8, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 8 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 8 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (8 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 8 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (8 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 8 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 8, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 8 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            8
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 8 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS9( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 9; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 9 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 9; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 9 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 9 * sizeof(PyObject *) );
            memcpy( python_pars + 9, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 9 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 9 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 9; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 9 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 9 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 9, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 9 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 9 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (9 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 9 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (9 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 9 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 9, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 9 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            9
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 9 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS10( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 10; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 10 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 10; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 10 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 10 * sizeof(PyObject *) );
            memcpy( python_pars + 10, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 10 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 10 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 10; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 10 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 10 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 10, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 10 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 10 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (10 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 10 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (10 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 10 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 10, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 10 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            10
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 10 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS11( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 11; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 11 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 11; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 11 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 11 * sizeof(PyObject *) );
            memcpy( python_pars + 11, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 11 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 11 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 11; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 11 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 11 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 11, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 11 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 11 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (11 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 11 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (11 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 11 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 11, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 11 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            11
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 11 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS13( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 13; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 13 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 13; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 13 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 13 * sizeof(PyObject *) );
            memcpy( python_pars + 13, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 13 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 13 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 13; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 13 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 13 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 13, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 13 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 13 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (13 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 13 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (13 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 13 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 13, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 13 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            13
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 13 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS14( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 14; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 14 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 14; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 14 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 14 * sizeof(PyObject *) );
            memcpy( python_pars + 14, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 14 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 14 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 14; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 14 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 14 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 14, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 14 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 14 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (14 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 14 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (14 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 14 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 14, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 14 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            14
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 14 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS15( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 15; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 15 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 15; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 15 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 15 * sizeof(PyObject *) );
            memcpy( python_pars + 15, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 15 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 15 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 15; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 15 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 15 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 15, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 15 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 15 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (15 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 15 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (15 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 15 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 15, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 15 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            15
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 15 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS16( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 16; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 16 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 16; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 16 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 16 * sizeof(PyObject *) );
            memcpy( python_pars + 16, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 16 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 16 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 16; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 16 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 16 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 16, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 16 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 16 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (16 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 16 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (16 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 16 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 16, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 16 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            16
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 16 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_FUNCTION_WITH_ARGS22( PyObject *called, PyObject **args )
{
    CHECK_OBJECT( called );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 22; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    if ( Nuitka_Function_Check( called ) )
    {
        if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
        {
            return NULL;
        }

        struct Nuitka_FunctionObject *function = (struct Nuitka_FunctionObject *)called;
        PyObject *result;

        if ( function->m_args_simple && 22 == function->m_args_positional_count )
        {
            for( Py_ssize_t i = 0; i < 22; i++ )
            {
                Py_INCREF( args[ i ] );
            }

            result = function->m_c_code( function, args );
        }
        else if ( function->m_args_simple && 22 + function->m_defaults_given == function->m_args_positional_count )
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
            PyObject *python_pars[ function->m_args_positional_count ];
#endif
            memcpy( python_pars, args, 22 * sizeof(PyObject *) );
            memcpy( python_pars + 22, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

            for( Py_ssize_t i = 0; i < function->m_args_positional_count; i++ )
            {
                Py_INCREF( python_pars[ i ] );
            }

            result = function->m_c_code( function, python_pars );
        }
        else
        {
#ifdef _MSC_VER
            PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
            PyObject *python_pars[ function->m_args_overall_count ];
#endif
            memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

            if ( parseArgumentsPos( function, python_pars, args, 22 ))
            {
                result = function->m_c_code( function, python_pars );
            }
            else
            {
                result = NULL;
            }
        }

        Py_LeaveRecursiveCall();

        return result;
    }
    else if ( Nuitka_Method_Check( called ) )
    {
        struct Nuitka_MethodObject *method = (struct Nuitka_MethodObject *)called;

        // Unbound method without arguments, let the error path be slow.
        if ( method->m_object != NULL )
        {
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }

            struct Nuitka_FunctionObject *function = method->m_function;

            PyObject *result;

            if ( function->m_args_simple && 22 + 1 == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                for( Py_ssize_t i = 0; i < 22; i++ )
                {
                    python_pars[ i + 1 ] = args[ i ];
                    Py_INCREF( args[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else if ( function->m_args_simple && 22 + 1 + function->m_defaults_given == function->m_args_positional_count )
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_positional_count );
#else
                PyObject *python_pars[ function->m_args_positional_count ];
#endif
                python_pars[ 0 ] = method->m_object;
                Py_INCREF( method->m_object );

                memcpy( python_pars+1, args, 22 * sizeof(PyObject *) );
                memcpy( python_pars+1 + 22, &PyTuple_GET_ITEM( function->m_defaults, 0 ), function->m_defaults_given * sizeof(PyObject *) );

                for( Py_ssize_t i = 1; i < function->m_args_overall_count; i++ )
                {
                    Py_INCREF( python_pars[ i ] );
                }

                result = function->m_c_code( function, python_pars );
            }
            else
            {
#ifdef _MSC_VER
                PyObject **python_pars = (PyObject **)_alloca( sizeof( PyObject * ) * function->m_args_overall_count );
#else
                PyObject *python_pars[ function->m_args_overall_count ];
#endif
                memset( python_pars, 0, function->m_args_overall_count * sizeof(PyObject *) );

                if ( parseArgumentsMethodPos( function, python_pars, method->m_object, args, 22 ) )
                {
                    result = function->m_c_code( function, python_pars );
                }
                else
                {
                    result = NULL;
                }
            }

            Py_LeaveRecursiveCall();

            return result;
        }
    }
    else if ( PyCFunction_Check( called ) )
    {
        // Try to be fast about wrapping the arguments.
        int flags = PyCFunction_GET_FLAGS( called ) & ~(METH_CLASS | METH_STATIC | METH_COEXIST);

        if ( flags & METH_NOARGS )
        {
#if 22 == 0
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, NULL );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(
                PyExc_TypeError,
                "%s() takes no arguments (22 given)",
                ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_O )
        {
#if 22 == 1
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

            PyObject *result = (*method)( self, args[0] );

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
            // Some buggy C functions do set an error, but do not indicate it
            // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                return NULL;
            }
#else
            PyErr_Format(PyExc_TypeError,
                "%s() takes exactly one argument (22 given)",
                 ((PyCFunctionObject *)called)->m_ml->ml_name
            );
            return NULL;
#endif
        }
        else if ( flags & METH_VARARGS )
        {
            PyCFunction method = PyCFunction_GET_FUNCTION( called );
            PyObject *self = PyCFunction_GET_SELF( called );

            PyObject *pos_args = MAKE_TUPLE( args, 22 );

            PyObject *result;

            // Recursion guard is not strictly necessary, as we already have
            // one on our way to here.
#ifdef _NUITKA_FULL_COMPAT
            if (unlikely( Py_EnterRecursiveCall( (char *)" while calling a Python object" ) ))
            {
                return NULL;
            }
#endif

#if PYTHON_VERSION < 360
            if ( flags & METH_KEYWORDS )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#else
            if ( flags == ( METH_VARARGS | METH_KEYWORDS ) )
            {
                result = (*(PyCFunctionWithKeywords)method)( self, pos_args, NULL );
            }
            else if ( flags == METH_FASTCALL )
            {
#if PYTHON_VERSION < 370
                result = (*(_PyCFunctionFast)method)( self, &PyTuple_GET_ITEM( pos_args, 0 ), 22, NULL );;
#else
                result = (*(_PyCFunctionFast)method)( self, &pos_args, 22 );;
#endif
            }
            else
            {
                result = (*method)( self, pos_args );
            }
#endif

#ifdef _NUITKA_FULL_COMPAT
            Py_LeaveRecursiveCall();
#endif

            if ( result != NULL )
            {
                // Some buggy C functions do set an error, but do not indicate it
                // and Nuitka inner workings can get upset/confused from it.
                DROP_ERROR_OCCURRED();

                Py_DECREF( pos_args );
                return result;
            }
            else
            {
                // Other buggy C functions do this, return NULL, but with
                // no error set, not allowed.
                if (unlikely( !ERROR_OCCURRED() ))
                {
                    PyErr_Format(
                        PyExc_SystemError,
                        "NULL result without error in PyObject_Call"
                    );
                }

                Py_DECREF( pos_args );
                return NULL;
            }
        }
    }
    else if ( PyFunction_Check( called ) )
    {
        return callPythonFunction(
            called,
            args,
            22
        );
    }

    PyObject *pos_args = MAKE_TUPLE( args, 22 );

    PyObject *result = CALL_FUNCTION(
        called,
        pos_args,
        NULL
    );

    Py_DECREF( pos_args );

    return result;
}

PyObject *CALL_METHOD_WITH_ARGS1( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 1; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS1(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS1(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    1
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS1(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS1(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS1( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    1
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS1( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS1( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS1(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS1(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS1(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS2( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 2; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS2(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS2(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    2
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS2(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS2(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS2( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    2
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS2( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS2( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS2(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS2(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS2(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS3( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 3; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS3(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS3(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    3
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS3(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS3(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS3( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    3
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS3( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS3( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS3(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS3(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS3(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS4( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 4; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS4(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS4(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    4
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS4(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS4(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS4( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    4
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS4( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS4( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS4(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS4(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS4(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS5( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 5; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS5(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS5(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    5
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS5(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS5(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS5( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    5
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS5( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS5( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS5(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS5(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS5(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS6( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 6; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS6(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS6(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    6
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS6(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS6(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS6( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    6
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS6( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS6( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS6(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS6(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS6(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS7( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 7; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS7(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS7(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    7
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS7(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS7(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS7( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    7
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS7( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS7( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS7(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS7(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS7(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS8( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 8; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS8(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS8(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    8
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS8(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS8(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS8( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    8
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS8( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS8( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS8(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS8(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS8(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS9( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 9; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS9(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS9(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    9
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS9(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS9(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS9( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    9
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS9( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS9( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS9(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS9(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS9(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS10( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 10; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS10(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS10(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    10
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS10(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS10(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS10( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    10
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS10( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS10( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS10(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS10(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS10(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}

PyObject *CALL_METHOD_WITH_ARGS13( PyObject *source, PyObject *attr_name, PyObject **args )
{
    CHECK_OBJECT( source );
    CHECK_OBJECT( attr_name );

    // Check if arguments are valid objects in debug mode.
#ifndef __NUITKA_NO_ASSERT__
    for( size_t i = 0; i < 13; i++ )
    {
        CHECK_OBJECT( args[ i ] );
    }
#endif

    PyTypeObject *type = Py_TYPE( source );

    if ( type->tp_getattro == PyObject_GenericGetAttr )
    {
        // Unfortunately this is required, although of cause rarely necessary.
        if (unlikely( type->tp_dict == NULL ))
        {
            if (unlikely( PyType_Ready( type ) < 0 ))
            {
                return NULL;
            }
        }

        PyObject *descr = _PyType_Lookup( type, attr_name );
        descrgetfunc func = NULL;

        if ( descr != NULL )
        {
            Py_INCREF( descr );

#if PYTHON_VERSION < 300
            if ( PyType_HasFeature( Py_TYPE( descr ), Py_TPFLAGS_HAVE_CLASS ) )
            {
#endif
                func = Py_TYPE( descr )->tp_descr_get;

                if ( func != NULL && PyDescr_IsData( descr ) )
                {
                    PyObject *called_object = func( descr, source, (PyObject *)type );
                    Py_DECREF( descr );

                    PyObject *result = CALL_FUNCTION_WITH_ARGS13(
                        called_object,
                        args
                    );
                    Py_DECREF( called_object );
                    return result;
                }
#if PYTHON_VERSION < 300
            }
#endif
        }

        Py_ssize_t dictoffset = type->tp_dictoffset;
        PyObject *dict = NULL;

        if ( dictoffset != 0 )
        {
            // Negative dictionary offsets have special meaning.
            if ( dictoffset < 0 )
            {
                Py_ssize_t tsize;
                size_t size;

                tsize = ((PyVarObject *)source)->ob_size;
                if (tsize < 0)
                    tsize = -tsize;
                size = _PyObject_VAR_SIZE( type, tsize );

                dictoffset += (long)size;
            }

            PyObject **dictptr = (PyObject **) ((char *)source + dictoffset);
            dict = *dictptr;
        }

        if ( dict != NULL )
        {
            CHECK_OBJECT( dict );

            Py_INCREF( dict );

            PyObject *called_object = PyDict_GetItem( dict, attr_name );

            if ( called_object != NULL )
            {
                Py_INCREF( called_object );
                Py_XDECREF( descr );
                Py_DECREF( dict );

                PyObject *result = CALL_FUNCTION_WITH_ARGS13(
                    called_object,
                    args
                );
                Py_DECREF( called_object );
                return result;
            }

            Py_DECREF( dict );
        }

        if ( func != NULL )
        {
            if ( func == Nuitka_Function_Type.tp_descr_get )
            {
                PyObject *result = Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)descr,
                    source,
                    args,
                    13
                );

                Py_DECREF( descr );

                return result;
            }
            else
            {
                PyObject *called_object = func( descr, source, (PyObject *)type );
                CHECK_OBJECT( called_object );

                Py_DECREF( descr );

                PyObject *result = CALL_FUNCTION_WITH_ARGS13(
                    called_object,
                    args
                );
                Py_DECREF( called_object );

                return result;
            }
        }

        if ( descr != NULL )
        {
            CHECK_OBJECT( descr );

            PyObject *result = CALL_FUNCTION_WITH_ARGS13(
                descr,
                args
            );
            Py_DECREF( descr );

            return result;
        }

#if PYTHON_VERSION < 300
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            PyString_AS_STRING( attr_name )
        );
#else
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%U'",
            type->tp_name,
            attr_name
        );
#endif
        return NULL;
    }
#if PYTHON_VERSION < 300
    else if ( type == &PyInstance_Type )
    {
        PyInstanceObject *source_instance = (PyInstanceObject *)source;

        // The special cases have their own variant on the code generation level
        // as we are called with constants only.
        assert( attr_name != const_str_plain___dict__ );
        assert( attr_name != const_str_plain___class__ );

        // Try the instance dict first.
        PyObject *called_object = GET_STRING_DICT_VALUE(
            (PyDictObject *)source_instance->in_dict,
            (PyStringObject *)attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            return CALL_FUNCTION_WITH_ARGS13( called_object, args );
        }

        // Then check the class dictionaries.
        called_object = FIND_ATTRIBUTE_IN_CLASS(
            source_instance->in_class,
            attr_name
        );

        // Note: The "called_object" was found without taking a reference,
        // so we need not release it in this branch.
        if ( called_object != NULL )
        {
            descrgetfunc descr_get = Py_TYPE( called_object )->tp_descr_get;

            if ( descr_get == Nuitka_Function_Type.tp_descr_get )
            {
                return Nuitka_CallMethodFunctionPosArgs(
                    (struct Nuitka_FunctionObject const *)called_object,
                    source,
                    args,
                    13
                );
            }
            else if ( descr_get != NULL )
            {
                PyObject *method = descr_get(
                    called_object,
                    source,
                    (PyObject *)source_instance->in_class
                );

                if (unlikely( method == NULL ))
                {
                    return NULL;
                }

                PyObject *result = CALL_FUNCTION_WITH_ARGS13( method, args );
                Py_DECREF( method );
                return result;
            }
            else
            {
                return CALL_FUNCTION_WITH_ARGS13( called_object, args );
            }

        }
        else if (unlikely( source_instance->in_class->cl_getattr == NULL ))
        {
            PyErr_Format(
                PyExc_AttributeError,
                "%s instance has no attribute '%s'",
                PyString_AS_STRING( source_instance->in_class->cl_name ),
                PyString_AS_STRING( attr_name )
            );

            return NULL;
        }
        else
        {
            // Finally allow the "__getattr__" override to provide it or else
            // it's an error.

            PyObject *args2[] = {
                source,
                attr_name
            };

            called_object = CALL_FUNCTION_WITH_ARGS2(
                source_instance->in_class->cl_getattr,
                args2
            );

            if (unlikely( called_object == NULL ))
            {
                return NULL;
            }

            PyObject *result = CALL_FUNCTION_WITH_ARGS13(
                called_object,
                args
            );
            Py_DECREF( called_object );
            return result;
        }
    }
#endif
    else if ( type->tp_getattro != NULL )
    {
        PyObject *called_object = (*type->tp_getattro)(
            source,
            attr_name
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS13(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else if ( type->tp_getattr != NULL )
    {
        PyObject *called_object = (*type->tp_getattr)(
            source,
            (char *)Nuitka_String_AsString_Unchecked( attr_name )
        );

        if (unlikely( called_object == NULL ))
        {
            return NULL;
        }

        PyObject *result = CALL_FUNCTION_WITH_ARGS13(
            called_object,
            args
        );
        Py_DECREF( called_object );
        return result;
    }
    else
    {
        PyErr_Format(
            PyExc_AttributeError,
            "'%s' object has no attribute '%s'",
            type->tp_name,
            Nuitka_String_AsString_Unchecked( attr_name )
        );

        return NULL;
    }
}
/* Code to register embedded modules for meta path based loading if any. */

#include "nuitka/unfreezing.h"

/* Table for lookup to find compiled or bytecode modules included in this
 * binary or module, or put along this binary as extension modules. We do
 * our own loading for each of these.
 */
MOD_ENTRY_DECL(PIL);
MOD_ENTRY_DECL(PIL$BmpImagePlugin);
MOD_ENTRY_DECL(PIL$GifImagePlugin);
MOD_ENTRY_DECL(PIL$GimpGradientFile);
MOD_ENTRY_DECL(PIL$GimpPaletteFile);
MOD_ENTRY_DECL(PIL$Image);
MOD_ENTRY_DECL(PIL$ImageChops);
MOD_ENTRY_DECL(PIL$ImageColor);
MOD_ENTRY_DECL(PIL$ImageFile);
MOD_ENTRY_DECL(PIL$ImageFilter);
MOD_ENTRY_DECL(PIL$ImageMode);
MOD_ENTRY_DECL(PIL$ImagePalette);
MOD_ENTRY_DECL(PIL$ImageQt);
MOD_ENTRY_DECL(PIL$ImageSequence);
MOD_ENTRY_DECL(PIL$ImageShow);
MOD_ENTRY_DECL(PIL$JpegImagePlugin);
MOD_ENTRY_DECL(PIL$JpegPresets);
MOD_ENTRY_DECL(PIL$MpoImagePlugin);
MOD_ENTRY_DECL(PIL$PaletteFile);
MOD_ENTRY_DECL(PIL$PngImagePlugin);
MOD_ENTRY_DECL(PIL$PpmImagePlugin);
MOD_ENTRY_DECL(PIL$PyAccess);
MOD_ENTRY_DECL(PIL$TiffImagePlugin);
MOD_ENTRY_DECL(PIL$TiffTags);
MOD_ENTRY_DECL(PIL$_binary);
MOD_ENTRY_DECL(PIL$_util);
MOD_ENTRY_DECL(PIL$_version);
MOD_INIT_DECL(__main__);
MOD_ENTRY_DECL(appnope);
MOD_ENTRY_DECL(appnope$_dummy);
MOD_ENTRY_DECL(appnope$_nope);
MOD_ENTRY_DECL(attr);
MOD_ENTRY_DECL(attr$_compat);
MOD_ENTRY_DECL(attr$_config);
MOD_ENTRY_DECL(attr$_funcs);
MOD_ENTRY_DECL(attr$_make);
MOD_ENTRY_DECL(attr$converters);
MOD_ENTRY_DECL(attr$exceptions);
MOD_ENTRY_DECL(attr$filters);
MOD_ENTRY_DECL(attr$validators);
MOD_ENTRY_DECL(backcall);
MOD_ENTRY_DECL(backcall$_signatures);
MOD_ENTRY_DECL(backcall$backcall);
MOD_ENTRY_DECL(backports);
MOD_ENTRY_DECL(backports$configparser);
MOD_ENTRY_DECL(backports$configparser$helpers);
MOD_ENTRY_DECL(backports$functools_lru_cache);
MOD_ENTRY_DECL(certifi);
MOD_ENTRY_DECL(certifi$core);
MOD_ENTRY_DECL(cffi);
MOD_ENTRY_DECL(cffi$api);
MOD_ENTRY_DECL(cffi$cffi_opcode);
MOD_ENTRY_DECL(cffi$commontypes);
MOD_ENTRY_DECL(cffi$cparser);
MOD_ENTRY_DECL(cffi$error);
MOD_ENTRY_DECL(cffi$ffiplatform);
MOD_ENTRY_DECL(cffi$lock);
MOD_ENTRY_DECL(cffi$model);
MOD_ENTRY_DECL(cffi$pkgconfig);
MOD_ENTRY_DECL(cffi$recompiler);
MOD_ENTRY_DECL(cffi$vengine_cpy);
MOD_ENTRY_DECL(cffi$vengine_gen);
MOD_ENTRY_DECL(cffi$verifier);
MOD_ENTRY_DECL(chardet);
MOD_ENTRY_DECL(chardet$big5freq);
MOD_ENTRY_DECL(chardet$big5prober);
MOD_ENTRY_DECL(chardet$chardistribution);
MOD_ENTRY_DECL(chardet$charsetgroupprober);
MOD_ENTRY_DECL(chardet$charsetprober);
MOD_ENTRY_DECL(chardet$codingstatemachine);
MOD_ENTRY_DECL(chardet$compat);
MOD_ENTRY_DECL(chardet$cp949prober);
MOD_ENTRY_DECL(chardet$enums);
MOD_ENTRY_DECL(chardet$escprober);
MOD_ENTRY_DECL(chardet$escsm);
MOD_ENTRY_DECL(chardet$eucjpprober);
MOD_ENTRY_DECL(chardet$euckrfreq);
MOD_ENTRY_DECL(chardet$euckrprober);
MOD_ENTRY_DECL(chardet$euctwfreq);
MOD_ENTRY_DECL(chardet$euctwprober);
MOD_ENTRY_DECL(chardet$gb2312freq);
MOD_ENTRY_DECL(chardet$gb2312prober);
MOD_ENTRY_DECL(chardet$hebrewprober);
MOD_ENTRY_DECL(chardet$jisfreq);
MOD_ENTRY_DECL(chardet$jpcntx);
MOD_ENTRY_DECL(chardet$langbulgarianmodel);
MOD_ENTRY_DECL(chardet$langcyrillicmodel);
MOD_ENTRY_DECL(chardet$langgreekmodel);
MOD_ENTRY_DECL(chardet$langhebrewmodel);
MOD_ENTRY_DECL(chardet$langthaimodel);
MOD_ENTRY_DECL(chardet$langturkishmodel);
MOD_ENTRY_DECL(chardet$latin1prober);
MOD_ENTRY_DECL(chardet$mbcharsetprober);
MOD_ENTRY_DECL(chardet$mbcsgroupprober);
MOD_ENTRY_DECL(chardet$mbcssm);
MOD_ENTRY_DECL(chardet$sbcharsetprober);
MOD_ENTRY_DECL(chardet$sbcsgroupprober);
MOD_ENTRY_DECL(chardet$sjisprober);
MOD_ENTRY_DECL(chardet$universaldetector);
MOD_ENTRY_DECL(chardet$utf8prober);
MOD_ENTRY_DECL(chardet$version);
MOD_ENTRY_DECL(cloudpickle);
MOD_ENTRY_DECL(cloudpickle$cloudpickle);
MOD_ENTRY_DECL(cloudpickle$cloudpickle_fast);
MOD_ENTRY_DECL(colorama);
MOD_ENTRY_DECL(colorama$ansi);
MOD_ENTRY_DECL(colorama$ansitowin32);
MOD_ENTRY_DECL(colorama$initialise);
MOD_ENTRY_DECL(colorama$win32);
MOD_ENTRY_DECL(colorama$winterm);
MOD_ENTRY_DECL(crf);
MOD_ENTRY_DECL(cycler);
MOD_ENTRY_DECL(data);
MOD_ENTRY_DECL(dateutil);
MOD_ENTRY_DECL(dateutil$_common);
MOD_ENTRY_DECL(dateutil$_version);
MOD_ENTRY_DECL(dateutil$easter);
MOD_ENTRY_DECL(dateutil$parser);
MOD_ENTRY_DECL(dateutil$parser$_parser);
MOD_ENTRY_DECL(dateutil$parser$isoparser);
MOD_ENTRY_DECL(dateutil$relativedelta);
MOD_ENTRY_DECL(dateutil$rrule);
MOD_ENTRY_DECL(dateutil$tz);
MOD_ENTRY_DECL(dateutil$tz$_common);
MOD_ENTRY_DECL(dateutil$tz$_factories);
MOD_ENTRY_DECL(dateutil$tz$tz);
MOD_ENTRY_DECL(dateutil$tz$win);
MOD_ENTRY_DECL(dateutil$zoneinfo);
MOD_ENTRY_DECL(dawg_python);
MOD_ENTRY_DECL(dawg_python$compat);
MOD_ENTRY_DECL(dawg_python$dawgs);
MOD_ENTRY_DECL(dawg_python$units);
MOD_ENTRY_DECL(dawg_python$wrapper);
MOD_ENTRY_DECL(decorator);
MOD_ENTRY_DECL(defusedxml);
MOD_ENTRY_DECL(defusedxml$ElementTree);
MOD_ENTRY_DECL(defusedxml$cElementTree);
MOD_ENTRY_DECL(defusedxml$common);
MOD_ENTRY_DECL(defusedxml$expatbuilder);
MOD_ENTRY_DECL(defusedxml$expatreader);
MOD_ENTRY_DECL(defusedxml$minidom);
MOD_ENTRY_DECL(defusedxml$pulldom);
MOD_ENTRY_DECL(defusedxml$sax);
MOD_ENTRY_DECL(defusedxml$xmlrpc);
MOD_ENTRY_DECL(dsl);
MOD_ENTRY_DECL(entrypoints);
MOD_ENTRY_DECL(extractors);
MOD_ENTRY_DECL(grammars);
MOD_ENTRY_DECL(grammars$address);
MOD_ENTRY_DECL(grammars$date);
MOD_ENTRY_DECL(grammars$location);
MOD_ENTRY_DECL(grammars$money);
MOD_ENTRY_DECL(grammars$name);
MOD_ENTRY_DECL(grammars$organisation);
MOD_ENTRY_DECL(grammars$person);
MOD_ENTRY_DECL(idna);
MOD_ENTRY_DECL(idna$core);
MOD_ENTRY_DECL(idna$idnadata);
MOD_ENTRY_DECL(idna$intranges);
MOD_ENTRY_DECL(idna$package_data);
MOD_ENTRY_DECL(idna$uts46data);
MOD_ENTRY_DECL(ipykernel);
MOD_ENTRY_DECL(ipykernel$_eventloop_macos);
MOD_ENTRY_DECL(ipykernel$_version);
MOD_ENTRY_DECL(ipykernel$codeutil);
MOD_ENTRY_DECL(ipykernel$comm);
MOD_ENTRY_DECL(ipykernel$comm$comm);
MOD_ENTRY_DECL(ipykernel$comm$manager);
MOD_ENTRY_DECL(ipykernel$connect);
MOD_ENTRY_DECL(ipykernel$displayhook);
MOD_ENTRY_DECL(ipykernel$embed);
MOD_ENTRY_DECL(ipykernel$eventloops);
MOD_ENTRY_DECL(ipykernel$gui);
MOD_ENTRY_DECL(ipykernel$gui$gtk3embed);
MOD_ENTRY_DECL(ipykernel$gui$gtkembed);
MOD_ENTRY_DECL(ipykernel$heartbeat);
MOD_ENTRY_DECL(ipykernel$inprocess);
MOD_ENTRY_DECL(ipykernel$inprocess$blocking);
MOD_ENTRY_DECL(ipykernel$inprocess$channels);
MOD_ENTRY_DECL(ipykernel$inprocess$client);
MOD_ENTRY_DECL(ipykernel$inprocess$constants);
MOD_ENTRY_DECL(ipykernel$inprocess$ipkernel);
MOD_ENTRY_DECL(ipykernel$inprocess$manager);
MOD_ENTRY_DECL(ipykernel$inprocess$socket);
MOD_ENTRY_DECL(ipykernel$iostream);
MOD_ENTRY_DECL(ipykernel$ipkernel);
MOD_ENTRY_DECL(ipykernel$jsonutil);
MOD_ENTRY_DECL(ipykernel$kernelapp);
MOD_ENTRY_DECL(ipykernel$kernelbase);
MOD_ENTRY_DECL(ipykernel$kernelspec);
MOD_ENTRY_DECL(ipykernel$parentpoller);
MOD_ENTRY_DECL(ipykernel$pickleutil);
MOD_ENTRY_DECL(ipykernel$pylab);
MOD_ENTRY_DECL(ipykernel$pylab$backend_inline);
MOD_ENTRY_DECL(ipykernel$pylab$config);
MOD_ENTRY_DECL(ipykernel$serialize);
MOD_ENTRY_DECL(ipykernel$zmqshell);
MOD_ENTRY_DECL(ipython_genutils);
MOD_ENTRY_DECL(ipython_genutils$_version);
MOD_ENTRY_DECL(ipython_genutils$encoding);
MOD_ENTRY_DECL(ipython_genutils$importstring);
MOD_ENTRY_DECL(ipython_genutils$ipstruct);
MOD_ENTRY_DECL(ipython_genutils$path);
MOD_ENTRY_DECL(ipython_genutils$py3compat);
MOD_ENTRY_DECL(ipython_genutils$tempdir);
MOD_ENTRY_DECL(ipython_genutils$text);
MOD_ENTRY_DECL(jedi);
MOD_ENTRY_DECL(jedi$_compatibility);
MOD_ENTRY_DECL(jedi$api);
MOD_ENTRY_DECL(jedi$api$classes);
MOD_ENTRY_DECL(jedi$api$completion);
MOD_ENTRY_DECL(jedi$api$environment);
MOD_ENTRY_DECL(jedi$api$exceptions);
MOD_ENTRY_DECL(jedi$api$helpers);
MOD_ENTRY_DECL(jedi$api$interpreter);
MOD_ENTRY_DECL(jedi$api$keywords);
MOD_ENTRY_DECL(jedi$api$project);
MOD_ENTRY_DECL(jedi$cache);
MOD_ENTRY_DECL(jedi$common);
MOD_ENTRY_DECL(jedi$common$context);
MOD_ENTRY_DECL(jedi$common$utils);
MOD_ENTRY_DECL(jedi$debug);
MOD_ENTRY_DECL(jedi$evaluate);
MOD_ENTRY_DECL(jedi$evaluate$analysis);
MOD_ENTRY_DECL(jedi$evaluate$arguments);
MOD_ENTRY_DECL(jedi$evaluate$base_context);
MOD_ENTRY_DECL(jedi$evaluate$cache);
MOD_ENTRY_DECL(jedi$evaluate$compiled);
MOD_ENTRY_DECL(jedi$evaluate$compiled$access);
MOD_ENTRY_DECL(jedi$evaluate$compiled$context);
MOD_ENTRY_DECL(jedi$evaluate$compiled$fake);
MOD_ENTRY_DECL(jedi$evaluate$compiled$getattr_static);
MOD_ENTRY_DECL(jedi$evaluate$compiled$mixed);
MOD_ENTRY_DECL(jedi$evaluate$compiled$subprocess);
MOD_ENTRY_DECL(jedi$evaluate$compiled$subprocess$functions);
MOD_ENTRY_DECL(jedi$evaluate$context);
MOD_ENTRY_DECL(jedi$evaluate$context$asynchronous);
MOD_ENTRY_DECL(jedi$evaluate$context$function);
MOD_ENTRY_DECL(jedi$evaluate$context$instance);
MOD_ENTRY_DECL(jedi$evaluate$context$iterable);
MOD_ENTRY_DECL(jedi$evaluate$context$klass);
MOD_ENTRY_DECL(jedi$evaluate$context$module);
MOD_ENTRY_DECL(jedi$evaluate$context$namespace);
MOD_ENTRY_DECL(jedi$evaluate$docstrings);
MOD_ENTRY_DECL(jedi$evaluate$dynamic);
MOD_ENTRY_DECL(jedi$evaluate$filters);
MOD_ENTRY_DECL(jedi$evaluate$finder);
MOD_ENTRY_DECL(jedi$evaluate$flow_analysis);
MOD_ENTRY_DECL(jedi$evaluate$helpers);
MOD_ENTRY_DECL(jedi$evaluate$imports);
MOD_ENTRY_DECL(jedi$evaluate$lazy_context);
MOD_ENTRY_DECL(jedi$evaluate$param);
MOD_ENTRY_DECL(jedi$evaluate$parser_cache);
MOD_ENTRY_DECL(jedi$evaluate$pep0484);
MOD_ENTRY_DECL(jedi$evaluate$recursion);
MOD_ENTRY_DECL(jedi$evaluate$stdlib);
MOD_ENTRY_DECL(jedi$evaluate$syntax_tree);
MOD_ENTRY_DECL(jedi$evaluate$sys_path);
MOD_ENTRY_DECL(jedi$evaluate$usages);
MOD_ENTRY_DECL(jedi$evaluate$utils);
MOD_ENTRY_DECL(jedi$parser_utils);
MOD_ENTRY_DECL(jedi$settings);
MOD_ENTRY_DECL(jinja2);
MOD_ENTRY_DECL(jinja2$_compat);
MOD_ENTRY_DECL(jinja2$_identifier);
MOD_ENTRY_DECL(jinja2$asyncfilters);
MOD_ENTRY_DECL(jinja2$asyncsupport);
MOD_ENTRY_DECL(jinja2$bccache);
MOD_ENTRY_DECL(jinja2$compiler);
MOD_ENTRY_DECL(jinja2$constants);
MOD_ENTRY_DECL(jinja2$debug);
MOD_ENTRY_DECL(jinja2$defaults);
MOD_ENTRY_DECL(jinja2$environment);
MOD_ENTRY_DECL(jinja2$exceptions);
MOD_ENTRY_DECL(jinja2$filters);
MOD_ENTRY_DECL(jinja2$idtracking);
MOD_ENTRY_DECL(jinja2$lexer);
MOD_ENTRY_DECL(jinja2$loaders);
MOD_ENTRY_DECL(jinja2$nodes);
MOD_ENTRY_DECL(jinja2$optimizer);
MOD_ENTRY_DECL(jinja2$parser);
MOD_ENTRY_DECL(jinja2$runtime);
MOD_ENTRY_DECL(jinja2$tests);
MOD_ENTRY_DECL(jinja2$utils);
MOD_ENTRY_DECL(jinja2$visitor);
MOD_ENTRY_DECL(jsonschema);
MOD_ENTRY_DECL(jsonschema$_format);
MOD_ENTRY_DECL(jsonschema$_legacy_validators);
MOD_ENTRY_DECL(jsonschema$_types);
MOD_ENTRY_DECL(jsonschema$_utils);
MOD_ENTRY_DECL(jsonschema$_validators);
MOD_ENTRY_DECL(jsonschema$compat);
MOD_ENTRY_DECL(jsonschema$exceptions);
MOD_ENTRY_DECL(jsonschema$validators);
MOD_ENTRY_DECL(jupyter_client);
MOD_ENTRY_DECL(jupyter_client$_version);
MOD_ENTRY_DECL(jupyter_client$adapter);
MOD_ENTRY_DECL(jupyter_client$blocking);
MOD_ENTRY_DECL(jupyter_client$blocking$channels);
MOD_ENTRY_DECL(jupyter_client$blocking$client);
MOD_ENTRY_DECL(jupyter_client$channels);
MOD_ENTRY_DECL(jupyter_client$channelsabc);
MOD_ENTRY_DECL(jupyter_client$client);
MOD_ENTRY_DECL(jupyter_client$clientabc);
MOD_ENTRY_DECL(jupyter_client$connect);
MOD_ENTRY_DECL(jupyter_client$ioloop);
MOD_ENTRY_DECL(jupyter_client$ioloop$manager);
MOD_ENTRY_DECL(jupyter_client$ioloop$restarter);
MOD_ENTRY_DECL(jupyter_client$jsonutil);
MOD_ENTRY_DECL(jupyter_client$kernelspec);
MOD_ENTRY_DECL(jupyter_client$launcher);
MOD_ENTRY_DECL(jupyter_client$localinterfaces);
MOD_ENTRY_DECL(jupyter_client$manager);
MOD_ENTRY_DECL(jupyter_client$managerabc);
MOD_ENTRY_DECL(jupyter_client$multikernelmanager);
MOD_ENTRY_DECL(jupyter_client$restarter);
MOD_ENTRY_DECL(jupyter_client$session);
MOD_ENTRY_DECL(jupyter_client$win_interrupt);
MOD_ENTRY_DECL(jupyter_core);
MOD_ENTRY_DECL(jupyter_core$application);
MOD_ENTRY_DECL(jupyter_core$migrate);
MOD_ENTRY_DECL(jupyter_core$paths);
MOD_ENTRY_DECL(jupyter_core$utils);
MOD_ENTRY_DECL(jupyter_core$utils$shutil_which);
MOD_ENTRY_DECL(jupyter_core$version);
MOD_ENTRY_DECL(lxml);
MOD_ENTRY_DECL(markup);
MOD_ENTRY_DECL(markupsafe);
MOD_ENTRY_DECL(markupsafe$_compat);
MOD_ENTRY_DECL(markupsafe$_constants);
MOD_ENTRY_DECL(markupsafe$_native);
MOD_ENTRY_DECL(matplotlib);
MOD_ENTRY_DECL(matplotlib$_cm);
MOD_ENTRY_DECL(matplotlib$_cm_listed);
MOD_ENTRY_DECL(matplotlib$_color_data);
MOD_ENTRY_DECL(matplotlib$_constrained_layout);
MOD_ENTRY_DECL(matplotlib$_layoutbox);
MOD_ENTRY_DECL(matplotlib$_mathtext_data);
MOD_ENTRY_DECL(matplotlib$_pylab_helpers);
MOD_ENTRY_DECL(matplotlib$_version);
MOD_ENTRY_DECL(matplotlib$afm);
MOD_ENTRY_DECL(matplotlib$artist);
MOD_ENTRY_DECL(matplotlib$axes);
MOD_ENTRY_DECL(matplotlib$axes$_axes);
MOD_ENTRY_DECL(matplotlib$axes$_base);
MOD_ENTRY_DECL(matplotlib$axes$_secondary_axes);
MOD_ENTRY_DECL(matplotlib$axes$_subplots);
MOD_ENTRY_DECL(matplotlib$axis);
MOD_ENTRY_DECL(matplotlib$backend_bases);
MOD_ENTRY_DECL(matplotlib$backend_managers);
MOD_ENTRY_DECL(matplotlib$backend_tools);
MOD_ENTRY_DECL(matplotlib$backends);
MOD_ENTRY_DECL(matplotlib$backends$_backend_tk);
MOD_ENTRY_DECL(matplotlib$backends$backend_agg);
MOD_ENTRY_DECL(matplotlib$backends$backend_tkagg);
MOD_ENTRY_DECL(matplotlib$backends$backend_webagg);
MOD_ENTRY_DECL(matplotlib$backends$backend_webagg_core);
MOD_ENTRY_DECL(matplotlib$bezier);
MOD_ENTRY_DECL(matplotlib$blocking_input);
MOD_ENTRY_DECL(matplotlib$category);
MOD_ENTRY_DECL(matplotlib$cbook);
MOD_ENTRY_DECL(matplotlib$cbook$deprecation);
MOD_ENTRY_DECL(matplotlib$cm);
MOD_ENTRY_DECL(matplotlib$collections);
MOD_ENTRY_DECL(matplotlib$colorbar);
MOD_ENTRY_DECL(matplotlib$colors);
MOD_ENTRY_DECL(matplotlib$container);
MOD_ENTRY_DECL(matplotlib$contour);
MOD_ENTRY_DECL(matplotlib$dates);
MOD_ENTRY_DECL(matplotlib$docstring);
MOD_ENTRY_DECL(matplotlib$dviread);
MOD_ENTRY_DECL(matplotlib$figure);
MOD_ENTRY_DECL(matplotlib$font_manager);
MOD_ENTRY_DECL(matplotlib$fontconfig_pattern);
MOD_ENTRY_DECL(matplotlib$gridspec);
MOD_ENTRY_DECL(matplotlib$hatch);
MOD_ENTRY_DECL(matplotlib$image);
MOD_ENTRY_DECL(matplotlib$legend);
MOD_ENTRY_DECL(matplotlib$legend_handler);
MOD_ENTRY_DECL(matplotlib$lines);
MOD_ENTRY_DECL(matplotlib$markers);
MOD_ENTRY_DECL(matplotlib$mathtext);
MOD_ENTRY_DECL(matplotlib$mlab);
MOD_ENTRY_DECL(matplotlib$offsetbox);
MOD_ENTRY_DECL(matplotlib$patches);
MOD_ENTRY_DECL(matplotlib$path);
MOD_ENTRY_DECL(matplotlib$patheffects);
MOD_ENTRY_DECL(matplotlib$projections);
MOD_ENTRY_DECL(matplotlib$projections$geo);
MOD_ENTRY_DECL(matplotlib$projections$polar);
MOD_ENTRY_DECL(matplotlib$pyplot);
MOD_ENTRY_DECL(matplotlib$quiver);
MOD_ENTRY_DECL(matplotlib$rcsetup);
MOD_ENTRY_DECL(matplotlib$scale);
MOD_ENTRY_DECL(matplotlib$spines);
MOD_ENTRY_DECL(matplotlib$stackplot);
MOD_ENTRY_DECL(matplotlib$streamplot);
MOD_ENTRY_DECL(matplotlib$style);
MOD_ENTRY_DECL(matplotlib$style$core);
MOD_ENTRY_DECL(matplotlib$table);
MOD_ENTRY_DECL(matplotlib$texmanager);
MOD_ENTRY_DECL(matplotlib$text);
MOD_ENTRY_DECL(matplotlib$textpath);
MOD_ENTRY_DECL(matplotlib$ticker);
MOD_ENTRY_DECL(matplotlib$tight_bbox);
MOD_ENTRY_DECL(matplotlib$tight_layout);
MOD_ENTRY_DECL(matplotlib$transforms);
MOD_ENTRY_DECL(matplotlib$tri);
MOD_ENTRY_DECL(matplotlib$tri$triangulation);
MOD_ENTRY_DECL(matplotlib$tri$tricontour);
MOD_ENTRY_DECL(matplotlib$tri$trifinder);
MOD_ENTRY_DECL(matplotlib$tri$triinterpolate);
MOD_ENTRY_DECL(matplotlib$tri$tripcolor);
MOD_ENTRY_DECL(matplotlib$tri$triplot);
MOD_ENTRY_DECL(matplotlib$tri$trirefine);
MOD_ENTRY_DECL(matplotlib$tri$tritools);
MOD_ENTRY_DECL(matplotlib$units);
MOD_ENTRY_DECL(matplotlib$widgets);
MOD_ENTRY_DECL(mistune);
MOD_ENTRY_DECL(nbconvert);
MOD_ENTRY_DECL(nbconvert$_version);
MOD_ENTRY_DECL(nbconvert$exporters);
MOD_ENTRY_DECL(nbconvert$exporters$asciidoc);
MOD_ENTRY_DECL(nbconvert$exporters$base);
MOD_ENTRY_DECL(nbconvert$exporters$exporter);
MOD_ENTRY_DECL(nbconvert$exporters$html);
MOD_ENTRY_DECL(nbconvert$exporters$latex);
MOD_ENTRY_DECL(nbconvert$exporters$markdown);
MOD_ENTRY_DECL(nbconvert$exporters$notebook);
MOD_ENTRY_DECL(nbconvert$exporters$pdf);
MOD_ENTRY_DECL(nbconvert$exporters$python);
MOD_ENTRY_DECL(nbconvert$exporters$rst);
MOD_ENTRY_DECL(nbconvert$exporters$script);
MOD_ENTRY_DECL(nbconvert$exporters$slides);
MOD_ENTRY_DECL(nbconvert$exporters$templateexporter);
MOD_ENTRY_DECL(nbconvert$filters);
MOD_ENTRY_DECL(nbconvert$filters$ansi);
MOD_ENTRY_DECL(nbconvert$filters$citation);
MOD_ENTRY_DECL(nbconvert$filters$datatypefilter);
MOD_ENTRY_DECL(nbconvert$filters$filter_links);
MOD_ENTRY_DECL(nbconvert$filters$highlight);
MOD_ENTRY_DECL(nbconvert$filters$latex);
MOD_ENTRY_DECL(nbconvert$filters$markdown);
MOD_ENTRY_DECL(nbconvert$filters$markdown_mistune);
MOD_ENTRY_DECL(nbconvert$filters$metadata);
MOD_ENTRY_DECL(nbconvert$filters$pandoc);
MOD_ENTRY_DECL(nbconvert$filters$strings);
MOD_ENTRY_DECL(nbconvert$postprocessors);
MOD_ENTRY_DECL(nbconvert$postprocessors$base);
MOD_ENTRY_DECL(nbconvert$postprocessors$serve);
MOD_ENTRY_DECL(nbconvert$preprocessors);
MOD_ENTRY_DECL(nbconvert$preprocessors$base);
MOD_ENTRY_DECL(nbconvert$preprocessors$clearmetadata);
MOD_ENTRY_DECL(nbconvert$preprocessors$clearoutput);
MOD_ENTRY_DECL(nbconvert$preprocessors$coalescestreams);
MOD_ENTRY_DECL(nbconvert$preprocessors$convertfigures);
MOD_ENTRY_DECL(nbconvert$preprocessors$csshtmlheader);
MOD_ENTRY_DECL(nbconvert$preprocessors$execute);
MOD_ENTRY_DECL(nbconvert$preprocessors$extractoutput);
MOD_ENTRY_DECL(nbconvert$preprocessors$highlightmagics);
MOD_ENTRY_DECL(nbconvert$preprocessors$latex);
MOD_ENTRY_DECL(nbconvert$preprocessors$regexremove);
MOD_ENTRY_DECL(nbconvert$preprocessors$svg2pdf);
MOD_ENTRY_DECL(nbconvert$preprocessors$tagremove);
MOD_ENTRY_DECL(nbconvert$resources);
MOD_ENTRY_DECL(nbconvert$utils);
MOD_ENTRY_DECL(nbconvert$utils$base);
MOD_ENTRY_DECL(nbconvert$utils$exceptions);
MOD_ENTRY_DECL(nbconvert$utils$io);
MOD_ENTRY_DECL(nbconvert$utils$pandoc);
MOD_ENTRY_DECL(nbconvert$utils$version);
MOD_ENTRY_DECL(nbconvert$writers);
MOD_ENTRY_DECL(nbconvert$writers$base);
MOD_ENTRY_DECL(nbconvert$writers$debug);
MOD_ENTRY_DECL(nbconvert$writers$files);
MOD_ENTRY_DECL(nbconvert$writers$stdout);
MOD_ENTRY_DECL(nbformat);
MOD_ENTRY_DECL(nbformat$_version);
MOD_ENTRY_DECL(nbformat$converter);
MOD_ENTRY_DECL(nbformat$notebooknode);
MOD_ENTRY_DECL(nbformat$reader);
MOD_ENTRY_DECL(nbformat$sentinel);
MOD_ENTRY_DECL(nbformat$v1);
MOD_ENTRY_DECL(nbformat$v1$convert);
MOD_ENTRY_DECL(nbformat$v1$nbbase);
MOD_ENTRY_DECL(nbformat$v1$nbjson);
MOD_ENTRY_DECL(nbformat$v1$rwbase);
MOD_ENTRY_DECL(nbformat$v2);
MOD_ENTRY_DECL(nbformat$v2$convert);
MOD_ENTRY_DECL(nbformat$v2$nbbase);
MOD_ENTRY_DECL(nbformat$v2$nbjson);
MOD_ENTRY_DECL(nbformat$v2$nbpy);
MOD_ENTRY_DECL(nbformat$v2$nbxml);
MOD_ENTRY_DECL(nbformat$v2$rwbase);
MOD_ENTRY_DECL(nbformat$v3);
MOD_ENTRY_DECL(nbformat$v3$convert);
MOD_ENTRY_DECL(nbformat$v3$nbbase);
MOD_ENTRY_DECL(nbformat$v3$nbjson);
MOD_ENTRY_DECL(nbformat$v3$nbpy);
MOD_ENTRY_DECL(nbformat$v3$rwbase);
MOD_ENTRY_DECL(nbformat$v4);
MOD_ENTRY_DECL(nbformat$v4$convert);
MOD_ENTRY_DECL(nbformat$v4$nbbase);
MOD_ENTRY_DECL(nbformat$v4$nbjson);
MOD_ENTRY_DECL(nbformat$v4$rwbase);
MOD_ENTRY_DECL(nbformat$validator);
MOD_ENTRY_DECL(notebook);
MOD_ENTRY_DECL(notebook$_version);
MOD_ENTRY_DECL(notebook$config_manager);
MOD_ENTRY_DECL(notebook$extensions);
MOD_ENTRY_DECL(notebook$nbextensions);
MOD_ENTRY_DECL(notebook$services);
MOD_ENTRY_DECL(notebook$services$contents);
MOD_ENTRY_DECL(notebook$services$contents$fileio);
MOD_ENTRY_DECL(notebook$utils);
MOD_ENTRY_DECL(numpy);
MOD_ENTRY_DECL(numpy$__config__);
MOD_ENTRY_DECL(numpy$_distributor_init);
MOD_ENTRY_DECL(numpy$_globals);
MOD_ENTRY_DECL(numpy$_pytesttester);
MOD_ENTRY_DECL(numpy$compat);
MOD_ENTRY_DECL(numpy$compat$_inspect);
MOD_ENTRY_DECL(numpy$compat$py3k);
MOD_ENTRY_DECL(numpy$core);
MOD_ENTRY_DECL(numpy$core$_add_newdocs);
MOD_ENTRY_DECL(numpy$core$_asarray);
MOD_ENTRY_DECL(numpy$core$_dtype);
MOD_ENTRY_DECL(numpy$core$_dtype_ctypes);
MOD_ENTRY_DECL(numpy$core$_exceptions);
MOD_ENTRY_DECL(numpy$core$_internal);
MOD_ENTRY_DECL(numpy$core$_methods);
MOD_ENTRY_DECL(numpy$core$_string_helpers);
MOD_ENTRY_DECL(numpy$core$_type_aliases);
MOD_ENTRY_DECL(numpy$core$_ufunc_config);
MOD_ENTRY_DECL(numpy$core$arrayprint);
MOD_ENTRY_DECL(numpy$core$defchararray);
MOD_ENTRY_DECL(numpy$core$einsumfunc);
MOD_ENTRY_DECL(numpy$core$fromnumeric);
MOD_ENTRY_DECL(numpy$core$function_base);
MOD_ENTRY_DECL(numpy$core$getlimits);
MOD_ENTRY_DECL(numpy$core$info);
MOD_ENTRY_DECL(numpy$core$machar);
MOD_ENTRY_DECL(numpy$core$memmap);
MOD_ENTRY_DECL(numpy$core$multiarray);
MOD_ENTRY_DECL(numpy$core$numeric);
MOD_ENTRY_DECL(numpy$core$numerictypes);
MOD_ENTRY_DECL(numpy$core$overrides);
MOD_ENTRY_DECL(numpy$core$records);
MOD_ENTRY_DECL(numpy$core$shape_base);
MOD_ENTRY_DECL(numpy$core$umath);
MOD_ENTRY_DECL(numpy$ctypeslib);
MOD_ENTRY_DECL(numpy$dual);
MOD_ENTRY_DECL(numpy$fft);
MOD_ENTRY_DECL(numpy$fft$_pocketfft);
MOD_ENTRY_DECL(numpy$fft$helper);
MOD_ENTRY_DECL(numpy$lib);
MOD_ENTRY_DECL(numpy$lib$_datasource);
MOD_ENTRY_DECL(numpy$lib$_iotools);
MOD_ENTRY_DECL(numpy$lib$_version);
MOD_ENTRY_DECL(numpy$lib$arraypad);
MOD_ENTRY_DECL(numpy$lib$arraysetops);
MOD_ENTRY_DECL(numpy$lib$arrayterator);
MOD_ENTRY_DECL(numpy$lib$financial);
MOD_ENTRY_DECL(numpy$lib$format);
MOD_ENTRY_DECL(numpy$lib$function_base);
MOD_ENTRY_DECL(numpy$lib$histograms);
MOD_ENTRY_DECL(numpy$lib$index_tricks);
MOD_ENTRY_DECL(numpy$lib$info);
MOD_ENTRY_DECL(numpy$lib$mixins);
MOD_ENTRY_DECL(numpy$lib$nanfunctions);
MOD_ENTRY_DECL(numpy$lib$npyio);
MOD_ENTRY_DECL(numpy$lib$polynomial);
MOD_ENTRY_DECL(numpy$lib$scimath);
MOD_ENTRY_DECL(numpy$lib$shape_base);
MOD_ENTRY_DECL(numpy$lib$stride_tricks);
MOD_ENTRY_DECL(numpy$lib$twodim_base);
MOD_ENTRY_DECL(numpy$lib$type_check);
MOD_ENTRY_DECL(numpy$lib$ufunclike);
MOD_ENTRY_DECL(numpy$lib$utils);
MOD_ENTRY_DECL(numpy$linalg);
MOD_ENTRY_DECL(numpy$linalg$info);
MOD_ENTRY_DECL(numpy$linalg$linalg);
MOD_ENTRY_DECL(numpy$ma);
MOD_ENTRY_DECL(numpy$ma$core);
MOD_ENTRY_DECL(numpy$ma$extras);
MOD_ENTRY_DECL(numpy$ma$mrecords);
MOD_ENTRY_DECL(numpy$matrixlib);
MOD_ENTRY_DECL(numpy$matrixlib$defmatrix);
MOD_ENTRY_DECL(numpy$polynomial);
MOD_ENTRY_DECL(numpy$polynomial$_polybase);
MOD_ENTRY_DECL(numpy$polynomial$chebyshev);
MOD_ENTRY_DECL(numpy$polynomial$hermite);
MOD_ENTRY_DECL(numpy$polynomial$hermite_e);
MOD_ENTRY_DECL(numpy$polynomial$laguerre);
MOD_ENTRY_DECL(numpy$polynomial$legendre);
MOD_ENTRY_DECL(numpy$polynomial$polynomial);
MOD_ENTRY_DECL(numpy$polynomial$polyutils);
MOD_ENTRY_DECL(numpy$random);
MOD_ENTRY_DECL(numpy$random$_pickle);
MOD_ENTRY_DECL(numpy$version);
MOD_ENTRY_DECL(packaging);
MOD_ENTRY_DECL(packaging$__about__);
MOD_ENTRY_DECL(pandocfilters);
MOD_ENTRY_DECL(parso);
MOD_ENTRY_DECL(parso$_compatibility);
MOD_ENTRY_DECL(parso$cache);
MOD_ENTRY_DECL(parso$file_io);
MOD_ENTRY_DECL(parso$grammar);
MOD_ENTRY_DECL(parso$normalizer);
MOD_ENTRY_DECL(parso$parser);
MOD_ENTRY_DECL(parso$pgen2);
MOD_ENTRY_DECL(parso$pgen2$generator);
MOD_ENTRY_DECL(parso$pgen2$grammar_parser);
MOD_ENTRY_DECL(parso$python);
MOD_ENTRY_DECL(parso$python$diff);
MOD_ENTRY_DECL(parso$python$errors);
MOD_ENTRY_DECL(parso$python$parser);
MOD_ENTRY_DECL(parso$python$pep8);
MOD_ENTRY_DECL(parso$python$prefix);
MOD_ENTRY_DECL(parso$python$token);
MOD_ENTRY_DECL(parso$python$tokenize);
MOD_ENTRY_DECL(parso$python$tree);
MOD_ENTRY_DECL(parso$tree);
MOD_ENTRY_DECL(parso$utils);
MOD_ENTRY_DECL(pickleshare);
MOD_ENTRY_DECL(preprocess);
MOD_ENTRY_DECL(prompt_toolkit);
MOD_ENTRY_DECL(prompt_toolkit$application);
MOD_ENTRY_DECL(prompt_toolkit$application$application);
MOD_ENTRY_DECL(prompt_toolkit$application$current);
MOD_ENTRY_DECL(prompt_toolkit$application$dummy);
MOD_ENTRY_DECL(prompt_toolkit$application$run_in_terminal);
MOD_ENTRY_DECL(prompt_toolkit$auto_suggest);
MOD_ENTRY_DECL(prompt_toolkit$buffer);
MOD_ENTRY_DECL(prompt_toolkit$cache);
MOD_ENTRY_DECL(prompt_toolkit$clipboard);
MOD_ENTRY_DECL(prompt_toolkit$clipboard$base);
MOD_ENTRY_DECL(prompt_toolkit$clipboard$in_memory);
MOD_ENTRY_DECL(prompt_toolkit$completion);
MOD_ENTRY_DECL(prompt_toolkit$completion$base);
MOD_ENTRY_DECL(prompt_toolkit$completion$filesystem);
MOD_ENTRY_DECL(prompt_toolkit$completion$fuzzy_completer);
MOD_ENTRY_DECL(prompt_toolkit$completion$word_completer);
MOD_ENTRY_DECL(prompt_toolkit$document);
MOD_ENTRY_DECL(prompt_toolkit$enums);
MOD_ENTRY_DECL(prompt_toolkit$eventloop);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$async_generator);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$asyncio_posix);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$asyncio_win32);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$base);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$context);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$coroutine);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$defaults);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$event);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$future);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$inputhook);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$posix);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$select);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$utils);
MOD_ENTRY_DECL(prompt_toolkit$eventloop$win32);
MOD_ENTRY_DECL(prompt_toolkit$filters);
MOD_ENTRY_DECL(prompt_toolkit$filters$app);
MOD_ENTRY_DECL(prompt_toolkit$filters$base);
MOD_ENTRY_DECL(prompt_toolkit$filters$cli);
MOD_ENTRY_DECL(prompt_toolkit$filters$utils);
MOD_ENTRY_DECL(prompt_toolkit$formatted_text);
MOD_ENTRY_DECL(prompt_toolkit$formatted_text$ansi);
MOD_ENTRY_DECL(prompt_toolkit$formatted_text$base);
MOD_ENTRY_DECL(prompt_toolkit$formatted_text$html);
MOD_ENTRY_DECL(prompt_toolkit$formatted_text$pygments);
MOD_ENTRY_DECL(prompt_toolkit$formatted_text$utils);
MOD_ENTRY_DECL(prompt_toolkit$history);
MOD_ENTRY_DECL(prompt_toolkit$input);
MOD_ENTRY_DECL(prompt_toolkit$input$ansi_escape_sequences);
MOD_ENTRY_DECL(prompt_toolkit$input$base);
MOD_ENTRY_DECL(prompt_toolkit$input$defaults);
MOD_ENTRY_DECL(prompt_toolkit$input$posix_pipe);
MOD_ENTRY_DECL(prompt_toolkit$input$posix_utils);
MOD_ENTRY_DECL(prompt_toolkit$input$typeahead);
MOD_ENTRY_DECL(prompt_toolkit$input$vt100);
MOD_ENTRY_DECL(prompt_toolkit$input$vt100_parser);
MOD_ENTRY_DECL(prompt_toolkit$input$win32);
MOD_ENTRY_DECL(prompt_toolkit$input$win32_pipe);
MOD_ENTRY_DECL(prompt_toolkit$key_binding);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$auto_suggest);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$basic);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$completion);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$cpr);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$emacs);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$focus);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$mouse);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$named_commands);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$open_in_editor);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$page_navigation);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$scroll);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$search);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$vi);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$defaults);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$digraphs);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$emacs_state);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$key_bindings);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$key_processor);
MOD_ENTRY_DECL(prompt_toolkit$key_binding$vi_state);
MOD_ENTRY_DECL(prompt_toolkit$keys);
MOD_ENTRY_DECL(prompt_toolkit$layout);
MOD_ENTRY_DECL(prompt_toolkit$layout$containers);
MOD_ENTRY_DECL(prompt_toolkit$layout$controls);
MOD_ENTRY_DECL(prompt_toolkit$layout$dimension);
MOD_ENTRY_DECL(prompt_toolkit$layout$dummy);
MOD_ENTRY_DECL(prompt_toolkit$layout$layout);
MOD_ENTRY_DECL(prompt_toolkit$layout$margins);
MOD_ENTRY_DECL(prompt_toolkit$layout$menus);
MOD_ENTRY_DECL(prompt_toolkit$layout$mouse_handlers);
MOD_ENTRY_DECL(prompt_toolkit$layout$processors);
MOD_ENTRY_DECL(prompt_toolkit$layout$screen);
MOD_ENTRY_DECL(prompt_toolkit$layout$utils);
MOD_ENTRY_DECL(prompt_toolkit$lexers);
MOD_ENTRY_DECL(prompt_toolkit$lexers$base);
MOD_ENTRY_DECL(prompt_toolkit$lexers$pygments);
MOD_ENTRY_DECL(prompt_toolkit$log);
MOD_ENTRY_DECL(prompt_toolkit$mouse_events);
MOD_ENTRY_DECL(prompt_toolkit$output);
MOD_ENTRY_DECL(prompt_toolkit$output$base);
MOD_ENTRY_DECL(prompt_toolkit$output$color_depth);
MOD_ENTRY_DECL(prompt_toolkit$output$conemu);
MOD_ENTRY_DECL(prompt_toolkit$output$defaults);
MOD_ENTRY_DECL(prompt_toolkit$output$vt100);
MOD_ENTRY_DECL(prompt_toolkit$output$win32);
MOD_ENTRY_DECL(prompt_toolkit$output$windows10);
MOD_ENTRY_DECL(prompt_toolkit$patch_stdout);
MOD_ENTRY_DECL(prompt_toolkit$renderer);
MOD_ENTRY_DECL(prompt_toolkit$search);
MOD_ENTRY_DECL(prompt_toolkit$selection);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$dialogs);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$progress_bar);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$progress_bar$base);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$progress_bar$formatters);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$prompt);
MOD_ENTRY_DECL(prompt_toolkit$shortcuts$utils);
MOD_ENTRY_DECL(prompt_toolkit$styles);
MOD_ENTRY_DECL(prompt_toolkit$styles$base);
MOD_ENTRY_DECL(prompt_toolkit$styles$defaults);
MOD_ENTRY_DECL(prompt_toolkit$styles$named_colors);
MOD_ENTRY_DECL(prompt_toolkit$styles$pygments);
MOD_ENTRY_DECL(prompt_toolkit$styles$style);
MOD_ENTRY_DECL(prompt_toolkit$styles$style_transformation);
MOD_ENTRY_DECL(prompt_toolkit$utils);
MOD_ENTRY_DECL(prompt_toolkit$validation);
MOD_ENTRY_DECL(prompt_toolkit$widgets);
MOD_ENTRY_DECL(prompt_toolkit$widgets$base);
MOD_ENTRY_DECL(prompt_toolkit$widgets$dialogs);
MOD_ENTRY_DECL(prompt_toolkit$widgets$menus);
MOD_ENTRY_DECL(prompt_toolkit$widgets$toolbars);
MOD_ENTRY_DECL(prompt_toolkit$win32_types);
MOD_ENTRY_DECL(psutil);
MOD_ENTRY_DECL(psutil$_common);
MOD_ENTRY_DECL(psutil$_compat);
MOD_ENTRY_DECL(psutil$_psaix);
MOD_ENTRY_DECL(psutil$_psbsd);
MOD_ENTRY_DECL(psutil$_pslinux);
MOD_ENTRY_DECL(psutil$_psosx);
MOD_ENTRY_DECL(psutil$_psposix);
MOD_ENTRY_DECL(psutil$_pssunos);
MOD_ENTRY_DECL(psutil$_pswindows);
MOD_ENTRY_DECL(ptyprocess);
MOD_ENTRY_DECL(ptyprocess$_fork_pty);
MOD_ENTRY_DECL(ptyprocess$ptyprocess);
MOD_ENTRY_DECL(ptyprocess$util);
MOD_ENTRY_DECL(pygments);
MOD_ENTRY_DECL(pygments$filter);
MOD_ENTRY_DECL(pygments$filters);
MOD_ENTRY_DECL(pygments$formatters);
MOD_ENTRY_DECL(pygments$formatters$_mapping);
MOD_ENTRY_DECL(pygments$lexer);
MOD_ENTRY_DECL(pygments$lexers);
MOD_ENTRY_DECL(pygments$lexers$_mapping);
MOD_ENTRY_DECL(pygments$lexers$special);
MOD_ENTRY_DECL(pygments$modeline);
MOD_ENTRY_DECL(pygments$plugin);
MOD_ENTRY_DECL(pygments$regexopt);
MOD_ENTRY_DECL(pygments$style);
MOD_ENTRY_DECL(pygments$styles);
MOD_ENTRY_DECL(pygments$token);
MOD_ENTRY_DECL(pygments$util);
MOD_ENTRY_DECL(pymorphy2);
MOD_ENTRY_DECL(pymorphy2$analyzer);
MOD_ENTRY_DECL(pymorphy2$constants);
MOD_ENTRY_DECL(pymorphy2$dawg);
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict);
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$compile);
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$parse);
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$preprocess);
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$storage);
MOD_ENTRY_DECL(pymorphy2$opencorpora_dict$wrapper);
MOD_ENTRY_DECL(pymorphy2$shapes);
MOD_ENTRY_DECL(pymorphy2$tagset);
MOD_ENTRY_DECL(pymorphy2$units);
MOD_ENTRY_DECL(pymorphy2$units$abbreviations);
MOD_ENTRY_DECL(pymorphy2$units$base);
MOD_ENTRY_DECL(pymorphy2$units$by_analogy);
MOD_ENTRY_DECL(pymorphy2$units$by_hyphen);
MOD_ENTRY_DECL(pymorphy2$units$by_lookup);
MOD_ENTRY_DECL(pymorphy2$units$by_shape);
MOD_ENTRY_DECL(pymorphy2$units$unkn);
MOD_ENTRY_DECL(pymorphy2$units$utils);
MOD_ENTRY_DECL(pymorphy2$utils);
MOD_ENTRY_DECL(pymorphy2$version);
MOD_ENTRY_DECL(pymorphy2_dicts);
MOD_ENTRY_DECL(pymorphy2_dicts$version);
MOD_ENTRY_DECL(pyparsing);
MOD_ENTRY_DECL(pyrsistent);
MOD_ENTRY_DECL(pyrsistent$_checked_types);
MOD_ENTRY_DECL(pyrsistent$_compat);
MOD_ENTRY_DECL(pyrsistent$_field_common);
MOD_ENTRY_DECL(pyrsistent$_helpers);
MOD_ENTRY_DECL(pyrsistent$_immutable);
MOD_ENTRY_DECL(pyrsistent$_pbag);
MOD_ENTRY_DECL(pyrsistent$_pclass);
MOD_ENTRY_DECL(pyrsistent$_pdeque);
MOD_ENTRY_DECL(pyrsistent$_plist);
MOD_ENTRY_DECL(pyrsistent$_pmap);
MOD_ENTRY_DECL(pyrsistent$_precord);
MOD_ENTRY_DECL(pyrsistent$_pset);
MOD_ENTRY_DECL(pyrsistent$_pvector);
MOD_ENTRY_DECL(pyrsistent$_toolz);
MOD_ENTRY_DECL(pyrsistent$_transformations);
MOD_ENTRY_DECL(requests);
MOD_ENTRY_DECL(requests$__version__);
MOD_ENTRY_DECL(requests$_internal_utils);
MOD_ENTRY_DECL(requests$adapters);
MOD_ENTRY_DECL(requests$api);
MOD_ENTRY_DECL(requests$auth);
MOD_ENTRY_DECL(requests$certs);
MOD_ENTRY_DECL(requests$compat);
MOD_ENTRY_DECL(requests$cookies);
MOD_ENTRY_DECL(requests$exceptions);
MOD_ENTRY_DECL(requests$hooks);
MOD_ENTRY_DECL(requests$models);
MOD_ENTRY_DECL(requests$packages);
MOD_ENTRY_DECL(requests$sessions);
MOD_ENTRY_DECL(requests$status_codes);
MOD_ENTRY_DECL(requests$structures);
MOD_ENTRY_DECL(requests$utils);
MOD_ENTRY_DECL(scipy);
MOD_ENTRY_DECL(scipy$__config__);
MOD_ENTRY_DECL(scipy$_distributor_init);
MOD_ENTRY_DECL(scipy$_lib);
MOD_ENTRY_DECL(scipy$_lib$_ccallback);
MOD_ENTRY_DECL(scipy$_lib$_testutils);
MOD_ENTRY_DECL(scipy$_lib$_version);
MOD_ENTRY_DECL(scipy$_lib$six);
MOD_ENTRY_DECL(scipy$version);
MOD_ENTRY_DECL(six);
MOD_ENTRY_DECL(testpath);
MOD_ENTRY_DECL(testpath$asserts);
MOD_ENTRY_DECL(testpath$commands);
MOD_ENTRY_DECL(testpath$env);
MOD_ENTRY_DECL(testpath$tempdir);
MOD_ENTRY_DECL(tokenizer);
MOD_ENTRY_DECL(tornado);
MOD_ENTRY_DECL(tornado$_locale_data);
MOD_ENTRY_DECL(tornado$autoreload);
MOD_ENTRY_DECL(tornado$concurrent);
MOD_ENTRY_DECL(tornado$escape);
MOD_ENTRY_DECL(tornado$gen);
MOD_ENTRY_DECL(tornado$http1connection);
MOD_ENTRY_DECL(tornado$httpclient);
MOD_ENTRY_DECL(tornado$httpserver);
MOD_ENTRY_DECL(tornado$httputil);
MOD_ENTRY_DECL(tornado$ioloop);
MOD_ENTRY_DECL(tornado$iostream);
MOD_ENTRY_DECL(tornado$locale);
MOD_ENTRY_DECL(tornado$locks);
MOD_ENTRY_DECL(tornado$log);
MOD_ENTRY_DECL(tornado$netutil);
MOD_ENTRY_DECL(tornado$options);
MOD_ENTRY_DECL(tornado$platform);
MOD_ENTRY_DECL(tornado$platform$asyncio);
MOD_ENTRY_DECL(tornado$platform$auto);
MOD_ENTRY_DECL(tornado$platform$posix);
MOD_ENTRY_DECL(tornado$platform$windows);
MOD_ENTRY_DECL(tornado$process);
MOD_ENTRY_DECL(tornado$queues);
MOD_ENTRY_DECL(tornado$routing);
MOD_ENTRY_DECL(tornado$simple_httpclient);
MOD_ENTRY_DECL(tornado$tcpclient);
MOD_ENTRY_DECL(tornado$tcpserver);
MOD_ENTRY_DECL(tornado$template);
MOD_ENTRY_DECL(tornado$util);
MOD_ENTRY_DECL(tornado$web);
MOD_ENTRY_DECL(tornado$websocket);
MOD_ENTRY_DECL(traitlets);
MOD_ENTRY_DECL(traitlets$_version);
MOD_ENTRY_DECL(traitlets$config);
MOD_ENTRY_DECL(traitlets$config$application);
MOD_ENTRY_DECL(traitlets$config$configurable);
MOD_ENTRY_DECL(traitlets$config$loader);
MOD_ENTRY_DECL(traitlets$log);
MOD_ENTRY_DECL(traitlets$traitlets);
MOD_ENTRY_DECL(traitlets$utils);
MOD_ENTRY_DECL(traitlets$utils$bunch);
MOD_ENTRY_DECL(traitlets$utils$getargspec);
MOD_ENTRY_DECL(traitlets$utils$importstring);
MOD_ENTRY_DECL(traitlets$utils$sentinel);
MOD_ENTRY_DECL(typing_extensions);
MOD_ENTRY_DECL(urllib3);
MOD_ENTRY_DECL(urllib3$_collections);
MOD_ENTRY_DECL(urllib3$connection);
MOD_ENTRY_DECL(urllib3$connectionpool);
MOD_ENTRY_DECL(urllib3$contrib);
MOD_ENTRY_DECL(urllib3$contrib$_appengine_environ);
MOD_ENTRY_DECL(urllib3$contrib$pyopenssl);
MOD_ENTRY_DECL(urllib3$contrib$socks);
MOD_ENTRY_DECL(urllib3$exceptions);
MOD_ENTRY_DECL(urllib3$fields);
MOD_ENTRY_DECL(urllib3$filepost);
MOD_ENTRY_DECL(urllib3$packages);
MOD_ENTRY_DECL(urllib3$packages$backports);
MOD_ENTRY_DECL(urllib3$packages$backports$makefile);
MOD_ENTRY_DECL(urllib3$packages$six);
MOD_ENTRY_DECL(urllib3$packages$ssl_match_hostname);
MOD_ENTRY_DECL(urllib3$packages$ssl_match_hostname$_implementation);
MOD_ENTRY_DECL(urllib3$poolmanager);
MOD_ENTRY_DECL(urllib3$request);
MOD_ENTRY_DECL(urllib3$response);
MOD_ENTRY_DECL(urllib3$util);
MOD_ENTRY_DECL(urllib3$util$connection);
MOD_ENTRY_DECL(urllib3$util$queue);
MOD_ENTRY_DECL(urllib3$util$request);
MOD_ENTRY_DECL(urllib3$util$response);
MOD_ENTRY_DECL(urllib3$util$retry);
MOD_ENTRY_DECL(urllib3$util$ssl_);
MOD_ENTRY_DECL(urllib3$util$timeout);
MOD_ENTRY_DECL(urllib3$util$url);
MOD_ENTRY_DECL(urllib3$util$wait);
MOD_ENTRY_DECL(utils);
MOD_ENTRY_DECL(wcwidth);
MOD_ENTRY_DECL(wcwidth$table_wide);
MOD_ENTRY_DECL(wcwidth$table_zero);
MOD_ENTRY_DECL(wcwidth$wcwidth);
MOD_ENTRY_DECL(yargy);
MOD_ENTRY_DECL(yargy$api);
MOD_ENTRY_DECL(yargy$compat);
MOD_ENTRY_DECL(yargy$dot);
MOD_ENTRY_DECL(yargy$interpretation);
MOD_ENTRY_DECL(yargy$interpretation$attribute);
MOD_ENTRY_DECL(yargy$interpretation$fact);
MOD_ENTRY_DECL(yargy$interpretation$interpretator);
MOD_ENTRY_DECL(yargy$interpretation$normalizer);
MOD_ENTRY_DECL(yargy$morph);
MOD_ENTRY_DECL(yargy$parser);
MOD_ENTRY_DECL(yargy$pipelines);
MOD_ENTRY_DECL(yargy$predicates);
MOD_ENTRY_DECL(yargy$predicates$bank);
MOD_ENTRY_DECL(yargy$predicates$constructors);
MOD_ENTRY_DECL(yargy$relations);
MOD_ENTRY_DECL(yargy$relations$bank);
MOD_ENTRY_DECL(yargy$relations$constructors);
MOD_ENTRY_DECL(yargy$relations$graph);
MOD_ENTRY_DECL(yargy$rule);
MOD_ENTRY_DECL(yargy$rule$bnf);
MOD_ENTRY_DECL(yargy$rule$constructors);
MOD_ENTRY_DECL(yargy$rule$transformators);
MOD_ENTRY_DECL(yargy$span);
MOD_ENTRY_DECL(yargy$tagger);
MOD_ENTRY_DECL(yargy$token);
MOD_ENTRY_DECL(yargy$tokenizer);
MOD_ENTRY_DECL(yargy$tree);
MOD_ENTRY_DECL(yargy$tree$constructors);
MOD_ENTRY_DECL(yargy$tree$transformators);
MOD_ENTRY_DECL(yargy$utils);
MOD_ENTRY_DECL(yargy$visitor);
MOD_ENTRY_DECL(zmq);
MOD_ENTRY_DECL(zmq$backend);
MOD_ENTRY_DECL(zmq$backend$cython);
MOD_ENTRY_DECL(zmq$backend$select);
MOD_ENTRY_DECL(zmq$error);
MOD_ENTRY_DECL(zmq$eventloop);
MOD_ENTRY_DECL(zmq$eventloop$_deprecated);
MOD_ENTRY_DECL(zmq$eventloop$ioloop);
MOD_ENTRY_DECL(zmq$eventloop$minitornado);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$concurrent);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$ioloop);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$log);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$platform);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$platform$auto);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$platform$common);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$platform$interface);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$platform$posix);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$platform$windows);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$stack_context);
MOD_ENTRY_DECL(zmq$eventloop$minitornado$util);
MOD_ENTRY_DECL(zmq$eventloop$zmqstream);
MOD_ENTRY_DECL(zmq$ssh);
MOD_ENTRY_DECL(zmq$ssh$forward);
MOD_ENTRY_DECL(zmq$ssh$tunnel);
MOD_ENTRY_DECL(zmq$sugar);
MOD_ENTRY_DECL(zmq$sugar$attrsettr);
MOD_ENTRY_DECL(zmq$sugar$constants);
MOD_ENTRY_DECL(zmq$sugar$context);
MOD_ENTRY_DECL(zmq$sugar$frame);
MOD_ENTRY_DECL(zmq$sugar$poll);
MOD_ENTRY_DECL(zmq$sugar$socket);
MOD_ENTRY_DECL(zmq$sugar$stopwatch);
MOD_ENTRY_DECL(zmq$sugar$tracker);
MOD_ENTRY_DECL(zmq$sugar$version);
MOD_ENTRY_DECL(zmq$utils);
MOD_ENTRY_DECL(zmq$utils$constant_names);
MOD_ENTRY_DECL(zmq$utils$interop);
MOD_ENTRY_DECL(zmq$utils$jsonapi);
MOD_ENTRY_DECL(zmq$utils$sixcerpt);
MOD_ENTRY_DECL(zmq$utils$strtypes);
static struct Nuitka_MetaPathBasedLoaderEntry meta_path_loader_entries[] =
{
    { "IPython", NULL, 5924572, 4594, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.core", NULL, 5929166, 134, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.core.alias", NULL, 5929300, 7007, NUITKA_BYTECODE_FLAG },
    { "IPython.core.application", NULL, 5936307, 13952, NUITKA_BYTECODE_FLAG },
    { "IPython.core.async_helpers", NULL, 5950259, 5050, NUITKA_BYTECODE_FLAG },
    { "IPython.core.autocall", NULL, 5955309, 2044, NUITKA_BYTECODE_FLAG },
    { "IPython.core.builtin_trap", NULL, 5957353, 2818, NUITKA_BYTECODE_FLAG },
    { "IPython.core.compilerop", NULL, 5960171, 3634, NUITKA_BYTECODE_FLAG },
    { "IPython.core.completer", NULL, 5963805, 59258, NUITKA_BYTECODE_FLAG },
    { "IPython.core.completerlib", NULL, 6023063, 8859, NUITKA_BYTECODE_FLAG },
    { "IPython.core.crashhandler", NULL, 6031922, 6308, NUITKA_BYTECODE_FLAG },
    { "IPython.core.debugger", NULL, 6038230, 17343, NUITKA_BYTECODE_FLAG },
    { "IPython.core.display", NULL, 6055573, 46766, NUITKA_BYTECODE_FLAG },
    { "IPython.core.display_trap", NULL, 6102339, 1684, NUITKA_BYTECODE_FLAG },
    { "IPython.core.displayhook", NULL, 6104023, 9293, NUITKA_BYTECODE_FLAG },
    { "IPython.core.displaypub", NULL, 6113316, 4637, NUITKA_BYTECODE_FLAG },
    { "IPython.core.error", NULL, 6117953, 1681, NUITKA_BYTECODE_FLAG },
    { "IPython.core.events", NULL, 6119634, 5506, NUITKA_BYTECODE_FLAG },
    { "IPython.core.excolors", NULL, 6125140, 2623, NUITKA_BYTECODE_FLAG },
    { "IPython.core.extensions", NULL, 6127763, 5788, NUITKA_BYTECODE_FLAG },
    { "IPython.core.formatters", NULL, 6133551, 30821, NUITKA_BYTECODE_FLAG },
    { "IPython.core.getipython", NULL, 6164372, 551, NUITKA_BYTECODE_FLAG },
    { "IPython.core.history", NULL, 6164923, 27704, NUITKA_BYTECODE_FLAG },
    { "IPython.core.hooks", NULL, 6192627, 7241, NUITKA_BYTECODE_FLAG },
    { "IPython.core.inputsplitter", NULL, 6199868, 19096, NUITKA_BYTECODE_FLAG },
    { "IPython.core.inputtransformer", NULL, 6218964, 15662, NUITKA_BYTECODE_FLAG },
    { "IPython.core.inputtransformer2", NULL, 6234626, 21679, NUITKA_BYTECODE_FLAG },
    { "IPython.core.interactiveshell", NULL, 6256305, 101623, NUITKA_BYTECODE_FLAG },
    { "IPython.core.latex_symbols", NULL, 6357928, 29336, NUITKA_BYTECODE_FLAG },
    { "IPython.core.logger", NULL, 6387264, 5242, NUITKA_BYTECODE_FLAG },
    { "IPython.core.macro", NULL, 6392506, 1727, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magic", NULL, 6394233, 19919, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magic_arguments", NULL, 6414152, 8566, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics", NULL, 6422718, 1357, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.core.magics.auto", NULL, 6424075, 3583, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.basic", NULL, 6427658, 20176, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.code", NULL, 6447834, 20575, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.config", NULL, 6468409, 4957, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.display", NULL, 6473366, 2578, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.execution", NULL, 6475944, 43599, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.extension", NULL, 6519543, 1946, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.history", NULL, 6521489, 9023, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.logging", NULL, 6530512, 5259, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.namespace", NULL, 6535771, 20136, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.osm", NULL, 6555907, 23794, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.packaging", NULL, 6579701, 2929, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.pylab", NULL, 6582630, 5563, NUITKA_BYTECODE_FLAG },
    { "IPython.core.magics.script", NULL, 6588193, 8273, NUITKA_BYTECODE_FLAG },
    { "IPython.core.oinspect", NULL, 6596466, 24837, NUITKA_BYTECODE_FLAG },
    { "IPython.core.page", NULL, 6621303, 8683, NUITKA_BYTECODE_FLAG },
    { "IPython.core.payload", NULL, 6629986, 1384, NUITKA_BYTECODE_FLAG },
    { "IPython.core.payloadpage", NULL, 6631370, 1389, NUITKA_BYTECODE_FLAG },
    { "IPython.core.prefilter", NULL, 6632759, 20624, NUITKA_BYTECODE_FLAG },
    { "IPython.core.profiledir", NULL, 6653383, 7364, NUITKA_BYTECODE_FLAG },
    { "IPython.core.pylabtools", NULL, 6660747, 9841, NUITKA_BYTECODE_FLAG },
    { "IPython.core.release", NULL, 6670588, 3074, NUITKA_BYTECODE_FLAG },
    { "IPython.core.shellapp", NULL, 6673662, 13119, NUITKA_BYTECODE_FLAG },
    { "IPython.core.splitinput", NULL, 6686781, 3532, NUITKA_BYTECODE_FLAG },
    { "IPython.core.ultratb", NULL, 6690313, 37677, NUITKA_BYTECODE_FLAG },
    { "IPython.core.usage", NULL, 6727990, 13241, NUITKA_BYTECODE_FLAG },
    { "IPython.display", NULL, 6741231, 254, NUITKA_BYTECODE_FLAG },
    { "IPython.extensions", NULL, 6741485, 199, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.extensions.storemagic", NULL, 6741684, 6359, NUITKA_BYTECODE_FLAG },
    { "IPython.external", NULL, 6748043, 234, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.external.decorators", NULL, 6748277, 412, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.external.decorators._decorators", NULL, 6748689, 4560, NUITKA_BYTECODE_FLAG },
    { "IPython.external.decorators._numpy_testing_noseclasses", NULL, 6753249, 1655, NUITKA_BYTECODE_FLAG },
    { "IPython.external.qt_for_kernel", NULL, 6754904, 2372, NUITKA_BYTECODE_FLAG },
    { "IPython.external.qt_loaders", NULL, 6757276, 8224, NUITKA_BYTECODE_FLAG },
    { "IPython.kernel", NULL, 6765500, 976, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.lib", NULL, 6766476, 233, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.lib.backgroundjobs", NULL, 6766709, 15912, NUITKA_BYTECODE_FLAG },
    { "IPython.lib.clipboard", NULL, 6782621, 2220, NUITKA_BYTECODE_FLAG },
    { "IPython.lib.display", NULL, 6784841, 21949, NUITKA_BYTECODE_FLAG },
    { "IPython.lib.guisupport", NULL, 6806790, 5018, NUITKA_BYTECODE_FLAG },
    { "IPython.lib.lexers", NULL, 6811808, 12520, NUITKA_BYTECODE_FLAG },
    { "IPython.lib.pretty", NULL, 6824328, 24015, NUITKA_BYTECODE_FLAG },
    { "IPython.lib.security", NULL, 6848343, 2703, NUITKA_BYTECODE_FLAG },
    { "IPython.paths", NULL, 6851046, 3584, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal", NULL, 6854630, 138, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.terminal.debugger", NULL, 6854768, 3945, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.embed", NULL, 6858713, 11456, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.interactiveshell", NULL, 6870169, 17384, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.ipapp", NULL, 6887553, 11570, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.magics", NULL, 6899123, 7072, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.prompts", NULL, 6906195, 3615, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.pt_inputhooks", NULL, 6909810, 1465, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.terminal.ptutils", NULL, 6911275, 4780, NUITKA_BYTECODE_FLAG },
    { "IPython.terminal.shortcuts", NULL, 6916055, 7183, NUITKA_BYTECODE_FLAG },
    { "IPython.testing", NULL, 6923238, 793, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.testing.decorators", NULL, 6924031, 11259, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.globalipapp", NULL, 6935290, 3395, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.iptest", NULL, 6938685, 13024, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.iptestcontroller", NULL, 6951709, 15284, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.ipunittest", NULL, 6966993, 5043, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.plugin", NULL, 6972036, 144, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.testing.plugin.ipdoctest", NULL, 6972180, 19202, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.skipdoctest", NULL, 6991382, 802, NUITKA_BYTECODE_FLAG },
    { "IPython.testing.tools", NULL, 6992184, 12607, NUITKA_BYTECODE_FLAG },
    { "IPython.utils", NULL, 7004791, 135, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "IPython.utils.PyColorize", NULL, 7004926, 5893, NUITKA_BYTECODE_FLAG },
    { "IPython.utils._process_cli", NULL, 7010819, 2300, NUITKA_BYTECODE_FLAG },
    { "IPython.utils._process_common", NULL, 7013119, 5475, NUITKA_BYTECODE_FLAG },
    { "IPython.utils._process_posix", NULL, 7018594, 4710, NUITKA_BYTECODE_FLAG },
    { "IPython.utils._process_win32", NULL, 7023304, 5429, NUITKA_BYTECODE_FLAG },
    { "IPython.utils._sysinfo", NULL, 7028733, 155, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.capture", NULL, 7028888, 5746, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.colorable", NULL, 7034634, 937, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.coloransi", NULL, 7035571, 5989, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.contexts", NULL, 7041560, 2483, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.data", NULL, 7044043, 1147, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.decorators", NULL, 7045190, 1574, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.dir2", NULL, 7046764, 1985, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.encoding", NULL, 7048749, 1759, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.frame", NULL, 7050508, 2898, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.generics", NULL, 7053406, 952, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.importstring", NULL, 7054358, 981, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.io", NULL, 7055339, 7662, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.ipstruct", NULL, 7063001, 10847, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.module_paths", NULL, 7073848, 1309, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.openpy", NULL, 7075157, 3446, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.path", NULL, 7078603, 12604, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.process", NULL, 7091207, 2037, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.py3compat", NULL, 7093244, 6231, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.sentinel", NULL, 7099475, 684, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.shimmodule", NULL, 7100159, 3471, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.strdispatch", NULL, 7103630, 2272, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.sysinfo", NULL, 7105902, 4371, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.syspathcontext", NULL, 7110273, 1869, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.tempdir", NULL, 7112142, 2621, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.terminal", NULL, 7114763, 2964, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.text", NULL, 7117727, 23490, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.timing", NULL, 7141217, 3483, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.tokenutil", NULL, 7144700, 2699, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.version", NULL, 7147399, 705, NUITKA_BYTECODE_FLAG },
    { "IPython.utils.wildcard", NULL, 7148104, 3309, NUITKA_BYTECODE_FLAG },
    { "PIL", MOD_INIT_NAME( PIL ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "PIL.BmpImagePlugin", MOD_INIT_NAME( PIL$BmpImagePlugin ), 0, 0,  },
    { "PIL.GifImagePlugin", MOD_INIT_NAME( PIL$GifImagePlugin ), 0, 0,  },
    { "PIL.GimpGradientFile", MOD_INIT_NAME( PIL$GimpGradientFile ), 0, 0,  },
    { "PIL.GimpPaletteFile", MOD_INIT_NAME( PIL$GimpPaletteFile ), 0, 0,  },
    { "PIL.Image", MOD_INIT_NAME( PIL$Image ), 0, 0,  },
    { "PIL.ImageChops", MOD_INIT_NAME( PIL$ImageChops ), 0, 0,  },
    { "PIL.ImageColor", MOD_INIT_NAME( PIL$ImageColor ), 0, 0,  },
    { "PIL.ImageFile", MOD_INIT_NAME( PIL$ImageFile ), 0, 0,  },
    { "PIL.ImageFilter", MOD_INIT_NAME( PIL$ImageFilter ), 0, 0,  },
    { "PIL.ImageMode", MOD_INIT_NAME( PIL$ImageMode ), 0, 0,  },
    { "PIL.ImagePalette", MOD_INIT_NAME( PIL$ImagePalette ), 0, 0,  },
    { "PIL.ImageQt", MOD_INIT_NAME( PIL$ImageQt ), 0, 0,  },
    { "PIL.ImageSequence", MOD_INIT_NAME( PIL$ImageSequence ), 0, 0,  },
    { "PIL.ImageShow", MOD_INIT_NAME( PIL$ImageShow ), 0, 0,  },
    { "PIL.JpegImagePlugin", MOD_INIT_NAME( PIL$JpegImagePlugin ), 0, 0,  },
    { "PIL.JpegPresets", MOD_INIT_NAME( PIL$JpegPresets ), 0, 0,  },
    { "PIL.MpoImagePlugin", MOD_INIT_NAME( PIL$MpoImagePlugin ), 0, 0,  },
    { "PIL.PaletteFile", MOD_INIT_NAME( PIL$PaletteFile ), 0, 0,  },
    { "PIL.PngImagePlugin", MOD_INIT_NAME( PIL$PngImagePlugin ), 0, 0,  },
    { "PIL.PpmImagePlugin", MOD_INIT_NAME( PIL$PpmImagePlugin ), 0, 0,  },
    { "PIL.PyAccess", MOD_INIT_NAME( PIL$PyAccess ), 0, 0,  },
    { "PIL.TiffImagePlugin", MOD_INIT_NAME( PIL$TiffImagePlugin ), 0, 0,  },
    { "PIL.TiffTags", MOD_INIT_NAME( PIL$TiffTags ), 0, 0,  },
    { "PIL._binary", MOD_INIT_NAME( PIL$_binary ), 0, 0,  },
    { "PIL._imaging", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "PIL._util", MOD_INIT_NAME( PIL$_util ), 0, 0,  },
    { "PIL._version", MOD_INIT_NAME( PIL$_version ), 0, 0,  },
    { "__future__", NULL, 7151413, 4165, NUITKA_BYTECODE_FLAG },
    { "__main__", MOD_INIT_NAME( __main__ ), 0, 0,  },
    { "_asyncio", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_bisect", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_blake2", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_bz2", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_cffi_backend", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_codecs_cn", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_codecs_hk", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_codecs_iso2022", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_codecs_jp", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_codecs_kr", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_codecs_tw", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_collections_abc", NULL, 7155578, 28975, NUITKA_BYTECODE_FLAG },
    { "_contextvars", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_crypt", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_csv", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_ctypes", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_curses", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_curses_panel", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_datetime", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_dbm", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_decimal", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_elementtree", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_gdbm", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_hashlib", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_heapq", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_json", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_lsprof", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_lzma", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_multibytecodec", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_multiprocessing", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_opcode", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_pickle", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_posixsubprocess", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_queue", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_random", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_scproxy", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_sha3", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_socket", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_sqlite3", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_ssl", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_struct", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_tkinter", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "_uuid", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "abc", NULL, 7184553, 6484, NUITKA_BYTECODE_FLAG },
    { "appnope", MOD_INIT_NAME( appnope ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "appnope._dummy", MOD_INIT_NAME( appnope$_dummy ), 0, 0,  },
    { "appnope._nope", MOD_INIT_NAME( appnope$_nope ), 0, 0,  },
    { "argparse", NULL, 7191037, 61946, NUITKA_BYTECODE_FLAG },
    { "array", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "ast", NULL, 7252983, 12113, NUITKA_BYTECODE_FLAG },
    { "asyncio", NULL, 7265096, 723, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "attr", MOD_INIT_NAME( attr ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "attr._compat", MOD_INIT_NAME( attr$_compat ), 0, 0,  },
    { "attr._config", MOD_INIT_NAME( attr$_config ), 0, 0,  },
    { "attr._funcs", MOD_INIT_NAME( attr$_funcs ), 0, 0,  },
    { "attr._make", MOD_INIT_NAME( attr$_make ), 0, 0,  },
    { "attr.converters", MOD_INIT_NAME( attr$converters ), 0, 0,  },
    { "attr.exceptions", MOD_INIT_NAME( attr$exceptions ), 0, 0,  },
    { "attr.filters", MOD_INIT_NAME( attr$filters ), 0, 0,  },
    { "attr.validators", MOD_INIT_NAME( attr$validators ), 0, 0,  },
    { "audioop", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "backcall", MOD_INIT_NAME( backcall ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "backcall._signatures", MOD_INIT_NAME( backcall$_signatures ), 0, 0,  },
    { "backcall.backcall", MOD_INIT_NAME( backcall$backcall ), 0, 0,  },
    { "backports", MOD_INIT_NAME( backports ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "backports.configparser", MOD_INIT_NAME( backports$configparser ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "backports.configparser.helpers", MOD_INIT_NAME( backports$configparser$helpers ), 0, 0,  },
    { "backports.functools_lru_cache", MOD_INIT_NAME( backports$functools_lru_cache ), 0, 0,  },
    { "base64", NULL, 7265819, 17021, NUITKA_BYTECODE_FLAG },
    { "bdb", NULL, 7282840, 24427, NUITKA_BYTECODE_FLAG },
    { "binascii", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "bisect", NULL, 7307267, 2731, NUITKA_BYTECODE_FLAG },
    { "bz2", NULL, 7309998, 11214, NUITKA_BYTECODE_FLAG },
    { "cProfile", NULL, 7321212, 4584, NUITKA_BYTECODE_FLAG },
    { "calendar", NULL, 7325796, 27457, NUITKA_BYTECODE_FLAG },
    { "certifi", MOD_INIT_NAME( certifi ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "certifi.core", MOD_INIT_NAME( certifi$core ), 0, 0,  },
    { "cffi", MOD_INIT_NAME( cffi ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "cffi.api", MOD_INIT_NAME( cffi$api ), 0, 0,  },
    { "cffi.cffi_opcode", MOD_INIT_NAME( cffi$cffi_opcode ), 0, 0,  },
    { "cffi.commontypes", MOD_INIT_NAME( cffi$commontypes ), 0, 0,  },
    { "cffi.cparser", MOD_INIT_NAME( cffi$cparser ), 0, 0,  },
    { "cffi.error", MOD_INIT_NAME( cffi$error ), 0, 0,  },
    { "cffi.ffiplatform", MOD_INIT_NAME( cffi$ffiplatform ), 0, 0,  },
    { "cffi.lock", MOD_INIT_NAME( cffi$lock ), 0, 0,  },
    { "cffi.model", MOD_INIT_NAME( cffi$model ), 0, 0,  },
    { "cffi.pkgconfig", MOD_INIT_NAME( cffi$pkgconfig ), 0, 0,  },
    { "cffi.recompiler", MOD_INIT_NAME( cffi$recompiler ), 0, 0,  },
    { "cffi.vengine_cpy", MOD_INIT_NAME( cffi$vengine_cpy ), 0, 0,  },
    { "cffi.vengine_gen", MOD_INIT_NAME( cffi$vengine_gen ), 0, 0,  },
    { "cffi.verifier", MOD_INIT_NAME( cffi$verifier ), 0, 0,  },
    { "cgi", NULL, 7353253, 27224, NUITKA_BYTECODE_FLAG },
    { "chardet", MOD_INIT_NAME( chardet ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "chardet.big5freq", MOD_INIT_NAME( chardet$big5freq ), 0, 0,  },
    { "chardet.big5prober", MOD_INIT_NAME( chardet$big5prober ), 0, 0,  },
    { "chardet.chardistribution", MOD_INIT_NAME( chardet$chardistribution ), 0, 0,  },
    { "chardet.charsetgroupprober", MOD_INIT_NAME( chardet$charsetgroupprober ), 0, 0,  },
    { "chardet.charsetprober", MOD_INIT_NAME( chardet$charsetprober ), 0, 0,  },
    { "chardet.codingstatemachine", MOD_INIT_NAME( chardet$codingstatemachine ), 0, 0,  },
    { "chardet.compat", MOD_INIT_NAME( chardet$compat ), 0, 0,  },
    { "chardet.cp949prober", MOD_INIT_NAME( chardet$cp949prober ), 0, 0,  },
    { "chardet.enums", MOD_INIT_NAME( chardet$enums ), 0, 0,  },
    { "chardet.escprober", MOD_INIT_NAME( chardet$escprober ), 0, 0,  },
    { "chardet.escsm", MOD_INIT_NAME( chardet$escsm ), 0, 0,  },
    { "chardet.eucjpprober", MOD_INIT_NAME( chardet$eucjpprober ), 0, 0,  },
    { "chardet.euckrfreq", MOD_INIT_NAME( chardet$euckrfreq ), 0, 0,  },
    { "chardet.euckrprober", MOD_INIT_NAME( chardet$euckrprober ), 0, 0,  },
    { "chardet.euctwfreq", MOD_INIT_NAME( chardet$euctwfreq ), 0, 0,  },
    { "chardet.euctwprober", MOD_INIT_NAME( chardet$euctwprober ), 0, 0,  },
    { "chardet.gb2312freq", MOD_INIT_NAME( chardet$gb2312freq ), 0, 0,  },
    { "chardet.gb2312prober", MOD_INIT_NAME( chardet$gb2312prober ), 0, 0,  },
    { "chardet.hebrewprober", MOD_INIT_NAME( chardet$hebrewprober ), 0, 0,  },
    { "chardet.jisfreq", MOD_INIT_NAME( chardet$jisfreq ), 0, 0,  },
    { "chardet.jpcntx", MOD_INIT_NAME( chardet$jpcntx ), 0, 0,  },
    { "chardet.langbulgarianmodel", MOD_INIT_NAME( chardet$langbulgarianmodel ), 0, 0,  },
    { "chardet.langcyrillicmodel", MOD_INIT_NAME( chardet$langcyrillicmodel ), 0, 0,  },
    { "chardet.langgreekmodel", MOD_INIT_NAME( chardet$langgreekmodel ), 0, 0,  },
    { "chardet.langhebrewmodel", MOD_INIT_NAME( chardet$langhebrewmodel ), 0, 0,  },
    { "chardet.langthaimodel", MOD_INIT_NAME( chardet$langthaimodel ), 0, 0,  },
    { "chardet.langturkishmodel", MOD_INIT_NAME( chardet$langturkishmodel ), 0, 0,  },
    { "chardet.latin1prober", MOD_INIT_NAME( chardet$latin1prober ), 0, 0,  },
    { "chardet.mbcharsetprober", MOD_INIT_NAME( chardet$mbcharsetprober ), 0, 0,  },
    { "chardet.mbcsgroupprober", MOD_INIT_NAME( chardet$mbcsgroupprober ), 0, 0,  },
    { "chardet.mbcssm", MOD_INIT_NAME( chardet$mbcssm ), 0, 0,  },
    { "chardet.sbcharsetprober", MOD_INIT_NAME( chardet$sbcharsetprober ), 0, 0,  },
    { "chardet.sbcsgroupprober", MOD_INIT_NAME( chardet$sbcsgroupprober ), 0, 0,  },
    { "chardet.sjisprober", MOD_INIT_NAME( chardet$sjisprober ), 0, 0,  },
    { "chardet.universaldetector", MOD_INIT_NAME( chardet$universaldetector ), 0, 0,  },
    { "chardet.utf8prober", MOD_INIT_NAME( chardet$utf8prober ), 0, 0,  },
    { "chardet.version", MOD_INIT_NAME( chardet$version ), 0, 0,  },
    { "cloudpickle", MOD_INIT_NAME( cloudpickle ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "cloudpickle.cloudpickle", MOD_INIT_NAME( cloudpickle$cloudpickle ), 0, 0,  },
    { "cloudpickle.cloudpickle_fast", MOD_INIT_NAME( cloudpickle$cloudpickle_fast ), 0, 0,  },
    { "codecs", NULL, 7380477, 33931, NUITKA_BYTECODE_FLAG },
    { "codeop", NULL, 7414408, 6323, NUITKA_BYTECODE_FLAG },
    { "collections", NULL, 7420731, 46643, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "colorama", MOD_INIT_NAME( colorama ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "colorama.ansi", MOD_INIT_NAME( colorama$ansi ), 0, 0,  },
    { "colorama.ansitowin32", MOD_INIT_NAME( colorama$ansitowin32 ), 0, 0,  },
    { "colorama.initialise", MOD_INIT_NAME( colorama$initialise ), 0, 0,  },
    { "colorama.win32", MOD_INIT_NAME( colorama$win32 ), 0, 0,  },
    { "colorama.winterm", MOD_INIT_NAME( colorama$winterm ), 0, 0,  },
    { "colorsys", NULL, 7467374, 3330, NUITKA_BYTECODE_FLAG },
    { "concurrent", NULL, 7470704, 179, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "concurrent.futures", NULL, 7470883, 1119, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "configparser", NULL, 7472002, 45913, NUITKA_BYTECODE_FLAG },
    { "contextlib", NULL, 7517915, 19933, NUITKA_BYTECODE_FLAG },
    { "copy", NULL, 7537848, 7134, NUITKA_BYTECODE_FLAG },
    { "copyreg", NULL, 7544982, 4277, NUITKA_BYTECODE_FLAG },
    { "crf", MOD_INIT_NAME( crf ), 0, 0,  },
    { "csv", NULL, 7549259, 11865, NUITKA_BYTECODE_FLAG },
    { "ctypes", NULL, 7561124, 16182, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "ctypes.util", NULL, 7577306, 7782, NUITKA_BYTECODE_FLAG },
    { "curses", NULL, 7585088, 1828, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "cycler", MOD_INIT_NAME( cycler ), 0, 0,  },
    { "data", MOD_INIT_NAME( data ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "datetime", NULL, 7586916, 57282, NUITKA_BYTECODE_FLAG },
    { "dateutil", MOD_INIT_NAME( dateutil ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "dateutil._common", MOD_INIT_NAME( dateutil$_common ), 0, 0,  },
    { "dateutil._version", MOD_INIT_NAME( dateutil$_version ), 0, 0,  },
    { "dateutil.easter", MOD_INIT_NAME( dateutil$easter ), 0, 0,  },
    { "dateutil.parser", MOD_INIT_NAME( dateutil$parser ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "dateutil.parser._parser", MOD_INIT_NAME( dateutil$parser$_parser ), 0, 0,  },
    { "dateutil.parser.isoparser", MOD_INIT_NAME( dateutil$parser$isoparser ), 0, 0,  },
    { "dateutil.relativedelta", MOD_INIT_NAME( dateutil$relativedelta ), 0, 0,  },
    { "dateutil.rrule", MOD_INIT_NAME( dateutil$rrule ), 0, 0,  },
    { "dateutil.tz", MOD_INIT_NAME( dateutil$tz ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "dateutil.tz._common", MOD_INIT_NAME( dateutil$tz$_common ), 0, 0,  },
    { "dateutil.tz._factories", MOD_INIT_NAME( dateutil$tz$_factories ), 0, 0,  },
    { "dateutil.tz.tz", MOD_INIT_NAME( dateutil$tz$tz ), 0, 0,  },
    { "dateutil.tz.win", MOD_INIT_NAME( dateutil$tz$win ), 0, 0,  },
    { "dateutil.zoneinfo", MOD_INIT_NAME( dateutil$zoneinfo ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "dawg_python", MOD_INIT_NAME( dawg_python ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "dawg_python.compat", MOD_INIT_NAME( dawg_python$compat ), 0, 0,  },
    { "dawg_python.dawgs", MOD_INIT_NAME( dawg_python$dawgs ), 0, 0,  },
    { "dawg_python.units", MOD_INIT_NAME( dawg_python$units ), 0, 0,  },
    { "dawg_python.wrapper", MOD_INIT_NAME( dawg_python$wrapper ), 0, 0,  },
    { "decorator", MOD_INIT_NAME( decorator ), 0, 0,  },
    { "defusedxml", MOD_INIT_NAME( defusedxml ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "defusedxml.ElementTree", MOD_INIT_NAME( defusedxml$ElementTree ), 0, 0,  },
    { "defusedxml.cElementTree", MOD_INIT_NAME( defusedxml$cElementTree ), 0, 0,  },
    { "defusedxml.common", MOD_INIT_NAME( defusedxml$common ), 0, 0,  },
    { "defusedxml.expatbuilder", MOD_INIT_NAME( defusedxml$expatbuilder ), 0, 0,  },
    { "defusedxml.expatreader", MOD_INIT_NAME( defusedxml$expatreader ), 0, 0,  },
    { "defusedxml.minidom", MOD_INIT_NAME( defusedxml$minidom ), 0, 0,  },
    { "defusedxml.pulldom", MOD_INIT_NAME( defusedxml$pulldom ), 0, 0,  },
    { "defusedxml.sax", MOD_INIT_NAME( defusedxml$sax ), 0, 0,  },
    { "defusedxml.xmlrpc", MOD_INIT_NAME( defusedxml$xmlrpc ), 0, 0,  },
    { "difflib", NULL, 7644198, 59471, NUITKA_BYTECODE_FLAG },
    { "dis", NULL, 7703669, 15238, NUITKA_BYTECODE_FLAG },
    { "distutils", NULL, 7718907, 431, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "distutils.ccompiler", NULL, 7719338, 33249, NUITKA_BYTECODE_FLAG },
    { "distutils.cmd", NULL, 7752587, 13941, NUITKA_BYTECODE_FLAG },
    { "distutils.command", NULL, 7766528, 588, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "distutils.command.bdist", NULL, 7767116, 3687, NUITKA_BYTECODE_FLAG },
    { "distutils.command.bdist_rpm", NULL, 7770803, 12527, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build", NULL, 7783330, 3868, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_clib", NULL, 7787198, 4918, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_ext", NULL, 7792116, 15825, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_py", NULL, 7807941, 10424, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_scripts", NULL, 7818365, 4323, NUITKA_BYTECODE_FLAG },
    { "distutils.command.config", NULL, 7822688, 10229, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install", NULL, 7832917, 13552, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_data", NULL, 7846469, 2318, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_headers", NULL, 7848787, 1733, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_scripts", NULL, 7850520, 2175, NUITKA_BYTECODE_FLAG },
    { "distutils.command.sdist", NULL, 7852695, 14541, NUITKA_BYTECODE_FLAG },
    { "distutils.core", NULL, 7867236, 6643, NUITKA_BYTECODE_FLAG },
    { "distutils.cygwinccompiler", NULL, 7873879, 8561, NUITKA_BYTECODE_FLAG },
    { "distutils.debug", NULL, 7882440, 241, NUITKA_BYTECODE_FLAG },
    { "distutils.dep_util", NULL, 7882681, 2757, NUITKA_BYTECODE_FLAG },
    { "distutils.dir_util", NULL, 7885438, 5851, NUITKA_BYTECODE_FLAG },
    { "distutils.dist", NULL, 7891289, 34472, NUITKA_BYTECODE_FLAG },
    { "distutils.errors", NULL, 7925761, 5527, NUITKA_BYTECODE_FLAG },
    { "distutils.extension", NULL, 7931288, 6938, NUITKA_BYTECODE_FLAG },
    { "distutils.fancy_getopt", NULL, 7938226, 10650, NUITKA_BYTECODE_FLAG },
    { "distutils.file_util", NULL, 7948876, 5936, NUITKA_BYTECODE_FLAG },
    { "distutils.filelist", NULL, 7954812, 9871, NUITKA_BYTECODE_FLAG },
    { "distutils.log", NULL, 7964683, 2352, NUITKA_BYTECODE_FLAG },
    { "distutils.msvccompiler", NULL, 7967035, 14604, NUITKA_BYTECODE_FLAG },
    { "distutils.spawn", NULL, 7981639, 5042, NUITKA_BYTECODE_FLAG },
    { "distutils.sysconfig", NULL, 7986681, 12016, NUITKA_BYTECODE_FLAG },
    { "distutils.unixccompiler", NULL, 7998697, 6573, NUITKA_BYTECODE_FLAG },
    { "distutils.util", NULL, 8005270, 15134, NUITKA_BYTECODE_FLAG },
    { "distutils.version", NULL, 8020404, 7389, NUITKA_BYTECODE_FLAG },
    { "doctest", NULL, 8027793, 75467, NUITKA_BYTECODE_FLAG },
    { "dsl", MOD_INIT_NAME( dsl ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "dummy_threading", NULL, 8103260, 1157, NUITKA_BYTECODE_FLAG },
    { "email", NULL, 8104417, 1724, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "email.parser", NULL, 8106141, 5780, NUITKA_BYTECODE_FLAG },
    { "email.utils", NULL, 8111921, 9500, NUITKA_BYTECODE_FLAG },
    { "encodings", NULL, 8121421, 3980, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "encodings.idna", NULL, 8125401, 5757, NUITKA_BYTECODE_FLAG },
    { "entrypoints", MOD_INIT_NAME( entrypoints ), 0, 0,  },
    { "enum", NULL, 8131158, 24304, NUITKA_BYTECODE_FLAG },
    { "extractors", MOD_INIT_NAME( extractors ), 0, 0,  },
    { "fcntl", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "filecmp", NULL, 8155462, 8340, NUITKA_BYTECODE_FLAG },
    { "fileinput", NULL, 8163802, 13231, NUITKA_BYTECODE_FLAG },
    { "fnmatch", NULL, 8177033, 3370, NUITKA_BYTECODE_FLAG },
    { "fractions", NULL, 8180403, 18461, NUITKA_BYTECODE_FLAG },
    { "functools", NULL, 8198864, 23988, NUITKA_BYTECODE_FLAG },
    { "getopt", NULL, 8222852, 6272, NUITKA_BYTECODE_FLAG },
    { "getpass", NULL, 8229124, 4197, NUITKA_BYTECODE_FLAG },
    { "gettext", NULL, 8233321, 14201, NUITKA_BYTECODE_FLAG },
    { "glob", NULL, 8247522, 4292, NUITKA_BYTECODE_FLAG },
    { "grammars", MOD_INIT_NAME( grammars ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "grammars.address", MOD_INIT_NAME( grammars$address ), 0, 0,  },
    { "grammars.date", MOD_INIT_NAME( grammars$date ), 0, 0,  },
    { "grammars.location", MOD_INIT_NAME( grammars$location ), 0, 0,  },
    { "grammars.money", MOD_INIT_NAME( grammars$money ), 0, 0,  },
    { "grammars.name", MOD_INIT_NAME( grammars$name ), 0, 0,  },
    { "grammars.organisation", MOD_INIT_NAME( grammars$organisation ), 0, 0,  },
    { "grammars.person", MOD_INIT_NAME( grammars$person ), 0, 0,  },
    { "grp", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "gzip", NULL, 8251814, 17218, NUITKA_BYTECODE_FLAG },
    { "hashlib", NULL, 8269032, 6624, NUITKA_BYTECODE_FLAG },
    { "heapq", NULL, 8275656, 14395, NUITKA_BYTECODE_FLAG },
    { "hmac", NULL, 8290051, 6146, NUITKA_BYTECODE_FLAG },
    { "html", NULL, 8296197, 3430, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "html.entities", NULL, 8299627, 50502, NUITKA_BYTECODE_FLAG },
    { "html.parser", NULL, 8350129, 11140, NUITKA_BYTECODE_FLAG },
    { "http", NULL, 8361269, 5955, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "http.client", NULL, 8367224, 34081, NUITKA_BYTECODE_FLAG },
    { "http.cookies", NULL, 8401305, 15277, NUITKA_BYTECODE_FLAG },
    { "idna", MOD_INIT_NAME( idna ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "idna.core", MOD_INIT_NAME( idna$core ), 0, 0,  },
    { "idna.idnadata", MOD_INIT_NAME( idna$idnadata ), 0, 0,  },
    { "idna.intranges", MOD_INIT_NAME( idna$intranges ), 0, 0,  },
    { "idna.package_data", MOD_INIT_NAME( idna$package_data ), 0, 0,  },
    { "idna.uts46data", MOD_INIT_NAME( idna$uts46data ), 0, 0,  },
    { "imp", NULL, 8416582, 9786, NUITKA_BYTECODE_FLAG },
    { "importlib", NULL, 8426368, 3765, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "importlib._bootstrap", NULL, 8430133, 29211, NUITKA_BYTECODE_FLAG },
    { "importlib.machinery", NULL, 8459344, 1005, NUITKA_BYTECODE_FLAG },
    { "importlib.util", NULL, 8460349, 9389, NUITKA_BYTECODE_FLAG },
    { "inspect", NULL, 8469738, 80065, NUITKA_BYTECODE_FLAG },
    { "io", NULL, 8549803, 3442, NUITKA_BYTECODE_FLAG },
    { "ipaddress", NULL, 8553245, 63027, NUITKA_BYTECODE_FLAG },
    { "ipykernel", MOD_INIT_NAME( ipykernel ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ipykernel._eventloop_macos", MOD_INIT_NAME( ipykernel$_eventloop_macos ), 0, 0,  },
    { "ipykernel._version", MOD_INIT_NAME( ipykernel$_version ), 0, 0,  },
    { "ipykernel.codeutil", MOD_INIT_NAME( ipykernel$codeutil ), 0, 0,  },
    { "ipykernel.comm", MOD_INIT_NAME( ipykernel$comm ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ipykernel.comm.comm", MOD_INIT_NAME( ipykernel$comm$comm ), 0, 0,  },
    { "ipykernel.comm.manager", MOD_INIT_NAME( ipykernel$comm$manager ), 0, 0,  },
    { "ipykernel.connect", MOD_INIT_NAME( ipykernel$connect ), 0, 0,  },
    { "ipykernel.displayhook", MOD_INIT_NAME( ipykernel$displayhook ), 0, 0,  },
    { "ipykernel.embed", MOD_INIT_NAME( ipykernel$embed ), 0, 0,  },
    { "ipykernel.eventloops", MOD_INIT_NAME( ipykernel$eventloops ), 0, 0,  },
    { "ipykernel.gui", MOD_INIT_NAME( ipykernel$gui ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ipykernel.gui.gtk3embed", MOD_INIT_NAME( ipykernel$gui$gtk3embed ), 0, 0,  },
    { "ipykernel.gui.gtkembed", MOD_INIT_NAME( ipykernel$gui$gtkembed ), 0, 0,  },
    { "ipykernel.heartbeat", MOD_INIT_NAME( ipykernel$heartbeat ), 0, 0,  },
    { "ipykernel.inprocess", MOD_INIT_NAME( ipykernel$inprocess ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ipykernel.inprocess.blocking", MOD_INIT_NAME( ipykernel$inprocess$blocking ), 0, 0,  },
    { "ipykernel.inprocess.channels", MOD_INIT_NAME( ipykernel$inprocess$channels ), 0, 0,  },
    { "ipykernel.inprocess.client", MOD_INIT_NAME( ipykernel$inprocess$client ), 0, 0,  },
    { "ipykernel.inprocess.constants", MOD_INIT_NAME( ipykernel$inprocess$constants ), 0, 0,  },
    { "ipykernel.inprocess.ipkernel", MOD_INIT_NAME( ipykernel$inprocess$ipkernel ), 0, 0,  },
    { "ipykernel.inprocess.manager", MOD_INIT_NAME( ipykernel$inprocess$manager ), 0, 0,  },
    { "ipykernel.inprocess.socket", MOD_INIT_NAME( ipykernel$inprocess$socket ), 0, 0,  },
    { "ipykernel.iostream", MOD_INIT_NAME( ipykernel$iostream ), 0, 0,  },
    { "ipykernel.ipkernel", MOD_INIT_NAME( ipykernel$ipkernel ), 0, 0,  },
    { "ipykernel.jsonutil", MOD_INIT_NAME( ipykernel$jsonutil ), 0, 0,  },
    { "ipykernel.kernelapp", MOD_INIT_NAME( ipykernel$kernelapp ), 0, 0,  },
    { "ipykernel.kernelbase", MOD_INIT_NAME( ipykernel$kernelbase ), 0, 0,  },
    { "ipykernel.kernelspec", MOD_INIT_NAME( ipykernel$kernelspec ), 0, 0,  },
    { "ipykernel.parentpoller", MOD_INIT_NAME( ipykernel$parentpoller ), 0, 0,  },
    { "ipykernel.pickleutil", MOD_INIT_NAME( ipykernel$pickleutil ), 0, 0,  },
    { "ipykernel.pylab", MOD_INIT_NAME( ipykernel$pylab ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ipykernel.pylab.backend_inline", MOD_INIT_NAME( ipykernel$pylab$backend_inline ), 0, 0,  },
    { "ipykernel.pylab.config", MOD_INIT_NAME( ipykernel$pylab$config ), 0, 0,  },
    { "ipykernel.serialize", MOD_INIT_NAME( ipykernel$serialize ), 0, 0,  },
    { "ipykernel.zmqshell", MOD_INIT_NAME( ipykernel$zmqshell ), 0, 0,  },
    { "ipython_genutils", MOD_INIT_NAME( ipython_genutils ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ipython_genutils._version", MOD_INIT_NAME( ipython_genutils$_version ), 0, 0,  },
    { "ipython_genutils.encoding", MOD_INIT_NAME( ipython_genutils$encoding ), 0, 0,  },
    { "ipython_genutils.importstring", MOD_INIT_NAME( ipython_genutils$importstring ), 0, 0,  },
    { "ipython_genutils.ipstruct", MOD_INIT_NAME( ipython_genutils$ipstruct ), 0, 0,  },
    { "ipython_genutils.path", MOD_INIT_NAME( ipython_genutils$path ), 0, 0,  },
    { "ipython_genutils.py3compat", MOD_INIT_NAME( ipython_genutils$py3compat ), 0, 0,  },
    { "ipython_genutils.tempdir", MOD_INIT_NAME( ipython_genutils$tempdir ), 0, 0,  },
    { "ipython_genutils.text", MOD_INIT_NAME( ipython_genutils$text ), 0, 0,  },
    { "jedi", MOD_INIT_NAME( jedi ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi._compatibility", MOD_INIT_NAME( jedi$_compatibility ), 0, 0,  },
    { "jedi.api", MOD_INIT_NAME( jedi$api ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi.api.classes", MOD_INIT_NAME( jedi$api$classes ), 0, 0,  },
    { "jedi.api.completion", MOD_INIT_NAME( jedi$api$completion ), 0, 0,  },
    { "jedi.api.environment", MOD_INIT_NAME( jedi$api$environment ), 0, 0,  },
    { "jedi.api.exceptions", MOD_INIT_NAME( jedi$api$exceptions ), 0, 0,  },
    { "jedi.api.helpers", MOD_INIT_NAME( jedi$api$helpers ), 0, 0,  },
    { "jedi.api.interpreter", MOD_INIT_NAME( jedi$api$interpreter ), 0, 0,  },
    { "jedi.api.keywords", MOD_INIT_NAME( jedi$api$keywords ), 0, 0,  },
    { "jedi.api.project", MOD_INIT_NAME( jedi$api$project ), 0, 0,  },
    { "jedi.cache", MOD_INIT_NAME( jedi$cache ), 0, 0,  },
    { "jedi.common", MOD_INIT_NAME( jedi$common ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi.common.context", MOD_INIT_NAME( jedi$common$context ), 0, 0,  },
    { "jedi.common.utils", MOD_INIT_NAME( jedi$common$utils ), 0, 0,  },
    { "jedi.debug", MOD_INIT_NAME( jedi$debug ), 0, 0,  },
    { "jedi.evaluate", MOD_INIT_NAME( jedi$evaluate ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi.evaluate.analysis", MOD_INIT_NAME( jedi$evaluate$analysis ), 0, 0,  },
    { "jedi.evaluate.arguments", MOD_INIT_NAME( jedi$evaluate$arguments ), 0, 0,  },
    { "jedi.evaluate.base_context", MOD_INIT_NAME( jedi$evaluate$base_context ), 0, 0,  },
    { "jedi.evaluate.cache", MOD_INIT_NAME( jedi$evaluate$cache ), 0, 0,  },
    { "jedi.evaluate.compiled", MOD_INIT_NAME( jedi$evaluate$compiled ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi.evaluate.compiled.access", MOD_INIT_NAME( jedi$evaluate$compiled$access ), 0, 0,  },
    { "jedi.evaluate.compiled.context", MOD_INIT_NAME( jedi$evaluate$compiled$context ), 0, 0,  },
    { "jedi.evaluate.compiled.fake", MOD_INIT_NAME( jedi$evaluate$compiled$fake ), 0, 0,  },
    { "jedi.evaluate.compiled.getattr_static", MOD_INIT_NAME( jedi$evaluate$compiled$getattr_static ), 0, 0,  },
    { "jedi.evaluate.compiled.mixed", MOD_INIT_NAME( jedi$evaluate$compiled$mixed ), 0, 0,  },
    { "jedi.evaluate.compiled.subprocess", MOD_INIT_NAME( jedi$evaluate$compiled$subprocess ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi.evaluate.compiled.subprocess.functions", MOD_INIT_NAME( jedi$evaluate$compiled$subprocess$functions ), 0, 0,  },
    { "jedi.evaluate.context", MOD_INIT_NAME( jedi$evaluate$context ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jedi.evaluate.context.asynchronous", MOD_INIT_NAME( jedi$evaluate$context$asynchronous ), 0, 0,  },
    { "jedi.evaluate.context.function", MOD_INIT_NAME( jedi$evaluate$context$function ), 0, 0,  },
    { "jedi.evaluate.context.instance", MOD_INIT_NAME( jedi$evaluate$context$instance ), 0, 0,  },
    { "jedi.evaluate.context.iterable", MOD_INIT_NAME( jedi$evaluate$context$iterable ), 0, 0,  },
    { "jedi.evaluate.context.klass", MOD_INIT_NAME( jedi$evaluate$context$klass ), 0, 0,  },
    { "jedi.evaluate.context.module", MOD_INIT_NAME( jedi$evaluate$context$module ), 0, 0,  },
    { "jedi.evaluate.context.namespace", MOD_INIT_NAME( jedi$evaluate$context$namespace ), 0, 0,  },
    { "jedi.evaluate.docstrings", MOD_INIT_NAME( jedi$evaluate$docstrings ), 0, 0,  },
    { "jedi.evaluate.dynamic", MOD_INIT_NAME( jedi$evaluate$dynamic ), 0, 0,  },
    { "jedi.evaluate.filters", MOD_INIT_NAME( jedi$evaluate$filters ), 0, 0,  },
    { "jedi.evaluate.finder", MOD_INIT_NAME( jedi$evaluate$finder ), 0, 0,  },
    { "jedi.evaluate.flow_analysis", MOD_INIT_NAME( jedi$evaluate$flow_analysis ), 0, 0,  },
    { "jedi.evaluate.helpers", MOD_INIT_NAME( jedi$evaluate$helpers ), 0, 0,  },
    { "jedi.evaluate.imports", MOD_INIT_NAME( jedi$evaluate$imports ), 0, 0,  },
    { "jedi.evaluate.lazy_context", MOD_INIT_NAME( jedi$evaluate$lazy_context ), 0, 0,  },
    { "jedi.evaluate.param", MOD_INIT_NAME( jedi$evaluate$param ), 0, 0,  },
    { "jedi.evaluate.parser_cache", MOD_INIT_NAME( jedi$evaluate$parser_cache ), 0, 0,  },
    { "jedi.evaluate.pep0484", MOD_INIT_NAME( jedi$evaluate$pep0484 ), 0, 0,  },
    { "jedi.evaluate.recursion", MOD_INIT_NAME( jedi$evaluate$recursion ), 0, 0,  },
    { "jedi.evaluate.stdlib", MOD_INIT_NAME( jedi$evaluate$stdlib ), 0, 0,  },
    { "jedi.evaluate.syntax_tree", MOD_INIT_NAME( jedi$evaluate$syntax_tree ), 0, 0,  },
    { "jedi.evaluate.sys_path", MOD_INIT_NAME( jedi$evaluate$sys_path ), 0, 0,  },
    { "jedi.evaluate.usages", MOD_INIT_NAME( jedi$evaluate$usages ), 0, 0,  },
    { "jedi.evaluate.utils", MOD_INIT_NAME( jedi$evaluate$utils ), 0, 0,  },
    { "jedi.parser_utils", MOD_INIT_NAME( jedi$parser_utils ), 0, 0,  },
    { "jedi.settings", MOD_INIT_NAME( jedi$settings ), 0, 0,  },
    { "jinja2", MOD_INIT_NAME( jinja2 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jinja2._compat", MOD_INIT_NAME( jinja2$_compat ), 0, 0,  },
    { "jinja2._identifier", MOD_INIT_NAME( jinja2$_identifier ), 0, 0,  },
    { "jinja2.asyncfilters", MOD_INIT_NAME( jinja2$asyncfilters ), 0, 0,  },
    { "jinja2.asyncsupport", MOD_INIT_NAME( jinja2$asyncsupport ), 0, 0,  },
    { "jinja2.bccache", MOD_INIT_NAME( jinja2$bccache ), 0, 0,  },
    { "jinja2.compiler", MOD_INIT_NAME( jinja2$compiler ), 0, 0,  },
    { "jinja2.constants", MOD_INIT_NAME( jinja2$constants ), 0, 0,  },
    { "jinja2.debug", MOD_INIT_NAME( jinja2$debug ), 0, 0,  },
    { "jinja2.defaults", MOD_INIT_NAME( jinja2$defaults ), 0, 0,  },
    { "jinja2.environment", MOD_INIT_NAME( jinja2$environment ), 0, 0,  },
    { "jinja2.exceptions", MOD_INIT_NAME( jinja2$exceptions ), 0, 0,  },
    { "jinja2.filters", MOD_INIT_NAME( jinja2$filters ), 0, 0,  },
    { "jinja2.idtracking", MOD_INIT_NAME( jinja2$idtracking ), 0, 0,  },
    { "jinja2.lexer", MOD_INIT_NAME( jinja2$lexer ), 0, 0,  },
    { "jinja2.loaders", MOD_INIT_NAME( jinja2$loaders ), 0, 0,  },
    { "jinja2.nodes", MOD_INIT_NAME( jinja2$nodes ), 0, 0,  },
    { "jinja2.optimizer", MOD_INIT_NAME( jinja2$optimizer ), 0, 0,  },
    { "jinja2.parser", MOD_INIT_NAME( jinja2$parser ), 0, 0,  },
    { "jinja2.runtime", MOD_INIT_NAME( jinja2$runtime ), 0, 0,  },
    { "jinja2.tests", MOD_INIT_NAME( jinja2$tests ), 0, 0,  },
    { "jinja2.utils", MOD_INIT_NAME( jinja2$utils ), 0, 0,  },
    { "jinja2.visitor", MOD_INIT_NAME( jinja2$visitor ), 0, 0,  },
    { "json", NULL, 8616272, 12371, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "jsonschema", MOD_INIT_NAME( jsonschema ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jsonschema._format", MOD_INIT_NAME( jsonschema$_format ), 0, 0,  },
    { "jsonschema._legacy_validators", MOD_INIT_NAME( jsonschema$_legacy_validators ), 0, 0,  },
    { "jsonschema._types", MOD_INIT_NAME( jsonschema$_types ), 0, 0,  },
    { "jsonschema._utils", MOD_INIT_NAME( jsonschema$_utils ), 0, 0,  },
    { "jsonschema._validators", MOD_INIT_NAME( jsonschema$_validators ), 0, 0,  },
    { "jsonschema.compat", MOD_INIT_NAME( jsonschema$compat ), 0, 0,  },
    { "jsonschema.exceptions", MOD_INIT_NAME( jsonschema$exceptions ), 0, 0,  },
    { "jsonschema.validators", MOD_INIT_NAME( jsonschema$validators ), 0, 0,  },
    { "jupyter_client", MOD_INIT_NAME( jupyter_client ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jupyter_client._version", MOD_INIT_NAME( jupyter_client$_version ), 0, 0,  },
    { "jupyter_client.adapter", MOD_INIT_NAME( jupyter_client$adapter ), 0, 0,  },
    { "jupyter_client.blocking", MOD_INIT_NAME( jupyter_client$blocking ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jupyter_client.blocking.channels", MOD_INIT_NAME( jupyter_client$blocking$channels ), 0, 0,  },
    { "jupyter_client.blocking.client", MOD_INIT_NAME( jupyter_client$blocking$client ), 0, 0,  },
    { "jupyter_client.channels", MOD_INIT_NAME( jupyter_client$channels ), 0, 0,  },
    { "jupyter_client.channelsabc", MOD_INIT_NAME( jupyter_client$channelsabc ), 0, 0,  },
    { "jupyter_client.client", MOD_INIT_NAME( jupyter_client$client ), 0, 0,  },
    { "jupyter_client.clientabc", MOD_INIT_NAME( jupyter_client$clientabc ), 0, 0,  },
    { "jupyter_client.connect", MOD_INIT_NAME( jupyter_client$connect ), 0, 0,  },
    { "jupyter_client.ioloop", MOD_INIT_NAME( jupyter_client$ioloop ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jupyter_client.ioloop.manager", MOD_INIT_NAME( jupyter_client$ioloop$manager ), 0, 0,  },
    { "jupyter_client.ioloop.restarter", MOD_INIT_NAME( jupyter_client$ioloop$restarter ), 0, 0,  },
    { "jupyter_client.jsonutil", MOD_INIT_NAME( jupyter_client$jsonutil ), 0, 0,  },
    { "jupyter_client.kernelspec", MOD_INIT_NAME( jupyter_client$kernelspec ), 0, 0,  },
    { "jupyter_client.launcher", MOD_INIT_NAME( jupyter_client$launcher ), 0, 0,  },
    { "jupyter_client.localinterfaces", MOD_INIT_NAME( jupyter_client$localinterfaces ), 0, 0,  },
    { "jupyter_client.manager", MOD_INIT_NAME( jupyter_client$manager ), 0, 0,  },
    { "jupyter_client.managerabc", MOD_INIT_NAME( jupyter_client$managerabc ), 0, 0,  },
    { "jupyter_client.multikernelmanager", MOD_INIT_NAME( jupyter_client$multikernelmanager ), 0, 0,  },
    { "jupyter_client.restarter", MOD_INIT_NAME( jupyter_client$restarter ), 0, 0,  },
    { "jupyter_client.session", MOD_INIT_NAME( jupyter_client$session ), 0, 0,  },
    { "jupyter_client.win_interrupt", MOD_INIT_NAME( jupyter_client$win_interrupt ), 0, 0,  },
    { "jupyter_core", MOD_INIT_NAME( jupyter_core ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jupyter_core.application", MOD_INIT_NAME( jupyter_core$application ), 0, 0,  },
    { "jupyter_core.migrate", MOD_INIT_NAME( jupyter_core$migrate ), 0, 0,  },
    { "jupyter_core.paths", MOD_INIT_NAME( jupyter_core$paths ), 0, 0,  },
    { "jupyter_core.utils", MOD_INIT_NAME( jupyter_core$utils ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "jupyter_core.utils.shutil_which", MOD_INIT_NAME( jupyter_core$utils$shutil_which ), 0, 0,  },
    { "jupyter_core.version", MOD_INIT_NAME( jupyter_core$version ), 0, 0,  },
    { "keyword", NULL, 8628643, 1842, NUITKA_BYTECODE_FLAG },
    { "kiwisolver", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "linecache", NULL, 8630485, 3822, NUITKA_BYTECODE_FLAG },
    { "locale", NULL, 8634307, 34588, NUITKA_BYTECODE_FLAG },
    { "logging", NULL, 8668895, 62591, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "logging.handlers", NULL, 8731486, 43016, NUITKA_BYTECODE_FLAG },
    { "lxml", MOD_INIT_NAME( lxml ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "lxml._elementpath", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "lxml.etree", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "lzma", NULL, 8774502, 11972, NUITKA_BYTECODE_FLAG },
    { "markup", MOD_INIT_NAME( markup ), 0, 0,  },
    { "markupsafe", MOD_INIT_NAME( markupsafe ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "markupsafe._compat", MOD_INIT_NAME( markupsafe$_compat ), 0, 0,  },
    { "markupsafe._constants", MOD_INIT_NAME( markupsafe$_constants ), 0, 0,  },
    { "markupsafe._native", MOD_INIT_NAME( markupsafe$_native ), 0, 0,  },
    { "markupsafe._speedups", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "math", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib", MOD_INIT_NAME( matplotlib ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib._cm", MOD_INIT_NAME( matplotlib$_cm ), 0, 0,  },
    { "matplotlib._cm_listed", MOD_INIT_NAME( matplotlib$_cm_listed ), 0, 0,  },
    { "matplotlib._color_data", MOD_INIT_NAME( matplotlib$_color_data ), 0, 0,  },
    { "matplotlib._constrained_layout", MOD_INIT_NAME( matplotlib$_constrained_layout ), 0, 0,  },
    { "matplotlib._contour", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib._image", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib._layoutbox", MOD_INIT_NAME( matplotlib$_layoutbox ), 0, 0,  },
    { "matplotlib._mathtext_data", MOD_INIT_NAME( matplotlib$_mathtext_data ), 0, 0,  },
    { "matplotlib._path", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib._png", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib._pylab_helpers", MOD_INIT_NAME( matplotlib$_pylab_helpers ), 0, 0,  },
    { "matplotlib._qhull", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib._tri", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib._version", MOD_INIT_NAME( matplotlib$_version ), 0, 0,  },
    { "matplotlib.afm", MOD_INIT_NAME( matplotlib$afm ), 0, 0,  },
    { "matplotlib.artist", MOD_INIT_NAME( matplotlib$artist ), 0, 0,  },
    { "matplotlib.axes", MOD_INIT_NAME( matplotlib$axes ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib.axes._axes", MOD_INIT_NAME( matplotlib$axes$_axes ), 0, 0,  },
    { "matplotlib.axes._base", MOD_INIT_NAME( matplotlib$axes$_base ), 0, 0,  },
    { "matplotlib.axes._secondary_axes", MOD_INIT_NAME( matplotlib$axes$_secondary_axes ), 0, 0,  },
    { "matplotlib.axes._subplots", MOD_INIT_NAME( matplotlib$axes$_subplots ), 0, 0,  },
    { "matplotlib.axis", MOD_INIT_NAME( matplotlib$axis ), 0, 0,  },
    { "matplotlib.backend_bases", MOD_INIT_NAME( matplotlib$backend_bases ), 0, 0,  },
    { "matplotlib.backend_managers", MOD_INIT_NAME( matplotlib$backend_managers ), 0, 0,  },
    { "matplotlib.backend_tools", MOD_INIT_NAME( matplotlib$backend_tools ), 0, 0,  },
    { "matplotlib.backends", MOD_INIT_NAME( matplotlib$backends ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib.backends._backend_agg", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib.backends._backend_tk", MOD_INIT_NAME( matplotlib$backends$_backend_tk ), 0, 0,  },
    { "matplotlib.backends._tkagg", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib.backends.backend_agg", MOD_INIT_NAME( matplotlib$backends$backend_agg ), 0, 0,  },
    { "matplotlib.backends.backend_tkagg", MOD_INIT_NAME( matplotlib$backends$backend_tkagg ), 0, 0,  },
    { "matplotlib.backends.backend_webagg", MOD_INIT_NAME( matplotlib$backends$backend_webagg ), 0, 0,  },
    { "matplotlib.backends.backend_webagg_core", MOD_INIT_NAME( matplotlib$backends$backend_webagg_core ), 0, 0,  },
    { "matplotlib.bezier", MOD_INIT_NAME( matplotlib$bezier ), 0, 0,  },
    { "matplotlib.blocking_input", MOD_INIT_NAME( matplotlib$blocking_input ), 0, 0,  },
    { "matplotlib.category", MOD_INIT_NAME( matplotlib$category ), 0, 0,  },
    { "matplotlib.cbook", MOD_INIT_NAME( matplotlib$cbook ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib.cbook.deprecation", MOD_INIT_NAME( matplotlib$cbook$deprecation ), 0, 0,  },
    { "matplotlib.cm", MOD_INIT_NAME( matplotlib$cm ), 0, 0,  },
    { "matplotlib.collections", MOD_INIT_NAME( matplotlib$collections ), 0, 0,  },
    { "matplotlib.colorbar", MOD_INIT_NAME( matplotlib$colorbar ), 0, 0,  },
    { "matplotlib.colors", MOD_INIT_NAME( matplotlib$colors ), 0, 0,  },
    { "matplotlib.container", MOD_INIT_NAME( matplotlib$container ), 0, 0,  },
    { "matplotlib.contour", MOD_INIT_NAME( matplotlib$contour ), 0, 0,  },
    { "matplotlib.dates", MOD_INIT_NAME( matplotlib$dates ), 0, 0,  },
    { "matplotlib.docstring", MOD_INIT_NAME( matplotlib$docstring ), 0, 0,  },
    { "matplotlib.dviread", MOD_INIT_NAME( matplotlib$dviread ), 0, 0,  },
    { "matplotlib.figure", MOD_INIT_NAME( matplotlib$figure ), 0, 0,  },
    { "matplotlib.font_manager", MOD_INIT_NAME( matplotlib$font_manager ), 0, 0,  },
    { "matplotlib.fontconfig_pattern", MOD_INIT_NAME( matplotlib$fontconfig_pattern ), 0, 0,  },
    { "matplotlib.ft2font", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "matplotlib.gridspec", MOD_INIT_NAME( matplotlib$gridspec ), 0, 0,  },
    { "matplotlib.hatch", MOD_INIT_NAME( matplotlib$hatch ), 0, 0,  },
    { "matplotlib.image", MOD_INIT_NAME( matplotlib$image ), 0, 0,  },
    { "matplotlib.legend", MOD_INIT_NAME( matplotlib$legend ), 0, 0,  },
    { "matplotlib.legend_handler", MOD_INIT_NAME( matplotlib$legend_handler ), 0, 0,  },
    { "matplotlib.lines", MOD_INIT_NAME( matplotlib$lines ), 0, 0,  },
    { "matplotlib.markers", MOD_INIT_NAME( matplotlib$markers ), 0, 0,  },
    { "matplotlib.mathtext", MOD_INIT_NAME( matplotlib$mathtext ), 0, 0,  },
    { "matplotlib.mlab", MOD_INIT_NAME( matplotlib$mlab ), 0, 0,  },
    { "matplotlib.offsetbox", MOD_INIT_NAME( matplotlib$offsetbox ), 0, 0,  },
    { "matplotlib.patches", MOD_INIT_NAME( matplotlib$patches ), 0, 0,  },
    { "matplotlib.path", MOD_INIT_NAME( matplotlib$path ), 0, 0,  },
    { "matplotlib.patheffects", MOD_INIT_NAME( matplotlib$patheffects ), 0, 0,  },
    { "matplotlib.projections", MOD_INIT_NAME( matplotlib$projections ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib.projections.geo", MOD_INIT_NAME( matplotlib$projections$geo ), 0, 0,  },
    { "matplotlib.projections.polar", MOD_INIT_NAME( matplotlib$projections$polar ), 0, 0,  },
    { "matplotlib.pyplot", MOD_INIT_NAME( matplotlib$pyplot ), 0, 0,  },
    { "matplotlib.quiver", MOD_INIT_NAME( matplotlib$quiver ), 0, 0,  },
    { "matplotlib.rcsetup", MOD_INIT_NAME( matplotlib$rcsetup ), 0, 0,  },
    { "matplotlib.scale", MOD_INIT_NAME( matplotlib$scale ), 0, 0,  },
    { "matplotlib.spines", MOD_INIT_NAME( matplotlib$spines ), 0, 0,  },
    { "matplotlib.stackplot", MOD_INIT_NAME( matplotlib$stackplot ), 0, 0,  },
    { "matplotlib.streamplot", MOD_INIT_NAME( matplotlib$streamplot ), 0, 0,  },
    { "matplotlib.style", MOD_INIT_NAME( matplotlib$style ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib.style.core", MOD_INIT_NAME( matplotlib$style$core ), 0, 0,  },
    { "matplotlib.table", MOD_INIT_NAME( matplotlib$table ), 0, 0,  },
    { "matplotlib.texmanager", MOD_INIT_NAME( matplotlib$texmanager ), 0, 0,  },
    { "matplotlib.text", MOD_INIT_NAME( matplotlib$text ), 0, 0,  },
    { "matplotlib.textpath", MOD_INIT_NAME( matplotlib$textpath ), 0, 0,  },
    { "matplotlib.ticker", MOD_INIT_NAME( matplotlib$ticker ), 0, 0,  },
    { "matplotlib.tight_bbox", MOD_INIT_NAME( matplotlib$tight_bbox ), 0, 0,  },
    { "matplotlib.tight_layout", MOD_INIT_NAME( matplotlib$tight_layout ), 0, 0,  },
    { "matplotlib.transforms", MOD_INIT_NAME( matplotlib$transforms ), 0, 0,  },
    { "matplotlib.tri", MOD_INIT_NAME( matplotlib$tri ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "matplotlib.tri.triangulation", MOD_INIT_NAME( matplotlib$tri$triangulation ), 0, 0,  },
    { "matplotlib.tri.tricontour", MOD_INIT_NAME( matplotlib$tri$tricontour ), 0, 0,  },
    { "matplotlib.tri.trifinder", MOD_INIT_NAME( matplotlib$tri$trifinder ), 0, 0,  },
    { "matplotlib.tri.triinterpolate", MOD_INIT_NAME( matplotlib$tri$triinterpolate ), 0, 0,  },
    { "matplotlib.tri.tripcolor", MOD_INIT_NAME( matplotlib$tri$tripcolor ), 0, 0,  },
    { "matplotlib.tri.triplot", MOD_INIT_NAME( matplotlib$tri$triplot ), 0, 0,  },
    { "matplotlib.tri.trirefine", MOD_INIT_NAME( matplotlib$tri$trirefine ), 0, 0,  },
    { "matplotlib.tri.tritools", MOD_INIT_NAME( matplotlib$tri$tritools ), 0, 0,  },
    { "matplotlib.units", MOD_INIT_NAME( matplotlib$units ), 0, 0,  },
    { "matplotlib.widgets", MOD_INIT_NAME( matplotlib$widgets ), 0, 0,  },
    { "mimetypes", NULL, 8786474, 15511, NUITKA_BYTECODE_FLAG },
    { "mistune", MOD_INIT_NAME( mistune ), 0, 0,  },
    { "mmap", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "multiprocessing", NULL, 8801985, 560, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "multiprocessing.pool", NULL, 8802545, 21269, NUITKA_BYTECODE_FLAG },
    { "nbconvert", MOD_INIT_NAME( nbconvert ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert._version", MOD_INIT_NAME( nbconvert$_version ), 0, 0,  },
    { "nbconvert.exporters", MOD_INIT_NAME( nbconvert$exporters ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.exporters.asciidoc", MOD_INIT_NAME( nbconvert$exporters$asciidoc ), 0, 0,  },
    { "nbconvert.exporters.base", MOD_INIT_NAME( nbconvert$exporters$base ), 0, 0,  },
    { "nbconvert.exporters.exporter", MOD_INIT_NAME( nbconvert$exporters$exporter ), 0, 0,  },
    { "nbconvert.exporters.html", MOD_INIT_NAME( nbconvert$exporters$html ), 0, 0,  },
    { "nbconvert.exporters.latex", MOD_INIT_NAME( nbconvert$exporters$latex ), 0, 0,  },
    { "nbconvert.exporters.markdown", MOD_INIT_NAME( nbconvert$exporters$markdown ), 0, 0,  },
    { "nbconvert.exporters.notebook", MOD_INIT_NAME( nbconvert$exporters$notebook ), 0, 0,  },
    { "nbconvert.exporters.pdf", MOD_INIT_NAME( nbconvert$exporters$pdf ), 0, 0,  },
    { "nbconvert.exporters.python", MOD_INIT_NAME( nbconvert$exporters$python ), 0, 0,  },
    { "nbconvert.exporters.rst", MOD_INIT_NAME( nbconvert$exporters$rst ), 0, 0,  },
    { "nbconvert.exporters.script", MOD_INIT_NAME( nbconvert$exporters$script ), 0, 0,  },
    { "nbconvert.exporters.slides", MOD_INIT_NAME( nbconvert$exporters$slides ), 0, 0,  },
    { "nbconvert.exporters.templateexporter", MOD_INIT_NAME( nbconvert$exporters$templateexporter ), 0, 0,  },
    { "nbconvert.filters", MOD_INIT_NAME( nbconvert$filters ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.filters.ansi", MOD_INIT_NAME( nbconvert$filters$ansi ), 0, 0,  },
    { "nbconvert.filters.citation", MOD_INIT_NAME( nbconvert$filters$citation ), 0, 0,  },
    { "nbconvert.filters.datatypefilter", MOD_INIT_NAME( nbconvert$filters$datatypefilter ), 0, 0,  },
    { "nbconvert.filters.filter_links", MOD_INIT_NAME( nbconvert$filters$filter_links ), 0, 0,  },
    { "nbconvert.filters.highlight", MOD_INIT_NAME( nbconvert$filters$highlight ), 0, 0,  },
    { "nbconvert.filters.latex", MOD_INIT_NAME( nbconvert$filters$latex ), 0, 0,  },
    { "nbconvert.filters.markdown", MOD_INIT_NAME( nbconvert$filters$markdown ), 0, 0,  },
    { "nbconvert.filters.markdown_mistune", MOD_INIT_NAME( nbconvert$filters$markdown_mistune ), 0, 0,  },
    { "nbconvert.filters.metadata", MOD_INIT_NAME( nbconvert$filters$metadata ), 0, 0,  },
    { "nbconvert.filters.pandoc", MOD_INIT_NAME( nbconvert$filters$pandoc ), 0, 0,  },
    { "nbconvert.filters.strings", MOD_INIT_NAME( nbconvert$filters$strings ), 0, 0,  },
    { "nbconvert.postprocessors", MOD_INIT_NAME( nbconvert$postprocessors ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.postprocessors.base", MOD_INIT_NAME( nbconvert$postprocessors$base ), 0, 0,  },
    { "nbconvert.postprocessors.serve", MOD_INIT_NAME( nbconvert$postprocessors$serve ), 0, 0,  },
    { "nbconvert.preprocessors", MOD_INIT_NAME( nbconvert$preprocessors ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.preprocessors.base", MOD_INIT_NAME( nbconvert$preprocessors$base ), 0, 0,  },
    { "nbconvert.preprocessors.clearmetadata", MOD_INIT_NAME( nbconvert$preprocessors$clearmetadata ), 0, 0,  },
    { "nbconvert.preprocessors.clearoutput", MOD_INIT_NAME( nbconvert$preprocessors$clearoutput ), 0, 0,  },
    { "nbconvert.preprocessors.coalescestreams", MOD_INIT_NAME( nbconvert$preprocessors$coalescestreams ), 0, 0,  },
    { "nbconvert.preprocessors.convertfigures", MOD_INIT_NAME( nbconvert$preprocessors$convertfigures ), 0, 0,  },
    { "nbconvert.preprocessors.csshtmlheader", MOD_INIT_NAME( nbconvert$preprocessors$csshtmlheader ), 0, 0,  },
    { "nbconvert.preprocessors.execute", MOD_INIT_NAME( nbconvert$preprocessors$execute ), 0, 0,  },
    { "nbconvert.preprocessors.extractoutput", MOD_INIT_NAME( nbconvert$preprocessors$extractoutput ), 0, 0,  },
    { "nbconvert.preprocessors.highlightmagics", MOD_INIT_NAME( nbconvert$preprocessors$highlightmagics ), 0, 0,  },
    { "nbconvert.preprocessors.latex", MOD_INIT_NAME( nbconvert$preprocessors$latex ), 0, 0,  },
    { "nbconvert.preprocessors.regexremove", MOD_INIT_NAME( nbconvert$preprocessors$regexremove ), 0, 0,  },
    { "nbconvert.preprocessors.svg2pdf", MOD_INIT_NAME( nbconvert$preprocessors$svg2pdf ), 0, 0,  },
    { "nbconvert.preprocessors.tagremove", MOD_INIT_NAME( nbconvert$preprocessors$tagremove ), 0, 0,  },
    { "nbconvert.resources", MOD_INIT_NAME( nbconvert$resources ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.utils", MOD_INIT_NAME( nbconvert$utils ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.utils.base", MOD_INIT_NAME( nbconvert$utils$base ), 0, 0,  },
    { "nbconvert.utils.exceptions", MOD_INIT_NAME( nbconvert$utils$exceptions ), 0, 0,  },
    { "nbconvert.utils.io", MOD_INIT_NAME( nbconvert$utils$io ), 0, 0,  },
    { "nbconvert.utils.pandoc", MOD_INIT_NAME( nbconvert$utils$pandoc ), 0, 0,  },
    { "nbconvert.utils.version", MOD_INIT_NAME( nbconvert$utils$version ), 0, 0,  },
    { "nbconvert.writers", MOD_INIT_NAME( nbconvert$writers ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbconvert.writers.base", MOD_INIT_NAME( nbconvert$writers$base ), 0, 0,  },
    { "nbconvert.writers.debug", MOD_INIT_NAME( nbconvert$writers$debug ), 0, 0,  },
    { "nbconvert.writers.files", MOD_INIT_NAME( nbconvert$writers$files ), 0, 0,  },
    { "nbconvert.writers.stdout", MOD_INIT_NAME( nbconvert$writers$stdout ), 0, 0,  },
    { "nbformat", MOD_INIT_NAME( nbformat ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbformat._version", MOD_INIT_NAME( nbformat$_version ), 0, 0,  },
    { "nbformat.converter", MOD_INIT_NAME( nbformat$converter ), 0, 0,  },
    { "nbformat.notebooknode", MOD_INIT_NAME( nbformat$notebooknode ), 0, 0,  },
    { "nbformat.reader", MOD_INIT_NAME( nbformat$reader ), 0, 0,  },
    { "nbformat.sentinel", MOD_INIT_NAME( nbformat$sentinel ), 0, 0,  },
    { "nbformat.v1", MOD_INIT_NAME( nbformat$v1 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbformat.v1.convert", MOD_INIT_NAME( nbformat$v1$convert ), 0, 0,  },
    { "nbformat.v1.nbbase", MOD_INIT_NAME( nbformat$v1$nbbase ), 0, 0,  },
    { "nbformat.v1.nbjson", MOD_INIT_NAME( nbformat$v1$nbjson ), 0, 0,  },
    { "nbformat.v1.rwbase", MOD_INIT_NAME( nbformat$v1$rwbase ), 0, 0,  },
    { "nbformat.v2", MOD_INIT_NAME( nbformat$v2 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbformat.v2.convert", MOD_INIT_NAME( nbformat$v2$convert ), 0, 0,  },
    { "nbformat.v2.nbbase", MOD_INIT_NAME( nbformat$v2$nbbase ), 0, 0,  },
    { "nbformat.v2.nbjson", MOD_INIT_NAME( nbformat$v2$nbjson ), 0, 0,  },
    { "nbformat.v2.nbpy", MOD_INIT_NAME( nbformat$v2$nbpy ), 0, 0,  },
    { "nbformat.v2.nbxml", MOD_INIT_NAME( nbformat$v2$nbxml ), 0, 0,  },
    { "nbformat.v2.rwbase", MOD_INIT_NAME( nbformat$v2$rwbase ), 0, 0,  },
    { "nbformat.v3", MOD_INIT_NAME( nbformat$v3 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbformat.v3.convert", MOD_INIT_NAME( nbformat$v3$convert ), 0, 0,  },
    { "nbformat.v3.nbbase", MOD_INIT_NAME( nbformat$v3$nbbase ), 0, 0,  },
    { "nbformat.v3.nbjson", MOD_INIT_NAME( nbformat$v3$nbjson ), 0, 0,  },
    { "nbformat.v3.nbpy", MOD_INIT_NAME( nbformat$v3$nbpy ), 0, 0,  },
    { "nbformat.v3.rwbase", MOD_INIT_NAME( nbformat$v3$rwbase ), 0, 0,  },
    { "nbformat.v4", MOD_INIT_NAME( nbformat$v4 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "nbformat.v4.convert", MOD_INIT_NAME( nbformat$v4$convert ), 0, 0,  },
    { "nbformat.v4.nbbase", MOD_INIT_NAME( nbformat$v4$nbbase ), 0, 0,  },
    { "nbformat.v4.nbjson", MOD_INIT_NAME( nbformat$v4$nbjson ), 0, 0,  },
    { "nbformat.v4.rwbase", MOD_INIT_NAME( nbformat$v4$rwbase ), 0, 0,  },
    { "nbformat.validator", MOD_INIT_NAME( nbformat$validator ), 0, 0,  },
    { "netrc", NULL, 8823814, 3796, NUITKA_BYTECODE_FLAG },
    { "notebook", MOD_INIT_NAME( notebook ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "notebook._version", MOD_INIT_NAME( notebook$_version ), 0, 0,  },
    { "notebook.config_manager", MOD_INIT_NAME( notebook$config_manager ), 0, 0,  },
    { "notebook.extensions", MOD_INIT_NAME( notebook$extensions ), 0, 0,  },
    { "notebook.nbextensions", MOD_INIT_NAME( notebook$nbextensions ), 0, 0,  },
    { "notebook.services", MOD_INIT_NAME( notebook$services ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "notebook.services.contents", MOD_INIT_NAME( notebook$services$contents ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "notebook.services.contents.fileio", MOD_INIT_NAME( notebook$services$contents$fileio ), 0, 0,  },
    { "notebook.utils", MOD_INIT_NAME( notebook$utils ), 0, 0,  },
    { "ntpath", NULL, 8827610, 13037, NUITKA_BYTECODE_FLAG },
    { "numbers", NULL, 8840647, 12225, NUITKA_BYTECODE_FLAG },
    { "numpy", MOD_INIT_NAME( numpy ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.__config__", MOD_INIT_NAME( numpy$__config__ ), 0, 0,  },
    { "numpy._distributor_init", MOD_INIT_NAME( numpy$_distributor_init ), 0, 0,  },
    { "numpy._globals", MOD_INIT_NAME( numpy$_globals ), 0, 0,  },
    { "numpy._pytesttester", MOD_INIT_NAME( numpy$_pytesttester ), 0, 0,  },
    { "numpy.compat", MOD_INIT_NAME( numpy$compat ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.compat._inspect", MOD_INIT_NAME( numpy$compat$_inspect ), 0, 0,  },
    { "numpy.compat.py3k", MOD_INIT_NAME( numpy$compat$py3k ), 0, 0,  },
    { "numpy.core", MOD_INIT_NAME( numpy$core ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.core._add_newdocs", MOD_INIT_NAME( numpy$core$_add_newdocs ), 0, 0,  },
    { "numpy.core._asarray", MOD_INIT_NAME( numpy$core$_asarray ), 0, 0,  },
    { "numpy.core._dtype", MOD_INIT_NAME( numpy$core$_dtype ), 0, 0,  },
    { "numpy.core._dtype_ctypes", MOD_INIT_NAME( numpy$core$_dtype_ctypes ), 0, 0,  },
    { "numpy.core._exceptions", MOD_INIT_NAME( numpy$core$_exceptions ), 0, 0,  },
    { "numpy.core._internal", MOD_INIT_NAME( numpy$core$_internal ), 0, 0,  },
    { "numpy.core._methods", MOD_INIT_NAME( numpy$core$_methods ), 0, 0,  },
    { "numpy.core._multiarray_tests", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.core._multiarray_umath", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.core._string_helpers", MOD_INIT_NAME( numpy$core$_string_helpers ), 0, 0,  },
    { "numpy.core._type_aliases", MOD_INIT_NAME( numpy$core$_type_aliases ), 0, 0,  },
    { "numpy.core._ufunc_config", MOD_INIT_NAME( numpy$core$_ufunc_config ), 0, 0,  },
    { "numpy.core.arrayprint", MOD_INIT_NAME( numpy$core$arrayprint ), 0, 0,  },
    { "numpy.core.defchararray", MOD_INIT_NAME( numpy$core$defchararray ), 0, 0,  },
    { "numpy.core.einsumfunc", MOD_INIT_NAME( numpy$core$einsumfunc ), 0, 0,  },
    { "numpy.core.fromnumeric", MOD_INIT_NAME( numpy$core$fromnumeric ), 0, 0,  },
    { "numpy.core.function_base", MOD_INIT_NAME( numpy$core$function_base ), 0, 0,  },
    { "numpy.core.getlimits", MOD_INIT_NAME( numpy$core$getlimits ), 0, 0,  },
    { "numpy.core.info", MOD_INIT_NAME( numpy$core$info ), 0, 0,  },
    { "numpy.core.machar", MOD_INIT_NAME( numpy$core$machar ), 0, 0,  },
    { "numpy.core.memmap", MOD_INIT_NAME( numpy$core$memmap ), 0, 0,  },
    { "numpy.core.multiarray", MOD_INIT_NAME( numpy$core$multiarray ), 0, 0,  },
    { "numpy.core.numeric", MOD_INIT_NAME( numpy$core$numeric ), 0, 0,  },
    { "numpy.core.numerictypes", MOD_INIT_NAME( numpy$core$numerictypes ), 0, 0,  },
    { "numpy.core.overrides", MOD_INIT_NAME( numpy$core$overrides ), 0, 0,  },
    { "numpy.core.records", MOD_INIT_NAME( numpy$core$records ), 0, 0,  },
    { "numpy.core.shape_base", MOD_INIT_NAME( numpy$core$shape_base ), 0, 0,  },
    { "numpy.core.umath", MOD_INIT_NAME( numpy$core$umath ), 0, 0,  },
    { "numpy.ctypeslib", MOD_INIT_NAME( numpy$ctypeslib ), 0, 0,  },
    { "numpy.distutils", NULL, 8852872, 1025, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "numpy.distutils.__config__", NULL, 8853897, 1279, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.__version__", NULL, 8855176, 338, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils._shell_utils", NULL, 8855514, 3175, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.ccompiler", NULL, 8858689, 19132, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command", NULL, 8877821, 1100, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "numpy.distutils.command.autodist", NULL, 8878921, 3152, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.bdist_rpm", NULL, 8882073, 905, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.build", NULL, 8882978, 1873, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.build_clib", NULL, 8884851, 7480, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.build_ext", NULL, 8892331, 12517, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.build_py", NULL, 8904848, 1421, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.build_scripts", NULL, 8906269, 1732, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.build_src", NULL, 8908001, 18311, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.config", NULL, 8926312, 13677, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.config_compiler", NULL, 8939989, 4141, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.develop", NULL, 8944130, 928, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.egg_info", NULL, 8945058, 1158, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.install", NULL, 8946216, 2134, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.install_clib", NULL, 8948350, 1688, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.install_data", NULL, 8950038, 952, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.install_headers", NULL, 8950990, 1026, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.command.sdist", NULL, 8952016, 1016, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.compat", NULL, 8953032, 473, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.conv_template", NULL, 8953505, 8397, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.core", NULL, 8961902, 4782, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.cpuinfo", NULL, 8966684, 33470, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.exec_command", NULL, 9000154, 9318, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.extension", NULL, 9009472, 2099, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.fcompiler", NULL, 9011571, 28178, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "numpy.distutils.fcompiler.environment", NULL, 9039749, 3380, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.from_template", NULL, 9043129, 7285, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.info", NULL, 9050414, 329, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.lib2def", NULL, 9050743, 3251, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.log", NULL, 9053994, 2463, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.mingw32ccompiler", NULL, 9056457, 14469, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.misc_util", NULL, 9070926, 67606, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.npy_pkg_config", NULL, 9138532, 12406, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.numpy_distribution", NULL, 9150938, 843, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.system_info", NULL, 9151781, 78845, NUITKA_BYTECODE_FLAG },
    { "numpy.distutils.unixccompiler", NULL, 9230626, 3382, NUITKA_BYTECODE_FLAG },
    { "numpy.dual", MOD_INIT_NAME( numpy$dual ), 0, 0,  },
    { "numpy.f2py", NULL, 9234008, 2746, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "numpy.f2py.__version__", NULL, 9236754, 409, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.auxfuncs", NULL, 9237163, 22171, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.capi_maps", NULL, 9259334, 17851, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.cb_rules", NULL, 9277185, 15478, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.cfuncs", NULL, 9292663, 38378, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.common_rules", NULL, 9331041, 4836, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.crackfortran", NULL, 9335877, 76089, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.diagnose", NULL, 9411966, 3779, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.f2py2e", NULL, 9415745, 20000, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.f2py_testing", NULL, 9435745, 1468, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.f90mod_rules", NULL, 9437213, 7377, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.func2subr", NULL, 9444590, 6753, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.rules", NULL, 9451343, 34094, NUITKA_BYTECODE_FLAG },
    { "numpy.f2py.use_rules", NULL, 9485437, 3111, NUITKA_BYTECODE_FLAG },
    { "numpy.fft", MOD_INIT_NAME( numpy$fft ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.fft._pocketfft", MOD_INIT_NAME( numpy$fft$_pocketfft ), 0, 0,  },
    { "numpy.fft._pocketfft_internal", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.fft.helper", MOD_INIT_NAME( numpy$fft$helper ), 0, 0,  },
    { "numpy.lib", MOD_INIT_NAME( numpy$lib ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.lib._datasource", MOD_INIT_NAME( numpy$lib$_datasource ), 0, 0,  },
    { "numpy.lib._iotools", MOD_INIT_NAME( numpy$lib$_iotools ), 0, 0,  },
    { "numpy.lib._version", MOD_INIT_NAME( numpy$lib$_version ), 0, 0,  },
    { "numpy.lib.arraypad", MOD_INIT_NAME( numpy$lib$arraypad ), 0, 0,  },
    { "numpy.lib.arraysetops", MOD_INIT_NAME( numpy$lib$arraysetops ), 0, 0,  },
    { "numpy.lib.arrayterator", MOD_INIT_NAME( numpy$lib$arrayterator ), 0, 0,  },
    { "numpy.lib.financial", MOD_INIT_NAME( numpy$lib$financial ), 0, 0,  },
    { "numpy.lib.format", MOD_INIT_NAME( numpy$lib$format ), 0, 0,  },
    { "numpy.lib.function_base", MOD_INIT_NAME( numpy$lib$function_base ), 0, 0,  },
    { "numpy.lib.histograms", MOD_INIT_NAME( numpy$lib$histograms ), 0, 0,  },
    { "numpy.lib.index_tricks", MOD_INIT_NAME( numpy$lib$index_tricks ), 0, 0,  },
    { "numpy.lib.info", MOD_INIT_NAME( numpy$lib$info ), 0, 0,  },
    { "numpy.lib.mixins", MOD_INIT_NAME( numpy$lib$mixins ), 0, 0,  },
    { "numpy.lib.nanfunctions", MOD_INIT_NAME( numpy$lib$nanfunctions ), 0, 0,  },
    { "numpy.lib.npyio", MOD_INIT_NAME( numpy$lib$npyio ), 0, 0,  },
    { "numpy.lib.polynomial", MOD_INIT_NAME( numpy$lib$polynomial ), 0, 0,  },
    { "numpy.lib.scimath", MOD_INIT_NAME( numpy$lib$scimath ), 0, 0,  },
    { "numpy.lib.shape_base", MOD_INIT_NAME( numpy$lib$shape_base ), 0, 0,  },
    { "numpy.lib.stride_tricks", MOD_INIT_NAME( numpy$lib$stride_tricks ), 0, 0,  },
    { "numpy.lib.twodim_base", MOD_INIT_NAME( numpy$lib$twodim_base ), 0, 0,  },
    { "numpy.lib.type_check", MOD_INIT_NAME( numpy$lib$type_check ), 0, 0,  },
    { "numpy.lib.ufunclike", MOD_INIT_NAME( numpy$lib$ufunclike ), 0, 0,  },
    { "numpy.lib.utils", MOD_INIT_NAME( numpy$lib$utils ), 0, 0,  },
    { "numpy.linalg", MOD_INIT_NAME( numpy$linalg ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.linalg._umath_linalg", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.linalg.info", MOD_INIT_NAME( numpy$linalg$info ), 0, 0,  },
    { "numpy.linalg.lapack_lite", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.linalg.linalg", MOD_INIT_NAME( numpy$linalg$linalg ), 0, 0,  },
    { "numpy.ma", MOD_INIT_NAME( numpy$ma ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.ma.core", MOD_INIT_NAME( numpy$ma$core ), 0, 0,  },
    { "numpy.ma.extras", MOD_INIT_NAME( numpy$ma$extras ), 0, 0,  },
    { "numpy.ma.mrecords", MOD_INIT_NAME( numpy$ma$mrecords ), 0, 0,  },
    { "numpy.matrixlib", MOD_INIT_NAME( numpy$matrixlib ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.matrixlib.defmatrix", MOD_INIT_NAME( numpy$matrixlib$defmatrix ), 0, 0,  },
    { "numpy.polynomial", MOD_INIT_NAME( numpy$polynomial ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.polynomial._polybase", MOD_INIT_NAME( numpy$polynomial$_polybase ), 0, 0,  },
    { "numpy.polynomial.chebyshev", MOD_INIT_NAME( numpy$polynomial$chebyshev ), 0, 0,  },
    { "numpy.polynomial.hermite", MOD_INIT_NAME( numpy$polynomial$hermite ), 0, 0,  },
    { "numpy.polynomial.hermite_e", MOD_INIT_NAME( numpy$polynomial$hermite_e ), 0, 0,  },
    { "numpy.polynomial.laguerre", MOD_INIT_NAME( numpy$polynomial$laguerre ), 0, 0,  },
    { "numpy.polynomial.legendre", MOD_INIT_NAME( numpy$polynomial$legendre ), 0, 0,  },
    { "numpy.polynomial.polynomial", MOD_INIT_NAME( numpy$polynomial$polynomial ), 0, 0,  },
    { "numpy.polynomial.polyutils", MOD_INIT_NAME( numpy$polynomial$polyutils ), 0, 0,  },
    { "numpy.random", MOD_INIT_NAME( numpy$random ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "numpy.random._pickle", MOD_INIT_NAME( numpy$random$_pickle ), 0, 0,  },
    { "numpy.random.bit_generator", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.bounded_integers", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.common", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.generator", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.mt19937", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.mtrand", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.pcg64", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.philox", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.random.sfc64", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "numpy.testing", NULL, 9488548, 802, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "numpy.testing._private", NULL, 9489350, 144, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "numpy.testing._private.decorators", NULL, 9489494, 9131, NUITKA_BYTECODE_FLAG },
    { "numpy.testing._private.noseclasses", NULL, 9498625, 9877, NUITKA_BYTECODE_FLAG },
    { "numpy.testing._private.nosetester", NULL, 9508502, 15394, NUITKA_BYTECODE_FLAG },
    { "numpy.testing._private.parameterized", NULL, 9523896, 16674, NUITKA_BYTECODE_FLAG },
    { "numpy.testing._private.utils", NULL, 9540570, 65723, NUITKA_BYTECODE_FLAG },
    { "numpy.testing.noseclasses", NULL, 9606293, 564, NUITKA_BYTECODE_FLAG },
    { "numpy.version", MOD_INIT_NAME( numpy$version ), 0, 0,  },
    { "opcode", NULL, 9606857, 5411, NUITKA_BYTECODE_FLAG },
    { "operator", NULL, 9612268, 13933, NUITKA_BYTECODE_FLAG },
    { "optparse", NULL, 9626201, 47926, NUITKA_BYTECODE_FLAG },
    { "os", NULL, 9674127, 29726, NUITKA_BYTECODE_FLAG },
    { "packaging", MOD_INIT_NAME( packaging ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "packaging.__about__", MOD_INIT_NAME( packaging$__about__ ), 0, 0,  },
    { "pandocfilters", MOD_INIT_NAME( pandocfilters ), 0, 0,  },
    { "parso", MOD_INIT_NAME( parso ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "parso._compatibility", MOD_INIT_NAME( parso$_compatibility ), 0, 0,  },
    { "parso.cache", MOD_INIT_NAME( parso$cache ), 0, 0,  },
    { "parso.file_io", MOD_INIT_NAME( parso$file_io ), 0, 0,  },
    { "parso.grammar", MOD_INIT_NAME( parso$grammar ), 0, 0,  },
    { "parso.normalizer", MOD_INIT_NAME( parso$normalizer ), 0, 0,  },
    { "parso.parser", MOD_INIT_NAME( parso$parser ), 0, 0,  },
    { "parso.pgen2", MOD_INIT_NAME( parso$pgen2 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "parso.pgen2.generator", MOD_INIT_NAME( parso$pgen2$generator ), 0, 0,  },
    { "parso.pgen2.grammar_parser", MOD_INIT_NAME( parso$pgen2$grammar_parser ), 0, 0,  },
    { "parso.python", MOD_INIT_NAME( parso$python ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "parso.python.diff", MOD_INIT_NAME( parso$python$diff ), 0, 0,  },
    { "parso.python.errors", MOD_INIT_NAME( parso$python$errors ), 0, 0,  },
    { "parso.python.parser", MOD_INIT_NAME( parso$python$parser ), 0, 0,  },
    { "parso.python.pep8", MOD_INIT_NAME( parso$python$pep8 ), 0, 0,  },
    { "parso.python.prefix", MOD_INIT_NAME( parso$python$prefix ), 0, 0,  },
    { "parso.python.token", MOD_INIT_NAME( parso$python$token ), 0, 0,  },
    { "parso.python.tokenize", MOD_INIT_NAME( parso$python$tokenize ), 0, 0,  },
    { "parso.python.tree", MOD_INIT_NAME( parso$python$tree ), 0, 0,  },
    { "parso.tree", MOD_INIT_NAME( parso$tree ), 0, 0,  },
    { "parso.utils", MOD_INIT_NAME( parso$utils ), 0, 0,  },
    { "pathlib", NULL, 9703853, 41665, NUITKA_BYTECODE_FLAG },
    { "pdb", NULL, 9745518, 46909, NUITKA_BYTECODE_FLAG },
    { "pexpect", NULL, 9792427, 3951, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pexpect._async", NULL, 9796378, 3295, NUITKA_BYTECODE_FLAG },
    { "pexpect.exceptions", NULL, 9799673, 1831, NUITKA_BYTECODE_FLAG },
    { "pexpect.expect", NULL, 9801504, 8004, NUITKA_BYTECODE_FLAG },
    { "pexpect.pty_spawn", NULL, 9809508, 31699, NUITKA_BYTECODE_FLAG },
    { "pexpect.run", NULL, 9841207, 5761, NUITKA_BYTECODE_FLAG },
    { "pexpect.spawnbase", NULL, 9846968, 17522, NUITKA_BYTECODE_FLAG },
    { "pexpect.utils", NULL, 9864490, 3720, NUITKA_BYTECODE_FLAG },
    { "pickle", NULL, 9868210, 42999, NUITKA_BYTECODE_FLAG },
    { "pickleshare", MOD_INIT_NAME( pickleshare ), 0, 0,  },
    { "pipes", NULL, 9911209, 7836, NUITKA_BYTECODE_FLAG },
    { "pkg_resources", NULL, 9919045, 99734, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pkg_resources._vendor", NULL, 10018779, 143, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pkg_resources._vendor.appdirs", NULL, 10018922, 20631, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging", NULL, 10039553, 517, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pkg_resources._vendor.packaging.__about__", NULL, 10040070, 679, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging._compat", NULL, 10040749, 969, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging._structures", NULL, 10041718, 2821, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging.markers", NULL, 10044539, 8829, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging.requirements", NULL, 10053368, 3834, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging.specifiers", NULL, 10057202, 19747, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.packaging.version", NULL, 10076949, 10514, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.pyparsing", NULL, 10087463, 202986, NUITKA_BYTECODE_FLAG },
    { "pkg_resources._vendor.six", NULL, 10290449, 24344, NUITKA_BYTECODE_FLAG },
    { "pkg_resources.extern", NULL, 10314793, 2362, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pkg_resources.py31compat", NULL, 10317155, 580, NUITKA_BYTECODE_FLAG },
    { "pkgutil", NULL, 10317735, 16393, NUITKA_BYTECODE_FLAG },
    { "platform", NULL, 10334128, 28253, NUITKA_BYTECODE_FLAG },
    { "plistlib", NULL, 10362381, 25128, NUITKA_BYTECODE_FLAG },
    { "posixpath", NULL, 10387509, 10463, NUITKA_BYTECODE_FLAG },
    { "pprint", NULL, 10397972, 15866, NUITKA_BYTECODE_FLAG },
    { "preprocess", MOD_INIT_NAME( preprocess ), 0, 0,  },
    { "profile", NULL, 10413838, 13874, NUITKA_BYTECODE_FLAG },
    { "prompt_toolkit", MOD_INIT_NAME( prompt_toolkit ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.application", MOD_INIT_NAME( prompt_toolkit$application ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.application.application", MOD_INIT_NAME( prompt_toolkit$application$application ), 0, 0,  },
    { "prompt_toolkit.application.current", MOD_INIT_NAME( prompt_toolkit$application$current ), 0, 0,  },
    { "prompt_toolkit.application.dummy", MOD_INIT_NAME( prompt_toolkit$application$dummy ), 0, 0,  },
    { "prompt_toolkit.application.run_in_terminal", MOD_INIT_NAME( prompt_toolkit$application$run_in_terminal ), 0, 0,  },
    { "prompt_toolkit.auto_suggest", MOD_INIT_NAME( prompt_toolkit$auto_suggest ), 0, 0,  },
    { "prompt_toolkit.buffer", MOD_INIT_NAME( prompt_toolkit$buffer ), 0, 0,  },
    { "prompt_toolkit.cache", MOD_INIT_NAME( prompt_toolkit$cache ), 0, 0,  },
    { "prompt_toolkit.clipboard", MOD_INIT_NAME( prompt_toolkit$clipboard ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.clipboard.base", MOD_INIT_NAME( prompt_toolkit$clipboard$base ), 0, 0,  },
    { "prompt_toolkit.clipboard.in_memory", MOD_INIT_NAME( prompt_toolkit$clipboard$in_memory ), 0, 0,  },
    { "prompt_toolkit.completion", MOD_INIT_NAME( prompt_toolkit$completion ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.completion.base", MOD_INIT_NAME( prompt_toolkit$completion$base ), 0, 0,  },
    { "prompt_toolkit.completion.filesystem", MOD_INIT_NAME( prompt_toolkit$completion$filesystem ), 0, 0,  },
    { "prompt_toolkit.completion.fuzzy_completer", MOD_INIT_NAME( prompt_toolkit$completion$fuzzy_completer ), 0, 0,  },
    { "prompt_toolkit.completion.word_completer", MOD_INIT_NAME( prompt_toolkit$completion$word_completer ), 0, 0,  },
    { "prompt_toolkit.document", MOD_INIT_NAME( prompt_toolkit$document ), 0, 0,  },
    { "prompt_toolkit.enums", MOD_INIT_NAME( prompt_toolkit$enums ), 0, 0,  },
    { "prompt_toolkit.eventloop", MOD_INIT_NAME( prompt_toolkit$eventloop ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.eventloop.async_generator", MOD_INIT_NAME( prompt_toolkit$eventloop$async_generator ), 0, 0,  },
    { "prompt_toolkit.eventloop.asyncio_posix", MOD_INIT_NAME( prompt_toolkit$eventloop$asyncio_posix ), 0, 0,  },
    { "prompt_toolkit.eventloop.asyncio_win32", MOD_INIT_NAME( prompt_toolkit$eventloop$asyncio_win32 ), 0, 0,  },
    { "prompt_toolkit.eventloop.base", MOD_INIT_NAME( prompt_toolkit$eventloop$base ), 0, 0,  },
    { "prompt_toolkit.eventloop.context", MOD_INIT_NAME( prompt_toolkit$eventloop$context ), 0, 0,  },
    { "prompt_toolkit.eventloop.coroutine", MOD_INIT_NAME( prompt_toolkit$eventloop$coroutine ), 0, 0,  },
    { "prompt_toolkit.eventloop.defaults", MOD_INIT_NAME( prompt_toolkit$eventloop$defaults ), 0, 0,  },
    { "prompt_toolkit.eventloop.event", MOD_INIT_NAME( prompt_toolkit$eventloop$event ), 0, 0,  },
    { "prompt_toolkit.eventloop.future", MOD_INIT_NAME( prompt_toolkit$eventloop$future ), 0, 0,  },
    { "prompt_toolkit.eventloop.inputhook", MOD_INIT_NAME( prompt_toolkit$eventloop$inputhook ), 0, 0,  },
    { "prompt_toolkit.eventloop.posix", MOD_INIT_NAME( prompt_toolkit$eventloop$posix ), 0, 0,  },
    { "prompt_toolkit.eventloop.select", MOD_INIT_NAME( prompt_toolkit$eventloop$select ), 0, 0,  },
    { "prompt_toolkit.eventloop.utils", MOD_INIT_NAME( prompt_toolkit$eventloop$utils ), 0, 0,  },
    { "prompt_toolkit.eventloop.win32", MOD_INIT_NAME( prompt_toolkit$eventloop$win32 ), 0, 0,  },
    { "prompt_toolkit.filters", MOD_INIT_NAME( prompt_toolkit$filters ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.filters.app", MOD_INIT_NAME( prompt_toolkit$filters$app ), 0, 0,  },
    { "prompt_toolkit.filters.base", MOD_INIT_NAME( prompt_toolkit$filters$base ), 0, 0,  },
    { "prompt_toolkit.filters.cli", MOD_INIT_NAME( prompt_toolkit$filters$cli ), 0, 0,  },
    { "prompt_toolkit.filters.utils", MOD_INIT_NAME( prompt_toolkit$filters$utils ), 0, 0,  },
    { "prompt_toolkit.formatted_text", MOD_INIT_NAME( prompt_toolkit$formatted_text ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.formatted_text.ansi", MOD_INIT_NAME( prompt_toolkit$formatted_text$ansi ), 0, 0,  },
    { "prompt_toolkit.formatted_text.base", MOD_INIT_NAME( prompt_toolkit$formatted_text$base ), 0, 0,  },
    { "prompt_toolkit.formatted_text.html", MOD_INIT_NAME( prompt_toolkit$formatted_text$html ), 0, 0,  },
    { "prompt_toolkit.formatted_text.pygments", MOD_INIT_NAME( prompt_toolkit$formatted_text$pygments ), 0, 0,  },
    { "prompt_toolkit.formatted_text.utils", MOD_INIT_NAME( prompt_toolkit$formatted_text$utils ), 0, 0,  },
    { "prompt_toolkit.history", MOD_INIT_NAME( prompt_toolkit$history ), 0, 0,  },
    { "prompt_toolkit.input", MOD_INIT_NAME( prompt_toolkit$input ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.input.ansi_escape_sequences", MOD_INIT_NAME( prompt_toolkit$input$ansi_escape_sequences ), 0, 0,  },
    { "prompt_toolkit.input.base", MOD_INIT_NAME( prompt_toolkit$input$base ), 0, 0,  },
    { "prompt_toolkit.input.defaults", MOD_INIT_NAME( prompt_toolkit$input$defaults ), 0, 0,  },
    { "prompt_toolkit.input.posix_pipe", MOD_INIT_NAME( prompt_toolkit$input$posix_pipe ), 0, 0,  },
    { "prompt_toolkit.input.posix_utils", MOD_INIT_NAME( prompt_toolkit$input$posix_utils ), 0, 0,  },
    { "prompt_toolkit.input.typeahead", MOD_INIT_NAME( prompt_toolkit$input$typeahead ), 0, 0,  },
    { "prompt_toolkit.input.vt100", MOD_INIT_NAME( prompt_toolkit$input$vt100 ), 0, 0,  },
    { "prompt_toolkit.input.vt100_parser", MOD_INIT_NAME( prompt_toolkit$input$vt100_parser ), 0, 0,  },
    { "prompt_toolkit.input.win32", MOD_INIT_NAME( prompt_toolkit$input$win32 ), 0, 0,  },
    { "prompt_toolkit.input.win32_pipe", MOD_INIT_NAME( prompt_toolkit$input$win32_pipe ), 0, 0,  },
    { "prompt_toolkit.key_binding", MOD_INIT_NAME( prompt_toolkit$key_binding ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.key_binding.bindings", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.key_binding.bindings.auto_suggest", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$auto_suggest ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.basic", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$basic ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.completion", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$completion ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.cpr", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$cpr ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.emacs", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$emacs ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.focus", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$focus ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.mouse", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$mouse ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.named_commands", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$named_commands ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.open_in_editor", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$open_in_editor ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.page_navigation", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$page_navigation ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.scroll", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$scroll ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.search", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$search ), 0, 0,  },
    { "prompt_toolkit.key_binding.bindings.vi", MOD_INIT_NAME( prompt_toolkit$key_binding$bindings$vi ), 0, 0,  },
    { "prompt_toolkit.key_binding.defaults", MOD_INIT_NAME( prompt_toolkit$key_binding$defaults ), 0, 0,  },
    { "prompt_toolkit.key_binding.digraphs", MOD_INIT_NAME( prompt_toolkit$key_binding$digraphs ), 0, 0,  },
    { "prompt_toolkit.key_binding.emacs_state", MOD_INIT_NAME( prompt_toolkit$key_binding$emacs_state ), 0, 0,  },
    { "prompt_toolkit.key_binding.key_bindings", MOD_INIT_NAME( prompt_toolkit$key_binding$key_bindings ), 0, 0,  },
    { "prompt_toolkit.key_binding.key_processor", MOD_INIT_NAME( prompt_toolkit$key_binding$key_processor ), 0, 0,  },
    { "prompt_toolkit.key_binding.vi_state", MOD_INIT_NAME( prompt_toolkit$key_binding$vi_state ), 0, 0,  },
    { "prompt_toolkit.keys", MOD_INIT_NAME( prompt_toolkit$keys ), 0, 0,  },
    { "prompt_toolkit.layout", MOD_INIT_NAME( prompt_toolkit$layout ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.layout.containers", MOD_INIT_NAME( prompt_toolkit$layout$containers ), 0, 0,  },
    { "prompt_toolkit.layout.controls", MOD_INIT_NAME( prompt_toolkit$layout$controls ), 0, 0,  },
    { "prompt_toolkit.layout.dimension", MOD_INIT_NAME( prompt_toolkit$layout$dimension ), 0, 0,  },
    { "prompt_toolkit.layout.dummy", MOD_INIT_NAME( prompt_toolkit$layout$dummy ), 0, 0,  },
    { "prompt_toolkit.layout.layout", MOD_INIT_NAME( prompt_toolkit$layout$layout ), 0, 0,  },
    { "prompt_toolkit.layout.margins", MOD_INIT_NAME( prompt_toolkit$layout$margins ), 0, 0,  },
    { "prompt_toolkit.layout.menus", MOD_INIT_NAME( prompt_toolkit$layout$menus ), 0, 0,  },
    { "prompt_toolkit.layout.mouse_handlers", MOD_INIT_NAME( prompt_toolkit$layout$mouse_handlers ), 0, 0,  },
    { "prompt_toolkit.layout.processors", MOD_INIT_NAME( prompt_toolkit$layout$processors ), 0, 0,  },
    { "prompt_toolkit.layout.screen", MOD_INIT_NAME( prompt_toolkit$layout$screen ), 0, 0,  },
    { "prompt_toolkit.layout.utils", MOD_INIT_NAME( prompt_toolkit$layout$utils ), 0, 0,  },
    { "prompt_toolkit.lexers", MOD_INIT_NAME( prompt_toolkit$lexers ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.lexers.base", MOD_INIT_NAME( prompt_toolkit$lexers$base ), 0, 0,  },
    { "prompt_toolkit.lexers.pygments", MOD_INIT_NAME( prompt_toolkit$lexers$pygments ), 0, 0,  },
    { "prompt_toolkit.log", MOD_INIT_NAME( prompt_toolkit$log ), 0, 0,  },
    { "prompt_toolkit.mouse_events", MOD_INIT_NAME( prompt_toolkit$mouse_events ), 0, 0,  },
    { "prompt_toolkit.output", MOD_INIT_NAME( prompt_toolkit$output ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.output.base", MOD_INIT_NAME( prompt_toolkit$output$base ), 0, 0,  },
    { "prompt_toolkit.output.color_depth", MOD_INIT_NAME( prompt_toolkit$output$color_depth ), 0, 0,  },
    { "prompt_toolkit.output.conemu", MOD_INIT_NAME( prompt_toolkit$output$conemu ), 0, 0,  },
    { "prompt_toolkit.output.defaults", MOD_INIT_NAME( prompt_toolkit$output$defaults ), 0, 0,  },
    { "prompt_toolkit.output.vt100", MOD_INIT_NAME( prompt_toolkit$output$vt100 ), 0, 0,  },
    { "prompt_toolkit.output.win32", MOD_INIT_NAME( prompt_toolkit$output$win32 ), 0, 0,  },
    { "prompt_toolkit.output.windows10", MOD_INIT_NAME( prompt_toolkit$output$windows10 ), 0, 0,  },
    { "prompt_toolkit.patch_stdout", MOD_INIT_NAME( prompt_toolkit$patch_stdout ), 0, 0,  },
    { "prompt_toolkit.renderer", MOD_INIT_NAME( prompt_toolkit$renderer ), 0, 0,  },
    { "prompt_toolkit.search", MOD_INIT_NAME( prompt_toolkit$search ), 0, 0,  },
    { "prompt_toolkit.selection", MOD_INIT_NAME( prompt_toolkit$selection ), 0, 0,  },
    { "prompt_toolkit.shortcuts", MOD_INIT_NAME( prompt_toolkit$shortcuts ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.shortcuts.dialogs", MOD_INIT_NAME( prompt_toolkit$shortcuts$dialogs ), 0, 0,  },
    { "prompt_toolkit.shortcuts.progress_bar", MOD_INIT_NAME( prompt_toolkit$shortcuts$progress_bar ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.shortcuts.progress_bar.base", MOD_INIT_NAME( prompt_toolkit$shortcuts$progress_bar$base ), 0, 0,  },
    { "prompt_toolkit.shortcuts.progress_bar.formatters", MOD_INIT_NAME( prompt_toolkit$shortcuts$progress_bar$formatters ), 0, 0,  },
    { "prompt_toolkit.shortcuts.prompt", MOD_INIT_NAME( prompt_toolkit$shortcuts$prompt ), 0, 0,  },
    { "prompt_toolkit.shortcuts.utils", MOD_INIT_NAME( prompt_toolkit$shortcuts$utils ), 0, 0,  },
    { "prompt_toolkit.styles", MOD_INIT_NAME( prompt_toolkit$styles ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.styles.base", MOD_INIT_NAME( prompt_toolkit$styles$base ), 0, 0,  },
    { "prompt_toolkit.styles.defaults", MOD_INIT_NAME( prompt_toolkit$styles$defaults ), 0, 0,  },
    { "prompt_toolkit.styles.named_colors", MOD_INIT_NAME( prompt_toolkit$styles$named_colors ), 0, 0,  },
    { "prompt_toolkit.styles.pygments", MOD_INIT_NAME( prompt_toolkit$styles$pygments ), 0, 0,  },
    { "prompt_toolkit.styles.style", MOD_INIT_NAME( prompt_toolkit$styles$style ), 0, 0,  },
    { "prompt_toolkit.styles.style_transformation", MOD_INIT_NAME( prompt_toolkit$styles$style_transformation ), 0, 0,  },
    { "prompt_toolkit.utils", MOD_INIT_NAME( prompt_toolkit$utils ), 0, 0,  },
    { "prompt_toolkit.validation", MOD_INIT_NAME( prompt_toolkit$validation ), 0, 0,  },
    { "prompt_toolkit.widgets", MOD_INIT_NAME( prompt_toolkit$widgets ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "prompt_toolkit.widgets.base", MOD_INIT_NAME( prompt_toolkit$widgets$base ), 0, 0,  },
    { "prompt_toolkit.widgets.dialogs", MOD_INIT_NAME( prompt_toolkit$widgets$dialogs ), 0, 0,  },
    { "prompt_toolkit.widgets.menus", MOD_INIT_NAME( prompt_toolkit$widgets$menus ), 0, 0,  },
    { "prompt_toolkit.widgets.toolbars", MOD_INIT_NAME( prompt_toolkit$widgets$toolbars ), 0, 0,  },
    { "prompt_toolkit.win32_types", MOD_INIT_NAME( prompt_toolkit$win32_types ), 0, 0,  },
    { "pstats", NULL, 10427712, 22327, NUITKA_BYTECODE_FLAG },
    { "psutil", MOD_INIT_NAME( psutil ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "psutil._common", MOD_INIT_NAME( psutil$_common ), 0, 0,  },
    { "psutil._compat", MOD_INIT_NAME( psutil$_compat ), 0, 0,  },
    { "psutil._psaix", MOD_INIT_NAME( psutil$_psaix ), 0, 0,  },
    { "psutil._psbsd", MOD_INIT_NAME( psutil$_psbsd ), 0, 0,  },
    { "psutil._pslinux", MOD_INIT_NAME( psutil$_pslinux ), 0, 0,  },
    { "psutil._psosx", MOD_INIT_NAME( psutil$_psosx ), 0, 0,  },
    { "psutil._psposix", MOD_INIT_NAME( psutil$_psposix ), 0, 0,  },
    { "psutil._pssunos", MOD_INIT_NAME( psutil$_pssunos ), 0, 0,  },
    { "psutil._psutil_osx", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "psutil._psutil_posix", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "psutil._pswindows", MOD_INIT_NAME( psutil$_pswindows ), 0, 0,  },
    { "pty", NULL, 10450039, 3916, NUITKA_BYTECODE_FLAG },
    { "ptyprocess", MOD_INIT_NAME( ptyprocess ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "ptyprocess._fork_pty", MOD_INIT_NAME( ptyprocess$_fork_pty ), 0, 0,  },
    { "ptyprocess.ptyprocess", MOD_INIT_NAME( ptyprocess$ptyprocess ), 0, 0,  },
    { "ptyprocess.util", MOD_INIT_NAME( ptyprocess$util ), 0, 0,  },
    { "pvectorc", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "pycparser", NULL, 10453955, 2442, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pycparser.ast_transforms", NULL, 10456397, 2495, NUITKA_BYTECODE_FLAG },
    { "pycparser.c_ast", NULL, 10458892, 37259, NUITKA_BYTECODE_FLAG },
    { "pycparser.c_lexer", NULL, 10496151, 11542, NUITKA_BYTECODE_FLAG },
    { "pycparser.c_parser", NULL, 10507693, 60847, NUITKA_BYTECODE_FLAG },
    { "pycparser.lextab", NULL, 10568540, 5600, NUITKA_BYTECODE_FLAG },
    { "pycparser.ply", NULL, 10574140, 184, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pycparser.ply.lex", NULL, 10574324, 21433, NUITKA_BYTECODE_FLAG },
    { "pycparser.ply.yacc", NULL, 10595757, 53650, NUITKA_BYTECODE_FLAG },
    { "pycparser.plyparser", NULL, 10649407, 4613, NUITKA_BYTECODE_FLAG },
    { "pycparser.yacctab", NULL, 10654020, 141854, NUITKA_BYTECODE_FLAG },
    { "pydoc", NULL, 10795874, 84463, NUITKA_BYTECODE_FLAG },
    { "pydoc_data", NULL, 10880337, 179, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pyexpat", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "pygments", MOD_INIT_NAME( pygments ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pygments.filter", MOD_INIT_NAME( pygments$filter ), 0, 0,  },
    { "pygments.filters", MOD_INIT_NAME( pygments$filters ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pygments.formatters", MOD_INIT_NAME( pygments$formatters ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pygments.formatters._mapping", MOD_INIT_NAME( pygments$formatters$_mapping ), 0, 0,  },
    { "pygments.lexer", MOD_INIT_NAME( pygments$lexer ), 0, 0,  },
    { "pygments.lexers", MOD_INIT_NAME( pygments$lexers ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pygments.lexers._mapping", MOD_INIT_NAME( pygments$lexers$_mapping ), 0, 0,  },
    { "pygments.lexers.special", MOD_INIT_NAME( pygments$lexers$special ), 0, 0,  },
    { "pygments.modeline", MOD_INIT_NAME( pygments$modeline ), 0, 0,  },
    { "pygments.plugin", MOD_INIT_NAME( pygments$plugin ), 0, 0,  },
    { "pygments.regexopt", MOD_INIT_NAME( pygments$regexopt ), 0, 0,  },
    { "pygments.style", MOD_INIT_NAME( pygments$style ), 0, 0,  },
    { "pygments.styles", MOD_INIT_NAME( pygments$styles ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pygments.token", MOD_INIT_NAME( pygments$token ), 0, 0,  },
    { "pygments.util", MOD_INIT_NAME( pygments$util ), 0, 0,  },
    { "pymorphy2", MOD_INIT_NAME( pymorphy2 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pymorphy2.analyzer", MOD_INIT_NAME( pymorphy2$analyzer ), 0, 0,  },
    { "pymorphy2.constants", MOD_INIT_NAME( pymorphy2$constants ), 0, 0,  },
    { "pymorphy2.dawg", MOD_INIT_NAME( pymorphy2$dawg ), 0, 0,  },
    { "pymorphy2.opencorpora_dict", MOD_INIT_NAME( pymorphy2$opencorpora_dict ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pymorphy2.opencorpora_dict.compile", MOD_INIT_NAME( pymorphy2$opencorpora_dict$compile ), 0, 0,  },
    { "pymorphy2.opencorpora_dict.parse", MOD_INIT_NAME( pymorphy2$opencorpora_dict$parse ), 0, 0,  },
    { "pymorphy2.opencorpora_dict.preprocess", MOD_INIT_NAME( pymorphy2$opencorpora_dict$preprocess ), 0, 0,  },
    { "pymorphy2.opencorpora_dict.storage", MOD_INIT_NAME( pymorphy2$opencorpora_dict$storage ), 0, 0,  },
    { "pymorphy2.opencorpora_dict.wrapper", MOD_INIT_NAME( pymorphy2$opencorpora_dict$wrapper ), 0, 0,  },
    { "pymorphy2.shapes", MOD_INIT_NAME( pymorphy2$shapes ), 0, 0,  },
    { "pymorphy2.tagset", MOD_INIT_NAME( pymorphy2$tagset ), 0, 0,  },
    { "pymorphy2.units", MOD_INIT_NAME( pymorphy2$units ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pymorphy2.units.abbreviations", MOD_INIT_NAME( pymorphy2$units$abbreviations ), 0, 0,  },
    { "pymorphy2.units.base", MOD_INIT_NAME( pymorphy2$units$base ), 0, 0,  },
    { "pymorphy2.units.by_analogy", MOD_INIT_NAME( pymorphy2$units$by_analogy ), 0, 0,  },
    { "pymorphy2.units.by_hyphen", MOD_INIT_NAME( pymorphy2$units$by_hyphen ), 0, 0,  },
    { "pymorphy2.units.by_lookup", MOD_INIT_NAME( pymorphy2$units$by_lookup ), 0, 0,  },
    { "pymorphy2.units.by_shape", MOD_INIT_NAME( pymorphy2$units$by_shape ), 0, 0,  },
    { "pymorphy2.units.unkn", MOD_INIT_NAME( pymorphy2$units$unkn ), 0, 0,  },
    { "pymorphy2.units.utils", MOD_INIT_NAME( pymorphy2$units$utils ), 0, 0,  },
    { "pymorphy2.utils", MOD_INIT_NAME( pymorphy2$utils ), 0, 0,  },
    { "pymorphy2.version", MOD_INIT_NAME( pymorphy2$version ), 0, 0,  },
    { "pymorphy2_dicts", MOD_INIT_NAME( pymorphy2_dicts ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pymorphy2_dicts.version", MOD_INIT_NAME( pymorphy2_dicts$version ), 0, 0,  },
    { "pyparsing", MOD_INIT_NAME( pyparsing ), 0, 0,  },
    { "pyrsistent", MOD_INIT_NAME( pyrsistent ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "pyrsistent._checked_types", MOD_INIT_NAME( pyrsistent$_checked_types ), 0, 0,  },
    { "pyrsistent._compat", MOD_INIT_NAME( pyrsistent$_compat ), 0, 0,  },
    { "pyrsistent._field_common", MOD_INIT_NAME( pyrsistent$_field_common ), 0, 0,  },
    { "pyrsistent._helpers", MOD_INIT_NAME( pyrsistent$_helpers ), 0, 0,  },
    { "pyrsistent._immutable", MOD_INIT_NAME( pyrsistent$_immutable ), 0, 0,  },
    { "pyrsistent._pbag", MOD_INIT_NAME( pyrsistent$_pbag ), 0, 0,  },
    { "pyrsistent._pclass", MOD_INIT_NAME( pyrsistent$_pclass ), 0, 0,  },
    { "pyrsistent._pdeque", MOD_INIT_NAME( pyrsistent$_pdeque ), 0, 0,  },
    { "pyrsistent._plist", MOD_INIT_NAME( pyrsistent$_plist ), 0, 0,  },
    { "pyrsistent._pmap", MOD_INIT_NAME( pyrsistent$_pmap ), 0, 0,  },
    { "pyrsistent._precord", MOD_INIT_NAME( pyrsistent$_precord ), 0, 0,  },
    { "pyrsistent._pset", MOD_INIT_NAME( pyrsistent$_pset ), 0, 0,  },
    { "pyrsistent._pvector", MOD_INIT_NAME( pyrsistent$_pvector ), 0, 0,  },
    { "pyrsistent._toolz", MOD_INIT_NAME( pyrsistent$_toolz ), 0, 0,  },
    { "pyrsistent._transformations", MOD_INIT_NAME( pyrsistent$_transformations ), 0, 0,  },
    { "queue", NULL, 10880516, 11505, NUITKA_BYTECODE_FLAG },
    { "random", NULL, 10892021, 19441, NUITKA_BYTECODE_FLAG },
    { "re", NULL, 10911462, 13837, NUITKA_BYTECODE_FLAG },
    { "readline", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "reprlib", NULL, 10925299, 5383, NUITKA_BYTECODE_FLAG },
    { "requests", MOD_INIT_NAME( requests ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "requests.__version__", MOD_INIT_NAME( requests$__version__ ), 0, 0,  },
    { "requests._internal_utils", MOD_INIT_NAME( requests$_internal_utils ), 0, 0,  },
    { "requests.adapters", MOD_INIT_NAME( requests$adapters ), 0, 0,  },
    { "requests.api", MOD_INIT_NAME( requests$api ), 0, 0,  },
    { "requests.auth", MOD_INIT_NAME( requests$auth ), 0, 0,  },
    { "requests.certs", MOD_INIT_NAME( requests$certs ), 0, 0,  },
    { "requests.compat", MOD_INIT_NAME( requests$compat ), 0, 0,  },
    { "requests.cookies", MOD_INIT_NAME( requests$cookies ), 0, 0,  },
    { "requests.exceptions", MOD_INIT_NAME( requests$exceptions ), 0, 0,  },
    { "requests.hooks", MOD_INIT_NAME( requests$hooks ), 0, 0,  },
    { "requests.models", MOD_INIT_NAME( requests$models ), 0, 0,  },
    { "requests.packages", MOD_INIT_NAME( requests$packages ), 0, 0,  },
    { "requests.sessions", MOD_INIT_NAME( requests$sessions ), 0, 0,  },
    { "requests.status_codes", MOD_INIT_NAME( requests$status_codes ), 0, 0,  },
    { "requests.structures", MOD_INIT_NAME( requests$structures ), 0, 0,  },
    { "requests.utils", MOD_INIT_NAME( requests$utils ), 0, 0,  },
    { "resource", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "runpy", NULL, 10930682, 7978, NUITKA_BYTECODE_FLAG },
    { "scipy", MOD_INIT_NAME( scipy ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "scipy.__config__", MOD_INIT_NAME( scipy$__config__ ), 0, 0,  },
    { "scipy._distributor_init", MOD_INIT_NAME( scipy$_distributor_init ), 0, 0,  },
    { "scipy._lib", MOD_INIT_NAME( scipy$_lib ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "scipy._lib._ccallback", MOD_INIT_NAME( scipy$_lib$_ccallback ), 0, 0,  },
    { "scipy._lib._ccallback_c", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "scipy._lib._testutils", MOD_INIT_NAME( scipy$_lib$_testutils ), 0, 0,  },
    { "scipy._lib._version", MOD_INIT_NAME( scipy$_lib$_version ), 0, 0,  },
    { "scipy._lib.messagestream", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "scipy._lib.six", MOD_INIT_NAME( scipy$_lib$six ), 0, 0,  },
    { "scipy.version", MOD_INIT_NAME( scipy$version ), 0, 0,  },
    { "select", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "selectors", NULL, 10938660, 16981, NUITKA_BYTECODE_FLAG },
    { "setuptools", NULL, 10955641, 7644, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "setuptools._deprecation_warning", NULL, 10963285, 498, NUITKA_BYTECODE_FLAG },
    { "setuptools._imp", NULL, 10963783, 1879, NUITKA_BYTECODE_FLAG },
    { "setuptools.archive_util", NULL, 10965662, 5079, NUITKA_BYTECODE_FLAG },
    { "setuptools.command", NULL, 10970741, 686, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "setuptools.command.bdist_egg", NULL, 10971427, 14146, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.bdist_rpm", NULL, 10985573, 1733, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.develop", NULL, 10987306, 6414, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.easy_install", NULL, 10993720, 64795, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.egg_info", NULL, 11058515, 21631, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.install", NULL, 11080146, 3961, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.install_scripts", NULL, 11084107, 2241, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.py36compat", NULL, 11086348, 4574, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.sdist", NULL, 11090922, 6733, NUITKA_BYTECODE_FLAG },
    { "setuptools.command.setopt", NULL, 11097655, 4473, NUITKA_BYTECODE_FLAG },
    { "setuptools.config", NULL, 11102128, 17742, NUITKA_BYTECODE_FLAG },
    { "setuptools.depends", NULL, 11119870, 5124, NUITKA_BYTECODE_FLAG },
    { "setuptools.dist", NULL, 11124994, 42201, NUITKA_BYTECODE_FLAG },
    { "setuptools.extension", NULL, 11167195, 1925, NUITKA_BYTECODE_FLAG },
    { "setuptools.extern", NULL, 11169120, 2376, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "setuptools.glibc", NULL, 11171496, 1490, NUITKA_BYTECODE_FLAG },
    { "setuptools.glob", NULL, 11172986, 3700, NUITKA_BYTECODE_FLAG },
    { "setuptools.monkey", NULL, 11176686, 4584, NUITKA_BYTECODE_FLAG },
    { "setuptools.namespaces", NULL, 11181270, 3562, NUITKA_BYTECODE_FLAG },
    { "setuptools.package_index", NULL, 11184832, 32657, NUITKA_BYTECODE_FLAG },
    { "setuptools.pep425tags", NULL, 11217489, 7152, NUITKA_BYTECODE_FLAG },
    { "setuptools.py27compat", NULL, 11224641, 1714, NUITKA_BYTECODE_FLAG },
    { "setuptools.py33compat", NULL, 11226355, 1386, NUITKA_BYTECODE_FLAG },
    { "setuptools.py34compat", NULL, 11227741, 432, NUITKA_BYTECODE_FLAG },
    { "setuptools.sandbox", NULL, 11228173, 15486, NUITKA_BYTECODE_FLAG },
    { "setuptools.ssl_support", NULL, 11243659, 6742, NUITKA_BYTECODE_FLAG },
    { "setuptools.unicode_utils", NULL, 11250401, 1119, NUITKA_BYTECODE_FLAG },
    { "setuptools.version", NULL, 11251520, 278, NUITKA_BYTECODE_FLAG },
    { "setuptools.wheel", NULL, 11251798, 6931, NUITKA_BYTECODE_FLAG },
    { "setuptools.windows_support", NULL, 11258729, 961, NUITKA_BYTECODE_FLAG },
    { "shlex", NULL, 11259690, 7023, NUITKA_BYTECODE_FLAG },
    { "shutil", NULL, 11266713, 30897, NUITKA_BYTECODE_FLAG },
    { "signal", NULL, 11297610, 2545, NUITKA_BYTECODE_FLAG },
    { "site", NULL, 11300155, 13289, NUITKA_BYTECODE_FLAG },
    { "six", MOD_INIT_NAME( six ), 0, 0,  },
    { "socket", NULL, 11313444, 22054, NUITKA_BYTECODE_FLAG },
    { "socketserver", NULL, 11335498, 24232, NUITKA_BYTECODE_FLAG },
    { "sqlite3", NULL, 11359730, 207, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "sre_constants", NULL, 11359937, 6324, NUITKA_BYTECODE_FLAG },
    { "ssl", NULL, 11366261, 39804, NUITKA_BYTECODE_FLAG },
    { "stat", NULL, 11406065, 3906, NUITKA_BYTECODE_FLAG },
    { "string", NULL, 11409971, 7868, NUITKA_BYTECODE_FLAG },
    { "struct", NULL, 11417839, 367, NUITKA_BYTECODE_FLAG },
    { "subprocess", NULL, 11418206, 38798, NUITKA_BYTECODE_FLAG },
    { "sysconfig", NULL, 11457004, 15477, NUITKA_BYTECODE_FLAG },
    { "tarfile", NULL, 11472481, 61874, NUITKA_BYTECODE_FLAG },
    { "tempfile", NULL, 11534355, 22178, NUITKA_BYTECODE_FLAG },
    { "termios", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "testpath", MOD_INIT_NAME( testpath ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "testpath.asserts", MOD_INIT_NAME( testpath$asserts ), 0, 0,  },
    { "testpath.commands", MOD_INIT_NAME( testpath$commands ), 0, 0,  },
    { "testpath.env", MOD_INIT_NAME( testpath$env ), 0, 0,  },
    { "testpath.tempdir", MOD_INIT_NAME( testpath$tempdir ), 0, 0,  },
    { "textwrap", NULL, 11556533, 13645, NUITKA_BYTECODE_FLAG },
    { "threading", NULL, 11570178, 37735, NUITKA_BYTECODE_FLAG },
    { "timeit", NULL, 11607913, 11680, NUITKA_BYTECODE_FLAG },
    { "tkinter", NULL, 11619593, 179208, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "tkinter.filedialog", NULL, 11798801, 12303, NUITKA_BYTECODE_FLAG },
    { "tkinter.messagebox", NULL, 11811104, 3085, NUITKA_BYTECODE_FLAG },
    { "tkinter.simpledialog", NULL, 11814189, 10579, NUITKA_BYTECODE_FLAG },
    { "token", NULL, 11824768, 3632, NUITKA_BYTECODE_FLAG },
    { "tokenize", NULL, 11828400, 17864, NUITKA_BYTECODE_FLAG },
    { "tokenizer", MOD_INIT_NAME( tokenizer ), 0, 0,  },
    { "tornado", MOD_INIT_NAME( tornado ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "tornado._locale_data", MOD_INIT_NAME( tornado$_locale_data ), 0, 0,  },
    { "tornado.autoreload", MOD_INIT_NAME( tornado$autoreload ), 0, 0,  },
    { "tornado.concurrent", MOD_INIT_NAME( tornado$concurrent ), 0, 0,  },
    { "tornado.escape", MOD_INIT_NAME( tornado$escape ), 0, 0,  },
    { "tornado.gen", MOD_INIT_NAME( tornado$gen ), 0, 0,  },
    { "tornado.http1connection", MOD_INIT_NAME( tornado$http1connection ), 0, 0,  },
    { "tornado.httpclient", MOD_INIT_NAME( tornado$httpclient ), 0, 0,  },
    { "tornado.httpserver", MOD_INIT_NAME( tornado$httpserver ), 0, 0,  },
    { "tornado.httputil", MOD_INIT_NAME( tornado$httputil ), 0, 0,  },
    { "tornado.ioloop", MOD_INIT_NAME( tornado$ioloop ), 0, 0,  },
    { "tornado.iostream", MOD_INIT_NAME( tornado$iostream ), 0, 0,  },
    { "tornado.locale", MOD_INIT_NAME( tornado$locale ), 0, 0,  },
    { "tornado.locks", MOD_INIT_NAME( tornado$locks ), 0, 0,  },
    { "tornado.log", MOD_INIT_NAME( tornado$log ), 0, 0,  },
    { "tornado.netutil", MOD_INIT_NAME( tornado$netutil ), 0, 0,  },
    { "tornado.options", MOD_INIT_NAME( tornado$options ), 0, 0,  },
    { "tornado.platform", MOD_INIT_NAME( tornado$platform ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "tornado.platform.asyncio", MOD_INIT_NAME( tornado$platform$asyncio ), 0, 0,  },
    { "tornado.platform.auto", MOD_INIT_NAME( tornado$platform$auto ), 0, 0,  },
    { "tornado.platform.posix", MOD_INIT_NAME( tornado$platform$posix ), 0, 0,  },
    { "tornado.platform.windows", MOD_INIT_NAME( tornado$platform$windows ), 0, 0,  },
    { "tornado.process", MOD_INIT_NAME( tornado$process ), 0, 0,  },
    { "tornado.queues", MOD_INIT_NAME( tornado$queues ), 0, 0,  },
    { "tornado.routing", MOD_INIT_NAME( tornado$routing ), 0, 0,  },
    { "tornado.simple_httpclient", MOD_INIT_NAME( tornado$simple_httpclient ), 0, 0,  },
    { "tornado.speedups", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "tornado.tcpclient", MOD_INIT_NAME( tornado$tcpclient ), 0, 0,  },
    { "tornado.tcpserver", MOD_INIT_NAME( tornado$tcpserver ), 0, 0,  },
    { "tornado.template", MOD_INIT_NAME( tornado$template ), 0, 0,  },
    { "tornado.util", MOD_INIT_NAME( tornado$util ), 0, 0,  },
    { "tornado.web", MOD_INIT_NAME( tornado$web ), 0, 0,  },
    { "tornado.websocket", MOD_INIT_NAME( tornado$websocket ), 0, 0,  },
    { "traceback", NULL, 11846264, 19656, NUITKA_BYTECODE_FLAG },
    { "traitlets", MOD_INIT_NAME( traitlets ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "traitlets._version", MOD_INIT_NAME( traitlets$_version ), 0, 0,  },
    { "traitlets.config", MOD_INIT_NAME( traitlets$config ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "traitlets.config.application", MOD_INIT_NAME( traitlets$config$application ), 0, 0,  },
    { "traitlets.config.configurable", MOD_INIT_NAME( traitlets$config$configurable ), 0, 0,  },
    { "traitlets.config.loader", MOD_INIT_NAME( traitlets$config$loader ), 0, 0,  },
    { "traitlets.log", MOD_INIT_NAME( traitlets$log ), 0, 0,  },
    { "traitlets.traitlets", MOD_INIT_NAME( traitlets$traitlets ), 0, 0,  },
    { "traitlets.utils", MOD_INIT_NAME( traitlets$utils ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "traitlets.utils.bunch", MOD_INIT_NAME( traitlets$utils$bunch ), 0, 0,  },
    { "traitlets.utils.getargspec", MOD_INIT_NAME( traitlets$utils$getargspec ), 0, 0,  },
    { "traitlets.utils.importstring", MOD_INIT_NAME( traitlets$utils$importstring ), 0, 0,  },
    { "traitlets.utils.sentinel", MOD_INIT_NAME( traitlets$utils$sentinel ), 0, 0,  },
    { "tty", NULL, 11865920, 1127, NUITKA_BYTECODE_FLAG },
    { "types", NULL, 11867047, 9009, NUITKA_BYTECODE_FLAG },
    { "typing", NULL, 11876056, 49991, NUITKA_BYTECODE_FLAG },
    { "typing_extensions", MOD_INIT_NAME( typing_extensions ), 0, 0,  },
    { "unicodedata", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "unittest", NULL, 11926047, 3044, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "unittest.case", NULL, 11929091, 48116, NUITKA_BYTECODE_FLAG },
    { "unittest.mock", NULL, 11977207, 63239, NUITKA_BYTECODE_FLAG },
    { "urllib", NULL, 12040446, 175, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "urllib.error", NULL, 12040621, 2809, NUITKA_BYTECODE_FLAG },
    { "urllib.parse", NULL, 12043430, 30463, NUITKA_BYTECODE_FLAG },
    { "urllib.request", NULL, 12073893, 72337, NUITKA_BYTECODE_FLAG },
    { "urllib3", MOD_INIT_NAME( urllib3 ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "urllib3._collections", MOD_INIT_NAME( urllib3$_collections ), 0, 0,  },
    { "urllib3.connection", MOD_INIT_NAME( urllib3$connection ), 0, 0,  },
    { "urllib3.connectionpool", MOD_INIT_NAME( urllib3$connectionpool ), 0, 0,  },
    { "urllib3.contrib", MOD_INIT_NAME( urllib3$contrib ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "urllib3.contrib._appengine_environ", MOD_INIT_NAME( urllib3$contrib$_appengine_environ ), 0, 0,  },
    { "urllib3.contrib.pyopenssl", MOD_INIT_NAME( urllib3$contrib$pyopenssl ), 0, 0,  },
    { "urllib3.contrib.socks", MOD_INIT_NAME( urllib3$contrib$socks ), 0, 0,  },
    { "urllib3.exceptions", MOD_INIT_NAME( urllib3$exceptions ), 0, 0,  },
    { "urllib3.fields", MOD_INIT_NAME( urllib3$fields ), 0, 0,  },
    { "urllib3.filepost", MOD_INIT_NAME( urllib3$filepost ), 0, 0,  },
    { "urllib3.packages", MOD_INIT_NAME( urllib3$packages ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "urllib3.packages.backports", MOD_INIT_NAME( urllib3$packages$backports ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "urllib3.packages.backports.makefile", MOD_INIT_NAME( urllib3$packages$backports$makefile ), 0, 0,  },
    { "urllib3.packages.six", MOD_INIT_NAME( urllib3$packages$six ), 0, 0,  },
    { "urllib3.packages.ssl_match_hostname", MOD_INIT_NAME( urllib3$packages$ssl_match_hostname ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "urllib3.packages.ssl_match_hostname._implementation", MOD_INIT_NAME( urllib3$packages$ssl_match_hostname$_implementation ), 0, 0,  },
    { "urllib3.poolmanager", MOD_INIT_NAME( urllib3$poolmanager ), 0, 0,  },
    { "urllib3.request", MOD_INIT_NAME( urllib3$request ), 0, 0,  },
    { "urllib3.response", MOD_INIT_NAME( urllib3$response ), 0, 0,  },
    { "urllib3.util", MOD_INIT_NAME( urllib3$util ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "urllib3.util.connection", MOD_INIT_NAME( urllib3$util$connection ), 0, 0,  },
    { "urllib3.util.queue", MOD_INIT_NAME( urllib3$util$queue ), 0, 0,  },
    { "urllib3.util.request", MOD_INIT_NAME( urllib3$util$request ), 0, 0,  },
    { "urllib3.util.response", MOD_INIT_NAME( urllib3$util$response ), 0, 0,  },
    { "urllib3.util.retry", MOD_INIT_NAME( urllib3$util$retry ), 0, 0,  },
    { "urllib3.util.ssl_", MOD_INIT_NAME( urllib3$util$ssl_ ), 0, 0,  },
    { "urllib3.util.timeout", MOD_INIT_NAME( urllib3$util$timeout ), 0, 0,  },
    { "urllib3.util.url", MOD_INIT_NAME( urllib3$util$url ), 0, 0,  },
    { "urllib3.util.wait", MOD_INIT_NAME( urllib3$util$wait ), 0, 0,  },
    { "utils", MOD_INIT_NAME( utils ), 0, 0,  },
    { "uuid", NULL, 12146230, 23232, NUITKA_BYTECODE_FLAG },
    { "warnings", NULL, 12169462, 13973, NUITKA_BYTECODE_FLAG },
    { "wave", NULL, 12183435, 18321, NUITKA_BYTECODE_FLAG },
    { "wcwidth", MOD_INIT_NAME( wcwidth ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "wcwidth.table_wide", MOD_INIT_NAME( wcwidth$table_wide ), 0, 0,  },
    { "wcwidth.table_zero", MOD_INIT_NAME( wcwidth$table_zero ), 0, 0,  },
    { "wcwidth.wcwidth", MOD_INIT_NAME( wcwidth$wcwidth ), 0, 0,  },
    { "weakref", NULL, 12201756, 19251, NUITKA_BYTECODE_FLAG },
    { "webbrowser", NULL, 12221007, 16407, NUITKA_BYTECODE_FLAG },
    { "xml", NULL, 12237414, 739, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.dom", NULL, 12238153, 5491, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.dom.expatbuilder", NULL, 12243644, 27053, NUITKA_BYTECODE_FLAG },
    { "xml.dom.minidom", NULL, 12270697, 55652, NUITKA_BYTECODE_FLAG },
    { "xml.dom.pulldom", NULL, 12326349, 10525, NUITKA_BYTECODE_FLAG },
    { "xml.etree", NULL, 12336874, 178, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.etree.ElementTree", NULL, 12337052, 44846, NUITKA_BYTECODE_FLAG },
    { "xml.etree.cElementTree", NULL, 12381898, 220, NUITKA_BYTECODE_FLAG },
    { "xml.parsers", NULL, 12382118, 352, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.parsers.expat", NULL, 12382470, 381, NUITKA_BYTECODE_FLAG },
    { "xml.sax", NULL, 12382851, 3211, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.sax.expatreader", NULL, 12386062, 12423, NUITKA_BYTECODE_FLAG },
    { "xmlrpc", NULL, 12398485, 175, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xmlrpc.client", NULL, 12398660, 34581, NUITKA_BYTECODE_FLAG },
    { "yargy", MOD_INIT_NAME( yargy ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "yargy.api", MOD_INIT_NAME( yargy$api ), 0, 0,  },
    { "yargy.compat", MOD_INIT_NAME( yargy$compat ), 0, 0,  },
    { "yargy.dot", MOD_INIT_NAME( yargy$dot ), 0, 0,  },
    { "yargy.interpretation", MOD_INIT_NAME( yargy$interpretation ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "yargy.interpretation.attribute", MOD_INIT_NAME( yargy$interpretation$attribute ), 0, 0,  },
    { "yargy.interpretation.fact", MOD_INIT_NAME( yargy$interpretation$fact ), 0, 0,  },
    { "yargy.interpretation.interpretator", MOD_INIT_NAME( yargy$interpretation$interpretator ), 0, 0,  },
    { "yargy.interpretation.normalizer", MOD_INIT_NAME( yargy$interpretation$normalizer ), 0, 0,  },
    { "yargy.morph", MOD_INIT_NAME( yargy$morph ), 0, 0,  },
    { "yargy.parser", MOD_INIT_NAME( yargy$parser ), 0, 0,  },
    { "yargy.pipelines", MOD_INIT_NAME( yargy$pipelines ), 0, 0,  },
    { "yargy.predicates", MOD_INIT_NAME( yargy$predicates ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "yargy.predicates.bank", MOD_INIT_NAME( yargy$predicates$bank ), 0, 0,  },
    { "yargy.predicates.constructors", MOD_INIT_NAME( yargy$predicates$constructors ), 0, 0,  },
    { "yargy.relations", MOD_INIT_NAME( yargy$relations ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "yargy.relations.bank", MOD_INIT_NAME( yargy$relations$bank ), 0, 0,  },
    { "yargy.relations.constructors", MOD_INIT_NAME( yargy$relations$constructors ), 0, 0,  },
    { "yargy.relations.graph", MOD_INIT_NAME( yargy$relations$graph ), 0, 0,  },
    { "yargy.rule", MOD_INIT_NAME( yargy$rule ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "yargy.rule.bnf", MOD_INIT_NAME( yargy$rule$bnf ), 0, 0,  },
    { "yargy.rule.constructors", MOD_INIT_NAME( yargy$rule$constructors ), 0, 0,  },
    { "yargy.rule.transformators", MOD_INIT_NAME( yargy$rule$transformators ), 0, 0,  },
    { "yargy.span", MOD_INIT_NAME( yargy$span ), 0, 0,  },
    { "yargy.tagger", MOD_INIT_NAME( yargy$tagger ), 0, 0,  },
    { "yargy.token", MOD_INIT_NAME( yargy$token ), 0, 0,  },
    { "yargy.tokenizer", MOD_INIT_NAME( yargy$tokenizer ), 0, 0,  },
    { "yargy.tree", MOD_INIT_NAME( yargy$tree ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "yargy.tree.constructors", MOD_INIT_NAME( yargy$tree$constructors ), 0, 0,  },
    { "yargy.tree.transformators", MOD_INIT_NAME( yargy$tree$transformators ), 0, 0,  },
    { "yargy.utils", MOD_INIT_NAME( yargy$utils ), 0, 0,  },
    { "yargy.visitor", MOD_INIT_NAME( yargy$visitor ), 0, 0,  },
    { "zipfile", NULL, 12433241, 49905, NUITKA_BYTECODE_FLAG },
    { "zlib", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq", MOD_INIT_NAME( zmq ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.backend", MOD_INIT_NAME( zmq$backend ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.backend.cython", MOD_INIT_NAME( zmq$backend$cython ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.backend.cython._device", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython._poll", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython._proxy_steerable", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython._version", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython.constants", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython.context", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython.error", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython.message", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython.socket", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.cython.utils", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.backend.select", MOD_INIT_NAME( zmq$backend$select ), 0, 0,  },
    { "zmq.error", MOD_INIT_NAME( zmq$error ), 0, 0,  },
    { "zmq.eventloop", MOD_INIT_NAME( zmq$eventloop ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.eventloop._deprecated", MOD_INIT_NAME( zmq$eventloop$_deprecated ), 0, 0,  },
    { "zmq.eventloop.ioloop", MOD_INIT_NAME( zmq$eventloop$ioloop ), 0, 0,  },
    { "zmq.eventloop.minitornado", MOD_INIT_NAME( zmq$eventloop$minitornado ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.eventloop.minitornado.concurrent", MOD_INIT_NAME( zmq$eventloop$minitornado$concurrent ), 0, 0,  },
    { "zmq.eventloop.minitornado.ioloop", MOD_INIT_NAME( zmq$eventloop$minitornado$ioloop ), 0, 0,  },
    { "zmq.eventloop.minitornado.log", MOD_INIT_NAME( zmq$eventloop$minitornado$log ), 0, 0,  },
    { "zmq.eventloop.minitornado.platform", MOD_INIT_NAME( zmq$eventloop$minitornado$platform ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.eventloop.minitornado.platform.auto", MOD_INIT_NAME( zmq$eventloop$minitornado$platform$auto ), 0, 0,  },
    { "zmq.eventloop.minitornado.platform.common", MOD_INIT_NAME( zmq$eventloop$minitornado$platform$common ), 0, 0,  },
    { "zmq.eventloop.minitornado.platform.interface", MOD_INIT_NAME( zmq$eventloop$minitornado$platform$interface ), 0, 0,  },
    { "zmq.eventloop.minitornado.platform.posix", MOD_INIT_NAME( zmq$eventloop$minitornado$platform$posix ), 0, 0,  },
    { "zmq.eventloop.minitornado.platform.windows", MOD_INIT_NAME( zmq$eventloop$minitornado$platform$windows ), 0, 0,  },
    { "zmq.eventloop.minitornado.stack_context", MOD_INIT_NAME( zmq$eventloop$minitornado$stack_context ), 0, 0,  },
    { "zmq.eventloop.minitornado.util", MOD_INIT_NAME( zmq$eventloop$minitornado$util ), 0, 0,  },
    { "zmq.eventloop.zmqstream", MOD_INIT_NAME( zmq$eventloop$zmqstream ), 0, 0,  },
    { "zmq.libzmq", NULL, 0, 0, NUITKA_SHLIB_FLAG },
    { "zmq.ssh", MOD_INIT_NAME( zmq$ssh ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.ssh.forward", MOD_INIT_NAME( zmq$ssh$forward ), 0, 0,  },
    { "zmq.ssh.tunnel", MOD_INIT_NAME( zmq$ssh$tunnel ), 0, 0,  },
    { "zmq.sugar", MOD_INIT_NAME( zmq$sugar ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.sugar.attrsettr", MOD_INIT_NAME( zmq$sugar$attrsettr ), 0, 0,  },
    { "zmq.sugar.constants", MOD_INIT_NAME( zmq$sugar$constants ), 0, 0,  },
    { "zmq.sugar.context", MOD_INIT_NAME( zmq$sugar$context ), 0, 0,  },
    { "zmq.sugar.frame", MOD_INIT_NAME( zmq$sugar$frame ), 0, 0,  },
    { "zmq.sugar.poll", MOD_INIT_NAME( zmq$sugar$poll ), 0, 0,  },
    { "zmq.sugar.socket", MOD_INIT_NAME( zmq$sugar$socket ), 0, 0,  },
    { "zmq.sugar.stopwatch", MOD_INIT_NAME( zmq$sugar$stopwatch ), 0, 0,  },
    { "zmq.sugar.tracker", MOD_INIT_NAME( zmq$sugar$tracker ), 0, 0,  },
    { "zmq.sugar.version", MOD_INIT_NAME( zmq$sugar$version ), 0, 0,  },
    { "zmq.utils", MOD_INIT_NAME( zmq$utils ), 0, 0, NUITKA_PACKAGE_FLAG },
    { "zmq.utils.constant_names", MOD_INIT_NAME( zmq$utils$constant_names ), 0, 0,  },
    { "zmq.utils.interop", MOD_INIT_NAME( zmq$utils$interop ), 0, 0,  },
    { "zmq.utils.jsonapi", MOD_INIT_NAME( zmq$utils$jsonapi ), 0, 0,  },
    { "zmq.utils.sixcerpt", MOD_INIT_NAME( zmq$utils$sixcerpt ), 0, 0,  },
    { "zmq.utils.strtypes", MOD_INIT_NAME( zmq$utils$strtypes ), 0, 0,  },
    { "__future__", NULL, 7151413, 4165, NUITKA_BYTECODE_FLAG },
    { "_bootlocale", NULL, 12483146, 1282, NUITKA_BYTECODE_FLAG },
    { "_compat_pickle", NULL, 12484428, 5841, NUITKA_BYTECODE_FLAG },
    { "_dummy_thread", NULL, 12490269, 4886, NUITKA_BYTECODE_FLAG },
    { "_markupbase", NULL, 12495155, 7818, NUITKA_BYTECODE_FLAG },
    { "_osx_support", NULL, 12502973, 9628, NUITKA_BYTECODE_FLAG },
    { "_py_abc", NULL, 12512601, 4687, NUITKA_BYTECODE_FLAG },
    { "_pyio", NULL, 12517288, 72851, NUITKA_BYTECODE_FLAG },
    { "_sitebuiltins", NULL, 12590139, 3498, NUITKA_BYTECODE_FLAG },
    { "_strptime", NULL, 12593637, 16137, NUITKA_BYTECODE_FLAG },
    { "_sysconfigdata_m_darwin_darwin", NULL, 12609774, 22755, NUITKA_BYTECODE_FLAG },
    { "_threading_local", NULL, 12632529, 6445, NUITKA_BYTECODE_FLAG },
    { "aifc", NULL, 12638974, 26176, NUITKA_BYTECODE_FLAG },
    { "argparse", NULL, 7191037, 61946, NUITKA_BYTECODE_FLAG },
    { "ast", NULL, 7252983, 12113, NUITKA_BYTECODE_FLAG },
    { "asynchat", NULL, 12665150, 6867, NUITKA_BYTECODE_FLAG },
    { "asyncio", NULL, 7265096, 723, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "asyncio.base_events", NULL, 12672017, 48149, NUITKA_BYTECODE_FLAG },
    { "asyncio.base_futures", NULL, 12720166, 2135, NUITKA_BYTECODE_FLAG },
    { "asyncio.base_subprocess", NULL, 12722301, 9224, NUITKA_BYTECODE_FLAG },
    { "asyncio.base_tasks", NULL, 12731525, 1899, NUITKA_BYTECODE_FLAG },
    { "asyncio.constants", NULL, 12733424, 624, NUITKA_BYTECODE_FLAG },
    { "asyncio.coroutines", NULL, 12734048, 6411, NUITKA_BYTECODE_FLAG },
    { "asyncio.events", NULL, 12740459, 27888, NUITKA_BYTECODE_FLAG },
    { "asyncio.format_helpers", NULL, 12768347, 2350, NUITKA_BYTECODE_FLAG },
    { "asyncio.futures", NULL, 12770697, 10778, NUITKA_BYTECODE_FLAG },
    { "asyncio.locks", NULL, 12781475, 15946, NUITKA_BYTECODE_FLAG },
    { "asyncio.log", NULL, 12797421, 273, NUITKA_BYTECODE_FLAG },
    { "asyncio.proactor_events", NULL, 12797694, 20144, NUITKA_BYTECODE_FLAG },
    { "asyncio.protocols", NULL, 12817838, 8761, NUITKA_BYTECODE_FLAG },
    { "asyncio.queues", NULL, 12826599, 8206, NUITKA_BYTECODE_FLAG },
    { "asyncio.runners", NULL, 12834805, 1947, NUITKA_BYTECODE_FLAG },
    { "asyncio.selector_events", NULL, 12836752, 28156, NUITKA_BYTECODE_FLAG },
    { "asyncio.sslproto", NULL, 12864908, 21244, NUITKA_BYTECODE_FLAG },
    { "asyncio.streams", NULL, 12886152, 20321, NUITKA_BYTECODE_FLAG },
    { "asyncio.subprocess", NULL, 12906473, 6785, NUITKA_BYTECODE_FLAG },
    { "asyncio.tasks", NULL, 12913258, 21899, NUITKA_BYTECODE_FLAG },
    { "asyncio.transports", NULL, 12935157, 12244, NUITKA_BYTECODE_FLAG },
    { "asyncio.unix_events", NULL, 12947401, 32139, NUITKA_BYTECODE_FLAG },
    { "asyncore", NULL, 12979540, 15877, NUITKA_BYTECODE_FLAG },
    { "bdb", NULL, 7282840, 24427, NUITKA_BYTECODE_FLAG },
    { "binhex", NULL, 12995417, 12092, NUITKA_BYTECODE_FLAG },
    { "bisect", NULL, 7307267, 2731, NUITKA_BYTECODE_FLAG },
    { "cProfile", NULL, 7321212, 4584, NUITKA_BYTECODE_FLAG },
    { "calendar", NULL, 7325796, 27457, NUITKA_BYTECODE_FLAG },
    { "cgi", NULL, 7353253, 27224, NUITKA_BYTECODE_FLAG },
    { "cgitb", NULL, 13007509, 10145, NUITKA_BYTECODE_FLAG },
    { "chunk", NULL, 13017654, 4952, NUITKA_BYTECODE_FLAG },
    { "cmd", NULL, 13022606, 12623, NUITKA_BYTECODE_FLAG },
    { "code", NULL, 13035229, 9891, NUITKA_BYTECODE_FLAG },
    { "codeop", NULL, 7414408, 6323, NUITKA_BYTECODE_FLAG },
    { "colorsys", NULL, 7467374, 3330, NUITKA_BYTECODE_FLAG },
    { "compileall", NULL, 13045120, 9139, NUITKA_BYTECODE_FLAG },
    { "concurrent", NULL, 7470704, 179, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "concurrent.futures", NULL, 7470883, 1119, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "concurrent.futures._base", NULL, 13054259, 20916, NUITKA_BYTECODE_FLAG },
    { "concurrent.futures.process", NULL, 13075175, 19594, NUITKA_BYTECODE_FLAG },
    { "concurrent.futures.thread", NULL, 13094769, 5207, NUITKA_BYTECODE_FLAG },
    { "configparser", NULL, 7472002, 45913, NUITKA_BYTECODE_FLAG },
    { "contextlib", NULL, 7517915, 19933, NUITKA_BYTECODE_FLAG },
    { "contextvars", NULL, 13099976, 290, NUITKA_BYTECODE_FLAG },
    { "copy", NULL, 7537848, 7134, NUITKA_BYTECODE_FLAG },
    { "crypt", NULL, 13100266, 3167, NUITKA_BYTECODE_FLAG },
    { "csv", NULL, 7549259, 11865, NUITKA_BYTECODE_FLAG },
    { "ctypes", NULL, 7561124, 16182, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "ctypes._aix", NULL, 13103433, 9856, NUITKA_BYTECODE_FLAG },
    { "ctypes._endian", NULL, 13113289, 1989, NUITKA_BYTECODE_FLAG },
    { "ctypes.macholib", NULL, 13115278, 346, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "ctypes.macholib.dyld", NULL, 13115624, 4410, NUITKA_BYTECODE_FLAG },
    { "ctypes.macholib.dylib", NULL, 13120034, 1973, NUITKA_BYTECODE_FLAG },
    { "ctypes.macholib.framework", NULL, 13122007, 2253, NUITKA_BYTECODE_FLAG },
    { "ctypes.util", NULL, 7577306, 7782, NUITKA_BYTECODE_FLAG },
    { "curses", NULL, 7585088, 1828, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "curses.ascii", NULL, 13124260, 3936, NUITKA_BYTECODE_FLAG },
    { "curses.has_key", NULL, 13128196, 4300, NUITKA_BYTECODE_FLAG },
    { "curses.panel", NULL, 13132496, 270, NUITKA_BYTECODE_FLAG },
    { "curses.textpad", NULL, 13132766, 5939, NUITKA_BYTECODE_FLAG },
    { "dataclasses", NULL, 13138705, 22498, NUITKA_BYTECODE_FLAG },
    { "datetime", NULL, 7586916, 57282, NUITKA_BYTECODE_FLAG },
    { "dbm", NULL, 13161203, 4199, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "dbm.dumb", NULL, 13165402, 8433, NUITKA_BYTECODE_FLAG },
    { "dbm.gnu", NULL, 13173835, 250, NUITKA_BYTECODE_FLAG },
    { "dbm.ndbm", NULL, 13174085, 249, NUITKA_BYTECODE_FLAG },
    { "decimal", NULL, 13174334, 162205, NUITKA_BYTECODE_FLAG },
    { "difflib", NULL, 7644198, 59471, NUITKA_BYTECODE_FLAG },
    { "distutils", NULL, 7718907, 431, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "distutils.archive_util", NULL, 13336539, 6565, NUITKA_BYTECODE_FLAG },
    { "distutils.bcppcompiler", NULL, 13343104, 6534, NUITKA_BYTECODE_FLAG },
    { "distutils.ccompiler", NULL, 7719338, 33249, NUITKA_BYTECODE_FLAG },
    { "distutils.cmd", NULL, 7752587, 13941, NUITKA_BYTECODE_FLAG },
    { "distutils.command", NULL, 7766528, 588, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "distutils.command.bdist", NULL, 7767116, 3687, NUITKA_BYTECODE_FLAG },
    { "distutils.command.bdist_dumb", NULL, 13349638, 3602, NUITKA_BYTECODE_FLAG },
    { "distutils.command.bdist_rpm", NULL, 7770803, 12527, NUITKA_BYTECODE_FLAG },
    { "distutils.command.bdist_wininst", NULL, 13353240, 8042, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build", NULL, 7783330, 3868, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_clib", NULL, 7787198, 4918, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_ext", NULL, 7792116, 15825, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_py", NULL, 7807941, 10424, NUITKA_BYTECODE_FLAG },
    { "distutils.command.build_scripts", NULL, 7818365, 4323, NUITKA_BYTECODE_FLAG },
    { "distutils.command.check", NULL, 13361282, 4843, NUITKA_BYTECODE_FLAG },
    { "distutils.command.clean", NULL, 13366125, 2125, NUITKA_BYTECODE_FLAG },
    { "distutils.command.config", NULL, 7822688, 10229, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install", NULL, 7832917, 13552, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_data", NULL, 7846469, 2318, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_egg_info", NULL, 13368250, 3016, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_headers", NULL, 7848787, 1733, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_lib", NULL, 13371266, 5111, NUITKA_BYTECODE_FLAG },
    { "distutils.command.install_scripts", NULL, 7850520, 2175, NUITKA_BYTECODE_FLAG },
    { "distutils.command.register", NULL, 13376377, 8510, NUITKA_BYTECODE_FLAG },
    { "distutils.command.sdist", NULL, 7852695, 14541, NUITKA_BYTECODE_FLAG },
    { "distutils.command.upload", NULL, 13384887, 5123, NUITKA_BYTECODE_FLAG },
    { "distutils.config", NULL, 13390010, 3518, NUITKA_BYTECODE_FLAG },
    { "distutils.core", NULL, 7867236, 6643, NUITKA_BYTECODE_FLAG },
    { "distutils.cygwinccompiler", NULL, 7873879, 8561, NUITKA_BYTECODE_FLAG },
    { "distutils.debug", NULL, 7882440, 241, NUITKA_BYTECODE_FLAG },
    { "distutils.dep_util", NULL, 7882681, 2757, NUITKA_BYTECODE_FLAG },
    { "distutils.dir_util", NULL, 7885438, 5851, NUITKA_BYTECODE_FLAG },
    { "distutils.dist", NULL, 7891289, 34472, NUITKA_BYTECODE_FLAG },
    { "distutils.errors", NULL, 7925761, 5527, NUITKA_BYTECODE_FLAG },
    { "distutils.extension", NULL, 7931288, 6938, NUITKA_BYTECODE_FLAG },
    { "distutils.fancy_getopt", NULL, 7938226, 10650, NUITKA_BYTECODE_FLAG },
    { "distutils.file_util", NULL, 7948876, 5936, NUITKA_BYTECODE_FLAG },
    { "distutils.filelist", NULL, 7954812, 9871, NUITKA_BYTECODE_FLAG },
    { "distutils.log", NULL, 7964683, 2352, NUITKA_BYTECODE_FLAG },
    { "distutils.msvccompiler", NULL, 7967035, 14604, NUITKA_BYTECODE_FLAG },
    { "distutils.spawn", NULL, 7981639, 5042, NUITKA_BYTECODE_FLAG },
    { "distutils.sysconfig", NULL, 7986681, 12016, NUITKA_BYTECODE_FLAG },
    { "distutils.text_file", NULL, 13393528, 8478, NUITKA_BYTECODE_FLAG },
    { "distutils.unixccompiler", NULL, 7998697, 6573, NUITKA_BYTECODE_FLAG },
    { "distutils.util", NULL, 8005270, 15134, NUITKA_BYTECODE_FLAG },
    { "distutils.version", NULL, 8020404, 7389, NUITKA_BYTECODE_FLAG },
    { "distutils.versionpredicate", NULL, 13402006, 5136, NUITKA_BYTECODE_FLAG },
    { "doctest", NULL, 8027793, 75467, NUITKA_BYTECODE_FLAG },
    { "dummy_threading", NULL, 8103260, 1157, NUITKA_BYTECODE_FLAG },
    { "email", NULL, 8104417, 1724, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "email._encoded_words", NULL, 13407142, 5641, NUITKA_BYTECODE_FLAG },
    { "email._header_value_parser", NULL, 13412783, 75663, NUITKA_BYTECODE_FLAG },
    { "email._parseaddr", NULL, 13488446, 12381, NUITKA_BYTECODE_FLAG },
    { "email._policybase", NULL, 13500827, 14883, NUITKA_BYTECODE_FLAG },
    { "email.base64mime", NULL, 13515710, 3268, NUITKA_BYTECODE_FLAG },
    { "email.charset", NULL, 13518978, 11562, NUITKA_BYTECODE_FLAG },
    { "email.contentmanager", NULL, 13530540, 7328, NUITKA_BYTECODE_FLAG },
    { "email.encoders", NULL, 13537868, 1697, NUITKA_BYTECODE_FLAG },
    { "email.errors", NULL, 13539565, 6224, NUITKA_BYTECODE_FLAG },
    { "email.feedparser", NULL, 13545789, 10658, NUITKA_BYTECODE_FLAG },
    { "email.generator", NULL, 13556447, 12533, NUITKA_BYTECODE_FLAG },
    { "email.header", NULL, 13568980, 16413, NUITKA_BYTECODE_FLAG },
    { "email.headerregistry", NULL, 13585393, 21330, NUITKA_BYTECODE_FLAG },
    { "email.iterators", NULL, 13606723, 1965, NUITKA_BYTECODE_FLAG },
    { "email.message", NULL, 13608688, 37973, NUITKA_BYTECODE_FLAG },
    { "email.mime", NULL, 13646661, 179, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "email.mime.application", NULL, 13646840, 1490, NUITKA_BYTECODE_FLAG },
    { "email.mime.audio", NULL, 13648330, 2649, NUITKA_BYTECODE_FLAG },
    { "email.mime.base", NULL, 13650979, 1115, NUITKA_BYTECODE_FLAG },
    { "email.mime.image", NULL, 13652094, 1935, NUITKA_BYTECODE_FLAG },
    { "email.mime.message", NULL, 13654029, 1364, NUITKA_BYTECODE_FLAG },
    { "email.mime.multipart", NULL, 13655393, 1586, NUITKA_BYTECODE_FLAG },
    { "email.mime.nonmultipart", NULL, 13656979, 801, NUITKA_BYTECODE_FLAG },
    { "email.mime.text", NULL, 13657780, 1348, NUITKA_BYTECODE_FLAG },
    { "email.parser", NULL, 8106141, 5780, NUITKA_BYTECODE_FLAG },
    { "email.policy", NULL, 13659128, 9673, NUITKA_BYTECODE_FLAG },
    { "email.quoprimime", NULL, 13668801, 7697, NUITKA_BYTECODE_FLAG },
    { "email.utils", NULL, 8111921, 9500, NUITKA_BYTECODE_FLAG },
    { "filecmp", NULL, 8155462, 8340, NUITKA_BYTECODE_FLAG },
    { "fileinput", NULL, 8163802, 13231, NUITKA_BYTECODE_FLAG },
    { "fnmatch", NULL, 8177033, 3370, NUITKA_BYTECODE_FLAG },
    { "formatter", NULL, 13676498, 17586, NUITKA_BYTECODE_FLAG },
    { "fractions", NULL, 8180403, 18461, NUITKA_BYTECODE_FLAG },
    { "ftplib", NULL, 13694084, 28100, NUITKA_BYTECODE_FLAG },
    { "getopt", NULL, 8222852, 6272, NUITKA_BYTECODE_FLAG },
    { "getpass", NULL, 8229124, 4197, NUITKA_BYTECODE_FLAG },
    { "gettext", NULL, 8233321, 14201, NUITKA_BYTECODE_FLAG },
    { "glob", NULL, 8247522, 4292, NUITKA_BYTECODE_FLAG },
    { "gzip", NULL, 8251814, 17218, NUITKA_BYTECODE_FLAG },
    { "hashlib", NULL, 8269032, 6624, NUITKA_BYTECODE_FLAG },
    { "hmac", NULL, 8290051, 6146, NUITKA_BYTECODE_FLAG },
    { "html", NULL, 8296197, 3430, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "html.entities", NULL, 8299627, 50502, NUITKA_BYTECODE_FLAG },
    { "html.parser", NULL, 8350129, 11140, NUITKA_BYTECODE_FLAG },
    { "http", NULL, 8361269, 5955, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "http.client", NULL, 8367224, 34081, NUITKA_BYTECODE_FLAG },
    { "http.cookiejar", NULL, 13722184, 53618, NUITKA_BYTECODE_FLAG },
    { "http.cookies", NULL, 8401305, 15277, NUITKA_BYTECODE_FLAG },
    { "http.server", NULL, 13775802, 33400, NUITKA_BYTECODE_FLAG },
    { "imaplib", NULL, 13809202, 41463, NUITKA_BYTECODE_FLAG },
    { "imghdr", NULL, 13850665, 4175, NUITKA_BYTECODE_FLAG },
    { "imp", NULL, 8416582, 9786, NUITKA_BYTECODE_FLAG },
    { "importlib.abc", NULL, 13854840, 13520, NUITKA_BYTECODE_FLAG },
    { "importlib.resources", NULL, 13868360, 8372, NUITKA_BYTECODE_FLAG },
    { "importlib.util", NULL, 8460349, 9389, NUITKA_BYTECODE_FLAG },
    { "ipaddress", NULL, 8553245, 63027, NUITKA_BYTECODE_FLAG },
    { "json", NULL, 8616272, 12371, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "json.decoder", NULL, 13876732, 9857, NUITKA_BYTECODE_FLAG },
    { "json.encoder", NULL, 13886589, 11339, NUITKA_BYTECODE_FLAG },
    { "json.scanner", NULL, 13897928, 2029, NUITKA_BYTECODE_FLAG },
    { "json.tool", NULL, 13899957, 1510, NUITKA_BYTECODE_FLAG },
    { "lib2to3", NULL, 13901467, 176, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "lib2to3.btm_matcher", NULL, 13901643, 4926, NUITKA_BYTECODE_FLAG },
    { "lib2to3.btm_utils", NULL, 13906569, 6176, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixer_base", NULL, 13912745, 6268, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixer_util", NULL, 13919013, 12079, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes", NULL, 13931092, 182, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "lib2to3.fixes.fix_apply", NULL, 13931274, 1720, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_asserts", NULL, 13932994, 1305, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_basestring", NULL, 13934299, 695, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_buffer", NULL, 13934994, 840, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_dict", NULL, 13935834, 3348, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_except", NULL, 13939182, 2850, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_exec", NULL, 13942032, 1181, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_execfile", NULL, 13943213, 1725, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_exitfunc", NULL, 13944938, 2328, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_filter", NULL, 13947266, 2393, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_funcattrs", NULL, 13949659, 1006, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_future", NULL, 13950665, 816, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_getcwdu", NULL, 13951481, 820, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_has_key", NULL, 13952301, 2950, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_idioms", NULL, 13955251, 3935, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_import", NULL, 13959186, 2817, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_imports", NULL, 13962003, 4381, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_imports2", NULL, 13966384, 580, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_input", NULL, 13966964, 982, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_intern", NULL, 13967946, 1189, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_isinstance", NULL, 13969135, 1587, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_itertools", NULL, 13970722, 1576, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_itertools_imports", NULL, 13972298, 1612, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_long", NULL, 13973910, 737, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_map", NULL, 13974647, 3125, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_metaclass", NULL, 13977772, 5378, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_methodattrs", NULL, 13983150, 968, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_ne", NULL, 13984118, 839, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_next", NULL, 13984957, 3092, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_nonzero", NULL, 13988049, 955, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_numliterals", NULL, 13989004, 1051, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_operator", NULL, 13990055, 4268, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_paren", NULL, 13994323, 1422, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_print", NULL, 13995745, 2357, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_raise", NULL, 13998102, 2281, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_raw_input", NULL, 14000383, 827, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_reduce", NULL, 14001210, 1162, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_reload", NULL, 14002372, 1201, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_renames", NULL, 14003573, 2025, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_repr", NULL, 14005598, 877, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_set_literal", NULL, 14006475, 1709, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_standarderror", NULL, 14008184, 752, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_sys_exc", NULL, 14008936, 1433, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_throw", NULL, 14010369, 1834, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_tuple_params", NULL, 14012203, 4628, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_types", NULL, 14016831, 1861, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_unicode", NULL, 14018692, 1573, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_urllib", NULL, 14020265, 5993, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_ws_comma", NULL, 14026258, 1155, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_xrange", NULL, 14027413, 2568, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_xreadlines", NULL, 14029981, 1149, NUITKA_BYTECODE_FLAG },
    { "lib2to3.fixes.fix_zip", NULL, 14031130, 1613, NUITKA_BYTECODE_FLAG },
    { "lib2to3.main", NULL, 14032743, 8569, NUITKA_BYTECODE_FLAG },
    { "lib2to3.patcomp", NULL, 14041312, 5644, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2", NULL, 14046956, 212, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "lib2to3.pgen2.driver", NULL, 14047168, 5173, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2.grammar", NULL, 14052341, 7049, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2.literals", NULL, 14059390, 1591, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2.parse", NULL, 14060981, 6337, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2.pgen", NULL, 14067318, 9813, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2.token", NULL, 14077131, 1905, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pgen2.tokenize", NULL, 14079036, 15173, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pygram", NULL, 14094209, 1231, NUITKA_BYTECODE_FLAG },
    { "lib2to3.pytree", NULL, 14095440, 25036, NUITKA_BYTECODE_FLAG },
    { "lib2to3.refactor", NULL, 14120476, 20437, NUITKA_BYTECODE_FLAG },
    { "logging", NULL, 8668895, 62591, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "logging.config", NULL, 14140913, 23048, NUITKA_BYTECODE_FLAG },
    { "logging.handlers", NULL, 8731486, 43016, NUITKA_BYTECODE_FLAG },
    { "lzma", NULL, 8774502, 11972, NUITKA_BYTECODE_FLAG },
    { "macpath", NULL, 14163961, 5840, NUITKA_BYTECODE_FLAG },
    { "mailbox", NULL, 14169801, 63681, NUITKA_BYTECODE_FLAG },
    { "mailcap", NULL, 14233482, 6517, NUITKA_BYTECODE_FLAG },
    { "mimetypes", NULL, 8786474, 15511, NUITKA_BYTECODE_FLAG },
    { "modulefinder", NULL, 14239999, 15385, NUITKA_BYTECODE_FLAG },
    { "multiprocessing", NULL, 8801985, 560, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "multiprocessing.connection", NULL, 14255384, 24965, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.context", NULL, 14280349, 13146, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.dummy", NULL, 14293495, 3838, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "multiprocessing.dummy.connection", NULL, 14297333, 2546, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.forkserver", NULL, 14299879, 7797, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.heap", NULL, 14307676, 6457, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.managers", NULL, 14314133, 34037, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.pool", NULL, 8802545, 21269, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.popen_fork", NULL, 14348170, 2559, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.popen_forkserver", NULL, 14350729, 2392, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.popen_spawn_posix", NULL, 14353121, 2174, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.process", NULL, 14355295, 9459, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.queues", NULL, 14364754, 9470, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.reduction", NULL, 14374224, 8051, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.resource_sharer", NULL, 14382275, 5236, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.semaphore_tracker", NULL, 14387511, 3773, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.sharedctypes", NULL, 14391284, 6950, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.spawn", NULL, 14398234, 6501, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.synchronize", NULL, 14404735, 11216, NUITKA_BYTECODE_FLAG },
    { "multiprocessing.util", NULL, 14415951, 9980, NUITKA_BYTECODE_FLAG },
    { "netrc", NULL, 8823814, 3796, NUITKA_BYTECODE_FLAG },
    { "nntplib", NULL, 14425931, 33784, NUITKA_BYTECODE_FLAG },
    { "ntpath", NULL, 8827610, 13037, NUITKA_BYTECODE_FLAG },
    { "nturl2path", NULL, 14459715, 1648, NUITKA_BYTECODE_FLAG },
    { "numbers", NULL, 8840647, 12225, NUITKA_BYTECODE_FLAG },
    { "optparse", NULL, 9626201, 47926, NUITKA_BYTECODE_FLAG },
    { "pathlib", NULL, 9703853, 41665, NUITKA_BYTECODE_FLAG },
    { "pdb", NULL, 9745518, 46909, NUITKA_BYTECODE_FLAG },
    { "pickle", NULL, 9868210, 42999, NUITKA_BYTECODE_FLAG },
    { "pickletools", NULL, 14461363, 65364, NUITKA_BYTECODE_FLAG },
    { "pipes", NULL, 9911209, 7836, NUITKA_BYTECODE_FLAG },
    { "pkgutil", NULL, 10317735, 16393, NUITKA_BYTECODE_FLAG },
    { "platform", NULL, 10334128, 28253, NUITKA_BYTECODE_FLAG },
    { "plistlib", NULL, 10362381, 25128, NUITKA_BYTECODE_FLAG },
    { "poplib", NULL, 14526727, 13369, NUITKA_BYTECODE_FLAG },
    { "pprint", NULL, 10397972, 15866, NUITKA_BYTECODE_FLAG },
    { "profile", NULL, 10413838, 13874, NUITKA_BYTECODE_FLAG },
    { "pstats", NULL, 10427712, 22327, NUITKA_BYTECODE_FLAG },
    { "pty", NULL, 10450039, 3916, NUITKA_BYTECODE_FLAG },
    { "py_compile", NULL, 14540096, 7224, NUITKA_BYTECODE_FLAG },
    { "pyclbr", NULL, 14547320, 10406, NUITKA_BYTECODE_FLAG },
    { "pydoc", NULL, 10795874, 84463, NUITKA_BYTECODE_FLAG },
    { "pydoc_data", NULL, 10880337, 179, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "pydoc_data.topics", NULL, 14557726, 411629, NUITKA_BYTECODE_FLAG },
    { "queue", NULL, 10880516, 11505, NUITKA_BYTECODE_FLAG },
    { "random", NULL, 10892021, 19441, NUITKA_BYTECODE_FLAG },
    { "rlcompleter", NULL, 14969355, 5780, NUITKA_BYTECODE_FLAG },
    { "runpy", NULL, 10930682, 7978, NUITKA_BYTECODE_FLAG },
    { "sched", NULL, 14975135, 6554, NUITKA_BYTECODE_FLAG },
    { "secrets", NULL, 14981689, 2217, NUITKA_BYTECODE_FLAG },
    { "selectors", NULL, 10938660, 16981, NUITKA_BYTECODE_FLAG },
    { "shelve", NULL, 14983906, 9539, NUITKA_BYTECODE_FLAG },
    { "shlex", NULL, 11259690, 7023, NUITKA_BYTECODE_FLAG },
    { "shutil", NULL, 11266713, 30897, NUITKA_BYTECODE_FLAG },
    { "signal", NULL, 11297610, 2545, NUITKA_BYTECODE_FLAG },
    { "site", NULL, 11300155, 13289, NUITKA_BYTECODE_FLAG },
    { "smtplib", NULL, 14993445, 35383, NUITKA_BYTECODE_FLAG },
    { "sndhdr", NULL, 15028828, 6936, NUITKA_BYTECODE_FLAG },
    { "socket", NULL, 11313444, 22054, NUITKA_BYTECODE_FLAG },
    { "socketserver", NULL, 11335498, 24232, NUITKA_BYTECODE_FLAG },
    { "sqlite3", NULL, 11359730, 207, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "sqlite3.dbapi2", NULL, 15035764, 2526, NUITKA_BYTECODE_FLAG },
    { "sqlite3.dump", NULL, 15038290, 1969, NUITKA_BYTECODE_FLAG },
    { "ssl", NULL, 11366261, 39804, NUITKA_BYTECODE_FLAG },
    { "statistics", NULL, 15040259, 18197, NUITKA_BYTECODE_FLAG },
    { "string", NULL, 11409971, 7868, NUITKA_BYTECODE_FLAG },
    { "subprocess", NULL, 11418206, 38798, NUITKA_BYTECODE_FLAG },
    { "sunau", NULL, 15058456, 17244, NUITKA_BYTECODE_FLAG },
    { "symbol", NULL, 15075700, 2598, NUITKA_BYTECODE_FLAG },
    { "symtable", NULL, 15078298, 10478, NUITKA_BYTECODE_FLAG },
    { "sysconfig", NULL, 11457004, 15477, NUITKA_BYTECODE_FLAG },
    { "tabnanny", NULL, 15088776, 7011, NUITKA_BYTECODE_FLAG },
    { "tarfile", NULL, 11472481, 61874, NUITKA_BYTECODE_FLAG },
    { "telnetlib", NULL, 15095787, 18135, NUITKA_BYTECODE_FLAG },
    { "tempfile", NULL, 11534355, 22178, NUITKA_BYTECODE_FLAG },
    { "textwrap", NULL, 11556533, 13645, NUITKA_BYTECODE_FLAG },
    { "this", NULL, 15113922, 1310, NUITKA_BYTECODE_FLAG },
    { "timeit", NULL, 11607913, 11680, NUITKA_BYTECODE_FLAG },
    { "tkinter", NULL, 11619593, 179208, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "tkinter.colorchooser", NULL, 15115232, 1168, NUITKA_BYTECODE_FLAG },
    { "tkinter.commondialog", NULL, 15116400, 1149, NUITKA_BYTECODE_FLAG },
    { "tkinter.constants", NULL, 15117549, 1701, NUITKA_BYTECODE_FLAG },
    { "tkinter.dialog", NULL, 15119250, 1477, NUITKA_BYTECODE_FLAG },
    { "tkinter.dnd", NULL, 15120727, 11219, NUITKA_BYTECODE_FLAG },
    { "tkinter.filedialog", NULL, 11798801, 12303, NUITKA_BYTECODE_FLAG },
    { "tkinter.font", NULL, 15131946, 6220, NUITKA_BYTECODE_FLAG },
    { "tkinter.messagebox", NULL, 11811104, 3085, NUITKA_BYTECODE_FLAG },
    { "tkinter.scrolledtext", NULL, 15138166, 2212, NUITKA_BYTECODE_FLAG },
    { "tkinter.simpledialog", NULL, 11814189, 10579, NUITKA_BYTECODE_FLAG },
    { "tkinter.tix", NULL, 15140378, 83699, NUITKA_BYTECODE_FLAG },
    { "tkinter.ttk", NULL, 15224077, 57883, NUITKA_BYTECODE_FLAG },
    { "trace", NULL, 15281960, 19180, NUITKA_BYTECODE_FLAG },
    { "tracemalloc", NULL, 15301140, 17309, NUITKA_BYTECODE_FLAG },
    { "tty", NULL, 11865920, 1127, NUITKA_BYTECODE_FLAG },
    { "turtle", NULL, 15318449, 130824, NUITKA_BYTECODE_FLAG },
    { "typing", NULL, 11876056, 49991, NUITKA_BYTECODE_FLAG },
    { "unittest", NULL, 11926047, 3044, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "unittest.case", NULL, 11929091, 48116, NUITKA_BYTECODE_FLAG },
    { "unittest.loader", NULL, 15449273, 14309, NUITKA_BYTECODE_FLAG },
    { "unittest.main", NULL, 15463582, 7471, NUITKA_BYTECODE_FLAG },
    { "unittest.mock", NULL, 11977207, 63239, NUITKA_BYTECODE_FLAG },
    { "unittest.result", NULL, 15471053, 7285, NUITKA_BYTECODE_FLAG },
    { "unittest.runner", NULL, 15478338, 7028, NUITKA_BYTECODE_FLAG },
    { "unittest.signals", NULL, 15485366, 2227, NUITKA_BYTECODE_FLAG },
    { "unittest.suite", NULL, 15487593, 9236, NUITKA_BYTECODE_FLAG },
    { "unittest.util", NULL, 15496829, 4554, NUITKA_BYTECODE_FLAG },
    { "urllib", NULL, 12040446, 175, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "urllib.error", NULL, 12040621, 2809, NUITKA_BYTECODE_FLAG },
    { "urllib.parse", NULL, 12043430, 30463, NUITKA_BYTECODE_FLAG },
    { "urllib.request", NULL, 12073893, 72337, NUITKA_BYTECODE_FLAG },
    { "urllib.response", NULL, 15501383, 3282, NUITKA_BYTECODE_FLAG },
    { "urllib.robotparser", NULL, 15504665, 7094, NUITKA_BYTECODE_FLAG },
    { "uu", NULL, 15511759, 3605, NUITKA_BYTECODE_FLAG },
    { "uuid", NULL, 12146230, 23232, NUITKA_BYTECODE_FLAG },
    { "venv", NULL, 15515364, 13870, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "wave", NULL, 12183435, 18321, NUITKA_BYTECODE_FLAG },
    { "weakref", NULL, 12201756, 19251, NUITKA_BYTECODE_FLAG },
    { "webbrowser", NULL, 12221007, 16407, NUITKA_BYTECODE_FLAG },
    { "wsgiref", NULL, 15529234, 771, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "wsgiref.handlers", NULL, 15530005, 16180, NUITKA_BYTECODE_FLAG },
    { "wsgiref.headers", NULL, 15546185, 7792, NUITKA_BYTECODE_FLAG },
    { "wsgiref.simple_server", NULL, 15553977, 5238, NUITKA_BYTECODE_FLAG },
    { "wsgiref.util", NULL, 15559215, 5213, NUITKA_BYTECODE_FLAG },
    { "wsgiref.validate", NULL, 15564428, 14709, NUITKA_BYTECODE_FLAG },
    { "xdrlib", NULL, 15579137, 8357, NUITKA_BYTECODE_FLAG },
    { "xml", NULL, 12237414, 739, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.dom", NULL, 12238153, 5491, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.dom.NodeFilter", NULL, 15587494, 1006, NUITKA_BYTECODE_FLAG },
    { "xml.dom.domreg", NULL, 15588500, 2883, NUITKA_BYTECODE_FLAG },
    { "xml.dom.expatbuilder", NULL, 12243644, 27053, NUITKA_BYTECODE_FLAG },
    { "xml.dom.minicompat", NULL, 15591383, 2852, NUITKA_BYTECODE_FLAG },
    { "xml.dom.minidom", NULL, 12270697, 55652, NUITKA_BYTECODE_FLAG },
    { "xml.dom.pulldom", NULL, 12326349, 10525, NUITKA_BYTECODE_FLAG },
    { "xml.dom.xmlbuilder", NULL, 15594235, 12472, NUITKA_BYTECODE_FLAG },
    { "xml.etree", NULL, 12336874, 178, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.etree.ElementInclude", NULL, 15606707, 1614, NUITKA_BYTECODE_FLAG },
    { "xml.etree.ElementPath", NULL, 15608321, 6382, NUITKA_BYTECODE_FLAG },
    { "xml.etree.ElementTree", NULL, 12337052, 44846, NUITKA_BYTECODE_FLAG },
    { "xml.etree.cElementTree", NULL, 12381898, 220, NUITKA_BYTECODE_FLAG },
    { "xml.parsers", NULL, 12382118, 352, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.parsers.expat", NULL, 12382470, 381, NUITKA_BYTECODE_FLAG },
    { "xml.sax", NULL, 12382851, 3211, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xml.sax._exceptions", NULL, 15614703, 5520, NUITKA_BYTECODE_FLAG },
    { "xml.sax.expatreader", NULL, 12386062, 12423, NUITKA_BYTECODE_FLAG },
    { "xml.sax.handler", NULL, 15620223, 12396, NUITKA_BYTECODE_FLAG },
    { "xml.sax.saxutils", NULL, 15632619, 12849, NUITKA_BYTECODE_FLAG },
    { "xml.sax.xmlreader", NULL, 15645468, 16957, NUITKA_BYTECODE_FLAG },
    { "xmlrpc", NULL, 12398485, 175, NUITKA_BYTECODE_FLAG | NUITKA_PACKAGE_FLAG },
    { "xmlrpc.client", NULL, 12398660, 34581, NUITKA_BYTECODE_FLAG },
    { "xmlrpc.server", NULL, 15662425, 29423, NUITKA_BYTECODE_FLAG },
    { "zipapp", NULL, 15691848, 5836, NUITKA_BYTECODE_FLAG },
    { "zipfile", NULL, 12433241, 49905, NUITKA_BYTECODE_FLAG },
    { NULL, NULL, 0, 0, 0 }
};

void setupMetaPathBasedLoader( void )
{
    static bool init_done = false;

    if ( init_done == false )
    {
        registerMetaPathBasedUnfreezer( meta_path_loader_entries );
        init_done = true;
    }
}
