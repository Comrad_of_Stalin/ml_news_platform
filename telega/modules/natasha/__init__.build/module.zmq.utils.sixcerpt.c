/* Generated code for Python module 'zmq.utils.sixcerpt'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_zmq$utils$sixcerpt" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_zmq$utils$sixcerpt;
PyDictObject *moduledict_zmq$utils$sixcerpt;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain___traceback__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_PY2;
extern PyObject *const_str_plain_tp;
static PyObject *const_str_digest_ba8deb25929f590b4f09a4178c40a1a2;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_digest_4518c3e53c13a3e08c44fad632be9feb;
extern PyObject *const_int_pos_1;
extern PyObject *const_tuple_str_plain_tp_str_plain_value_str_plain_tb_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_exec_;
extern PyObject *const_str_plain_with_traceback;
extern PyObject *const_str_plain__code_;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_tb;
extern PyObject *const_str_digest_52da6974eceb6e05255f92901fc2e2f2;
extern PyObject *const_str_plain_value;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_f_locals;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_digest_b02ad234820ee046133451a5a61ca33a;
extern PyObject *const_str_plain__getframe;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_f_globals;
static PyObject *const_str_digest_103fa7c0082d3a15aed94faa04f0c3dc;
extern PyObject *const_tuple_empty;
extern PyObject *const_tuple_90e06424913ba9ad885f60767479fe37_tuple;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_tuple_str_digest_cac36d305945ac2bbd2d1b0c1a3dadc8_tuple;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain__locs_;
extern PyObject *const_str_digest_70a0120f28d7311f633323a6bfb46104;
extern PyObject *const_str_angle_string;
extern PyObject *const_str_digest_cac36d305945ac2bbd2d1b0c1a3dadc8;
extern PyObject *const_str_plain_frame;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain__globs_;
extern PyObject *const_str_plain_PY3;
extern PyObject *const_str_plain_reraise;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_exec;
extern PyObject *const_str_plain___debug__;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_ba8deb25929f590b4f09a4178c40a1a2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853511 ], 27, 0 );
    const_str_digest_4518c3e53c13a3e08c44fad632be9feb = UNSTREAM_STRING_ASCII( &constant_bin[ 5853538 ], 21, 0 );
    const_str_digest_103fa7c0082d3a15aed94faa04f0c3dc = UNSTREAM_STRING_ASCII( &constant_bin[ 5853559 ], 18, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_zmq$utils$sixcerpt( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_75db9b765e5c6a74f1a3e425494f34e0;
static PyCodeObject *codeobj_012b55fc77970634a120b08da9b79ac8;
static PyCodeObject *codeobj_f03599e404cc5bd042f164fe8965ec9b;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_4518c3e53c13a3e08c44fad632be9feb );
    codeobj_75db9b765e5c6a74f1a3e425494f34e0 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_ba8deb25929f590b4f09a4178c40a1a2, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_012b55fc77970634a120b08da9b79ac8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_exec_, 37, const_tuple_90e06424913ba9ad885f60767479fe37_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f03599e404cc5bd042f164fe8965ec9b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reraise, 31, const_tuple_str_plain_tp_str_plain_value_str_plain_tb_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_zmq$utils$sixcerpt$$$function_1_reraise( PyObject *defaults );


static PyObject *MAKE_FUNCTION_zmq$utils$sixcerpt$$$function_2_exec_( PyObject *defaults );


// The module function definitions.
static PyObject *impl_zmq$utils$sixcerpt$$$function_1_reraise( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tp = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *par_tb = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_f03599e404cc5bd042f164fe8965ec9b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f03599e404cc5bd042f164fe8965ec9b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f03599e404cc5bd042f164fe8965ec9b, codeobj_f03599e404cc5bd042f164fe8965ec9b, module_zmq$utils$sixcerpt, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f03599e404cc5bd042f164fe8965ec9b = cache_frame_f03599e404cc5bd042f164fe8965ec9b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f03599e404cc5bd042f164fe8965ec9b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f03599e404cc5bd042f164fe8965ec9b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_value );
        tmp_source_name_1 = par_value;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___traceback__ );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_tb );
        tmp_compexpr_right_1 = par_tb;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_value );
            tmp_called_instance_1 = par_value;
            CHECK_OBJECT( par_tb );
            tmp_args_element_name_1 = par_tb;
            frame_f03599e404cc5bd042f164fe8965ec9b->m_frame.f_lineno = 33;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_raise_type_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_with_traceback, call_args );
            }

            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 33;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_raise_type_2;
        CHECK_OBJECT( par_value );
        tmp_raise_type_2 = par_value;
        exception_type = tmp_raise_type_2;
        Py_INCREF( tmp_raise_type_2 );
        exception_lineno = 34;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f03599e404cc5bd042f164fe8965ec9b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f03599e404cc5bd042f164fe8965ec9b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f03599e404cc5bd042f164fe8965ec9b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f03599e404cc5bd042f164fe8965ec9b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f03599e404cc5bd042f164fe8965ec9b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f03599e404cc5bd042f164fe8965ec9b,
        type_description_1,
        par_tp,
        par_value,
        par_tb
    );


    // Release cached frame.
    if ( frame_f03599e404cc5bd042f164fe8965ec9b == cache_frame_f03599e404cc5bd042f164fe8965ec9b )
    {
        Py_DECREF( frame_f03599e404cc5bd042f164fe8965ec9b );
    }
    cache_frame_f03599e404cc5bd042f164fe8965ec9b = NULL;

    assertFrameObject( frame_f03599e404cc5bd042f164fe8965ec9b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$utils$sixcerpt$$$function_1_reraise );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tp );
    Py_DECREF( par_tp );
    par_tp = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_tb );
    Py_DECREF( par_tb );
    par_tb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$utils$sixcerpt$$$function_1_reraise );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_zmq$utils$sixcerpt$$$function_2_exec_( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__code_ = python_pars[ 0 ];
    PyObject *par__globs_ = python_pars[ 1 ];
    PyObject *par__locs_ = python_pars[ 2 ];
    PyObject *var_frame = NULL;
    PyObject *tmp_exec_call_1__globals = NULL;
    PyObject *tmp_exec_call_1__locals = NULL;
    struct Nuitka_FrameObject *frame_012b55fc77970634a120b08da9b79ac8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *locals_zmq$utils$sixcerpt$$$function_2_exec_ = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_012b55fc77970634a120b08da9b79ac8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_012b55fc77970634a120b08da9b79ac8, codeobj_012b55fc77970634a120b08da9b79ac8, module_zmq$utils$sixcerpt, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_012b55fc77970634a120b08da9b79ac8 = cache_frame_012b55fc77970634a120b08da9b79ac8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_012b55fc77970634a120b08da9b79ac8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_012b55fc77970634a120b08da9b79ac8 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par__globs_ );
        tmp_compexpr_left_1 = par__globs_;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 40;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_012b55fc77970634a120b08da9b79ac8->m_frame.f_lineno = 40;
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain__getframe, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_frame == NULL );
            var_frame = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( var_frame );
            tmp_source_name_1 = var_frame;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_f_globals );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par__globs_;
                assert( old != NULL );
                par__globs_ = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par__locs_ );
            tmp_compexpr_left_2 = par__locs_;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_source_name_2;
                CHECK_OBJECT( var_frame );
                tmp_source_name_2 = var_frame;
                tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_f_locals );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 43;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par__locs_;
                    assert( old != NULL );
                    par__locs_ = tmp_assign_source_3;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        CHECK_OBJECT( var_frame );
        Py_DECREF( var_frame );
        var_frame = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par__locs_ );
            tmp_compexpr_left_3 = par__locs_;
            tmp_compexpr_right_3 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( par__globs_ );
                tmp_assign_source_4 = par__globs_;
                {
                    PyObject *old = par__locs_;
                    assert( old != NULL );
                    par__locs_ = tmp_assign_source_4;
                    Py_INCREF( par__locs_ );
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_outline_return_value_1;
        {
            PyObject *tmp_assign_source_5;
            if (locals_zmq$utils$sixcerpt$$$function_2_exec_ == NULL) locals_zmq$utils$sixcerpt$$$function_2_exec_ = PyDict_New();
            tmp_assign_source_5 = locals_zmq$utils$sixcerpt$$$function_2_exec_;
            Py_INCREF( tmp_assign_source_5 );
            if ( par__code_ != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par__code_ );
                value = par__code_;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain__code_, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain__code_ );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( par__globs_ != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par__globs_ );
                value = par__globs_;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain__globs_, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain__globs_ );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( par__locs_ != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( par__locs_ );
                value = par__locs_;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain__locs_, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain__locs_ );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            if ( var_frame != NULL )
            {
                PyObject *value;
                CHECK_OBJECT( var_frame );
                value = var_frame;

                UPDATE_STRING_DICT0( (PyDictObject *)tmp_assign_source_5, (Nuitka_StringObject *)const_str_plain_frame, value );
            }
            else
            {
                int res = PyDict_DelItem( tmp_assign_source_5, const_str_plain_frame );

                if ( res != 0 )
                {
                    CLEAR_ERROR_OCCURRED();
                }
            }

            assert( tmp_exec_call_1__locals == NULL );
            tmp_exec_call_1__locals = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = (PyObject *)moduledict_zmq$utils$sixcerpt;
            assert( tmp_exec_call_1__globals == NULL );
            Py_INCREF( tmp_assign_source_6 );
            tmp_exec_call_1__globals = tmp_assign_source_6;
        }
        // Tried code:
        {
            PyObject *tmp_eval_source_1;
            PyObject *tmp_eval_globals_1;
            PyObject *tmp_eval_locals_1;
            PyObject *tmp_eval_compiled_1;
            tmp_eval_source_1 = const_str_digest_70a0120f28d7311f633323a6bfb46104;
            CHECK_OBJECT( tmp_exec_call_1__globals );
            tmp_eval_globals_1 = tmp_exec_call_1__globals;
            CHECK_OBJECT( tmp_exec_call_1__locals );
            tmp_eval_locals_1 = tmp_exec_call_1__locals;
            tmp_eval_compiled_1 = COMPILE_CODE( tmp_eval_source_1, const_str_angle_string, const_str_plain_exec, NULL, NULL, NULL );
            if ( tmp_eval_compiled_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_outline_return_value_1 = EVAL_CODE( tmp_eval_compiled_1, tmp_eval_globals_1, tmp_eval_locals_1 );
            Py_DECREF( tmp_eval_compiled_1 );
            if ( tmp_outline_return_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            goto try_return_handler_2;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( zmq$utils$sixcerpt$$$function_2_exec_ );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
        Py_DECREF( tmp_exec_call_1__globals );
        tmp_exec_call_1__globals = NULL;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
        Py_DECREF( tmp_exec_call_1__locals );
        tmp_exec_call_1__locals = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__globals );
        Py_DECREF( tmp_exec_call_1__globals );
        tmp_exec_call_1__globals = NULL;

        CHECK_OBJECT( (PyObject *)tmp_exec_call_1__locals );
        Py_DECREF( tmp_exec_call_1__locals );
        tmp_exec_call_1__locals = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( zmq$utils$sixcerpt$$$function_2_exec_ );
        return NULL;
        outline_result_1:;
        Py_DECREF( tmp_outline_return_value_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_012b55fc77970634a120b08da9b79ac8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_012b55fc77970634a120b08da9b79ac8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_012b55fc77970634a120b08da9b79ac8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_012b55fc77970634a120b08da9b79ac8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_012b55fc77970634a120b08da9b79ac8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_012b55fc77970634a120b08da9b79ac8,
        type_description_1,
        par__code_,
        par__globs_,
        par__locs_,
        var_frame
    );


    // Release cached frame.
    if ( frame_012b55fc77970634a120b08da9b79ac8 == cache_frame_012b55fc77970634a120b08da9b79ac8 )
    {
        Py_DECREF( frame_012b55fc77970634a120b08da9b79ac8 );
    }
    cache_frame_012b55fc77970634a120b08da9b79ac8 = NULL;

    assertFrameObject( frame_012b55fc77970634a120b08da9b79ac8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$utils$sixcerpt$$$function_2_exec_ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par__code_ );
    par__code_ = NULL;

    Py_XDECREF( par__globs_ );
    par__globs_ = NULL;

    Py_XDECREF( par__locs_ );
    par__locs_ = NULL;

    Py_XDECREF( var_frame );
    var_frame = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par__code_ );
    par__code_ = NULL;

    Py_XDECREF( par__globs_ );
    par__globs_ = NULL;

    Py_XDECREF( par__locs_ );
    par__locs_ = NULL;

    Py_XDECREF( var_frame );
    var_frame = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$utils$sixcerpt$$$function_2_exec_ );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_zmq$utils$sixcerpt$$$function_2_exec_ );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_zmq$utils$sixcerpt$$$function_2_exec_ );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_zmq$utils$sixcerpt$$$function_1_reraise( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$utils$sixcerpt$$$function_1_reraise,
        const_str_plain_reraise,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f03599e404cc5bd042f164fe8965ec9b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$utils$sixcerpt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_zmq$utils$sixcerpt$$$function_2_exec_( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$utils$sixcerpt$$$function_2_exec_,
        const_str_plain_exec_,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_012b55fc77970634a120b08da9b79ac8,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$utils$sixcerpt,
        const_str_digest_52da6974eceb6e05255f92901fc2e2f2,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_zmq$utils$sixcerpt =
{
    PyModuleDef_HEAD_INIT,
    "zmq.utils.sixcerpt",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(zmq$utils$sixcerpt)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(zmq$utils$sixcerpt)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_zmq$utils$sixcerpt );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("zmq.utils.sixcerpt: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.utils.sixcerpt: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.utils.sixcerpt: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initzmq$utils$sixcerpt" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_zmq$utils$sixcerpt = Py_InitModule4(
        "zmq.utils.sixcerpt",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_zmq$utils$sixcerpt = PyModule_Create( &mdef_zmq$utils$sixcerpt );
#endif

    moduledict_zmq$utils$sixcerpt = MODULE_DICT( module_zmq$utils$sixcerpt );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_zmq$utils$sixcerpt,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_zmq$utils$sixcerpt,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$utils$sixcerpt,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$utils$sixcerpt,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_zmq$utils$sixcerpt );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_b02ad234820ee046133451a5a61ca33a, module_zmq$utils$sixcerpt );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_75db9b765e5c6a74f1a3e425494f34e0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_103fa7c0082d3a15aed94faa04f0c3dc;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_75db9b765e5c6a74f1a3e425494f34e0 = MAKE_MODULE_FRAME( codeobj_75db9b765e5c6a74f1a3e425494f34e0, module_zmq$utils$sixcerpt );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_75db9b765e5c6a74f1a3e425494f34e0 );
    assert( Py_REFCNT( frame_75db9b765e5c6a74f1a3e425494f34e0 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_sys;
        tmp_globals_name_1 = (PyObject *)moduledict_zmq$utils$sixcerpt;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_75db9b765e5c6a74f1a3e425494f34e0->m_frame.f_lineno = 23;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version_info );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_2;
        tmp_assign_source_5 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_PY2, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_subscript_name_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version_info );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_compexpr_left_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_3;
        tmp_assign_source_6 = RICH_COMPARE_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_PY3, tmp_assign_source_6 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_mvar_value_5;
        int tmp_truth_name_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_PY3 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_mvar_value_5 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_assign_source_7 = MAKE_FUNCTION_zmq$utils$sixcerpt$$$function_1_reraise( tmp_defaults_1 );



            UPDATE_STRING_DICT1( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_reraise, tmp_assign_source_7 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_none_none_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_assign_source_8 = MAKE_FUNCTION_zmq$utils$sixcerpt$$$function_2_exec_( tmp_defaults_2 );



            UPDATE_STRING_DICT1( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_exec_, tmp_assign_source_8 );
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_call_result_1;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$sixcerpt, (Nuitka_StringObject *)const_str_plain_exec_ );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_exec_ );
            }

            CHECK_OBJECT( tmp_mvar_value_6 );
            tmp_called_name_1 = tmp_mvar_value_6;
            frame_75db9b765e5c6a74f1a3e425494f34e0->m_frame.f_lineno = 50;
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_digest_cac36d305945ac2bbd2d1b0c1a3dadc8_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_end_1:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_75db9b765e5c6a74f1a3e425494f34e0 );
#endif
    popFrameStack();

    assertFrameObject( frame_75db9b765e5c6a74f1a3e425494f34e0 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_75db9b765e5c6a74f1a3e425494f34e0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_75db9b765e5c6a74f1a3e425494f34e0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_75db9b765e5c6a74f1a3e425494f34e0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_75db9b765e5c6a74f1a3e425494f34e0, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_zmq$utils$sixcerpt );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
