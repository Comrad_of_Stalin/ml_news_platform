/* Generated code for Python module 'cffi.pkgconfig'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_cffi$pkgconfig" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_cffi$pkgconfig;
PyDictObject *moduledict_cffi$pkgconfig;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_9c30163b8eb7499dbcbbd7fbc6a6fb1a;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7;
static PyObject *const_str_digest_1c6d85b53d0cecbbf6a21f6756d2dbc6;
static PyObject *const_str_digest_571eccf4664cb01ecea1867fcbf66f8d;
extern PyObject *const_str_plain_string;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_get_include_dirs;
extern PyObject *const_str_plain_ret;
static PyObject *const_str_digest_9971bee4a83ffe5fd693568114cd1837;
extern PyObject *const_str_plain_a;
extern PyObject *const_str_plain_items;
static PyObject *const_str_digest_ac3f4fc4213394446df35759ac278c3c;
extern PyObject *const_str_plain_os;
static PyObject *const_tuple_str_digest_571eccf4664cb01ecea1867fcbf66f8d_tuple;
extern PyObject *const_str_chr_61;
extern PyObject *const_str_plain_getfilesystemencoding;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_define_macros;
extern PyObject *const_str_plain_strip;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_tuple_str_plain_PkgConfigError_tuple;
static PyObject *const_str_digest_469be60f53f1755b4aead40cf2e73689;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_PIPE;
extern PyObject *const_str_plain_Popen;
static PyObject *const_str_digest_98e43324dd3156c35f65918332800bb0;
extern PyObject *const_str_plain_extend;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_c734b157194e4cfe6b7a4d0fcbc9e221;
static PyObject *const_str_plain_lib_flags;
extern PyObject *const_str_plain_pc;
static PyObject *const_str_digest_9870a3ace7357eb3938eb55819eb2a49;
static PyObject *const_str_digest_78de961cee629be58db46bc0f3de1d5d;
static PyObject *const_str_plain_extra_compile_args;
extern PyObject *const_str_plain_str;
static PyObject *const_str_digest_1f3be37482e2e2ffa90e044d6fd21859;
extern PyObject *const_str_plain_libs;
extern PyObject *const_str_plain_library_dirs;
extern PyObject *const_str_plain_value;
static PyObject *const_tuple_53f4419c1ff1f1305220b3022879c402_tuple;
static PyObject *const_list_bbb383acc6be587538eedcf6c289f0f2_list;
static PyObject *const_str_plain_all_libs;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_subprocess;
static PyObject *const_str_digest_6d04c7b33d96ded68bee460a6cbfa3b6;
static PyObject *const_str_digest_7bfbc57b26bdf1b702d94c1830a1b38e;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_stderr;
static PyObject *const_str_plain_get_macros;
static PyObject *const_str_digest_2edc37e13b19d31b2954d5650d171696;
static PyObject *const_str_digest_2c5bc3e8fe330b270674a4c0c467264a;
static PyObject *const_tuple_str_plain_string_str_plain__macro_tuple;
extern PyObject *const_str_chr_92;
extern PyObject *const_tuple_str_plain_x_tuple;
extern PyObject *const_str_plain_error;
static PyObject *const_str_plain_fse;
extern PyObject *const_str_plain_split;
static PyObject *const_str_plain_get_other_libs;
extern PyObject *const_str_plain_flags_from_pkgconfig;
extern PyObject *const_str_plain_returncode;
extern PyObject *const_str_plain_decode;
extern PyObject *const_tuple_int_pos_3_tuple;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_libname;
static PyObject *const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple;
static PyObject *const_tuple_str_digest_9870a3ace7357eb3938eb55819eb2a49_tuple;
static PyObject *const_tuple_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742_tuple;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_digest_9102dcc66aa2f241d2460e0ec5e7734c;
static PyObject *const_str_plain_cfg1;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_altsep;
extern PyObject *const_str_plain_call;
static PyObject *const_str_digest_3a97a1837cb7d50e2a696eee11269799;
static PyObject *const_str_digest_42bb4843562d0537c6721bc53ce8dba3;
extern PyObject *const_str_plain_origin;
extern PyObject *const_tuple_str_plain_string_tuple;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_tuple_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7_tuple;
extern PyObject *const_tuple_str_chr_61_int_pos_1_tuple;
static PyObject *const_str_digest_d76a4a6dfe9355808ee3ab2ee14a97e2;
extern PyObject *const_str_plain_communicate;
static PyObject *const_str_digest_1b4cad4ef984868ccd5523eb932489e5;
static PyObject *const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple;
static PyObject *const_str_digest_c1275aa5deb6bb446724ec968188db8e;
extern PyObject *const_str_plain_merge_flags;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_d521200a76c655966214e3d83504daf9;
static PyObject *const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple;
extern PyObject *const_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742;
static PyObject *const_str_plain_get_libraries;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_PkgConfigError;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain__macro;
static PyObject *const_str_digest_cbbcfb3743e2c13010ed3a50318990fc;
extern PyObject *const_str_plain_key;
extern PyObject *const_slice_int_pos_2_none_none;
extern PyObject *const_str_plain_get_library_dirs;
extern PyObject *const_int_pos_3;
static PyObject *const_str_digest_698989052dca859c5e8622ba5404fa01;
static PyObject *const_str_plain_all_cflags;
static PyObject *const_str_plain_cfg2;
extern PyObject *const_str_plain_extra_link_args;
static PyObject *const_str_plain_bout;
static PyObject *const_tuple_str_plain_x_str_plain__macro_tuple;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_plain_libraries;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_a707b596db3c29ec22f6fd3a21719c67;
extern PyObject *const_str_plain_flag;
extern PyObject *const_str_plain_startswith;
static PyObject *const_str_plain_berr;
static PyObject *const_str_plain_get_other_cflags;
extern PyObject *const_str_plain_include_dirs;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_9c30163b8eb7499dbcbbd7fbc6a6fb1a = UNSTREAM_STRING_ASCII( &constant_bin[ 200943 ], 10, 0 );
    const_str_digest_1c6d85b53d0cecbbf6a21f6756d2dbc6 = UNSTREAM_STRING_ASCII( &constant_bin[ 205028 ], 14, 0 );
    const_str_digest_571eccf4664cb01ecea1867fcbf66f8d = UNSTREAM_STRING_ASCII( &constant_bin[ 205042 ], 2, 0 );
    const_str_plain_get_include_dirs = UNSTREAM_STRING_ASCII( &constant_bin[ 205044 ], 16, 1 );
    const_str_digest_9971bee4a83ffe5fd693568114cd1837 = UNSTREAM_STRING_ASCII( &constant_bin[ 205060 ], 46, 0 );
    const_str_digest_ac3f4fc4213394446df35759ac278c3c = UNSTREAM_STRING_ASCII( &constant_bin[ 205106 ], 69, 0 );
    const_tuple_str_digest_571eccf4664cb01ecea1867fcbf66f8d_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_571eccf4664cb01ecea1867fcbf66f8d_tuple, 0, const_str_digest_571eccf4664cb01ecea1867fcbf66f8d ); Py_INCREF( const_str_digest_571eccf4664cb01ecea1867fcbf66f8d );
    const_str_digest_469be60f53f1755b4aead40cf2e73689 = UNSTREAM_STRING_ASCII( &constant_bin[ 205175 ], 40, 0 );
    const_str_digest_98e43324dd3156c35f65918332800bb0 = UNSTREAM_STRING_ASCII( &constant_bin[ 205215 ], 172, 0 );
    const_str_digest_c734b157194e4cfe6b7a4d0fcbc9e221 = UNSTREAM_STRING_ASCII( &constant_bin[ 205387 ], 46, 0 );
    const_str_plain_lib_flags = UNSTREAM_STRING_ASCII( &constant_bin[ 205433 ], 9, 1 );
    const_str_digest_9870a3ace7357eb3938eb55819eb2a49 = UNSTREAM_STRING_ASCII( &constant_bin[ 205442 ], 2, 0 );
    const_str_digest_78de961cee629be58db46bc0f3de1d5d = UNSTREAM_STRING_ASCII( &constant_bin[ 205444 ], 478, 0 );
    const_str_plain_extra_compile_args = UNSTREAM_STRING_ASCII( &constant_bin[ 205744 ], 18, 1 );
    const_str_digest_1f3be37482e2e2ffa90e044d6fd21859 = UNSTREAM_STRING_ASCII( &constant_bin[ 205922 ], 46, 0 );
    const_tuple_53f4419c1ff1f1305220b3022879c402_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 0, const_str_plain_libname ); Py_INCREF( const_str_plain_libname );
    const_str_plain_fse = UNSTREAM_STRING_ASCII( &constant_bin[ 3313 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 1, const_str_plain_fse ); Py_INCREF( const_str_plain_fse );
    const_str_plain_all_cflags = UNSTREAM_STRING_ASCII( &constant_bin[ 205968 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 2, const_str_plain_all_cflags ); Py_INCREF( const_str_plain_all_cflags );
    const_str_plain_all_libs = UNSTREAM_STRING_ASCII( &constant_bin[ 205978 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 3, const_str_plain_all_libs ); Py_INCREF( const_str_plain_all_libs );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 4, const_str_plain_get_include_dirs ); Py_INCREF( const_str_plain_get_include_dirs );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 5, const_str_plain_get_library_dirs ); Py_INCREF( const_str_plain_get_library_dirs );
    const_str_plain_get_libraries = UNSTREAM_STRING_ASCII( &constant_bin[ 205986 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 6, const_str_plain_get_libraries ); Py_INCREF( const_str_plain_get_libraries );
    const_str_plain_get_macros = UNSTREAM_STRING_ASCII( &constant_bin[ 205205 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 7, const_str_plain_get_macros ); Py_INCREF( const_str_plain_get_macros );
    const_str_plain_get_other_cflags = UNSTREAM_STRING_ASCII( &constant_bin[ 205417 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 8, const_str_plain_get_other_cflags ); Py_INCREF( const_str_plain_get_other_cflags );
    const_str_plain_get_other_libs = UNSTREAM_STRING_ASCII( &constant_bin[ 205999 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 9, const_str_plain_get_other_libs ); Py_INCREF( const_str_plain_get_other_libs );
    const_list_bbb383acc6be587538eedcf6c289f0f2_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_bbb383acc6be587538eedcf6c289f0f2_list, 0, const_str_digest_9c30163b8eb7499dbcbbd7fbc6a6fb1a ); Py_INCREF( const_str_digest_9c30163b8eb7499dbcbbd7fbc6a6fb1a );
    PyList_SET_ITEM( const_list_bbb383acc6be587538eedcf6c289f0f2_list, 1, const_str_digest_1c6d85b53d0cecbbf6a21f6756d2dbc6 ); Py_INCREF( const_str_digest_1c6d85b53d0cecbbf6a21f6756d2dbc6 );
    const_str_digest_6d04c7b33d96ded68bee460a6cbfa3b6 = UNSTREAM_STRING_ASCII( &constant_bin[ 206013 ], 23, 0 );
    const_str_digest_7bfbc57b26bdf1b702d94c1830a1b38e = UNSTREAM_STRING_ASCII( &constant_bin[ 206036 ], 6, 0 );
    const_str_digest_2edc37e13b19d31b2954d5650d171696 = UNSTREAM_STRING_ASCII( &constant_bin[ 206042 ], 56, 0 );
    const_str_digest_2c5bc3e8fe330b270674a4c0c467264a = UNSTREAM_STRING_ASCII( &constant_bin[ 206098 ], 36, 0 );
    const_tuple_str_plain_string_str_plain__macro_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_string_str_plain__macro_tuple, 0, const_str_plain_string ); Py_INCREF( const_str_plain_string );
    const_str_plain__macro = UNSTREAM_STRING_ASCII( &constant_bin[ 194459 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_string_str_plain__macro_tuple, 1, const_str_plain__macro ); Py_INCREF( const_str_plain__macro );
    const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple = PyTuple_New( 4 );
    const_str_plain_cfg1 = UNSTREAM_STRING_ASCII( &constant_bin[ 206134 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple, 0, const_str_plain_cfg1 ); Py_INCREF( const_str_plain_cfg1 );
    const_str_plain_cfg2 = UNSTREAM_STRING_ASCII( &constant_bin[ 205251 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple, 1, const_str_plain_cfg2 ); Py_INCREF( const_str_plain_cfg2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple, 2, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple, 3, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_tuple_str_digest_9870a3ace7357eb3938eb55819eb2a49_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_9870a3ace7357eb3938eb55819eb2a49_tuple, 0, const_str_digest_9870a3ace7357eb3938eb55819eb2a49 ); Py_INCREF( const_str_digest_9870a3ace7357eb3938eb55819eb2a49 );
    const_tuple_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742_tuple, 0, const_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742 ); Py_INCREF( const_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742 );
    const_str_digest_9102dcc66aa2f241d2460e0ec5e7734c = UNSTREAM_STRING_ASCII( &constant_bin[ 206138 ], 43, 0 );
    const_str_digest_3a97a1837cb7d50e2a696eee11269799 = UNSTREAM_STRING_ASCII( &constant_bin[ 206181 ], 36, 0 );
    const_str_digest_42bb4843562d0537c6721bc53ce8dba3 = UNSTREAM_STRING_ASCII( &constant_bin[ 206217 ], 44, 0 );
    const_tuple_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7_tuple, 0, const_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7 ); Py_INCREF( const_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7 );
    const_str_digest_d76a4a6dfe9355808ee3ab2ee14a97e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 206261 ], 17, 0 );
    const_str_digest_1b4cad4ef984868ccd5523eb932489e5 = UNSTREAM_STRING_ASCII( &constant_bin[ 206278 ], 75, 0 );
    const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 0, const_str_plain_libs ); Py_INCREF( const_str_plain_libs );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 1, const_str_plain_get_include_dirs ); Py_INCREF( const_str_plain_get_include_dirs );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 2, const_str_plain_get_library_dirs ); Py_INCREF( const_str_plain_get_library_dirs );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 3, const_str_plain_get_libraries ); Py_INCREF( const_str_plain_get_libraries );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 4, const_str_plain_get_macros ); Py_INCREF( const_str_plain_get_macros );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 5, const_str_plain_get_other_cflags ); Py_INCREF( const_str_plain_get_other_cflags );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 6, const_str_plain_get_other_libs ); Py_INCREF( const_str_plain_get_other_libs );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 7, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 8, const_str_plain_ret ); Py_INCREF( const_str_plain_ret );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 9, const_str_plain_libname ); Py_INCREF( const_str_plain_libname );
    PyTuple_SET_ITEM( const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 10, const_str_plain_lib_flags ); Py_INCREF( const_str_plain_lib_flags );
    const_str_digest_c1275aa5deb6bb446724ec968188db8e = UNSTREAM_STRING_ASCII( &constant_bin[ 206353 ], 8, 0 );
    const_str_digest_d521200a76c655966214e3d83504daf9 = UNSTREAM_STRING_ASCII( &constant_bin[ 206361 ], 36, 0 );
    const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 0, const_str_plain_libname ); Py_INCREF( const_str_plain_libname );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 1, const_str_plain_flag ); Py_INCREF( const_str_plain_flag );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 2, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 3, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 4, const_str_plain_pc ); Py_INCREF( const_str_plain_pc );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 5, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_str_plain_bout = UNSTREAM_STRING_ASCII( &constant_bin[ 1310 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 6, const_str_plain_bout ); Py_INCREF( const_str_plain_bout );
    const_str_plain_berr = UNSTREAM_STRING_ASCII( &constant_bin[ 206397 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 7, const_str_plain_berr ); Py_INCREF( const_str_plain_berr );
    const_str_digest_cbbcfb3743e2c13010ed3a50318990fc = UNSTREAM_STRING_ASCII( &constant_bin[ 206401 ], 25, 0 );
    const_str_digest_698989052dca859c5e8622ba5404fa01 = UNSTREAM_STRING_ASCII( &constant_bin[ 206426 ], 53, 0 );
    const_tuple_str_plain_x_str_plain__macro_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain__macro_tuple, 0, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain__macro_tuple, 1, const_str_plain__macro ); Py_INCREF( const_str_plain__macro );
    const_str_digest_a707b596db3c29ec22f6fd3a21719c67 = UNSTREAM_STRING_ASCII( &constant_bin[ 206021 ], 14, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_cffi$pkgconfig( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_a29d0ab846e79881efc107e37df4b4d8;
static PyCodeObject *codeobj_8e9d071f46a68f760619dc1b636266b4;
static PyCodeObject *codeobj_37e6613b1bf27d7de778f889e87a3b63;
static PyCodeObject *codeobj_4409553ea769b9ad3f8ed4142228ea35;
static PyCodeObject *codeobj_88cb1c45161649d40c67807e0aabae41;
static PyCodeObject *codeobj_f9078ed88ad5f8460e26cdc3d0e747f8;
static PyCodeObject *codeobj_f4845c574ec21b2e87029a6a6ea8c1ca;
static PyCodeObject *codeobj_e460aa9bcda8fda05b695e0d1f096030;
static PyCodeObject *codeobj_8e914d53d9af12625ceb8d5aec97242b;
static PyCodeObject *codeobj_677545be6b2daf860f6d8f30017aed7f;
static PyCodeObject *codeobj_50a6cf518d1fc0705863a31b6a70d3f6;
static PyCodeObject *codeobj_6da2625bbbf9aa031107b6c08ed8706a;
static PyCodeObject *codeobj_e7bc752963c4f65030506fc7c1833e33;
static PyCodeObject *codeobj_49691a548ce33c04a71c62b934547aed;
static PyCodeObject *codeobj_31a3d120e78fa5c17818142281108366;
static PyCodeObject *codeobj_6fa2080fb82cd5dd1042652f76372080;
static PyCodeObject *codeobj_0dbac9949f9d1e0598072df5c8b8af62;
static PyCodeObject *codeobj_b879299286186359c279df79176b4244;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_d76a4a6dfe9355808ee3ab2ee14a97e2 );
    codeobj_a29d0ab846e79881efc107e37df4b4d8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 76, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8e9d071f46a68f760619dc1b636266b4 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 79, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_37e6613b1bf27d7de778f889e87a3b63 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 82, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4409553ea769b9ad3f8ed4142228ea35 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 95, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_88cb1c45161649d40c67807e0aabae41 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 99, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f9078ed88ad5f8460e26cdc3d0e747f8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 92, const_tuple_str_plain_x_str_plain__macro_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f4845c574ec21b2e87029a6a6ea8c1ca = MAKE_CODEOBJ( module_filename_obj, const_str_digest_6d04c7b33d96ded68bee460a6cbfa3b6, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_e460aa9bcda8fda05b695e0d1f096030 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__macro, 86, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8e914d53d9af12625ceb8d5aec97242b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_call, 26, const_tuple_f2246031637f21106eb7e53c5e00aa66_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_677545be6b2daf860f6d8f30017aed7f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_flags_from_pkgconfig, 60, const_tuple_eddc9ceb3eaffb6f2f6bb641b5aa9275_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_50a6cf518d1fc0705863a31b6a70d3f6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_include_dirs, 75, const_tuple_str_plain_string_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6da2625bbbf9aa031107b6c08ed8706a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_libraries, 81, const_tuple_str_plain_string_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e7bc752963c4f65030506fc7c1833e33 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_library_dirs, 78, const_tuple_str_plain_string_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_49691a548ce33c04a71c62b934547aed = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_macros, 85, const_tuple_str_plain_string_str_plain__macro_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_31a3d120e78fa5c17818142281108366 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_other_cflags, 94, const_tuple_str_plain_string_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6fa2080fb82cd5dd1042652f76372080 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_other_libs, 98, const_tuple_str_plain_string_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0dbac9949f9d1e0598072df5c8b8af62 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_kwargs, 103, const_tuple_53f4419c1ff1f1305220b3022879c402_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_b879299286186359c279df79176b4244 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_merge_flags, 7, const_tuple_str_plain_cfg1_str_plain_cfg2_str_plain_key_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_1_merge_flags(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_2_call( PyObject *defaults );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs(  );


static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs(  );


// The module function definitions.
static PyObject *impl_cffi$pkgconfig$$$function_1_merge_flags( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cfg1 = python_pars[ 0 ];
    PyObject *par_cfg2 = python_pars[ 1 ];
    PyObject *var_key = NULL;
    PyObject *var_value = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_b879299286186359c279df79176b4244;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_b879299286186359c279df79176b4244 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b879299286186359c279df79176b4244, codeobj_b879299286186359c279df79176b4244, module_cffi$pkgconfig, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b879299286186359c279df79176b4244 = cache_frame_b879299286186359c279df79176b4244;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b879299286186359c279df79176b4244 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b879299286186359c279df79176b4244 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_cfg2 );
        tmp_called_instance_1 = par_cfg2;
        frame_b879299286186359c279df79176b4244->m_frame.f_lineno = 14;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 14;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 14;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 14;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooo";
                    exception_lineno = 14;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooo";
            exception_lineno = 14;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_6;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_value;
            var_value = tmp_assign_source_7;
            Py_INCREF( var_value );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_key );
        tmp_compexpr_left_1 = var_key;
        CHECK_OBJECT( par_cfg1 );
        tmp_compexpr_right_1 = par_cfg1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( var_value );
            tmp_ass_subvalue_1 = var_value;
            CHECK_OBJECT( par_cfg1 );
            tmp_ass_subscribed_1 = par_cfg1;
            CHECK_OBJECT( var_key );
            tmp_ass_subscript_1 = var_key;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 16;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_cfg1 );
            tmp_subscribed_name_1 = par_cfg1;
            CHECK_OBJECT( var_key );
            tmp_subscript_name_1 = var_key;
            tmp_isinstance_inst_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_isinstance_inst_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_isinstance_cls_1 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            Py_DECREF( tmp_isinstance_inst_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_1;
                tmp_left_name_1 = const_str_digest_3a97a1837cb7d50e2a696eee11269799;
                CHECK_OBJECT( var_key );
                tmp_tuple_element_1 = var_key;
                tmp_right_name_1 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_make_exception_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 19;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                frame_b879299286186359c279df79176b4244->m_frame.f_lineno = 19;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_1 );
                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 19;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            CHECK_OBJECT( var_value );
            tmp_isinstance_inst_2 = var_value;
            tmp_isinstance_cls_2 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_2;
                tmp_left_name_2 = const_str_digest_2c5bc3e8fe330b270674a4c0c467264a;
                CHECK_OBJECT( var_key );
                tmp_tuple_element_2 = var_key;
                tmp_right_name_2 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_2 );
                tmp_make_exception_arg_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_make_exception_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                frame_b879299286186359c279df79176b4244->m_frame.f_lineno = 21;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_2 );
                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 21;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_cfg1 );
            tmp_subscribed_name_2 = par_cfg1;
            CHECK_OBJECT( var_key );
            tmp_subscript_name_2 = var_key;
            tmp_called_instance_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( var_value );
            tmp_args_element_name_1 = var_value;
            frame_b879299286186359c279df79176b4244->m_frame.f_lineno = 22;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_extend, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b879299286186359c279df79176b4244 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b879299286186359c279df79176b4244 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b879299286186359c279df79176b4244, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b879299286186359c279df79176b4244->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b879299286186359c279df79176b4244, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b879299286186359c279df79176b4244,
        type_description_1,
        par_cfg1,
        par_cfg2,
        var_key,
        var_value
    );


    // Release cached frame.
    if ( frame_b879299286186359c279df79176b4244 == cache_frame_b879299286186359c279df79176b4244 )
    {
        Py_DECREF( frame_b879299286186359c279df79176b4244 );
    }
    cache_frame_b879299286186359c279df79176b4244 = NULL;

    assertFrameObject( frame_b879299286186359c279df79176b4244 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( par_cfg1 );
    tmp_return_value = par_cfg1;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_1_merge_flags );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cfg1 );
    Py_DECREF( par_cfg1 );
    par_cfg1 = NULL;

    CHECK_OBJECT( (PyObject *)par_cfg2 );
    Py_DECREF( par_cfg2 );
    par_cfg2 = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cfg1 );
    Py_DECREF( par_cfg1 );
    par_cfg1 = NULL;

    CHECK_OBJECT( (PyObject *)par_cfg2 );
    Py_DECREF( par_cfg2 );
    par_cfg2 = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_1_merge_flags );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_2_call( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_libname = python_pars[ 0 ];
    PyObject *par_flag = python_pars[ 1 ];
    PyObject *par_encoding = python_pars[ 2 ];
    PyObject *var_a = NULL;
    PyObject *var_pc = NULL;
    PyObject *var_e = NULL;
    PyObject *var_bout = NULL;
    PyObject *var_berr = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_8e914d53d9af12625ceb8d5aec97242b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    static struct Nuitka_FrameObject *cache_frame_8e914d53d9af12625ceb8d5aec97242b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = LIST_COPY( const_list_bbb383acc6be587538eedcf6c289f0f2_list );
        assert( var_a == NULL );
        var_a = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8e914d53d9af12625ceb8d5aec97242b, codeobj_8e914d53d9af12625ceb8d5aec97242b, module_cffi$pkgconfig, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8e914d53d9af12625ceb8d5aec97242b = cache_frame_8e914d53d9af12625ceb8d5aec97242b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8e914d53d9af12625ceb8d5aec97242b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8e914d53d9af12625ceb8d5aec97242b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_a );
        tmp_called_instance_1 = var_a;
        CHECK_OBJECT( par_flag );
        tmp_args_element_name_1 = par_flag;
        frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_a );
        tmp_called_instance_2 = var_a;
        CHECK_OBJECT( par_libname );
        tmp_args_element_name_2 = par_libname;
        frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 31;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_a );
        tmp_tuple_element_1 = var_a;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_stdout;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_stderr;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_subprocess );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_3;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_PIPE );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 33;
        tmp_assign_source_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        assert( var_pc == NULL );
        var_pc = tmp_assign_source_2;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_8e914d53d9af12625ceb8d5aec97242b, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_8e914d53d9af12625ceb8d5aec97242b, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_EnvironmentError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = EXC_VALUE(PyThreadState_GET());
            assert( var_e == NULL );
            Py_INCREF( tmp_assign_source_3 );
            var_e = tmp_assign_source_3;
        }
        // Tried code:
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_unicode_arg_1;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_PkgConfigError );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PkgConfigError );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PkgConfigError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 35;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_2 = tmp_mvar_value_4;
            tmp_left_name_1 = const_str_digest_cbbcfb3743e2c13010ed3a50318990fc;
            CHECK_OBJECT( var_e );
            tmp_unicode_arg_1 = var_e;
            tmp_called_instance_3 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 35;
            tmp_tuple_element_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_strip );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            tmp_right_name_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
            tmp_args_element_name_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 35;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 35;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_2_call );
        return NULL;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 32;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_8e914d53d9af12625ceb8d5aec97242b->m_frame) frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_2_call );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( var_pc );
        tmp_called_instance_4 = var_pc;
        frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 37;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_communicate );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 37;
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 37;
            goto try_except_handler_6;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 37;
                    goto try_except_handler_6;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 37;
            goto try_except_handler_6;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_5;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        assert( var_bout == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_bout = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        assert( var_berr == NULL );
        Py_INCREF( tmp_assign_source_8 );
        var_berr = tmp_assign_source_8;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_pc );
        tmp_source_name_4 = var_pc;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_returncode );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        // Tried code:
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_args_element_name_4;
            CHECK_OBJECT( var_berr );
            tmp_called_instance_5 = var_berr;
            CHECK_OBJECT( par_encoding );
            tmp_args_element_name_4 = par_encoding;
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 40;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_assign_source_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_decode, call_args );
            }

            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oooooooo";
                goto try_except_handler_7;
            }
            {
                PyObject *old = var_berr;
                assert( old != NULL );
                var_berr = tmp_assign_source_9;
                Py_DECREF( old );
            }

        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_2 );
        exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_2 );
        exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_2 );

        if ( exception_keeper_tb_6 == NULL )
        {
            exception_keeper_tb_6 = MAKE_TRACEBACK( frame_8e914d53d9af12625ceb8d5aec97242b, exception_keeper_lineno_6 );
        }
        else if ( exception_keeper_lineno_6 != 0 )
        {
            exception_keeper_tb_6 = ADD_TRACEBACK( exception_keeper_tb_6, frame_8e914d53d9af12625ceb8d5aec97242b, exception_keeper_lineno_6 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
        PyException_SetTraceback( exception_keeper_value_6, (PyObject *)exception_keeper_tb_6 );
        PUBLISH_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_3 = PyExc_Exception;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "oooooooo";
                goto try_except_handler_8;
            }
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "oooooooo";
                goto try_except_handler_8;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 39;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_8e914d53d9af12625ceb8d5aec97242b->m_frame) frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooo";
            goto try_except_handler_8;
            branch_no_3:;
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto frame_exception_exit_1;
        // End of try:
        try_end_5:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        goto try_end_4;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_2_call );
        return NULL;
        // End of try:
        try_end_4:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_instance_6;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_PkgConfigError );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PkgConfigError );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PkgConfigError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_5;
            CHECK_OBJECT( var_berr );
            tmp_called_instance_6 = var_berr;
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 43;
            tmp_args_element_name_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_strip );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 43;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_raise_type_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_2;
            exception_lineno = 43;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_6;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_version_info );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_tuple_int_pos_3_tuple;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_bout );
        tmp_isinstance_inst_1 = var_bout;
        tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_4 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_4 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        // Tried code:
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_called_instance_7;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( var_bout );
            tmp_called_instance_7 = var_bout;
            CHECK_OBJECT( par_encoding );
            tmp_args_element_name_6 = par_encoding;
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 47;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_assign_source_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_decode, call_args );
            }

            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_1 = "oooooooo";
                goto try_except_handler_9;
            }
            {
                PyObject *old = var_bout;
                assert( old != NULL );
                var_bout = tmp_assign_source_10;
                Py_DECREF( old );
            }

        }
        goto try_end_6;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_3 );
        exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_3 );
        exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_3 );

        if ( exception_keeper_tb_8 == NULL )
        {
            exception_keeper_tb_8 = MAKE_TRACEBACK( frame_8e914d53d9af12625ceb8d5aec97242b, exception_keeper_lineno_8 );
        }
        else if ( exception_keeper_lineno_8 != 0 )
        {
            exception_keeper_tb_8 = ADD_TRACEBACK( exception_keeper_tb_8, frame_8e914d53d9af12625ceb8d5aec97242b, exception_keeper_lineno_8 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_8, &exception_keeper_value_8, &exception_keeper_tb_8 );
        PyException_SetTraceback( exception_keeper_value_8, (PyObject *)exception_keeper_tb_8 );
        PUBLISH_EXCEPTION( &exception_keeper_type_8, &exception_keeper_value_8, &exception_keeper_tb_8 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            tmp_compexpr_left_5 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_5 = PyExc_UnicodeDecodeError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "oooooooo";
                goto try_except_handler_10;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_called_name_4;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_args_element_name_7;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_3;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_PkgConfigError );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PkgConfigError );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PkgConfigError" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 49;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_10;
                }

                tmp_called_name_4 = tmp_mvar_value_7;
                tmp_left_name_2 = const_str_digest_1b4cad4ef984868ccd5523eb932489e5;
                CHECK_OBJECT( par_flag );
                tmp_tuple_element_3 = par_flag;
                tmp_right_name_2 = PyTuple_New( 4 );
                Py_INCREF( tmp_tuple_element_3 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( par_libname );
                tmp_tuple_element_3 = par_libname;
                Py_INCREF( tmp_tuple_element_3 );
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_3 );
                CHECK_OBJECT( par_encoding );
                tmp_tuple_element_3 = par_encoding;
                Py_INCREF( tmp_tuple_element_3 );
                PyTuple_SET_ITEM( tmp_right_name_2, 2, tmp_tuple_element_3 );
                CHECK_OBJECT( var_bout );
                tmp_tuple_element_3 = var_bout;
                Py_INCREF( tmp_tuple_element_3 );
                PyTuple_SET_ITEM( tmp_right_name_2, 3, tmp_tuple_element_3 );
                tmp_args_element_name_7 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_args_element_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_10;
                }
                frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 49;
                {
                    PyObject *call_args[] = { tmp_args_element_name_7 };
                    tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_args_element_name_7 );
                if ( tmp_raise_type_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 49;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_10;
                }
                exception_type = tmp_raise_type_3;
                exception_lineno = 49;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                goto try_except_handler_10;
            }
            goto branch_end_5;
            branch_no_5:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 46;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_8e914d53d9af12625ceb8d5aec97242b->m_frame) frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooo";
            goto try_except_handler_10;
            branch_end_5:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_2_call );
        return NULL;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        try_end_6:;
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_8;
        tmp_compexpr_left_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_altsep );
        if ( tmp_compexpr_left_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_6 = const_str_chr_92;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        tmp_compexpr_left_7 = const_str_chr_92;
        CHECK_OBJECT( var_bout );
        tmp_compexpr_right_7 = var_bout;
        tmp_res = PySequence_Contains( tmp_compexpr_right_7, tmp_compexpr_left_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_6 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_condition_result_6 = tmp_and_left_value_2;
        and_end_2:;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_raise_type_4;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_tuple_element_4;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_PkgConfigError );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PkgConfigError );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PkgConfigError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 54;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_9;
            tmp_left_name_3 = const_str_digest_ac3f4fc4213394446df35759ac278c3c;
            CHECK_OBJECT( par_flag );
            tmp_tuple_element_4 = par_flag;
            tmp_right_name_3 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( par_libname );
            tmp_tuple_element_4 = par_libname;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_4 );
            CHECK_OBJECT( var_bout );
            tmp_tuple_element_4 = var_bout;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_right_name_3, 2, tmp_tuple_element_4 );
            tmp_args_element_name_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            frame_8e914d53d9af12625ceb8d5aec97242b->m_frame.f_lineno = 54;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_raise_type_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_raise_type_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_4;
            exception_lineno = 54;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_6:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e914d53d9af12625ceb8d5aec97242b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e914d53d9af12625ceb8d5aec97242b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8e914d53d9af12625ceb8d5aec97242b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8e914d53d9af12625ceb8d5aec97242b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8e914d53d9af12625ceb8d5aec97242b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8e914d53d9af12625ceb8d5aec97242b,
        type_description_1,
        par_libname,
        par_flag,
        par_encoding,
        var_a,
        var_pc,
        var_e,
        var_bout,
        var_berr
    );


    // Release cached frame.
    if ( frame_8e914d53d9af12625ceb8d5aec97242b == cache_frame_8e914d53d9af12625ceb8d5aec97242b )
    {
        Py_DECREF( frame_8e914d53d9af12625ceb8d5aec97242b );
    }
    cache_frame_8e914d53d9af12625ceb8d5aec97242b = NULL;

    assertFrameObject( frame_8e914d53d9af12625ceb8d5aec97242b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_bout );
    tmp_return_value = var_bout;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_2_call );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_libname );
    Py_DECREF( par_libname );
    par_libname = NULL;

    CHECK_OBJECT( (PyObject *)par_flag );
    Py_DECREF( par_flag );
    par_flag = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)var_a );
    Py_DECREF( var_a );
    var_a = NULL;

    CHECK_OBJECT( (PyObject *)var_pc );
    Py_DECREF( var_pc );
    var_pc = NULL;

    CHECK_OBJECT( (PyObject *)var_bout );
    Py_DECREF( var_bout );
    var_bout = NULL;

    CHECK_OBJECT( (PyObject *)var_berr );
    Py_DECREF( var_berr );
    var_berr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_libname );
    Py_DECREF( par_libname );
    par_libname = NULL;

    CHECK_OBJECT( (PyObject *)par_flag );
    Py_DECREF( par_flag );
    par_flag = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)var_a );
    Py_DECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_pc );
    var_pc = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_bout );
    var_bout = NULL;

    Py_XDECREF( var_berr );
    var_berr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_2_call );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_libs = python_pars[ 0 ];
    struct Nuitka_CellObject *var_get_include_dirs = PyCell_EMPTY();
    struct Nuitka_CellObject *var_get_library_dirs = PyCell_EMPTY();
    struct Nuitka_CellObject *var_get_libraries = PyCell_EMPTY();
    struct Nuitka_CellObject *var_get_macros = PyCell_EMPTY();
    struct Nuitka_CellObject *var_get_other_cflags = PyCell_EMPTY();
    struct Nuitka_CellObject *var_get_other_libs = PyCell_EMPTY();
    PyObject *var_kwargs = NULL;
    PyObject *var_ret = NULL;
    PyObject *var_libname = NULL;
    PyObject *var_lib_flags = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_677545be6b2daf860f6d8f30017aed7f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_677545be6b2daf860f6d8f30017aed7f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs(  );



        assert( PyCell_GET( var_get_include_dirs ) == NULL );
        PyCell_SET( var_get_include_dirs, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs(  );



        assert( PyCell_GET( var_get_library_dirs ) == NULL );
        PyCell_SET( var_get_library_dirs, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries(  );



        assert( PyCell_GET( var_get_libraries ) == NULL );
        PyCell_SET( var_get_libraries, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros(  );



        assert( PyCell_GET( var_get_macros ) == NULL );
        PyCell_SET( var_get_macros, tmp_assign_source_4 );

    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags(  );



        assert( PyCell_GET( var_get_other_cflags ) == NULL );
        PyCell_SET( var_get_other_cflags, tmp_assign_source_5 );

    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs(  );



        assert( PyCell_GET( var_get_other_libs ) == NULL );
        PyCell_SET( var_get_other_libs, tmp_assign_source_6 );

    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] = var_get_include_dirs;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[1] = var_get_libraries;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[2] = var_get_library_dirs;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[3] = var_get_macros;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[3] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[4] = var_get_other_cflags;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[4] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[5] = var_get_other_libs;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[5] );


        assert( var_kwargs == NULL );
        var_kwargs = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = PyDict_New();
        assert( var_ret == NULL );
        var_ret = tmp_assign_source_8;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_677545be6b2daf860f6d8f30017aed7f, codeobj_677545be6b2daf860f6d8f30017aed7f, module_cffi$pkgconfig, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_677545be6b2daf860f6d8f30017aed7f = cache_frame_677545be6b2daf860f6d8f30017aed7f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_677545be6b2daf860f6d8f30017aed7f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_677545be6b2daf860f6d8f30017aed7f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_libs );
        tmp_iter_arg_1 = par_libs;
        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "occccccoooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_9;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "occccccoooo";
                exception_lineno = 118;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_11 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_libname;
            var_libname = tmp_assign_source_11;
            Py_INCREF( var_libname );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_kwargs );
        tmp_called_name_1 = var_kwargs;
        CHECK_OBJECT( var_libname );
        tmp_args_element_name_1 = var_libname;
        frame_677545be6b2daf860f6d8f30017aed7f->m_frame.f_lineno = 119;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "occccccoooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_lib_flags;
            var_lib_flags = tmp_assign_source_12;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_merge_flags );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_merge_flags );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "merge_flags" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "occccccoooo";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( var_ret );
        tmp_args_element_name_2 = var_ret;
        CHECK_OBJECT( var_lib_flags );
        tmp_args_element_name_3 = var_lib_flags;
        frame_677545be6b2daf860f6d8f30017aed7f->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "occccccoooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 118;
        type_description_1 = "occccccoooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_677545be6b2daf860f6d8f30017aed7f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_677545be6b2daf860f6d8f30017aed7f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_677545be6b2daf860f6d8f30017aed7f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_677545be6b2daf860f6d8f30017aed7f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_677545be6b2daf860f6d8f30017aed7f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_677545be6b2daf860f6d8f30017aed7f,
        type_description_1,
        par_libs,
        var_get_include_dirs,
        var_get_library_dirs,
        var_get_libraries,
        var_get_macros,
        var_get_other_cflags,
        var_get_other_libs,
        var_kwargs,
        var_ret,
        var_libname,
        var_lib_flags
    );


    // Release cached frame.
    if ( frame_677545be6b2daf860f6d8f30017aed7f == cache_frame_677545be6b2daf860f6d8f30017aed7f )
    {
        Py_DECREF( frame_677545be6b2daf860f6d8f30017aed7f );
    }
    cache_frame_677545be6b2daf860f6d8f30017aed7f = NULL;

    assertFrameObject( frame_677545be6b2daf860f6d8f30017aed7f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_ret );
    tmp_return_value = var_ret;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_libs );
    Py_DECREF( par_libs );
    par_libs = NULL;

    CHECK_OBJECT( (PyObject *)var_get_include_dirs );
    Py_DECREF( var_get_include_dirs );
    var_get_include_dirs = NULL;

    CHECK_OBJECT( (PyObject *)var_get_library_dirs );
    Py_DECREF( var_get_library_dirs );
    var_get_library_dirs = NULL;

    CHECK_OBJECT( (PyObject *)var_get_libraries );
    Py_DECREF( var_get_libraries );
    var_get_libraries = NULL;

    CHECK_OBJECT( (PyObject *)var_get_macros );
    Py_DECREF( var_get_macros );
    var_get_macros = NULL;

    CHECK_OBJECT( (PyObject *)var_get_other_cflags );
    Py_DECREF( var_get_other_cflags );
    var_get_other_cflags = NULL;

    CHECK_OBJECT( (PyObject *)var_get_other_libs );
    Py_DECREF( var_get_other_libs );
    var_get_other_libs = NULL;

    CHECK_OBJECT( (PyObject *)var_kwargs );
    Py_DECREF( var_kwargs );
    var_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_ret );
    Py_DECREF( var_ret );
    var_ret = NULL;

    Py_XDECREF( var_libname );
    var_libname = NULL;

    Py_XDECREF( var_lib_flags );
    var_lib_flags = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_libs );
    Py_DECREF( par_libs );
    par_libs = NULL;

    CHECK_OBJECT( (PyObject *)var_get_include_dirs );
    Py_DECREF( var_get_include_dirs );
    var_get_include_dirs = NULL;

    CHECK_OBJECT( (PyObject *)var_get_library_dirs );
    Py_DECREF( var_get_library_dirs );
    var_get_library_dirs = NULL;

    CHECK_OBJECT( (PyObject *)var_get_libraries );
    Py_DECREF( var_get_libraries );
    var_get_libraries = NULL;

    CHECK_OBJECT( (PyObject *)var_get_macros );
    Py_DECREF( var_get_macros );
    var_get_macros = NULL;

    CHECK_OBJECT( (PyObject *)var_get_other_cflags );
    Py_DECREF( var_get_other_cflags );
    var_get_other_cflags = NULL;

    CHECK_OBJECT( (PyObject *)var_get_other_libs );
    Py_DECREF( var_get_other_libs );
    var_get_other_libs = NULL;

    CHECK_OBJECT( (PyObject *)var_kwargs );
    Py_DECREF( var_kwargs );
    var_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_ret );
    Py_DECREF( var_ret );
    var_ret = NULL;

    Py_XDECREF( var_libname );
    var_libname = NULL;

    Py_XDECREF( var_lib_flags );
    var_lib_flags = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_50a6cf518d1fc0705863a31b6a70d3f6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_a29d0ab846e79881efc107e37df4b4d8_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_a29d0ab846e79881efc107e37df4b4d8_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_50a6cf518d1fc0705863a31b6a70d3f6 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_50a6cf518d1fc0705863a31b6a70d3f6, codeobj_50a6cf518d1fc0705863a31b6a70d3f6, module_cffi$pkgconfig, sizeof(void *) );
    frame_50a6cf518d1fc0705863a31b6a70d3f6 = cache_frame_50a6cf518d1fc0705863a31b6a70d3f6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_50a6cf518d1fc0705863a31b6a70d3f6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_50a6cf518d1fc0705863a31b6a70d3f6 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_string );
        tmp_called_instance_1 = par_string;
        frame_50a6cf518d1fc0705863a31b6a70d3f6->m_frame.f_lineno = 76;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_a29d0ab846e79881efc107e37df4b4d8_2, codeobj_a29d0ab846e79881efc107e37df4b4d8, module_cffi$pkgconfig, sizeof(void *) );
    frame_a29d0ab846e79881efc107e37df4b4d8_2 = cache_frame_a29d0ab846e79881efc107e37df4b4d8_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a29d0ab846e79881efc107e37df4b4d8_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a29d0ab846e79881efc107e37df4b4d8_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 76;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_x;
            outline_0_var_x = tmp_assign_source_4;
            Py_INCREF( outline_0_var_x );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_2 = outline_0_var_x;
        frame_a29d0ab846e79881efc107e37df4b4d8_2->m_frame.f_lineno = 76;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_571eccf4664cb01ecea1867fcbf66f8d_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 76;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_x );
            tmp_subscribed_name_1 = outline_0_var_x;
            tmp_subscript_name_1 = const_slice_int_pos_2_none_none;
            tmp_append_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        type_description_2 = "o";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a29d0ab846e79881efc107e37df4b4d8_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a29d0ab846e79881efc107e37df4b4d8_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a29d0ab846e79881efc107e37df4b4d8_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a29d0ab846e79881efc107e37df4b4d8_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a29d0ab846e79881efc107e37df4b4d8_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a29d0ab846e79881efc107e37df4b4d8_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a29d0ab846e79881efc107e37df4b4d8_2,
        type_description_2,
        outline_0_var_x
    );


    // Release cached frame.
    if ( frame_a29d0ab846e79881efc107e37df4b4d8_2 == cache_frame_a29d0ab846e79881efc107e37df4b4d8_2 )
    {
        Py_DECREF( frame_a29d0ab846e79881efc107e37df4b4d8_2 );
    }
    cache_frame_a29d0ab846e79881efc107e37df4b4d8_2 = NULL;

    assertFrameObject( frame_a29d0ab846e79881efc107e37df4b4d8_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs );
    return NULL;
    outline_exception_1:;
    exception_lineno = 76;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50a6cf518d1fc0705863a31b6a70d3f6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_50a6cf518d1fc0705863a31b6a70d3f6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50a6cf518d1fc0705863a31b6a70d3f6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_50a6cf518d1fc0705863a31b6a70d3f6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_50a6cf518d1fc0705863a31b6a70d3f6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_50a6cf518d1fc0705863a31b6a70d3f6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_50a6cf518d1fc0705863a31b6a70d3f6,
        type_description_1,
        par_string
    );


    // Release cached frame.
    if ( frame_50a6cf518d1fc0705863a31b6a70d3f6 == cache_frame_50a6cf518d1fc0705863a31b6a70d3f6 )
    {
        Py_DECREF( frame_50a6cf518d1fc0705863a31b6a70d3f6 );
    }
    cache_frame_50a6cf518d1fc0705863a31b6a70d3f6 = NULL;

    assertFrameObject( frame_50a6cf518d1fc0705863a31b6a70d3f6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_e7bc752963c4f65030506fc7c1833e33;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_8e9d071f46a68f760619dc1b636266b4_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_8e9d071f46a68f760619dc1b636266b4_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_e7bc752963c4f65030506fc7c1833e33 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e7bc752963c4f65030506fc7c1833e33, codeobj_e7bc752963c4f65030506fc7c1833e33, module_cffi$pkgconfig, sizeof(void *) );
    frame_e7bc752963c4f65030506fc7c1833e33 = cache_frame_e7bc752963c4f65030506fc7c1833e33;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e7bc752963c4f65030506fc7c1833e33 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e7bc752963c4f65030506fc7c1833e33 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_string );
        tmp_called_instance_1 = par_string;
        frame_e7bc752963c4f65030506fc7c1833e33->m_frame.f_lineno = 79;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_8e9d071f46a68f760619dc1b636266b4_2, codeobj_8e9d071f46a68f760619dc1b636266b4, module_cffi$pkgconfig, sizeof(void *) );
    frame_8e9d071f46a68f760619dc1b636266b4_2 = cache_frame_8e9d071f46a68f760619dc1b636266b4_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8e9d071f46a68f760619dc1b636266b4_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8e9d071f46a68f760619dc1b636266b4_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 79;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_x;
            outline_0_var_x = tmp_assign_source_4;
            Py_INCREF( outline_0_var_x );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_2 = outline_0_var_x;
        frame_8e9d071f46a68f760619dc1b636266b4_2->m_frame.f_lineno = 79;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_9870a3ace7357eb3938eb55819eb2a49_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 79;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_x );
            tmp_subscribed_name_1 = outline_0_var_x;
            tmp_subscript_name_1 = const_slice_int_pos_2_none_none;
            tmp_append_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 79;
        type_description_2 = "o";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e9d071f46a68f760619dc1b636266b4_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e9d071f46a68f760619dc1b636266b4_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e9d071f46a68f760619dc1b636266b4_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8e9d071f46a68f760619dc1b636266b4_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8e9d071f46a68f760619dc1b636266b4_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8e9d071f46a68f760619dc1b636266b4_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8e9d071f46a68f760619dc1b636266b4_2,
        type_description_2,
        outline_0_var_x
    );


    // Release cached frame.
    if ( frame_8e9d071f46a68f760619dc1b636266b4_2 == cache_frame_8e9d071f46a68f760619dc1b636266b4_2 )
    {
        Py_DECREF( frame_8e9d071f46a68f760619dc1b636266b4_2 );
    }
    cache_frame_8e9d071f46a68f760619dc1b636266b4_2 = NULL;

    assertFrameObject( frame_8e9d071f46a68f760619dc1b636266b4_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs );
    return NULL;
    outline_exception_1:;
    exception_lineno = 79;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7bc752963c4f65030506fc7c1833e33 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7bc752963c4f65030506fc7c1833e33 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e7bc752963c4f65030506fc7c1833e33 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e7bc752963c4f65030506fc7c1833e33, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e7bc752963c4f65030506fc7c1833e33->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e7bc752963c4f65030506fc7c1833e33, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e7bc752963c4f65030506fc7c1833e33,
        type_description_1,
        par_string
    );


    // Release cached frame.
    if ( frame_e7bc752963c4f65030506fc7c1833e33 == cache_frame_e7bc752963c4f65030506fc7c1833e33 )
    {
        Py_DECREF( frame_e7bc752963c4f65030506fc7c1833e33 );
    }
    cache_frame_e7bc752963c4f65030506fc7c1833e33 = NULL;

    assertFrameObject( frame_e7bc752963c4f65030506fc7c1833e33 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_6da2625bbbf9aa031107b6c08ed8706a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_37e6613b1bf27d7de778f889e87a3b63_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_37e6613b1bf27d7de778f889e87a3b63_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_6da2625bbbf9aa031107b6c08ed8706a = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6da2625bbbf9aa031107b6c08ed8706a, codeobj_6da2625bbbf9aa031107b6c08ed8706a, module_cffi$pkgconfig, sizeof(void *) );
    frame_6da2625bbbf9aa031107b6c08ed8706a = cache_frame_6da2625bbbf9aa031107b6c08ed8706a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6da2625bbbf9aa031107b6c08ed8706a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6da2625bbbf9aa031107b6c08ed8706a ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_string );
        tmp_called_instance_1 = par_string;
        frame_6da2625bbbf9aa031107b6c08ed8706a->m_frame.f_lineno = 82;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_37e6613b1bf27d7de778f889e87a3b63_2, codeobj_37e6613b1bf27d7de778f889e87a3b63, module_cffi$pkgconfig, sizeof(void *) );
    frame_37e6613b1bf27d7de778f889e87a3b63_2 = cache_frame_37e6613b1bf27d7de778f889e87a3b63_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_37e6613b1bf27d7de778f889e87a3b63_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_37e6613b1bf27d7de778f889e87a3b63_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 82;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_x;
            outline_0_var_x = tmp_assign_source_4;
            Py_INCREF( outline_0_var_x );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_2 = outline_0_var_x;
        frame_37e6613b1bf27d7de778f889e87a3b63_2->m_frame.f_lineno = 82;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 82;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_x );
            tmp_subscribed_name_1 = outline_0_var_x;
            tmp_subscript_name_1 = const_slice_int_pos_2_none_none;
            tmp_append_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 82;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 82;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 82;
        type_description_2 = "o";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_37e6613b1bf27d7de778f889e87a3b63_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_37e6613b1bf27d7de778f889e87a3b63_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_37e6613b1bf27d7de778f889e87a3b63_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_37e6613b1bf27d7de778f889e87a3b63_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_37e6613b1bf27d7de778f889e87a3b63_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_37e6613b1bf27d7de778f889e87a3b63_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_37e6613b1bf27d7de778f889e87a3b63_2,
        type_description_2,
        outline_0_var_x
    );


    // Release cached frame.
    if ( frame_37e6613b1bf27d7de778f889e87a3b63_2 == cache_frame_37e6613b1bf27d7de778f889e87a3b63_2 )
    {
        Py_DECREF( frame_37e6613b1bf27d7de778f889e87a3b63_2 );
    }
    cache_frame_37e6613b1bf27d7de778f889e87a3b63_2 = NULL;

    assertFrameObject( frame_37e6613b1bf27d7de778f889e87a3b63_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries );
    return NULL;
    outline_exception_1:;
    exception_lineno = 82;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6da2625bbbf9aa031107b6c08ed8706a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6da2625bbbf9aa031107b6c08ed8706a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6da2625bbbf9aa031107b6c08ed8706a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6da2625bbbf9aa031107b6c08ed8706a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6da2625bbbf9aa031107b6c08ed8706a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6da2625bbbf9aa031107b6c08ed8706a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6da2625bbbf9aa031107b6c08ed8706a,
        type_description_1,
        par_string
    );


    // Release cached frame.
    if ( frame_6da2625bbbf9aa031107b6c08ed8706a == cache_frame_6da2625bbbf9aa031107b6c08ed8706a )
    {
        Py_DECREF( frame_6da2625bbbf9aa031107b6c08ed8706a );
    }
    cache_frame_6da2625bbbf9aa031107b6c08ed8706a = NULL;

    assertFrameObject( frame_6da2625bbbf9aa031107b6c08ed8706a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    PyObject *var__macro = NULL;
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_49691a548ce33c04a71c62b934547aed;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_f9078ed88ad5f8460e26cdc3d0e747f8_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_49691a548ce33c04a71c62b934547aed = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro(  );



        assert( var__macro == NULL );
        var__macro = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_49691a548ce33c04a71c62b934547aed, codeobj_49691a548ce33c04a71c62b934547aed, module_cffi$pkgconfig, sizeof(void *)+sizeof(void *) );
    frame_49691a548ce33c04a71c62b934547aed = cache_frame_49691a548ce33c04a71c62b934547aed;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_49691a548ce33c04a71c62b934547aed );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_49691a548ce33c04a71c62b934547aed ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_string );
        tmp_called_instance_1 = par_string;
        frame_49691a548ce33c04a71c62b934547aed->m_frame.f_lineno = 92;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_3;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_f9078ed88ad5f8460e26cdc3d0e747f8_2, codeobj_f9078ed88ad5f8460e26cdc3d0e747f8, module_cffi$pkgconfig, sizeof(void *)+sizeof(void *) );
    frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 = cache_frame_f9078ed88ad5f8460e26cdc3d0e747f8_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "oo";
                exception_lineno = 92;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_x;
            outline_0_var_x = tmp_assign_source_5;
            Py_INCREF( outline_0_var_x );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_2 = outline_0_var_x;
        frame_f9078ed88ad5f8460e26cdc3d0e747f8_2->m_frame.f_lineno = 92;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 92;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( var__macro );
            tmp_called_name_1 = var__macro;
            CHECK_OBJECT( outline_0_var_x );
            tmp_args_element_name_1 = outline_0_var_x;
            frame_f9078ed88ad5f8460e26cdc3d0e747f8_2->m_frame.f_lineno = 92;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 92;
        type_description_2 = "oo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f9078ed88ad5f8460e26cdc3d0e747f8_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f9078ed88ad5f8460e26cdc3d0e747f8_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f9078ed88ad5f8460e26cdc3d0e747f8_2,
        type_description_2,
        outline_0_var_x,
        var__macro
    );


    // Release cached frame.
    if ( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 == cache_frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 )
    {
        Py_DECREF( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 );
    }
    cache_frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 = NULL;

    assertFrameObject( frame_f9078ed88ad5f8460e26cdc3d0e747f8_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "oo";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros );
    return NULL;
    outline_exception_1:;
    exception_lineno = 92;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_49691a548ce33c04a71c62b934547aed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_49691a548ce33c04a71c62b934547aed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_49691a548ce33c04a71c62b934547aed );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_49691a548ce33c04a71c62b934547aed, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_49691a548ce33c04a71c62b934547aed->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_49691a548ce33c04a71c62b934547aed, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_49691a548ce33c04a71c62b934547aed,
        type_description_1,
        par_string,
        var__macro
    );


    // Release cached frame.
    if ( frame_49691a548ce33c04a71c62b934547aed == cache_frame_49691a548ce33c04a71c62b934547aed )
    {
        Py_DECREF( frame_49691a548ce33c04a71c62b934547aed );
    }
    cache_frame_49691a548ce33c04a71c62b934547aed = NULL;

    assertFrameObject( frame_49691a548ce33c04a71c62b934547aed );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    CHECK_OBJECT( (PyObject *)var__macro );
    Py_DECREF( var__macro );
    var__macro = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    CHECK_OBJECT( (PyObject *)var__macro );
    Py_DECREF( var__macro );
    var__macro = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e460aa9bcda8fda05b695e0d1f096030;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e460aa9bcda8fda05b695e0d1f096030 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e460aa9bcda8fda05b695e0d1f096030, codeobj_e460aa9bcda8fda05b695e0d1f096030, module_cffi$pkgconfig, sizeof(void *) );
    frame_e460aa9bcda8fda05b695e0d1f096030 = cache_frame_e460aa9bcda8fda05b695e0d1f096030;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e460aa9bcda8fda05b695e0d1f096030 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e460aa9bcda8fda05b695e0d1f096030 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_x );
        tmp_subscribed_name_1 = par_x;
        tmp_subscript_name_1 = const_slice_int_pos_2_none_none;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_x;
            assert( old != NULL );
            par_x = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = const_str_chr_61;
        CHECK_OBJECT( par_x );
        tmp_compexpr_right_1 = par_x;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_tuple_arg_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_x );
            tmp_called_instance_1 = par_x;
            frame_e460aa9bcda8fda05b695e0d1f096030->m_frame.f_lineno = 89;
            tmp_tuple_arg_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_chr_61_int_pos_1_tuple, 0 ) );

            if ( tmp_tuple_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_return_value = PySequence_Tuple( tmp_tuple_arg_1 );
            Py_DECREF( tmp_tuple_arg_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( par_x );
            tmp_tuple_element_1 = par_x;
            tmp_return_value = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
            tmp_tuple_element_1 = Py_None;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e460aa9bcda8fda05b695e0d1f096030 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e460aa9bcda8fda05b695e0d1f096030 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e460aa9bcda8fda05b695e0d1f096030 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e460aa9bcda8fda05b695e0d1f096030, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e460aa9bcda8fda05b695e0d1f096030->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e460aa9bcda8fda05b695e0d1f096030, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e460aa9bcda8fda05b695e0d1f096030,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_e460aa9bcda8fda05b695e0d1f096030 == cache_frame_e460aa9bcda8fda05b695e0d1f096030 )
    {
        Py_DECREF( frame_e460aa9bcda8fda05b695e0d1f096030 );
    }
    cache_frame_e460aa9bcda8fda05b695e0d1f096030 = NULL;

    assertFrameObject( frame_e460aa9bcda8fda05b695e0d1f096030 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_31a3d120e78fa5c17818142281108366;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_4409553ea769b9ad3f8ed4142228ea35_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_4409553ea769b9ad3f8ed4142228ea35_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_31a3d120e78fa5c17818142281108366 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_31a3d120e78fa5c17818142281108366, codeobj_31a3d120e78fa5c17818142281108366, module_cffi$pkgconfig, sizeof(void *) );
    frame_31a3d120e78fa5c17818142281108366 = cache_frame_31a3d120e78fa5c17818142281108366;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_31a3d120e78fa5c17818142281108366 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_31a3d120e78fa5c17818142281108366 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_string );
        tmp_called_instance_1 = par_string;
        frame_31a3d120e78fa5c17818142281108366->m_frame.f_lineno = 95;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_4409553ea769b9ad3f8ed4142228ea35_2, codeobj_4409553ea769b9ad3f8ed4142228ea35, module_cffi$pkgconfig, sizeof(void *) );
    frame_4409553ea769b9ad3f8ed4142228ea35_2 = cache_frame_4409553ea769b9ad3f8ed4142228ea35_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4409553ea769b9ad3f8ed4142228ea35_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4409553ea769b9ad3f8ed4142228ea35_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 95;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_x;
            outline_0_var_x = tmp_assign_source_4;
            Py_INCREF( outline_0_var_x );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_2 = outline_0_var_x;
        frame_4409553ea769b9ad3f8ed4142228ea35_2->m_frame.f_lineno = 95;
        tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_571eccf4664cb01ecea1867fcbf66f8d_tuple, 0 ) );

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_and_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_3 = outline_0_var_x;
        frame_4409553ea769b9ad3f8ed4142228ea35_2->m_frame.f_lineno = 96;
        tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_1e1c78e5ad306ab29a8b7c1012aa3742_tuple, 0 ) );

        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_x );
            tmp_append_value_1 = outline_0_var_x;
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 95;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 95;
        type_description_2 = "o";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4409553ea769b9ad3f8ed4142228ea35_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4409553ea769b9ad3f8ed4142228ea35_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4409553ea769b9ad3f8ed4142228ea35_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4409553ea769b9ad3f8ed4142228ea35_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4409553ea769b9ad3f8ed4142228ea35_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4409553ea769b9ad3f8ed4142228ea35_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4409553ea769b9ad3f8ed4142228ea35_2,
        type_description_2,
        outline_0_var_x
    );


    // Release cached frame.
    if ( frame_4409553ea769b9ad3f8ed4142228ea35_2 == cache_frame_4409553ea769b9ad3f8ed4142228ea35_2 )
    {
        Py_DECREF( frame_4409553ea769b9ad3f8ed4142228ea35_2 );
    }
    cache_frame_4409553ea769b9ad3f8ed4142228ea35_2 = NULL;

    assertFrameObject( frame_4409553ea769b9ad3f8ed4142228ea35_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags );
    return NULL;
    outline_exception_1:;
    exception_lineno = 95;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_31a3d120e78fa5c17818142281108366 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_31a3d120e78fa5c17818142281108366 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_31a3d120e78fa5c17818142281108366 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_31a3d120e78fa5c17818142281108366, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_31a3d120e78fa5c17818142281108366->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_31a3d120e78fa5c17818142281108366, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_31a3d120e78fa5c17818142281108366,
        type_description_1,
        par_string
    );


    // Release cached frame.
    if ( frame_31a3d120e78fa5c17818142281108366 == cache_frame_31a3d120e78fa5c17818142281108366 )
    {
        Py_DECREF( frame_31a3d120e78fa5c17818142281108366 );
    }
    cache_frame_31a3d120e78fa5c17818142281108366 = NULL;

    assertFrameObject( frame_31a3d120e78fa5c17818142281108366 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_string = python_pars[ 0 ];
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_6fa2080fb82cd5dd1042652f76372080;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_88cb1c45161649d40c67807e0aabae41_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_88cb1c45161649d40c67807e0aabae41_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_6fa2080fb82cd5dd1042652f76372080 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6fa2080fb82cd5dd1042652f76372080, codeobj_6fa2080fb82cd5dd1042652f76372080, module_cffi$pkgconfig, sizeof(void *) );
    frame_6fa2080fb82cd5dd1042652f76372080 = cache_frame_6fa2080fb82cd5dd1042652f76372080;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6fa2080fb82cd5dd1042652f76372080 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6fa2080fb82cd5dd1042652f76372080 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_string );
        tmp_called_instance_1 = par_string;
        frame_6fa2080fb82cd5dd1042652f76372080->m_frame.f_lineno = 99;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_split );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_88cb1c45161649d40c67807e0aabae41_2, codeobj_88cb1c45161649d40c67807e0aabae41, module_cffi$pkgconfig, sizeof(void *) );
    frame_88cb1c45161649d40c67807e0aabae41_2 = cache_frame_88cb1c45161649d40c67807e0aabae41_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_88cb1c45161649d40c67807e0aabae41_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_88cb1c45161649d40c67807e0aabae41_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "o";
                exception_lineno = 99;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_x;
            outline_0_var_x = tmp_assign_source_4;
            Py_INCREF( outline_0_var_x );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_2 = outline_0_var_x;
        frame_88cb1c45161649d40c67807e0aabae41_2->m_frame.f_lineno = 99;
        tmp_operand_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_9870a3ace7357eb3938eb55819eb2a49_tuple, 0 ) );

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_and_left_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( outline_0_var_x );
        tmp_called_instance_3 = outline_0_var_x;
        frame_88cb1c45161649d40c67807e0aabae41_2->m_frame.f_lineno = 100;
        tmp_operand_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_8ff55f2924abba1dc6fcc430fa7ff3a7_tuple, 0 ) );

        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_x );
            tmp_append_value_1 = outline_0_var_x;
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 99;
        type_description_2 = "o";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_88cb1c45161649d40c67807e0aabae41_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_88cb1c45161649d40c67807e0aabae41_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_88cb1c45161649d40c67807e0aabae41_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_88cb1c45161649d40c67807e0aabae41_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_88cb1c45161649d40c67807e0aabae41_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_88cb1c45161649d40c67807e0aabae41_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_88cb1c45161649d40c67807e0aabae41_2,
        type_description_2,
        outline_0_var_x
    );


    // Release cached frame.
    if ( frame_88cb1c45161649d40c67807e0aabae41_2 == cache_frame_88cb1c45161649d40c67807e0aabae41_2 )
    {
        Py_DECREF( frame_88cb1c45161649d40c67807e0aabae41_2 );
    }
    cache_frame_88cb1c45161649d40c67807e0aabae41_2 = NULL;

    assertFrameObject( frame_88cb1c45161649d40c67807e0aabae41_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_x );
    outline_0_var_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs );
    return NULL;
    outline_exception_1:;
    exception_lineno = 99;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fa2080fb82cd5dd1042652f76372080 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fa2080fb82cd5dd1042652f76372080 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6fa2080fb82cd5dd1042652f76372080 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6fa2080fb82cd5dd1042652f76372080, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6fa2080fb82cd5dd1042652f76372080->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6fa2080fb82cd5dd1042652f76372080, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6fa2080fb82cd5dd1042652f76372080,
        type_description_1,
        par_string
    );


    // Release cached frame.
    if ( frame_6fa2080fb82cd5dd1042652f76372080 == cache_frame_6fa2080fb82cd5dd1042652f76372080 )
    {
        Py_DECREF( frame_6fa2080fb82cd5dd1042652f76372080 );
    }
    cache_frame_6fa2080fb82cd5dd1042652f76372080 = NULL;

    assertFrameObject( frame_6fa2080fb82cd5dd1042652f76372080 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_string );
    Py_DECREF( par_string );
    par_string = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_libname = python_pars[ 0 ];
    PyObject *var_fse = NULL;
    PyObject *var_all_cflags = NULL;
    PyObject *var_all_libs = NULL;
    struct Nuitka_FrameObject *frame_0dbac9949f9d1e0598072df5c8b8af62;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_0dbac9949f9d1e0598072df5c8b8af62 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0dbac9949f9d1e0598072df5c8b8af62, codeobj_0dbac9949f9d1e0598072df5c8b8af62, module_cffi$pkgconfig, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0dbac9949f9d1e0598072df5c8b8af62 = cache_frame_0dbac9949f9d1e0598072df5c8b8af62;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0dbac9949f9d1e0598072df5c8b8af62 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0dbac9949f9d1e0598072df5c8b8af62 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 104;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_getfilesystemencoding );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        assert( var_fse == NULL );
        var_fse = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_call );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_call );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "call" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_libname );
        tmp_args_element_name_1 = par_libname;
        tmp_args_element_name_2 = const_str_digest_c1275aa5deb6bb446724ec968188db8e;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        assert( var_all_cflags == NULL );
        var_all_cflags = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_call );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_call );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "call" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_libname );
        tmp_args_element_name_3 = par_libname;
        tmp_args_element_name_4 = const_str_digest_7bfbc57b26bdf1b702d94c1830a1b38e;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 106;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        assert( var_all_libs == NULL );
        var_all_libs = tmp_assign_source_3;
    }
    {
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_called_name_5;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_called_name_6;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_called_name_7;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_called_name_8;
        PyObject *tmp_args_element_name_10;
        tmp_dict_key_1 = const_str_plain_include_dirs;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_include_dirs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( var_all_cflags );
        tmp_args_element_name_5 = var_all_cflags;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 108;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_return_value = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_library_dirs;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_library_dirs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 109;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = PyCell_GET( self->m_closure[2] );
        CHECK_OBJECT( var_all_libs );
        tmp_args_element_name_6 = var_all_libs;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 109;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_dict_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 109;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_libraries;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_libraries" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( var_all_libs );
        tmp_args_element_name_7 = var_all_libs;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_dict_value_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 110;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_define_macros;
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_macros" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = PyCell_GET( self->m_closure[3] );
        CHECK_OBJECT( var_all_cflags );
        tmp_args_element_name_8 = var_all_cflags;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_dict_value_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 111;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_extra_compile_args;
        if ( PyCell_GET( self->m_closure[4] ) == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_other_cflags" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = PyCell_GET( self->m_closure[4] );
        CHECK_OBJECT( var_all_cflags );
        tmp_args_element_name_9 = var_all_cflags;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_dict_value_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        if ( tmp_dict_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 112;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_extra_link_args;
        if ( PyCell_GET( self->m_closure[5] ) == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "get_other_libs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = PyCell_GET( self->m_closure[5] );
        CHECK_OBJECT( var_all_libs );
        tmp_args_element_name_10 = var_all_libs;
        frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame.f_lineno = 113;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_dict_value_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        if ( tmp_dict_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_return_value );

            exception_lineno = 113;
            type_description_1 = "oooocccccc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0dbac9949f9d1e0598072df5c8b8af62 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0dbac9949f9d1e0598072df5c8b8af62 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0dbac9949f9d1e0598072df5c8b8af62 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0dbac9949f9d1e0598072df5c8b8af62, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0dbac9949f9d1e0598072df5c8b8af62->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0dbac9949f9d1e0598072df5c8b8af62, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0dbac9949f9d1e0598072df5c8b8af62,
        type_description_1,
        par_libname,
        var_fse,
        var_all_cflags,
        var_all_libs,
        self->m_closure[0],
        self->m_closure[2],
        self->m_closure[1],
        self->m_closure[3],
        self->m_closure[4],
        self->m_closure[5]
    );


    // Release cached frame.
    if ( frame_0dbac9949f9d1e0598072df5c8b8af62 == cache_frame_0dbac9949f9d1e0598072df5c8b8af62 )
    {
        Py_DECREF( frame_0dbac9949f9d1e0598072df5c8b8af62 );
    }
    cache_frame_0dbac9949f9d1e0598072df5c8b8af62 = NULL;

    assertFrameObject( frame_0dbac9949f9d1e0598072df5c8b8af62 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_libname );
    Py_DECREF( par_libname );
    par_libname = NULL;

    CHECK_OBJECT( (PyObject *)var_fse );
    Py_DECREF( var_fse );
    var_fse = NULL;

    CHECK_OBJECT( (PyObject *)var_all_cflags );
    Py_DECREF( var_all_cflags );
    var_all_cflags = NULL;

    CHECK_OBJECT( (PyObject *)var_all_libs );
    Py_DECREF( var_all_libs );
    var_all_libs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_libname );
    Py_DECREF( par_libname );
    par_libname = NULL;

    Py_XDECREF( var_fse );
    var_fse = NULL;

    Py_XDECREF( var_all_cflags );
    var_all_cflags = NULL;

    Py_XDECREF( var_all_libs );
    var_all_libs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_1_merge_flags(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_1_merge_flags,
        const_str_plain_merge_flags,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b879299286186359c279df79176b4244,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        const_str_digest_98e43324dd3156c35f65918332800bb0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_2_call( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_2_call,
        const_str_plain_call,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8e914d53d9af12625ceb8d5aec97242b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        const_str_digest_698989052dca859c5e8622ba5404fa01,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig,
        const_str_plain_flags_from_pkgconfig,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_677545be6b2daf860f6d8f30017aed7f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        const_str_digest_78de961cee629be58db46bc0f3de1d5d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_1_get_include_dirs,
        const_str_plain_get_include_dirs,
#if PYTHON_VERSION >= 300
        const_str_digest_1f3be37482e2e2ffa90e044d6fd21859,
#endif
        codeobj_50a6cf518d1fc0705863a31b6a70d3f6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_2_get_library_dirs,
        const_str_plain_get_library_dirs,
#if PYTHON_VERSION >= 300
        const_str_digest_9971bee4a83ffe5fd693568114cd1837,
#endif
        codeobj_e7bc752963c4f65030506fc7c1833e33,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_3_get_libraries,
        const_str_plain_get_libraries,
#if PYTHON_VERSION >= 300
        const_str_digest_9102dcc66aa2f241d2460e0ec5e7734c,
#endif
        codeobj_6da2625bbbf9aa031107b6c08ed8706a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros,
        const_str_plain_get_macros,
#if PYTHON_VERSION >= 300
        const_str_digest_469be60f53f1755b4aead40cf2e73689,
#endif
        codeobj_49691a548ce33c04a71c62b934547aed,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_4_get_macros$$$function_1__macro,
        const_str_plain__macro,
#if PYTHON_VERSION >= 300
        const_str_digest_2edc37e13b19d31b2954d5650d171696,
#endif
        codeobj_e460aa9bcda8fda05b695e0d1f096030,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_5_get_other_cflags,
        const_str_plain_get_other_cflags,
#if PYTHON_VERSION >= 300
        const_str_digest_c734b157194e4cfe6b7a4d0fcbc9e221,
#endif
        codeobj_31a3d120e78fa5c17818142281108366,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_6_get_other_libs,
        const_str_plain_get_other_libs,
#if PYTHON_VERSION >= 300
        const_str_digest_42bb4843562d0537c6721bc53ce8dba3,
#endif
        codeobj_6fa2080fb82cd5dd1042652f76372080,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$pkgconfig$$$function_3_flags_from_pkgconfig$$$function_7_kwargs,
        const_str_plain_kwargs,
#if PYTHON_VERSION >= 300
        const_str_digest_d521200a76c655966214e3d83504daf9,
#endif
        codeobj_0dbac9949f9d1e0598072df5c8b8af62,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$pkgconfig,
        NULL,
        6
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_cffi$pkgconfig =
{
    PyModuleDef_HEAD_INIT,
    "cffi.pkgconfig",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(cffi$pkgconfig)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(cffi$pkgconfig)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_cffi$pkgconfig );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("cffi.pkgconfig: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("cffi.pkgconfig: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("cffi.pkgconfig: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initcffi$pkgconfig" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_cffi$pkgconfig = Py_InitModule4(
        "cffi.pkgconfig",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_cffi$pkgconfig = PyModule_Create( &mdef_cffi$pkgconfig );
#endif

    moduledict_cffi$pkgconfig = MODULE_DICT( module_cffi$pkgconfig );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_cffi$pkgconfig,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_cffi$pkgconfig,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_cffi$pkgconfig,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_cffi$pkgconfig,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_cffi$pkgconfig );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_a707b596db3c29ec22f6fd3a21719c67, module_cffi$pkgconfig );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_f4845c574ec21b2e87029a6a6ea8c1ca;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_f4845c574ec21b2e87029a6a6ea8c1ca = MAKE_MODULE_FRAME( codeobj_f4845c574ec21b2e87029a6a6ea8c1ca, module_cffi$pkgconfig );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_f4845c574ec21b2e87029a6a6ea8c1ca );
    assert( Py_REFCNT( frame_f4845c574ec21b2e87029a6a6ea8c1ca ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_sys;
        tmp_globals_name_1 = (PyObject *)moduledict_cffi$pkgconfig;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_f4845c574ec21b2e87029a6a6ea8c1ca->m_frame.f_lineno = 2;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_os;
        tmp_globals_name_2 = (PyObject *)moduledict_cffi$pkgconfig;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_f4845c574ec21b2e87029a6a6ea8c1ca->m_frame.f_lineno = 2;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_subprocess;
        tmp_globals_name_3 = (PyObject *)moduledict_cffi$pkgconfig;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_f4845c574ec21b2e87029a6a6ea8c1ca->m_frame.f_lineno = 2;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_subprocess, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_error;
        tmp_globals_name_4 = (PyObject *)moduledict_cffi$pkgconfig;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_PkgConfigError_tuple;
        tmp_level_name_4 = const_int_pos_1;
        frame_f4845c574ec21b2e87029a6a6ea8c1ca->m_frame.f_lineno = 4;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_7 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_cffi$pkgconfig,
                const_str_plain_PkgConfigError,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_PkgConfigError );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_PkgConfigError, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = MAKE_FUNCTION_cffi$pkgconfig$$$function_1_merge_flags(  );



        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_merge_flags, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_defaults_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 26;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_f4845c574ec21b2e87029a6a6ea8c1ca->m_frame.f_lineno = 26;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_getfilesystemencoding );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_defaults_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
        tmp_assign_source_9 = MAKE_FUNCTION_cffi$pkgconfig$$$function_2_call( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_call, tmp_assign_source_9 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4845c574ec21b2e87029a6a6ea8c1ca );
#endif
    popFrameStack();

    assertFrameObject( frame_f4845c574ec21b2e87029a6a6ea8c1ca );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4845c574ec21b2e87029a6a6ea8c1ca );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f4845c574ec21b2e87029a6a6ea8c1ca, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f4845c574ec21b2e87029a6a6ea8c1ca->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f4845c574ec21b2e87029a6a6ea8c1ca, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_cffi$pkgconfig$$$function_3_flags_from_pkgconfig(  );



        UPDATE_STRING_DICT1( moduledict_cffi$pkgconfig, (Nuitka_StringObject *)const_str_plain_flags_from_pkgconfig, tmp_assign_source_10 );
    }

    return MOD_RETURN_VALUE( module_cffi$pkgconfig );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
