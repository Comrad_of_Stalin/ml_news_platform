/* Generated code for Python module 'jupyter_client.launcher'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jupyter_client$launcher" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jupyter_client$launcher;
PyDictObject *moduledict_jupyter_client$launcher;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_angle_dictcontraction;
static PyObject *const_str_digest_3a446f0691307d3156e6a88d64741e5b;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_win32_interrupt_event;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_preexec_fn;
static PyObject *const_str_digest_77577b44b8280b183dc130f9110d86aa;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_JPY_PARENT_PID;
extern PyObject *const_str_plain_getdefaultencoding;
extern PyObject *const_str_plain_env;
extern PyObject *const_str_plain_prefer_stream;
extern PyObject *const_str_plain_get_logger;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_getfilesystemencoding;
extern PyObject *const_str_plain_stdout;
static PyObject *const_tuple_str_plain_create_interrupt_event_tuple;
extern PyObject *const_str_digest_50bd9189c156b5539c462ddc680b0a93;
extern PyObject *const_str_plain_platform;
static PyObject *const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple;
extern PyObject *const_str_plain_PY3;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_PIPE;
extern PyObject *const_str_plain_Popen;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_close;
static PyObject *const_tuple_str_plain_getdefaultencoding_tuple;
extern PyObject *const_str_plain_devnull;
extern PyObject *const_str_digest_f239d4ac02f17c205f0d85fba95f72ff;
static PyObject *const_str_digest_c15ef88fea42b3f34f0d1835bf5a8659;
extern PyObject *const_tuple_str_plain_key_str_plain_value_tuple;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_digest_a854e3b2bb775c8c59ed1fbb95556781;
extern PyObject *const_str_plain_value;
static PyObject *const_str_plain_CREATE_NEW_PROCESS_GROUP;
extern PyObject *const_str_plain_subprocess;
static PyObject *const_str_digest_1b13362816a945fa6909c601273ecebe;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_stderr;
extern PyObject *const_str_plain__winapi;
extern PyObject *const_str_plain_JPY_INTERRUPT_EVENT;
extern PyObject *const_str_plain_endswith;
extern PyObject *const_str_plain_error;
extern PyObject *const_str_plain_setsid;
static PyObject *const_tuple_ad501a282a33f90cf80735a7da6e296f_tuple;
extern PyObject *const_str_plain_creationflags;
extern PyObject *const_str_plain_start_new_session;
extern PyObject *const_str_plain_win_interrupt;
extern PyObject *const_str_plain_create_interrupt_event;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_cast_bytes_py2;
extern PyObject *const_str_plain___all__;
static PyObject *const_tuple_str_plain_creationflags_int_0_tuple;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_plain_DuplicateHandle;
extern PyObject *const_int_0;
extern PyObject *const_tuple_str_plain_Popen_str_plain_PIPE_tuple;
static PyObject *const_str_plain_GetCurrentProcess;
static PyObject *const_str_digest_0a731563c866e1ddde4a5434153745d1;
extern PyObject *const_int_pos_134217728;
static PyObject *const_tuple_str_plain_cast_bytes_py2_str_plain_PY3_tuple;
static PyObject *const_str_digest_00f6f3f4777f3f842cb3b18df7dd2c0a;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_close_fds;
static PyObject *const_tuple_str_plain_c_str_plain_encoding_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_c;
extern PyObject *const_tuple_str_digest_50bd9189c156b5539c462ddc680b0a93_tuple;
extern PyObject *const_str_plain_setdefault;
extern PyObject *const_str_plain_PATH;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_IPY_INTERRUPT_EVENT;
extern PyObject *const_str_plain_cwd;
extern PyObject *const_str_plain__subprocess;
static PyObject *const_dict_f62dbb2a58f7202f3efb8c5e503eb4ac;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_update;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_digest_34e3966f9679fde95e40f5931481245c;
extern PyObject *const_str_plain_copy;
static PyObject *const_tuple_none_none_none_none_false_none_tuple;
extern PyObject *const_str_plain_key;
static PyObject *const_str_plain_DUPLICATE_SAME_ACCESS;
static PyObject *const_str_digest_0f6510cfa46e9522531fe88c37c493ee;
extern PyObject *const_str_plain_launch_kernel;
extern PyObject *const_str_plain_stdin;
extern PyObject *const_tuple_str_plain_get_logger_tuple;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_ascii;
extern PyObject *const_str_plain_getpid;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_executable;
extern PyObject *const_str_plain_win32;
extern PyObject *const_str_plain_defpath;
static PyObject *const_list_str_plain_launch_kernel_list;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_3a446f0691307d3156e6a88d64741e5b = UNSTREAM_STRING_ASCII( &constant_bin[ 1201406 ], 31, 0 );
    const_str_plain_preexec_fn = UNSTREAM_STRING_ASCII( &constant_bin[ 1201437 ], 10, 1 );
    const_str_digest_77577b44b8280b183dc130f9110d86aa = UNSTREAM_STRING_ASCII( &constant_bin[ 1201447 ], 62, 0 );
    const_tuple_str_plain_create_interrupt_event_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_create_interrupt_event_tuple, 0, const_str_plain_create_interrupt_event ); Py_INCREF( const_str_plain_create_interrupt_event );
    const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple = PyTuple_New( 4 );
    const_str_plain_DuplicateHandle = UNSTREAM_STRING_ASCII( &constant_bin[ 1201509 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple, 0, const_str_plain_DuplicateHandle ); Py_INCREF( const_str_plain_DuplicateHandle );
    const_str_plain_GetCurrentProcess = UNSTREAM_STRING_ASCII( &constant_bin[ 1201524 ], 17, 1 );
    PyTuple_SET_ITEM( const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple, 1, const_str_plain_GetCurrentProcess ); Py_INCREF( const_str_plain_GetCurrentProcess );
    const_str_plain_DUPLICATE_SAME_ACCESS = UNSTREAM_STRING_ASCII( &constant_bin[ 1201541 ], 21, 1 );
    PyTuple_SET_ITEM( const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple, 2, const_str_plain_DUPLICATE_SAME_ACCESS ); Py_INCREF( const_str_plain_DUPLICATE_SAME_ACCESS );
    const_str_plain_CREATE_NEW_PROCESS_GROUP = UNSTREAM_STRING_ASCII( &constant_bin[ 1201562 ], 24, 1 );
    PyTuple_SET_ITEM( const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple, 3, const_str_plain_CREATE_NEW_PROCESS_GROUP ); Py_INCREF( const_str_plain_CREATE_NEW_PROCESS_GROUP );
    const_tuple_str_plain_getdefaultencoding_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_getdefaultencoding_tuple, 0, const_str_plain_getdefaultencoding ); Py_INCREF( const_str_plain_getdefaultencoding );
    const_str_digest_c15ef88fea42b3f34f0d1835bf5a8659 = UNSTREAM_STRING_ASCII( &constant_bin[ 1201586 ], 26, 0 );
    const_str_digest_1b13362816a945fa6909c601273ecebe = UNSTREAM_STRING_ASCII( &constant_bin[ 1201612 ], 31, 0 );
    const_tuple_ad501a282a33f90cf80735a7da6e296f_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1201643 ], 318 );
    const_tuple_str_plain_creationflags_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_creationflags_int_0_tuple, 0, const_str_plain_creationflags ); Py_INCREF( const_str_plain_creationflags );
    PyTuple_SET_ITEM( const_tuple_str_plain_creationflags_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_digest_0a731563c866e1ddde4a5434153745d1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1201961 ], 957, 0 );
    const_tuple_str_plain_cast_bytes_py2_str_plain_PY3_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cast_bytes_py2_str_plain_PY3_tuple, 0, const_str_plain_cast_bytes_py2 ); Py_INCREF( const_str_plain_cast_bytes_py2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cast_bytes_py2_str_plain_PY3_tuple, 1, const_str_plain_PY3 ); Py_INCREF( const_str_plain_PY3 );
    const_str_digest_00f6f3f4777f3f842cb3b18df7dd2c0a = UNSTREAM_STRING_ASCII( &constant_bin[ 1202918 ], 23, 0 );
    const_tuple_str_plain_c_str_plain_encoding_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_str_plain_encoding_tuple, 0, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_str_plain_encoding_tuple, 1, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    const_str_plain_IPY_INTERRUPT_EVENT = UNSTREAM_STRING_ASCII( &constant_bin[ 1202941 ], 19, 1 );
    const_dict_f62dbb2a58f7202f3efb8c5e503eb4ac = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_f62dbb2a58f7202f3efb8c5e503eb4ac, const_str_plain_prefer_stream, Py_False );
    assert( PyDict_Size( const_dict_f62dbb2a58f7202f3efb8c5e503eb4ac ) == 1 );
    const_tuple_none_none_none_none_false_none_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_none_none_none_none_false_none_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_none_false_none_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_none_false_none_tuple, 2, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_none_false_none_tuple, 3, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_none_false_none_tuple, 4, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_none_none_none_none_false_none_tuple, 5, Py_None ); Py_INCREF( Py_None );
    const_str_digest_0f6510cfa46e9522531fe88c37c493ee = UNSTREAM_STRING_ASCII( &constant_bin[ 1202960 ], 32, 0 );
    const_list_str_plain_launch_kernel_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_launch_kernel_list, 0, const_str_plain_launch_kernel ); Py_INCREF( const_str_plain_launch_kernel );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jupyter_client$launcher( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_8310b94c08572ae416fe4b245784152b;
static PyCodeObject *codeobj_b499e081d695f48cbb04d9dee8fce877;
static PyCodeObject *codeobj_ba6fa4bdecdf2d6b5a2e1eec9750f9e0;
static PyCodeObject *codeobj_99361a8744f467847ed0d8e96c6979a3;
static PyCodeObject *codeobj_5bfe21b1ae42d205d913d52f1d9d5b74;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_c15ef88fea42b3f34f0d1835bf5a8659 );
    codeobj_8310b94c08572ae416fe4b245784152b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_dictcontraction, 147, const_tuple_str_plain_key_str_plain_value_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b499e081d695f48cbb04d9dee8fce877 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 133, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ba6fa4bdecdf2d6b5a2e1eec9750f9e0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 85, const_tuple_str_plain_c_str_plain_encoding_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_99361a8744f467847ed0d8e96c6979a3 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_0f6510cfa46e9522531fe88c37c493ee, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_5bfe21b1ae42d205d913d52f1d9d5b74 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_launch_kernel, 15, const_tuple_ad501a282a33f90cf80735a7da6e296f_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jupyter_client$launcher$$$function_1_launch_kernel( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$launcher$$$function_1_launch_kernel$$$function_1_lambda(  );


// The module function definitions.
static PyObject *impl_jupyter_client$launcher$$$function_1_launch_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cmd = python_pars[ 0 ];
    PyObject *par_stdin = python_pars[ 1 ];
    PyObject *par_stdout = python_pars[ 2 ];
    PyObject *par_stderr = python_pars[ 3 ];
    PyObject *par_env = python_pars[ 4 ];
    PyObject *par_independent = python_pars[ 5 ];
    PyObject *par_cwd = python_pars[ 6 ];
    PyObject *par_kw = python_pars[ 7 ];
    PyObject *var__stdin = NULL;
    PyObject *var_redirect_out = NULL;
    PyObject *var_blackhole = NULL;
    PyObject *var__stdout = NULL;
    PyObject *var__stderr = NULL;
    PyObject *var_encoding = NULL;
    PyObject *var_kwargs = NULL;
    PyObject *var_main_args = NULL;
    PyObject *var_create_interrupt_event = NULL;
    PyObject *var_interrupt_event = NULL;
    PyObject *var_DuplicateHandle = NULL;
    PyObject *var_GetCurrentProcess = NULL;
    PyObject *var_DUPLICATE_SAME_ACCESS = NULL;
    PyObject *var_CREATE_NEW_PROCESS_GROUP = NULL;
    PyObject *var_pid = NULL;
    PyObject *var_handle = NULL;
    PyObject *var_proc = NULL;
    PyObject *var_exc = NULL;
    PyObject *var_msg = NULL;
    PyObject *var_without_env = NULL;
    PyObject *outline_0_var_c = NULL;
    PyObject *outline_1_var_key = NULL;
    PyObject *outline_1_var_value = NULL;
    PyObject *tmp_dictcontraction$tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_dictcontraction$tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_dictcontraction$tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_dictcontraction_1__$0 = NULL;
    PyObject *tmp_dictcontraction_1__contraction = NULL;
    PyObject *tmp_dictcontraction_1__iter_value_0 = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_5bfe21b1ae42d205d913d52f1d9d5b74;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    struct Nuitka_FrameObject *frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    bool tmp_result;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    struct Nuitka_FrameObject *frame_8310b94c08572ae416fe4b245784152b_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    static struct Nuitka_FrameObject *cache_frame_8310b94c08572ae416fe4b245784152b_3 = NULL;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    static struct Nuitka_FrameObject *cache_frame_5bfe21b1ae42d205d913d52f1d9d5b74 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5bfe21b1ae42d205d913d52f1d9d5b74, codeobj_5bfe21b1ae42d205d913d52f1d9d5b74, module_jupyter_client$launcher, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5bfe21b1ae42d205d913d52f1d9d5b74 = cache_frame_5bfe21b1ae42d205d913d52f1d9d5b74;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5bfe21b1ae42d205d913d52f1d9d5b74 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5bfe21b1ae42d205d913d52f1d9d5b74 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_stdin );
        tmp_compexpr_left_1 = par_stdin;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_PIPE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PIPE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PIPE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_1 = tmp_mvar_value_1;
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( par_stdin );
        tmp_assign_source_1 = par_stdin;
        condexpr_end_1:;
        assert( var__stdin == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var__stdin = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_executable );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 61;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_digest_50bd9189c156b5539c462ddc680b0a93_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_redirect_out == NULL );
        var_redirect_out = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_redirect_out );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_redirect_out );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_open_filename_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_open_mode_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_os );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 63;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = tmp_mvar_value_3;
            tmp_open_filename_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_devnull );
            if ( tmp_open_filename_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_open_mode_1 = const_str_plain_w;
            tmp_assign_source_3 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
            Py_DECREF( tmp_open_filename_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_blackhole == NULL );
            var_blackhole = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_stdout );
            tmp_compexpr_left_2 = par_stdout;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            CHECK_OBJECT( var_blackhole );
            tmp_assign_source_4 = var_blackhole;
            goto condexpr_end_2;
            condexpr_false_2:;
            CHECK_OBJECT( par_stdout );
            tmp_assign_source_4 = par_stdout;
            condexpr_end_2:;
            assert( var__stdout == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var__stdout = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( par_stderr );
            tmp_compexpr_left_3 = par_stderr;
            tmp_compexpr_right_3 = Py_None;
            tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            CHECK_OBJECT( var_blackhole );
            tmp_assign_source_5 = var_blackhole;
            goto condexpr_end_3;
            condexpr_false_3:;
            CHECK_OBJECT( par_stderr );
            tmp_assign_source_5 = par_stderr;
            condexpr_end_3:;
            assert( var__stderr == NULL );
            Py_INCREF( tmp_assign_source_5 );
            var__stderr = tmp_assign_source_5;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( par_stdout );
            tmp_tuple_element_1 = par_stdout;
            tmp_iter_arg_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_iter_arg_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( par_stderr );
            tmp_tuple_element_1 = par_stderr;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_iter_arg_1, 1, tmp_tuple_element_1 );
            tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            assert( !(tmp_assign_source_6 == NULL) );
            assert( tmp_tuple_unpack_1__source_iter == NULL );
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooNoooooooooooooooooooo";
                exception_lineno = 67;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_1 == NULL );
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "ooooooooNoooooooooooooooooooo";
                exception_lineno = 67;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_2 == NULL );
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_9 = tmp_tuple_unpack_1__element_1;
            assert( var__stdout == NULL );
            Py_INCREF( tmp_assign_source_9 );
            var__stdout = tmp_assign_source_9;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_10 = tmp_tuple_unpack_1__element_2;
            assert( var__stderr == NULL );
            Py_INCREF( tmp_assign_source_10 );
            var__stderr = tmp_assign_source_10;
        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_11;
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_4;
        CHECK_OBJECT( par_env );
        tmp_compexpr_left_4 = par_env;
        tmp_compexpr_right_4 = Py_None;
        tmp_condition_result_5 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( par_env );
        tmp_assign_source_11 = par_env;
        Py_INCREF( tmp_assign_source_11 );
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_4;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_environ );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 69;
        tmp_assign_source_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_copy );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        condexpr_end_4:;
        {
            PyObject *old = par_env;
            assert( old != NULL );
            par_env = tmp_assign_source_11;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_getdefaultencoding );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_getdefaultencoding );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "getdefaultencoding" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_5;
        tmp_kw_name_1 = PyDict_Copy( const_dict_f62dbb2a58f7202f3efb8c5e503eb4ac );
        frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 71;
        tmp_assign_source_12 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_encoding == NULL );
        var_encoding = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( par_kw );
        tmp_called_instance_3 = par_kw;
        frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 72;
        tmp_assign_source_13 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_copy );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_kwargs == NULL );
        var_kwargs = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        tmp_dict_key_1 = const_str_plain_stdin;
        CHECK_OBJECT( var__stdin );
        tmp_dict_value_1 = var__stdin;
        tmp_assign_source_14 = _PyDict_NewPresized( 5 );
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_stdout;
        CHECK_OBJECT( var__stdout );
        tmp_dict_value_2 = var__stdout;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_stderr;
        CHECK_OBJECT( var__stderr );
        tmp_dict_value_3 = var__stderr;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_cwd;
        CHECK_OBJECT( par_cwd );
        tmp_dict_value_4 = par_cwd;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_env;
        CHECK_OBJECT( par_env );
        tmp_dict_value_5 = par_env;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        assert( var_main_args == NULL );
        var_main_args = tmp_assign_source_14;
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_kwargs );
        tmp_called_instance_4 = var_kwargs;
        CHECK_OBJECT( var_main_args );
        tmp_args_element_name_1 = var_main_args;
        frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 80;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_update, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 83;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_6;
        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_platform );
        if ( tmp_compexpr_left_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_5 = const_str_plain_win32;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        Py_DECREF( tmp_compexpr_left_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_15;
            // Tried code:
            {
                PyObject *tmp_assign_source_16;
                PyObject *tmp_iter_arg_2;
                CHECK_OBJECT( par_cmd );
                tmp_iter_arg_2 = par_cmd;
                tmp_assign_source_16 = MAKE_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_16 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 85;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto try_except_handler_4;
                }
                assert( tmp_listcomp_1__$0 == NULL );
                tmp_listcomp_1__$0 = tmp_assign_source_16;
            }
            {
                PyObject *tmp_assign_source_17;
                tmp_assign_source_17 = PyList_New( 0 );
                assert( tmp_listcomp_1__contraction == NULL );
                tmp_listcomp_1__contraction = tmp_assign_source_17;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2, codeobj_ba6fa4bdecdf2d6b5a2e1eec9750f9e0, module_jupyter_client$launcher, sizeof(void *)+sizeof(void *) );
            frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 = cache_frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_18;
                CHECK_OBJECT( tmp_listcomp_1__$0 );
                tmp_next_source_1 = tmp_listcomp_1__$0;
                tmp_assign_source_18 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_18 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "oo";
                        exception_lineno = 85;
                        goto try_except_handler_5;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_0;
                    tmp_listcomp_1__iter_value_0 = tmp_assign_source_18;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_19;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                tmp_assign_source_19 = tmp_listcomp_1__iter_value_0;
                {
                    PyObject *old = outline_0_var_c;
                    outline_0_var_c = tmp_assign_source_19;
                    Py_INCREF( outline_0_var_c );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_called_name_2;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_cast_bytes_py2 );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cast_bytes_py2 );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cast_bytes_py2" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 85;
                    type_description_2 = "oo";
                    goto try_except_handler_5;
                }

                tmp_called_name_2 = tmp_mvar_value_7;
                CHECK_OBJECT( outline_0_var_c );
                tmp_args_element_name_2 = outline_0_var_c;
                CHECK_OBJECT( var_encoding );
                tmp_args_element_name_3 = var_encoding;
                frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2->m_frame.f_lineno = 85;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
                }

                if ( tmp_append_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 85;
                    type_description_2 = "oo";
                    goto try_except_handler_5;
                }
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 85;
                    type_description_2 = "oo";
                    goto try_except_handler_5;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            goto loop_start_1;
            loop_end_1:;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_assign_source_15 = tmp_listcomp_1__contraction;
            Py_INCREF( tmp_assign_source_15 );
            goto try_return_handler_5;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
            return NULL;
            // Return handler code:
            try_return_handler_5:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            goto frame_return_exit_1;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_1:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_4;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2,
                type_description_2,
                outline_0_var_c,
                var_encoding
            );


            // Release cached frame.
            if ( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 == cache_frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 )
            {
                Py_DECREF( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 );
            }
            cache_frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 = NULL;

            assertFrameObject( frame_ba6fa4bdecdf2d6b5a2e1eec9750f9e0_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto try_except_handler_4;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
            return NULL;
            // Return handler code:
            try_return_handler_4:;
            Py_XDECREF( outline_0_var_c );
            outline_0_var_c = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_0_var_c );
            outline_0_var_c = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
            return NULL;
            outline_exception_1:;
            exception_lineno = 85;
            goto frame_exception_exit_1;
            outline_result_1:;
            {
                PyObject *old = par_cmd;
                assert( old != NULL );
                par_cmd = tmp_assign_source_15;
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_7;
            int tmp_truth_name_2;
            CHECK_OBJECT( par_cwd );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_cwd );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_20;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                int tmp_or_left_truth_1;
                PyObject *tmp_or_left_value_1;
                PyObject *tmp_or_right_value_1;
                PyObject *tmp_called_instance_5;
                PyObject *tmp_mvar_value_9;
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_cast_bytes_py2 );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cast_bytes_py2 );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cast_bytes_py2" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 87;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_3 = tmp_mvar_value_8;
                CHECK_OBJECT( par_cwd );
                tmp_args_element_name_4 = par_cwd;
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_sys );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 87;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_5 = tmp_mvar_value_9;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 87;
                tmp_or_left_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_getfilesystemencoding );
                if ( tmp_or_left_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 87;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
                if ( tmp_or_left_truth_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_or_left_value_1 );

                    exception_lineno = 87;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_1 == 1 )
                {
                    goto or_left_1;
                }
                else
                {
                    goto or_right_1;
                }
                or_right_1:;
                Py_DECREF( tmp_or_left_value_1 );
                tmp_or_right_value_1 = const_str_plain_ascii;
                Py_INCREF( tmp_or_right_value_1 );
                tmp_args_element_name_5 = tmp_or_right_value_1;
                goto or_end_1;
                or_left_1:;
                tmp_args_element_name_5 = tmp_or_left_value_1;
                or_end_1:;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 87;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                    tmp_assign_source_20 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_args_element_name_5 );
                if ( tmp_assign_source_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 87;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_cwd;
                    assert( old != NULL );
                    par_cwd = tmp_assign_source_20;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_ass_subscript_1;
                CHECK_OBJECT( par_cwd );
                tmp_ass_subvalue_1 = par_cwd;
                CHECK_OBJECT( var_kwargs );
                tmp_ass_subscribed_1 = var_kwargs;
                tmp_ass_subscript_1 = const_str_plain_cwd;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 88;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_3:;
        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_plain_win_interrupt;
            tmp_globals_name_1 = (PyObject *)moduledict_jupyter_client$launcher;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain_create_interrupt_event_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 90;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_21 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_jupyter_client$launcher,
                    const_str_plain_create_interrupt_event,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_create_interrupt_event );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_create_interrupt_event == NULL );
            var_create_interrupt_event = tmp_assign_source_21;
        }
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_called_name_4;
            CHECK_OBJECT( var_create_interrupt_event );
            tmp_called_name_4 = var_create_interrupt_event;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 93;
            tmp_assign_source_22 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_interrupt_event == NULL );
            var_interrupt_event = tmp_assign_source_22;
        }
        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_ass_subscript_2;
            CHECK_OBJECT( var_interrupt_event );
            tmp_unicode_arg_1 = var_interrupt_event;
            tmp_ass_subvalue_2 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_ass_subvalue_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_env );
            tmp_ass_subscribed_2 = par_env;
            tmp_ass_subscript_2 = const_str_plain_JPY_INTERRUPT_EVENT;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            Py_DECREF( tmp_ass_subvalue_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_ass_subvalue_3;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_ass_subscribed_3;
            PyObject *tmp_ass_subscript_3;
            CHECK_OBJECT( par_env );
            tmp_subscribed_name_1 = par_env;
            tmp_subscript_name_1 = const_str_plain_JPY_INTERRUPT_EVENT;
            tmp_ass_subvalue_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_ass_subvalue_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 96;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_env );
            tmp_ass_subscribed_3 = par_env;
            tmp_ass_subscript_3 = const_str_plain_IPY_INTERRUPT_EVENT;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
            Py_DECREF( tmp_ass_subvalue_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 96;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_name_name_2;
            PyObject *tmp_globals_name_2;
            PyObject *tmp_locals_name_2;
            PyObject *tmp_fromlist_name_2;
            PyObject *tmp_level_name_2;
            tmp_name_name_2 = const_str_plain__winapi;
            tmp_globals_name_2 = (PyObject *)moduledict_jupyter_client$launcher;
            tmp_locals_name_2 = Py_None;
            tmp_fromlist_name_2 = const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple;
            tmp_level_name_2 = const_int_0;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 99;
            tmp_assign_source_23 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_6;
            }
            assert( tmp_import_from_1__module == NULL );
            tmp_import_from_1__module = tmp_assign_source_23;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_import_name_from_2;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_2 = tmp_import_from_1__module;
            tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_DuplicateHandle );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_7;
            }
            assert( var_DuplicateHandle == NULL );
            var_DuplicateHandle = tmp_assign_source_24;
        }
        {
            PyObject *tmp_assign_source_25;
            PyObject *tmp_import_name_from_3;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_3 = tmp_import_from_1__module;
            tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_GetCurrentProcess );
            if ( tmp_assign_source_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_7;
            }
            assert( var_GetCurrentProcess == NULL );
            var_GetCurrentProcess = tmp_assign_source_25;
        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_import_name_from_4;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_4 = tmp_import_from_1__module;
            tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_DUPLICATE_SAME_ACCESS );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_7;
            }
            assert( var_DUPLICATE_SAME_ACCESS == NULL );
            var_DUPLICATE_SAME_ACCESS = tmp_assign_source_26;
        }
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_import_name_from_5;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_5 = tmp_import_from_1__module;
            tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_CREATE_NEW_PROCESS_GROUP );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_7;
            }
            assert( var_CREATE_NEW_PROCESS_GROUP == NULL );
            var_CREATE_NEW_PROCESS_GROUP = tmp_assign_source_27;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        try_end_3:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        goto try_end_4;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_6 == NULL )
        {
            exception_keeper_tb_6 = MAKE_TRACEBACK( frame_5bfe21b1ae42d205d913d52f1d9d5b74, exception_keeper_lineno_6 );
        }
        else if ( exception_keeper_lineno_6 != 0 )
        {
            exception_keeper_tb_6 = ADD_TRACEBACK( exception_keeper_tb_6, frame_5bfe21b1ae42d205d913d52f1d9d5b74, exception_keeper_lineno_6 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
        PyException_SetTraceback( exception_keeper_value_6, (PyObject *)exception_keeper_tb_6 );
        PUBLISH_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
        // Tried code:
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_name_name_3;
            PyObject *tmp_globals_name_3;
            PyObject *tmp_locals_name_3;
            PyObject *tmp_fromlist_name_3;
            PyObject *tmp_level_name_3;
            tmp_name_name_3 = const_str_plain__subprocess;
            tmp_globals_name_3 = (PyObject *)moduledict_jupyter_client$launcher;
            tmp_locals_name_3 = Py_None;
            tmp_fromlist_name_3 = const_tuple_9be3f7e643d2c094d64f9107f17cd83c_tuple;
            tmp_level_name_3 = const_int_0;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 102;
            tmp_assign_source_28 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_8;
            }
            assert( tmp_import_from_2__module == NULL );
            tmp_import_from_2__module = tmp_assign_source_28;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_import_name_from_6;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_6 = tmp_import_from_2__module;
            tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_DuplicateHandle );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_9;
            }
            {
                PyObject *old = var_DuplicateHandle;
                var_DuplicateHandle = tmp_assign_source_29;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_import_name_from_7;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_7 = tmp_import_from_2__module;
            tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_GetCurrentProcess );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_9;
            }
            {
                PyObject *old = var_GetCurrentProcess;
                var_GetCurrentProcess = tmp_assign_source_30;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_import_name_from_8;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_8 = tmp_import_from_2__module;
            tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_DUPLICATE_SAME_ACCESS );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_9;
            }
            {
                PyObject *old = var_DUPLICATE_SAME_ACCESS;
                var_DUPLICATE_SAME_ACCESS = tmp_assign_source_31;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_import_name_from_9;
            CHECK_OBJECT( tmp_import_from_2__module );
            tmp_import_name_from_9 = tmp_import_from_2__module;
            tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_CREATE_NEW_PROCESS_GROUP );
            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_9;
            }
            assert( var_CREATE_NEW_PROCESS_GROUP == NULL );
            var_CREATE_NEW_PROCESS_GROUP = tmp_assign_source_32;
        }
        goto try_end_5;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
        Py_DECREF( tmp_import_from_2__module );
        tmp_import_from_2__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto try_except_handler_8;
        // End of try:
        try_end_5:;
        goto try_end_6;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto frame_exception_exit_1;
        // End of try:
        try_end_6:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
        Py_DECREF( tmp_import_from_2__module );
        tmp_import_from_2__module = NULL;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_4;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
        return NULL;
        // End of try:
        try_end_4:;
        {
            nuitka_bool tmp_condition_result_8;
            int tmp_truth_name_3;
            CHECK_OBJECT( par_independent );
            tmp_truth_name_3 = CHECK_IF_TRUE( par_independent );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_8 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_ass_subvalue_4;
                PyObject *tmp_ass_subscribed_4;
                PyObject *tmp_ass_subscript_4;
                CHECK_OBJECT( var_CREATE_NEW_PROCESS_GROUP );
                tmp_ass_subvalue_4 = var_CREATE_NEW_PROCESS_GROUP;
                CHECK_OBJECT( var_kwargs );
                tmp_ass_subscribed_4 = var_kwargs;
                tmp_ass_subscript_4 = const_str_plain_creationflags;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_4, tmp_ass_subscript_4, tmp_ass_subvalue_4 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 107;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_4;
            branch_no_4:;
            {
                PyObject *tmp_assign_source_33;
                PyObject *tmp_called_name_5;
                CHECK_OBJECT( var_GetCurrentProcess );
                tmp_called_name_5 = var_GetCurrentProcess;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 109;
                tmp_assign_source_33 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
                if ( tmp_assign_source_33 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_pid == NULL );
                var_pid = tmp_assign_source_33;
            }
            {
                PyObject *tmp_assign_source_34;
                PyObject *tmp_called_name_6;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_args_element_name_7;
                PyObject *tmp_args_element_name_8;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_args_element_name_10;
                PyObject *tmp_args_element_name_11;
                CHECK_OBJECT( var_DuplicateHandle );
                tmp_called_name_6 = var_DuplicateHandle;
                CHECK_OBJECT( var_pid );
                tmp_args_element_name_6 = var_pid;
                CHECK_OBJECT( var_pid );
                tmp_args_element_name_7 = var_pid;
                CHECK_OBJECT( var_pid );
                tmp_args_element_name_8 = var_pid;
                tmp_args_element_name_9 = const_int_0;
                tmp_args_element_name_10 = Py_True;
                CHECK_OBJECT( var_DUPLICATE_SAME_ACCESS );
                tmp_args_element_name_11 = var_DUPLICATE_SAME_ACCESS;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 110;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11 };
                    tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_6, call_args );
                }

                if ( tmp_assign_source_34 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 110;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_handle == NULL );
                var_handle = tmp_assign_source_34;
            }
            {
                PyObject *tmp_ass_subvalue_5;
                PyObject *tmp_unicode_arg_2;
                PyObject *tmp_int_arg_1;
                PyObject *tmp_ass_subscribed_5;
                PyObject *tmp_ass_subscript_5;
                CHECK_OBJECT( var_handle );
                tmp_int_arg_1 = var_handle;
                tmp_unicode_arg_2 = PyNumber_Int( tmp_int_arg_1 );
                if ( tmp_unicode_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 113;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_ass_subvalue_5 = PyObject_Unicode( tmp_unicode_arg_2 );
                Py_DECREF( tmp_unicode_arg_2 );
                if ( tmp_ass_subvalue_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 113;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_env );
                tmp_ass_subscribed_5 = par_env;
                tmp_ass_subscript_5 = const_str_plain_JPY_PARENT_PID;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_5, tmp_ass_subscript_5, tmp_ass_subvalue_5 );
                Py_DECREF( tmp_ass_subvalue_5 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 113;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_end_4:;
        }
        {
            nuitka_bool tmp_condition_result_9;
            int tmp_truth_name_4;
            CHECK_OBJECT( var_redirect_out );
            tmp_truth_name_4 = CHECK_IF_TRUE( var_redirect_out );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_9 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_ass_subvalue_6;
                PyObject *tmp_left_name_1;
                PyObject *tmp_called_instance_6;
                PyObject *tmp_right_name_1;
                PyObject *tmp_ass_subscribed_6;
                PyObject *tmp_ass_subscript_6;
                CHECK_OBJECT( var_kwargs );
                tmp_called_instance_6 = var_kwargs;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 117;
                tmp_left_name_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain_setdefault, &PyTuple_GET_ITEM( const_tuple_str_plain_creationflags_int_0_tuple, 0 ) );

                if ( tmp_left_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_1 = const_int_pos_134217728;
                tmp_ass_subvalue_6 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_left_name_1 );
                if ( tmp_ass_subvalue_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_kwargs );
                tmp_ass_subscribed_6 = var_kwargs;
                tmp_ass_subscript_6 = const_str_plain_creationflags;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_6, tmp_ass_subscript_6, tmp_ass_subvalue_6 );
                Py_DECREF( tmp_ass_subvalue_6 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_5:;
        }
        {
            PyObject *tmp_ass_subvalue_7;
            PyObject *tmp_ass_subscribed_7;
            PyObject *tmp_ass_subscript_7;
            tmp_ass_subvalue_7 = Py_False;
            CHECK_OBJECT( var_kwargs );
            tmp_ass_subscribed_7 = var_kwargs;
            tmp_ass_subscript_7 = const_str_plain_close_fds;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_7, tmp_ass_subscript_7, tmp_ass_subvalue_7 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_mvar_value_10;
            int tmp_truth_name_5;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_PY3 );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_truth_name_5 = CHECK_IF_TRUE( tmp_mvar_value_10 );
            if ( tmp_truth_name_5 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_10 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_ass_subvalue_8;
                PyObject *tmp_ass_subscribed_8;
                PyObject *tmp_ass_subscript_8;
                tmp_ass_subvalue_8 = Py_True;
                CHECK_OBJECT( var_kwargs );
                tmp_ass_subscribed_8 = var_kwargs;
                tmp_ass_subscript_8 = const_str_plain_start_new_session;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_8, tmp_ass_subscript_8, tmp_ass_subvalue_8 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 131;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_6;
            branch_no_6:;
            {
                PyObject *tmp_ass_subvalue_9;
                PyObject *tmp_ass_subscribed_9;
                PyObject *tmp_ass_subscript_9;
                tmp_ass_subvalue_9 = MAKE_FUNCTION_jupyter_client$launcher$$$function_1_launch_kernel$$$function_1_lambda(  );



                CHECK_OBJECT( var_kwargs );
                tmp_ass_subscribed_9 = var_kwargs;
                tmp_ass_subscript_9 = const_str_plain_preexec_fn;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_9, tmp_ass_subscript_9, tmp_ass_subvalue_9 );
                Py_DECREF( tmp_ass_subvalue_9 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 133;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_end_6:;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_1;
            CHECK_OBJECT( par_independent );
            tmp_operand_name_1 = par_independent;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_ass_subvalue_10;
                PyObject *tmp_unicode_arg_3;
                PyObject *tmp_called_instance_7;
                PyObject *tmp_mvar_value_11;
                PyObject *tmp_ass_subscribed_10;
                PyObject *tmp_ass_subscript_10;
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_os );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 135;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_7 = tmp_mvar_value_11;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 135;
                tmp_unicode_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_getpid );
                if ( tmp_unicode_arg_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_ass_subvalue_10 = PyObject_Unicode( tmp_unicode_arg_3 );
                Py_DECREF( tmp_unicode_arg_3 );
                if ( tmp_ass_subvalue_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_env );
                tmp_ass_subscribed_10 = par_env;
                tmp_ass_subscript_10 = const_str_plain_JPY_PARENT_PID;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_10, tmp_ass_subscript_10, tmp_ass_subvalue_10 );
                Py_DECREF( tmp_ass_subvalue_10 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_7:;
        }
        branch_end_2:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_dircall_arg3_1;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_Popen );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Popen );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Popen" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto try_except_handler_10;
        }

        tmp_dircall_arg1_1 = tmp_mvar_value_12;
        CHECK_OBJECT( par_cmd );
        tmp_tuple_element_2 = par_cmd;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var_kwargs );
        tmp_dircall_arg3_1 = var_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_35 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto try_except_handler_10;
        }
        assert( var_proc == NULL );
        var_proc = tmp_assign_source_35;
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_9 == NULL )
    {
        exception_keeper_tb_9 = MAKE_TRACEBACK( frame_5bfe21b1ae42d205d913d52f1d9d5b74, exception_keeper_lineno_9 );
    }
    else if ( exception_keeper_lineno_9 != 0 )
    {
        exception_keeper_tb_9 = ADD_TRACEBACK( exception_keeper_tb_9, frame_5bfe21b1ae42d205d913d52f1d9d5b74, exception_keeper_lineno_9 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
    PyException_SetTraceback( exception_keeper_value_9, (PyObject *)exception_keeper_tb_9 );
    PUBLISH_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        tmp_compexpr_left_6 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_6 = PyExc_Exception;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto try_except_handler_11;
        }
        tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = EXC_VALUE(PyThreadState_GET());
            assert( var_exc == NULL );
            Py_INCREF( tmp_assign_source_36 );
            var_exc = tmp_assign_source_36;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_37;
            // Tried code:
            {
                PyObject *tmp_assign_source_38;
                PyObject *tmp_iter_arg_3;
                PyObject *tmp_called_instance_8;
                CHECK_OBJECT( var_kwargs );
                tmp_called_instance_8 = var_kwargs;
                frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 147;
                tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_items );
                if ( tmp_iter_arg_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto try_except_handler_13;
                }
                tmp_assign_source_38 = MAKE_ITERATOR( tmp_iter_arg_3 );
                Py_DECREF( tmp_iter_arg_3 );
                if ( tmp_assign_source_38 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_1 = "ooooooooNoooooooooooooooooooo";
                    goto try_except_handler_13;
                }
                assert( tmp_dictcontraction_1__$0 == NULL );
                tmp_dictcontraction_1__$0 = tmp_assign_source_38;
            }
            {
                PyObject *tmp_assign_source_39;
                tmp_assign_source_39 = PyDict_New();
                assert( tmp_dictcontraction_1__contraction == NULL );
                tmp_dictcontraction_1__contraction = tmp_assign_source_39;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_8310b94c08572ae416fe4b245784152b_3, codeobj_8310b94c08572ae416fe4b245784152b, module_jupyter_client$launcher, sizeof(void *)+sizeof(void *) );
            frame_8310b94c08572ae416fe4b245784152b_3 = cache_frame_8310b94c08572ae416fe4b245784152b_3;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_8310b94c08572ae416fe4b245784152b_3 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_8310b94c08572ae416fe4b245784152b_3 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_40;
                CHECK_OBJECT( tmp_dictcontraction_1__$0 );
                tmp_next_source_2 = tmp_dictcontraction_1__$0;
                tmp_assign_source_40 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_40 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "oo";
                        exception_lineno = 147;
                        goto try_except_handler_14;
                    }
                }

                {
                    PyObject *old = tmp_dictcontraction_1__iter_value_0;
                    tmp_dictcontraction_1__iter_value_0 = tmp_assign_source_40;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_41;
                PyObject *tmp_iter_arg_4;
                CHECK_OBJECT( tmp_dictcontraction_1__iter_value_0 );
                tmp_iter_arg_4 = tmp_dictcontraction_1__iter_value_0;
                tmp_assign_source_41 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
                if ( tmp_assign_source_41 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_2 = "oo";
                    goto try_except_handler_15;
                }
                {
                    PyObject *old = tmp_dictcontraction$tuple_unpack_1__source_iter;
                    tmp_dictcontraction$tuple_unpack_1__source_iter = tmp_assign_source_41;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_42;
                PyObject *tmp_unpack_3;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__source_iter );
                tmp_unpack_3 = tmp_dictcontraction$tuple_unpack_1__source_iter;
                tmp_assign_source_42 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                if ( tmp_assign_source_42 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_2 = "oo";
                    exception_lineno = 147;
                    goto try_except_handler_16;
                }
                {
                    PyObject *old = tmp_dictcontraction$tuple_unpack_1__element_1;
                    tmp_dictcontraction$tuple_unpack_1__element_1 = tmp_assign_source_42;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_43;
                PyObject *tmp_unpack_4;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__source_iter );
                tmp_unpack_4 = tmp_dictcontraction$tuple_unpack_1__source_iter;
                tmp_assign_source_43 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                if ( tmp_assign_source_43 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_2 = "oo";
                    exception_lineno = 147;
                    goto try_except_handler_16;
                }
                {
                    PyObject *old = tmp_dictcontraction$tuple_unpack_1__element_2;
                    tmp_dictcontraction$tuple_unpack_1__element_2 = tmp_assign_source_43;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_1;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__source_iter );
                tmp_iterator_name_1 = tmp_dictcontraction$tuple_unpack_1__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_2 = "oo";
                            exception_lineno = 147;
                            goto try_except_handler_16;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_2 = "oo";
                    exception_lineno = 147;
                    goto try_except_handler_16;
                }
            }
            goto try_end_8;
            // Exception handler code:
            try_except_handler_16:;
            exception_keeper_type_10 = exception_type;
            exception_keeper_value_10 = exception_value;
            exception_keeper_tb_10 = exception_tb;
            exception_keeper_lineno_10 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction$tuple_unpack_1__source_iter );
            Py_DECREF( tmp_dictcontraction$tuple_unpack_1__source_iter );
            tmp_dictcontraction$tuple_unpack_1__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_10;
            exception_value = exception_keeper_value_10;
            exception_tb = exception_keeper_tb_10;
            exception_lineno = exception_keeper_lineno_10;

            goto try_except_handler_15;
            // End of try:
            try_end_8:;
            goto try_end_9;
            // Exception handler code:
            try_except_handler_15:;
            exception_keeper_type_11 = exception_type;
            exception_keeper_value_11 = exception_value;
            exception_keeper_tb_11 = exception_tb;
            exception_keeper_lineno_11 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_1 );
            tmp_dictcontraction$tuple_unpack_1__element_1 = NULL;

            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_2 );
            tmp_dictcontraction$tuple_unpack_1__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_11;
            exception_value = exception_keeper_value_11;
            exception_tb = exception_keeper_tb_11;
            exception_lineno = exception_keeper_lineno_11;

            goto try_except_handler_14;
            // End of try:
            try_end_9:;
            CHECK_OBJECT( (PyObject *)tmp_dictcontraction$tuple_unpack_1__source_iter );
            Py_DECREF( tmp_dictcontraction$tuple_unpack_1__source_iter );
            tmp_dictcontraction$tuple_unpack_1__source_iter = NULL;

            {
                PyObject *tmp_assign_source_44;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__element_1 );
                tmp_assign_source_44 = tmp_dictcontraction$tuple_unpack_1__element_1;
                {
                    PyObject *old = outline_1_var_key;
                    outline_1_var_key = tmp_assign_source_44;
                    Py_INCREF( outline_1_var_key );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_1 );
            tmp_dictcontraction$tuple_unpack_1__element_1 = NULL;

            {
                PyObject *tmp_assign_source_45;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__element_2 );
                tmp_assign_source_45 = tmp_dictcontraction$tuple_unpack_1__element_2;
                {
                    PyObject *old = outline_1_var_value;
                    outline_1_var_value = tmp_assign_source_45;
                    Py_INCREF( outline_1_var_value );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_2 );
            tmp_dictcontraction$tuple_unpack_1__element_2 = NULL;

            {
                nuitka_bool tmp_condition_result_13;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                CHECK_OBJECT( outline_1_var_key );
                tmp_compexpr_left_7 = outline_1_var_key;
                tmp_compexpr_right_7 = const_str_plain_env;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_2 = "oo";
                    goto try_except_handler_14;
                }
                tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                CHECK_OBJECT( outline_1_var_value );
                tmp_dictset_value = outline_1_var_value;
                CHECK_OBJECT( tmp_dictcontraction_1__contraction );
                tmp_dictset_dict = tmp_dictcontraction_1__contraction;
                CHECK_OBJECT( outline_1_var_key );
                tmp_dictset_key = outline_1_var_key;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_2 = "oo";
                    goto try_except_handler_14;
                }
                branch_no_9:;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 147;
                type_description_2 = "oo";
                goto try_except_handler_14;
            }
            goto loop_start_2;
            loop_end_2:;
            CHECK_OBJECT( tmp_dictcontraction_1__contraction );
            tmp_assign_source_37 = tmp_dictcontraction_1__contraction;
            Py_INCREF( tmp_assign_source_37 );
            goto try_return_handler_14;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
            return NULL;
            // Return handler code:
            try_return_handler_14:;
            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__$0 );
            Py_DECREF( tmp_dictcontraction_1__$0 );
            tmp_dictcontraction_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__contraction );
            Py_DECREF( tmp_dictcontraction_1__contraction );
            tmp_dictcontraction_1__contraction = NULL;

            Py_XDECREF( tmp_dictcontraction_1__iter_value_0 );
            tmp_dictcontraction_1__iter_value_0 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_14:;
            exception_keeper_type_12 = exception_type;
            exception_keeper_value_12 = exception_value;
            exception_keeper_tb_12 = exception_tb;
            exception_keeper_lineno_12 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__$0 );
            Py_DECREF( tmp_dictcontraction_1__$0 );
            tmp_dictcontraction_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__contraction );
            Py_DECREF( tmp_dictcontraction_1__contraction );
            tmp_dictcontraction_1__contraction = NULL;

            Py_XDECREF( tmp_dictcontraction_1__iter_value_0 );
            tmp_dictcontraction_1__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_12;
            exception_value = exception_keeper_value_12;
            exception_tb = exception_keeper_tb_12;
            exception_lineno = exception_keeper_lineno_12;

            goto frame_exception_exit_3;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_8310b94c08572ae416fe4b245784152b_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_2;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_8310b94c08572ae416fe4b245784152b_3 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_13;

            frame_exception_exit_3:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_8310b94c08572ae416fe4b245784152b_3 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_8310b94c08572ae416fe4b245784152b_3, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_8310b94c08572ae416fe4b245784152b_3->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_8310b94c08572ae416fe4b245784152b_3, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_8310b94c08572ae416fe4b245784152b_3,
                type_description_2,
                outline_1_var_key,
                outline_1_var_value
            );


            // Release cached frame.
            if ( frame_8310b94c08572ae416fe4b245784152b_3 == cache_frame_8310b94c08572ae416fe4b245784152b_3 )
            {
                Py_DECREF( frame_8310b94c08572ae416fe4b245784152b_3 );
            }
            cache_frame_8310b94c08572ae416fe4b245784152b_3 = NULL;

            assertFrameObject( frame_8310b94c08572ae416fe4b245784152b_3 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_2;

            frame_no_exception_2:;
            goto skip_nested_handling_2;
            nested_frame_exit_2:;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto try_except_handler_13;
            skip_nested_handling_2:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
            return NULL;
            // Return handler code:
            try_return_handler_13:;
            Py_XDECREF( outline_1_var_key );
            outline_1_var_key = NULL;

            Py_XDECREF( outline_1_var_value );
            outline_1_var_value = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_13:;
            exception_keeper_type_13 = exception_type;
            exception_keeper_value_13 = exception_value;
            exception_keeper_tb_13 = exception_tb;
            exception_keeper_lineno_13 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_1_var_key );
            outline_1_var_key = NULL;

            Py_XDECREF( outline_1_var_value );
            outline_1_var_value = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_13;
            exception_value = exception_keeper_value_13;
            exception_tb = exception_keeper_tb_13;
            exception_lineno = exception_keeper_lineno_13;

            goto outline_exception_2;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
            return NULL;
            outline_exception_2:;
            exception_lineno = 147;
            goto try_except_handler_12;
            outline_result_2:;
            assert( var_without_env == NULL );
            var_without_env = tmp_assign_source_37;
        }
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_args_element_name_13;
            PyObject *tmp_called_name_8;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_element_name_14;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_args_element_name_16;
            tmp_source_name_5 = const_str_digest_77577b44b8280b183dc130f9110d86aa;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_format );
            assert( !(tmp_called_name_7 == NULL) );
            CHECK_OBJECT( par_cmd );
            tmp_args_element_name_12 = par_cmd;
            CHECK_OBJECT( par_env );
            tmp_source_name_6 = par_env;
            tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_get );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_7 );

                exception_lineno = 148;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }
            tmp_args_element_name_14 = const_str_plain_PATH;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_os );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
            }

            if ( tmp_mvar_value_13 == NULL )
            {
                Py_DECREF( tmp_called_name_7 );
                Py_DECREF( tmp_called_name_8 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 148;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_source_name_7 = tmp_mvar_value_13;
            tmp_args_element_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_defpath );
            if ( tmp_args_element_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_7 );
                Py_DECREF( tmp_called_name_8 );

                exception_lineno = 148;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_14, tmp_args_element_name_15 };
                tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_args_element_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_7 );

                exception_lineno = 148;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }
            CHECK_OBJECT( var_without_env );
            tmp_args_element_name_16 = var_without_env;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_16 };
                tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_element_name_13 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }
            assert( var_msg == NULL );
            var_msg = tmp_assign_source_46;
        }
        {
            PyObject *tmp_called_instance_9;
            PyObject *tmp_called_name_9;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_17;
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_get_logger );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_logger );
            }

            if ( tmp_mvar_value_14 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_logger" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 149;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }

            tmp_called_name_9 = tmp_mvar_value_14;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 149;
            tmp_called_instance_9 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
            if ( tmp_called_instance_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }
            CHECK_OBJECT( var_msg );
            tmp_args_element_name_17 = var_msg;
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 149;
            {
                PyObject *call_args[] = { tmp_args_element_name_17 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_error, call_args );
            }

            Py_DECREF( tmp_called_instance_9 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto try_except_handler_12;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 150;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame) frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooooNoooooooooooooooooooo";
        goto try_except_handler_12;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
        return NULL;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_exc );
        var_exc = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto try_except_handler_11;
        // End of try:
        goto branch_end_8;
        branch_no_8:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 137;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame) frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooooooNoooooooooooooooooooo";
        goto try_except_handler_11;
        branch_end_8:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
    return NULL;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_7:;
    {
        nuitka_bool tmp_condition_result_14;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 152;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_15;
        tmp_compexpr_left_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_platform );
        if ( tmp_compexpr_left_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_8 = const_str_plain_win32;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
        Py_DECREF( tmp_compexpr_left_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooooooooNoooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            if ( var_interrupt_event == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "interrupt_event" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 154;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_assattr_name_1 = var_interrupt_event;
            CHECK_OBJECT( var_proc );
            tmp_assattr_target_1 = var_proc;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_win32_interrupt_event, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_10:;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_compexpr_left_9;
        PyObject *tmp_compexpr_right_9;
        CHECK_OBJECT( par_stdin );
        tmp_compexpr_left_9 = par_stdin;
        tmp_compexpr_right_9 = Py_None;
        tmp_condition_result_15 = ( tmp_compexpr_left_9 == tmp_compexpr_right_9 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_11;
        }
        else
        {
            goto branch_no_11;
        }
        branch_yes_11:;
        {
            PyObject *tmp_called_instance_10;
            PyObject *tmp_source_name_9;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( var_proc );
            tmp_source_name_9 = var_proc;
            tmp_called_instance_10 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_stdin );
            if ( tmp_called_instance_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame.f_lineno = 159;
            tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_close );
            Py_DECREF( tmp_called_instance_10 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_1 = "ooooooooNoooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_11:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5bfe21b1ae42d205d913d52f1d9d5b74 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5bfe21b1ae42d205d913d52f1d9d5b74 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5bfe21b1ae42d205d913d52f1d9d5b74, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5bfe21b1ae42d205d913d52f1d9d5b74->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5bfe21b1ae42d205d913d52f1d9d5b74, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5bfe21b1ae42d205d913d52f1d9d5b74,
        type_description_1,
        par_cmd,
        par_stdin,
        par_stdout,
        par_stderr,
        par_env,
        par_independent,
        par_cwd,
        par_kw,
        NULL,
        var__stdin,
        var_redirect_out,
        var_blackhole,
        var__stdout,
        var__stderr,
        var_encoding,
        var_kwargs,
        var_main_args,
        var_create_interrupt_event,
        var_interrupt_event,
        var_DuplicateHandle,
        var_GetCurrentProcess,
        var_DUPLICATE_SAME_ACCESS,
        var_CREATE_NEW_PROCESS_GROUP,
        var_pid,
        var_handle,
        var_proc,
        var_exc,
        var_msg,
        var_without_env
    );


    // Release cached frame.
    if ( frame_5bfe21b1ae42d205d913d52f1d9d5b74 == cache_frame_5bfe21b1ae42d205d913d52f1d9d5b74 )
    {
        Py_DECREF( frame_5bfe21b1ae42d205d913d52f1d9d5b74 );
    }
    cache_frame_5bfe21b1ae42d205d913d52f1d9d5b74 = NULL;

    assertFrameObject( frame_5bfe21b1ae42d205d913d52f1d9d5b74 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    CHECK_OBJECT( var_proc );
    tmp_return_value = var_proc;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cmd );
    Py_DECREF( par_cmd );
    par_cmd = NULL;

    CHECK_OBJECT( (PyObject *)par_stdin );
    Py_DECREF( par_stdin );
    par_stdin = NULL;

    CHECK_OBJECT( (PyObject *)par_stdout );
    Py_DECREF( par_stdout );
    par_stdout = NULL;

    CHECK_OBJECT( (PyObject *)par_stderr );
    Py_DECREF( par_stderr );
    par_stderr = NULL;

    CHECK_OBJECT( (PyObject *)par_env );
    Py_DECREF( par_env );
    par_env = NULL;

    CHECK_OBJECT( (PyObject *)par_independent );
    Py_DECREF( par_independent );
    par_independent = NULL;

    Py_XDECREF( par_cwd );
    par_cwd = NULL;

    CHECK_OBJECT( (PyObject *)par_kw );
    Py_DECREF( par_kw );
    par_kw = NULL;

    CHECK_OBJECT( (PyObject *)var__stdin );
    Py_DECREF( var__stdin );
    var__stdin = NULL;

    CHECK_OBJECT( (PyObject *)var_redirect_out );
    Py_DECREF( var_redirect_out );
    var_redirect_out = NULL;

    Py_XDECREF( var_blackhole );
    var_blackhole = NULL;

    CHECK_OBJECT( (PyObject *)var__stdout );
    Py_DECREF( var__stdout );
    var__stdout = NULL;

    CHECK_OBJECT( (PyObject *)var__stderr );
    Py_DECREF( var__stderr );
    var__stderr = NULL;

    CHECK_OBJECT( (PyObject *)var_encoding );
    Py_DECREF( var_encoding );
    var_encoding = NULL;

    CHECK_OBJECT( (PyObject *)var_kwargs );
    Py_DECREF( var_kwargs );
    var_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_main_args );
    Py_DECREF( var_main_args );
    var_main_args = NULL;

    Py_XDECREF( var_create_interrupt_event );
    var_create_interrupt_event = NULL;

    Py_XDECREF( var_interrupt_event );
    var_interrupt_event = NULL;

    Py_XDECREF( var_DuplicateHandle );
    var_DuplicateHandle = NULL;

    Py_XDECREF( var_GetCurrentProcess );
    var_GetCurrentProcess = NULL;

    Py_XDECREF( var_DUPLICATE_SAME_ACCESS );
    var_DUPLICATE_SAME_ACCESS = NULL;

    Py_XDECREF( var_CREATE_NEW_PROCESS_GROUP );
    var_CREATE_NEW_PROCESS_GROUP = NULL;

    Py_XDECREF( var_pid );
    var_pid = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_proc );
    Py_DECREF( var_proc );
    var_proc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_cmd );
    par_cmd = NULL;

    CHECK_OBJECT( (PyObject *)par_stdin );
    Py_DECREF( par_stdin );
    par_stdin = NULL;

    CHECK_OBJECT( (PyObject *)par_stdout );
    Py_DECREF( par_stdout );
    par_stdout = NULL;

    CHECK_OBJECT( (PyObject *)par_stderr );
    Py_DECREF( par_stderr );
    par_stderr = NULL;

    CHECK_OBJECT( (PyObject *)par_env );
    Py_DECREF( par_env );
    par_env = NULL;

    CHECK_OBJECT( (PyObject *)par_independent );
    Py_DECREF( par_independent );
    par_independent = NULL;

    Py_XDECREF( par_cwd );
    par_cwd = NULL;

    CHECK_OBJECT( (PyObject *)par_kw );
    Py_DECREF( par_kw );
    par_kw = NULL;

    Py_XDECREF( var__stdin );
    var__stdin = NULL;

    Py_XDECREF( var_redirect_out );
    var_redirect_out = NULL;

    Py_XDECREF( var_blackhole );
    var_blackhole = NULL;

    Py_XDECREF( var__stdout );
    var__stdout = NULL;

    Py_XDECREF( var__stderr );
    var__stderr = NULL;

    Py_XDECREF( var_encoding );
    var_encoding = NULL;

    Py_XDECREF( var_kwargs );
    var_kwargs = NULL;

    Py_XDECREF( var_main_args );
    var_main_args = NULL;

    Py_XDECREF( var_create_interrupt_event );
    var_create_interrupt_event = NULL;

    Py_XDECREF( var_interrupt_event );
    var_interrupt_event = NULL;

    Py_XDECREF( var_DuplicateHandle );
    var_DuplicateHandle = NULL;

    Py_XDECREF( var_GetCurrentProcess );
    var_GetCurrentProcess = NULL;

    Py_XDECREF( var_DUPLICATE_SAME_ACCESS );
    var_DUPLICATE_SAME_ACCESS = NULL;

    Py_XDECREF( var_CREATE_NEW_PROCESS_GROUP );
    var_CREATE_NEW_PROCESS_GROUP = NULL;

    Py_XDECREF( var_pid );
    var_pid = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var_proc );
    var_proc = NULL;

    Py_XDECREF( var_exc );
    var_exc = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_without_env );
    var_without_env = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$launcher$$$function_1_launch_kernel$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_b499e081d695f48cbb04d9dee8fce877;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b499e081d695f48cbb04d9dee8fce877 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_b499e081d695f48cbb04d9dee8fce877, codeobj_b499e081d695f48cbb04d9dee8fce877, module_jupyter_client$launcher, 0 );
    frame_b499e081d695f48cbb04d9dee8fce877 = cache_frame_b499e081d695f48cbb04d9dee8fce877;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b499e081d695f48cbb04d9dee8fce877 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b499e081d695f48cbb04d9dee8fce877 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_b499e081d695f48cbb04d9dee8fce877->m_frame.f_lineno = 133;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_setsid );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b499e081d695f48cbb04d9dee8fce877 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b499e081d695f48cbb04d9dee8fce877 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b499e081d695f48cbb04d9dee8fce877 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b499e081d695f48cbb04d9dee8fce877, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b499e081d695f48cbb04d9dee8fce877->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b499e081d695f48cbb04d9dee8fce877, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b499e081d695f48cbb04d9dee8fce877,
        type_description_1
    );


    // Release cached frame.
    if ( frame_b499e081d695f48cbb04d9dee8fce877 == cache_frame_b499e081d695f48cbb04d9dee8fce877 )
    {
        Py_DECREF( frame_b499e081d695f48cbb04d9dee8fce877 );
    }
    cache_frame_b499e081d695f48cbb04d9dee8fce877 = NULL;

    assertFrameObject( frame_b499e081d695f48cbb04d9dee8fce877 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$launcher$$$function_1_launch_kernel$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jupyter_client$launcher$$$function_1_launch_kernel( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$launcher$$$function_1_launch_kernel,
        const_str_plain_launch_kernel,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5bfe21b1ae42d205d913d52f1d9d5b74,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$launcher,
        const_str_digest_0a731563c866e1ddde4a5434153745d1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$launcher$$$function_1_launch_kernel$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$launcher$$$function_1_launch_kernel$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_3a446f0691307d3156e6a88d64741e5b,
#endif
        codeobj_b499e081d695f48cbb04d9dee8fce877,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$launcher,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jupyter_client$launcher =
{
    PyModuleDef_HEAD_INIT,
    "jupyter_client.launcher",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jupyter_client$launcher)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jupyter_client$launcher)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jupyter_client$launcher );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jupyter_client.launcher: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jupyter_client.launcher: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jupyter_client.launcher: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjupyter_client$launcher" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jupyter_client$launcher = Py_InitModule4(
        "jupyter_client.launcher",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jupyter_client$launcher = PyModule_Create( &mdef_jupyter_client$launcher );
#endif

    moduledict_jupyter_client$launcher = MODULE_DICT( module_jupyter_client$launcher );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jupyter_client$launcher,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jupyter_client$launcher,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jupyter_client$launcher,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jupyter_client$launcher,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jupyter_client$launcher );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_00f6f3f4777f3f842cb3b18df7dd2c0a, module_jupyter_client$launcher );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_99361a8744f467847ed0d8e96c6979a3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_1b13362816a945fa6909c601273ecebe;
        UPDATE_STRING_DICT0( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_99361a8744f467847ed0d8e96c6979a3 = MAKE_MODULE_FRAME( codeobj_99361a8744f467847ed0d8e96c6979a3, module_jupyter_client$launcher );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_99361a8744f467847ed0d8e96c6979a3 );
    assert( Py_REFCNT( frame_99361a8744f467847ed0d8e96c6979a3 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_os;
        tmp_globals_name_1 = (PyObject *)moduledict_jupyter_client$launcher;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_99361a8744f467847ed0d8e96c6979a3->m_frame.f_lineno = 6;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_sys;
        tmp_globals_name_2 = (PyObject *)moduledict_jupyter_client$launcher;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_99361a8744f467847ed0d8e96c6979a3->m_frame.f_lineno = 7;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        assert( !(tmp_assign_source_5 == NULL) );
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_subprocess;
        tmp_globals_name_3 = (PyObject *)moduledict_jupyter_client$launcher;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_Popen_str_plain_PIPE_tuple;
        tmp_level_name_3 = const_int_0;
        frame_99361a8744f467847ed0d8e96c6979a3->m_frame.f_lineno = 8;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Popen );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_Popen, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_PIPE );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_PIPE, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_a854e3b2bb775c8c59ed1fbb95556781;
        tmp_globals_name_4 = (PyObject *)moduledict_jupyter_client$launcher;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_getdefaultencoding_tuple;
        tmp_level_name_4 = const_int_0;
        frame_99361a8744f467847ed0d8e96c6979a3->m_frame.f_lineno = 10;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_getdefaultencoding );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_getdefaultencoding, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_34e3966f9679fde95e40f5931481245c;
        tmp_globals_name_5 = (PyObject *)moduledict_jupyter_client$launcher;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_cast_bytes_py2_str_plain_PY3_tuple;
        tmp_level_name_5 = const_int_0;
        frame_99361a8744f467847ed0d8e96c6979a3->m_frame.f_lineno = 11;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_cast_bytes_py2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_cast_bytes_py2, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_PY3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_PY3, tmp_assign_source_12 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_f239d4ac02f17c205f0d85fba95f72ff;
        tmp_globals_name_6 = (PyObject *)moduledict_jupyter_client$launcher;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_get_logger_tuple;
        tmp_level_name_6 = const_int_0;
        frame_99361a8744f467847ed0d8e96c6979a3->m_frame.f_lineno = 12;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_get_logger );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_get_logger, tmp_assign_source_13 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_99361a8744f467847ed0d8e96c6979a3 );
#endif
    popFrameStack();

    assertFrameObject( frame_99361a8744f467847ed0d8e96c6979a3 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_99361a8744f467847ed0d8e96c6979a3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_99361a8744f467847ed0d8e96c6979a3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_99361a8744f467847ed0d8e96c6979a3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_99361a8744f467847ed0d8e96c6979a3, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_none_none_none_false_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_14 = MAKE_FUNCTION_jupyter_client$launcher$$$function_1_launch_kernel( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain_launch_kernel, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = LIST_COPY( const_list_str_plain_launch_kernel_list );
        UPDATE_STRING_DICT1( moduledict_jupyter_client$launcher, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_15 );
    }

    return MOD_RETURN_VALUE( module_jupyter_client$launcher );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
