/* Generated code for Python module 'pandocfilters'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_pandocfilters" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandocfilters;
PyDictObject *moduledict_pandocfilters;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_05dc8befe0400d916af2ff4b5795ee7e;
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
static PyObject *const_str_plain_HorizontalRule;
extern PyObject *const_str_plain_walk;
static PyObject *const_str_plain_Null;
extern PyObject *const_str_plain_result;
static PyObject *const_tuple_str_plain_Quoted_int_pos_2_tuple;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_str_plain_CodeBlock_int_pos_2_tuple;
static PyObject *const_str_plain_actions;
extern PyObject *const_str_plain_action;
extern PyObject *const_str_plain_array;
extern PyObject *const_str_plain_dict;
static PyObject *const_tuple_str_plain_Strong_int_pos_1_tuple;
extern PyObject *const_tuple_str_space_tuple;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_str_plain_Span_int_pos_2_tuple;
static PyObject *const_str_digest_b5d121348c71f5f8f1075409de4b9d9c;
extern PyObject *const_str_plain_mkdir;
static PyObject *const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple;
extern PyObject *const_str_plain_elt;
extern PyObject *const_str_plain_args;
static PyObject *const_str_plain_Plain;
extern PyObject *const_str_plain_encode;
static PyObject *const_tuple_str_plain_RawBlock_int_pos_2_tuple;
static PyObject *const_tuple_str_plain_Null_int_0_tuple;
extern PyObject *const_str_plain_numargs;
static PyObject *const_tuple_str_plain_format_str_plain_default_str_plain_alternates_tuple;
extern PyObject *const_str_plain_json;
extern PyObject *const_str_plain_default;
extern PyObject *const_str_plain_fn;
static PyObject *const_tuple_0252737fdfceae69bea5906ec19d4030_tuple;
static PyObject *const_str_plain_keyvals;
extern PyObject *const_str_plain_id;
static PyObject *const_str_plain_RawBlock;
static PyObject *const_str_plain_OrderedList;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_getfilesystemencoding;
static PyObject *const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple;
extern PyObject *const_str_plain_classes;
static PyObject *const_str_plain_get_caption;
extern PyObject *const_int_pos_5;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_82706484618aa4cfb291f1f1599d7dda;
extern PyObject *const_str_plain_read;
static PyObject *const_str_plain_Cite;
extern PyObject *const_str_plain_get_extension;
extern PyObject *const_str_plain_argv;
static PyObject *const_str_digest_a9bbf8eddc9736a0653e7e7b30eefd84;
extern PyObject *const_str_plain_RawInline;
static PyObject *const_str_plain_unMeta;
static PyObject *const_str_plain_alternates;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_meta;
extern PyObject *const_str_dot;
static PyObject *const_tuple_b710bb8489ab703046473c2e40479860_tuple;
static PyObject *const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple;
static PyObject *const_str_digest_40fe20f63d60820135c9f2ec9801401c;
static PyObject *const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_ff847e3cdb51ac69134ff0ad25002866;
static PyObject *const_tuple_str_plain_BlockQuote_int_pos_1_tuple;
extern PyObject *const_str_plain_Emph;
static PyObject *const_tuple_str_plain_Superscript_int_pos_1_tuple;
extern PyObject *const_str_plain_res;
extern PyObject *const_str_plain_attrs;
static PyObject *const_tuple_str_plain_LineBreak_int_0_tuple;
extern PyObject *const_tuple_str_plain_id_str_empty_tuple;
static PyObject *const_str_plain_typef;
static PyObject *const_str_plain_CodeBlock;
static PyObject *const_str_plain_Math;
static PyObject *const_tuple_str_plain_RawInline_int_pos_2_tuple;
extern PyObject *const_str_plain_hexdigest;
static PyObject *const_str_plain_LineBreak;
extern PyObject *const_str_plain_t;
static PyObject *const_str_digest_b7c4c5e97ce628bf6b0dac3111668c34;
extern PyObject *const_str_plain_ext;
extern PyObject *const_str_plain_hashlib;
static PyObject *const_tuple_str_plain_Space_int_0_tuple;
static PyObject *const_tuple_str_plain_x_str_plain_attrs_tuple;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_item;
static PyObject *const_str_plain_input_stream;
extern PyObject *const_str_plain_value;
static PyObject *const_tuple_str_plain_OrderedList_int_pos_2_tuple;
static PyObject *const_str_plain_toJSONFilter;
extern PyObject *const_str_plain_kv;
static PyObject *const_tuple_str_plain_BulletList_int_pos_1_tuple;
static PyObject *const_tuple_str_plain_Note_int_pos_1_tuple;
static PyObject *const_str_digest_e03f4b9ec54eb6292fbf5f07893f0cda;
static PyObject *const_list_str_plain_Str_str_plain_MetaString_list;
static PyObject *const_str_digest_3d3d836f635866494e20f3ca0f20012b;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_stringify;
extern PyObject *const_str_plain_fun;
extern PyObject *const_str_space;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_stderr;
extern PyObject *const_str_plain_go;
static PyObject *const_str_plain_Quoted;
static PyObject *const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple;
static PyObject *const_tuple_str_plain_Plain_int_pos_1_tuple;
static PyObject *const_str_plain_eltType;
extern PyObject *const_str_plain_Link;
static PyObject *const_tuple_str_plain_SoftBreak_int_0_tuple;
extern PyObject *const_str_plain_xs;
static PyObject *const_str_plain_toJSONFilters;
static PyObject *const_str_digest_958c3218d8b975cccdb175fa77cac6ba;
static PyObject *const_tuple_str_plain_Link_int_pos_3_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_buffer;
extern PyObject *const_tuple_str_empty_tuple;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
static PyObject *const_str_digest_6b99c96f548d8a10ff28a0c9b4533196;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_digest_acec08ef2139364a6dc0302b7e4147a3;
extern PyObject *const_str_plain_applyJSONFilters;
static PyObject *const_tuple_str_plain_Str_int_pos_1_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_content;
extern PyObject *const_str_plain_get_value;
extern PyObject *const_str_plain_pandocfilters;
static PyObject *const_str_plain_SoftBreak;
extern PyObject *const_str_plain_val;
static PyObject *const_str_digest_ea1e0497265d8883d491b27d8c3a1a02;
extern PyObject *const_str_plain_Table;
extern PyObject *const_str_plain_dumps;
extern PyObject *const_str_plain_Strong;
static PyObject *const_str_plain_Subscript;
static PyObject *const_tuple_str_plain_Image_int_pos_3_tuple;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_altered;
static PyObject *const_str_digest_616d99c141a2994de9878ed27e1e904b;
extern PyObject *const_str_plain_x;
static PyObject *const_str_plain_caption;
static PyObject *const_str_plain_SmallCaps;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_tuple_str_plain_eltType_str_plain_numargs_str_plain_fun_tuple;
static PyObject *const_str_plain_DefinitionList;
extern PyObject *const_dict_81b3970727674c20ce12b1a4757dad21;
static PyObject *const_str_digest_7c3f14c99057ecc3e4a3a5c8ef77815c;
static PyObject *const_str_plain_Para;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_Superscript;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_Span;
static PyObject *const_str_digest_b4781d1fee4f6ac63d247204fd474340;
static PyObject *const_tuple_str_plain_Strikeout_int_pos_1_tuple;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_str_digest_9e86034c6ef53ef2eec3c49e69fd5864;
extern PyObject *const_str_plain_codecs;
extern PyObject *const_str_plain_source;
static PyObject *const_str_plain_MetaString;
static PyObject *const_str_plain_lenargs;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_io;
static PyObject *const_str_digest_e62c65f4757cdefae810eec0f3a3e1a9;
static PyObject *const_str_digest_4c9fed436cc981918d54287b53c873de;
extern PyObject *const_str_plain_sha1;
extern PyObject *const_str_plain_z;
static PyObject *const_tuple_str_plain_DefinitionList_int_pos_1_tuple;
extern PyObject *const_str_plain_doc;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_Header;
static PyObject *const_tuple_str_plain_Code_int_pos_2_tuple;
static PyObject *const_tuple_str_plain_SmallCaps_int_pos_1_tuple;
extern PyObject *const_str_plain_ident;
static PyObject *const_tuple_str_plain_HorizontalRule_int_0_tuple;
extern PyObject *const_str_plain_key;
static PyObject *const_str_plain_Space;
extern PyObject *const_str_newline;
static PyObject *const_str_digest_baab06bf48ed3d0177b4cf0cc94ff514;
static PyObject *const_str_plain_BlockQuote;
extern PyObject *const_int_pos_3;
static PyObject *const_tuple_str_plain_Subscript_int_pos_1_tuple;
static PyObject *const_tuple_c690c777671328bbaed97bae8773aa7f_tuple;
static PyObject *const_tuple_str_plain_Div_int_pos_2_tuple;
static PyObject *const_tuple_str_plain_action_tuple;
extern PyObject *const_str_plain_TextIOWrapper;
extern PyObject *const_str_plain_write;
extern PyObject *const_str_plain_stdin;
static PyObject *const_tuple_str_plain_Header_int_pos_3_tuple;
extern PyObject *const_str_plain_Div;
static PyObject *const_tuple_str_plain_Math_int_pos_2_tuple;
static PyObject *const_tuple_str_plain_x_str_plain_result_str_plain_go_tuple;
extern PyObject *const_str_plain_module;
extern PyObject *const_str_plain_loads;
static PyObject *const_tuple_str_plain_Table_int_pos_5_tuple;
extern PyObject *const_str_plain_get;
static PyObject *const_tuple_str_plain_Cite_int_pos_2_tuple;
static PyObject *const_str_plain_Note;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_Strikeout;
extern PyObject *const_int_pos_2;
static PyObject *const_tuple_str_plain_Emph_int_pos_1_tuple;
extern PyObject *const_str_plain_Image;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_attributes;
extern PyObject *const_str_empty;
static PyObject *const_str_plain_BulletList;
static PyObject *const_str_plain_imagedir;
extern PyObject *const_str_plain_getreader;
static PyObject *const_str_plain_Code;
static PyObject *const_str_plain_get_filename4code;
static PyObject *const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple;
static PyObject *const_tuple_str_plain_Para_int_pos_1_tuple;
static PyObject *const_str_plain_Str;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_05dc8befe0400d916af2ff4b5795ee7e = UNSTREAM_STRING_ASCII( &constant_bin[ 4517704 ], 16, 0 );
    const_str_plain_HorizontalRule = UNSTREAM_STRING_ASCII( &constant_bin[ 4517720 ], 14, 1 );
    const_str_plain_Null = UNSTREAM_STRING_ASCII( &constant_bin[ 3831 ], 4, 1 );
    const_tuple_str_plain_Quoted_int_pos_2_tuple = PyTuple_New( 2 );
    const_str_plain_Quoted = UNSTREAM_STRING_ASCII( &constant_bin[ 4517734 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Quoted_int_pos_2_tuple, 0, const_str_plain_Quoted ); Py_INCREF( const_str_plain_Quoted );
    PyTuple_SET_ITEM( const_tuple_str_plain_Quoted_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_tuple_str_plain_CodeBlock_int_pos_2_tuple = PyTuple_New( 2 );
    const_str_plain_CodeBlock = UNSTREAM_STRING_ASCII( &constant_bin[ 4517740 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CodeBlock_int_pos_2_tuple, 0, const_str_plain_CodeBlock ); Py_INCREF( const_str_plain_CodeBlock );
    PyTuple_SET_ITEM( const_tuple_str_plain_CodeBlock_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_str_plain_actions = UNSTREAM_STRING_ASCII( &constant_bin[ 904350 ], 7, 1 );
    const_tuple_str_plain_Strong_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Strong_int_pos_1_tuple, 0, const_str_plain_Strong ); Py_INCREF( const_str_plain_Strong );
    PyTuple_SET_ITEM( const_tuple_str_plain_Strong_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_str_plain_Span_int_pos_2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Span_int_pos_2_tuple, 0, const_str_plain_Span ); Py_INCREF( const_str_plain_Span );
    PyTuple_SET_ITEM( const_tuple_str_plain_Span_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_str_digest_b5d121348c71f5f8f1075409de4b9d9c = UNSTREAM_STRING_ASCII( &constant_bin[ 4517749 ], 21, 0 );
    const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 0, const_str_plain_actions ); Py_INCREF( const_str_plain_actions );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 1, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 2, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 3, const_str_plain_doc ); Py_INCREF( const_str_plain_doc );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 4, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    const_str_plain_altered = UNSTREAM_STRING_ASCII( &constant_bin[ 64270 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 5, const_str_plain_altered ); Py_INCREF( const_str_plain_altered );
    PyTuple_SET_ITEM( const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 6, const_str_plain_action ); Py_INCREF( const_str_plain_action );
    const_str_plain_Plain = UNSTREAM_STRING_ASCII( &constant_bin[ 4517770 ], 5, 1 );
    const_tuple_str_plain_RawBlock_int_pos_2_tuple = PyTuple_New( 2 );
    const_str_plain_RawBlock = UNSTREAM_STRING_ASCII( &constant_bin[ 4517775 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RawBlock_int_pos_2_tuple, 0, const_str_plain_RawBlock ); Py_INCREF( const_str_plain_RawBlock );
    PyTuple_SET_ITEM( const_tuple_str_plain_RawBlock_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_tuple_str_plain_Null_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Null_int_0_tuple, 0, const_str_plain_Null ); Py_INCREF( const_str_plain_Null );
    PyTuple_SET_ITEM( const_tuple_str_plain_Null_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_tuple_str_plain_format_str_plain_default_str_plain_alternates_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_format_str_plain_default_str_plain_alternates_tuple, 0, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_str_plain_format_str_plain_default_str_plain_alternates_tuple, 1, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_str_plain_alternates = UNSTREAM_STRING_ASCII( &constant_bin[ 4517783 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_format_str_plain_default_str_plain_alternates_tuple, 2, const_str_plain_alternates ); Py_INCREF( const_str_plain_alternates );
    const_tuple_0252737fdfceae69bea5906ec19d4030_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 0, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 1, const_str_plain_action ); Py_INCREF( const_str_plain_action );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 2, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 3, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 4, const_str_plain_array ); Py_INCREF( const_str_plain_array );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 5, const_str_plain_item ); Py_INCREF( const_str_plain_item );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 6, const_str_plain_res ); Py_INCREF( const_str_plain_res );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 7, const_str_plain_z ); Py_INCREF( const_str_plain_z );
    PyTuple_SET_ITEM( const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 8, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    const_str_plain_keyvals = UNSTREAM_STRING_ASCII( &constant_bin[ 4517793 ], 7, 1 );
    const_str_plain_OrderedList = UNSTREAM_STRING_ASCII( &constant_bin[ 4517800 ], 11, 1 );
    const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple, 0, const_str_plain_kv ); Py_INCREF( const_str_plain_kv );
    const_str_plain_caption = UNSTREAM_STRING_ASCII( &constant_bin[ 1824132 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple, 1, const_str_plain_caption ); Py_INCREF( const_str_plain_caption );
    const_str_plain_typef = UNSTREAM_STRING_ASCII( &constant_bin[ 191149 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple, 2, const_str_plain_typef ); Py_INCREF( const_str_plain_typef );
    PyTuple_SET_ITEM( const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple, 3, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple, 4, const_str_plain_res ); Py_INCREF( const_str_plain_res );
    const_str_plain_get_caption = UNSTREAM_STRING_ASCII( &constant_bin[ 4517811 ], 11, 1 );
    const_str_digest_82706484618aa4cfb291f1f1599d7dda = UNSTREAM_STRING_ASCII( &constant_bin[ 4517822 ], 7, 0 );
    const_str_plain_Cite = UNSTREAM_STRING_ASCII( &constant_bin[ 4517829 ], 4, 1 );
    const_str_digest_a9bbf8eddc9736a0653e7e7b30eefd84 = UNSTREAM_STRING_ASCII( &constant_bin[ 189852 ], 9, 0 );
    const_str_plain_unMeta = UNSTREAM_STRING_ASCII( &constant_bin[ 4517833 ], 6, 1 );
    const_tuple_b710bb8489ab703046473c2e40479860_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b710bb8489ab703046473c2e40479860_tuple, 0, const_str_plain_module ); Py_INCREF( const_str_plain_module );
    PyTuple_SET_ITEM( const_tuple_b710bb8489ab703046473c2e40479860_tuple, 1, const_str_plain_content ); Py_INCREF( const_str_plain_content );
    PyTuple_SET_ITEM( const_tuple_b710bb8489ab703046473c2e40479860_tuple, 2, const_str_plain_ext ); Py_INCREF( const_str_plain_ext );
    const_str_plain_imagedir = UNSTREAM_STRING_ASCII( &constant_bin[ 4517839 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_b710bb8489ab703046473c2e40479860_tuple, 3, const_str_plain_imagedir ); Py_INCREF( const_str_plain_imagedir );
    PyTuple_SET_ITEM( const_tuple_b710bb8489ab703046473c2e40479860_tuple, 4, const_str_plain_fn ); Py_INCREF( const_str_plain_fn );
    const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 0, const_str_plain_kv ); Py_INCREF( const_str_plain_kv );
    PyTuple_SET_ITEM( const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 1, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 2, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 3, const_str_plain_res ); Py_INCREF( const_str_plain_res );
    PyTuple_SET_ITEM( const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 4, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    PyTuple_SET_ITEM( const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 5, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    const_str_digest_40fe20f63d60820135c9f2ec9801401c = UNSTREAM_STRING_ASCII( &constant_bin[ 4517847 ], 284, 0 );
    const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple, 0, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple, 1, const_str_plain_ident ); Py_INCREF( const_str_plain_ident );
    PyTuple_SET_ITEM( const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple, 2, const_str_plain_classes ); Py_INCREF( const_str_plain_classes );
    PyTuple_SET_ITEM( const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple, 3, const_str_plain_keyvals ); Py_INCREF( const_str_plain_keyvals );
    const_str_digest_ff847e3cdb51ac69134ff0ad25002866 = UNSTREAM_STRING_ASCII( &constant_bin[ 206419 ], 4, 0 );
    const_tuple_str_plain_BlockQuote_int_pos_1_tuple = PyTuple_New( 2 );
    const_str_plain_BlockQuote = UNSTREAM_STRING_ASCII( &constant_bin[ 4518131 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_BlockQuote_int_pos_1_tuple, 0, const_str_plain_BlockQuote ); Py_INCREF( const_str_plain_BlockQuote );
    PyTuple_SET_ITEM( const_tuple_str_plain_BlockQuote_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_str_plain_Superscript_int_pos_1_tuple = PyTuple_New( 2 );
    const_str_plain_Superscript = UNSTREAM_STRING_ASCII( &constant_bin[ 4518141 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Superscript_int_pos_1_tuple, 0, const_str_plain_Superscript ); Py_INCREF( const_str_plain_Superscript );
    PyTuple_SET_ITEM( const_tuple_str_plain_Superscript_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_str_plain_LineBreak_int_0_tuple = PyTuple_New( 2 );
    const_str_plain_LineBreak = UNSTREAM_STRING_ASCII( &constant_bin[ 4518152 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LineBreak_int_0_tuple, 0, const_str_plain_LineBreak ); Py_INCREF( const_str_plain_LineBreak );
    PyTuple_SET_ITEM( const_tuple_str_plain_LineBreak_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_plain_Math = UNSTREAM_STRING_ASCII( &constant_bin[ 1267445 ], 4, 1 );
    const_tuple_str_plain_RawInline_int_pos_2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RawInline_int_pos_2_tuple, 0, const_str_plain_RawInline ); Py_INCREF( const_str_plain_RawInline );
    PyTuple_SET_ITEM( const_tuple_str_plain_RawInline_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_str_digest_b7c4c5e97ce628bf6b0dac3111668c34 = UNSTREAM_STRING_ASCII( &constant_bin[ 4518161 ], 601, 0 );
    const_tuple_str_plain_Space_int_0_tuple = PyTuple_New( 2 );
    const_str_plain_Space = UNSTREAM_STRING_ASCII( &constant_bin[ 108922 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Space_int_0_tuple, 0, const_str_plain_Space ); Py_INCREF( const_str_plain_Space );
    PyTuple_SET_ITEM( const_tuple_str_plain_Space_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_tuple_str_plain_x_str_plain_attrs_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_attrs_tuple, 0, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_attrs_tuple, 1, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    const_str_plain_input_stream = UNSTREAM_STRING_ASCII( &constant_bin[ 4518762 ], 12, 1 );
    const_tuple_str_plain_OrderedList_int_pos_2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_OrderedList_int_pos_2_tuple, 0, const_str_plain_OrderedList ); Py_INCREF( const_str_plain_OrderedList );
    PyTuple_SET_ITEM( const_tuple_str_plain_OrderedList_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_str_plain_toJSONFilter = UNSTREAM_STRING_ASCII( &constant_bin[ 4518774 ], 12, 1 );
    const_tuple_str_plain_BulletList_int_pos_1_tuple = PyTuple_New( 2 );
    const_str_plain_BulletList = UNSTREAM_STRING_ASCII( &constant_bin[ 4518786 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_BulletList_int_pos_1_tuple, 0, const_str_plain_BulletList ); Py_INCREF( const_str_plain_BulletList );
    PyTuple_SET_ITEM( const_tuple_str_plain_BulletList_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_str_plain_Note_int_pos_1_tuple = PyTuple_New( 2 );
    const_str_plain_Note = UNSTREAM_STRING_ASCII( &constant_bin[ 9482 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Note_int_pos_1_tuple, 0, const_str_plain_Note ); Py_INCREF( const_str_plain_Note );
    PyTuple_SET_ITEM( const_tuple_str_plain_Note_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_str_digest_e03f4b9ec54eb6292fbf5f07893f0cda = UNSTREAM_STRING_ASCII( &constant_bin[ 4518796 ], 22, 0 );
    const_list_str_plain_Str_str_plain_MetaString_list = PyList_New( 2 );
    const_str_plain_Str = UNSTREAM_STRING_ASCII( &constant_bin[ 89741 ], 3, 1 );
    PyList_SET_ITEM( const_list_str_plain_Str_str_plain_MetaString_list, 0, const_str_plain_Str ); Py_INCREF( const_str_plain_Str );
    const_str_plain_MetaString = UNSTREAM_STRING_ASCII( &constant_bin[ 4518818 ], 10, 1 );
    PyList_SET_ITEM( const_list_str_plain_Str_str_plain_MetaString_list, 1, const_str_plain_MetaString ); Py_INCREF( const_str_plain_MetaString );
    const_str_digest_3d3d836f635866494e20f3ca0f20012b = UNSTREAM_STRING_ASCII( &constant_bin[ 4518828 ], 74, 0 );
    const_str_plain_stringify = UNSTREAM_STRING_ASCII( &constant_bin[ 2853072 ], 9, 1 );
    const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple, 0, const_str_plain_actions ); Py_INCREF( const_str_plain_actions );
    PyTuple_SET_ITEM( const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple, 1, const_str_plain_input_stream ); Py_INCREF( const_str_plain_input_stream );
    PyTuple_SET_ITEM( const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple, 2, const_str_plain_source ); Py_INCREF( const_str_plain_source );
    PyTuple_SET_ITEM( const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple, 3, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    const_tuple_str_plain_Plain_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Plain_int_pos_1_tuple, 0, const_str_plain_Plain ); Py_INCREF( const_str_plain_Plain );
    PyTuple_SET_ITEM( const_tuple_str_plain_Plain_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_str_plain_eltType = UNSTREAM_STRING_ASCII( &constant_bin[ 4518902 ], 7, 1 );
    const_tuple_str_plain_SoftBreak_int_0_tuple = PyTuple_New( 2 );
    const_str_plain_SoftBreak = UNSTREAM_STRING_ASCII( &constant_bin[ 4518909 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SoftBreak_int_0_tuple, 0, const_str_plain_SoftBreak ); Py_INCREF( const_str_plain_SoftBreak );
    PyTuple_SET_ITEM( const_tuple_str_plain_SoftBreak_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_plain_toJSONFilters = UNSTREAM_STRING_ASCII( &constant_bin[ 4518918 ], 13, 1 );
    const_str_digest_958c3218d8b975cccdb175fa77cac6ba = UNSTREAM_STRING_ASCII( &constant_bin[ 4518931 ], 164, 0 );
    const_tuple_str_plain_Link_int_pos_3_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Link_int_pos_3_tuple, 0, const_str_plain_Link ); Py_INCREF( const_str_plain_Link );
    PyTuple_SET_ITEM( const_tuple_str_plain_Link_int_pos_3_tuple, 1, const_int_pos_3 ); Py_INCREF( const_int_pos_3 );
    const_str_digest_6b99c96f548d8a10ff28a0c9b4533196 = UNSTREAM_STRING_ASCII( &constant_bin[ 4519095 ], 65, 0 );
    const_str_digest_acec08ef2139364a6dc0302b7e4147a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4519160 ], 38, 0 );
    const_tuple_str_plain_Str_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Str_int_pos_1_tuple, 0, const_str_plain_Str ); Py_INCREF( const_str_plain_Str );
    PyTuple_SET_ITEM( const_tuple_str_plain_Str_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_str_digest_ea1e0497265d8883d491b27d8c3a1a02 = UNSTREAM_STRING_ASCII( &constant_bin[ 4519198 ], 18, 0 );
    const_str_plain_Subscript = UNSTREAM_STRING_ASCII( &constant_bin[ 1114932 ], 9, 1 );
    const_tuple_str_plain_Image_int_pos_3_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Image_int_pos_3_tuple, 0, const_str_plain_Image ); Py_INCREF( const_str_plain_Image );
    PyTuple_SET_ITEM( const_tuple_str_plain_Image_int_pos_3_tuple, 1, const_int_pos_3 ); Py_INCREF( const_int_pos_3 );
    const_str_digest_616d99c141a2994de9878ed27e1e904b = UNSTREAM_STRING_ASCII( &constant_bin[ 4519216 ], 22, 0 );
    const_str_plain_SmallCaps = UNSTREAM_STRING_ASCII( &constant_bin[ 4519238 ], 9, 1 );
    const_tuple_str_plain_eltType_str_plain_numargs_str_plain_fun_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_eltType_str_plain_numargs_str_plain_fun_tuple, 0, const_str_plain_eltType ); Py_INCREF( const_str_plain_eltType );
    PyTuple_SET_ITEM( const_tuple_str_plain_eltType_str_plain_numargs_str_plain_fun_tuple, 1, const_str_plain_numargs ); Py_INCREF( const_str_plain_numargs );
    PyTuple_SET_ITEM( const_tuple_str_plain_eltType_str_plain_numargs_str_plain_fun_tuple, 2, const_str_plain_fun ); Py_INCREF( const_str_plain_fun );
    const_str_plain_DefinitionList = UNSTREAM_STRING_ASCII( &constant_bin[ 4519247 ], 14, 1 );
    const_str_digest_7c3f14c99057ecc3e4a3a5c8ef77815c = UNSTREAM_STRING_ASCII( &constant_bin[ 4519261 ], 210, 0 );
    const_str_plain_Para = UNSTREAM_STRING_ASCII( &constant_bin[ 9584 ], 4, 1 );
    const_str_digest_b4781d1fee4f6ac63d247204fd474340 = UNSTREAM_STRING_ASCII( &constant_bin[ 4519471 ], 534, 0 );
    const_tuple_str_plain_Strikeout_int_pos_1_tuple = PyTuple_New( 2 );
    const_str_plain_Strikeout = UNSTREAM_STRING_ASCII( &constant_bin[ 4520005 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Strikeout_int_pos_1_tuple, 0, const_str_plain_Strikeout ); Py_INCREF( const_str_plain_Strikeout );
    PyTuple_SET_ITEM( const_tuple_str_plain_Strikeout_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_str_digest_9e86034c6ef53ef2eec3c49e69fd5864 = UNSTREAM_STRING_ASCII( &constant_bin[ 4520014 ], 16, 0 );
    const_str_plain_lenargs = UNSTREAM_STRING_ASCII( &constant_bin[ 4520030 ], 7, 1 );
    const_str_digest_e62c65f4757cdefae810eec0f3a3e1a9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4520037 ], 89, 0 );
    const_str_digest_4c9fed436cc981918d54287b53c873de = UNSTREAM_STRING_ASCII( &constant_bin[ 4520126 ], 902, 0 );
    const_tuple_str_plain_DefinitionList_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_DefinitionList_int_pos_1_tuple, 0, const_str_plain_DefinitionList ); Py_INCREF( const_str_plain_DefinitionList );
    PyTuple_SET_ITEM( const_tuple_str_plain_DefinitionList_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_str_plain_Header = UNSTREAM_STRING_ASCII( &constant_bin[ 164472 ], 6, 1 );
    const_tuple_str_plain_Code_int_pos_2_tuple = PyTuple_New( 2 );
    const_str_plain_Code = UNSTREAM_STRING_ASCII( &constant_bin[ 60091 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Code_int_pos_2_tuple, 0, const_str_plain_Code ); Py_INCREF( const_str_plain_Code );
    PyTuple_SET_ITEM( const_tuple_str_plain_Code_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_tuple_str_plain_SmallCaps_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SmallCaps_int_pos_1_tuple, 0, const_str_plain_SmallCaps ); Py_INCREF( const_str_plain_SmallCaps );
    PyTuple_SET_ITEM( const_tuple_str_plain_SmallCaps_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_str_plain_HorizontalRule_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_HorizontalRule_int_0_tuple, 0, const_str_plain_HorizontalRule ); Py_INCREF( const_str_plain_HorizontalRule );
    PyTuple_SET_ITEM( const_tuple_str_plain_HorizontalRule_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_digest_baab06bf48ed3d0177b4cf0cc94ff514 = UNSTREAM_STRING_ASCII( &constant_bin[ 4521028 ], 94, 0 );
    const_tuple_str_plain_Subscript_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Subscript_int_pos_1_tuple, 0, const_str_plain_Subscript ); Py_INCREF( const_str_plain_Subscript );
    PyTuple_SET_ITEM( const_tuple_str_plain_Subscript_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_tuple_c690c777671328bbaed97bae8773aa7f_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_c690c777671328bbaed97bae8773aa7f_tuple, 0, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_c690c777671328bbaed97bae8773aa7f_tuple, 1, const_str_plain_val ); Py_INCREF( const_str_plain_val );
    PyTuple_SET_ITEM( const_tuple_c690c777671328bbaed97bae8773aa7f_tuple, 2, const_str_plain_format ); Py_INCREF( const_str_plain_format );
    PyTuple_SET_ITEM( const_tuple_c690c777671328bbaed97bae8773aa7f_tuple, 3, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    PyTuple_SET_ITEM( const_tuple_c690c777671328bbaed97bae8773aa7f_tuple, 4, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_tuple_str_plain_Div_int_pos_2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Div_int_pos_2_tuple, 0, const_str_plain_Div ); Py_INCREF( const_str_plain_Div );
    PyTuple_SET_ITEM( const_tuple_str_plain_Div_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_tuple_str_plain_action_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_action_tuple, 0, const_str_plain_action ); Py_INCREF( const_str_plain_action );
    const_tuple_str_plain_Header_int_pos_3_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Header_int_pos_3_tuple, 0, const_str_plain_Header ); Py_INCREF( const_str_plain_Header );
    PyTuple_SET_ITEM( const_tuple_str_plain_Header_int_pos_3_tuple, 1, const_int_pos_3 ); Py_INCREF( const_int_pos_3 );
    const_tuple_str_plain_Math_int_pos_2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Math_int_pos_2_tuple, 0, const_str_plain_Math ); Py_INCREF( const_str_plain_Math );
    PyTuple_SET_ITEM( const_tuple_str_plain_Math_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_tuple_str_plain_x_str_plain_result_str_plain_go_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_result_str_plain_go_tuple, 0, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_result_str_plain_go_tuple, 1, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_result_str_plain_go_tuple, 2, const_str_plain_go ); Py_INCREF( const_str_plain_go );
    const_tuple_str_plain_Table_int_pos_5_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Table_int_pos_5_tuple, 0, const_str_plain_Table ); Py_INCREF( const_str_plain_Table );
    PyTuple_SET_ITEM( const_tuple_str_plain_Table_int_pos_5_tuple, 1, const_int_pos_5 ); Py_INCREF( const_int_pos_5 );
    const_tuple_str_plain_Cite_int_pos_2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Cite_int_pos_2_tuple, 0, const_str_plain_Cite ); Py_INCREF( const_str_plain_Cite );
    PyTuple_SET_ITEM( const_tuple_str_plain_Cite_int_pos_2_tuple, 1, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_tuple_str_plain_Emph_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Emph_int_pos_1_tuple, 0, const_str_plain_Emph ); Py_INCREF( const_str_plain_Emph );
    PyTuple_SET_ITEM( const_tuple_str_plain_Emph_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    const_str_plain_get_filename4code = UNSTREAM_STRING_ASCII( &constant_bin[ 4519431 ], 17, 1 );
    const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple, 1, const_str_plain_lenargs ); Py_INCREF( const_str_plain_lenargs );
    PyTuple_SET_ITEM( const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple, 2, const_str_plain_xs ); Py_INCREF( const_str_plain_xs );
    PyTuple_SET_ITEM( const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple, 3, const_str_plain_numargs ); Py_INCREF( const_str_plain_numargs );
    PyTuple_SET_ITEM( const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple, 4, const_str_plain_eltType ); Py_INCREF( const_str_plain_eltType );
    const_tuple_str_plain_Para_int_pos_1_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Para_int_pos_1_tuple, 0, const_str_plain_Para ); Py_INCREF( const_str_plain_Para );
    PyTuple_SET_ITEM( const_tuple_str_plain_Para_int_pos_1_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandocfilters( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_bd05f4064d4a6a0d1fa7ab1c001893c6;
static PyCodeObject *codeobj_78b2c5c00f2c1d6935eadf75ebf50097;
static PyCodeObject *codeobj_2ace21b5f79b6395c93652b67400b9d7;
static PyCodeObject *codeobj_a9091592226457ed985ac7f3fe33f487;
static PyCodeObject *codeobj_fcdf362aea12a18c50571154483517b8;
static PyCodeObject *codeobj_38724a48fc9148b82430f41e71a062fa;
static PyCodeObject *codeobj_962b66337c3d5fc86e07218a95b46fa6;
static PyCodeObject *codeobj_1a332dd507d0cb6ab01b4c04db7622ce;
static PyCodeObject *codeobj_e3f09db8d4c929f2f1b9bf0b73f2aa38;
static PyCodeObject *codeobj_a5ec9d4f1c1212786c79053b3506070f;
static PyCodeObject *codeobj_021e39f3275eaecb297c28ed89eb0977;
static PyCodeObject *codeobj_f053323cec25fcaf778ba4918c970aa2;
static PyCodeObject *codeobj_dfc0a58bb7af92e212ba12ad21c11dbb;
static PyCodeObject *codeobj_cdbc59d74965b61d67a1dff28e347b6b;
static PyCodeObject *codeobj_55333e787b20ec5b5feaae48e24322b7;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_9e86034c6ef53ef2eec3c49e69fd5864 );
    codeobj_bd05f4064d4a6a0d1fa7ab1c001893c6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 233, const_tuple_str_plain_x_str_plain_attrs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_78b2c5c00f2c1d6935eadf75ebf50097 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_e03f4b9ec54eb6292fbf5f07893f0cda, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_2ace21b5f79b6395c93652b67400b9d7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_applyJSONFilters, 168, const_tuple_4dfd47d75ceb7e0660ef1f32caf0c816_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a9091592226457ed985ac7f3fe33f487 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_attributes, 226, const_tuple_8c76e66ef3e6e58fa4ee91ae9ae7c98b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fcdf362aea12a18c50571154483517b8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_elt, 237, const_tuple_str_plain_eltType_str_plain_numargs_str_plain_fun_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_38724a48fc9148b82430f41e71a062fa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fun, 238, const_tuple_0f3444d7c33341469a2e1a593d3ffc3e_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS );
    codeobj_962b66337c3d5fc86e07218a95b46fa6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_caption, 51, const_tuple_33141a372ee29fab46e99bdeb67759a1_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1a332dd507d0cb6ab01b4c04db7622ce = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_extension, 71, const_tuple_str_plain_format_str_plain_default_str_plain_alternates_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_e3f09db8d4c929f2f1b9bf0b73f2aa38 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_filename4code, 21, const_tuple_b710bb8489ab703046473c2e40479860_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a5ec9d4f1c1212786c79053b3506070f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_value, 41, const_tuple_507597a29c48ff3ebc2c6daa6eb0ac05_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_021e39f3275eaecb297c28ed89eb0977 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_go, 208, const_tuple_c690c777671328bbaed97bae8773aa7f_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_f053323cec25fcaf778ba4918c970aa2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_stringify, 202, const_tuple_str_plain_x_str_plain_result_str_plain_go_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dfc0a58bb7af92e212ba12ad21c11dbb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_toJSONFilter, 129, const_tuple_str_plain_action_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cdbc59d74965b61d67a1dff28e347b6b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_toJSONFilters, 135, const_tuple_c30d4d0c6e116a4e2fdd9ce0b478ff4b_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_55333e787b20ec5b5feaae48e24322b7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_walk, 85, const_tuple_0252737fdfceae69bea5906ec19d4030_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandocfilters$$$function_10_attributes(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_11_elt(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_11_elt$$$function_1_fun(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_1_get_filename4code( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_2_get_value( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_3_get_caption(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_4_get_extension(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_5_walk(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_6_toJSONFilter(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_7_toJSONFilters(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_8_applyJSONFilters( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_9_stringify(  );


static PyObject *MAKE_FUNCTION_pandocfilters$$$function_9_stringify$$$function_1_go(  );


// The module function definitions.
static PyObject *impl_pandocfilters$$$function_1_get_filename4code( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_module = python_pars[ 0 ];
    PyObject *par_content = python_pars[ 1 ];
    PyObject *par_ext = python_pars[ 2 ];
    PyObject *var_imagedir = NULL;
    PyObject *var_fn = NULL;
    struct Nuitka_FrameObject *frame_e3f09db8d4c929f2f1b9bf0b73f2aa38;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e3f09db8d4c929f2f1b9bf0b73f2aa38, codeobj_e3f09db8d4c929f2f1b9bf0b73f2aa38, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 = cache_frame_e3f09db8d4c929f2f1b9bf0b73f2aa38;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_module );
        tmp_left_name_1 = par_module;
        tmp_right_name_1 = const_str_digest_82706484618aa4cfb291f1f1599d7dda;
        tmp_assign_source_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_imagedir == NULL );
        var_imagedir = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_hashlib );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_hashlib );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "hashlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sha1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_content );
        tmp_source_name_2 = par_content;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_encode );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 31;
        tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_getfilesystemencoding );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 31;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 31;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 31;
        tmp_assign_source_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_hexdigest );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_fn == NULL );
        var_fn = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_3 = tmp_mvar_value_3;
        CHECK_OBJECT( var_imagedir );
        tmp_args_element_name_3 = var_imagedir;
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 33;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_mkdir, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_4 = tmp_mvar_value_4;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stderr );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_write );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_left_name_3 = const_str_digest_ea1e0497265d8883d491b27d8c3a1a02;
        CHECK_OBJECT( var_imagedir );
        tmp_right_name_2 = var_imagedir;
        tmp_left_name_2 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_3, tmp_right_name_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_right_name_3 = const_str_newline;
        tmp_args_element_name_4 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 34;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_e3f09db8d4c929f2f1b9bf0b73f2aa38, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_OSError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 32;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame) frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooo";
        goto try_except_handler_3;
        branch_no_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_1_get_filename4code );
    return NULL;
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_ext );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_ext );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            PyObject *tmp_left_name_5;
            PyObject *tmp_right_name_5;
            CHECK_OBJECT( var_fn );
            tmp_left_name_4 = var_fn;
            tmp_left_name_5 = const_str_dot;
            CHECK_OBJECT( par_ext );
            tmp_right_name_5 = par_ext;
            tmp_right_name_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_5, tmp_right_name_5 );
            if ( tmp_right_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = tmp_left_name_4;
            var_fn = tmp_assign_source_3;

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_5;
        tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_path );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_imagedir );
        tmp_args_element_name_5 = var_imagedir;
        CHECK_OBJECT( var_fn );
        tmp_args_element_name_6 = var_fn;
        frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_join, call_args );
        }

        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e3f09db8d4c929f2f1b9bf0b73f2aa38->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e3f09db8d4c929f2f1b9bf0b73f2aa38, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e3f09db8d4c929f2f1b9bf0b73f2aa38,
        type_description_1,
        par_module,
        par_content,
        par_ext,
        var_imagedir,
        var_fn
    );


    // Release cached frame.
    if ( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 == cache_frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 )
    {
        Py_DECREF( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 );
    }
    cache_frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 = NULL;

    assertFrameObject( frame_e3f09db8d4c929f2f1b9bf0b73f2aa38 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_1_get_filename4code );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_module );
    Py_DECREF( par_module );
    par_module = NULL;

    CHECK_OBJECT( (PyObject *)par_content );
    Py_DECREF( par_content );
    par_content = NULL;

    CHECK_OBJECT( (PyObject *)par_ext );
    Py_DECREF( par_ext );
    par_ext = NULL;

    CHECK_OBJECT( (PyObject *)var_imagedir );
    Py_DECREF( var_imagedir );
    var_imagedir = NULL;

    CHECK_OBJECT( (PyObject *)var_fn );
    Py_DECREF( var_fn );
    var_fn = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_module );
    Py_DECREF( par_module );
    par_module = NULL;

    CHECK_OBJECT( (PyObject *)par_content );
    Py_DECREF( par_content );
    par_content = NULL;

    CHECK_OBJECT( (PyObject *)par_ext );
    Py_DECREF( par_ext );
    par_ext = NULL;

    Py_XDECREF( var_imagedir );
    var_imagedir = NULL;

    Py_XDECREF( var_fn );
    var_fn = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_1_get_filename4code );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_2_get_value( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kv = python_pars[ 0 ];
    PyObject *par_key = python_pars[ 1 ];
    PyObject *par_value = python_pars[ 2 ];
    PyObject *var_res = NULL;
    PyObject *var_k = NULL;
    PyObject *var_v = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_a5ec9d4f1c1212786c79053b3506070f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a5ec9d4f1c1212786c79053b3506070f = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_res == NULL );
        var_res = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a5ec9d4f1c1212786c79053b3506070f, codeobj_a5ec9d4f1c1212786c79053b3506070f, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a5ec9d4f1c1212786c79053b3506070f = cache_frame_a5ec9d4f1c1212786c79053b3506070f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a5ec9d4f1c1212786c79053b3506070f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a5ec9d4f1c1212786c79053b3506070f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_kv );
        tmp_iter_arg_1 = par_kv;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooo";
                exception_lineno = 44;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_4 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 44;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooo";
            exception_lineno = 44;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooo";
                    exception_lineno = 44;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooo";
            exception_lineno = 44;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_k;
            var_k = tmp_assign_source_7;
            Py_INCREF( var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_8;
            Py_INCREF( var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_k );
        tmp_compexpr_left_1 = var_k;
        CHECK_OBJECT( par_key );
        tmp_compexpr_right_1 = par_key;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( var_v );
            tmp_assign_source_9 = var_v;
            {
                PyObject *old = par_value;
                par_value = tmp_assign_source_9;
                Py_INCREF( par_value );
                Py_XDECREF( old );
            }

        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_list_element_1;
            CHECK_OBJECT( var_res );
            tmp_called_instance_1 = var_res;
            CHECK_OBJECT( var_k );
            tmp_list_element_1 = var_k;
            tmp_args_element_name_1 = PyList_New( 2 );
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_1, 0, tmp_list_element_1 );
            CHECK_OBJECT( var_v );
            tmp_list_element_1 = var_v;
            Py_INCREF( tmp_list_element_1 );
            PyList_SET_ITEM( tmp_args_element_name_1, 1, tmp_list_element_1 );
            frame_a5ec9d4f1c1212786c79053b3506070f->m_frame.f_lineno = 48;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 44;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_tuple_element_1;
        if ( par_value == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "value" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 49;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = par_value;
        tmp_return_value = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_res );
        tmp_tuple_element_1 = var_res;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5ec9d4f1c1212786c79053b3506070f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5ec9d4f1c1212786c79053b3506070f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5ec9d4f1c1212786c79053b3506070f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a5ec9d4f1c1212786c79053b3506070f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a5ec9d4f1c1212786c79053b3506070f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a5ec9d4f1c1212786c79053b3506070f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a5ec9d4f1c1212786c79053b3506070f,
        type_description_1,
        par_kv,
        par_key,
        par_value,
        var_res,
        var_k,
        var_v
    );


    // Release cached frame.
    if ( frame_a5ec9d4f1c1212786c79053b3506070f == cache_frame_a5ec9d4f1c1212786c79053b3506070f )
    {
        Py_DECREF( frame_a5ec9d4f1c1212786c79053b3506070f );
    }
    cache_frame_a5ec9d4f1c1212786c79053b3506070f = NULL;

    assertFrameObject( frame_a5ec9d4f1c1212786c79053b3506070f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_2_get_value );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kv );
    Py_DECREF( par_kv );
    par_kv = NULL;

    CHECK_OBJECT( (PyObject *)par_key );
    Py_DECREF( par_key );
    par_key = NULL;

    Py_XDECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)var_res );
    Py_DECREF( var_res );
    var_res = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kv );
    Py_DECREF( par_kv );
    par_kv = NULL;

    CHECK_OBJECT( (PyObject *)par_key );
    Py_DECREF( par_key );
    par_key = NULL;

    Py_XDECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)var_res );
    Py_DECREF( var_res );
    var_res = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_2_get_value );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_3_get_caption( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_kv = python_pars[ 0 ];
    PyObject *var_caption = NULL;
    PyObject *var_typef = NULL;
    PyObject *var_value = NULL;
    PyObject *var_res = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_962b66337c3d5fc86e07218a95b46fa6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_962b66337c3d5fc86e07218a95b46fa6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_caption == NULL );
        var_caption = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = const_str_empty;
        assert( var_typef == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_typef = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_962b66337c3d5fc86e07218a95b46fa6, codeobj_962b66337c3d5fc86e07218a95b46fa6, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_962b66337c3d5fc86e07218a95b46fa6 = cache_frame_962b66337c3d5fc86e07218a95b46fa6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_962b66337c3d5fc86e07218a95b46fa6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_962b66337c3d5fc86e07218a95b46fa6 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_get_value );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_value );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_value" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_kv );
        tmp_args_element_name_1 = par_kv;
        tmp_args_element_name_2 = const_str_plain_caption;
        frame_962b66337c3d5fc86e07218a95b46fa6->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 63;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooo";
            exception_lineno = 63;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooo";
                    exception_lineno = 63;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooo";
            exception_lineno = 63;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        assert( var_value == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_value = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        assert( var_res == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_res = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_value );
        tmp_compexpr_left_1 = var_value;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_list_element_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Str );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Str );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Str" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 65;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( var_value );
            tmp_args_element_name_3 = var_value;
            frame_962b66337c3d5fc86e07218a95b46fa6->m_frame.f_lineno = 65;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_list_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 65;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_8 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_assign_source_8, 0, tmp_list_element_1 );
            {
                PyObject *old = var_caption;
                assert( old != NULL );
                var_caption = tmp_assign_source_8;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            tmp_assign_source_9 = const_str_digest_ff847e3cdb51ac69134ff0ad25002866;
            {
                PyObject *old = var_typef;
                assert( old != NULL );
                var_typef = tmp_assign_source_9;
                Py_INCREF( var_typef );
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_962b66337c3d5fc86e07218a95b46fa6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_962b66337c3d5fc86e07218a95b46fa6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_962b66337c3d5fc86e07218a95b46fa6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_962b66337c3d5fc86e07218a95b46fa6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_962b66337c3d5fc86e07218a95b46fa6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_962b66337c3d5fc86e07218a95b46fa6,
        type_description_1,
        par_kv,
        var_caption,
        var_typef,
        var_value,
        var_res
    );


    // Release cached frame.
    if ( frame_962b66337c3d5fc86e07218a95b46fa6 == cache_frame_962b66337c3d5fc86e07218a95b46fa6 )
    {
        Py_DECREF( frame_962b66337c3d5fc86e07218a95b46fa6 );
    }
    cache_frame_962b66337c3d5fc86e07218a95b46fa6 = NULL;

    assertFrameObject( frame_962b66337c3d5fc86e07218a95b46fa6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( var_caption );
        tmp_tuple_element_1 = var_caption;
        tmp_return_value = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_typef );
        tmp_tuple_element_1 = var_typef;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_res );
        tmp_tuple_element_1 = var_res;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_1 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_3_get_caption );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_kv );
    Py_DECREF( par_kv );
    par_kv = NULL;

    CHECK_OBJECT( (PyObject *)var_caption );
    Py_DECREF( var_caption );
    var_caption = NULL;

    CHECK_OBJECT( (PyObject *)var_typef );
    Py_DECREF( var_typef );
    var_typef = NULL;

    CHECK_OBJECT( (PyObject *)var_value );
    Py_DECREF( var_value );
    var_value = NULL;

    CHECK_OBJECT( (PyObject *)var_res );
    Py_DECREF( var_res );
    var_res = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_kv );
    Py_DECREF( par_kv );
    par_kv = NULL;

    CHECK_OBJECT( (PyObject *)var_caption );
    Py_DECREF( var_caption );
    var_caption = NULL;

    CHECK_OBJECT( (PyObject *)var_typef );
    Py_DECREF( var_typef );
    var_typef = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    Py_XDECREF( var_res );
    var_res = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_3_get_caption );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_4_get_extension( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_format = python_pars[ 0 ];
    PyObject *par_default = python_pars[ 1 ];
    PyObject *par_alternates = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1a332dd507d0cb6ab01b4c04db7622ce;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_1a332dd507d0cb6ab01b4c04db7622ce = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1a332dd507d0cb6ab01b4c04db7622ce, codeobj_1a332dd507d0cb6ab01b4c04db7622ce, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1a332dd507d0cb6ab01b4c04db7622ce = cache_frame_1a332dd507d0cb6ab01b4c04db7622ce;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1a332dd507d0cb6ab01b4c04db7622ce );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1a332dd507d0cb6ab01b4c04db7622ce ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_alternates );
        tmp_subscribed_name_1 = par_alternates;
        CHECK_OBJECT( par_format );
        tmp_subscript_name_1 = par_format;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_4_get_extension );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_1a332dd507d0cb6ab01b4c04db7622ce, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_1a332dd507d0cb6ab01b4c04db7622ce, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_default );
        tmp_return_value = par_default;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_3;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 77;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_1a332dd507d0cb6ab01b4c04db7622ce->m_frame) frame_1a332dd507d0cb6ab01b4c04db7622ce->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_4_get_extension );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1a332dd507d0cb6ab01b4c04db7622ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1a332dd507d0cb6ab01b4c04db7622ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1a332dd507d0cb6ab01b4c04db7622ce );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1a332dd507d0cb6ab01b4c04db7622ce, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1a332dd507d0cb6ab01b4c04db7622ce->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1a332dd507d0cb6ab01b4c04db7622ce, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1a332dd507d0cb6ab01b4c04db7622ce,
        type_description_1,
        par_format,
        par_default,
        par_alternates
    );


    // Release cached frame.
    if ( frame_1a332dd507d0cb6ab01b4c04db7622ce == cache_frame_1a332dd507d0cb6ab01b4c04db7622ce )
    {
        Py_DECREF( frame_1a332dd507d0cb6ab01b4c04db7622ce );
    }
    cache_frame_1a332dd507d0cb6ab01b4c04db7622ce = NULL;

    assertFrameObject( frame_1a332dd507d0cb6ab01b4c04db7622ce );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_4_get_extension );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    CHECK_OBJECT( (PyObject *)par_alternates );
    Py_DECREF( par_alternates );
    par_alternates = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    CHECK_OBJECT( (PyObject *)par_alternates );
    Py_DECREF( par_alternates );
    par_alternates = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_4_get_extension );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_5_walk( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *par_action = python_pars[ 1 ];
    PyObject *par_format = python_pars[ 2 ];
    PyObject *par_meta = python_pars[ 3 ];
    PyObject *var_array = NULL;
    PyObject *var_item = NULL;
    PyObject *var_res = NULL;
    PyObject *var_z = NULL;
    PyObject *var_k = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    struct Nuitka_FrameObject *frame_55333e787b20ec5b5feaae48e24322b7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_55333e787b20ec5b5feaae48e24322b7 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_55333e787b20ec5b5feaae48e24322b7, codeobj_55333e787b20ec5b5feaae48e24322b7, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_55333e787b20ec5b5feaae48e24322b7 = cache_frame_55333e787b20ec5b5feaae48e24322b7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_55333e787b20ec5b5feaae48e24322b7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_55333e787b20ec5b5feaae48e24322b7 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_x );
        tmp_isinstance_inst_1 = par_x;
        tmp_isinstance_cls_1 = (PyObject *)&PyList_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = PyList_New( 0 );
            assert( var_array == NULL );
            var_array = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_x );
            tmp_iter_arg_1 = par_x;
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 108;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_2;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_3;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooo";
                    exception_lineno = 108;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_3;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_4 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_item;
                var_item = tmp_assign_source_4;
                Py_INCREF( var_item );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_item );
            tmp_isinstance_inst_2 = var_item;
            tmp_isinstance_cls_2 = (PyObject *)&PyDict_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_1 = "ooooooooo";
                goto try_except_handler_2;
            }
            tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            tmp_compexpr_left_1 = const_str_plain_t;
            CHECK_OBJECT( var_item );
            tmp_compexpr_right_1 = var_item;
            tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_1 = "ooooooooo";
                goto try_except_handler_2;
            }
            tmp_and_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_2 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_called_name_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_args_element_name_2;
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_args_element_name_4;
                CHECK_OBJECT( par_action );
                tmp_called_name_1 = par_action;
                CHECK_OBJECT( var_item );
                tmp_subscribed_name_1 = var_item;
                tmp_subscript_name_1 = const_str_plain_t;
                tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                if ( tmp_args_element_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 110;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                tmp_compexpr_left_2 = const_str_plain_c;
                CHECK_OBJECT( var_item );
                tmp_compexpr_right_2 = var_item;
                tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_args_element_name_1 );

                    exception_lineno = 111;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                CHECK_OBJECT( var_item );
                tmp_subscribed_name_2 = var_item;
                tmp_subscript_name_2 = const_str_plain_c;
                tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_args_element_name_1 );

                    exception_lineno = 111;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                goto condexpr_end_1;
                condexpr_false_1:;
                tmp_args_element_name_2 = Py_None;
                Py_INCREF( tmp_args_element_name_2 );
                condexpr_end_1:;
                CHECK_OBJECT( par_format );
                tmp_args_element_name_3 = par_format;
                CHECK_OBJECT( par_meta );
                tmp_args_element_name_4 = par_meta;
                frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 110;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_1 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 110;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_res;
                    var_res = tmp_assign_source_5;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( var_res );
                tmp_compexpr_left_3 = var_res;
                tmp_compexpr_right_3 = Py_None;
                tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_1;
                    PyObject *tmp_call_result_1;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_1;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_args_element_name_9;
                    CHECK_OBJECT( var_array );
                    tmp_source_name_1 = var_array;
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 113;
                        type_description_1 = "ooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

                    if (unlikely( tmp_mvar_value_1 == NULL ))
                    {
                        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
                    }

                    if ( tmp_mvar_value_1 == NULL )
                    {
                        Py_DECREF( tmp_called_name_2 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 113;
                        type_description_1 = "ooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_called_name_3 = tmp_mvar_value_1;
                    CHECK_OBJECT( var_item );
                    tmp_args_element_name_6 = var_item;
                    CHECK_OBJECT( par_action );
                    tmp_args_element_name_7 = par_action;
                    CHECK_OBJECT( par_format );
                    tmp_args_element_name_8 = par_format;
                    CHECK_OBJECT( par_meta );
                    tmp_args_element_name_9 = par_meta;
                    frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 113;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                        tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_3, call_args );
                    }

                    if ( tmp_args_element_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 113;
                        type_description_1 = "ooooooooo";
                        goto try_except_handler_2;
                    }
                    frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 113;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_5 };
                        tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_element_name_5 );
                    if ( tmp_call_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 113;
                        type_description_1 = "ooooooooo";
                        goto try_except_handler_2;
                    }
                    Py_DECREF( tmp_call_result_1 );
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_isinstance_inst_3;
                    PyObject *tmp_isinstance_cls_3;
                    CHECK_OBJECT( var_res );
                    tmp_isinstance_inst_3 = var_res;
                    tmp_isinstance_cls_3 = (PyObject *)&PyList_Type;
                    tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_3, tmp_isinstance_cls_3 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 114;
                        type_description_1 = "ooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_assign_source_6;
                        PyObject *tmp_iter_arg_2;
                        CHECK_OBJECT( var_res );
                        tmp_iter_arg_2 = var_res;
                        tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
                        if ( tmp_assign_source_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 115;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_2;
                        }
                        {
                            PyObject *old = tmp_for_loop_2__for_iterator;
                            tmp_for_loop_2__for_iterator = tmp_assign_source_6;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    loop_start_2:;
                    {
                        PyObject *tmp_next_source_2;
                        PyObject *tmp_assign_source_7;
                        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                        tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_2 );
                        if ( tmp_assign_source_7 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_2;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "ooooooooo";
                                exception_lineno = 115;
                                goto try_except_handler_3;
                            }
                        }

                        {
                            PyObject *old = tmp_for_loop_2__iter_value;
                            tmp_for_loop_2__iter_value = tmp_assign_source_7;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_8;
                        CHECK_OBJECT( tmp_for_loop_2__iter_value );
                        tmp_assign_source_8 = tmp_for_loop_2__iter_value;
                        {
                            PyObject *old = var_z;
                            var_z = tmp_assign_source_8;
                            Py_INCREF( var_z );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_source_name_2;
                        PyObject *tmp_call_result_2;
                        PyObject *tmp_args_element_name_10;
                        PyObject *tmp_called_name_5;
                        PyObject *tmp_mvar_value_2;
                        PyObject *tmp_args_element_name_11;
                        PyObject *tmp_args_element_name_12;
                        PyObject *tmp_args_element_name_13;
                        PyObject *tmp_args_element_name_14;
                        CHECK_OBJECT( var_array );
                        tmp_source_name_2 = var_array;
                        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
                        if ( tmp_called_name_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 116;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

                        if (unlikely( tmp_mvar_value_2 == NULL ))
                        {
                            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
                        }

                        if ( tmp_mvar_value_2 == NULL )
                        {
                            Py_DECREF( tmp_called_name_4 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 116;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_3;
                        }

                        tmp_called_name_5 = tmp_mvar_value_2;
                        CHECK_OBJECT( var_z );
                        tmp_args_element_name_11 = var_z;
                        CHECK_OBJECT( par_action );
                        tmp_args_element_name_12 = par_action;
                        CHECK_OBJECT( par_format );
                        tmp_args_element_name_13 = par_format;
                        CHECK_OBJECT( par_meta );
                        tmp_args_element_name_14 = par_meta;
                        frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 116;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
                            tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_5, call_args );
                        }

                        if ( tmp_args_element_name_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_4 );

                            exception_lineno = 116;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_3;
                        }
                        frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 116;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_10 };
                            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                        }

                        Py_DECREF( tmp_called_name_4 );
                        Py_DECREF( tmp_args_element_name_10 );
                        if ( tmp_call_result_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 116;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_3;
                        }
                        Py_DECREF( tmp_call_result_2 );
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 115;
                        type_description_1 = "ooooooooo";
                        goto try_except_handler_3;
                    }
                    goto loop_start_2;
                    loop_end_2:;
                    goto try_end_1;
                    // Exception handler code:
                    try_except_handler_3:;
                    exception_keeper_type_1 = exception_type;
                    exception_keeper_value_1 = exception_value;
                    exception_keeper_tb_1 = exception_tb;
                    exception_keeper_lineno_1 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_for_loop_2__iter_value );
                    tmp_for_loop_2__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                    Py_DECREF( tmp_for_loop_2__for_iterator );
                    tmp_for_loop_2__for_iterator = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_1;
                    exception_value = exception_keeper_value_1;
                    exception_tb = exception_keeper_tb_1;
                    exception_lineno = exception_keeper_lineno_1;

                    goto try_except_handler_2;
                    // End of try:
                    try_end_1:;
                    Py_XDECREF( tmp_for_loop_2__iter_value );
                    tmp_for_loop_2__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                    Py_DECREF( tmp_for_loop_2__for_iterator );
                    tmp_for_loop_2__for_iterator = NULL;

                    goto branch_end_4;
                    branch_no_4:;
                    {
                        PyObject *tmp_called_name_6;
                        PyObject *tmp_source_name_3;
                        PyObject *tmp_call_result_3;
                        PyObject *tmp_args_element_name_15;
                        PyObject *tmp_called_name_7;
                        PyObject *tmp_mvar_value_3;
                        PyObject *tmp_args_element_name_16;
                        PyObject *tmp_args_element_name_17;
                        PyObject *tmp_args_element_name_18;
                        PyObject *tmp_args_element_name_19;
                        CHECK_OBJECT( var_array );
                        tmp_source_name_3 = var_array;
                        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_append );
                        if ( tmp_called_name_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 118;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

                        if (unlikely( tmp_mvar_value_3 == NULL ))
                        {
                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
                        }

                        if ( tmp_mvar_value_3 == NULL )
                        {
                            Py_DECREF( tmp_called_name_6 );
                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 118;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_called_name_7 = tmp_mvar_value_3;
                        CHECK_OBJECT( var_res );
                        tmp_args_element_name_16 = var_res;
                        CHECK_OBJECT( par_action );
                        tmp_args_element_name_17 = par_action;
                        CHECK_OBJECT( par_format );
                        tmp_args_element_name_18 = par_format;
                        CHECK_OBJECT( par_meta );
                        tmp_args_element_name_19 = par_meta;
                        frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 118;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_16, tmp_args_element_name_17, tmp_args_element_name_18, tmp_args_element_name_19 };
                            tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_7, call_args );
                        }

                        if ( tmp_args_element_name_15 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_6 );

                            exception_lineno = 118;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_2;
                        }
                        frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 118;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_15 };
                            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                        }

                        Py_DECREF( tmp_called_name_6 );
                        Py_DECREF( tmp_args_element_name_15 );
                        if ( tmp_call_result_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 118;
                            type_description_1 = "ooooooooo";
                            goto try_except_handler_2;
                        }
                        Py_DECREF( tmp_call_result_3 );
                    }
                    branch_end_4:;
                }
                branch_end_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_called_name_8;
                PyObject *tmp_source_name_4;
                PyObject *tmp_call_result_4;
                PyObject *tmp_args_element_name_20;
                PyObject *tmp_called_name_9;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_21;
                PyObject *tmp_args_element_name_22;
                PyObject *tmp_args_element_name_23;
                PyObject *tmp_args_element_name_24;
                CHECK_OBJECT( var_array );
                tmp_source_name_4 = var_array;
                tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
                if ( tmp_called_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 120;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
                }

                if ( tmp_mvar_value_4 == NULL )
                {
                    Py_DECREF( tmp_called_name_8 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 120;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }

                tmp_called_name_9 = tmp_mvar_value_4;
                CHECK_OBJECT( var_item );
                tmp_args_element_name_21 = var_item;
                CHECK_OBJECT( par_action );
                tmp_args_element_name_22 = par_action;
                CHECK_OBJECT( par_format );
                tmp_args_element_name_23 = par_format;
                CHECK_OBJECT( par_meta );
                tmp_args_element_name_24 = par_meta;
                frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 120;
                {
                    PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22, tmp_args_element_name_23, tmp_args_element_name_24 };
                    tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_9, call_args );
                }

                if ( tmp_args_element_name_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_8 );

                    exception_lineno = 120;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 120;
                {
                    PyObject *call_args[] = { tmp_args_element_name_20 };
                    tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
                }

                Py_DECREF( tmp_called_name_8 );
                Py_DECREF( tmp_args_element_name_20 );
                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 120;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            branch_end_2:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "ooooooooo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        CHECK_OBJECT( var_array );
        tmp_return_value = var_array;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_isinstance_inst_4;
            PyObject *tmp_isinstance_cls_4;
            CHECK_OBJECT( par_x );
            tmp_isinstance_inst_4 = par_x;
            tmp_isinstance_cls_4 = (PyObject *)&PyDict_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_4, tmp_isinstance_cls_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_iter_arg_3;
                CHECK_OBJECT( par_x );
                tmp_iter_arg_3 = par_x;
                tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_3 );
                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 123;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_for_loop_3__for_iterator == NULL );
                tmp_for_loop_3__for_iterator = tmp_assign_source_9;
            }
            // Tried code:
            loop_start_3:;
            {
                PyObject *tmp_next_source_3;
                PyObject *tmp_assign_source_10;
                CHECK_OBJECT( tmp_for_loop_3__for_iterator );
                tmp_next_source_3 = tmp_for_loop_3__for_iterator;
                tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_3 );
                if ( tmp_assign_source_10 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_3;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooooooo";
                        exception_lineno = 123;
                        goto try_except_handler_4;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_3__iter_value;
                    tmp_for_loop_3__iter_value = tmp_assign_source_10;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( tmp_for_loop_3__iter_value );
                tmp_assign_source_11 = tmp_for_loop_3__iter_value;
                {
                    PyObject *old = var_k;
                    var_k = tmp_assign_source_11;
                    Py_INCREF( var_k );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_called_name_10;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_25;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_args_element_name_26;
                PyObject *tmp_args_element_name_27;
                PyObject *tmp_args_element_name_28;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_ass_subscript_1;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 124;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_4;
                }

                tmp_called_name_10 = tmp_mvar_value_5;
                CHECK_OBJECT( par_x );
                tmp_subscribed_name_3 = par_x;
                CHECK_OBJECT( var_k );
                tmp_subscript_name_3 = var_k;
                tmp_args_element_name_25 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                if ( tmp_args_element_name_25 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_4;
                }
                CHECK_OBJECT( par_action );
                tmp_args_element_name_26 = par_action;
                CHECK_OBJECT( par_format );
                tmp_args_element_name_27 = par_format;
                CHECK_OBJECT( par_meta );
                tmp_args_element_name_28 = par_meta;
                frame_55333e787b20ec5b5feaae48e24322b7->m_frame.f_lineno = 124;
                {
                    PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26, tmp_args_element_name_27, tmp_args_element_name_28 };
                    tmp_ass_subvalue_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_10, call_args );
                }

                Py_DECREF( tmp_args_element_name_25 );
                if ( tmp_ass_subvalue_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_4;
                }
                CHECK_OBJECT( par_x );
                tmp_ass_subscribed_1 = par_x;
                CHECK_OBJECT( var_k );
                tmp_ass_subscript_1 = var_k;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                Py_DECREF( tmp_ass_subvalue_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "ooooooooo";
                    goto try_except_handler_4;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 123;
                type_description_1 = "ooooooooo";
                goto try_except_handler_4;
            }
            goto loop_start_3;
            loop_end_3:;
            goto try_end_3;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_3__iter_value );
            tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
            Py_DECREF( tmp_for_loop_3__for_iterator );
            tmp_for_loop_3__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_1;
            // End of try:
            try_end_3:;
            Py_XDECREF( tmp_for_loop_3__iter_value );
            tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
            Py_DECREF( tmp_for_loop_3__for_iterator );
            tmp_for_loop_3__for_iterator = NULL;

            CHECK_OBJECT( par_x );
            tmp_return_value = par_x;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_5;
            branch_no_5:;
            CHECK_OBJECT( par_x );
            tmp_return_value = par_x;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_end_5:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_55333e787b20ec5b5feaae48e24322b7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_55333e787b20ec5b5feaae48e24322b7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_55333e787b20ec5b5feaae48e24322b7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_55333e787b20ec5b5feaae48e24322b7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_55333e787b20ec5b5feaae48e24322b7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_55333e787b20ec5b5feaae48e24322b7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_55333e787b20ec5b5feaae48e24322b7,
        type_description_1,
        par_x,
        par_action,
        par_format,
        par_meta,
        var_array,
        var_item,
        var_res,
        var_z,
        var_k
    );


    // Release cached frame.
    if ( frame_55333e787b20ec5b5feaae48e24322b7 == cache_frame_55333e787b20ec5b5feaae48e24322b7 )
    {
        Py_DECREF( frame_55333e787b20ec5b5feaae48e24322b7 );
    }
    cache_frame_55333e787b20ec5b5feaae48e24322b7 = NULL;

    assertFrameObject( frame_55333e787b20ec5b5feaae48e24322b7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_5_walk );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)par_action );
    Py_DECREF( par_action );
    par_action = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    Py_XDECREF( var_array );
    var_array = NULL;

    Py_XDECREF( var_item );
    var_item = NULL;

    Py_XDECREF( var_res );
    var_res = NULL;

    Py_XDECREF( var_z );
    var_z = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)par_action );
    Py_DECREF( par_action );
    par_action = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    Py_XDECREF( var_array );
    var_array = NULL;

    Py_XDECREF( var_item );
    var_item = NULL;

    Py_XDECREF( var_res );
    var_res = NULL;

    Py_XDECREF( var_z );
    var_z = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_5_walk );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_6_toJSONFilter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_action = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_dfc0a58bb7af92e212ba12ad21c11dbb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dfc0a58bb7af92e212ba12ad21c11dbb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dfc0a58bb7af92e212ba12ad21c11dbb, codeobj_dfc0a58bb7af92e212ba12ad21c11dbb, module_pandocfilters, sizeof(void *) );
    frame_dfc0a58bb7af92e212ba12ad21c11dbb = cache_frame_dfc0a58bb7af92e212ba12ad21c11dbb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dfc0a58bb7af92e212ba12ad21c11dbb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dfc0a58bb7af92e212ba12ad21c11dbb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_list_element_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_toJSONFilters );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_toJSONFilters );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "toJSONFilters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_action );
        tmp_list_element_1 = par_action;
        tmp_args_element_name_1 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_1, 0, tmp_list_element_1 );
        frame_dfc0a58bb7af92e212ba12ad21c11dbb->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfc0a58bb7af92e212ba12ad21c11dbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dfc0a58bb7af92e212ba12ad21c11dbb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dfc0a58bb7af92e212ba12ad21c11dbb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dfc0a58bb7af92e212ba12ad21c11dbb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dfc0a58bb7af92e212ba12ad21c11dbb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dfc0a58bb7af92e212ba12ad21c11dbb,
        type_description_1,
        par_action
    );


    // Release cached frame.
    if ( frame_dfc0a58bb7af92e212ba12ad21c11dbb == cache_frame_dfc0a58bb7af92e212ba12ad21c11dbb )
    {
        Py_DECREF( frame_dfc0a58bb7af92e212ba12ad21c11dbb );
    }
    cache_frame_dfc0a58bb7af92e212ba12ad21c11dbb = NULL;

    assertFrameObject( frame_dfc0a58bb7af92e212ba12ad21c11dbb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_6_toJSONFilter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_action );
    Py_DECREF( par_action );
    par_action = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_action );
    Py_DECREF( par_action );
    par_action = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_6_toJSONFilter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_7_toJSONFilters( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_actions = python_pars[ 0 ];
    PyObject *var_input_stream = NULL;
    PyObject *var_source = NULL;
    PyObject *var_format = NULL;
    struct Nuitka_FrameObject *frame_cdbc59d74965b61d67a1dff28e347b6b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_cdbc59d74965b61d67a1dff28e347b6b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cdbc59d74965b61d67a1dff28e347b6b, codeobj_cdbc59d74965b61d67a1dff28e347b6b, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cdbc59d74965b61d67a1dff28e347b6b = cache_frame_cdbc59d74965b61d67a1dff28e347b6b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cdbc59d74965b61d67a1dff28e347b6b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cdbc59d74965b61d67a1dff28e347b6b ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_io );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_io );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "io" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_TextIOWrapper );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_source_name_3 = tmp_mvar_value_2;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_stdin );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 154;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_buffer );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 154;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_81b3970727674c20ce12b1a4757dad21 );
        frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = 154;
        tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( var_input_stream == NULL );
        var_input_stream = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_cdbc59d74965b61d67a1dff28e347b6b, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_cdbc59d74965b61d67a1dff28e347b6b, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_codecs );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_codecs );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "codecs" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 158;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }

            tmp_called_instance_1 = tmp_mvar_value_3;
            frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = 158;
            tmp_called_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_getreader, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 158;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 158;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }

            tmp_source_name_4 = tmp_mvar_value_4;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stdin );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 158;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = 158;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 158;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            assert( var_input_stream == NULL );
            var_input_stream = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 153;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame) frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_7_toJSONFilters );
    return NULL;
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        if ( var_input_stream == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "input_stream" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = var_input_stream;
        frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = 160;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_read );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_source == NULL );
        var_source = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 161;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_5;
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_argv );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_subscript_name_1;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 162;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_6 = tmp_mvar_value_6;
            tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_argv );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_subscript_name_1 = const_int_pos_1;
            tmp_assign_source_4 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_format == NULL );
            var_format = tmp_assign_source_4;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = const_str_empty;
            assert( var_format == NULL );
            Py_INCREF( tmp_assign_source_5 );
            var_format = tmp_assign_source_5;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_7;
        tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_stdout );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_write );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_applyJSONFilters );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_applyJSONFilters );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "applyJSONFilters" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_8;
        CHECK_OBJECT( par_actions );
        tmp_args_element_name_3 = par_actions;
        CHECK_OBJECT( var_source );
        tmp_args_element_name_4 = var_source;
        CHECK_OBJECT( var_format );
        tmp_args_element_name_5 = var_format;
        frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_3 );

            exception_lineno = 166;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cdbc59d74965b61d67a1dff28e347b6b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cdbc59d74965b61d67a1dff28e347b6b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cdbc59d74965b61d67a1dff28e347b6b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cdbc59d74965b61d67a1dff28e347b6b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cdbc59d74965b61d67a1dff28e347b6b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cdbc59d74965b61d67a1dff28e347b6b,
        type_description_1,
        par_actions,
        var_input_stream,
        var_source,
        var_format
    );


    // Release cached frame.
    if ( frame_cdbc59d74965b61d67a1dff28e347b6b == cache_frame_cdbc59d74965b61d67a1dff28e347b6b )
    {
        Py_DECREF( frame_cdbc59d74965b61d67a1dff28e347b6b );
    }
    cache_frame_cdbc59d74965b61d67a1dff28e347b6b = NULL;

    assertFrameObject( frame_cdbc59d74965b61d67a1dff28e347b6b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_7_toJSONFilters );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_actions );
    Py_DECREF( par_actions );
    par_actions = NULL;

    Py_XDECREF( var_input_stream );
    var_input_stream = NULL;

    CHECK_OBJECT( (PyObject *)var_source );
    Py_DECREF( var_source );
    var_source = NULL;

    CHECK_OBJECT( (PyObject *)var_format );
    Py_DECREF( var_format );
    var_format = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_actions );
    Py_DECREF( par_actions );
    par_actions = NULL;

    Py_XDECREF( var_input_stream );
    var_input_stream = NULL;

    Py_XDECREF( var_source );
    var_source = NULL;

    Py_XDECREF( var_format );
    var_format = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_7_toJSONFilters );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_8_applyJSONFilters( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_actions = python_pars[ 0 ];
    PyObject *par_source = python_pars[ 1 ];
    PyObject *par_format = python_pars[ 2 ];
    PyObject *var_doc = NULL;
    PyObject *var_meta = NULL;
    PyObject *var_altered = NULL;
    PyObject *var_action = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_2ace21b5f79b6395c93652b67400b9d7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_2ace21b5f79b6395c93652b67400b9d7 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ace21b5f79b6395c93652b67400b9d7, codeobj_2ace21b5f79b6395c93652b67400b9d7, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2ace21b5f79b6395c93652b67400b9d7 = cache_frame_2ace21b5f79b6395c93652b67400b9d7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ace21b5f79b6395c93652b67400b9d7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ace21b5f79b6395c93652b67400b9d7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_source );
        tmp_args_element_name_1 = par_source;
        frame_2ace21b5f79b6395c93652b67400b9d7->m_frame.f_lineno = 187;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_loads, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_doc == NULL );
        var_doc = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = const_str_plain_meta;
        CHECK_OBJECT( var_doc );
        tmp_compexpr_right_1 = var_doc;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_doc );
            tmp_subscribed_name_1 = var_doc;
            tmp_subscript_name_1 = const_str_plain_meta;
            tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_meta == NULL );
            var_meta = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_subscript_result_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_doc );
            tmp_subscribed_name_2 = var_doc;
            tmp_subscript_name_2 = const_int_0;
            tmp_subscript_result_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
            if ( tmp_subscript_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_subscript_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_subscript_result_1 );

                exception_lineno = 191;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_subscript_result_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_subscript_name_4;
                CHECK_OBJECT( var_doc );
                tmp_subscribed_name_4 = var_doc;
                tmp_subscript_name_3 = const_int_0;
                tmp_subscribed_name_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_3, 0 );
                if ( tmp_subscribed_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_4 = const_str_plain_unMeta;
                tmp_assign_source_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_4 );
                Py_DECREF( tmp_subscribed_name_3 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_meta == NULL );
                var_meta = tmp_assign_source_3;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assign_source_4;
                tmp_assign_source_4 = PyDict_New();
                assert( var_meta == NULL );
                var_meta = tmp_assign_source_4;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( var_doc );
        tmp_assign_source_5 = var_doc;
        assert( var_altered == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_altered = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_actions );
        tmp_iter_arg_1 = par_actions;
        tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_6;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 196;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_8 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_action;
            var_action = tmp_assign_source_8;
            Py_INCREF( var_action );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( var_altered );
        tmp_args_element_name_2 = var_altered;
        CHECK_OBJECT( var_action );
        tmp_args_element_name_3 = var_action;
        CHECK_OBJECT( par_format );
        tmp_args_element_name_4 = par_format;
        if ( var_meta == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "meta" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }

        tmp_args_element_name_5 = var_meta;
        frame_2ace21b5f79b6395c93652b67400b9d7->m_frame.f_lineno = 197;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_altered;
            assert( old != NULL );
            var_altered = tmp_assign_source_9;
            Py_DECREF( old );
        }

    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 196;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_3;
        CHECK_OBJECT( var_altered );
        tmp_args_element_name_6 = var_altered;
        frame_2ace21b5f79b6395c93652b67400b9d7->m_frame.f_lineno = 199;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_dumps, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ace21b5f79b6395c93652b67400b9d7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ace21b5f79b6395c93652b67400b9d7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ace21b5f79b6395c93652b67400b9d7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ace21b5f79b6395c93652b67400b9d7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ace21b5f79b6395c93652b67400b9d7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ace21b5f79b6395c93652b67400b9d7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ace21b5f79b6395c93652b67400b9d7,
        type_description_1,
        par_actions,
        par_source,
        par_format,
        var_doc,
        var_meta,
        var_altered,
        var_action
    );


    // Release cached frame.
    if ( frame_2ace21b5f79b6395c93652b67400b9d7 == cache_frame_2ace21b5f79b6395c93652b67400b9d7 )
    {
        Py_DECREF( frame_2ace21b5f79b6395c93652b67400b9d7 );
    }
    cache_frame_2ace21b5f79b6395c93652b67400b9d7 = NULL;

    assertFrameObject( frame_2ace21b5f79b6395c93652b67400b9d7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_8_applyJSONFilters );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_actions );
    Py_DECREF( par_actions );
    par_actions = NULL;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)var_doc );
    Py_DECREF( var_doc );
    var_doc = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    CHECK_OBJECT( (PyObject *)var_altered );
    Py_DECREF( var_altered );
    var_altered = NULL;

    Py_XDECREF( var_action );
    var_action = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_actions );
    Py_DECREF( par_actions );
    par_actions = NULL;

    CHECK_OBJECT( (PyObject *)par_source );
    Py_DECREF( par_source );
    par_source = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    Py_XDECREF( var_doc );
    var_doc = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    Py_XDECREF( var_altered );
    var_altered = NULL;

    Py_XDECREF( var_action );
    var_action = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_8_applyJSONFilters );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_9_stringify( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_CellObject *var_result = PyCell_EMPTY();
    PyObject *var_go = NULL;
    struct Nuitka_FrameObject *frame_f053323cec25fcaf778ba4918c970aa2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f053323cec25fcaf778ba4918c970aa2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( PyCell_GET( var_result ) == NULL );
        PyCell_SET( var_result, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = MAKE_FUNCTION_pandocfilters$$$function_9_stringify$$$function_1_go(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = var_result;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );


        assert( var_go == NULL );
        var_go = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f053323cec25fcaf778ba4918c970aa2, codeobj_f053323cec25fcaf778ba4918c970aa2, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f053323cec25fcaf778ba4918c970aa2 = cache_frame_f053323cec25fcaf778ba4918c970aa2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f053323cec25fcaf778ba4918c970aa2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f053323cec25fcaf778ba4918c970aa2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_walk );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "walk" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 222;
            type_description_1 = "oco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_x );
        tmp_args_element_name_1 = par_x;
        CHECK_OBJECT( var_go );
        tmp_args_element_name_2 = var_go;
        tmp_args_element_name_3 = const_str_empty;
        tmp_args_element_name_4 = PyDict_New();
        frame_f053323cec25fcaf778ba4918c970aa2->m_frame.f_lineno = 222;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "oco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_5;
        tmp_called_instance_1 = const_str_empty;
        CHECK_OBJECT( PyCell_GET( var_result ) );
        tmp_args_element_name_5 = PyCell_GET( var_result );
        frame_f053323cec25fcaf778ba4918c970aa2->m_frame.f_lineno = 223;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f053323cec25fcaf778ba4918c970aa2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f053323cec25fcaf778ba4918c970aa2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f053323cec25fcaf778ba4918c970aa2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f053323cec25fcaf778ba4918c970aa2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f053323cec25fcaf778ba4918c970aa2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f053323cec25fcaf778ba4918c970aa2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f053323cec25fcaf778ba4918c970aa2,
        type_description_1,
        par_x,
        var_result,
        var_go
    );


    // Release cached frame.
    if ( frame_f053323cec25fcaf778ba4918c970aa2 == cache_frame_f053323cec25fcaf778ba4918c970aa2 )
    {
        Py_DECREF( frame_f053323cec25fcaf778ba4918c970aa2 );
    }
    cache_frame_f053323cec25fcaf778ba4918c970aa2 = NULL;

    assertFrameObject( frame_f053323cec25fcaf778ba4918c970aa2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_9_stringify );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    CHECK_OBJECT( (PyObject *)var_go );
    Py_DECREF( var_go );
    var_go = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    CHECK_OBJECT( (PyObject *)var_go );
    Py_DECREF( var_go );
    var_go = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_9_stringify );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_9_stringify$$$function_1_go( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_key = python_pars[ 0 ];
    PyObject *par_val = python_pars[ 1 ];
    PyObject *par_format = python_pars[ 2 ];
    PyObject *par_meta = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_021e39f3275eaecb297c28ed89eb0977;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_021e39f3275eaecb297c28ed89eb0977 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_021e39f3275eaecb297c28ed89eb0977, codeobj_021e39f3275eaecb297c28ed89eb0977, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_021e39f3275eaecb297c28ed89eb0977 = cache_frame_021e39f3275eaecb297c28ed89eb0977;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_021e39f3275eaecb297c28ed89eb0977 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_021e39f3275eaecb297c28ed89eb0977 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_key );
        tmp_compexpr_left_1 = par_key;
        tmp_compexpr_right_1 = LIST_COPY( const_list_str_plain_Str_str_plain_MetaString_list );
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;
            type_description_1 = "ooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "result" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 210;
                type_description_1 = "ooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
            CHECK_OBJECT( par_val );
            tmp_args_element_name_1 = par_val;
            frame_021e39f3275eaecb297c28ed89eb0977->m_frame.f_lineno = 210;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "ooooc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_key );
            tmp_compexpr_left_2 = par_key;
            tmp_compexpr_right_2 = const_str_plain_Code;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "ooooc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_1;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                if ( PyCell_GET( self->m_closure[0] ) == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "result" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 212;
                    type_description_1 = "ooooc";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 212;
                    type_description_1 = "ooooc";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_val );
                tmp_subscribed_name_1 = par_val;
                tmp_subscript_name_1 = const_int_pos_1;
                tmp_args_element_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_1 );

                    exception_lineno = 212;
                    type_description_1 = "ooooc";
                    goto frame_exception_exit_1;
                }
                frame_021e39f3275eaecb297c28ed89eb0977->m_frame.f_lineno = 212;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 212;
                    type_description_1 = "ooooc";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( par_key );
                tmp_compexpr_left_3 = par_key;
                tmp_compexpr_right_3 = const_str_plain_Math;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 213;
                    type_description_1 = "ooooc";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_2;
                    PyObject *tmp_call_result_3;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_subscribed_name_2;
                    PyObject *tmp_subscript_name_2;
                    if ( PyCell_GET( self->m_closure[0] ) == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "result" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 214;
                        type_description_1 = "ooooc";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_2 = PyCell_GET( self->m_closure[0] );
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 214;
                        type_description_1 = "ooooc";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_val );
                    tmp_subscribed_name_2 = par_val;
                    tmp_subscript_name_2 = const_int_pos_1;
                    tmp_args_element_name_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
                    if ( tmp_args_element_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 214;
                        type_description_1 = "ooooc";
                        goto frame_exception_exit_1;
                    }
                    frame_021e39f3275eaecb297c28ed89eb0977->m_frame.f_lineno = 214;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3 };
                        tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_element_name_3 );
                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 214;
                        type_description_1 = "ooooc";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    CHECK_OBJECT( par_key );
                    tmp_compexpr_left_4 = par_key;
                    tmp_compexpr_right_4 = const_str_plain_LineBreak;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 215;
                        type_description_1 = "ooooc";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_called_instance_2;
                        PyObject *tmp_call_result_4;
                        if ( PyCell_GET( self->m_closure[0] ) == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "result" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 216;
                            type_description_1 = "ooooc";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_2 = PyCell_GET( self->m_closure[0] );
                        frame_021e39f3275eaecb297c28ed89eb0977->m_frame.f_lineno = 216;
                        tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_space_tuple, 0 ) );

                        if ( tmp_call_result_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 216;
                            type_description_1 = "ooooc";
                            goto frame_exception_exit_1;
                        }
                        Py_DECREF( tmp_call_result_4 );
                    }
                    goto branch_end_4;
                    branch_no_4:;
                    {
                        nuitka_bool tmp_condition_result_5;
                        PyObject *tmp_compexpr_left_5;
                        PyObject *tmp_compexpr_right_5;
                        CHECK_OBJECT( par_key );
                        tmp_compexpr_left_5 = par_key;
                        tmp_compexpr_right_5 = const_str_plain_SoftBreak;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 217;
                            type_description_1 = "ooooc";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_5;
                        }
                        else
                        {
                            goto branch_no_5;
                        }
                        branch_yes_5:;
                        {
                            PyObject *tmp_called_instance_3;
                            PyObject *tmp_call_result_5;
                            if ( PyCell_GET( self->m_closure[0] ) == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "result" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 218;
                                type_description_1 = "ooooc";
                                goto frame_exception_exit_1;
                            }

                            tmp_called_instance_3 = PyCell_GET( self->m_closure[0] );
                            frame_021e39f3275eaecb297c28ed89eb0977->m_frame.f_lineno = 218;
                            tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_space_tuple, 0 ) );

                            if ( tmp_call_result_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 218;
                                type_description_1 = "ooooc";
                                goto frame_exception_exit_1;
                            }
                            Py_DECREF( tmp_call_result_5 );
                        }
                        goto branch_end_5;
                        branch_no_5:;
                        {
                            nuitka_bool tmp_condition_result_6;
                            PyObject *tmp_compexpr_left_6;
                            PyObject *tmp_compexpr_right_6;
                            CHECK_OBJECT( par_key );
                            tmp_compexpr_left_6 = par_key;
                            tmp_compexpr_right_6 = const_str_plain_Space;
                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 219;
                                type_description_1 = "ooooc";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_6;
                            }
                            else
                            {
                                goto branch_no_6;
                            }
                            branch_yes_6:;
                            {
                                PyObject *tmp_called_instance_4;
                                PyObject *tmp_call_result_6;
                                if ( PyCell_GET( self->m_closure[0] ) == NULL )
                                {

                                    exception_type = PyExc_NameError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "result" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 220;
                                    type_description_1 = "ooooc";
                                    goto frame_exception_exit_1;
                                }

                                tmp_called_instance_4 = PyCell_GET( self->m_closure[0] );
                                frame_021e39f3275eaecb297c28ed89eb0977->m_frame.f_lineno = 220;
                                tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_space_tuple, 0 ) );

                                if ( tmp_call_result_6 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 220;
                                    type_description_1 = "ooooc";
                                    goto frame_exception_exit_1;
                                }
                                Py_DECREF( tmp_call_result_6 );
                            }
                            branch_no_6:;
                        }
                        branch_end_5:;
                    }
                    branch_end_4:;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_021e39f3275eaecb297c28ed89eb0977 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_021e39f3275eaecb297c28ed89eb0977 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_021e39f3275eaecb297c28ed89eb0977, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_021e39f3275eaecb297c28ed89eb0977->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_021e39f3275eaecb297c28ed89eb0977, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_021e39f3275eaecb297c28ed89eb0977,
        type_description_1,
        par_key,
        par_val,
        par_format,
        par_meta,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_021e39f3275eaecb297c28ed89eb0977 == cache_frame_021e39f3275eaecb297c28ed89eb0977 )
    {
        Py_DECREF( frame_021e39f3275eaecb297c28ed89eb0977 );
    }
    cache_frame_021e39f3275eaecb297c28ed89eb0977 = NULL;

    assertFrameObject( frame_021e39f3275eaecb297c28ed89eb0977 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_9_stringify$$$function_1_go );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_key );
    Py_DECREF( par_key );
    par_key = NULL;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_key );
    Py_DECREF( par_key );
    par_key = NULL;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    CHECK_OBJECT( (PyObject *)par_format );
    Py_DECREF( par_format );
    par_format = NULL;

    CHECK_OBJECT( (PyObject *)par_meta );
    Py_DECREF( par_meta );
    par_meta = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_9_stringify$$$function_1_go );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_10_attributes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_attrs = python_pars[ 0 ];
    PyObject *var_ident = NULL;
    PyObject *var_classes = NULL;
    PyObject *var_keyvals = NULL;
    PyObject *outline_0_var_x = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_a9091592226457ed985ac7f3fe33f487;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_a9091592226457ed985ac7f3fe33f487 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a9091592226457ed985ac7f3fe33f487, codeobj_a9091592226457ed985ac7f3fe33f487, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a9091592226457ed985ac7f3fe33f487 = cache_frame_a9091592226457ed985ac7f3fe33f487;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a9091592226457ed985ac7f3fe33f487 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a9091592226457ed985ac7f3fe33f487 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        CHECK_OBJECT( par_attrs );
        tmp_or_left_value_1 = par_attrs;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 230;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = PyDict_New();
        tmp_assign_source_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assign_source_1 = tmp_or_left_value_1;
        or_end_1:;
        {
            PyObject *old = par_attrs;
            assert( old != NULL );
            par_attrs = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_attrs );
        tmp_called_instance_1 = par_attrs;
        frame_a9091592226457ed985ac7f3fe33f487->m_frame.f_lineno = 231;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_id_str_empty_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_ident == NULL );
        var_ident = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        CHECK_OBJECT( par_attrs );
        tmp_called_instance_2 = par_attrs;
        tmp_call_arg_element_1 = const_str_plain_classes;
        tmp_call_arg_element_2 = PyList_New( 0 );
        frame_a9091592226457ed985ac7f3fe33f487->m_frame.f_lineno = 232;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_classes == NULL );
        var_classes = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_attrs );
            tmp_iter_arg_1 = par_attrs;
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 233;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2, codeobj_bd05f4064d4a6a0d1fa7ab1c001893c6, module_pandocfilters, sizeof(void *)+sizeof(void *) );
        frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 = cache_frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 233;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_8 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_x;
                outline_0_var_x = tmp_assign_source_8;
                Py_INCREF( outline_0_var_x );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_1;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( outline_0_var_x );
            tmp_compexpr_left_1 = outline_0_var_x;
            tmp_compexpr_right_1 = const_str_plain_classes;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 233;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( outline_0_var_x );
            tmp_compexpr_left_2 = outline_0_var_x;
            tmp_compexpr_right_2 = const_str_plain_id;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 233;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_1 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_1 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_1;
            }
            else
            {
                goto branch_no_1;
            }
            branch_yes_1:;
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_list_element_1;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_x );
                tmp_list_element_1 = outline_0_var_x;
                tmp_append_value_1 = PyList_New( 2 );
                Py_INCREF( tmp_list_element_1 );
                PyList_SET_ITEM( tmp_append_value_1, 0, tmp_list_element_1 );
                CHECK_OBJECT( par_attrs );
                tmp_subscribed_name_1 = par_attrs;
                CHECK_OBJECT( outline_0_var_x );
                tmp_subscript_name_1 = outline_0_var_x;
                tmp_list_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                if ( tmp_list_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_append_value_1 );

                    exception_lineno = 233;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
                PyList_SET_ITEM( tmp_append_value_1, 1, tmp_list_element_1 );
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 233;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
            }
            branch_no_1:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_4 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_4 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_10_attributes );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2,
            type_description_2,
            outline_0_var_x,
            par_attrs
        );


        // Release cached frame.
        if ( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 == cache_frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 )
        {
            Py_DECREF( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 );
        }
        cache_frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 = NULL;

        assertFrameObject( frame_bd05f4064d4a6a0d1fa7ab1c001893c6_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_10_attributes );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_x );
        outline_0_var_x = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_x );
        outline_0_var_x = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_10_attributes );
        return NULL;
        outline_exception_1:;
        exception_lineno = 233;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_keyvals == NULL );
        var_keyvals = tmp_assign_source_4;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9091592226457ed985ac7f3fe33f487 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9091592226457ed985ac7f3fe33f487 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a9091592226457ed985ac7f3fe33f487, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a9091592226457ed985ac7f3fe33f487->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a9091592226457ed985ac7f3fe33f487, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a9091592226457ed985ac7f3fe33f487,
        type_description_1,
        par_attrs,
        var_ident,
        var_classes,
        var_keyvals
    );


    // Release cached frame.
    if ( frame_a9091592226457ed985ac7f3fe33f487 == cache_frame_a9091592226457ed985ac7f3fe33f487 )
    {
        Py_DECREF( frame_a9091592226457ed985ac7f3fe33f487 );
    }
    cache_frame_a9091592226457ed985ac7f3fe33f487 = NULL;

    assertFrameObject( frame_a9091592226457ed985ac7f3fe33f487 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    {
        PyObject *tmp_list_element_2;
        CHECK_OBJECT( var_ident );
        tmp_list_element_2 = var_ident;
        tmp_return_value = PyList_New( 3 );
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_2 );
        CHECK_OBJECT( var_classes );
        tmp_list_element_2 = var_classes;
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_return_value, 1, tmp_list_element_2 );
        CHECK_OBJECT( var_keyvals );
        tmp_list_element_2 = var_keyvals;
        Py_INCREF( tmp_list_element_2 );
        PyList_SET_ITEM( tmp_return_value, 2, tmp_list_element_2 );
        goto try_return_handler_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_10_attributes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var_ident );
    Py_DECREF( var_ident );
    var_ident = NULL;

    CHECK_OBJECT( (PyObject *)var_classes );
    Py_DECREF( var_classes );
    var_classes = NULL;

    CHECK_OBJECT( (PyObject *)var_keyvals );
    Py_DECREF( var_keyvals );
    var_keyvals = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    Py_XDECREF( var_ident );
    var_ident = NULL;

    Py_XDECREF( var_classes );
    var_classes = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_10_attributes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_11_elt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_eltType = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_numargs = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *var_fun = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_pandocfilters$$$function_11_elt$$$function_1_fun(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_eltType;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] = par_numargs;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] );


        assert( var_fun == NULL );
        var_fun = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_fun );
    tmp_return_value = var_fun;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_11_elt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_eltType );
    Py_DECREF( par_eltType );
    par_eltType = NULL;

    CHECK_OBJECT( (PyObject *)par_numargs );
    Py_DECREF( par_numargs );
    par_numargs = NULL;

    CHECK_OBJECT( (PyObject *)var_fun );
    Py_DECREF( var_fun );
    var_fun = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_11_elt );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pandocfilters$$$function_11_elt$$$function_1_fun( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *var_lenargs = NULL;
    PyObject *var_xs = NULL;
    struct Nuitka_FrameObject *frame_38724a48fc9148b82430f41e71a062fa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_38724a48fc9148b82430f41e71a062fa = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_38724a48fc9148b82430f41e71a062fa, codeobj_38724a48fc9148b82430f41e71a062fa, module_pandocfilters, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_38724a48fc9148b82430f41e71a062fa = cache_frame_38724a48fc9148b82430f41e71a062fa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_38724a48fc9148b82430f41e71a062fa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_38724a48fc9148b82430f41e71a062fa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_args );
        tmp_len_arg_1 = par_args;
        tmp_assign_source_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        assert( var_lenargs == NULL );
        var_lenargs = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_lenargs );
        tmp_compexpr_left_1 = var_lenargs;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "numargs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = PyCell_GET( self->m_closure[1] );
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_1;
            PyObject *tmp_right_name_2;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_right_name_3;
            PyObject *tmp_right_name_4;
            PyObject *tmp_unicode_arg_2;
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "eltType" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 241;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_left_name_4 = PyCell_GET( self->m_closure[0] );
            tmp_right_name_1 = const_str_digest_a9bbf8eddc9736a0653e7e7b30eefd84;
            tmp_left_name_3 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_4, tmp_right_name_1 );
            if ( tmp_left_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 241;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_left_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "numargs" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 241;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_unicode_arg_1 = PyCell_GET( self->m_closure[1] );
            tmp_right_name_2 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_3 );

                exception_lineno = 241;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_3 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 241;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            tmp_right_name_3 = const_str_digest_616d99c141a2994de9878ed27e1e904b;
            tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_2, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 241;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_lenargs );
            tmp_unicode_arg_2 = var_lenargs;
            tmp_right_name_4 = PyObject_Unicode( tmp_unicode_arg_2 );
            if ( tmp_right_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 242;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            tmp_make_exception_arg_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_4 );
            Py_DECREF( tmp_left_name_1 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 242;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            frame_38724a48fc9148b82430f41e71a062fa->m_frame.f_lineno = 241;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 241;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "numargs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_2 = PyCell_GET( self->m_closure[1] );
        tmp_compexpr_right_2 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = PyList_New( 0 );
            assert( var_xs == NULL );
            var_xs = tmp_assign_source_2;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_len_arg_2;
            CHECK_OBJECT( par_args );
            tmp_len_arg_2 = par_args;
            tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_2 );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( par_args );
                tmp_subscribed_name_1 = par_args;
                tmp_subscript_name_1 = const_int_0;
                tmp_assign_source_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 246;
                    type_description_1 = "ooocc";
                    goto frame_exception_exit_1;
                }
                assert( var_xs == NULL );
                var_xs = tmp_assign_source_3;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_list_arg_1;
                CHECK_OBJECT( par_args );
                tmp_list_arg_1 = par_args;
                tmp_assign_source_4 = PySequence_List( tmp_list_arg_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 248;
                    type_description_1 = "ooocc";
                    goto frame_exception_exit_1;
                }
                assert( var_xs == NULL );
                var_xs = tmp_assign_source_4;
            }
            branch_end_3:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        tmp_dict_key_1 = const_str_plain_t;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "eltType" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = PyCell_GET( self->m_closure[0] );
        tmp_return_value = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_c;
        if ( var_xs == NULL )
        {
            Py_DECREF( tmp_return_value );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "xs" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_2 = var_xs;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_38724a48fc9148b82430f41e71a062fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_38724a48fc9148b82430f41e71a062fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_38724a48fc9148b82430f41e71a062fa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_38724a48fc9148b82430f41e71a062fa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_38724a48fc9148b82430f41e71a062fa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_38724a48fc9148b82430f41e71a062fa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_38724a48fc9148b82430f41e71a062fa,
        type_description_1,
        par_args,
        var_lenargs,
        var_xs,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_38724a48fc9148b82430f41e71a062fa == cache_frame_38724a48fc9148b82430f41e71a062fa )
    {
        Py_DECREF( frame_38724a48fc9148b82430f41e71a062fa );
    }
    cache_frame_38724a48fc9148b82430f41e71a062fa = NULL;

    assertFrameObject( frame_38724a48fc9148b82430f41e71a062fa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_11_elt$$$function_1_fun );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)var_lenargs );
    Py_DECREF( var_lenargs );
    var_lenargs = NULL;

    Py_XDECREF( var_xs );
    var_xs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    Py_XDECREF( var_lenargs );
    var_lenargs = NULL;

    Py_XDECREF( var_xs );
    var_xs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandocfilters$$$function_11_elt$$$function_1_fun );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_10_attributes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_10_attributes,
        const_str_plain_attributes,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a9091592226457ed985ac7f3fe33f487,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_3d3d836f635866494e20f3ca0f20012b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_11_elt(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_11_elt,
        const_str_plain_elt,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fcdf362aea12a18c50571154483517b8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_11_elt$$$function_1_fun(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_11_elt$$$function_1_fun,
        const_str_plain_fun,
#if PYTHON_VERSION >= 300
        const_str_digest_05dc8befe0400d916af2ff4b5795ee7e,
#endif
        codeobj_38724a48fc9148b82430f41e71a062fa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_1_get_filename4code( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_1_get_filename4code,
        const_str_plain_get_filename4code,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e3f09db8d4c929f2f1b9bf0b73f2aa38,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_7c3f14c99057ecc3e4a3a5c8ef77815c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_2_get_value( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_2_get_value,
        const_str_plain_get_value,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a5ec9d4f1c1212786c79053b3506070f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_acec08ef2139364a6dc0302b7e4147a3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_3_get_caption(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_3_get_caption,
        const_str_plain_get_caption,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_962b66337c3d5fc86e07218a95b46fa6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_40fe20f63d60820135c9f2ec9801401c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_4_get_extension(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_4_get_extension,
        const_str_plain_get_extension,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1a332dd507d0cb6ab01b4c04db7622ce,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_958c3218d8b975cccdb175fa77cac6ba,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_5_walk(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_5_walk,
        const_str_plain_walk,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_55333e787b20ec5b5feaae48e24322b7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_4c9fed436cc981918d54287b53c873de,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_6_toJSONFilter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_6_toJSONFilter,
        const_str_plain_toJSONFilter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dfc0a58bb7af92e212ba12ad21c11dbb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_6b99c96f548d8a10ff28a0c9b4533196,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_7_toJSONFilters(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_7_toJSONFilters,
        const_str_plain_toJSONFilters,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cdbc59d74965b61d67a1dff28e347b6b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_b7c4c5e97ce628bf6b0dac3111668c34,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_8_applyJSONFilters( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_8_applyJSONFilters,
        const_str_plain_applyJSONFilters,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ace21b5f79b6395c93652b67400b9d7,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_b4781d1fee4f6ac63d247204fd474340,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_9_stringify(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_9_stringify,
        const_str_plain_stringify,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f053323cec25fcaf778ba4918c970aa2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        const_str_digest_baab06bf48ed3d0177b4cf0cc94ff514,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandocfilters$$$function_9_stringify$$$function_1_go(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandocfilters$$$function_9_stringify$$$function_1_go,
        const_str_plain_go,
#if PYTHON_VERSION >= 300
        const_str_digest_b5d121348c71f5f8f1075409de4b9d9c,
#endif
        codeobj_021e39f3275eaecb297c28ed89eb0977,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandocfilters,
        NULL,
        1
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandocfilters =
{
    PyModuleDef_HEAD_INIT,
    "pandocfilters",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(pandocfilters)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(pandocfilters)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandocfilters );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandocfilters: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandocfilters: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandocfilters: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandocfilters" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandocfilters = Py_InitModule4(
        "pandocfilters",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandocfilters = PyModule_Create( &mdef_pandocfilters );
#endif

    moduledict_pandocfilters = MODULE_DICT( module_pandocfilters );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_pandocfilters,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandocfilters,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandocfilters,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandocfilters,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandocfilters );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_plain_pandocfilters, module_pandocfilters );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_78b2c5c00f2c1d6935eadf75ebf50097;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_e62c65f4757cdefae810eec0f3a3e1a9;
        UPDATE_STRING_DICT0( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_78b2c5c00f2c1d6935eadf75ebf50097 = MAKE_MODULE_FRAME( codeobj_78b2c5c00f2c1d6935eadf75ebf50097, module_pandocfilters );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_78b2c5c00f2c1d6935eadf75ebf50097 );
    assert( Py_REFCNT( frame_78b2c5c00f2c1d6935eadf75ebf50097 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_codecs;
        tmp_globals_name_1 = (PyObject *)moduledict_pandocfilters;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 10;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_codecs, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_hashlib;
        tmp_globals_name_2 = (PyObject *)moduledict_pandocfilters;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 11;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_hashlib, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_io;
        tmp_globals_name_3 = (PyObject *)moduledict_pandocfilters;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 12;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_io, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_json;
        tmp_globals_name_4 = (PyObject *)moduledict_pandocfilters;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 13;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_json, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_os;
        tmp_globals_name_5 = (PyObject *)moduledict_pandocfilters;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 14;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_sys;
        tmp_globals_name_6 = (PyObject *)moduledict_pandocfilters;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 15;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        assert( !(tmp_assign_source_9 == NULL) );
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_10 = MAKE_FUNCTION_pandocfilters$$$function_1_get_filename4code( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_get_filename4code, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_11 = MAKE_FUNCTION_pandocfilters$$$function_2_get_value( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_get_value, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_pandocfilters$$$function_3_get_caption(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_get_caption, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_pandocfilters$$$function_4_get_extension(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_get_extension, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_pandocfilters$$$function_5_walk(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_walk, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_pandocfilters$$$function_6_toJSONFilter(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_toJSONFilter, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_pandocfilters$$$function_7_toJSONFilters(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_toJSONFilters, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_str_empty_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_17 = MAKE_FUNCTION_pandocfilters$$$function_8_applyJSONFilters( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_applyJSONFilters, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_pandocfilters$$$function_9_stringify(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_stringify, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_pandocfilters$$$function_10_attributes(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_attributes, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_pandocfilters$$$function_11_elt(  );



        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 254;
        tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_str_plain_Plain_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Plain, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 255;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 255;
        tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_plain_Para_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Para, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 256;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_5;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 256;
        tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_plain_CodeBlock_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_CodeBlock, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 257;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_6;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 257;
        tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_plain_RawBlock_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_RawBlock, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 258;

            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_7;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 258;
        tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_plain_BlockQuote_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_BlockQuote, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 259;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_8;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 259;
        tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_plain_OrderedList_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_OrderedList, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 260;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_9;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 260;
        tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_str_plain_BulletList_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_BulletList, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 261;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_10;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 261;
        tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_str_plain_DefinitionList_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_DefinitionList, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 262;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_11;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 262;
        tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_plain_Header_int_pos_3_tuple, 0 ) );

        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Header, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 263;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_12;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 263;
        tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_str_plain_HorizontalRule_int_0_tuple, 0 ) );

        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 263;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_HorizontalRule, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_13;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 264;
        tmp_assign_source_31 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_plain_Table_int_pos_5_tuple, 0 ) );

        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Table, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_14;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 265;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_14;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 265;
        tmp_assign_source_32 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_plain_Div_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Div, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_15;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 266;
        tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_plain_Null_int_0_tuple, 0 ) );

        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 266;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Null, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_16;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 270;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_16;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 270;
        tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_str_plain_Str_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 270;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Str, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_17;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 271;
        tmp_assign_source_35 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_plain_Emph_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Emph, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_18;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 272;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_18;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 272;
        tmp_assign_source_36 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_plain_Strong_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Strong, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_19;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 273;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_19;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 273;
        tmp_assign_source_37 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_Strikeout_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 273;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Strikeout, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_20;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 274;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_20;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 274;
        tmp_assign_source_38 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_plain_Superscript_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Superscript, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_21;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 275;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_21;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 275;
        tmp_assign_source_39 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_19, &PyTuple_GET_ITEM( const_tuple_str_plain_Subscript_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Subscript, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_22;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_22;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 276;
        tmp_assign_source_40 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_str_plain_SmallCaps_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 276;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_SmallCaps, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_23;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 277;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_23;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 277;
        tmp_assign_source_41 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_str_plain_Quoted_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Quoted, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_24;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 278;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_24;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 278;
        tmp_assign_source_42 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_22, &PyTuple_GET_ITEM( const_tuple_str_plain_Cite_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Cite, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_called_name_23;
        PyObject *tmp_mvar_value_25;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 279;

            goto frame_exception_exit_1;
        }

        tmp_called_name_23 = tmp_mvar_value_25;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 279;
        tmp_assign_source_43 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_23, &PyTuple_GET_ITEM( const_tuple_str_plain_Code_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 279;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Code, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_26;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 280;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_26;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 280;
        tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_24, &PyTuple_GET_ITEM( const_tuple_str_plain_Space_int_0_tuple, 0 ) );

        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Space, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_27;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 281;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_27;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 281;
        tmp_assign_source_45 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_25, &PyTuple_GET_ITEM( const_tuple_str_plain_LineBreak_int_0_tuple, 0 ) );

        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_LineBreak, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_28;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_28 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 282;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_28;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 282;
        tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_26, &PyTuple_GET_ITEM( const_tuple_str_plain_Math_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 282;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Math, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_29;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 283;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_29;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 283;
        tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_27, &PyTuple_GET_ITEM( const_tuple_str_plain_RawInline_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_RawInline, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_30;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_30 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 284;

            goto frame_exception_exit_1;
        }

        tmp_called_name_28 = tmp_mvar_value_30;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 284;
        tmp_assign_source_48 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_28, &PyTuple_GET_ITEM( const_tuple_str_plain_Link_int_pos_3_tuple, 0 ) );

        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 284;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Link, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_31;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 285;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_31;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 285;
        tmp_assign_source_49 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, &PyTuple_GET_ITEM( const_tuple_str_plain_Image_int_pos_3_tuple, 0 ) );

        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 285;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Image, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_32;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_32 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 286;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_32;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 286;
        tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_30, &PyTuple_GET_ITEM( const_tuple_str_plain_Note_int_pos_1_tuple, 0 ) );

        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Note, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_called_name_31;
        PyObject *tmp_mvar_value_33;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_33 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 287;

            goto frame_exception_exit_1;
        }

        tmp_called_name_31 = tmp_mvar_value_33;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 287;
        tmp_assign_source_51 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_31, &PyTuple_GET_ITEM( const_tuple_str_plain_SoftBreak_int_0_tuple, 0 ) );

        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_SoftBreak, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_called_name_32;
        PyObject *tmp_mvar_value_34;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_elt );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_elt );
        }

        if ( tmp_mvar_value_34 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "elt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 288;

            goto frame_exception_exit_1;
        }

        tmp_called_name_32 = tmp_mvar_value_34;
        frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame.f_lineno = 288;
        tmp_assign_source_52 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_32, &PyTuple_GET_ITEM( const_tuple_str_plain_Span_int_pos_2_tuple, 0 ) );

        if ( tmp_assign_source_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 288;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pandocfilters, (Nuitka_StringObject *)const_str_plain_Span, tmp_assign_source_52 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_78b2c5c00f2c1d6935eadf75ebf50097 );
#endif
    popFrameStack();

    assertFrameObject( frame_78b2c5c00f2c1d6935eadf75ebf50097 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_78b2c5c00f2c1d6935eadf75ebf50097 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_78b2c5c00f2c1d6935eadf75ebf50097, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_78b2c5c00f2c1d6935eadf75ebf50097->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_78b2c5c00f2c1d6935eadf75ebf50097, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_pandocfilters );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
