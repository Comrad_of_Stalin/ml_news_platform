/* Generated code for Python module 'grammars.organisation'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_grammars$organisation" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_grammars$organisation;
PyDictObject *moduledict_grammars$organisation;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_chr_38;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_str_plain_Orgn_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_RuleTransformator;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_str_chr_1080_tuple;
extern PyObject *const_str_plain_visit_InterpretationRule;
extern PyObject *const_tuple_str_plain_RuleTransformator_tuple;
extern PyObject *const_tuple_str_plain_gent_tuple;
extern PyObject *const_tuple_str_chr_44_tuple;
static PyObject *const_str_plain_Orgn;
extern PyObject *const_str_plain_PERSON;
static PyObject *const_str_plain_QUOTED;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_plain_ADJF_tuple;
extern PyObject *const_str_dot;
extern PyObject *const_tuple_str_plain_self_str_plain_item_tuple;
static PyObject *const_str_digest_96c2c8356ced32520033680b52ca8a91;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_ADJF;
extern PyObject *const_str_plain_POSITION_NAME;
extern PyObject *const_str_chr_44;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_gnc_relation;
static PyObject *const_str_plain_StripInterpretationTransformator;
extern PyObject *const_str_plain_item;
extern PyObject *const_str_chr_45;
static PyObject *const_tuple_str_plain_attribute_str_plain_fact_tuple;
extern PyObject *const_str_plain_QUOTES;
extern PyObject *const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
extern PyObject *const_str_plain_transform;
extern PyObject *const_str_plain_LATIN;
static PyObject *const_tuple_str_plain_POSITION_NAME_tuple;
static PyObject *const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple;
extern PyObject *const_tuple_str_dot_tuple;
extern PyObject *const_str_plain_or_;
extern PyObject *const_tuple_str_chr_45_tuple;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_QUOTED_WITH_ADJF_PREFIX;
extern PyObject *const_str_plain_attribute;
extern PyObject *const_str_plain_gnc;
extern PyObject *const_str_plain_interpretation;
extern PyObject *const_str_digest_b356db3f83f00c40fb4d261edfeed58e;
extern PyObject *const_str_plain_name;
extern PyObject *const_tuple_str_plain_morph_pipeline_tuple;
static PyObject *const_str_chr_1080;
extern PyObject *const_str_digest_3b0d1c4c904989a80e46e8a766322a96;
extern PyObject *const_str_plain_in_;
static PyObject *const_str_plain_TRIPLE_QUOTED;
extern PyObject *const_str_digest_ea78dcde38a5066c777d6274d25657b3;
static PyObject *const_str_digest_935ea372b25a2a50d081ad39f75c6423;
extern PyObject *const_tuple_str_digest_b356db3f83f00c40fb4d261edfeed58e_tuple;
extern PyObject *const_str_plain_match;
static PyObject *const_str_plain_ADJF_PREFIX;
extern PyObject *const_str_plain_eq;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_yargy;
static PyObject *const_set_e87e07f414d4699c101fb9279e3b145e;
extern PyObject *const_str_plain_gent;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_repeatable;
static PyObject *const_str_plain_Organisation;
extern PyObject *const_str_plain_NAME;
static PyObject *const_tuple_str_digest_aab517adf3a2a34bb0496eeca2922a8e_tuple;
static PyObject *const_str_plain_NAMED;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_is_capitalized;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_tuple_str_plain_LATIN_tuple;
extern PyObject *const_str_plain_SIMPLE_NAME;
static PyObject *const_str_plain_ORGANISATION_;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_fact;
extern PyObject *const_list_str_plain_name_list;
extern PyObject *const_str_plain_normalized;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_caseless;
extern PyObject *const_str_plain_TYPE;
static PyObject *const_str_digest_aab517adf3a2a34bb0496eeca2922a8e;
extern PyObject *const_str_plain_true;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_rule;
static PyObject *const_list_2c5f87a482b9eb949bd53888ea508bc0_list;
extern PyObject *const_str_chr_47;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_tuple_str_plain_SIMPLE_NAME_tuple;
extern PyObject *const_str_plain_optional;
static PyObject *const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple;
extern PyObject *const_str_plain_case_relation;
extern PyObject *const_str_plain_visit;
extern PyObject *const_str_plain_ORGANISATION;
extern PyObject *const_str_plain_case;
static PyObject *const_tuple_str_plain_gnc_relation_str_plain_case_relation_tuple;
extern PyObject *const_str_digest_ece91ea33402621841e0e9196aa27a3d;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_morph_pipeline;
extern PyObject *const_tuple_str_plain_QUOTES_tuple;
static PyObject *const_str_digest_0f6a0beb9e00af8e72b8efff37c9f1ec;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_digest_09460345ee7f4232b97d5c10b6119bc9;
extern PyObject *const_str_digest_bcd331dcd05bcf687d1b0b75ef91d71d;
extern PyObject *const_str_plain_not_;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_BASIC;
extern PyObject *const_str_plain_and_;
static PyObject *const_str_plain_KNOWN;
extern PyObject *const_str_digest_727025073b9dac9adc1805116cfc5a57;
static PyObject *const_str_plain_GENT_GROUP;
extern PyObject *const_str_plain_gram;
extern PyObject *const_str_plain_person;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_Orgn_tuple = PyTuple_New( 1 );
    const_str_plain_Orgn = UNSTREAM_STRING_ASCII( &constant_bin[ 679607 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Orgn_tuple, 0, const_str_plain_Orgn ); Py_INCREF( const_str_plain_Orgn );
    const_tuple_str_chr_1080_tuple = PyTuple_New( 1 );
    const_str_chr_1080 = UNSTREAM_STRING( &constant_bin[ 667379 ], 2, 0 );
    PyTuple_SET_ITEM( const_tuple_str_chr_1080_tuple, 0, const_str_chr_1080 ); Py_INCREF( const_str_chr_1080 );
    const_str_plain_QUOTED = UNSTREAM_STRING_ASCII( &constant_bin[ 679611 ], 6, 1 );
    const_str_digest_96c2c8356ced32520033680b52ca8a91 = UNSTREAM_STRING_ASCII( &constant_bin[ 679617 ], 57, 0 );
    const_str_plain_StripInterpretationTransformator = UNSTREAM_STRING_ASCII( &constant_bin[ 679617 ], 32, 1 );
    const_tuple_str_plain_attribute_str_plain_fact_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_attribute_str_plain_fact_tuple, 0, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_str_plain_attribute_str_plain_fact_tuple, 1, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    const_tuple_str_plain_POSITION_NAME_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_POSITION_NAME_tuple, 0, const_str_plain_POSITION_NAME ); Py_INCREF( const_str_plain_POSITION_NAME );
    const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 0, const_str_plain_eq ); Py_INCREF( const_str_plain_eq );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 1, const_str_plain_in_ ); Py_INCREF( const_str_plain_in_ );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 2, const_str_plain_true ); Py_INCREF( const_str_plain_true );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 3, const_str_plain_gram ); Py_INCREF( const_str_plain_gram );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 4, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 5, const_str_plain_caseless ); Py_INCREF( const_str_plain_caseless );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 6, const_str_plain_normalized ); Py_INCREF( const_str_plain_normalized );
    PyTuple_SET_ITEM( const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple, 7, const_str_plain_is_capitalized ); Py_INCREF( const_str_plain_is_capitalized );
    const_str_plain_QUOTED_WITH_ADJF_PREFIX = UNSTREAM_STRING_ASCII( &constant_bin[ 679674 ], 23, 1 );
    const_str_plain_TRIPLE_QUOTED = UNSTREAM_STRING_ASCII( &constant_bin[ 679697 ], 13, 1 );
    const_str_digest_935ea372b25a2a50d081ad39f75c6423 = UNSTREAM_STRING_ASCII( &constant_bin[ 679710 ], 30, 0 );
    const_str_plain_ADJF_PREFIX = UNSTREAM_STRING_ASCII( &constant_bin[ 679686 ], 11, 1 );
    const_set_e87e07f414d4699c101fb9279e3b145e = PySet_New( NULL );
    PySet_Add( const_set_e87e07f414d4699c101fb9279e3b145e, const_str_dot );
    PySet_Add( const_set_e87e07f414d4699c101fb9279e3b145e, const_str_chr_38 );
    PySet_Add( const_set_e87e07f414d4699c101fb9279e3b145e, const_str_chr_47 );
    assert( PySet_Size( const_set_e87e07f414d4699c101fb9279e3b145e ) == 3 );
    const_str_plain_Organisation = UNSTREAM_STRING_ASCII( &constant_bin[ 667024 ], 12, 1 );
    const_tuple_str_digest_aab517adf3a2a34bb0496eeca2922a8e_tuple = PyTuple_New( 1 );
    const_str_digest_aab517adf3a2a34bb0496eeca2922a8e = UNSTREAM_STRING( &constant_bin[ 679740 ], 6, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_aab517adf3a2a34bb0496eeca2922a8e_tuple, 0, const_str_digest_aab517adf3a2a34bb0496eeca2922a8e ); Py_INCREF( const_str_digest_aab517adf3a2a34bb0496eeca2922a8e );
    const_str_plain_NAMED = UNSTREAM_STRING_ASCII( &constant_bin[ 679746 ], 5, 1 );
    const_tuple_str_plain_LATIN_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LATIN_tuple, 0, const_str_plain_LATIN ); Py_INCREF( const_str_plain_LATIN );
    const_str_plain_ORGANISATION_ = UNSTREAM_STRING_ASCII( &constant_bin[ 679751 ], 13, 1 );
    const_list_2c5f87a482b9eb949bd53888ea508bc0_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 679764 ], 2565 );
    const_tuple_str_plain_SIMPLE_NAME_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SIMPLE_NAME_tuple, 0, const_str_plain_SIMPLE_NAME ); Py_INCREF( const_str_plain_SIMPLE_NAME );
    const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple, 0, const_str_plain_rule ); Py_INCREF( const_str_plain_rule );
    PyTuple_SET_ITEM( const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple, 1, const_str_plain_not_ ); Py_INCREF( const_str_plain_not_ );
    PyTuple_SET_ITEM( const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple, 2, const_str_plain_and_ ); Py_INCREF( const_str_plain_and_ );
    PyTuple_SET_ITEM( const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple, 3, const_str_plain_or_ ); Py_INCREF( const_str_plain_or_ );
    const_tuple_str_plain_gnc_relation_str_plain_case_relation_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gnc_relation_str_plain_case_relation_tuple, 0, const_str_plain_gnc_relation ); Py_INCREF( const_str_plain_gnc_relation );
    PyTuple_SET_ITEM( const_tuple_str_plain_gnc_relation_str_plain_case_relation_tuple, 1, const_str_plain_case_relation ); Py_INCREF( const_str_plain_case_relation );
    const_str_digest_0f6a0beb9e00af8e72b8efff37c9f1ec = UNSTREAM_STRING_ASCII( &constant_bin[ 682329 ], 24, 0 );
    const_str_plain_BASIC = UNSTREAM_STRING_ASCII( &constant_bin[ 682353 ], 5, 1 );
    const_str_plain_KNOWN = UNSTREAM_STRING_ASCII( &constant_bin[ 194680 ], 5, 1 );
    const_str_plain_GENT_GROUP = UNSTREAM_STRING_ASCII( &constant_bin[ 682358 ], 10, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_grammars$organisation( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_1dffb6e1762a215e07418c24e99f24f0;
static PyCodeObject *codeobj_98540a4ae4f554b7ee743fb1c6417294;
static PyCodeObject *codeobj_ec192f603202f8d6fbb0569acf3a31f1;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_0f6a0beb9e00af8e72b8efff37c9f1ec );
    codeobj_1dffb6e1762a215e07418c24e99f24f0 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_935ea372b25a2a50d081ad39f75c6423, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_98540a4ae4f554b7ee743fb1c6417294 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_StripInterpretationTransformator, 34, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ec192f603202f8d6fbb0569acf3a31f1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_visit_InterpretationRule, 35, const_tuple_str_plain_self_str_plain_item_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_grammars$organisation$$$function_1_visit_InterpretationRule(  );


// The module function definitions.
static PyObject *impl_grammars$organisation$$$function_1_visit_InterpretationRule( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_item = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_ec192f603202f8d6fbb0569acf3a31f1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ec192f603202f8d6fbb0569acf3a31f1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec192f603202f8d6fbb0569acf3a31f1, codeobj_ec192f603202f8d6fbb0569acf3a31f1, module_grammars$organisation, sizeof(void *)+sizeof(void *) );
    frame_ec192f603202f8d6fbb0569acf3a31f1 = cache_frame_ec192f603202f8d6fbb0569acf3a31f1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec192f603202f8d6fbb0569acf3a31f1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec192f603202f8d6fbb0569acf3a31f1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_visit );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_item );
        tmp_source_name_2 = par_item;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_rule );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 36;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_ec192f603202f8d6fbb0569acf3a31f1->m_frame.f_lineno = 36;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec192f603202f8d6fbb0569acf3a31f1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec192f603202f8d6fbb0569acf3a31f1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec192f603202f8d6fbb0569acf3a31f1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec192f603202f8d6fbb0569acf3a31f1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec192f603202f8d6fbb0569acf3a31f1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec192f603202f8d6fbb0569acf3a31f1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec192f603202f8d6fbb0569acf3a31f1,
        type_description_1,
        par_self,
        par_item
    );


    // Release cached frame.
    if ( frame_ec192f603202f8d6fbb0569acf3a31f1 == cache_frame_ec192f603202f8d6fbb0569acf3a31f1 )
    {
        Py_DECREF( frame_ec192f603202f8d6fbb0569acf3a31f1 );
    }
    cache_frame_ec192f603202f8d6fbb0569acf3a31f1 = NULL;

    assertFrameObject( frame_ec192f603202f8d6fbb0569acf3a31f1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$organisation$$$function_1_visit_InterpretationRule );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_item );
    Py_DECREF( par_item );
    par_item = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_item );
    Py_DECREF( par_item );
    par_item = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$organisation$$$function_1_visit_InterpretationRule );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_grammars$organisation$$$function_1_visit_InterpretationRule(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$organisation$$$function_1_visit_InterpretationRule,
        const_str_plain_visit_InterpretationRule,
#if PYTHON_VERSION >= 300
        const_str_digest_96c2c8356ced32520033680b52ca8a91,
#endif
        codeobj_ec192f603202f8d6fbb0569acf3a31f1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$organisation,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_grammars$organisation =
{
    PyModuleDef_HEAD_INIT,
    "grammars.organisation",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(grammars$organisation)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(grammars$organisation)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_grammars$organisation );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("grammars.organisation: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.organisation: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.organisation: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initgrammars$organisation" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_grammars$organisation = Py_InitModule4(
        "grammars.organisation",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_grammars$organisation = PyModule_Create( &mdef_grammars$organisation );
#endif

    moduledict_grammars$organisation = MODULE_DICT( module_grammars$organisation );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_grammars$organisation,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_grammars$organisation,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$organisation,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$organisation,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_grammars$organisation );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_09460345ee7f4232b97d5c10b6119bc9, module_grammars$organisation );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    struct Nuitka_FrameObject *frame_1dffb6e1762a215e07418c24e99f24f0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_grammars$organisation_34 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_98540a4ae4f554b7ee743fb1c6417294_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_98540a4ae4f554b7ee743fb1c6417294_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_1dffb6e1762a215e07418c24e99f24f0 = MAKE_MODULE_FRAME( codeobj_1dffb6e1762a215e07418c24e99f24f0, module_grammars$organisation );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_1dffb6e1762a215e07418c24e99f24f0 );
    assert( Py_REFCNT( frame_1dffb6e1762a215e07418c24e99f24f0 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_yargy;
        tmp_globals_name_1 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_rule_str_plain_not__str_plain_and__str_plain_or__tuple;
        tmp_level_name_1 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 4;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_rule );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_not_ );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_not_, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_and_ );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_and_, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_or_ );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
        tmp_globals_name_2 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_attribute_str_plain_fact_tuple;
        tmp_level_name_2 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 10;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_attribute );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_attribute, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_fact );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_fact, tmp_assign_source_12 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_ea78dcde38a5066c777d6274d25657b3;
        tmp_globals_name_3 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_ff351f0a8797e270e0108e6824f906a8_tuple;
        tmp_level_name_3 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 11;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_eq );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_eq, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_9 = tmp_import_from_3__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_in_ );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_10 = tmp_import_from_3__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_true );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_true, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_11 = tmp_import_from_3__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_gram );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gram, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_12 = tmp_import_from_3__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_type );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_type, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_13 = tmp_import_from_3__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_caseless );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_caseless, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_14 = tmp_import_from_3__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_normalized );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_normalized, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_15 = tmp_import_from_3__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_is_capitalized );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_is_capitalized, tmp_assign_source_21 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_3b0d1c4c904989a80e46e8a766322a96;
        tmp_globals_name_4 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_gnc_relation_str_plain_case_relation_tuple;
        tmp_level_name_4 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 21;
        tmp_assign_source_22 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_22;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_16 = tmp_import_from_4__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_gnc_relation );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gnc_relation, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_17 = tmp_import_from_4__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_case_relation );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_case_relation, tmp_assign_source_24 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_18;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_bcd331dcd05bcf687d1b0b75ef91d71d;
        tmp_globals_name_5 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_morph_pipeline_tuple;
        tmp_level_name_5 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 25;
        tmp_import_name_from_18 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_morph_pipeline );
        Py_DECREF( tmp_import_name_from_18 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_morph_pipeline, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_19;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_727025073b9dac9adc1805116cfc5a57;
        tmp_globals_name_6 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_QUOTES_tuple;
        tmp_level_name_6 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 26;
        tmp_import_name_from_19 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_QUOTES );
        Py_DECREF( tmp_import_name_from_19 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_20;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_name;
        tmp_globals_name_7 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_SIMPLE_NAME_tuple;
        tmp_level_name_7 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 28;
        tmp_import_name_from_20 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_SIMPLE_NAME );
        Py_DECREF( tmp_import_name_from_20 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_21;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_person;
        tmp_globals_name_8 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_POSITION_NAME_tuple;
        tmp_level_name_8 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 29;
        tmp_import_name_from_21 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_POSITION_NAME );
        Py_DECREF( tmp_import_name_from_21 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_POSITION_NAME, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_22;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_ece91ea33402621841e0e9196aa27a3d;
        tmp_globals_name_9 = (PyObject *)moduledict_grammars$organisation;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_RuleTransformator_tuple;
        tmp_level_name_9 = const_int_0;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 31;
        tmp_import_name_from_22 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_RuleTransformator );
        Py_DECREF( tmp_import_name_from_22 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_RuleTransformator, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_RuleTransformator );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RuleTransformator );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_assign_source_30 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_30, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_30;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_31 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_33 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_33;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_5;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;

                goto try_except_handler_5;
            }
            tmp_tuple_element_2 = const_str_plain_StripInterpretationTransformator;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 34;
            tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_34;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;

                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 34;

                    goto try_except_handler_5;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 34;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 34;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 34;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_35;
            tmp_assign_source_35 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_35;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_36;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_grammars$organisation_34 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_09460345ee7f4232b97d5c10b6119bc9;
        tmp_res = PyObject_SetItem( locals_grammars$organisation_34, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_StripInterpretationTransformator;
        tmp_res = PyObject_SetItem( locals_grammars$organisation_34, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;

            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_98540a4ae4f554b7ee743fb1c6417294_2, codeobj_98540a4ae4f554b7ee743fb1c6417294, module_grammars$organisation, sizeof(void *) );
        frame_98540a4ae4f554b7ee743fb1c6417294_2 = cache_frame_98540a4ae4f554b7ee743fb1c6417294_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_98540a4ae4f554b7ee743fb1c6417294_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_98540a4ae4f554b7ee743fb1c6417294_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_grammars$organisation$$$function_1_visit_InterpretationRule(  );



        tmp_res = PyObject_SetItem( locals_grammars$organisation_34, const_str_plain_visit_InterpretationRule, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_98540a4ae4f554b7ee743fb1c6417294_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_98540a4ae4f554b7ee743fb1c6417294_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_98540a4ae4f554b7ee743fb1c6417294_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_98540a4ae4f554b7ee743fb1c6417294_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_98540a4ae4f554b7ee743fb1c6417294_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_98540a4ae4f554b7ee743fb1c6417294_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_98540a4ae4f554b7ee743fb1c6417294_2 == cache_frame_98540a4ae4f554b7ee743fb1c6417294_2 )
        {
            Py_DECREF( frame_98540a4ae4f554b7ee743fb1c6417294_2 );
        }
        cache_frame_98540a4ae4f554b7ee743fb1c6417294_2 = NULL;

        assertFrameObject( frame_98540a4ae4f554b7ee743fb1c6417294_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_7;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;

                goto try_except_handler_7;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_grammars$organisation_34, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;

                goto try_except_handler_7;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_StripInterpretationTransformator;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_grammars$organisation_34;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 34;
            tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;

                goto try_except_handler_7;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_37;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_36 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_36 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$organisation );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_grammars$organisation_34 );
        locals_grammars$organisation_34 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_grammars$organisation_34 );
        locals_grammars$organisation_34 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$organisation );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( grammars$organisation );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 34;
        goto try_except_handler_5;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_StripInterpretationTransformator, tmp_assign_source_36 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIMPLE_NAME );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SIMPLE_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_4;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_transform );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_StripInterpretationTransformator );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_StripInterpretationTransformator );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "StripInterpretationTransformator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_5;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_38 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_NAME, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_POSITION_NAME );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_POSITION_NAME );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "POSITION_NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_6;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_transform );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_StripInterpretationTransformator );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_StripInterpretationTransformator );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "StripInterpretationTransformator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_7;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_PERSON, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_8;
        tmp_call_arg_element_1 = const_str_plain_Organisation;
        tmp_call_arg_element_2 = LIST_COPY( const_list_str_plain_name_list );
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 43;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_40 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_Organisation, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_arg_element_3;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_morph_pipeline );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_morph_pipeline );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "morph_pipeline" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_9;
        tmp_call_arg_element_3 = LIST_COPY( const_list_2c5f87a482b9eb949bd53888ea508bc0_list );
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_call_arg_element_3 };
            tmp_assign_source_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_call_arg_element_3 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TYPE, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gnc_relation );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc_relation );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_10;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 153;
        tmp_assign_source_42 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gnc, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_11;
        PyObject *tmp_source_name_7;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_16;
        PyObject *tmp_source_name_8;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_23;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_11;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 155;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_12;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_13;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_14;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 156;
        tmp_source_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_match );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_called_name_11 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = tmp_mvar_value_15;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 157;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_16;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_true );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_true );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "true" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_17;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 158;
        tmp_args_element_name_8 = CALL_FUNCTION_NO_ARGS( tmp_called_name_14 );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 158;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_18;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 159;
        tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_chr_45_tuple, 0 ) );

        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_19;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 160;
        tmp_source_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_ADJF_tuple, 0 ) );

        if ( tmp_source_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_match );
        Py_DECREF( tmp_source_name_8 );
        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gnc );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gnc );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );
            Py_DECREF( tmp_called_name_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gnc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_11 = tmp_mvar_value_20;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );
            Py_DECREF( tmp_args_element_name_8 );
            Py_DECREF( tmp_args_element_name_9 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 157;
        {
            PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
            tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_args_element_name_8 );
        Py_DECREF( tmp_args_element_name_9 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_4 );

            exception_lineno = 157;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 155;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_7 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_args_element_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_21;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_args_element_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_22;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 163;
        tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, &PyTuple_GET_ITEM( const_tuple_str_chr_1080_tuple, 0 ) );

        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_23;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 163;
        tmp_args_element_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_str_chr_44_tuple, 0 ) );

        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_13 );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_called_instance_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_args_element_name_13 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 163;
        tmp_args_element_name_12 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 163;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 154;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_12 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 154;
        tmp_assign_source_43 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_ADJF_PREFIX, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_24;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_case_relation );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_case_relation );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "case_relation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_24;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 166;
        tmp_assign_source_44 = CALL_FUNCTION_NO_ARGS( tmp_called_name_21 );
        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_case, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_name_23;
        PyObject *tmp_source_name_9;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_mvar_value_27;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_25;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_26;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 168;
        tmp_source_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, &PyTuple_GET_ITEM( const_tuple_str_plain_gent_tuple, 0 ) );

        if ( tmp_source_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;

            goto frame_exception_exit_1;
        }
        tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_match );
        Py_DECREF( tmp_source_name_9 );
        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_case );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_case );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_called_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "case" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 168;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_16 = tmp_mvar_value_27;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 168;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 167;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_called_instance_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 167;
        tmp_called_instance_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 167;
        tmp_assign_source_45 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_GENT_GROUP, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_mvar_value_36;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_28 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 171;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_28;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TYPE );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TYPE );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TYPE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_17 = tmp_mvar_value_29;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_30 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_30;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_19 = tmp_mvar_value_31;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 173;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_args_element_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
        }

        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_args_element_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_32;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_args_element_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_called_name_28 = tmp_mvar_value_33;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_args_element_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_22 = tmp_mvar_value_34;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 174;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_18 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 174;
        {
            PyObject *call_args[] = { tmp_args_element_name_21 };
            tmp_called_instance_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
        }

        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_called_instance_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_18 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 174;
        tmp_args_element_name_20 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_5 );
        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_18 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_35;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_24 = tmp_mvar_value_36;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_args_element_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_29, call_args );
        }

        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_18 );
            Py_DECREF( tmp_args_element_name_20 );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 171;
        {
            PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18, tmp_args_element_name_20, tmp_args_element_name_23 };
            tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_args_element_name_18 );
        Py_DECREF( tmp_args_element_name_20 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_called_name_31;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_called_name_32;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_called_name_35;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_mvar_value_50;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 178;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_37;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TYPE );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TYPE );
        }

        if ( tmp_mvar_value_38 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TYPE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 179;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_25 = tmp_mvar_value_38;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_39 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 180;

            goto frame_exception_exit_1;
        }

        tmp_called_name_31 = tmp_mvar_value_39;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_40 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 180;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_27 = tmp_mvar_value_40;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 180;
        {
            PyObject *call_args[] = { tmp_args_element_name_27 };
            tmp_args_element_name_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, call_args );
        }

        if ( tmp_args_element_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_called_name_32 = tmp_mvar_value_41;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_called_name_33 = tmp_mvar_value_42;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_30 = tmp_mvar_value_43;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, call_args );
        }

        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_29 };
            tmp_called_instance_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_args_element_name_29 );
        if ( tmp_called_instance_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 181;
        tmp_args_element_name_28 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_6 );
        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_44;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_32 = tmp_mvar_value_45;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 182;
        {
            PyObject *call_args[] = { tmp_args_element_name_32 };
            tmp_args_element_name_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, call_args );
        }

        if ( tmp_args_element_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_not_ );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_not_ );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "not_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_called_name_35 = tmp_mvar_value_46;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_47 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_47;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_35 = tmp_mvar_value_48;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_args_element_name_35 };
            tmp_args_element_name_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, call_args );
        }

        if ( tmp_args_element_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_args_element_name_34 };
            tmp_called_instance_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_35, call_args );
        }

        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 183;
        tmp_args_element_name_33 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_args_element_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );
            Py_DECREF( tmp_args_element_name_33 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;

            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_49;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTES );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTES );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );
            Py_DECREF( tmp_args_element_name_33 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 184;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_37 = tmp_mvar_value_50;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 184;
        {
            PyObject *call_args[] = { tmp_args_element_name_37 };
            tmp_args_element_name_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, call_args );
        }

        if ( tmp_args_element_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_26 );
            Py_DECREF( tmp_args_element_name_28 );
            Py_DECREF( tmp_args_element_name_31 );
            Py_DECREF( tmp_args_element_name_33 );

            exception_lineno = 184;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 178;
        {
            PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26, tmp_args_element_name_28, tmp_args_element_name_31, tmp_args_element_name_33, tmp_args_element_name_36 };
            tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_30, call_args );
        }

        Py_DECREF( tmp_args_element_name_26 );
        Py_DECREF( tmp_args_element_name_28 );
        Py_DECREF( tmp_args_element_name_31 );
        Py_DECREF( tmp_args_element_name_33 );
        Py_DECREF( tmp_args_element_name_36 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 178;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TRIPLE_QUOTED, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_called_name_38;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_mvar_value_53;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_51 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 187;

            goto frame_exception_exit_1;
        }

        tmp_called_name_38 = tmp_mvar_value_51;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_ADJF_PREFIX );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ADJF_PREFIX );
        }

        if ( tmp_mvar_value_52 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ADJF_PREFIX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 188;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_38 = tmp_mvar_value_52;
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTED );
        }

        if ( tmp_mvar_value_53 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTED" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_39 = tmp_mvar_value_53;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 187;
        {
            PyObject *call_args[] = { tmp_args_element_name_38, tmp_args_element_name_39 };
            tmp_assign_source_48 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_38, call_args );
        }

        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED_WITH_ADJF_PREFIX, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_mvar_value_56;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_54 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;

            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_54;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_ADJF_PREFIX );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ADJF_PREFIX );
        }

        if ( tmp_mvar_value_55 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ADJF_PREFIX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_40 = tmp_mvar_value_55;
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TYPE );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TYPE );
        }

        if ( tmp_mvar_value_56 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TYPE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_41 = tmp_mvar_value_56;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 192;
        {
            PyObject *call_args[] = { tmp_args_element_name_40, tmp_args_element_name_41 };
            tmp_assign_source_49 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_39, call_args );
        }

        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_BASIC, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_called_name_40;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_args_element_name_42;
        PyObject *tmp_called_name_41;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_args_element_name_48;
        PyObject *tmp_called_name_43;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_args_element_name_49;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_args_element_name_50;
        PyObject *tmp_called_name_45;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_args_element_name_51;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_args_element_name_52;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_called_name_47;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_args_element_name_53;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_args_element_name_54;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_args_element_name_55;
        PyObject *tmp_mvar_value_71;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_57 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }

        tmp_called_name_40 = tmp_mvar_value_57;
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_58 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 198;

            goto frame_exception_exit_1;
        }

        tmp_called_name_41 = tmp_mvar_value_58;
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTED );
        }

        if ( tmp_mvar_value_59 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTED" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_43 = tmp_mvar_value_59;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED_WITH_ADJF_PREFIX );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTED_WITH_ADJF_PREFIX );
        }

        if ( tmp_mvar_value_60 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTED_WITH_ADJF_PREFIX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 200;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_44 = tmp_mvar_value_60;
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_BASIC );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BASIC );
        }

        CHECK_OBJECT( tmp_mvar_value_61 );
        tmp_args_element_name_45 = tmp_mvar_value_61;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 198;
        {
            PyObject *call_args[] = { tmp_args_element_name_43, tmp_args_element_name_44, tmp_args_element_name_45 };
            tmp_args_element_name_42 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_41, call_args );
        }

        if ( tmp_args_element_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_GENT_GROUP );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_GENT_GROUP );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "GENT_GROUP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_46 = tmp_mvar_value_62;
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 204;

            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_63;
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }

        tmp_called_name_43 = tmp_mvar_value_64;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_65;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 205;
        tmp_args_element_name_49 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_44, &PyTuple_GET_ITEM( const_tuple_str_digest_aab517adf3a2a34bb0496eeca2922a8e_tuple, 0 ) );

        if ( tmp_args_element_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 205;
        {
            PyObject *call_args[] = { tmp_args_element_name_49 };
            tmp_args_element_name_48 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_43, call_args );
        }

        Py_DECREF( tmp_args_element_name_49 );
        if ( tmp_args_element_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }

        tmp_called_name_45 = tmp_mvar_value_66;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_67;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 206;
        tmp_args_element_name_51 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_46, &PyTuple_GET_ITEM( const_tuple_str_digest_b356db3f83f00c40fb4d261edfeed58e_tuple, 0 ) );

        if ( tmp_args_element_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }

        tmp_called_name_47 = tmp_mvar_value_68;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 206;
        tmp_called_instance_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_47, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_called_instance_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 206;
        tmp_args_element_name_52 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_8 );
        if ( tmp_args_element_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );
            Py_DECREF( tmp_args_element_name_51 );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 206;
        {
            PyObject *call_args[] = { tmp_args_element_name_51, tmp_args_element_name_52 };
            tmp_args_element_name_50 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_45, call_args );
        }

        Py_DECREF( tmp_args_element_name_51 );
        Py_DECREF( tmp_args_element_name_52 );
        if ( tmp_args_element_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_48 );

            exception_lineno = 206;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 204;
        {
            PyObject *call_args[] = { tmp_args_element_name_48, tmp_args_element_name_50 };
            tmp_args_element_name_47 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_42, call_args );
        }

        Py_DECREF( tmp_args_element_name_48 );
        Py_DECREF( tmp_args_element_name_50 );
        if ( tmp_args_element_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );

            exception_lineno = 204;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_47 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 208;

            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_69;
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_NAME );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAME );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_47 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAME" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 209;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_54 = tmp_mvar_value_70;
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_PERSON );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PERSON );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_47 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PERSON" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 210;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_55 = tmp_mvar_value_71;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 208;
        {
            PyObject *call_args[] = { tmp_args_element_name_54, tmp_args_element_name_55 };
            tmp_args_element_name_53 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_48, call_args );
        }

        if ( tmp_args_element_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_42 );
            Py_DECREF( tmp_args_element_name_47 );

            exception_lineno = 208;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 197;
        {
            PyObject *call_args[] = { tmp_args_element_name_42, tmp_args_element_name_46, tmp_args_element_name_47, tmp_args_element_name_53 };
            tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_40, call_args );
        }

        Py_DECREF( tmp_args_element_name_42 );
        Py_DECREF( tmp_args_element_name_47 );
        Py_DECREF( tmp_args_element_name_53 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_NAMED, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_called_name_49;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_args_element_name_56;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_called_instance_9;
        PyObject *tmp_called_name_50;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_args_element_name_58;
        PyObject *tmp_called_name_51;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_args_element_name_59;
        PyObject *tmp_called_name_52;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_args_element_name_60;
        PyObject *tmp_called_name_53;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_args_element_name_61;
        PyObject *tmp_called_name_54;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_args_element_name_62;
        PyObject *tmp_called_name_55;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_args_element_name_63;
        PyObject *tmp_called_name_56;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_args_element_name_64;
        PyObject *tmp_called_name_57;
        PyObject *tmp_mvar_value_81;
        PyObject *tmp_call_arg_element_4;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_called_name_58;
        PyObject *tmp_mvar_value_82;
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_72 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 214;

            goto frame_exception_exit_1;
        }

        tmp_called_name_49 = tmp_mvar_value_72;
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TYPE );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TYPE );
        }

        if ( tmp_mvar_value_73 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TYPE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 215;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_56 = tmp_mvar_value_73;
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_74 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;

            goto frame_exception_exit_1;
        }

        tmp_called_name_50 = tmp_mvar_value_74;
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_75 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 217;

            goto frame_exception_exit_1;
        }

        tmp_called_name_51 = tmp_mvar_value_75;
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_76 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 218;

            goto frame_exception_exit_1;
        }

        tmp_called_name_52 = tmp_mvar_value_76;
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_type );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_type );
        }

        if ( tmp_mvar_value_77 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 219;

            goto frame_exception_exit_1;
        }

        tmp_called_name_53 = tmp_mvar_value_77;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 219;
        tmp_args_element_name_60 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_53, &PyTuple_GET_ITEM( const_tuple_str_plain_LATIN_tuple, 0 ) );

        if ( tmp_args_element_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_is_capitalized );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_capitalized );
        }

        if ( tmp_mvar_value_78 == NULL )
        {
            Py_DECREF( tmp_args_element_name_60 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_capitalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 220;

            goto frame_exception_exit_1;
        }

        tmp_called_name_54 = tmp_mvar_value_78;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 220;
        tmp_args_element_name_61 = CALL_FUNCTION_NO_ARGS( tmp_called_name_54 );
        if ( tmp_args_element_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_60 );

            exception_lineno = 220;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 218;
        {
            PyObject *call_args[] = { tmp_args_element_name_60, tmp_args_element_name_61 };
            tmp_args_element_name_59 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_52, call_args );
        }

        Py_DECREF( tmp_args_element_name_60 );
        Py_DECREF( tmp_args_element_name_61 );
        if ( tmp_args_element_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 217;
        {
            PyObject *call_args[] = { tmp_args_element_name_59 };
            tmp_args_element_name_58 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_51, call_args );
        }

        Py_DECREF( tmp_args_element_name_59 );
        if ( tmp_args_element_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 217;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_79 == NULL )
        {
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 223;

            goto frame_exception_exit_1;
        }

        tmp_called_name_55 = tmp_mvar_value_79;
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_type );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_type );
        }

        if ( tmp_mvar_value_80 == NULL )
        {
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 224;

            goto frame_exception_exit_1;
        }

        tmp_called_name_56 = tmp_mvar_value_80;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 224;
        tmp_args_element_name_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_56, &PyTuple_GET_ITEM( const_tuple_str_plain_LATIN_tuple, 0 ) );

        if ( tmp_args_element_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_58 );

            exception_lineno = 224;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_args_element_name_58 );
            Py_DECREF( tmp_args_element_name_63 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 225;

            goto frame_exception_exit_1;
        }

        tmp_called_name_57 = tmp_mvar_value_81;
        tmp_call_arg_element_4 = PySet_New( const_set_e87e07f414d4699c101fb9279e3b145e );
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 225;
        {
            PyObject *call_args[] = { tmp_call_arg_element_4 };
            tmp_args_element_name_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_57, call_args );
        }

        Py_DECREF( tmp_call_arg_element_4 );
        if ( tmp_args_element_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_58 );
            Py_DECREF( tmp_args_element_name_63 );

            exception_lineno = 225;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_type );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_type );
        }

        if ( tmp_mvar_value_82 == NULL )
        {
            Py_DECREF( tmp_args_element_name_58 );
            Py_DECREF( tmp_args_element_name_63 );
            Py_DECREF( tmp_args_element_name_64 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 226;

            goto frame_exception_exit_1;
        }

        tmp_called_name_58 = tmp_mvar_value_82;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 226;
        tmp_args_element_name_65 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_58, &PyTuple_GET_ITEM( const_tuple_str_plain_LATIN_tuple, 0 ) );

        if ( tmp_args_element_name_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_58 );
            Py_DECREF( tmp_args_element_name_63 );
            Py_DECREF( tmp_args_element_name_64 );

            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 223;
        {
            PyObject *call_args[] = { tmp_args_element_name_63, tmp_args_element_name_64, tmp_args_element_name_65 };
            tmp_args_element_name_62 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_55, call_args );
        }

        Py_DECREF( tmp_args_element_name_63 );
        Py_DECREF( tmp_args_element_name_64 );
        Py_DECREF( tmp_args_element_name_65 );
        if ( tmp_args_element_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_58 );

            exception_lineno = 223;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 216;
        {
            PyObject *call_args[] = { tmp_args_element_name_58, tmp_args_element_name_62 };
            tmp_called_instance_9 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_50, call_args );
        }

        Py_DECREF( tmp_args_element_name_58 );
        Py_DECREF( tmp_args_element_name_62 );
        if ( tmp_called_instance_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 216;
        tmp_args_element_name_57 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_repeatable );
        Py_DECREF( tmp_called_instance_9 );
        if ( tmp_args_element_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 214;
        {
            PyObject *call_args[] = { tmp_args_element_name_56, tmp_args_element_name_57 };
            tmp_assign_source_51 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_49, call_args );
        }

        Py_DECREF( tmp_args_element_name_57 );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 214;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_LATIN, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_called_name_59;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_called_name_60;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_args_element_name_67;
        PyObject *tmp_mvar_value_85;
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_83 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 231;

            goto frame_exception_exit_1;
        }

        tmp_called_name_59 = tmp_mvar_value_83;
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_84 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 232;

            goto frame_exception_exit_1;
        }

        tmp_called_name_60 = tmp_mvar_value_84;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 232;
        tmp_args_element_name_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_60, &PyTuple_GET_ITEM( const_tuple_str_plain_Orgn_tuple, 0 ) );

        if ( tmp_args_element_name_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_GENT_GROUP );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_GENT_GROUP );
        }

        if ( tmp_mvar_value_85 == NULL )
        {
            Py_DECREF( tmp_args_element_name_66 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "GENT_GROUP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_67 = tmp_mvar_value_85;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 231;
        {
            PyObject *call_args[] = { tmp_args_element_name_66, tmp_args_element_name_67 };
            tmp_assign_source_52 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_59, call_args );
        }

        Py_DECREF( tmp_args_element_name_66 );
        if ( tmp_assign_source_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 231;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_KNOWN, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_called_name_61;
        PyObject *tmp_mvar_value_86;
        PyObject *tmp_args_element_name_68;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_args_element_name_69;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_args_element_name_70;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_args_element_name_71;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_args_element_name_72;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_args_element_name_73;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_args_element_name_74;
        PyObject *tmp_mvar_value_93;
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_86 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 236;

            goto frame_exception_exit_1;
        }

        tmp_called_name_61 = tmp_mvar_value_86;
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_TRIPLE_QUOTED );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TRIPLE_QUOTED );
        }

        if ( tmp_mvar_value_87 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TRIPLE_QUOTED" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 237;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_68 = tmp_mvar_value_87;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTED );
        }

        if ( tmp_mvar_value_88 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTED" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_69 = tmp_mvar_value_88;
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_QUOTED_WITH_ADJF_PREFIX );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_QUOTED_WITH_ADJF_PREFIX );
        }

        if ( tmp_mvar_value_89 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "QUOTED_WITH_ADJF_PREFIX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 239;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_70 = tmp_mvar_value_89;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_BASIC );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BASIC );
        }

        if ( tmp_mvar_value_90 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BASIC" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_71 = tmp_mvar_value_90;
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_NAMED );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NAMED );
        }

        if ( tmp_mvar_value_91 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NAMED" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 241;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_72 = tmp_mvar_value_91;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_LATIN );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LATIN );
        }

        if ( tmp_mvar_value_92 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LATIN" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_73 = tmp_mvar_value_92;
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_KNOWN );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KNOWN );
        }

        CHECK_OBJECT( tmp_mvar_value_93 );
        tmp_args_element_name_74 = tmp_mvar_value_93;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 236;
        {
            PyObject *call_args[] = { tmp_args_element_name_68, tmp_args_element_name_69, tmp_args_element_name_70, tmp_args_element_name_71, tmp_args_element_name_72, tmp_args_element_name_73, tmp_args_element_name_74 };
            tmp_assign_source_53 = CALL_FUNCTION_WITH_ARGS7( tmp_called_name_61, call_args );
        }

        if ( tmp_assign_source_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 236;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_ORGANISATION_, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_called_name_62;
        PyObject *tmp_source_name_10;
        PyObject *tmp_called_name_63;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_args_element_name_75;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_args_element_name_76;
        PyObject *tmp_mvar_value_96;
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_ORGANISATION_ );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ORGANISATION_ );
        }

        CHECK_OBJECT( tmp_mvar_value_94 );
        tmp_source_name_11 = tmp_mvar_value_94;
        tmp_called_name_63 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_interpretation );
        if ( tmp_called_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_Organisation );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Organisation );
        }

        if ( tmp_mvar_value_95 == NULL )
        {
            Py_DECREF( tmp_called_name_63 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Organisation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 247;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_95;
        tmp_args_element_name_75 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_name );
        if ( tmp_args_element_name_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_63 );

            exception_lineno = 247;

            goto frame_exception_exit_1;
        }
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 246;
        {
            PyObject *call_args[] = { tmp_args_element_name_75 };
            tmp_source_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_63, call_args );
        }

        Py_DECREF( tmp_called_name_63 );
        Py_DECREF( tmp_args_element_name_75 );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto frame_exception_exit_1;
        }
        tmp_called_name_62 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_called_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_Organisation );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Organisation );
        }

        if ( tmp_mvar_value_96 == NULL )
        {
            Py_DECREF( tmp_called_name_62 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Organisation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_76 = tmp_mvar_value_96;
        frame_1dffb6e1762a215e07418c24e99f24f0->m_frame.f_lineno = 246;
        {
            PyObject *call_args[] = { tmp_args_element_name_76 };
            tmp_assign_source_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_62, call_args );
        }

        Py_DECREF( tmp_called_name_62 );
        if ( tmp_assign_source_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$organisation, (Nuitka_StringObject *)const_str_plain_ORGANISATION, tmp_assign_source_54 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1dffb6e1762a215e07418c24e99f24f0 );
#endif
    popFrameStack();

    assertFrameObject( frame_1dffb6e1762a215e07418c24e99f24f0 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1dffb6e1762a215e07418c24e99f24f0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1dffb6e1762a215e07418c24e99f24f0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1dffb6e1762a215e07418c24e99f24f0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1dffb6e1762a215e07418c24e99f24f0, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_grammars$organisation );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
