/* Generated code for Python module 'matplotlib.tri.tripcolor'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$tri$tripcolor" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$tri$tripcolor;
PyDictObject *moduledict_matplotlib$tri$tripcolor;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_grid;
extern PyObject *const_tuple_float_0_25_tuple;
extern PyObject *const_dict_22b54c5c9112a5c8c51dc4a23fd6481b;
extern PyObject *const_str_plain_set_alpha;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_list_str_plain_flat_str_plain_gouraud_list_tuple;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_set_array;
extern PyObject *const_str_plain_max;
extern PyObject *const_str_plain_gouraud;
extern PyObject *const_str_plain_stack;
extern PyObject *const_str_plain_PolyCollection;
static PyObject *const_str_digest_c4bf50f886519643d93a69638824c9ae;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_set_cmap;
extern PyObject *const_str_plain_cmap;
extern PyObject *const_str_plain_linewidths;
extern PyObject *const_str_digest_0771b85ee859c0a4971fcb369a1ddb4d;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_add_collection;
extern PyObject *const_float_0_25;
extern PyObject *const_str_digest_56aa55135057d4e1eddba0c96f175966;
static PyObject *const_str_digest_3dfbcf582f91c9c2ecbeb1f07d94c4cb;
extern PyObject *const_str_plain_autoscale_view;
extern PyObject *const_dict_45f01d790dd247ccf5429943c7c59cc7;
extern PyObject *const_str_plain_set_norm;
static PyObject *const_str_digest_e8dd1e82cb61cf64198048eb017fbcd7;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_lower;
extern PyObject *const_str_plain___debug__;
static PyObject *const_list_str_plain_flat_str_plain_gouraud_list;
extern PyObject *const_str_plain_update_datalim;
extern PyObject *const_str_plain_get_from_args_and_kwargs;
extern PyObject *const_tuple_str_plain_linewidth_tuple;
extern PyObject *const_str_plain_matplotlib;
static PyObject *const_str_digest_b57e5d1973c167a31853bc52e3633ae6;
static PyObject *const_tuple_str_plain_PolyCollection_str_plain_TriMesh_tuple;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_antialiased;
extern PyObject *const_str_plain_none;
extern PyObject *const_str_plain_min;
static PyObject *const_str_digest_8865fc010038957aa57fc97e8d9985c2;
extern PyObject *const_str_plain_antialiaseds;
extern PyObject *const_float_1_0;
extern PyObject *const_str_plain_edgecolors;
extern PyObject *const_str_plain_alpha;
extern PyObject *const_str_plain_mask;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_Normalize;
extern PyObject *const_str_plain_norm;
extern PyObject *const_str_plain_asarray;
extern PyObject *const_str_plain_triangles;
extern PyObject *const_tuple_str_plain_antialiased_tuple;
extern PyObject *const_str_plain_cbook;
extern PyObject *const_tuple_str_plain_edgecolors_str_plain_none_tuple;
extern PyObject *const_tuple_str_plain_cbook_tuple;
extern PyObject *const_str_plain_vmin;
extern PyObject *const_str_plain_TriMesh;
extern PyObject *const_str_plain_facecolors;
extern PyObject *const_str_plain_False;
extern PyObject *const_tuple_str_plain_Normalize_tuple;
extern PyObject *const_str_plain_pop;
extern PyObject *const_str_plain_set_clim;
extern PyObject *const_int_0;
extern PyObject *const_tuple_str_plain_edgecolor_tuple;
extern PyObject *const_str_plain_Triangulation;
extern PyObject *const_str_plain_tripcolor;
extern PyObject *const_str_plain__check_in_list;
extern PyObject *const_str_plain_autoscale_None;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_164047f92223e0e1f00801cccc1730c3;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_plain_vmax;
extern PyObject *const_str_plain_setdefault;
extern PyObject *const_tuple_str_plain_linewidths_tuple_float_0_25_tuple_tuple;
static PyObject *const_tuple_afe17bd85ce03b02415970a1d6c78afc_tuple;
extern PyObject *const_str_plain_flat;
static PyObject *const_str_digest_2408b8dc88f77364c97d43120d99ce81;
extern PyObject *const_str_plain_edgecolor;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_tuple_str_plain_Triangulation_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_y;
extern PyObject *const_str_plain_axis;
static PyObject *const_str_digest_1544171cc85d8d0c8e1f63976c522d14;
extern PyObject *const_str_plain_get_masked_triangles;
extern PyObject *const_dict_c4b01644824ba6bad132707fcdaa03f1;
extern PyObject *const_str_plain_mean;
extern PyObject *const_str_plain_np;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_digest_38fb7bc0e5fceab2d00d507c3db86934;
extern PyObject *const_str_plain_shading;
extern PyObject *const_str_plain_linewidth;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_list_str_plain_flat_str_plain_gouraud_list_tuple = PyTuple_New( 1 );
    const_list_str_plain_flat_str_plain_gouraud_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_flat_str_plain_gouraud_list, 0, const_str_plain_flat ); Py_INCREF( const_str_plain_flat );
    PyList_SET_ITEM( const_list_str_plain_flat_str_plain_gouraud_list, 1, const_str_plain_gouraud ); Py_INCREF( const_str_plain_gouraud );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_flat_str_plain_gouraud_list_tuple, 0, const_list_str_plain_flat_str_plain_gouraud_list ); Py_INCREF( const_list_str_plain_flat_str_plain_gouraud_list );
    const_str_digest_c4bf50f886519643d93a69638824c9ae = UNSTREAM_STRING_ASCII( &constant_bin[ 2664884 ], 24, 0 );
    const_str_digest_3dfbcf582f91c9c2ecbeb1f07d94c4cb = UNSTREAM_STRING_ASCII( &constant_bin[ 2664908 ], 60, 0 );
    const_str_digest_e8dd1e82cb61cf64198048eb017fbcd7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2664968 ], 103, 0 );
    const_str_digest_b57e5d1973c167a31853bc52e3633ae6 = UNSTREAM_STRING_ASCII( &constant_bin[ 2665071 ], 33, 0 );
    const_tuple_str_plain_PolyCollection_str_plain_TriMesh_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PolyCollection_str_plain_TriMesh_tuple, 0, const_str_plain_PolyCollection ); Py_INCREF( const_str_plain_PolyCollection );
    PyTuple_SET_ITEM( const_tuple_str_plain_PolyCollection_str_plain_TriMesh_tuple, 1, const_str_plain_TriMesh ); Py_INCREF( const_str_plain_TriMesh );
    const_str_digest_8865fc010038957aa57fc97e8d9985c2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2665104 ], 1485, 0 );
    const_str_digest_164047f92223e0e1f00801cccc1730c3 = UNSTREAM_STRING_ASCII( &constant_bin[ 2666589 ], 108, 0 );
    const_tuple_afe17bd85ce03b02415970a1d6c78afc_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2666697 ], 172 );
    const_str_digest_2408b8dc88f77364c97d43120d99ce81 = UNSTREAM_STRING_ASCII( &constant_bin[ 2666869 ], 27, 0 );
    const_str_digest_1544171cc85d8d0c8e1f63976c522d14 = UNSTREAM_STRING_ASCII( &constant_bin[ 2666896 ], 41, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$tri$tripcolor( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_5ad0a695b9e1539445f8ac8cbe882509;
static PyCodeObject *codeobj_61d84f715f2aeabcfcbc5a88835f20ba;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_2408b8dc88f77364c97d43120d99ce81 );
    codeobj_5ad0a695b9e1539445f8ac8cbe882509 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_b57e5d1973c167a31853bc52e3633ae6, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_61d84f715f2aeabcfcbc5a88835f20ba = MAKE_CODEOBJ( module_filename_obj, const_str_plain_tripcolor, 9, const_tuple_afe17bd85ce03b02415970a1d6c78afc_tuple, 1, 7, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_matplotlib$tri$tripcolor$$$function_1_tripcolor( PyObject *kw_defaults );


// The module function definitions.
static PyObject *impl_matplotlib$tri$tripcolor$$$function_1_tripcolor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_ax = python_pars[ 0 ];
    PyObject *par_alpha = python_pars[ 1 ];
    PyObject *par_norm = python_pars[ 2 ];
    PyObject *par_cmap = python_pars[ 3 ];
    PyObject *par_vmin = python_pars[ 4 ];
    PyObject *par_vmax = python_pars[ 5 ];
    PyObject *par_shading = python_pars[ 6 ];
    PyObject *par_facecolors = python_pars[ 7 ];
    PyObject *par_args = python_pars[ 8 ];
    PyObject *par_kwargs = python_pars[ 9 ];
    PyObject *var_tri = NULL;
    PyObject *var_C = NULL;
    PyObject *var_ec = NULL;
    PyObject *var_collection = NULL;
    PyObject *var_maskedTris = NULL;
    PyObject *var_verts = NULL;
    PyObject *var_minx = NULL;
    PyObject *var_maxx = NULL;
    PyObject *var_miny = NULL;
    PyObject *var_maxy = NULL;
    PyObject *var_corners = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_61d84f715f2aeabcfcbc5a88835f20ba;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_61d84f715f2aeabcfcbc5a88835f20ba = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_61d84f715f2aeabcfcbc5a88835f20ba, codeobj_61d84f715f2aeabcfcbc5a88835f20ba, module_matplotlib$tri$tripcolor, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_61d84f715f2aeabcfcbc5a88835f20ba = cache_frame_61d84f715f2aeabcfcbc5a88835f20ba;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_61d84f715f2aeabcfcbc5a88835f20ba );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_61d84f715f2aeabcfcbc5a88835f20ba ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_cbook );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cbook );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cbook" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__check_in_list );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = DEEP_COPY( const_tuple_list_str_plain_flat_str_plain_gouraud_list_tuple );
        tmp_dict_key_1 = const_str_plain_shading;
        CHECK_OBJECT( par_shading );
        tmp_dict_value_1 = par_shading;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 50;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_Triangulation );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Triangulation );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Triangulation" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto try_except_handler_2;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get_from_args_and_kwargs );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_iter_arg_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooNNooooooooo";
            exception_lineno = 52;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooNNooooooooo";
            exception_lineno = 52;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooNNooooooooo";
            exception_lineno = 52;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_3 == NULL );
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooNNooooooooo";
                    exception_lineno = 52;
                    goto try_except_handler_3;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooooNNooooooooo";
            exception_lineno = 52;
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
        assert( var_tri == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_tri = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = par_args;
            assert( old != NULL );
            par_args = tmp_assign_source_6;
            Py_INCREF( par_args );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_3;
        {
            PyObject *old = par_kwargs;
            assert( old != NULL );
            par_kwargs = tmp_assign_source_7;
            Py_INCREF( par_kwargs );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_facecolors );
        tmp_compexpr_left_1 = par_facecolors;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( par_facecolors );
            tmp_assign_source_8 = par_facecolors;
            assert( var_C == NULL );
            Py_INCREF( tmp_assign_source_8 );
            var_C = tmp_assign_source_8;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_np );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 60;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = tmp_mvar_value_3;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_asarray );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_args );
            tmp_subscribed_name_1 = par_args;
            tmp_subscript_name_1 = const_int_0;
            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 60;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 60;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_C == NULL );
            var_C = tmp_assign_source_9;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_len_arg_2;
        PyObject *tmp_source_name_4;
        int tmp_and_left_truth_3;
        nuitka_bool tmp_and_left_value_3;
        nuitka_bool tmp_and_right_value_3;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_len_arg_3;
        PyObject *tmp_len_arg_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        CHECK_OBJECT( par_facecolors );
        tmp_compexpr_left_2 = par_facecolors;
        tmp_compexpr_right_2 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_C );
        tmp_len_arg_1 = var_C;
        tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tri );
        tmp_source_name_4 = var_tri;
        tmp_len_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_triangles );
        if ( tmp_len_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_3 );

            exception_lineno = 66;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_3 = BUILTIN_LEN( tmp_len_arg_2 );
        Py_DECREF( tmp_len_arg_2 );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_3 );

            exception_lineno = 66;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        assert( !(tmp_res == -1) );
        tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        CHECK_OBJECT( var_C );
        tmp_len_arg_3 = var_C;
        tmp_compexpr_left_4 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tri );
        tmp_source_name_5 = var_tri;
        tmp_len_arg_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_x );
        if ( tmp_len_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_4 );

            exception_lineno = 67;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = BUILTIN_LEN( tmp_len_arg_4 );
        Py_DECREF( tmp_len_arg_4 );
        if ( tmp_compexpr_right_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_4 );

            exception_lineno = 67;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        Py_DECREF( tmp_compexpr_right_4 );
        assert( !(tmp_res == -1) );
        tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_3 == 1 )
        {
            goto and_right_3;
        }
        else
        {
            goto and_left_3;
        }
        and_right_3:;
        CHECK_OBJECT( par_shading );
        tmp_compexpr_left_5 = par_shading;
        tmp_compexpr_right_5 = const_str_plain_gouraud;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_right_value_2 = tmp_and_right_value_3;
        goto and_end_3;
        and_left_3:;
        tmp_and_right_value_2 = tmp_and_left_value_3;
        and_end_3:;
        tmp_and_right_value_1 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_and_right_value_1 = tmp_and_left_value_2;
        and_end_2:;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_10;
            CHECK_OBJECT( var_C );
            tmp_assign_source_10 = var_C;
            {
                PyObject *old = par_facecolors;
                assert( old != NULL );
                par_facecolors = tmp_assign_source_10;
                Py_INCREF( par_facecolors );
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        int tmp_and_left_truth_4;
        nuitka_bool tmp_and_left_value_4;
        nuitka_bool tmp_and_right_value_4;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        PyObject *tmp_len_arg_5;
        PyObject *tmp_len_arg_6;
        PyObject *tmp_source_name_6;
        int tmp_and_left_truth_5;
        nuitka_bool tmp_and_left_value_5;
        nuitka_bool tmp_and_right_value_5;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        PyObject *tmp_compexpr_left_9;
        PyObject *tmp_compexpr_right_9;
        PyObject *tmp_len_arg_7;
        PyObject *tmp_len_arg_8;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( par_facecolors );
        tmp_compexpr_left_6 = par_facecolors;
        tmp_compexpr_right_6 = Py_None;
        tmp_and_left_value_4 = ( tmp_compexpr_left_6 == tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_4 = tmp_and_left_value_4 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_4 == 1 )
        {
            goto and_right_4;
        }
        else
        {
            goto and_left_4;
        }
        and_right_4:;
        CHECK_OBJECT( var_C );
        tmp_len_arg_5 = var_C;
        tmp_compexpr_left_7 = BUILTIN_LEN( tmp_len_arg_5 );
        if ( tmp_compexpr_left_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tri );
        tmp_source_name_6 = var_tri;
        tmp_len_arg_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_x );
        if ( tmp_len_arg_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_7 );

            exception_lineno = 71;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_7 = BUILTIN_LEN( tmp_len_arg_6 );
        Py_DECREF( tmp_len_arg_6 );
        if ( tmp_compexpr_right_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_7 );

            exception_lineno = 71;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
        Py_DECREF( tmp_compexpr_left_7 );
        Py_DECREF( tmp_compexpr_right_7 );
        assert( !(tmp_res == -1) );
        tmp_and_right_value_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_value_1 = tmp_and_right_value_4;
        goto and_end_4;
        and_left_4:;
        tmp_or_left_value_1 = tmp_and_left_value_4;
        and_end_4:;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_facecolors );
        tmp_compexpr_left_8 = par_facecolors;
        tmp_compexpr_right_8 = Py_None;
        tmp_and_left_value_5 = ( tmp_compexpr_left_8 != tmp_compexpr_right_8 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_5 = tmp_and_left_value_5 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_5 == 1 )
        {
            goto and_right_5;
        }
        else
        {
            goto and_left_5;
        }
        and_right_5:;
        CHECK_OBJECT( var_C );
        tmp_len_arg_7 = var_C;
        tmp_compexpr_left_9 = BUILTIN_LEN( tmp_len_arg_7 );
        if ( tmp_compexpr_left_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_tri );
        tmp_source_name_7 = var_tri;
        tmp_len_arg_8 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_triangles );
        if ( tmp_len_arg_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_9 );

            exception_lineno = 72;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_9 = BUILTIN_LEN( tmp_len_arg_8 );
        Py_DECREF( tmp_len_arg_8 );
        if ( tmp_compexpr_right_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_9 );

            exception_lineno = 72;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
        Py_DECREF( tmp_compexpr_left_9 );
        Py_DECREF( tmp_compexpr_right_9 );
        assert( !(tmp_res == -1) );
        tmp_and_right_value_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_right_value_1 = tmp_and_right_value_5;
        goto and_end_5;
        and_left_5:;
        tmp_or_right_value_1 = tmp_and_left_value_5;
        and_end_5:;
        tmp_condition_result_3 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_3 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_e8dd1e82cb61cf64198048eb017fbcd7;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 73;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 73;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_10;
        PyObject *tmp_compexpr_right_10;
        tmp_compexpr_left_10 = const_str_plain_linewidth;
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_10 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_10, tmp_compexpr_left_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( par_kwargs );
            tmp_called_instance_1 = par_kwargs;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 81;
            tmp_ass_subvalue_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_linewidth_tuple, 0 ) );

            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_1 = par_kwargs;
            tmp_ass_subscript_1 = const_str_plain_linewidths;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_4:;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_kwargs );
        tmp_called_instance_2 = par_kwargs;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 82;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_setdefault, &PyTuple_GET_ITEM( const_tuple_str_plain_linewidths_tuple_float_0_25_tuple_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_compexpr_left_11;
        PyObject *tmp_compexpr_right_11;
        tmp_compexpr_left_11 = const_str_plain_edgecolor;
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_11 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_11, tmp_compexpr_left_11 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_ass_subscript_2;
            CHECK_OBJECT( par_kwargs );
            tmp_called_instance_3 = par_kwargs;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 86;
            tmp_ass_subvalue_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_edgecolor_tuple, 0 ) );

            if ( tmp_ass_subvalue_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_2 = par_kwargs;
            tmp_ass_subscript_2 = const_str_plain_edgecolors;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            Py_DECREF( tmp_ass_subvalue_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_5:;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( par_kwargs );
        tmp_called_instance_4 = par_kwargs;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 87;
        tmp_assign_source_11 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_setdefault, &PyTuple_GET_ITEM( const_tuple_str_plain_edgecolors_str_plain_none_tuple, 0 ) );

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 87;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_ec == NULL );
        var_ec = tmp_assign_source_11;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_12;
        PyObject *tmp_compexpr_right_12;
        tmp_compexpr_left_12 = const_str_plain_antialiased;
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_12 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_12, tmp_compexpr_left_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_ass_subvalue_3;
            PyObject *tmp_called_instance_5;
            PyObject *tmp_ass_subscribed_3;
            PyObject *tmp_ass_subscript_3;
            CHECK_OBJECT( par_kwargs );
            tmp_called_instance_5 = par_kwargs;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 90;
            tmp_ass_subvalue_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_str_plain_antialiased_tuple, 0 ) );

            if ( tmp_ass_subvalue_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_3 = par_kwargs;
            tmp_ass_subscript_3 = const_str_plain_antialiaseds;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
            Py_DECREF( tmp_ass_subvalue_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_7;
        int tmp_and_left_truth_6;
        nuitka_bool tmp_and_left_value_6;
        nuitka_bool tmp_and_right_value_6;
        PyObject *tmp_compexpr_left_13;
        PyObject *tmp_compexpr_right_13;
        PyObject *tmp_compexpr_left_14;
        PyObject *tmp_compexpr_right_14;
        PyObject *tmp_called_instance_6;
        tmp_compexpr_left_13 = const_str_plain_antialiaseds;
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_13 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_13, tmp_compexpr_left_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_6 = tmp_and_left_value_6 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_6 == 1 )
        {
            goto and_right_6;
        }
        else
        {
            goto and_left_6;
        }
        and_right_6:;
        CHECK_OBJECT( var_ec );
        tmp_called_instance_6 = var_ec;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 91;
        tmp_compexpr_left_14 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_lower );
        if ( tmp_compexpr_left_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_14 = const_str_plain_none;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
        Py_DECREF( tmp_compexpr_left_14 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_7 = tmp_and_right_value_6;
        goto and_end_6;
        and_left_6:;
        tmp_condition_result_7 = tmp_and_left_value_6;
        and_end_6:;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_ass_subvalue_4;
            PyObject *tmp_ass_subscribed_4;
            PyObject *tmp_ass_subscript_4;
            tmp_ass_subvalue_4 = Py_False;
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_4 = par_kwargs;
            tmp_ass_subscript_4 = const_str_plain_antialiaseds;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_4, tmp_ass_subscript_4, tmp_ass_subvalue_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_7:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_compexpr_left_15;
        PyObject *tmp_compexpr_right_15;
        CHECK_OBJECT( par_shading );
        tmp_compexpr_left_15 = par_shading;
        tmp_compexpr_right_15 = const_str_plain_gouraud;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_15, tmp_compexpr_right_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_16;
            PyObject *tmp_compexpr_right_16;
            CHECK_OBJECT( par_facecolors );
            tmp_compexpr_left_16 = par_facecolors;
            tmp_compexpr_right_16 = Py_None;
            tmp_condition_result_9 = ( tmp_compexpr_left_16 != tmp_compexpr_right_16 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                tmp_make_exception_arg_2 = const_str_digest_3dfbcf582f91c9c2ecbeb1f07d94c4cb;
                frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 96;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 96;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            branch_no_9:;
        }
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_compexpr_left_17;
            PyObject *tmp_compexpr_right_17;
            PyObject *tmp_len_arg_9;
            PyObject *tmp_len_arg_10;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( var_C );
            tmp_len_arg_9 = var_C;
            tmp_compexpr_left_17 = BUILTIN_LEN( tmp_len_arg_9 );
            if ( tmp_compexpr_left_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_tri );
            tmp_source_name_8 = var_tri;
            tmp_len_arg_10 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_x );
            if ( tmp_len_arg_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_17 );

                exception_lineno = 98;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_17 = BUILTIN_LEN( tmp_len_arg_10 );
            Py_DECREF( tmp_len_arg_10 );
            if ( tmp_compexpr_right_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_17 );

                exception_lineno = 98;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_17, tmp_compexpr_right_17 );
            Py_DECREF( tmp_compexpr_left_17 );
            Py_DECREF( tmp_compexpr_right_17 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_10;
            }
            else
            {
                goto branch_no_10;
            }
            branch_yes_10:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_make_exception_arg_3;
                tmp_make_exception_arg_3 = const_str_digest_164047f92223e0e1f00801cccc1730c3;
                frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 99;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_3 };
                    tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                assert( !(tmp_raise_type_3 == NULL) );
                exception_type = tmp_raise_type_3;
                exception_lineno = 99;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            branch_no_10:;
        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_dircall_arg1_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_dircall_arg2_2;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_dircall_arg3_2;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_TriMesh );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TriMesh );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TriMesh" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 102;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_dircall_arg1_2 = tmp_mvar_value_4;
            CHECK_OBJECT( var_tri );
            tmp_tuple_element_1 = var_tri;
            tmp_dircall_arg2_2 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_dircall_arg2_2, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg3_2 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_2 );
            Py_INCREF( tmp_dircall_arg3_2 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
                tmp_assign_source_12 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
            }
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_collection == NULL );
            var_collection = tmp_assign_source_12;
        }
        goto branch_end_8;
        branch_no_8:;
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_called_instance_7;
            CHECK_OBJECT( var_tri );
            tmp_called_instance_7 = var_tri;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 105;
            tmp_assign_source_13 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_get_masked_triangles );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_maskedTris == NULL );
            var_maskedTris = tmp_assign_source_13;
        }
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_9;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_source_name_10;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_source_name_11;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_kw_name_2;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_np );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_np );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "np" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_9 = tmp_mvar_value_5;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_stack );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_tri );
            tmp_source_name_10 = var_tri;
            tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_x );
            if ( tmp_subscribed_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_maskedTris );
            tmp_subscript_name_2 = var_maskedTris;
            tmp_tuple_element_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscribed_name_2 );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_tuple_element_2 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_tuple_element_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( var_tri );
            tmp_source_name_11 = var_tri;
            tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_y );
            if ( tmp_subscribed_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_tuple_element_2 );

                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_maskedTris );
            tmp_subscript_name_3 = var_maskedTris;
            tmp_tuple_element_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_tuple_element_2 );

                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_tuple_element_2, 1, tmp_tuple_element_3 );
            tmp_args_name_2 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
            tmp_kw_name_2 = PyDict_Copy( const_dict_22b54c5c9112a5c8c51dc4a23fd6481b );
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 106;
            tmp_assign_source_14 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_verts == NULL );
            var_verts = tmp_assign_source_14;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_compexpr_left_18;
            PyObject *tmp_compexpr_right_18;
            CHECK_OBJECT( par_facecolors );
            tmp_compexpr_left_18 = par_facecolors;
            tmp_compexpr_right_18 = Py_None;
            tmp_condition_result_11 = ( tmp_compexpr_left_18 == tmp_compexpr_right_18 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_assign_source_15;
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_12;
                PyObject *tmp_subscribed_name_4;
                PyObject *tmp_subscript_name_4;
                PyObject *tmp_kw_name_3;
                CHECK_OBJECT( var_C );
                tmp_subscribed_name_4 = var_C;
                CHECK_OBJECT( var_maskedTris );
                tmp_subscript_name_4 = var_maskedTris;
                tmp_source_name_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
                if ( tmp_source_name_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_1 = "ooooooooooooNNooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_mean );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_1 = "ooooooooooooNNooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_kw_name_3 = PyDict_Copy( const_dict_c4b01644824ba6bad132707fcdaa03f1 );
                frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 111;
                tmp_assign_source_15 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_3 );
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_kw_name_3 );
                if ( tmp_assign_source_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_1 = "ooooooooooooNNooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_C;
                    assert( old != NULL );
                    var_C = tmp_assign_source_15;
                    Py_DECREF( old );
                }

            }
            goto branch_end_11;
            branch_no_11:;
            {
                nuitka_bool tmp_condition_result_12;
                PyObject *tmp_compexpr_left_19;
                PyObject *tmp_compexpr_right_19;
                PyObject *tmp_source_name_13;
                CHECK_OBJECT( var_tri );
                tmp_source_name_13 = var_tri;
                tmp_compexpr_left_19 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_mask );
                if ( tmp_compexpr_left_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_1 = "ooooooooooooNNooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_19 = Py_None;
                tmp_condition_result_12 = ( tmp_compexpr_left_19 != tmp_compexpr_right_19 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_compexpr_left_19 );
                if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                {
                    PyObject *tmp_assign_source_16;
                    PyObject *tmp_subscribed_name_5;
                    PyObject *tmp_subscript_name_5;
                    PyObject *tmp_operand_name_1;
                    PyObject *tmp_source_name_14;
                    CHECK_OBJECT( var_C );
                    tmp_subscribed_name_5 = var_C;
                    CHECK_OBJECT( var_tri );
                    tmp_source_name_14 = var_tri;
                    tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_mask );
                    if ( tmp_operand_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 114;
                        type_description_1 = "ooooooooooooNNooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_5 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
                    Py_DECREF( tmp_operand_name_1 );
                    if ( tmp_subscript_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 114;
                        type_description_1 = "ooooooooooooNNooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_16 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
                    Py_DECREF( tmp_subscript_name_5 );
                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 114;
                        type_description_1 = "ooooooooooooNNooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_C;
                        assert( old != NULL );
                        var_C = tmp_assign_source_16;
                        Py_DECREF( old );
                    }

                }
                branch_no_12:;
            }
            branch_end_11:;
        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_dircall_arg1_3;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_dircall_arg2_3;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_dircall_arg3_3;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_PolyCollection );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PolyCollection );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PolyCollection" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 116;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_dircall_arg1_3 = tmp_mvar_value_6;
            CHECK_OBJECT( var_verts );
            tmp_tuple_element_4 = var_verts;
            tmp_dircall_arg2_3 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_dircall_arg2_3, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg3_3 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_3 );
            Py_INCREF( tmp_dircall_arg3_3 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_3, tmp_dircall_arg2_3, tmp_dircall_arg3_3};
                tmp_assign_source_17 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
            }
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_collection == NULL );
            var_collection = tmp_assign_source_17;
        }
        branch_end_8:;
    }
    {
        PyObject *tmp_called_instance_8;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( var_collection );
        tmp_called_instance_8 = var_collection;
        CHECK_OBJECT( par_alpha );
        tmp_args_element_name_2 = par_alpha;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_set_alpha, call_args );
        }

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_15;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( var_collection );
        tmp_source_name_15 = var_collection;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_set_array );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        if ( var_C == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "C" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = var_C;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 119;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        nuitka_bool tmp_condition_result_13;
        int tmp_and_left_truth_7;
        nuitka_bool tmp_and_left_value_7;
        nuitka_bool tmp_and_right_value_7;
        PyObject *tmp_compexpr_left_20;
        PyObject *tmp_compexpr_right_20;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_7;
        CHECK_OBJECT( par_norm );
        tmp_compexpr_left_20 = par_norm;
        tmp_compexpr_right_20 = Py_None;
        tmp_and_left_value_7 = ( tmp_compexpr_left_20 != tmp_compexpr_right_20 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_7 = tmp_and_left_value_7 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_7 == 1 )
        {
            goto and_right_7;
        }
        else
        {
            goto and_left_7;
        }
        and_right_7:;
        CHECK_OBJECT( par_norm );
        tmp_isinstance_inst_1 = par_norm;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_Normalize );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Normalize );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Normalize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_7;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_13 = tmp_and_right_value_7;
        goto and_end_7;
        and_left_7:;
        tmp_condition_result_13 = tmp_and_left_value_7;
        and_end_7:;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        {
            PyObject *tmp_raise_type_4;
            PyObject *tmp_make_exception_arg_4;
            tmp_make_exception_arg_4 = const_str_digest_1544171cc85d8d0c8e1f63976c522d14;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 121;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_4 };
                tmp_raise_type_4 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
            }

            assert( !(tmp_raise_type_4 == NULL) );
            exception_type = tmp_raise_type_4;
            exception_lineno = 121;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_13:;
    }
    {
        PyObject *tmp_called_instance_9;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( var_collection );
        tmp_called_instance_9 = var_collection;
        CHECK_OBJECT( par_cmap );
        tmp_args_element_name_4 = par_cmap;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_set_cmap, call_args );
        }

        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_instance_10;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( var_collection );
        tmp_called_instance_10 = var_collection;
        CHECK_OBJECT( par_norm );
        tmp_args_element_name_5 = par_norm;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_set_norm, call_args );
        }

        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        nuitka_bool tmp_condition_result_14;
        int tmp_or_left_truth_2;
        nuitka_bool tmp_or_left_value_2;
        nuitka_bool tmp_or_right_value_2;
        PyObject *tmp_compexpr_left_21;
        PyObject *tmp_compexpr_right_21;
        PyObject *tmp_compexpr_left_22;
        PyObject *tmp_compexpr_right_22;
        CHECK_OBJECT( par_vmin );
        tmp_compexpr_left_21 = par_vmin;
        tmp_compexpr_right_21 = Py_None;
        tmp_or_left_value_2 = ( tmp_compexpr_left_21 != tmp_compexpr_right_21 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_2 = tmp_or_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_2 == 1 )
        {
            goto or_left_2;
        }
        else
        {
            goto or_right_2;
        }
        or_right_2:;
        CHECK_OBJECT( par_vmax );
        tmp_compexpr_left_22 = par_vmax;
        tmp_compexpr_right_22 = Py_None;
        tmp_or_right_value_2 = ( tmp_compexpr_left_22 != tmp_compexpr_right_22 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_14 = tmp_or_right_value_2;
        goto or_end_2;
        or_left_2:;
        tmp_condition_result_14 = tmp_or_left_value_2;
        or_end_2:;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_called_instance_11;
            PyObject *tmp_call_result_7;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( var_collection );
            tmp_called_instance_11 = var_collection;
            CHECK_OBJECT( par_vmin );
            tmp_args_element_name_6 = par_vmin;
            CHECK_OBJECT( par_vmax );
            tmp_args_element_name_7 = par_vmax;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 125;
            {
                PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                tmp_call_result_7 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_11, const_str_plain_set_clim, call_args );
            }

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        goto branch_end_14;
        branch_no_14:;
        {
            PyObject *tmp_called_instance_12;
            PyObject *tmp_call_result_8;
            CHECK_OBJECT( var_collection );
            tmp_called_instance_12 = var_collection;
            frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 127;
            tmp_call_result_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_autoscale_None );
            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 127;
                type_description_1 = "ooooooooooooNNooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_end_14:;
    }
    {
        PyObject *tmp_called_instance_13;
        PyObject *tmp_call_result_9;
        CHECK_OBJECT( par_ax );
        tmp_called_instance_13 = par_ax;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 128;
        tmp_call_result_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_13, const_str_plain_grid, &PyTuple_GET_ITEM( const_tuple_false_tuple, 0 ) );

        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_9 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_called_instance_14;
        PyObject *tmp_source_name_16;
        CHECK_OBJECT( var_tri );
        tmp_source_name_16 = var_tri;
        tmp_called_instance_14 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_x );
        if ( tmp_called_instance_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 130;
        tmp_assign_source_18 = CALL_METHOD_NO_ARGS( tmp_called_instance_14, const_str_plain_min );
        Py_DECREF( tmp_called_instance_14 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_minx == NULL );
        var_minx = tmp_assign_source_18;
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_instance_15;
        PyObject *tmp_source_name_17;
        CHECK_OBJECT( var_tri );
        tmp_source_name_17 = var_tri;
        tmp_called_instance_15 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_x );
        if ( tmp_called_instance_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 131;
        tmp_assign_source_19 = CALL_METHOD_NO_ARGS( tmp_called_instance_15, const_str_plain_max );
        Py_DECREF( tmp_called_instance_15 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_maxx == NULL );
        var_maxx = tmp_assign_source_19;
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_called_instance_16;
        PyObject *tmp_source_name_18;
        CHECK_OBJECT( var_tri );
        tmp_source_name_18 = var_tri;
        tmp_called_instance_16 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_y );
        if ( tmp_called_instance_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 132;
        tmp_assign_source_20 = CALL_METHOD_NO_ARGS( tmp_called_instance_16, const_str_plain_min );
        Py_DECREF( tmp_called_instance_16 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_miny == NULL );
        var_miny = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_instance_17;
        PyObject *tmp_source_name_19;
        CHECK_OBJECT( var_tri );
        tmp_source_name_19 = var_tri;
        tmp_called_instance_17 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_y );
        if ( tmp_called_instance_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 133;
        tmp_assign_source_21 = CALL_METHOD_NO_ARGS( tmp_called_instance_17, const_str_plain_max );
        Py_DECREF( tmp_called_instance_17 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_maxy == NULL );
        var_maxy = tmp_assign_source_21;
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_tuple_element_6;
        PyObject *tmp_tuple_element_7;
        CHECK_OBJECT( var_minx );
        tmp_tuple_element_6 = var_minx;
        tmp_tuple_element_5 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_tuple_element_5, 0, tmp_tuple_element_6 );
        CHECK_OBJECT( var_miny );
        tmp_tuple_element_6 = var_miny;
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_tuple_element_5, 1, tmp_tuple_element_6 );
        tmp_assign_source_22 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_assign_source_22, 0, tmp_tuple_element_5 );
        CHECK_OBJECT( var_maxx );
        tmp_tuple_element_7 = var_maxx;
        tmp_tuple_element_5 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_tuple_element_5, 0, tmp_tuple_element_7 );
        CHECK_OBJECT( var_maxy );
        tmp_tuple_element_7 = var_maxy;
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_tuple_element_5, 1, tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_assign_source_22, 1, tmp_tuple_element_5 );
        assert( var_corners == NULL );
        var_corners = tmp_assign_source_22;
    }
    {
        PyObject *tmp_called_instance_18;
        PyObject *tmp_call_result_10;
        PyObject *tmp_args_element_name_8;
        CHECK_OBJECT( par_ax );
        tmp_called_instance_18 = par_ax;
        CHECK_OBJECT( var_corners );
        tmp_args_element_name_8 = var_corners;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 135;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_call_result_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_18, const_str_plain_update_datalim, call_args );
        }

        if ( tmp_call_result_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_10 );
    }
    {
        PyObject *tmp_called_instance_19;
        PyObject *tmp_call_result_11;
        CHECK_OBJECT( par_ax );
        tmp_called_instance_19 = par_ax;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 136;
        tmp_call_result_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_19, const_str_plain_autoscale_view );
        if ( tmp_call_result_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_11 );
    }
    {
        PyObject *tmp_called_instance_20;
        PyObject *tmp_call_result_12;
        PyObject *tmp_args_element_name_9;
        CHECK_OBJECT( par_ax );
        tmp_called_instance_20 = par_ax;
        CHECK_OBJECT( var_collection );
        tmp_args_element_name_9 = var_collection;
        frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_call_result_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_20, const_str_plain_add_collection, call_args );
        }

        if ( tmp_call_result_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooooooooNNooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_12 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_61d84f715f2aeabcfcbc5a88835f20ba );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_61d84f715f2aeabcfcbc5a88835f20ba );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_61d84f715f2aeabcfcbc5a88835f20ba, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_61d84f715f2aeabcfcbc5a88835f20ba->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_61d84f715f2aeabcfcbc5a88835f20ba, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_61d84f715f2aeabcfcbc5a88835f20ba,
        type_description_1,
        par_ax,
        par_alpha,
        par_norm,
        par_cmap,
        par_vmin,
        par_vmax,
        par_shading,
        par_facecolors,
        par_args,
        par_kwargs,
        var_tri,
        var_C,
        NULL,
        NULL,
        var_ec,
        var_collection,
        var_maskedTris,
        var_verts,
        var_minx,
        var_maxx,
        var_miny,
        var_maxy,
        var_corners
    );


    // Release cached frame.
    if ( frame_61d84f715f2aeabcfcbc5a88835f20ba == cache_frame_61d84f715f2aeabcfcbc5a88835f20ba )
    {
        Py_DECREF( frame_61d84f715f2aeabcfcbc5a88835f20ba );
    }
    cache_frame_61d84f715f2aeabcfcbc5a88835f20ba = NULL;

    assertFrameObject( frame_61d84f715f2aeabcfcbc5a88835f20ba );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_collection );
    tmp_return_value = var_collection;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$tri$tripcolor$$$function_1_tripcolor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_ax );
    Py_DECREF( par_ax );
    par_ax = NULL;

    CHECK_OBJECT( (PyObject *)par_alpha );
    Py_DECREF( par_alpha );
    par_alpha = NULL;

    CHECK_OBJECT( (PyObject *)par_norm );
    Py_DECREF( par_norm );
    par_norm = NULL;

    CHECK_OBJECT( (PyObject *)par_cmap );
    Py_DECREF( par_cmap );
    par_cmap = NULL;

    CHECK_OBJECT( (PyObject *)par_vmin );
    Py_DECREF( par_vmin );
    par_vmin = NULL;

    CHECK_OBJECT( (PyObject *)par_vmax );
    Py_DECREF( par_vmax );
    par_vmax = NULL;

    CHECK_OBJECT( (PyObject *)par_shading );
    Py_DECREF( par_shading );
    par_shading = NULL;

    CHECK_OBJECT( (PyObject *)par_facecolors );
    Py_DECREF( par_facecolors );
    par_facecolors = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_tri );
    Py_DECREF( var_tri );
    var_tri = NULL;

    Py_XDECREF( var_C );
    var_C = NULL;

    CHECK_OBJECT( (PyObject *)var_ec );
    Py_DECREF( var_ec );
    var_ec = NULL;

    CHECK_OBJECT( (PyObject *)var_collection );
    Py_DECREF( var_collection );
    var_collection = NULL;

    Py_XDECREF( var_maskedTris );
    var_maskedTris = NULL;

    Py_XDECREF( var_verts );
    var_verts = NULL;

    CHECK_OBJECT( (PyObject *)var_minx );
    Py_DECREF( var_minx );
    var_minx = NULL;

    CHECK_OBJECT( (PyObject *)var_maxx );
    Py_DECREF( var_maxx );
    var_maxx = NULL;

    CHECK_OBJECT( (PyObject *)var_miny );
    Py_DECREF( var_miny );
    var_miny = NULL;

    CHECK_OBJECT( (PyObject *)var_maxy );
    Py_DECREF( var_maxy );
    var_maxy = NULL;

    CHECK_OBJECT( (PyObject *)var_corners );
    Py_DECREF( var_corners );
    var_corners = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_ax );
    Py_DECREF( par_ax );
    par_ax = NULL;

    CHECK_OBJECT( (PyObject *)par_alpha );
    Py_DECREF( par_alpha );
    par_alpha = NULL;

    CHECK_OBJECT( (PyObject *)par_norm );
    Py_DECREF( par_norm );
    par_norm = NULL;

    CHECK_OBJECT( (PyObject *)par_cmap );
    Py_DECREF( par_cmap );
    par_cmap = NULL;

    CHECK_OBJECT( (PyObject *)par_vmin );
    Py_DECREF( par_vmin );
    par_vmin = NULL;

    CHECK_OBJECT( (PyObject *)par_vmax );
    Py_DECREF( par_vmax );
    par_vmax = NULL;

    CHECK_OBJECT( (PyObject *)par_shading );
    Py_DECREF( par_shading );
    par_shading = NULL;

    Py_XDECREF( par_facecolors );
    par_facecolors = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_tri );
    var_tri = NULL;

    Py_XDECREF( var_C );
    var_C = NULL;

    Py_XDECREF( var_ec );
    var_ec = NULL;

    Py_XDECREF( var_collection );
    var_collection = NULL;

    Py_XDECREF( var_maskedTris );
    var_maskedTris = NULL;

    Py_XDECREF( var_verts );
    var_verts = NULL;

    Py_XDECREF( var_minx );
    var_minx = NULL;

    Py_XDECREF( var_maxx );
    var_maxx = NULL;

    Py_XDECREF( var_miny );
    var_miny = NULL;

    Py_XDECREF( var_maxy );
    var_maxy = NULL;

    Py_XDECREF( var_corners );
    var_corners = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$tri$tripcolor$$$function_1_tripcolor );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_matplotlib$tri$tripcolor$$$function_1_tripcolor( PyObject *kw_defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$tri$tripcolor$$$function_1_tripcolor,
        const_str_plain_tripcolor,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_61d84f715f2aeabcfcbc5a88835f20ba,
        NULL,
#if PYTHON_VERSION >= 300
        kw_defaults,
        NULL,
#endif
        module_matplotlib$tri$tripcolor,
        const_str_digest_8865fc010038957aa57fc97e8d9985c2,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$tri$tripcolor =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.tri.tripcolor",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$tri$tripcolor)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$tri$tripcolor)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$tri$tripcolor );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.tri.tripcolor: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.tri.tripcolor: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.tri.tripcolor: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$tri$tripcolor" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$tri$tripcolor = Py_InitModule4(
        "matplotlib.tri.tripcolor",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$tri$tripcolor = PyModule_Create( &mdef_matplotlib$tri$tripcolor );
#endif

    moduledict_matplotlib$tri$tripcolor = MODULE_DICT( module_matplotlib$tri$tripcolor );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$tri$tripcolor,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$tri$tripcolor,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$tri$tripcolor,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$tri$tripcolor,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$tri$tripcolor );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_c4bf50f886519643d93a69638824c9ae, module_matplotlib$tri$tripcolor );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_5ad0a695b9e1539445f8ac8cbe882509;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_5ad0a695b9e1539445f8ac8cbe882509 = MAKE_MODULE_FRAME( codeobj_5ad0a695b9e1539445f8ac8cbe882509, module_matplotlib$tri$tripcolor );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_5ad0a695b9e1539445f8ac8cbe882509 );
    assert( Py_REFCNT( frame_5ad0a695b9e1539445f8ac8cbe882509 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_numpy;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$tri$tripcolor;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_5ad0a695b9e1539445f8ac8cbe882509->m_frame.f_lineno = 1;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_np, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_matplotlib;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$tri$tripcolor;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_cbook_tuple;
        tmp_level_name_2 = const_int_0;
        frame_5ad0a695b9e1539445f8ac8cbe882509->m_frame.f_lineno = 3;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_cbook );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_cbook, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_38fb7bc0e5fceab2d00d507c3db86934;
        tmp_globals_name_3 = (PyObject *)moduledict_matplotlib$tri$tripcolor;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_PolyCollection_str_plain_TriMesh_tuple;
        tmp_level_name_3 = const_int_0;
        frame_5ad0a695b9e1539445f8ac8cbe882509->m_frame.f_lineno = 4;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_PolyCollection );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_PolyCollection, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_TriMesh );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_TriMesh, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_56aa55135057d4e1eddba0c96f175966;
        tmp_globals_name_4 = (PyObject *)moduledict_matplotlib$tri$tripcolor;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Normalize_tuple;
        tmp_level_name_4 = const_int_0;
        frame_5ad0a695b9e1539445f8ac8cbe882509->m_frame.f_lineno = 5;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Normalize );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_Normalize, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_0771b85ee859c0a4971fcb369a1ddb4d;
        tmp_globals_name_5 = (PyObject *)moduledict_matplotlib$tri$tripcolor;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_Triangulation_tuple;
        tmp_level_name_5 = const_int_0;
        frame_5ad0a695b9e1539445f8ac8cbe882509->m_frame.f_lineno = 6;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Triangulation );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_Triangulation, tmp_assign_source_10 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ad0a695b9e1539445f8ac8cbe882509 );
#endif
    popFrameStack();

    assertFrameObject( frame_5ad0a695b9e1539445f8ac8cbe882509 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ad0a695b9e1539445f8ac8cbe882509 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5ad0a695b9e1539445f8ac8cbe882509, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5ad0a695b9e1539445f8ac8cbe882509->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5ad0a695b9e1539445f8ac8cbe882509, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_kw_defaults_1;
        tmp_kw_defaults_1 = PyDict_Copy( const_dict_45f01d790dd247ccf5429943c7c59cc7 );
        tmp_assign_source_11 = MAKE_FUNCTION_matplotlib$tri$tripcolor$$$function_1_tripcolor( tmp_kw_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$tri$tripcolor, (Nuitka_StringObject *)const_str_plain_tripcolor, tmp_assign_source_11 );
    }

    return MOD_RETURN_VALUE( module_matplotlib$tri$tripcolor );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
