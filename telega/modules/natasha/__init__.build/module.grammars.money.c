/* Generated code for Python module 'grammars.money'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_grammars$money" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_grammars$money;
PyDictObject *moduledict_grammars$money;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_str_digest_59a11a6e625897d10e074ffcd41643bf_tuple;
static PyObject *const_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0;
static PyObject *const_tuple_str_digest_dcdf57511c5b0b189cd12de141314f13_tuple;
extern PyObject *const_tuple_str_plain_self_str_plain_amount_tuple;
static PyObject *const_tuple_str_chr_162_tuple;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_NUMR;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_str_chr_8381_tuple;
static PyObject *const_str_digest_b742afac9704728799839c36673c41ca;
extern PyObject *const_str_plain_DAY;
extern PyObject *const_str_plain_length_eq;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_INT;
extern PyObject *const_str_plain_MULTIPLIER;
extern PyObject *const_str_plain_PART;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain___file__;
static PyObject *const_list_str_plain_min_str_plain_max_list;
extern PyObject *const_str_plain_0;
static PyObject *const_str_chr_1082;
extern PyObject *const_str_plain_max;
static PyObject *const_str_digest_d2c6bd976f7c4a7ac144dc0944fc507c;
static PyObject *const_str_plain_RANGE_MIN;
static PyObject *const_tuple_str_plain_value_str_plain_integer_tuple;
static PyObject *const_list_str_plain_money_str_plain_period_list;
static PyObject *const_str_digest_41c141bf81764f9ac68af82f2b4ca503;
extern PyObject *const_str_plain_division;
extern PyObject *const_str_plain_RATE;
static PyObject *const_set_50604704cd9ba591707baff6a8d78c02;
static PyObject *const_str_plain_EURO;
extern PyObject *const_str_chr_36;
static PyObject *const_str_digest_b9aed00e01fcf634265224e656e794f2;
extern PyObject *const_str_plain_sub;
extern PyObject *const_tuple_str_plain_rule_str_plain_and__str_plain_or__tuple;
static PyObject *const_tuple_str_digest_41c141bf81764f9ac68af82f2b4ca503_tuple;
static PyObject *const_tuple_str_digest_e63c6a46700d15c5f24a1f3b0fd72300_tuple;
static PyObject *const_str_digest_965adec4a43b2a0478ac4e9936b60ebc;
static PyObject *const_str_digest_59a11a6e625897d10e074ffcd41643bf;
static PyObject *const_str_plain_PERIOD;
static PyObject *const_str_digest_1e49954e22a85f9441edab70f6b8f8cc;
extern PyObject *const_tuple_int_pos_1_tuple;
static PyObject *const_tuple_str_plain_fact_str_plain_const_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_dot;
static PyObject *const_str_chr_162;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_set_c6fa3d51bbb77f078b7a7a8efe6e81fa;
static PyObject *const_str_digest_56ce7d87aa319265c4313bfd404de5bb;
static PyObject *const_str_digest_dcdf57511c5b0b189cd12de141314f13;
static PyObject *const_str_plain_DOLLARS;
static PyObject *const_str_digest_9f6c7a124d818d2a6edb7fc7c29d68ef;
static PyObject *const_str_plain_COINS_AMOUNT;
static PyObject *const_str_digest_466f93b729b83f3a308e0e7afe45f3b3;
extern PyObject *const_tuple_int_pos_1000_tuple;
static PyObject *const_str_plain_money;
static PyObject *const_str_digest_e4faf4a78ea69479b096ba26e307d349;
static PyObject *const_tuple_str_digest_772d0cc28b36ccfb74082c80a60cc725_tuple;
extern PyObject *const_str_plain_ljust;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_chr_1088;
static PyObject *const_tuple_str_plain_Normalizable_str_plain_money_tuple;
extern PyObject *const_str_plain_Range;
static PyObject *const_str_plain_INTEGER;
extern PyObject *const_str_plain_min;
static PyObject *const_str_digest_8c677a17282a622d6297d1a88d7f30d4;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_chr_45;
static PyObject *const_tuple_int_pos_1000000_tuple;
extern PyObject *const_str_plain_value;
static PyObject *const_str_digest_ae79f570343c44dd8f204be5345c0cf2;
static PyObject *const_str_digest_58bd83dc3da9dfefee294376dcb4509f;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
static PyObject *const_str_chr_8381;
static PyObject *const_str_digest_e63c6a46700d15c5f24a1f3b0fd72300;
static PyObject *const_tuple_str_plain_value_str_plain_fraction_tuple;
static PyObject *const_str_digest_0b75389130edf28a9b215a4bec8e96ca;
static PyObject *const_str_plain_COINS_CURRENCY;
extern PyObject *const_str_plain_const;
extern PyObject *const_str_plain_or_;
extern PyObject *const_tuple_str_dot_tuple;
static PyObject *const_tuple_int_pos_1000000000_tuple;
static PyObject *const_tuple_str_digest_4c8eff7f5e29462fc7c29201d0400be1_tuple;
extern PyObject *const_tuple_str_chr_1090_tuple;
static PyObject *const_str_plain_multiplier;
extern PyObject *const_tuple_str_chr_45_tuple;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_str_plain_NUMR_tuple;
extern PyObject *const_tuple_str_digest_947f7a6ac444bb1ddd86a5723bc9579c_tuple;
extern PyObject *const_str_plain_interpretation;
static PyObject *const_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b;
static PyObject *const_str_plain_coins;
static PyObject *const_str_digest_4c8eff7f5e29462fc7c29201d0400be1;
static PyObject *const_tuple_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b_tuple;
static PyObject *const_str_plain_THOUSAND;
static PyObject *const_str_plain_currency;
static PyObject *const_tuple_str_digest_1e49954e22a85f9441edab70f6b8f8cc_tuple;
static PyObject *const_str_digest_9cb636bec25664fa7d233da27db30d4d;
extern PyObject *const_str_plain_in_;
static PyObject *const_tuple_int_pos_2_str_plain_0_tuple;
static PyObject *const_tuple_str_digest_d42564e7189ed8786c6f9df2730bd9a7_tuple;
static PyObject *const_str_plain_RANGE_MAX;
extern PyObject *const_str_digest_ea78dcde38a5066c777d6274d25657b3;
static PyObject *const_list_5bb9d78e7987bc0c2182e5db86b90e03_list;
static PyObject *const_str_digest_8d5784bbf3384a0fb3d7fb523bcc9ee7;
static PyObject *const_str_chr_1074;
extern PyObject *const_str_plain_integer;
static PyObject *const_tuple_str_digest_b9aed00e01fcf634265224e656e794f2_tuple;
static PyObject *const_str_plain_normalize_fraction;
extern PyObject *const_str_plain_SEP;
extern PyObject *const_str_plain_eq;
static PyObject *const_str_digest_772d0cc28b36ccfb74082c80a60cc725;
static PyObject *const_str_digest_7e65076f194e6bb0570c15048cbf7536;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_tuple_int_pos_3_tuple;
extern PyObject *const_str_plain_DOT;
extern PyObject *const_str_plain_yargy;
static PyObject *const_str_digest_81ef627c76b7b5654449d61a984299c4;
static PyObject *const_str_plain_RATE_MONEY;
extern PyObject *const_tuple_str_chr_36_tuple;
static PyObject *const_str_plain_Rate;
extern PyObject *const_str_plain_MONEY;
static PyObject *const_str_plain_KOPEIKA;
static PyObject *const_tuple_str_digest_0b75389130edf28a9b215a4bec8e96ca_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_repeatable;
static PyObject *const_str_plain_NUMERAL;
static PyObject *const_tuple_str_plain_self_str_plain_min_str_plain_max_tuple;
extern PyObject *const_int_pos_1000;
static PyObject *const_str_plain_PER;
static PyObject *const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple;
extern PyObject *const_tuple_int_pos_2_tuple;
static PyObject *const_int_pos_1000000000;
static PyObject *const_str_plain_PERIODS;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_MILLIARD;
extern PyObject *const_str_plain_BOUND;
static PyObject *const_str_plain_AMOUNT;
extern PyObject *const_str_plain_Record;
static PyObject *const_str_plain_FRACTION;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_tuple_str_chr_1088_tuple;
static PyObject *const_str_digest_d42564e7189ed8786c6f9df2730bd9a7;
extern PyObject *const_str_plain_DASH;
extern PyObject *const_tuple_type_int_tuple;
extern PyObject *const_tuple_str_plain_INT_tuple;
static PyObject *const_tuple_str_digest_56ce7d87aa319265c4313bfd404de5bb_tuple;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_in_caseless;
extern PyObject *const_str_plain_fact;
extern PyObject *const_str_plain_SHIFT;
extern PyObject *const_str_plain_RANGE;
static PyObject *const_str_digest_7169fd129074536e81f99b30f4e4ce37;
extern PyObject *const_str_plain_normalized;
static PyObject *const_str_digest_584b890b28c48ea4d19053880727278f;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_chr_1090;
static PyObject *const_str_plain_normalize_integer;
extern PyObject *const_str_plain_caseless;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_chr_8364;
static PyObject *const_str_plain_Money;
extern PyObject *const_tuple_str_plain_Record_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_rule;
extern PyObject *const_str_chr_47;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_Normalizable;
extern PyObject *const_str_plain_optional;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_RANGE_MONEY;
static PyObject *const_str_plain_COINS_INTEGER;
extern PyObject *const_tuple_str_chr_47_tuple;
extern PyObject *const_int_pos_1000000;
static PyObject *const_str_plain_EUROCENT;
static PyObject *const_str_plain_MILLION;
static PyObject *const_str_digest_3f64ef963bd11fa8f2123823d705c4fe;
extern PyObject *const_str_plain_dsl;
static PyObject *const_str_digest_8e927fbf4dd4fc49ce36755f44533963;
static PyObject *const_str_digest_f412c137078cf09f69b52ce7a279c654;
static PyObject *const_str_digest_0005f1018b913d59f0698ba7ad8e7254;
static PyObject *const_set_ae8070333b58fef640018259e0c8aae3;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_digest_9ab9015b4a2054b77da734c3e8a94159;
extern PyObject *const_int_pos_3;
static PyObject *const_tuple_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0_tuple;
static PyObject *const_tuple_str_digest_0005f1018b913d59f0698ba7ad8e7254_tuple;
extern PyObject *const_str_plain_fraction;
static PyObject *const_str_digest_254e47bddf4399538dfb2488241325ab;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_1e3f6bdc2a647812d6f448036fdfc355;
static PyObject *const_str_plain_MODIFIER;
extern PyObject *const_str_digest_754bbdb51d29ec017fab6feb3184da49;
static PyObject *const_tuple_str_digest_7169fd129074536e81f99b30f4e4ce37_tuple;
extern PyObject *const_str_plain_dictionary;
extern PyObject *const_str_plain_amount;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
static PyObject *const_str_plain_CURRENCY;
static PyObject *const_tuple_str_chr_8364_tuple;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_period;
extern PyObject *const_str_plain_and_;
extern PyObject *const_str_empty;
static PyObject *const_tuple_str_digest_81ef627c76b7b5654449d61a984299c4_tuple;
static PyObject *const_tuple_str_digest_254e47bddf4399538dfb2488241325ab_tuple;
static PyObject *const_tuple_str_chr_1082_tuple;
extern PyObject *const_str_plain_custom;
extern PyObject *const_str_digest_947f7a6ac444bb1ddd86a5723bc9579c;
extern PyObject *const_int_pos_100;
static PyObject *const_str_plain_HOUR;
static PyObject *const_str_plain_RUBLES;
extern PyObject *const_str_plain_gram;
static PyObject *const_str_plain_CENT;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_digest_59a11a6e625897d10e074ffcd41643bf_tuple = PyTuple_New( 1 );
    const_str_digest_59a11a6e625897d10e074ffcd41643bf = UNSTREAM_STRING_ASCII( &constant_bin[ 678689 ], 4, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_59a11a6e625897d10e074ffcd41643bf_tuple, 0, const_str_digest_59a11a6e625897d10e074ffcd41643bf ); Py_INCREF( const_str_digest_59a11a6e625897d10e074ffcd41643bf );
    const_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0 = UNSTREAM_STRING( &constant_bin[ 678693 ], 8, 0 );
    const_tuple_str_digest_dcdf57511c5b0b189cd12de141314f13_tuple = PyTuple_New( 1 );
    const_str_digest_dcdf57511c5b0b189cd12de141314f13 = UNSTREAM_STRING( &constant_bin[ 678701 ], 10, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_dcdf57511c5b0b189cd12de141314f13_tuple, 0, const_str_digest_dcdf57511c5b0b189cd12de141314f13 ); Py_INCREF( const_str_digest_dcdf57511c5b0b189cd12de141314f13 );
    const_tuple_str_chr_162_tuple = PyTuple_New( 1 );
    const_str_chr_162 = UNSTREAM_STRING( &constant_bin[ 678711 ], 2, 0 );
    PyTuple_SET_ITEM( const_tuple_str_chr_162_tuple, 0, const_str_chr_162 ); Py_INCREF( const_str_chr_162 );
    const_tuple_str_chr_8381_tuple = PyTuple_New( 1 );
    const_str_chr_8381 = UNSTREAM_STRING( &constant_bin[ 678713 ], 3, 0 );
    PyTuple_SET_ITEM( const_tuple_str_chr_8381_tuple, 0, const_str_chr_8381 ); Py_INCREF( const_str_chr_8381 );
    const_str_digest_b742afac9704728799839c36673c41ca = UNSTREAM_STRING( &constant_bin[ 678716 ], 10, 0 );
    const_list_str_plain_min_str_plain_max_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_min_str_plain_max_list, 0, const_str_plain_min ); Py_INCREF( const_str_plain_min );
    PyList_SET_ITEM( const_list_str_plain_min_str_plain_max_list, 1, const_str_plain_max ); Py_INCREF( const_str_plain_max );
    const_str_chr_1082 = UNSTREAM_STRING( &constant_bin[ 667377 ], 2, 0 );
    const_str_digest_d2c6bd976f7c4a7ac144dc0944fc507c = UNSTREAM_STRING_ASCII( &constant_bin[ 678726 ], 7, 0 );
    const_str_plain_RANGE_MIN = UNSTREAM_STRING_ASCII( &constant_bin[ 678733 ], 9, 1 );
    const_tuple_str_plain_value_str_plain_integer_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_integer_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_integer_tuple, 1, const_str_plain_integer ); Py_INCREF( const_str_plain_integer );
    const_list_str_plain_money_str_plain_period_list = PyList_New( 2 );
    const_str_plain_money = UNSTREAM_STRING_ASCII( &constant_bin[ 678742 ], 5, 1 );
    PyList_SET_ITEM( const_list_str_plain_money_str_plain_period_list, 0, const_str_plain_money ); Py_INCREF( const_str_plain_money );
    PyList_SET_ITEM( const_list_str_plain_money_str_plain_period_list, 1, const_str_plain_period ); Py_INCREF( const_str_plain_period );
    const_str_digest_41c141bf81764f9ac68af82f2b4ca503 = UNSTREAM_STRING( &constant_bin[ 678747 ], 12, 0 );
    const_set_50604704cd9ba591707baff6a8d78c02 = PySet_New( NULL );
    const_str_digest_466f93b729b83f3a308e0e7afe45f3b3 = UNSTREAM_STRING( &constant_bin[ 667632 ], 4, 0 );
    PySet_Add( const_set_50604704cd9ba591707baff6a8d78c02, const_str_digest_466f93b729b83f3a308e0e7afe45f3b3 );
    const_str_chr_1074 = UNSTREAM_STRING( &constant_bin[ 667409 ], 2, 0 );
    PySet_Add( const_set_50604704cd9ba591707baff6a8d78c02, const_str_chr_1074 );
    assert( PySet_Size( const_set_50604704cd9ba591707baff6a8d78c02 ) == 2 );
    const_str_plain_EURO = UNSTREAM_STRING_ASCII( &constant_bin[ 678759 ], 4, 1 );
    const_str_digest_b9aed00e01fcf634265224e656e794f2 = UNSTREAM_STRING( &constant_bin[ 678763 ], 12, 0 );
    const_tuple_str_digest_41c141bf81764f9ac68af82f2b4ca503_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_41c141bf81764f9ac68af82f2b4ca503_tuple, 0, const_str_digest_41c141bf81764f9ac68af82f2b4ca503 ); Py_INCREF( const_str_digest_41c141bf81764f9ac68af82f2b4ca503 );
    const_tuple_str_digest_e63c6a46700d15c5f24a1f3b0fd72300_tuple = PyTuple_New( 1 );
    const_str_digest_e63c6a46700d15c5f24a1f3b0fd72300 = UNSTREAM_STRING_ASCII( &constant_bin[ 678775 ], 2, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e63c6a46700d15c5f24a1f3b0fd72300_tuple, 0, const_str_digest_e63c6a46700d15c5f24a1f3b0fd72300 ); Py_INCREF( const_str_digest_e63c6a46700d15c5f24a1f3b0fd72300 );
    const_str_digest_965adec4a43b2a0478ac4e9936b60ebc = UNSTREAM_STRING_ASCII( &constant_bin[ 678777 ], 15, 0 );
    const_str_plain_PERIOD = UNSTREAM_STRING_ASCII( &constant_bin[ 678792 ], 6, 1 );
    const_str_digest_1e49954e22a85f9441edab70f6b8f8cc = UNSTREAM_STRING( &constant_bin[ 678798 ], 8, 0 );
    const_tuple_str_plain_fact_str_plain_const_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fact_str_plain_const_tuple, 0, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    PyTuple_SET_ITEM( const_tuple_str_plain_fact_str_plain_const_tuple, 1, const_str_plain_const ); Py_INCREF( const_str_plain_const );
    const_set_c6fa3d51bbb77f078b7a7a8efe6e81fa = PySet_New( NULL );
    const_str_digest_ae79f570343c44dd8f204be5345c0cf2 = UNSTREAM_STRING( &constant_bin[ 673979 ], 8, 0 );
    PySet_Add( const_set_c6fa3d51bbb77f078b7a7a8efe6e81fa, const_str_digest_ae79f570343c44dd8f204be5345c0cf2 );
    const_str_digest_e4faf4a78ea69479b096ba26e307d349 = UNSTREAM_STRING( &constant_bin[ 678806 ], 8, 0 );
    PySet_Add( const_set_c6fa3d51bbb77f078b7a7a8efe6e81fa, const_str_digest_e4faf4a78ea69479b096ba26e307d349 );
    assert( PySet_Size( const_set_c6fa3d51bbb77f078b7a7a8efe6e81fa ) == 2 );
    const_str_digest_56ce7d87aa319265c4313bfd404de5bb = UNSTREAM_STRING( &constant_bin[ 673150 ], 6, 0 );
    const_str_plain_DOLLARS = UNSTREAM_STRING_ASCII( &constant_bin[ 678814 ], 7, 1 );
    const_str_digest_9f6c7a124d818d2a6edb7fc7c29d68ef = UNSTREAM_STRING( &constant_bin[ 678821 ], 8, 0 );
    const_str_plain_COINS_AMOUNT = UNSTREAM_STRING_ASCII( &constant_bin[ 678829 ], 12, 1 );
    const_tuple_str_digest_772d0cc28b36ccfb74082c80a60cc725_tuple = PyTuple_New( 1 );
    const_str_digest_772d0cc28b36ccfb74082c80a60cc725 = UNSTREAM_STRING( &constant_bin[ 667570 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_772d0cc28b36ccfb74082c80a60cc725_tuple, 0, const_str_digest_772d0cc28b36ccfb74082c80a60cc725 ); Py_INCREF( const_str_digest_772d0cc28b36ccfb74082c80a60cc725 );
    const_tuple_str_plain_Normalizable_str_plain_money_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Normalizable_str_plain_money_tuple, 0, const_str_plain_Normalizable ); Py_INCREF( const_str_plain_Normalizable );
    PyTuple_SET_ITEM( const_tuple_str_plain_Normalizable_str_plain_money_tuple, 1, const_str_plain_money ); Py_INCREF( const_str_plain_money );
    const_str_plain_INTEGER = UNSTREAM_STRING_ASCII( &constant_bin[ 678841 ], 7, 1 );
    const_str_digest_8c677a17282a622d6297d1a88d7f30d4 = UNSTREAM_STRING_ASCII( &constant_bin[ 678848 ], 11, 0 );
    const_tuple_int_pos_1000000_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1000000_tuple, 0, const_int_pos_1000000 ); Py_INCREF( const_int_pos_1000000 );
    const_str_digest_58bd83dc3da9dfefee294376dcb4509f = UNSTREAM_STRING( &constant_bin[ 678859 ], 10, 0 );
    const_tuple_str_plain_value_str_plain_fraction_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_fraction_tuple, 0, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_value_str_plain_fraction_tuple, 1, const_str_plain_fraction ); Py_INCREF( const_str_plain_fraction );
    const_str_digest_0b75389130edf28a9b215a4bec8e96ca = UNSTREAM_STRING( &constant_bin[ 678869 ], 16, 0 );
    const_str_plain_COINS_CURRENCY = UNSTREAM_STRING_ASCII( &constant_bin[ 678885 ], 14, 1 );
    const_tuple_int_pos_1000000000_tuple = PyTuple_New( 1 );
    const_int_pos_1000000000 = PyLong_FromUnsignedLong( 1000000000ul );
    PyTuple_SET_ITEM( const_tuple_int_pos_1000000000_tuple, 0, const_int_pos_1000000000 ); Py_INCREF( const_int_pos_1000000000 );
    const_tuple_str_digest_4c8eff7f5e29462fc7c29201d0400be1_tuple = PyTuple_New( 1 );
    const_str_digest_4c8eff7f5e29462fc7c29201d0400be1 = UNSTREAM_STRING( &constant_bin[ 678899 ], 6, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4c8eff7f5e29462fc7c29201d0400be1_tuple, 0, const_str_digest_4c8eff7f5e29462fc7c29201d0400be1 ); Py_INCREF( const_str_digest_4c8eff7f5e29462fc7c29201d0400be1 );
    const_str_plain_multiplier = UNSTREAM_STRING_ASCII( &constant_bin[ 678905 ], 10, 1 );
    const_tuple_str_plain_NUMR_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_NUMR_tuple, 0, const_str_plain_NUMR ); Py_INCREF( const_str_plain_NUMR );
    const_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b = UNSTREAM_STRING( &constant_bin[ 678915 ], 16, 0 );
    const_str_plain_coins = UNSTREAM_STRING_ASCII( &constant_bin[ 678931 ], 5, 1 );
    const_tuple_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b_tuple, 0, const_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b ); Py_INCREF( const_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b );
    const_str_plain_THOUSAND = UNSTREAM_STRING_ASCII( &constant_bin[ 678936 ], 8, 1 );
    const_str_plain_currency = UNSTREAM_STRING_ASCII( &constant_bin[ 678944 ], 8, 1 );
    const_tuple_str_digest_1e49954e22a85f9441edab70f6b8f8cc_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_1e49954e22a85f9441edab70f6b8f8cc_tuple, 0, const_str_digest_1e49954e22a85f9441edab70f6b8f8cc ); Py_INCREF( const_str_digest_1e49954e22a85f9441edab70f6b8f8cc );
    const_str_digest_9cb636bec25664fa7d233da27db30d4d = UNSTREAM_STRING( &constant_bin[ 678952 ], 10, 0 );
    const_tuple_int_pos_2_str_plain_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_2_str_plain_0_tuple, 0, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_2_str_plain_0_tuple, 1, const_str_plain_0 ); Py_INCREF( const_str_plain_0 );
    const_tuple_str_digest_d42564e7189ed8786c6f9df2730bd9a7_tuple = PyTuple_New( 1 );
    const_str_digest_d42564e7189ed8786c6f9df2730bd9a7 = UNSTREAM_STRING( &constant_bin[ 678962 ], 14, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_d42564e7189ed8786c6f9df2730bd9a7_tuple, 0, const_str_digest_d42564e7189ed8786c6f9df2730bd9a7 ); Py_INCREF( const_str_digest_d42564e7189ed8786c6f9df2730bd9a7 );
    const_str_plain_RANGE_MAX = UNSTREAM_STRING_ASCII( &constant_bin[ 678976 ], 9, 1 );
    const_list_5bb9d78e7987bc0c2182e5db86b90e03_list = PyList_New( 5 );
    PyList_SET_ITEM( const_list_5bb9d78e7987bc0c2182e5db86b90e03_list, 0, const_str_plain_integer ); Py_INCREF( const_str_plain_integer );
    PyList_SET_ITEM( const_list_5bb9d78e7987bc0c2182e5db86b90e03_list, 1, const_str_plain_fraction ); Py_INCREF( const_str_plain_fraction );
    PyList_SET_ITEM( const_list_5bb9d78e7987bc0c2182e5db86b90e03_list, 2, const_str_plain_multiplier ); Py_INCREF( const_str_plain_multiplier );
    PyList_SET_ITEM( const_list_5bb9d78e7987bc0c2182e5db86b90e03_list, 3, const_str_plain_currency ); Py_INCREF( const_str_plain_currency );
    PyList_SET_ITEM( const_list_5bb9d78e7987bc0c2182e5db86b90e03_list, 4, const_str_plain_coins ); Py_INCREF( const_str_plain_coins );
    const_str_digest_8d5784bbf3384a0fb3d7fb523bcc9ee7 = UNSTREAM_STRING( &constant_bin[ 678985 ], 6, 0 );
    const_tuple_str_digest_b9aed00e01fcf634265224e656e794f2_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b9aed00e01fcf634265224e656e794f2_tuple, 0, const_str_digest_b9aed00e01fcf634265224e656e794f2 ); Py_INCREF( const_str_digest_b9aed00e01fcf634265224e656e794f2 );
    const_str_plain_normalize_fraction = UNSTREAM_STRING_ASCII( &constant_bin[ 678991 ], 18, 1 );
    const_str_digest_7e65076f194e6bb0570c15048cbf7536 = UNSTREAM_STRING_ASCII( &constant_bin[ 679009 ], 16, 0 );
    const_str_digest_81ef627c76b7b5654449d61a984299c4 = UNSTREAM_STRING( &constant_bin[ 679025 ], 14, 0 );
    const_str_plain_RATE_MONEY = UNSTREAM_STRING_ASCII( &constant_bin[ 679039 ], 10, 1 );
    const_str_plain_Rate = UNSTREAM_STRING_ASCII( &constant_bin[ 666845 ], 4, 1 );
    const_str_plain_KOPEIKA = UNSTREAM_STRING_ASCII( &constant_bin[ 679049 ], 7, 1 );
    const_tuple_str_digest_0b75389130edf28a9b215a4bec8e96ca_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_0b75389130edf28a9b215a4bec8e96ca_tuple, 0, const_str_digest_0b75389130edf28a9b215a4bec8e96ca ); Py_INCREF( const_str_digest_0b75389130edf28a9b215a4bec8e96ca );
    const_str_plain_NUMERAL = UNSTREAM_STRING_ASCII( &constant_bin[ 679056 ], 7, 1 );
    const_tuple_str_plain_self_str_plain_min_str_plain_max_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_min_str_plain_max_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_min_str_plain_max_tuple, 1, const_str_plain_min ); Py_INCREF( const_str_plain_min );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_min_str_plain_max_tuple, 2, const_str_plain_max ); Py_INCREF( const_str_plain_max );
    const_str_plain_PER = UNSTREAM_STRING_ASCII( &constant_bin[ 34714 ], 3, 1 );
    const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 0, const_str_plain_eq ); Py_INCREF( const_str_plain_eq );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 1, const_str_plain_length_eq ); Py_INCREF( const_str_plain_length_eq );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 2, const_str_plain_in_ ); Py_INCREF( const_str_plain_in_ );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 3, const_str_plain_in_caseless ); Py_INCREF( const_str_plain_in_caseless );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 4, const_str_plain_gram ); Py_INCREF( const_str_plain_gram );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 5, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 6, const_str_plain_normalized ); Py_INCREF( const_str_plain_normalized );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 7, const_str_plain_caseless ); Py_INCREF( const_str_plain_caseless );
    PyTuple_SET_ITEM( const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple, 8, const_str_plain_dictionary ); Py_INCREF( const_str_plain_dictionary );
    const_str_plain_PERIODS = UNSTREAM_STRING_ASCII( &constant_bin[ 679063 ], 7, 1 );
    const_str_plain_MILLIARD = UNSTREAM_STRING_ASCII( &constant_bin[ 679070 ], 8, 1 );
    const_str_plain_AMOUNT = UNSTREAM_STRING_ASCII( &constant_bin[ 678835 ], 6, 1 );
    const_str_plain_FRACTION = UNSTREAM_STRING_ASCII( &constant_bin[ 591657 ], 8, 1 );
    const_tuple_str_digest_56ce7d87aa319265c4313bfd404de5bb_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_56ce7d87aa319265c4313bfd404de5bb_tuple, 0, const_str_digest_56ce7d87aa319265c4313bfd404de5bb ); Py_INCREF( const_str_digest_56ce7d87aa319265c4313bfd404de5bb );
    const_str_digest_7169fd129074536e81f99b30f4e4ce37 = UNSTREAM_STRING( &constant_bin[ 678747 ], 6, 0 );
    const_str_digest_584b890b28c48ea4d19053880727278f = UNSTREAM_STRING_ASCII( &constant_bin[ 679078 ], 23, 0 );
    const_str_plain_normalize_integer = UNSTREAM_STRING_ASCII( &constant_bin[ 679101 ], 17, 1 );
    const_str_chr_8364 = UNSTREAM_STRING( &constant_bin[ 679118 ], 3, 0 );
    const_str_plain_Money = UNSTREAM_STRING_ASCII( &constant_bin[ 666840 ], 5, 1 );
    const_str_plain_RANGE_MONEY = UNSTREAM_STRING_ASCII( &constant_bin[ 679121 ], 11, 1 );
    const_str_plain_COINS_INTEGER = UNSTREAM_STRING_ASCII( &constant_bin[ 679132 ], 13, 1 );
    const_str_plain_EUROCENT = UNSTREAM_STRING_ASCII( &constant_bin[ 679145 ], 8, 1 );
    const_str_plain_MILLION = UNSTREAM_STRING_ASCII( &constant_bin[ 679153 ], 7, 1 );
    const_str_digest_3f64ef963bd11fa8f2123823d705c4fe = UNSTREAM_STRING( &constant_bin[ 679160 ], 10, 0 );
    const_str_digest_8e927fbf4dd4fc49ce36755f44533963 = UNSTREAM_STRING_ASCII( &constant_bin[ 679170 ], 17, 0 );
    const_str_digest_f412c137078cf09f69b52ce7a279c654 = UNSTREAM_STRING( &constant_bin[ 679187 ], 14, 0 );
    const_str_digest_0005f1018b913d59f0698ba7ad8e7254 = UNSTREAM_STRING( &constant_bin[ 667696 ], 4, 0 );
    const_set_ae8070333b58fef640018259e0c8aae3 = PySet_New( NULL );
    PySet_Add( const_set_ae8070333b58fef640018259e0c8aae3, const_str_digest_58bd83dc3da9dfefee294376dcb4509f );
    PySet_Add( const_set_ae8070333b58fef640018259e0c8aae3, const_str_digest_9cb636bec25664fa7d233da27db30d4d );
    PySet_Add( const_set_ae8070333b58fef640018259e0c8aae3, const_str_digest_f412c137078cf09f69b52ce7a279c654 );
    assert( PySet_Size( const_set_ae8070333b58fef640018259e0c8aae3 ) == 3 );
    const_str_digest_9ab9015b4a2054b77da734c3e8a94159 = UNSTREAM_STRING_ASCII( &constant_bin[ 679201 ], 13, 0 );
    const_tuple_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0_tuple, 0, const_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0 ); Py_INCREF( const_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0 );
    const_tuple_str_digest_0005f1018b913d59f0698ba7ad8e7254_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_0005f1018b913d59f0698ba7ad8e7254_tuple, 0, const_str_digest_0005f1018b913d59f0698ba7ad8e7254 ); Py_INCREF( const_str_digest_0005f1018b913d59f0698ba7ad8e7254 );
    const_str_digest_254e47bddf4399538dfb2488241325ab = UNSTREAM_STRING( &constant_bin[ 672719 ], 6, 0 );
    const_str_digest_1e3f6bdc2a647812d6f448036fdfc355 = UNSTREAM_STRING_ASCII( &constant_bin[ 679214 ], 16, 0 );
    const_str_plain_MODIFIER = UNSTREAM_STRING_ASCII( &constant_bin[ 668553 ], 8, 1 );
    const_tuple_str_digest_7169fd129074536e81f99b30f4e4ce37_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_7169fd129074536e81f99b30f4e4ce37_tuple, 0, const_str_digest_7169fd129074536e81f99b30f4e4ce37 ); Py_INCREF( const_str_digest_7169fd129074536e81f99b30f4e4ce37 );
    const_str_plain_CURRENCY = UNSTREAM_STRING_ASCII( &constant_bin[ 678891 ], 8, 1 );
    const_tuple_str_chr_8364_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_chr_8364_tuple, 0, const_str_chr_8364 ); Py_INCREF( const_str_chr_8364 );
    const_tuple_str_digest_81ef627c76b7b5654449d61a984299c4_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_81ef627c76b7b5654449d61a984299c4_tuple, 0, const_str_digest_81ef627c76b7b5654449d61a984299c4 ); Py_INCREF( const_str_digest_81ef627c76b7b5654449d61a984299c4 );
    const_tuple_str_digest_254e47bddf4399538dfb2488241325ab_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_254e47bddf4399538dfb2488241325ab_tuple, 0, const_str_digest_254e47bddf4399538dfb2488241325ab ); Py_INCREF( const_str_digest_254e47bddf4399538dfb2488241325ab );
    const_tuple_str_chr_1082_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_chr_1082_tuple, 0, const_str_chr_1082 ); Py_INCREF( const_str_chr_1082 );
    const_str_plain_HOUR = UNSTREAM_STRING_ASCII( &constant_bin[ 602938 ], 4, 1 );
    const_str_plain_RUBLES = UNSTREAM_STRING_ASCII( &constant_bin[ 679230 ], 6, 1 );
    const_str_plain_CENT = UNSTREAM_STRING_ASCII( &constant_bin[ 679149 ], 4, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_grammars$money( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_61aadf8837dc934bae8e309d56e02ee2;
static PyCodeObject *codeobj_9b311b5df972be73e01887347bf06532;
static PyCodeObject *codeobj_3f86f284caacec5df8359fcd1b6e88d8;
static PyCodeObject *codeobj_b1f3733f6842e66afa864afad3f5419b;
static PyCodeObject *codeobj_148d079781484ab729137f0240324599;
static PyCodeObject *codeobj_72f42d2ae68f45ed855436d380208b3f;
static PyCodeObject *codeobj_ab3ce681a8f810bd8d36d512ce7c4917;
static PyCodeObject *codeobj_6aab4e12ef4c9f8a8cad31da66043cb6;
static PyCodeObject *codeobj_4af84bfe423338aa920b7f3085424170;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_8e927fbf4dd4fc49ce36755f44533963 );
    codeobj_61aadf8837dc934bae8e309d56e02ee2 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_584b890b28c48ea4d19053880727278f, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9b311b5df972be73e01887347bf06532 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Money, 35, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3f86f284caacec5df8359fcd1b6e88d8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Range, 69, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_b1f3733f6842e66afa864afad3f5419b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Rate, 54, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_148d079781484ab729137f0240324599 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalize_fraction, 247, const_tuple_str_plain_value_str_plain_fraction_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_72f42d2ae68f45ed855436d380208b3f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalize_integer, 242, const_tuple_str_plain_value_str_plain_integer_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ab3ce681a8f810bd8d36d512ce7c4917 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalized, 55, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6aab4e12ef4c9f8a8cad31da66043cb6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalized, 36, const_tuple_str_plain_self_str_plain_amount_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4af84bfe423338aa920b7f3085424170 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalized, 70, const_tuple_str_plain_self_str_plain_min_str_plain_max_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_grammars$money$$$function_1_normalized(  );


static PyObject *MAKE_FUNCTION_grammars$money$$$function_2_normalized(  );


static PyObject *MAKE_FUNCTION_grammars$money$$$function_3_normalized(  );


static PyObject *MAKE_FUNCTION_grammars$money$$$function_4_normalize_integer(  );


static PyObject *MAKE_FUNCTION_grammars$money$$$function_5_normalize_fraction(  );


// The module function definitions.
static PyObject *impl_grammars$money$$$function_1_normalized( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_amount = NULL;
    struct Nuitka_FrameObject *frame_6aab4e12ef4c9f8a8cad31da66043cb6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_6aab4e12ef4c9f8a8cad31da66043cb6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6aab4e12ef4c9f8a8cad31da66043cb6, codeobj_6aab4e12ef4c9f8a8cad31da66043cb6, module_grammars$money, sizeof(void *)+sizeof(void *) );
    frame_6aab4e12ef4c9f8a8cad31da66043cb6 = cache_frame_6aab4e12ef4c9f8a8cad31da66043cb6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6aab4e12ef4c9f8a8cad31da66043cb6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6aab4e12ef4c9f8a8cad31da66043cb6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_integer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_amount == NULL );
        var_amount = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fraction );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 39;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_amount );
            tmp_left_name_1 = var_amount;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_left_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_fraction );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = const_int_pos_100;
            tmp_right_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = tmp_left_name_1;
            var_amount = tmp_assign_source_2;

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_attribute_value_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_attribute_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_multiplier );
        if ( tmp_attribute_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_2 );

            exception_lineno = 41;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_amount );
            tmp_left_name_3 = var_amount;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_multiplier );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_MUL_INPLACE( &tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = tmp_left_name_3;
            var_amount = tmp_assign_source_3;

        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_attribute_value_3;
        int tmp_truth_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_attribute_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_coins );
        if ( tmp_attribute_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_attribute_value_3 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_3 );

            exception_lineno = 43;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_3 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            PyObject *tmp_left_name_5;
            PyObject *tmp_source_name_7;
            PyObject *tmp_right_name_5;
            if ( var_amount == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "amount" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 44;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_left_name_4 = var_amount;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_left_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_coins );
            if ( tmp_left_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_5 = const_int_pos_100;
            tmp_right_name_4 = BINARY_OPERATION_TRUEDIV_OBJECT_LONG( tmp_left_name_5, tmp_right_name_5 );
            Py_DECREF( tmp_left_name_5 );
            if ( tmp_right_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_4 = tmp_left_name_4;
            var_amount = tmp_assign_source_4;

        }
        branch_no_3:;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_8;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_9;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_8 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_Money );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        if ( var_amount == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "amount" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = var_amount;
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_currency );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 45;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_6aab4e12ef4c9f8a8cad31da66043cb6->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6aab4e12ef4c9f8a8cad31da66043cb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6aab4e12ef4c9f8a8cad31da66043cb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6aab4e12ef4c9f8a8cad31da66043cb6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6aab4e12ef4c9f8a8cad31da66043cb6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6aab4e12ef4c9f8a8cad31da66043cb6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6aab4e12ef4c9f8a8cad31da66043cb6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6aab4e12ef4c9f8a8cad31da66043cb6,
        type_description_1,
        par_self,
        var_amount
    );


    // Release cached frame.
    if ( frame_6aab4e12ef4c9f8a8cad31da66043cb6 == cache_frame_6aab4e12ef4c9f8a8cad31da66043cb6 )
    {
        Py_DECREF( frame_6aab4e12ef4c9f8a8cad31da66043cb6 );
    }
    cache_frame_6aab4e12ef4c9f8a8cad31da66043cb6 = NULL;

    assertFrameObject( frame_6aab4e12ef4c9f8a8cad31da66043cb6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_1_normalized );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_amount );
    var_amount = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_amount );
    var_amount = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_1_normalized );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_grammars$money$$$function_2_normalized( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ab3ce681a8f810bd8d36d512ce7c4917;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ab3ce681a8f810bd8d36d512ce7c4917 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ab3ce681a8f810bd8d36d512ce7c4917, codeobj_ab3ce681a8f810bd8d36d512ce7c4917, module_grammars$money, sizeof(void *) );
    frame_ab3ce681a8f810bd8d36d512ce7c4917 = cache_frame_ab3ce681a8f810bd8d36d512ce7c4917;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ab3ce681a8f810bd8d36d512ce7c4917 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ab3ce681a8f810bd8d36d512ce7c4917 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Rate );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_money );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 58;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_normalized );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 58;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_period );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 59;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_ab3ce681a8f810bd8d36d512ce7c4917->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ab3ce681a8f810bd8d36d512ce7c4917 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ab3ce681a8f810bd8d36d512ce7c4917 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ab3ce681a8f810bd8d36d512ce7c4917 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ab3ce681a8f810bd8d36d512ce7c4917, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ab3ce681a8f810bd8d36d512ce7c4917->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ab3ce681a8f810bd8d36d512ce7c4917, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ab3ce681a8f810bd8d36d512ce7c4917,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_ab3ce681a8f810bd8d36d512ce7c4917 == cache_frame_ab3ce681a8f810bd8d36d512ce7c4917 )
    {
        Py_DECREF( frame_ab3ce681a8f810bd8d36d512ce7c4917 );
    }
    cache_frame_ab3ce681a8f810bd8d36d512ce7c4917 = NULL;

    assertFrameObject( frame_ab3ce681a8f810bd8d36d512ce7c4917 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_2_normalized );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_2_normalized );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_grammars$money$$$function_3_normalized( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_min = NULL;
    PyObject *var_max = NULL;
    struct Nuitka_FrameObject *frame_4af84bfe423338aa920b7f3085424170;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_4af84bfe423338aa920b7f3085424170 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4af84bfe423338aa920b7f3085424170, codeobj_4af84bfe423338aa920b7f3085424170, module_grammars$money, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4af84bfe423338aa920b7f3085424170 = cache_frame_4af84bfe423338aa920b7f3085424170;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4af84bfe423338aa920b7f3085424170 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4af84bfe423338aa920b7f3085424170 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_min );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_normalized );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_min == NULL );
        var_min = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_max );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_normalized );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_max == NULL );
        var_max = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_min );
        tmp_source_name_5 = var_min;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_currency );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_max );
            tmp_source_name_6 = var_max;
            tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_currency );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_min );
            tmp_assattr_target_1 = var_min;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_currency, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 75;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 76;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_min );
        tmp_args_element_name_1 = var_min;
        CHECK_OBJECT( var_max );
        tmp_args_element_name_2 = var_max;
        frame_4af84bfe423338aa920b7f3085424170->m_frame.f_lineno = 76;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_Range, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4af84bfe423338aa920b7f3085424170 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4af84bfe423338aa920b7f3085424170 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4af84bfe423338aa920b7f3085424170 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4af84bfe423338aa920b7f3085424170, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4af84bfe423338aa920b7f3085424170->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4af84bfe423338aa920b7f3085424170, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4af84bfe423338aa920b7f3085424170,
        type_description_1,
        par_self,
        var_min,
        var_max
    );


    // Release cached frame.
    if ( frame_4af84bfe423338aa920b7f3085424170 == cache_frame_4af84bfe423338aa920b7f3085424170 )
    {
        Py_DECREF( frame_4af84bfe423338aa920b7f3085424170 );
    }
    cache_frame_4af84bfe423338aa920b7f3085424170 = NULL;

    assertFrameObject( frame_4af84bfe423338aa920b7f3085424170 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_3_normalized );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_min );
    Py_DECREF( var_min );
    var_min = NULL;

    CHECK_OBJECT( (PyObject *)var_max );
    Py_DECREF( var_max );
    var_max = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_min );
    var_min = NULL;

    Py_XDECREF( var_max );
    var_max = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_3_normalized );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_grammars$money$$$function_4_normalize_integer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *var_integer = NULL;
    struct Nuitka_FrameObject *frame_72f42d2ae68f45ed855436d380208b3f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_72f42d2ae68f45ed855436d380208b3f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_72f42d2ae68f45ed855436d380208b3f, codeobj_72f42d2ae68f45ed855436d380208b3f, module_grammars$money, sizeof(void *)+sizeof(void *) );
    frame_72f42d2ae68f45ed855436d380208b3f = cache_frame_72f42d2ae68f45ed855436d380208b3f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_72f42d2ae68f45ed855436d380208b3f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_72f42d2ae68f45ed855436d380208b3f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        tmp_args_element_name_1 = const_str_digest_d2c6bd976f7c4a7ac144dc0944fc507c;
        tmp_args_element_name_2 = const_str_empty;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_3 = par_value;
        frame_72f42d2ae68f45ed855436d380208b3f->m_frame.f_lineno = 243;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_sub, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_integer == NULL );
        var_integer = tmp_assign_source_1;
    }
    {
        PyObject *tmp_int_arg_1;
        CHECK_OBJECT( var_integer );
        tmp_int_arg_1 = var_integer;
        tmp_return_value = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 244;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_72f42d2ae68f45ed855436d380208b3f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_72f42d2ae68f45ed855436d380208b3f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_72f42d2ae68f45ed855436d380208b3f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_72f42d2ae68f45ed855436d380208b3f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_72f42d2ae68f45ed855436d380208b3f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_72f42d2ae68f45ed855436d380208b3f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_72f42d2ae68f45ed855436d380208b3f,
        type_description_1,
        par_value,
        var_integer
    );


    // Release cached frame.
    if ( frame_72f42d2ae68f45ed855436d380208b3f == cache_frame_72f42d2ae68f45ed855436d380208b3f )
    {
        Py_DECREF( frame_72f42d2ae68f45ed855436d380208b3f );
    }
    cache_frame_72f42d2ae68f45ed855436d380208b3f = NULL;

    assertFrameObject( frame_72f42d2ae68f45ed855436d380208b3f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_4_normalize_integer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)var_integer );
    Py_DECREF( var_integer );
    var_integer = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var_integer );
    var_integer = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_4_normalize_integer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_grammars$money$$$function_5_normalize_fraction( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_value = python_pars[ 0 ];
    PyObject *var_fraction = NULL;
    struct Nuitka_FrameObject *frame_148d079781484ab729137f0240324599;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_148d079781484ab729137f0240324599 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_148d079781484ab729137f0240324599, codeobj_148d079781484ab729137f0240324599, module_grammars$money, sizeof(void *)+sizeof(void *) );
    frame_148d079781484ab729137f0240324599 = cache_frame_148d079781484ab729137f0240324599;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_148d079781484ab729137f0240324599 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_148d079781484ab729137f0240324599 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_value );
        tmp_called_instance_1 = par_value;
        frame_148d079781484ab729137f0240324599->m_frame.f_lineno = 248;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_ljust, &PyTuple_GET_ITEM( const_tuple_int_pos_2_str_plain_0_tuple, 0 ) );

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_fraction == NULL );
        var_fraction = tmp_assign_source_1;
    }
    {
        PyObject *tmp_int_arg_1;
        CHECK_OBJECT( var_fraction );
        tmp_int_arg_1 = var_fraction;
        tmp_return_value = PyNumber_Int( tmp_int_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_148d079781484ab729137f0240324599 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_148d079781484ab729137f0240324599 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_148d079781484ab729137f0240324599 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_148d079781484ab729137f0240324599, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_148d079781484ab729137f0240324599->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_148d079781484ab729137f0240324599, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_148d079781484ab729137f0240324599,
        type_description_1,
        par_value,
        var_fraction
    );


    // Release cached frame.
    if ( frame_148d079781484ab729137f0240324599 == cache_frame_148d079781484ab729137f0240324599 )
    {
        Py_DECREF( frame_148d079781484ab729137f0240324599 );
    }
    cache_frame_148d079781484ab729137f0240324599 = NULL;

    assertFrameObject( frame_148d079781484ab729137f0240324599 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_5_normalize_fraction );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)var_fraction );
    Py_DECREF( var_fraction );
    var_fraction = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var_fraction );
    var_fraction = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( grammars$money$$$function_5_normalize_fraction );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_grammars$money$$$function_1_normalized(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$money$$$function_1_normalized,
        const_str_plain_normalized,
#if PYTHON_VERSION >= 300
        const_str_digest_1e3f6bdc2a647812d6f448036fdfc355,
#endif
        codeobj_6aab4e12ef4c9f8a8cad31da66043cb6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$money,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_grammars$money$$$function_2_normalized(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$money$$$function_2_normalized,
        const_str_plain_normalized,
#if PYTHON_VERSION >= 300
        const_str_digest_965adec4a43b2a0478ac4e9936b60ebc,
#endif
        codeobj_ab3ce681a8f810bd8d36d512ce7c4917,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$money,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_grammars$money$$$function_3_normalized(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$money$$$function_3_normalized,
        const_str_plain_normalized,
#if PYTHON_VERSION >= 300
        const_str_digest_7e65076f194e6bb0570c15048cbf7536,
#endif
        codeobj_4af84bfe423338aa920b7f3085424170,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$money,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_grammars$money$$$function_4_normalize_integer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$money$$$function_4_normalize_integer,
        const_str_plain_normalize_integer,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_72f42d2ae68f45ed855436d380208b3f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$money,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_grammars$money$$$function_5_normalize_fraction(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_grammars$money$$$function_5_normalize_fraction,
        const_str_plain_normalize_fraction,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_148d079781484ab729137f0240324599,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_grammars$money,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_grammars$money =
{
    PyModuleDef_HEAD_INIT,
    "grammars.money",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(grammars$money)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(grammars$money)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_grammars$money );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("grammars.money: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.money: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("grammars.money: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initgrammars$money" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_grammars$money = Py_InitModule4(
        "grammars.money",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_grammars$money = PyModule_Create( &mdef_grammars$money );
#endif

    moduledict_grammars$money = MODULE_DICT( module_grammars$money );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_grammars$money,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_grammars$money,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$money,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_grammars$money,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_grammars$money );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_754bbdb51d29ec017fab6feb3184da49, module_grammars$money );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    PyObject *tmp_import_from_5__module = NULL;
    struct Nuitka_FrameObject *frame_61aadf8837dc934bae8e309d56e02ee2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_grammars$money_35 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_9b311b5df972be73e01887347bf06532_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_9b311b5df972be73e01887347bf06532_2 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_grammars$money_54 = NULL;
    struct Nuitka_FrameObject *frame_b1f3733f6842e66afa864afad3f5419b_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_b1f3733f6842e66afa864afad3f5419b_3 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *locals_grammars$money_69 = NULL;
    struct Nuitka_FrameObject *frame_3f86f284caacec5df8359fcd1b6e88d8_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3f86f284caacec5df8359fcd1b6e88d8_4 = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_61aadf8837dc934bae8e309d56e02ee2 = MAKE_MODULE_FRAME( codeobj_61aadf8837dc934bae8e309d56e02ee2, module_grammars$money );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_61aadf8837dc934bae8e309d56e02ee2 );
    assert( Py_REFCNT( frame_61aadf8837dc934bae8e309d56e02ee2 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 2;
        tmp_assign_source_4 = PyImport_ImportModule("__future__");
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_import_from_1__module == NULL );
        Py_INCREF( tmp_assign_source_4 );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_division );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_division, tmp_assign_source_6 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_grammars$money;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 4;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_yargy;
        tmp_globals_name_2 = (PyObject *)moduledict_grammars$money;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_rule_str_plain_and__str_plain_or__tuple;
        tmp_level_name_2 = const_int_0;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 6;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_3 = tmp_import_from_2__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_rule );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_and_ );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_and_, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_or_ );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_, tmp_assign_source_11 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_0b01e19927d4b494ab1f3c70e75cc4b2;
        tmp_globals_name_3 = (PyObject *)moduledict_grammars$money;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_fact_str_plain_const_tuple;
        tmp_level_name_3 = const_int_0;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 10;
        tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_12;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_6 = tmp_import_from_3__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_fact );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_fact, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_7 = tmp_import_from_3__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_const );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const, tmp_assign_source_14 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_ea78dcde38a5066c777d6274d25657b3;
        tmp_globals_name_4 = (PyObject *)moduledict_grammars$money;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_c6f5501d99c5b23e70ddc5c94c5798df_tuple;
        tmp_level_name_4 = const_int_0;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 14;
        tmp_assign_source_15 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_15;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_8 = tmp_import_from_4__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_eq );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_9 = tmp_import_from_4__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_length_eq );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_length_eq, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_10 = tmp_import_from_4__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_in_ );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_in_, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_11 = tmp_import_from_4__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_in_caseless );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_in_caseless, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_12 = tmp_import_from_4__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_gram );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_gram, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_13 = tmp_import_from_4__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_type );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_type, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_14 = tmp_import_from_4__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_normalized );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_15 = tmp_import_from_4__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_caseless );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_16 = tmp_import_from_4__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_dictionary );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dictionary, tmp_assign_source_24 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_17;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_9ab9015b4a2054b77da734c3e8a94159;
        tmp_globals_name_5 = (PyObject *)moduledict_grammars$money;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_Record_tuple;
        tmp_level_name_5 = const_int_0;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 21;
        tmp_import_name_from_17 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_Record );
        Py_DECREF( tmp_import_name_from_17 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Record, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_8c677a17282a622d6297d1a88d7f30d4;
        tmp_globals_name_6 = (PyObject *)moduledict_grammars$money;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_Normalizable_str_plain_money_tuple;
        tmp_level_name_6 = const_int_0;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 23;
        tmp_assign_source_26 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_5__module == NULL );
        tmp_import_from_5__module = tmp_assign_source_26;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_18 = tmp_import_from_5__module;
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_Normalizable );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Normalizable, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_19;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_19 = tmp_import_from_5__module;
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_money );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl, tmp_assign_source_28 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_arg_element_1;
        PyObject *tmp_call_arg_element_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 29;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_call_arg_element_1 = const_str_plain_Money;
        tmp_call_arg_element_2 = LIST_COPY( const_list_5bb9d78e7987bc0c2182e5db86b90e03_list );
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 29;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2 };
            tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money, tmp_assign_source_29 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_tuple_element_1 = tmp_mvar_value_4;
        tmp_assign_source_30 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_30, 0, tmp_tuple_element_1 );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Normalizable );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Normalizable );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_assign_source_30 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Normalizable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 35;

            goto try_except_handler_6;
        }

        tmp_tuple_element_1 = tmp_mvar_value_5;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_30, 1, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_30;
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_31 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_33 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_33;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_6;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;

                goto try_except_handler_6;
            }
            tmp_tuple_element_2 = const_str_plain_Money;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 35;
            tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_34;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;

                goto try_except_handler_6;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;

                    goto try_except_handler_6;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 35;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 35;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_35;
            tmp_assign_source_35 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_35;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_36;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_grammars$money_35 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_754bbdb51d29ec017fab6feb3184da49;
        tmp_res = PyObject_SetItem( locals_grammars$money_35, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_Money;
        tmp_res = PyObject_SetItem( locals_grammars$money_35, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_9b311b5df972be73e01887347bf06532_2, codeobj_9b311b5df972be73e01887347bf06532, module_grammars$money, sizeof(void *) );
        frame_9b311b5df972be73e01887347bf06532_2 = cache_frame_9b311b5df972be73e01887347bf06532_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_9b311b5df972be73e01887347bf06532_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_9b311b5df972be73e01887347bf06532_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_2;
            tmp_res = MAPPING_HAS_ITEM( locals_grammars$money_35, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_3 = PyObject_GetItem( locals_grammars$money_35, const_str_plain_property );

            if ( tmp_called_name_3 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_1 = MAKE_FUNCTION_grammars$money$$$function_1_normalized(  );



            frame_9b311b5df972be73e01887347bf06532_2->m_frame.f_lineno = 36;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_called_name_4 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_2 = MAKE_FUNCTION_grammars$money$$$function_1_normalized(  );



            frame_9b311b5df972be73e01887347bf06532_2->m_frame.f_lineno = 36;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_grammars$money_35, const_str_plain_normalized, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_9b311b5df972be73e01887347bf06532_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_9b311b5df972be73e01887347bf06532_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_9b311b5df972be73e01887347bf06532_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_9b311b5df972be73e01887347bf06532_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_9b311b5df972be73e01887347bf06532_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_9b311b5df972be73e01887347bf06532_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_9b311b5df972be73e01887347bf06532_2 == cache_frame_9b311b5df972be73e01887347bf06532_2 )
        {
            Py_DECREF( frame_9b311b5df972be73e01887347bf06532_2 );
        }
        cache_frame_9b311b5df972be73e01887347bf06532_2 = NULL;

        assertFrameObject( frame_9b311b5df972be73e01887347bf06532_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_8;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;

                goto try_except_handler_8;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_grammars$money_35, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;

                goto try_except_handler_8;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_5 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_Money;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_grammars$money_35;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 35;
            tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;

                goto try_except_handler_8;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_37;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_36 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_36 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_grammars$money_35 );
        locals_grammars$money_35 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_grammars$money_35 );
        locals_grammars$money_35 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 35;
        goto try_except_handler_6;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money, tmp_assign_source_36 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_call_arg_element_3;
        PyObject *tmp_call_arg_element_4;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        tmp_call_arg_element_3 = const_str_plain_Rate;
        tmp_call_arg_element_4 = LIST_COPY( const_list_str_plain_money_str_plain_period_list );
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_call_arg_element_3, tmp_call_arg_element_4 };
            tmp_assign_source_38 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_call_arg_element_4 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Rate, tmp_assign_source_38 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Rate );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rate );
        }

        CHECK_OBJECT( tmp_mvar_value_7 );
        tmp_tuple_element_5 = tmp_mvar_value_7;
        tmp_assign_source_39 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_39, 0, tmp_tuple_element_5 );
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Normalizable );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Normalizable );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_assign_source_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Normalizable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;

            goto try_except_handler_9;
        }

        tmp_tuple_element_5 = tmp_mvar_value_8;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_39, 1, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_40 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        tmp_condition_result_9 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_5:;
        condexpr_end_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_42 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_42;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_9;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_9;
            }
            tmp_tuple_element_6 = const_str_plain_Rate;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 54;
            tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_43;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_9;
            }
            tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 54;

                    goto try_except_handler_9;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 54;

                    goto try_except_handler_9;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 54;

                    goto try_except_handler_9;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 54;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_9;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_44;
            tmp_assign_source_44 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_44;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_45;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_grammars$money_54 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_754bbdb51d29ec017fab6feb3184da49;
        tmp_res = PyObject_SetItem( locals_grammars$money_54, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_plain_Rate;
        tmp_res = PyObject_SetItem( locals_grammars$money_54, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto try_except_handler_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_b1f3733f6842e66afa864afad3f5419b_3, codeobj_b1f3733f6842e66afa864afad3f5419b, module_grammars$money, sizeof(void *) );
        frame_b1f3733f6842e66afa864afad3f5419b_3 = cache_frame_b1f3733f6842e66afa864afad3f5419b_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_b1f3733f6842e66afa864afad3f5419b_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_b1f3733f6842e66afa864afad3f5419b_3 ) == 2 ); // Frame stack

        // Framed code:
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_called_name_9;
            PyObject *tmp_args_element_name_4;
            tmp_res = MAPPING_HAS_ITEM( locals_grammars$money_54, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_13 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_6;
            }
            else
            {
                goto condexpr_false_6;
            }
            condexpr_true_6:;
            tmp_called_name_8 = PyObject_GetItem( locals_grammars$money_54, const_str_plain_property );

            if ( tmp_called_name_8 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_args_element_name_3 = MAKE_FUNCTION_grammars$money$$$function_2_normalized(  );



            frame_b1f3733f6842e66afa864afad3f5419b_3->m_frame.f_lineno = 55;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_6;
            condexpr_false_6:;
            tmp_called_name_9 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_4 = MAKE_FUNCTION_grammars$money$$$function_2_normalized(  );



            frame_b1f3733f6842e66afa864afad3f5419b_3->m_frame.f_lineno = 55;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_6:;
            tmp_res = PyObject_SetItem( locals_grammars$money_54, const_str_plain_normalized, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 55;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_b1f3733f6842e66afa864afad3f5419b_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_b1f3733f6842e66afa864afad3f5419b_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_b1f3733f6842e66afa864afad3f5419b_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_b1f3733f6842e66afa864afad3f5419b_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_b1f3733f6842e66afa864afad3f5419b_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_b1f3733f6842e66afa864afad3f5419b_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_b1f3733f6842e66afa864afad3f5419b_3 == cache_frame_b1f3733f6842e66afa864afad3f5419b_3 )
        {
            Py_DECREF( frame_b1f3733f6842e66afa864afad3f5419b_3 );
        }
        cache_frame_b1f3733f6842e66afa864afad3f5419b_3 = NULL;

        assertFrameObject( frame_b1f3733f6842e66afa864afad3f5419b_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_11;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_11;
            }
            tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_grammars$money_54, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_11;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_10 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_Rate;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_grammars$money_54;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 54;
            tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_11;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_46;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_45 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_45 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        Py_DECREF( locals_grammars$money_54 );
        locals_grammars$money_54 = NULL;
        goto try_return_handler_10;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_grammars$money_54 );
        locals_grammars$money_54 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_10;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 54;
        goto try_except_handler_9;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Rate, tmp_assign_source_45 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_arg_element_5;
        PyObject *tmp_call_arg_element_6;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_fact );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fact );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fact" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_9;
        tmp_call_arg_element_5 = const_str_plain_Range;
        tmp_call_arg_element_6 = LIST_COPY( const_list_str_plain_min_str_plain_max_list );
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_call_arg_element_5, tmp_call_arg_element_6 };
            tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_call_arg_element_6 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Range, tmp_assign_source_47 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Range );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Range );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_tuple_element_9 = tmp_mvar_value_10;
        tmp_assign_source_48 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_assign_source_48, 0, tmp_tuple_element_9 );
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Normalizable );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Normalizable );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_assign_source_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Normalizable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;

            goto try_except_handler_12;
        }

        tmp_tuple_element_9 = tmp_mvar_value_11;
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_assign_source_48, 1, tmp_tuple_element_9 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_48;
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_49 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_49;
    }
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_50;
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_16;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        tmp_condition_result_16 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_8:;
        condexpr_end_7:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_51 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_51;
    }
    {
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_12;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_18;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_9 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_52;
            PyObject *tmp_called_name_12;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_10 = tmp_class_creation_3__metaclass;
            tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;

                goto try_except_handler_12;
            }
            tmp_tuple_element_10 = const_str_plain_Range;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_10 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 69;
            tmp_assign_source_52 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_52 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;

                goto try_except_handler_12;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_52;
        }
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_11 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;

                goto try_except_handler_12;
            }
            tmp_condition_result_19 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_11;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_11 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 69;

                    goto try_except_handler_12;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_11 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 69;

                    goto try_except_handler_12;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_11 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 69;

                    goto try_except_handler_12;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 69;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_12;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_53;
            tmp_assign_source_53 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_53;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_54;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_grammars$money_69 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_754bbdb51d29ec017fab6feb3184da49;
        tmp_res = PyObject_SetItem( locals_grammars$money_69, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_14;
        }
        tmp_dictset_value = const_str_plain_Range;
        tmp_res = PyObject_SetItem( locals_grammars$money_69, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;

            goto try_except_handler_14;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3f86f284caacec5df8359fcd1b6e88d8_4, codeobj_3f86f284caacec5df8359fcd1b6e88d8, module_grammars$money, sizeof(void *) );
        frame_3f86f284caacec5df8359fcd1b6e88d8_4 = cache_frame_3f86f284caacec5df8359fcd1b6e88d8_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3f86f284caacec5df8359fcd1b6e88d8_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3f86f284caacec5df8359fcd1b6e88d8_4 ) == 2 ); // Frame stack

        // Framed code:
        {
            nuitka_bool tmp_condition_result_20;
            PyObject *tmp_called_name_13;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_element_name_6;
            tmp_res = MAPPING_HAS_ITEM( locals_grammars$money_69, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_20 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_9;
            }
            else
            {
                goto condexpr_false_9;
            }
            condexpr_true_9:;
            tmp_called_name_13 = PyObject_GetItem( locals_grammars$money_69, const_str_plain_property );

            if ( tmp_called_name_13 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_grammars$money$$$function_3_normalized(  );



            frame_3f86f284caacec5df8359fcd1b6e88d8_4->m_frame.f_lineno = 70;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
            }

            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_9;
            condexpr_false_9:;
            tmp_called_name_14 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_6 = MAKE_FUNCTION_grammars$money$$$function_3_normalized(  );



            frame_3f86f284caacec5df8359fcd1b6e88d8_4->m_frame.f_lineno = 70;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_9:;
            tmp_res = PyObject_SetItem( locals_grammars$money_69, const_str_plain_normalized, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3f86f284caacec5df8359fcd1b6e88d8_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3f86f284caacec5df8359fcd1b6e88d8_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3f86f284caacec5df8359fcd1b6e88d8_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3f86f284caacec5df8359fcd1b6e88d8_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3f86f284caacec5df8359fcd1b6e88d8_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3f86f284caacec5df8359fcd1b6e88d8_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_3f86f284caacec5df8359fcd1b6e88d8_4 == cache_frame_3f86f284caacec5df8359fcd1b6e88d8_4 )
        {
            Py_DECREF( frame_3f86f284caacec5df8359fcd1b6e88d8_4 );
        }
        cache_frame_3f86f284caacec5df8359fcd1b6e88d8_4 = NULL;

        assertFrameObject( frame_3f86f284caacec5df8359fcd1b6e88d8_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_14;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_21;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;

                goto try_except_handler_14;
            }
            tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_grammars$money_69, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;

                goto try_except_handler_14;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_55;
            PyObject *tmp_called_name_15;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_15 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_12 = const_str_plain_Range;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_12 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_12 );
            tmp_tuple_element_12 = locals_grammars$money_69;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 69;
            tmp_assign_source_55 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_55 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;

                goto try_except_handler_14;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_55;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_54 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_54 );
        goto try_return_handler_14;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        Py_DECREF( locals_grammars$money_69 );
        locals_grammars$money_69 = NULL;
        goto try_return_handler_13;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_grammars$money_69 );
        locals_grammars$money_69 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto try_except_handler_13;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( grammars$money );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 69;
        goto try_except_handler_12;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Range, tmp_assign_source_54 );
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_12;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 79;
        tmp_assign_source_56 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

        if ( tmp_assign_source_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_type );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_type );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 80;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_13;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 80;
        tmp_assign_source_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_plain_INT_tuple, 0 ) );

        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_called_name_18;
        PyObject *tmp_source_name_13;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_18;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 90;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_14;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_15;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 91;
        tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_str_digest_1e49954e22a85f9441edab70f6b8f8cc_tuple, 0 ) );

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_args_element_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_16;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 92;
        tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_str_chr_8364_tuple, 0 ) );

        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_7 );

            exception_lineno = 92;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_source_name_13 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_source_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_13 );
        if ( tmp_called_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_const );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_called_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "const" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_17;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_called_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_18;
        tmp_args_element_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_EURO );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_18 );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_18 );

            exception_lineno = 94;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_assign_source_58 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_called_name_18 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assign_source_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_EURO, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        PyObject *tmp_called_name_23;
        PyObject *tmp_source_name_15;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_source_name_16;
        PyObject *tmp_mvar_value_23;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_19;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_20;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 98;
        tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, &PyTuple_GET_ITEM( const_tuple_str_digest_b9aed00e01fcf634265224e656e794f2_tuple, 0 ) );

        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_args_element_name_11 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_21;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 99;
        tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, &PyTuple_GET_ITEM( const_tuple_str_chr_36_tuple, 0 ) );

        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_11 );

            exception_lineno = 99;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12 };
            tmp_source_name_15 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_args_element_name_11 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_source_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_15 );
        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_const );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_called_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "const" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_22;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_called_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = tmp_mvar_value_23;
        tmp_args_element_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_DOLLARS );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_23 );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
        }

        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_23 );

            exception_lineno = 101;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_assign_source_59 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_assign_source_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOLLARS, tmp_assign_source_59 );
    }
    {
        PyObject *tmp_assign_source_60;
        PyObject *tmp_called_name_28;
        PyObject *tmp_source_name_17;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_called_name_31;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_called_name_32;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_called_name_35;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_source_name_18;
        PyObject *tmp_mvar_value_34;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_24;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_25;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;

            goto frame_exception_exit_1;
        }

        tmp_called_name_31 = tmp_mvar_value_26;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 105;
        tmp_args_element_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, &PyTuple_GET_ITEM( const_tuple_str_digest_dcdf57511c5b0b189cd12de141314f13_tuple, 0 ) );

        if ( tmp_args_element_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, call_args );
        }

        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_args_element_name_15 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }

        tmp_called_name_32 = tmp_mvar_value_27;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_args_element_name_15 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }

        tmp_called_name_33 = tmp_mvar_value_28;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_args_element_name_15 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;

            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_29;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 108;
        tmp_args_element_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, &PyTuple_GET_ITEM( const_tuple_str_digest_56ce7d87aa319265c4313bfd404de5bb_tuple, 0 ) );

        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_15 );

            exception_lineno = 108;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_args_element_name_15 );
            Py_DECREF( tmp_args_element_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 109;

            goto frame_exception_exit_1;
        }

        tmp_called_name_35 = tmp_mvar_value_30;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 109;
        tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_35, &PyTuple_GET_ITEM( const_tuple_str_chr_1088_tuple, 0 ) );

        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_15 );
            Py_DECREF( tmp_args_element_name_19 );

            exception_lineno = 109;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_args_element_name_15 );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_31;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 110;
        tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, &PyTuple_GET_ITEM( const_tuple_str_chr_8381_tuple, 0 ) );

        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_15 );
            Py_DECREF( tmp_args_element_name_19 );
            Py_DECREF( tmp_args_element_name_20 );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21 };
            tmp_args_element_name_18 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_33, call_args );
        }

        Py_DECREF( tmp_args_element_name_19 );
        Py_DECREF( tmp_args_element_name_20 );
        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_15 );

            exception_lineno = 107;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOT );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_args_element_name_15 );
            Py_DECREF( tmp_args_element_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_32;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 112;
        tmp_args_element_name_22 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_optional );
        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_15 );
            Py_DECREF( tmp_args_element_name_18 );

            exception_lineno = 112;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 106;
        {
            PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_22 };
            tmp_args_element_name_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_args_element_name_18 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_15 );

            exception_lineno = 106;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_17 };
            tmp_source_name_17 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_29, call_args );
        }

        Py_DECREF( tmp_args_element_name_15 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_source_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_called_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_17 );
        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_const );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_called_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "const" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_33;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_called_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }

        tmp_source_name_18 = tmp_mvar_value_34;
        tmp_args_element_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_RUBLES );
        if ( tmp_args_element_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_args_element_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, call_args );
        }

        Py_DECREF( tmp_args_element_name_24 );
        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 115;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_23 };
            tmp_assign_source_60 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_assign_source_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RUBLES, tmp_assign_source_60 );
    }
    {
        PyObject *tmp_assign_source_61;
        PyObject *tmp_called_name_38;
        PyObject *tmp_source_name_19;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_source_name_20;
        PyObject *tmp_mvar_value_39;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_35;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_EURO );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EURO );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EURO" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_25 = tmp_mvar_value_36;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOLLARS );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOLLARS );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOLLARS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_26 = tmp_mvar_value_37;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RUBLES );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RUBLES );
        }

        CHECK_OBJECT( tmp_mvar_value_38 );
        tmp_args_element_name_27 = tmp_mvar_value_38;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26, tmp_args_element_name_27 };
            tmp_source_name_19 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_39, call_args );
        }

        if ( tmp_source_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        tmp_called_name_38 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_19 );
        if ( tmp_called_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_called_name_38 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;

            goto frame_exception_exit_1;
        }

        tmp_source_name_20 = tmp_mvar_value_39;
        tmp_args_element_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_currency );
        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_38 );

            exception_lineno = 123;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_assign_source_61 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_38, call_args );
        }

        Py_DECREF( tmp_called_name_38 );
        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_assign_source_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_CURRENCY, tmp_assign_source_61 );
    }
    {
        PyObject *tmp_assign_source_62;
        PyObject *tmp_called_name_40;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_name_41;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_called_name_43;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_called_name_45;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_47;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_40 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 126;

            goto frame_exception_exit_1;
        }

        tmp_called_name_40 = tmp_mvar_value_40;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }

        tmp_called_name_41 = tmp_mvar_value_41;
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_42 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;

            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_42;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 127;
        tmp_args_element_name_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_42, &PyTuple_GET_ITEM( const_tuple_str_digest_81ef627c76b7b5654449d61a984299c4_tuple, 0 ) );

        if ( tmp_args_element_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_41, call_args );
        }

        Py_DECREF( tmp_args_element_name_30 );
        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;

            goto frame_exception_exit_1;
        }

        tmp_called_name_43 = tmp_mvar_value_43;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_44;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }

        tmp_called_name_45 = tmp_mvar_value_45;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 130;
        tmp_args_element_name_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, &PyTuple_GET_ITEM( const_tuple_str_digest_254e47bddf4399538dfb2488241325ab_tuple, 0 ) );

        if ( tmp_args_element_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );

            exception_lineno = 130;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            Py_DECREF( tmp_args_element_name_33 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_46;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 131;
        tmp_args_element_name_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_46, &PyTuple_GET_ITEM( const_tuple_str_chr_1082_tuple, 0 ) );

        if ( tmp_args_element_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );
            Py_DECREF( tmp_args_element_name_33 );

            exception_lineno = 131;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_34 };
            tmp_args_element_name_32 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_44, call_args );
        }

        Py_DECREF( tmp_args_element_name_33 );
        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_args_element_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );

            exception_lineno = 129;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOT );
        }

        if ( tmp_mvar_value_47 == NULL )
        {
            Py_DECREF( tmp_args_element_name_29 );
            Py_DECREF( tmp_args_element_name_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_47;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 133;
        tmp_args_element_name_35 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_optional );
        if ( tmp_args_element_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );
            Py_DECREF( tmp_args_element_name_32 );

            exception_lineno = 133;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 128;
        {
            PyObject *call_args[] = { tmp_args_element_name_32, tmp_args_element_name_35 };
            tmp_args_element_name_31 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_43, call_args );
        }

        Py_DECREF( tmp_args_element_name_32 );
        Py_DECREF( tmp_args_element_name_35 );
        if ( tmp_args_element_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_29 );

            exception_lineno = 128;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 126;
        {
            PyObject *call_args[] = { tmp_args_element_name_29, tmp_args_element_name_31 };
            tmp_assign_source_62 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_40, call_args );
        }

        Py_DECREF( tmp_args_element_name_29 );
        Py_DECREF( tmp_args_element_name_31 );
        if ( tmp_assign_source_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_KOPEIKA, tmp_assign_source_62 );
    }
    {
        PyObject *tmp_assign_source_63;
        PyObject *tmp_called_name_47;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_called_name_49;
        PyObject *tmp_mvar_value_50;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_48 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;

            goto frame_exception_exit_1;
        }

        tmp_called_name_47 = tmp_mvar_value_48;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_49 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_49;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 138;
        tmp_args_element_name_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_48, &PyTuple_GET_ITEM( const_tuple_str_digest_772d0cc28b36ccfb74082c80a60cc725_tuple, 0 ) );

        if ( tmp_args_element_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_args_element_name_36 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }

        tmp_called_name_49 = tmp_mvar_value_50;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 139;
        tmp_args_element_name_37 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_49, &PyTuple_GET_ITEM( const_tuple_str_chr_162_tuple, 0 ) );

        if ( tmp_args_element_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_36 );

            exception_lineno = 139;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_36, tmp_args_element_name_37 };
            tmp_assign_source_63 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_47, call_args );
        }

        Py_DECREF( tmp_args_element_name_36 );
        Py_DECREF( tmp_args_element_name_37 );
        if ( tmp_assign_source_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_CENT, tmp_assign_source_63 );
    }
    {
        PyObject *tmp_assign_source_64;
        PyObject *tmp_called_name_50;
        PyObject *tmp_mvar_value_51;
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_51 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;

            goto frame_exception_exit_1;
        }

        tmp_called_name_50 = tmp_mvar_value_51;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 142;
        tmp_assign_source_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_50, &PyTuple_GET_ITEM( const_tuple_str_digest_0b75389130edf28a9b215a4bec8e96ca_tuple, 0 ) );

        if ( tmp_assign_source_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_EUROCENT, tmp_assign_source_64 );
    }
    {
        PyObject *tmp_assign_source_65;
        PyObject *tmp_called_name_51;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_called_name_52;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_called_name_53;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_args_element_name_42;
        PyObject *tmp_mvar_value_57;
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_52 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }

        tmp_called_name_51 = tmp_mvar_value_52;
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_KOPEIKA );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KOPEIKA );
        }

        if ( tmp_mvar_value_53 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KOPEIKA" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_38 = tmp_mvar_value_53;
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_54 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_called_name_52 = tmp_mvar_value_54;
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_CENT );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CENT );
        }

        if ( tmp_mvar_value_55 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CENT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_40 = tmp_mvar_value_55;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 146;
        {
            PyObject *call_args[] = { tmp_args_element_name_40 };
            tmp_args_element_name_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_52, call_args );
        }

        if ( tmp_args_element_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_called_name_53 = tmp_mvar_value_56;
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_EUROCENT );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EUROCENT );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_args_element_name_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EUROCENT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_42 = tmp_mvar_value_57;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_42 };
            tmp_args_element_name_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_53, call_args );
        }

        if ( tmp_args_element_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_39 );

            exception_lineno = 147;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_38, tmp_args_element_name_39, tmp_args_element_name_41 };
            tmp_assign_source_65 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_51, call_args );
        }

        Py_DECREF( tmp_args_element_name_39 );
        Py_DECREF( tmp_args_element_name_41 );
        if ( tmp_assign_source_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_CURRENCY, tmp_assign_source_65 );
    }
    {
        PyObject *tmp_assign_source_66;
        PyObject *tmp_called_name_54;
        PyObject *tmp_source_name_21;
        PyObject *tmp_called_name_55;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_called_name_56;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_called_name_57;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_called_name_58;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_called_name_59;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_args_element_name_48;
        PyObject *tmp_called_name_60;
        PyObject *tmp_mvar_value_64;
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_58 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 158;

            goto frame_exception_exit_1;
        }

        tmp_called_name_55 = tmp_mvar_value_58;
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_59 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }

        tmp_called_name_56 = tmp_mvar_value_59;
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_60 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }

        tmp_called_name_57 = tmp_mvar_value_60;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 159;
        tmp_args_element_name_44 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_57, &PyTuple_GET_ITEM( const_tuple_str_digest_98fbc9a81cd6a5dca3fa20e9cd07d0d0_tuple, 0 ) );

        if ( tmp_args_element_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOT );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_args_element_name_44 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_61;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 159;
        tmp_args_element_name_45 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_optional );
        if ( tmp_args_element_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_44 );

            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 159;
        {
            PyObject *call_args[] = { tmp_args_element_name_44, tmp_args_element_name_45 };
            tmp_args_element_name_43 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_56, call_args );
        }

        Py_DECREF( tmp_args_element_name_44 );
        Py_DECREF( tmp_args_element_name_45 );
        if ( tmp_args_element_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_args_element_name_43 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_called_name_58 = tmp_mvar_value_62;
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_args_element_name_43 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }

        tmp_called_name_59 = tmp_mvar_value_63;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 160;
        tmp_args_element_name_47 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_59, &PyTuple_GET_ITEM( const_tuple_str_digest_c6e75e4da4cb0b2f9ee775e896aba69b_tuple, 0 ) );

        if ( tmp_args_element_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_43 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_47 };
            tmp_args_element_name_46 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_58, call_args );
        }

        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_args_element_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_43 );

            exception_lineno = 160;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_43, tmp_args_element_name_46 };
            tmp_source_name_21 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_55, call_args );
        }

        Py_DECREF( tmp_args_element_name_43 );
        Py_DECREF( tmp_args_element_name_46 );
        if ( tmp_source_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;

            goto frame_exception_exit_1;
        }
        tmp_called_name_54 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_21 );
        if ( tmp_called_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_const );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_called_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "const" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;

            goto frame_exception_exit_1;
        }

        tmp_called_name_60 = tmp_mvar_value_64;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 162;
        tmp_args_element_name_48 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_60, &PyTuple_GET_ITEM( const_tuple_int_pos_1000000000_tuple, 0 ) );

        if ( tmp_args_element_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_54 );

            exception_lineno = 162;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_48 };
            tmp_assign_source_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_54, call_args );
        }

        Py_DECREF( tmp_called_name_54 );
        Py_DECREF( tmp_args_element_name_48 );
        if ( tmp_assign_source_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MILLIARD, tmp_assign_source_66 );
    }
    {
        PyObject *tmp_assign_source_67;
        PyObject *tmp_called_name_61;
        PyObject *tmp_source_name_22;
        PyObject *tmp_called_name_62;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_args_element_name_49;
        PyObject *tmp_called_name_63;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_args_element_name_50;
        PyObject *tmp_called_name_64;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_args_element_name_51;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_args_element_name_52;
        PyObject *tmp_called_name_65;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_args_element_name_53;
        PyObject *tmp_called_name_66;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_args_element_name_54;
        PyObject *tmp_called_name_67;
        PyObject *tmp_mvar_value_71;
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_65 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;

            goto frame_exception_exit_1;
        }

        tmp_called_name_62 = tmp_mvar_value_65;
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_66 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_called_name_63 = tmp_mvar_value_66;
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_67 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_called_name_64 = tmp_mvar_value_67;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 166;
        tmp_args_element_name_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_64, &PyTuple_GET_ITEM( const_tuple_str_digest_4c8eff7f5e29462fc7c29201d0400be1_tuple, 0 ) );

        if ( tmp_args_element_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOT );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_args_element_name_50 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_68;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 166;
        tmp_args_element_name_51 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_optional );
        if ( tmp_args_element_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_50 );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_50, tmp_args_element_name_51 };
            tmp_args_element_name_49 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_63, call_args );
        }

        Py_DECREF( tmp_args_element_name_50 );
        Py_DECREF( tmp_args_element_name_51 );
        if ( tmp_args_element_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_called_name_65 = tmp_mvar_value_69;
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_args_element_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }

        tmp_called_name_66 = tmp_mvar_value_70;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 167;
        tmp_args_element_name_53 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_66, &PyTuple_GET_ITEM( const_tuple_str_digest_d42564e7189ed8786c6f9df2730bd9a7_tuple, 0 ) );

        if ( tmp_args_element_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 167;
        {
            PyObject *call_args[] = { tmp_args_element_name_53 };
            tmp_args_element_name_52 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_65, call_args );
        }

        Py_DECREF( tmp_args_element_name_53 );
        if ( tmp_args_element_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_49 );

            exception_lineno = 167;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_49, tmp_args_element_name_52 };
            tmp_source_name_22 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_62, call_args );
        }

        Py_DECREF( tmp_args_element_name_49 );
        Py_DECREF( tmp_args_element_name_52 );
        if ( tmp_source_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_called_name_61 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_22 );
        if ( tmp_called_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_const );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_called_name_61 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "const" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }

        tmp_called_name_67 = tmp_mvar_value_71;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 169;
        tmp_args_element_name_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_67, &PyTuple_GET_ITEM( const_tuple_int_pos_1000000_tuple, 0 ) );

        if ( tmp_args_element_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_61 );

            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_54 };
            tmp_assign_source_67 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_61, call_args );
        }

        Py_DECREF( tmp_called_name_61 );
        Py_DECREF( tmp_args_element_name_54 );
        if ( tmp_assign_source_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MILLION, tmp_assign_source_67 );
    }
    {
        PyObject *tmp_assign_source_68;
        PyObject *tmp_called_name_68;
        PyObject *tmp_source_name_23;
        PyObject *tmp_called_name_69;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_args_element_name_55;
        PyObject *tmp_called_name_70;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_args_element_name_56;
        PyObject *tmp_called_name_71;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_args_element_name_58;
        PyObject *tmp_called_name_72;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_args_element_name_59;
        PyObject *tmp_called_name_73;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_args_element_name_60;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_args_element_name_61;
        PyObject *tmp_called_name_74;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_args_element_name_62;
        PyObject *tmp_called_name_75;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_args_element_name_63;
        PyObject *tmp_called_name_76;
        PyObject *tmp_mvar_value_81;
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_72 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 172;

            goto frame_exception_exit_1;
        }

        tmp_called_name_69 = tmp_mvar_value_72;
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_73 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_called_name_70 = tmp_mvar_value_73;
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_74 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_called_name_71 = tmp_mvar_value_74;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 173;
        tmp_args_element_name_56 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_71, &PyTuple_GET_ITEM( const_tuple_str_chr_1090_tuple, 0 ) );

        if ( tmp_args_element_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOT );
        }

        if ( tmp_mvar_value_75 == NULL )
        {
            Py_DECREF( tmp_args_element_name_56 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 173;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_57 = tmp_mvar_value_75;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 173;
        {
            PyObject *call_args[] = { tmp_args_element_name_56, tmp_args_element_name_57 };
            tmp_args_element_name_55 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_70, call_args );
        }

        Py_DECREF( tmp_args_element_name_56 );
        if ( tmp_args_element_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_76 == NULL )
        {
            Py_DECREF( tmp_args_element_name_55 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_called_name_72 = tmp_mvar_value_76;
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_caseless );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_caseless );
        }

        if ( tmp_mvar_value_77 == NULL )
        {
            Py_DECREF( tmp_args_element_name_55 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_called_name_73 = tmp_mvar_value_77;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 174;
        tmp_args_element_name_59 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_73, &PyTuple_GET_ITEM( const_tuple_str_digest_7169fd129074536e81f99b30f4e4ce37_tuple, 0 ) );

        if ( tmp_args_element_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_55 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DOT );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOT );
        }

        if ( tmp_mvar_value_78 == NULL )
        {
            Py_DECREF( tmp_args_element_name_55 );
            Py_DECREF( tmp_args_element_name_59 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DOT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_5 = tmp_mvar_value_78;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 174;
        tmp_args_element_name_60 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_optional );
        if ( tmp_args_element_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_55 );
            Py_DECREF( tmp_args_element_name_59 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 174;
        {
            PyObject *call_args[] = { tmp_args_element_name_59, tmp_args_element_name_60 };
            tmp_args_element_name_58 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_72, call_args );
        }

        Py_DECREF( tmp_args_element_name_59 );
        Py_DECREF( tmp_args_element_name_60 );
        if ( tmp_args_element_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_55 );

            exception_lineno = 174;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_79 == NULL )
        {
            Py_DECREF( tmp_args_element_name_55 );
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_called_name_74 = tmp_mvar_value_79;
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalized );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalized );
        }

        if ( tmp_mvar_value_80 == NULL )
        {
            Py_DECREF( tmp_args_element_name_55 );
            Py_DECREF( tmp_args_element_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalized" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }

        tmp_called_name_75 = tmp_mvar_value_80;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 175;
        tmp_args_element_name_62 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_75, &PyTuple_GET_ITEM( const_tuple_str_digest_41c141bf81764f9ac68af82f2b4ca503_tuple, 0 ) );

        if ( tmp_args_element_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_55 );
            Py_DECREF( tmp_args_element_name_58 );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_62 };
            tmp_args_element_name_61 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_74, call_args );
        }

        Py_DECREF( tmp_args_element_name_62 );
        if ( tmp_args_element_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_55 );
            Py_DECREF( tmp_args_element_name_58 );

            exception_lineno = 175;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 172;
        {
            PyObject *call_args[] = { tmp_args_element_name_55, tmp_args_element_name_58, tmp_args_element_name_61 };
            tmp_source_name_23 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_69, call_args );
        }

        Py_DECREF( tmp_args_element_name_55 );
        Py_DECREF( tmp_args_element_name_58 );
        Py_DECREF( tmp_args_element_name_61 );
        if ( tmp_source_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        tmp_called_name_68 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_23 );
        if ( tmp_called_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_const );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_const );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_called_name_68 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "const" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }

        tmp_called_name_76 = tmp_mvar_value_81;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 177;
        tmp_args_element_name_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_76, &PyTuple_GET_ITEM( const_tuple_int_pos_1000_tuple, 0 ) );

        if ( tmp_args_element_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_68 );

            exception_lineno = 177;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 172;
        {
            PyObject *call_args[] = { tmp_args_element_name_63 };
            tmp_assign_source_68 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_68, call_args );
        }

        Py_DECREF( tmp_called_name_68 );
        Py_DECREF( tmp_args_element_name_63 );
        if ( tmp_assign_source_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_THOUSAND, tmp_assign_source_68 );
    }
    {
        PyObject *tmp_assign_source_69;
        PyObject *tmp_called_name_77;
        PyObject *tmp_source_name_24;
        PyObject *tmp_called_name_78;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_args_element_name_64;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_mvar_value_85;
        PyObject *tmp_args_element_name_67;
        PyObject *tmp_source_name_25;
        PyObject *tmp_mvar_value_86;
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_82 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 180;

            goto frame_exception_exit_1;
        }

        tmp_called_name_78 = tmp_mvar_value_82;
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MILLIARD );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MILLIARD );
        }

        if ( tmp_mvar_value_83 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MILLIARD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_64 = tmp_mvar_value_83;
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MILLION );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MILLION );
        }

        if ( tmp_mvar_value_84 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MILLION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_65 = tmp_mvar_value_84;
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_THOUSAND );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_THOUSAND );
        }

        CHECK_OBJECT( tmp_mvar_value_85 );
        tmp_args_element_name_66 = tmp_mvar_value_85;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 180;
        {
            PyObject *call_args[] = { tmp_args_element_name_64, tmp_args_element_name_65, tmp_args_element_name_66 };
            tmp_source_name_24 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_78, call_args );
        }

        if ( tmp_source_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;

            goto frame_exception_exit_1;
        }
        tmp_called_name_77 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_24 );
        if ( tmp_called_name_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_86 == NULL )
        {
            Py_DECREF( tmp_called_name_77 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 185;

            goto frame_exception_exit_1;
        }

        tmp_source_name_25 = tmp_mvar_value_86;
        tmp_args_element_name_67 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_multiplier );
        if ( tmp_args_element_name_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_77 );

            exception_lineno = 185;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 180;
        {
            PyObject *call_args[] = { tmp_args_element_name_67 };
            tmp_assign_source_69 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_77, call_args );
        }

        Py_DECREF( tmp_called_name_77 );
        Py_DECREF( tmp_args_element_name_67 );
        if ( tmp_assign_source_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MULTIPLIER, tmp_assign_source_69 );
    }
    {
        PyObject *tmp_assign_source_70;
        PyObject *tmp_called_name_79;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_args_element_name_68;
        PyObject *tmp_called_name_80;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_args_element_name_69;
        PyObject *tmp_called_name_81;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_call_arg_element_7;
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_87 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }

        tmp_called_name_79 = tmp_mvar_value_87;
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_gram );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gram );
        }

        if ( tmp_mvar_value_88 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gram" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }

        tmp_called_name_80 = tmp_mvar_value_88;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 197;
        tmp_args_element_name_68 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_80, &PyTuple_GET_ITEM( const_tuple_str_plain_NUMR_tuple, 0 ) );

        if ( tmp_args_element_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_89 == NULL )
        {
            Py_DECREF( tmp_args_element_name_68 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }

        tmp_called_name_81 = tmp_mvar_value_89;
        tmp_call_arg_element_7 = PySet_New( const_set_c6fa3d51bbb77f078b7a7a8efe6e81fa );
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 199;
        {
            PyObject *call_args[] = { tmp_call_arg_element_7 };
            tmp_args_element_name_69 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_81, call_args );
        }

        Py_DECREF( tmp_call_arg_element_7 );
        if ( tmp_args_element_name_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_68 );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 196;
        {
            PyObject *call_args[] = { tmp_args_element_name_68, tmp_args_element_name_69 };
            tmp_assign_source_70 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_79, call_args );
        }

        Py_DECREF( tmp_args_element_name_68 );
        Py_DECREF( tmp_args_element_name_69 );
        if ( tmp_assign_source_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_NUMR, tmp_assign_source_70 );
    }
    {
        PyObject *tmp_assign_source_71;
        PyObject *tmp_called_name_82;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_call_arg_element_8;
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_in_caseless );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_caseless );
        }

        if ( tmp_mvar_value_90 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 205;

            goto frame_exception_exit_1;
        }

        tmp_called_name_82 = tmp_mvar_value_90;
        tmp_call_arg_element_8 = PySet_New( const_set_ae8070333b58fef640018259e0c8aae3 );
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 205;
        {
            PyObject *call_args[] = { tmp_call_arg_element_8 };
            tmp_assign_source_71 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_82, call_args );
        }

        Py_DECREF( tmp_call_arg_element_8 );
        if ( tmp_assign_source_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MODIFIER, tmp_assign_source_71 );
    }
    {
        PyObject *tmp_assign_source_72;
        PyObject *tmp_called_name_83;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_args_element_name_70;
        PyObject *tmp_called_name_84;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_args_element_name_71;
        PyObject *tmp_called_name_85;
        PyObject *tmp_mvar_value_93;
        PyObject *tmp_args_element_name_72;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_args_element_name_73;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_args_element_name_74;
        PyObject *tmp_mvar_value_96;
        PyObject *tmp_args_element_name_75;
        PyObject *tmp_mvar_value_97;
        PyObject *tmp_args_element_name_76;
        PyObject *tmp_mvar_value_98;
        PyObject *tmp_args_element_name_77;
        PyObject *tmp_mvar_value_99;
        PyObject *tmp_args_element_name_78;
        PyObject *tmp_mvar_value_100;
        PyObject *tmp_args_element_name_79;
        PyObject *tmp_mvar_value_101;
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_91 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 211;

            goto frame_exception_exit_1;
        }

        tmp_called_name_83 = tmp_mvar_value_91;
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_92 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 212;

            goto frame_exception_exit_1;
        }

        tmp_called_name_84 = tmp_mvar_value_92;
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_93 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 213;

            goto frame_exception_exit_1;
        }

        tmp_called_name_85 = tmp_mvar_value_93;
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_94 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 214;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_72 = tmp_mvar_value_94;
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_NUMR );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NUMR );
        }

        if ( tmp_mvar_value_95 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NUMR" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 215;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_73 = tmp_mvar_value_95;
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MODIFIER );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MODIFIER );
        }

        CHECK_OBJECT( tmp_mvar_value_96 );
        tmp_args_element_name_74 = tmp_mvar_value_96;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 213;
        {
            PyObject *call_args[] = { tmp_args_element_name_72, tmp_args_element_name_73, tmp_args_element_name_74 };
            tmp_args_element_name_71 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_85, call_args );
        }

        if ( tmp_args_element_name_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 212;
        {
            PyObject *call_args[] = { tmp_args_element_name_71 };
            tmp_args_element_name_70 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_84, call_args );
        }

        Py_DECREF( tmp_args_element_name_71 );
        if ( tmp_args_element_name_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MILLIARD );

        if (unlikely( tmp_mvar_value_97 == NULL ))
        {
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MILLIARD );
        }

        if ( tmp_mvar_value_97 == NULL )
        {
            Py_DECREF( tmp_args_element_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MILLIARD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 219;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_75 = tmp_mvar_value_97;
        tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MILLION );

        if (unlikely( tmp_mvar_value_98 == NULL ))
        {
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MILLION );
        }

        if ( tmp_mvar_value_98 == NULL )
        {
            Py_DECREF( tmp_args_element_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MILLION" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 220;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_76 = tmp_mvar_value_98;
        tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_THOUSAND );

        if (unlikely( tmp_mvar_value_99 == NULL ))
        {
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_THOUSAND );
        }

        if ( tmp_mvar_value_99 == NULL )
        {
            Py_DECREF( tmp_args_element_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "THOUSAND" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 221;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_77 = tmp_mvar_value_99;
        tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_CURRENCY );

        if (unlikely( tmp_mvar_value_100 == NULL ))
        {
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CURRENCY );
        }

        if ( tmp_mvar_value_100 == NULL )
        {
            Py_DECREF( tmp_args_element_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CURRENCY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 222;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_78 = tmp_mvar_value_100;
        tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_CURRENCY );

        if (unlikely( tmp_mvar_value_101 == NULL ))
        {
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COINS_CURRENCY );
        }

        if ( tmp_mvar_value_101 == NULL )
        {
            Py_DECREF( tmp_args_element_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COINS_CURRENCY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 223;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_79 = tmp_mvar_value_101;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 211;
        {
            PyObject *call_args[] = { tmp_args_element_name_70, tmp_args_element_name_75, tmp_args_element_name_76, tmp_args_element_name_77, tmp_args_element_name_78, tmp_args_element_name_79 };
            tmp_assign_source_72 = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_83, call_args );
        }

        Py_DECREF( tmp_args_element_name_70 );
        if ( tmp_assign_source_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART, tmp_assign_source_72 );
    }
    {
        PyObject *tmp_assign_source_73;
        PyObject *tmp_called_name_86;
        PyObject *tmp_mvar_value_102;
        tmp_mvar_value_102 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_102 == NULL ))
        {
            tmp_mvar_value_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_102 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 226;

            goto frame_exception_exit_1;
        }

        tmp_called_name_86 = tmp_mvar_value_102;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 226;
        tmp_assign_source_73 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_86, &PyTuple_GET_ITEM( const_tuple_str_digest_59a11a6e625897d10e074ffcd41643bf_tuple, 0 ) );

        if ( tmp_assign_source_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_BOUND, tmp_assign_source_73 );
    }
    {
        PyObject *tmp_assign_source_74;
        PyObject *tmp_called_name_87;
        PyObject *tmp_mvar_value_103;
        PyObject *tmp_args_element_name_80;
        PyObject *tmp_mvar_value_104;
        PyObject *tmp_args_element_name_81;
        PyObject *tmp_called_instance_6;
        PyObject *tmp_mvar_value_105;
        PyObject *tmp_args_element_name_82;
        PyObject *tmp_mvar_value_106;
        tmp_mvar_value_103 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_103 == NULL ))
        {
            tmp_mvar_value_103 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_103 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;

            goto frame_exception_exit_1;
        }

        tmp_called_name_87 = tmp_mvar_value_103;
        tmp_mvar_value_104 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_BOUND );

        if (unlikely( tmp_mvar_value_104 == NULL ))
        {
            tmp_mvar_value_104 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BOUND );
        }

        CHECK_OBJECT( tmp_mvar_value_104 );
        tmp_args_element_name_80 = tmp_mvar_value_104;
        tmp_mvar_value_105 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_105 == NULL ))
        {
            tmp_mvar_value_105 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_105 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 230;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_6 = tmp_mvar_value_105;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 230;
        tmp_args_element_name_81 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_repeatable );
        if ( tmp_args_element_name_81 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 230;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_106 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_BOUND );

        if (unlikely( tmp_mvar_value_106 == NULL ))
        {
            tmp_mvar_value_106 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BOUND );
        }

        if ( tmp_mvar_value_106 == NULL )
        {
            Py_DECREF( tmp_args_element_name_81 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BOUND" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 231;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_82 = tmp_mvar_value_106;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 228;
        {
            PyObject *call_args[] = { tmp_args_element_name_80, tmp_args_element_name_81, tmp_args_element_name_82 };
            tmp_assign_source_74 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_87, call_args );
        }

        Py_DECREF( tmp_args_element_name_81 );
        if ( tmp_assign_source_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_NUMERAL, tmp_assign_source_74 );
    }
    {
        PyObject *tmp_assign_source_75;
        tmp_assign_source_75 = MAKE_FUNCTION_grammars$money$$$function_4_normalize_integer(  );



        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalize_integer, tmp_assign_source_75 );
    }
    {
        PyObject *tmp_assign_source_76;
        tmp_assign_source_76 = MAKE_FUNCTION_grammars$money$$$function_5_normalize_fraction(  );



        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalize_fraction, tmp_assign_source_76 );
    }
    {
        PyObject *tmp_assign_source_77;
        PyObject *tmp_called_name_88;
        PyObject *tmp_mvar_value_107;
        PyObject *tmp_args_element_name_83;
        PyObject *tmp_mvar_value_108;
        PyObject *tmp_args_element_name_84;
        PyObject *tmp_called_name_89;
        PyObject *tmp_mvar_value_109;
        tmp_mvar_value_107 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_107 == NULL ))
        {
            tmp_mvar_value_107 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_107 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 252;

            goto frame_exception_exit_1;
        }

        tmp_called_name_88 = tmp_mvar_value_107;
        tmp_mvar_value_108 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_108 == NULL ))
        {
            tmp_mvar_value_108 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_108 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 253;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_83 = tmp_mvar_value_108;
        tmp_mvar_value_109 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_length_eq );

        if (unlikely( tmp_mvar_value_109 == NULL ))
        {
            tmp_mvar_value_109 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_length_eq );
        }

        if ( tmp_mvar_value_109 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "length_eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 254;

            goto frame_exception_exit_1;
        }

        tmp_called_name_89 = tmp_mvar_value_109;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 254;
        tmp_args_element_name_84 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_89, &PyTuple_GET_ITEM( const_tuple_int_pos_3_tuple, 0 ) );

        if ( tmp_args_element_name_84 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 252;
        {
            PyObject *call_args[] = { tmp_args_element_name_83, tmp_args_element_name_84 };
            tmp_assign_source_77 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_88, call_args );
        }

        Py_DECREF( tmp_args_element_name_84 );
        if ( tmp_assign_source_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART, tmp_assign_source_77 );
    }
    {
        PyObject *tmp_assign_source_78;
        PyObject *tmp_called_name_90;
        PyObject *tmp_mvar_value_110;
        tmp_mvar_value_110 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_in_ );

        if (unlikely( tmp_mvar_value_110 == NULL ))
        {
            tmp_mvar_value_110 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_ );
        }

        if ( tmp_mvar_value_110 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 257;

            goto frame_exception_exit_1;
        }

        tmp_called_name_90 = tmp_mvar_value_110;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 257;
        tmp_assign_source_78 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_90, &PyTuple_GET_ITEM( const_tuple_str_digest_e63c6a46700d15c5f24a1f3b0fd72300_tuple, 0 ) );

        if ( tmp_assign_source_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 257;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_SEP, tmp_assign_source_78 );
    }
    {
        PyObject *tmp_assign_source_79;
        PyObject *tmp_called_name_91;
        PyObject *tmp_source_name_26;
        PyObject *tmp_called_name_92;
        PyObject *tmp_mvar_value_111;
        PyObject *tmp_args_element_name_85;
        PyObject *tmp_called_name_93;
        PyObject *tmp_mvar_value_112;
        PyObject *tmp_args_element_name_86;
        PyObject *tmp_mvar_value_113;
        PyObject *tmp_args_element_name_87;
        PyObject *tmp_called_name_94;
        PyObject *tmp_mvar_value_114;
        PyObject *tmp_args_element_name_88;
        PyObject *tmp_mvar_value_115;
        PyObject *tmp_args_element_name_89;
        PyObject *tmp_mvar_value_116;
        PyObject *tmp_args_element_name_90;
        PyObject *tmp_called_name_95;
        PyObject *tmp_mvar_value_117;
        PyObject *tmp_args_element_name_91;
        PyObject *tmp_mvar_value_118;
        PyObject *tmp_args_element_name_92;
        PyObject *tmp_mvar_value_119;
        PyObject *tmp_args_element_name_93;
        PyObject *tmp_mvar_value_120;
        PyObject *tmp_args_element_name_94;
        PyObject *tmp_called_name_96;
        PyObject *tmp_mvar_value_121;
        PyObject *tmp_args_element_name_95;
        PyObject *tmp_mvar_value_122;
        PyObject *tmp_args_element_name_96;
        PyObject *tmp_mvar_value_123;
        PyObject *tmp_args_element_name_97;
        PyObject *tmp_mvar_value_124;
        PyObject *tmp_args_element_name_98;
        PyObject *tmp_called_name_97;
        PyObject *tmp_mvar_value_125;
        PyObject *tmp_args_element_name_99;
        PyObject *tmp_mvar_value_126;
        PyObject *tmp_args_element_name_100;
        PyObject *tmp_mvar_value_127;
        PyObject *tmp_args_element_name_101;
        PyObject *tmp_mvar_value_128;
        PyObject *tmp_args_element_name_102;
        PyObject *tmp_mvar_value_129;
        PyObject *tmp_args_element_name_103;
        PyObject *tmp_mvar_value_130;
        PyObject *tmp_args_element_name_104;
        PyObject *tmp_called_name_98;
        PyObject *tmp_source_name_27;
        PyObject *tmp_source_name_28;
        PyObject *tmp_mvar_value_131;
        PyObject *tmp_args_element_name_105;
        PyObject *tmp_mvar_value_132;
        tmp_mvar_value_111 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_111 == NULL ))
        {
            tmp_mvar_value_111 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_111 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 259;

            goto frame_exception_exit_1;
        }

        tmp_called_name_92 = tmp_mvar_value_111;
        tmp_mvar_value_112 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_112 == NULL ))
        {
            tmp_mvar_value_112 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_112 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 260;

            goto frame_exception_exit_1;
        }

        tmp_called_name_93 = tmp_mvar_value_112;
        tmp_mvar_value_113 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_113 == NULL ))
        {
            tmp_mvar_value_113 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_113 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 260;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_86 = tmp_mvar_value_113;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 260;
        {
            PyObject *call_args[] = { tmp_args_element_name_86 };
            tmp_args_element_name_85 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_93, call_args );
        }

        if ( tmp_args_element_name_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_114 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_114 == NULL ))
        {
            tmp_mvar_value_114 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_114 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 261;

            goto frame_exception_exit_1;
        }

        tmp_called_name_94 = tmp_mvar_value_114;
        tmp_mvar_value_115 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_115 == NULL ))
        {
            tmp_mvar_value_115 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_115 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 261;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_88 = tmp_mvar_value_115;
        tmp_mvar_value_116 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_116 == NULL ))
        {
            tmp_mvar_value_116 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_116 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 261;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_89 = tmp_mvar_value_116;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 261;
        {
            PyObject *call_args[] = { tmp_args_element_name_88, tmp_args_element_name_89 };
            tmp_args_element_name_87 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_94, call_args );
        }

        if ( tmp_args_element_name_87 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_85 );

            exception_lineno = 261;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_117 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_117 == NULL ))
        {
            tmp_mvar_value_117 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_117 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 262;

            goto frame_exception_exit_1;
        }

        tmp_called_name_95 = tmp_mvar_value_117;
        tmp_mvar_value_118 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_118 == NULL ))
        {
            tmp_mvar_value_118 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_118 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 262;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_91 = tmp_mvar_value_118;
        tmp_mvar_value_119 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_119 == NULL ))
        {
            tmp_mvar_value_119 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_119 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 262;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_92 = tmp_mvar_value_119;
        tmp_mvar_value_120 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_120 == NULL ))
        {
            tmp_mvar_value_120 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_120 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 262;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_93 = tmp_mvar_value_120;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 262;
        {
            PyObject *call_args[] = { tmp_args_element_name_91, tmp_args_element_name_92, tmp_args_element_name_93 };
            tmp_args_element_name_90 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_95, call_args );
        }

        if ( tmp_args_element_name_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );

            exception_lineno = 262;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_121 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_121 == NULL ))
        {
            tmp_mvar_value_121 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_121 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 263;

            goto frame_exception_exit_1;
        }

        tmp_called_name_96 = tmp_mvar_value_121;
        tmp_mvar_value_122 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_122 == NULL ))
        {
            tmp_mvar_value_122 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_122 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 263;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_95 = tmp_mvar_value_122;
        tmp_mvar_value_123 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_SEP );

        if (unlikely( tmp_mvar_value_123 == NULL ))
        {
            tmp_mvar_value_123 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SEP );
        }

        if ( tmp_mvar_value_123 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SEP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 263;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_96 = tmp_mvar_value_123;
        tmp_mvar_value_124 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_124 == NULL ))
        {
            tmp_mvar_value_124 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_124 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 263;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_97 = tmp_mvar_value_124;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 263;
        {
            PyObject *call_args[] = { tmp_args_element_name_95, tmp_args_element_name_96, tmp_args_element_name_97 };
            tmp_args_element_name_94 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_96, call_args );
        }

        if ( tmp_args_element_name_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );

            exception_lineno = 263;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_125 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_125 == NULL ))
        {
            tmp_mvar_value_125 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_125 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_called_name_97 = tmp_mvar_value_125;
        tmp_mvar_value_126 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_126 == NULL ))
        {
            tmp_mvar_value_126 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_126 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_99 = tmp_mvar_value_126;
        tmp_mvar_value_127 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_SEP );

        if (unlikely( tmp_mvar_value_127 == NULL ))
        {
            tmp_mvar_value_127 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SEP );
        }

        if ( tmp_mvar_value_127 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SEP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_100 = tmp_mvar_value_127;
        tmp_mvar_value_128 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_128 == NULL ))
        {
            tmp_mvar_value_128 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_128 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_101 = tmp_mvar_value_128;
        tmp_mvar_value_129 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_SEP );

        if (unlikely( tmp_mvar_value_129 == NULL ))
        {
            tmp_mvar_value_129 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SEP );
        }

        if ( tmp_mvar_value_129 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SEP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_102 = tmp_mvar_value_129;
        tmp_mvar_value_130 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PART );

        if (unlikely( tmp_mvar_value_130 == NULL ))
        {
            tmp_mvar_value_130 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PART );
        }

        if ( tmp_mvar_value_130 == NULL )
        {
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PART" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_103 = tmp_mvar_value_130;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 264;
        {
            PyObject *call_args[] = { tmp_args_element_name_99, tmp_args_element_name_100, tmp_args_element_name_101, tmp_args_element_name_102, tmp_args_element_name_103 };
            tmp_args_element_name_98 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_97, call_args );
        }

        if ( tmp_args_element_name_98 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_85 );
            Py_DECREF( tmp_args_element_name_87 );
            Py_DECREF( tmp_args_element_name_90 );
            Py_DECREF( tmp_args_element_name_94 );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 259;
        {
            PyObject *call_args[] = { tmp_args_element_name_85, tmp_args_element_name_87, tmp_args_element_name_90, tmp_args_element_name_94, tmp_args_element_name_98 };
            tmp_source_name_26 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_92, call_args );
        }

        Py_DECREF( tmp_args_element_name_85 );
        Py_DECREF( tmp_args_element_name_87 );
        Py_DECREF( tmp_args_element_name_90 );
        Py_DECREF( tmp_args_element_name_94 );
        Py_DECREF( tmp_args_element_name_98 );
        if ( tmp_source_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;

            goto frame_exception_exit_1;
        }
        tmp_called_name_91 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_26 );
        if ( tmp_called_name_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_131 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_131 == NULL ))
        {
            tmp_mvar_value_131 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_131 == NULL )
        {
            Py_DECREF( tmp_called_name_91 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }

        tmp_source_name_28 = tmp_mvar_value_131;
        tmp_source_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_integer );
        if ( tmp_source_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_91 );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }
        tmp_called_name_98 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_custom );
        Py_DECREF( tmp_source_name_27 );
        if ( tmp_called_name_98 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_91 );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_132 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalize_integer );

        if (unlikely( tmp_mvar_value_132 == NULL ))
        {
            tmp_mvar_value_132 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalize_integer );
        }

        if ( tmp_mvar_value_132 == NULL )
        {
            Py_DECREF( tmp_called_name_91 );
            Py_DECREF( tmp_called_name_98 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalize_integer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_105 = tmp_mvar_value_132;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 266;
        {
            PyObject *call_args[] = { tmp_args_element_name_105 };
            tmp_args_element_name_104 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_98, call_args );
        }

        Py_DECREF( tmp_called_name_98 );
        if ( tmp_args_element_name_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_91 );

            exception_lineno = 266;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 259;
        {
            PyObject *call_args[] = { tmp_args_element_name_104 };
            tmp_assign_source_79 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_91, call_args );
        }

        Py_DECREF( tmp_called_name_91 );
        Py_DECREF( tmp_args_element_name_104 );
        if ( tmp_assign_source_79 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INTEGER, tmp_assign_source_79 );
    }
    {
        PyObject *tmp_assign_source_80;
        PyObject *tmp_called_name_99;
        PyObject *tmp_source_name_29;
        PyObject *tmp_called_name_100;
        PyObject *tmp_mvar_value_133;
        PyObject *tmp_args_element_name_106;
        PyObject *tmp_mvar_value_134;
        PyObject *tmp_args_element_name_107;
        PyObject *tmp_called_name_101;
        PyObject *tmp_mvar_value_135;
        PyObject *tmp_args_element_name_108;
        PyObject *tmp_called_name_102;
        PyObject *tmp_mvar_value_136;
        PyObject *tmp_args_element_name_109;
        PyObject *tmp_called_name_103;
        PyObject *tmp_mvar_value_137;
        PyObject *tmp_args_element_name_110;
        PyObject *tmp_called_name_104;
        PyObject *tmp_source_name_30;
        PyObject *tmp_source_name_31;
        PyObject *tmp_mvar_value_138;
        PyObject *tmp_args_element_name_111;
        PyObject *tmp_mvar_value_139;
        tmp_mvar_value_133 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_133 == NULL ))
        {
            tmp_mvar_value_133 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_133 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 269;

            goto frame_exception_exit_1;
        }

        tmp_called_name_100 = tmp_mvar_value_133;
        tmp_mvar_value_134 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_134 == NULL ))
        {
            tmp_mvar_value_134 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_134 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 270;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_106 = tmp_mvar_value_134;
        tmp_mvar_value_135 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_135 == NULL ))
        {
            tmp_mvar_value_135 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_135 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;

            goto frame_exception_exit_1;
        }

        tmp_called_name_101 = tmp_mvar_value_135;
        tmp_mvar_value_136 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_length_eq );

        if (unlikely( tmp_mvar_value_136 == NULL ))
        {
            tmp_mvar_value_136 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_length_eq );
        }

        if ( tmp_mvar_value_136 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "length_eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 272;

            goto frame_exception_exit_1;
        }

        tmp_called_name_102 = tmp_mvar_value_136;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 272;
        tmp_args_element_name_108 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_102, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_args_element_name_108 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_137 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_length_eq );

        if (unlikely( tmp_mvar_value_137 == NULL ))
        {
            tmp_mvar_value_137 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_length_eq );
        }

        if ( tmp_mvar_value_137 == NULL )
        {
            Py_DECREF( tmp_args_element_name_108 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "length_eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 273;

            goto frame_exception_exit_1;
        }

        tmp_called_name_103 = tmp_mvar_value_137;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 273;
        tmp_args_element_name_109 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_103, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_args_element_name_109 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_108 );

            exception_lineno = 273;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 271;
        {
            PyObject *call_args[] = { tmp_args_element_name_108, tmp_args_element_name_109 };
            tmp_args_element_name_107 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_101, call_args );
        }

        Py_DECREF( tmp_args_element_name_108 );
        Py_DECREF( tmp_args_element_name_109 );
        if ( tmp_args_element_name_107 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 269;
        {
            PyObject *call_args[] = { tmp_args_element_name_106, tmp_args_element_name_107 };
            tmp_source_name_29 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_100, call_args );
        }

        Py_DECREF( tmp_args_element_name_107 );
        if ( tmp_source_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;

            goto frame_exception_exit_1;
        }
        tmp_called_name_99 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_29 );
        if ( tmp_called_name_99 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_138 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_138 == NULL ))
        {
            tmp_mvar_value_138 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_138 == NULL )
        {
            Py_DECREF( tmp_called_name_99 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }

        tmp_source_name_31 = tmp_mvar_value_138;
        tmp_source_name_30 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_fraction );
        if ( tmp_source_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_99 );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }
        tmp_called_name_104 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_custom );
        Py_DECREF( tmp_source_name_30 );
        if ( tmp_called_name_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_99 );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_139 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_normalize_fraction );

        if (unlikely( tmp_mvar_value_139 == NULL ))
        {
            tmp_mvar_value_139 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_normalize_fraction );
        }

        if ( tmp_mvar_value_139 == NULL )
        {
            Py_DECREF( tmp_called_name_99 );
            Py_DECREF( tmp_called_name_104 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "normalize_fraction" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_111 = tmp_mvar_value_139;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 276;
        {
            PyObject *call_args[] = { tmp_args_element_name_111 };
            tmp_args_element_name_110 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_104, call_args );
        }

        Py_DECREF( tmp_called_name_104 );
        if ( tmp_args_element_name_110 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_99 );

            exception_lineno = 276;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 269;
        {
            PyObject *call_args[] = { tmp_args_element_name_110 };
            tmp_assign_source_80 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_99, call_args );
        }

        Py_DECREF( tmp_called_name_99 );
        Py_DECREF( tmp_args_element_name_110 );
        if ( tmp_assign_source_80 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_FRACTION, tmp_assign_source_80 );
    }
    {
        PyObject *tmp_assign_source_81;
        PyObject *tmp_called_name_105;
        PyObject *tmp_mvar_value_140;
        PyObject *tmp_args_element_name_112;
        PyObject *tmp_mvar_value_141;
        PyObject *tmp_args_element_name_113;
        PyObject *tmp_called_instance_7;
        PyObject *tmp_called_name_106;
        PyObject *tmp_mvar_value_142;
        PyObject *tmp_args_element_name_114;
        PyObject *tmp_mvar_value_143;
        PyObject *tmp_args_element_name_115;
        PyObject *tmp_mvar_value_144;
        PyObject *tmp_args_element_name_116;
        PyObject *tmp_called_instance_8;
        PyObject *tmp_mvar_value_145;
        PyObject *tmp_args_element_name_117;
        PyObject *tmp_called_instance_9;
        PyObject *tmp_mvar_value_146;
        tmp_mvar_value_140 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_140 == NULL ))
        {
            tmp_mvar_value_140 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_140 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 279;

            goto frame_exception_exit_1;
        }

        tmp_called_name_105 = tmp_mvar_value_140;
        tmp_mvar_value_141 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INTEGER );

        if (unlikely( tmp_mvar_value_141 == NULL ))
        {
            tmp_mvar_value_141 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INTEGER );
        }

        if ( tmp_mvar_value_141 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INTEGER" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 280;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_112 = tmp_mvar_value_141;
        tmp_mvar_value_142 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_142 == NULL ))
        {
            tmp_mvar_value_142 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_142 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 281;

            goto frame_exception_exit_1;
        }

        tmp_called_name_106 = tmp_mvar_value_142;
        tmp_mvar_value_143 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_SEP );

        if (unlikely( tmp_mvar_value_143 == NULL ))
        {
            tmp_mvar_value_143 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SEP );
        }

        if ( tmp_mvar_value_143 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SEP" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 282;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_114 = tmp_mvar_value_143;
        tmp_mvar_value_144 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_FRACTION );

        if (unlikely( tmp_mvar_value_144 == NULL ))
        {
            tmp_mvar_value_144 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FRACTION );
        }

        CHECK_OBJECT( tmp_mvar_value_144 );
        tmp_args_element_name_115 = tmp_mvar_value_144;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 281;
        {
            PyObject *call_args[] = { tmp_args_element_name_114, tmp_args_element_name_115 };
            tmp_called_instance_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_106, call_args );
        }

        if ( tmp_called_instance_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 281;
        tmp_args_element_name_113 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_7 );
        if ( tmp_args_element_name_113 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_145 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MULTIPLIER );

        if (unlikely( tmp_mvar_value_145 == NULL ))
        {
            tmp_mvar_value_145 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MULTIPLIER );
        }

        if ( tmp_mvar_value_145 == NULL )
        {
            Py_DECREF( tmp_args_element_name_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MULTIPLIER" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 285;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_8 = tmp_mvar_value_145;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 285;
        tmp_args_element_name_116 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_optional );
        if ( tmp_args_element_name_116 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_113 );

            exception_lineno = 285;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_146 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_NUMERAL );

        if (unlikely( tmp_mvar_value_146 == NULL ))
        {
            tmp_mvar_value_146 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NUMERAL );
        }

        if ( tmp_mvar_value_146 == NULL )
        {
            Py_DECREF( tmp_args_element_name_113 );
            Py_DECREF( tmp_args_element_name_116 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NUMERAL" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 286;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_9 = tmp_mvar_value_146;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 286;
        tmp_args_element_name_117 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_optional );
        if ( tmp_args_element_name_117 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_113 );
            Py_DECREF( tmp_args_element_name_116 );

            exception_lineno = 286;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 279;
        {
            PyObject *call_args[] = { tmp_args_element_name_112, tmp_args_element_name_113, tmp_args_element_name_116, tmp_args_element_name_117 };
            tmp_assign_source_81 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_105, call_args );
        }

        Py_DECREF( tmp_args_element_name_113 );
        Py_DECREF( tmp_args_element_name_116 );
        Py_DECREF( tmp_args_element_name_117 );
        if ( tmp_assign_source_81 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 279;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_AMOUNT, tmp_assign_source_81 );
    }
    {
        PyObject *tmp_assign_source_82;
        PyObject *tmp_called_name_107;
        PyObject *tmp_source_name_32;
        PyObject *tmp_called_name_108;
        PyObject *tmp_mvar_value_147;
        PyObject *tmp_args_element_name_118;
        PyObject *tmp_mvar_value_148;
        PyObject *tmp_args_element_name_119;
        PyObject *tmp_called_name_109;
        PyObject *tmp_mvar_value_149;
        PyObject *tmp_args_element_name_120;
        PyObject *tmp_called_name_110;
        PyObject *tmp_mvar_value_150;
        PyObject *tmp_args_element_name_121;
        PyObject *tmp_called_name_111;
        PyObject *tmp_mvar_value_151;
        PyObject *tmp_args_element_name_122;
        PyObject *tmp_called_instance_10;
        PyObject *tmp_source_name_33;
        PyObject *tmp_mvar_value_152;
        tmp_mvar_value_147 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_and_ );

        if (unlikely( tmp_mvar_value_147 == NULL ))
        {
            tmp_mvar_value_147 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_and_ );
        }

        if ( tmp_mvar_value_147 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "and_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 289;

            goto frame_exception_exit_1;
        }

        tmp_called_name_108 = tmp_mvar_value_147;
        tmp_mvar_value_148 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_INT );

        if (unlikely( tmp_mvar_value_148 == NULL ))
        {
            tmp_mvar_value_148 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_INT );
        }

        if ( tmp_mvar_value_148 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "INT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 290;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_118 = tmp_mvar_value_148;
        tmp_mvar_value_149 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_149 == NULL ))
        {
            tmp_mvar_value_149 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_149 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 291;

            goto frame_exception_exit_1;
        }

        tmp_called_name_109 = tmp_mvar_value_149;
        tmp_mvar_value_150 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_length_eq );

        if (unlikely( tmp_mvar_value_150 == NULL ))
        {
            tmp_mvar_value_150 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_length_eq );
        }

        if ( tmp_mvar_value_150 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "length_eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 292;

            goto frame_exception_exit_1;
        }

        tmp_called_name_110 = tmp_mvar_value_150;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 292;
        tmp_args_element_name_120 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_110, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_args_element_name_120 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_151 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_length_eq );

        if (unlikely( tmp_mvar_value_151 == NULL ))
        {
            tmp_mvar_value_151 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_length_eq );
        }

        if ( tmp_mvar_value_151 == NULL )
        {
            Py_DECREF( tmp_args_element_name_120 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "length_eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 293;

            goto frame_exception_exit_1;
        }

        tmp_called_name_111 = tmp_mvar_value_151;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 293;
        tmp_args_element_name_121 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_111, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

        if ( tmp_args_element_name_121 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_120 );

            exception_lineno = 293;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 291;
        {
            PyObject *call_args[] = { tmp_args_element_name_120, tmp_args_element_name_121 };
            tmp_args_element_name_119 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_109, call_args );
        }

        Py_DECREF( tmp_args_element_name_120 );
        Py_DECREF( tmp_args_element_name_121 );
        if ( tmp_args_element_name_119 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 291;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 289;
        {
            PyObject *call_args[] = { tmp_args_element_name_118, tmp_args_element_name_119 };
            tmp_source_name_32 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_108, call_args );
        }

        Py_DECREF( tmp_args_element_name_119 );
        if ( tmp_source_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;

            goto frame_exception_exit_1;
        }
        tmp_called_name_107 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_32 );
        if ( tmp_called_name_107 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_152 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_152 == NULL ))
        {
            tmp_mvar_value_152 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_152 == NULL )
        {
            Py_DECREF( tmp_called_name_107 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 296;

            goto frame_exception_exit_1;
        }

        tmp_source_name_33 = tmp_mvar_value_152;
        tmp_called_instance_10 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_coins );
        if ( tmp_called_instance_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_107 );

            exception_lineno = 296;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 296;
        tmp_args_element_name_122 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_custom, &PyTuple_GET_ITEM( const_tuple_type_int_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_10 );
        if ( tmp_args_element_name_122 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_107 );

            exception_lineno = 296;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 289;
        {
            PyObject *call_args[] = { tmp_args_element_name_122 };
            tmp_assign_source_82 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_107, call_args );
        }

        Py_DECREF( tmp_called_name_107 );
        Py_DECREF( tmp_args_element_name_122 );
        if ( tmp_assign_source_82 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 289;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_INTEGER, tmp_assign_source_82 );
    }
    {
        PyObject *tmp_assign_source_83;
        PyObject *tmp_called_name_112;
        PyObject *tmp_mvar_value_153;
        PyObject *tmp_args_element_name_123;
        PyObject *tmp_mvar_value_154;
        PyObject *tmp_args_element_name_124;
        PyObject *tmp_called_instance_11;
        PyObject *tmp_mvar_value_155;
        tmp_mvar_value_153 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_153 == NULL ))
        {
            tmp_mvar_value_153 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_153 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 299;

            goto frame_exception_exit_1;
        }

        tmp_called_name_112 = tmp_mvar_value_153;
        tmp_mvar_value_154 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_INTEGER );

        if (unlikely( tmp_mvar_value_154 == NULL ))
        {
            tmp_mvar_value_154 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COINS_INTEGER );
        }

        CHECK_OBJECT( tmp_mvar_value_154 );
        tmp_args_element_name_123 = tmp_mvar_value_154;
        tmp_mvar_value_155 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_NUMERAL );

        if (unlikely( tmp_mvar_value_155 == NULL ))
        {
            tmp_mvar_value_155 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NUMERAL );
        }

        if ( tmp_mvar_value_155 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NUMERAL" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 301;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_11 = tmp_mvar_value_155;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 301;
        tmp_args_element_name_124 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_optional );
        if ( tmp_args_element_name_124 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 301;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 299;
        {
            PyObject *call_args[] = { tmp_args_element_name_123, tmp_args_element_name_124 };
            tmp_assign_source_83 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_112, call_args );
        }

        Py_DECREF( tmp_args_element_name_124 );
        if ( tmp_assign_source_83 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_AMOUNT, tmp_assign_source_83 );
    }
    {
        PyObject *tmp_assign_source_84;
        PyObject *tmp_called_name_113;
        PyObject *tmp_source_name_34;
        PyObject *tmp_called_name_114;
        PyObject *tmp_mvar_value_156;
        PyObject *tmp_args_element_name_125;
        PyObject *tmp_mvar_value_157;
        PyObject *tmp_args_element_name_126;
        PyObject *tmp_mvar_value_158;
        PyObject *tmp_args_element_name_127;
        PyObject *tmp_called_instance_12;
        PyObject *tmp_mvar_value_159;
        PyObject *tmp_args_element_name_128;
        PyObject *tmp_called_instance_13;
        PyObject *tmp_mvar_value_160;
        PyObject *tmp_args_element_name_129;
        PyObject *tmp_mvar_value_161;
        tmp_mvar_value_156 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_156 == NULL ))
        {
            tmp_mvar_value_156 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_156 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 312;

            goto frame_exception_exit_1;
        }

        tmp_called_name_114 = tmp_mvar_value_156;
        tmp_mvar_value_157 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_AMOUNT );

        if (unlikely( tmp_mvar_value_157 == NULL ))
        {
            tmp_mvar_value_157 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AMOUNT );
        }

        if ( tmp_mvar_value_157 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AMOUNT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 313;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_125 = tmp_mvar_value_157;
        tmp_mvar_value_158 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_CURRENCY );

        if (unlikely( tmp_mvar_value_158 == NULL ))
        {
            tmp_mvar_value_158 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CURRENCY );
        }

        if ( tmp_mvar_value_158 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CURRENCY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 314;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_126 = tmp_mvar_value_158;
        tmp_mvar_value_159 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_AMOUNT );

        if (unlikely( tmp_mvar_value_159 == NULL ))
        {
            tmp_mvar_value_159 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COINS_AMOUNT );
        }

        CHECK_OBJECT( tmp_mvar_value_159 );
        tmp_called_instance_12 = tmp_mvar_value_159;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 315;
        tmp_args_element_name_127 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_optional );
        if ( tmp_args_element_name_127 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 315;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_160 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_COINS_CURRENCY );

        if (unlikely( tmp_mvar_value_160 == NULL ))
        {
            tmp_mvar_value_160 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COINS_CURRENCY );
        }

        if ( tmp_mvar_value_160 == NULL )
        {
            Py_DECREF( tmp_args_element_name_127 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COINS_CURRENCY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 316;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_13 = tmp_mvar_value_160;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 316;
        tmp_args_element_name_128 = CALL_METHOD_NO_ARGS( tmp_called_instance_13, const_str_plain_optional );
        if ( tmp_args_element_name_128 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_127 );

            exception_lineno = 316;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 312;
        {
            PyObject *call_args[] = { tmp_args_element_name_125, tmp_args_element_name_126, tmp_args_element_name_127, tmp_args_element_name_128 };
            tmp_source_name_34 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_114, call_args );
        }

        Py_DECREF( tmp_args_element_name_127 );
        Py_DECREF( tmp_args_element_name_128 );
        if ( tmp_source_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;

            goto frame_exception_exit_1;
        }
        tmp_called_name_113 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_34 );
        if ( tmp_called_name_113 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_161 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_161 == NULL ))
        {
            tmp_mvar_value_161 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_161 == NULL )
        {
            Py_DECREF( tmp_called_name_113 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 318;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_129 = tmp_mvar_value_161;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 312;
        {
            PyObject *call_args[] = { tmp_args_element_name_129 };
            tmp_assign_source_84 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_113, call_args );
        }

        Py_DECREF( tmp_called_name_113 );
        if ( tmp_assign_source_84 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MONEY, tmp_assign_source_84 );
    }
    {
        PyObject *tmp_assign_source_85;
        PyObject *tmp_called_name_115;
        PyObject *tmp_source_name_35;
        PyObject *tmp_mvar_value_162;
        PyObject *tmp_args_element_name_130;
        PyObject *tmp_source_name_36;
        PyObject *tmp_mvar_value_163;
        tmp_mvar_value_162 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_MONEY );

        if (unlikely( tmp_mvar_value_162 == NULL ))
        {
            tmp_mvar_value_162 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MONEY );
        }

        CHECK_OBJECT( tmp_mvar_value_162 );
        tmp_source_name_35 = tmp_mvar_value_162;
        tmp_called_name_115 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_interpretation );
        if ( tmp_called_name_115 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_163 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Rate );

        if (unlikely( tmp_mvar_value_163 == NULL ))
        {
            tmp_mvar_value_163 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rate );
        }

        if ( tmp_mvar_value_163 == NULL )
        {
            Py_DECREF( tmp_called_name_115 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Rate" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 330;

            goto frame_exception_exit_1;
        }

        tmp_source_name_36 = tmp_mvar_value_163;
        tmp_args_element_name_130 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_money );
        if ( tmp_args_element_name_130 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_115 );

            exception_lineno = 330;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 329;
        {
            PyObject *call_args[] = { tmp_args_element_name_130 };
            tmp_assign_source_85 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_115, call_args );
        }

        Py_DECREF( tmp_called_name_115 );
        Py_DECREF( tmp_args_element_name_130 );
        if ( tmp_assign_source_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 329;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RATE_MONEY, tmp_assign_source_85 );
    }
    {
        PyObject *tmp_assign_source_86;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_37;
        PyObject *tmp_mvar_value_164;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_38;
        PyObject *tmp_mvar_value_165;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_39;
        PyObject *tmp_mvar_value_166;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_source_name_40;
        PyObject *tmp_mvar_value_167;
        tmp_dict_key_1 = const_str_digest_9f6c7a124d818d2a6edb7fc7c29d68ef;
        tmp_mvar_value_164 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_164 == NULL ))
        {
            tmp_mvar_value_164 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_164 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 334;

            goto frame_exception_exit_1;
        }

        tmp_source_name_37 = tmp_mvar_value_164;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_DAY );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_86 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_assign_source_86, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_digest_b742afac9704728799839c36673c41ca;
        tmp_mvar_value_165 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_165 == NULL ))
        {
            tmp_mvar_value_165 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_165 == NULL )
        {
            Py_DECREF( tmp_assign_source_86 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 335;

            goto frame_exception_exit_1;
        }

        tmp_source_name_38 = tmp_mvar_value_165;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_DAY );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_86 );

            exception_lineno = 335;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_86, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_digest_8d5784bbf3384a0fb3d7fb523bcc9ee7;
        tmp_mvar_value_166 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_166 == NULL ))
        {
            tmp_mvar_value_166 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_166 == NULL )
        {
            Py_DECREF( tmp_assign_source_86 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 336;

            goto frame_exception_exit_1;
        }

        tmp_source_name_39 = tmp_mvar_value_166;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_HOUR );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_86 );

            exception_lineno = 336;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_86, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_digest_3f64ef963bd11fa8f2123823d705c4fe;
        tmp_mvar_value_167 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dsl );

        if (unlikely( tmp_mvar_value_167 == NULL ))
        {
            tmp_mvar_value_167 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dsl );
        }

        if ( tmp_mvar_value_167 == NULL )
        {
            Py_DECREF( tmp_assign_source_86 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dsl" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 337;

            goto frame_exception_exit_1;
        }

        tmp_source_name_40 = tmp_mvar_value_167;
        tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_SHIFT );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_86 );

            exception_lineno = 337;

            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_assign_source_86, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PERIODS, tmp_assign_source_86 );
    }
    {
        PyObject *tmp_assign_source_87;
        PyObject *tmp_called_name_116;
        PyObject *tmp_source_name_41;
        PyObject *tmp_called_name_117;
        PyObject *tmp_mvar_value_168;
        PyObject *tmp_args_element_name_131;
        PyObject *tmp_mvar_value_169;
        PyObject *tmp_args_element_name_132;
        PyObject *tmp_called_name_118;
        PyObject *tmp_source_name_42;
        PyObject *tmp_called_instance_14;
        PyObject *tmp_source_name_43;
        PyObject *tmp_mvar_value_170;
        PyObject *tmp_args_element_name_133;
        PyObject *tmp_source_name_44;
        PyObject *tmp_mvar_value_171;
        tmp_mvar_value_168 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_dictionary );

        if (unlikely( tmp_mvar_value_168 == NULL ))
        {
            tmp_mvar_value_168 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dictionary );
        }

        if ( tmp_mvar_value_168 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dictionary" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 340;

            goto frame_exception_exit_1;
        }

        tmp_called_name_117 = tmp_mvar_value_168;
        tmp_mvar_value_169 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PERIODS );

        if (unlikely( tmp_mvar_value_169 == NULL ))
        {
            tmp_mvar_value_169 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PERIODS );
        }

        CHECK_OBJECT( tmp_mvar_value_169 );
        tmp_args_element_name_131 = tmp_mvar_value_169;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 340;
        {
            PyObject *call_args[] = { tmp_args_element_name_131 };
            tmp_source_name_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_117, call_args );
        }

        if ( tmp_source_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;

            goto frame_exception_exit_1;
        }
        tmp_called_name_116 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_41 );
        if ( tmp_called_name_116 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_170 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Rate );

        if (unlikely( tmp_mvar_value_170 == NULL ))
        {
            tmp_mvar_value_170 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rate );
        }

        if ( tmp_mvar_value_170 == NULL )
        {
            Py_DECREF( tmp_called_name_116 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Rate" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }

        tmp_source_name_43 = tmp_mvar_value_170;
        tmp_called_instance_14 = LOOKUP_ATTRIBUTE( tmp_source_name_43, const_str_plain_period );
        if ( tmp_called_instance_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_116 );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 343;
        tmp_source_name_42 = CALL_METHOD_NO_ARGS( tmp_called_instance_14, const_str_plain_normalized );
        Py_DECREF( tmp_called_instance_14 );
        if ( tmp_source_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_116 );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }
        tmp_called_name_118 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_custom );
        Py_DECREF( tmp_source_name_42 );
        if ( tmp_called_name_118 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_116 );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_171 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PERIODS );

        if (unlikely( tmp_mvar_value_171 == NULL ))
        {
            tmp_mvar_value_171 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PERIODS );
        }

        if ( tmp_mvar_value_171 == NULL )
        {
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PERIODS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }

        tmp_source_name_44 = tmp_mvar_value_171;
        tmp_args_element_name_133 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain___getitem__ );
        if ( tmp_args_element_name_133 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 343;
        {
            PyObject *call_args[] = { tmp_args_element_name_133 };
            tmp_args_element_name_132 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_118, call_args );
        }

        Py_DECREF( tmp_called_name_118 );
        Py_DECREF( tmp_args_element_name_133 );
        if ( tmp_args_element_name_132 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_116 );

            exception_lineno = 343;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 340;
        {
            PyObject *call_args[] = { tmp_args_element_name_132 };
            tmp_assign_source_87 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_116, call_args );
        }

        Py_DECREF( tmp_called_name_116 );
        Py_DECREF( tmp_args_element_name_132 );
        if ( tmp_assign_source_87 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PERIOD, tmp_assign_source_87 );
    }
    {
        PyObject *tmp_assign_source_88;
        PyObject *tmp_called_name_119;
        PyObject *tmp_mvar_value_172;
        PyObject *tmp_args_element_name_134;
        PyObject *tmp_called_name_120;
        PyObject *tmp_mvar_value_173;
        PyObject *tmp_args_element_name_135;
        PyObject *tmp_called_name_121;
        PyObject *tmp_mvar_value_174;
        PyObject *tmp_call_arg_element_9;
        tmp_mvar_value_172 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_or_ );

        if (unlikely( tmp_mvar_value_172 == NULL ))
        {
            tmp_mvar_value_172 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_or_ );
        }

        if ( tmp_mvar_value_172 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "or_" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 346;

            goto frame_exception_exit_1;
        }

        tmp_called_name_119 = tmp_mvar_value_172;
        tmp_mvar_value_173 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_173 == NULL ))
        {
            tmp_mvar_value_173 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_173 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 347;

            goto frame_exception_exit_1;
        }

        tmp_called_name_120 = tmp_mvar_value_173;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 347;
        tmp_args_element_name_134 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_120, &PyTuple_GET_ITEM( const_tuple_str_chr_47_tuple, 0 ) );

        if ( tmp_args_element_name_134 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 347;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_174 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_in_caseless );

        if (unlikely( tmp_mvar_value_174 == NULL ))
        {
            tmp_mvar_value_174 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_caseless );
        }

        if ( tmp_mvar_value_174 == NULL )
        {
            Py_DECREF( tmp_args_element_name_134 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_caseless" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 348;

            goto frame_exception_exit_1;
        }

        tmp_called_name_121 = tmp_mvar_value_174;
        tmp_call_arg_element_9 = PySet_New( const_set_50604704cd9ba591707baff6a8d78c02 );
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 348;
        {
            PyObject *call_args[] = { tmp_call_arg_element_9 };
            tmp_args_element_name_135 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_121, call_args );
        }

        Py_DECREF( tmp_call_arg_element_9 );
        if ( tmp_args_element_name_135 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_134 );

            exception_lineno = 348;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 346;
        {
            PyObject *call_args[] = { tmp_args_element_name_134, tmp_args_element_name_135 };
            tmp_assign_source_88 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_119, call_args );
        }

        Py_DECREF( tmp_args_element_name_134 );
        Py_DECREF( tmp_args_element_name_135 );
        if ( tmp_assign_source_88 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 346;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PER, tmp_assign_source_88 );
    }
    {
        PyObject *tmp_assign_source_89;
        PyObject *tmp_called_name_122;
        PyObject *tmp_source_name_45;
        PyObject *tmp_called_name_123;
        PyObject *tmp_mvar_value_175;
        PyObject *tmp_args_element_name_136;
        PyObject *tmp_mvar_value_176;
        PyObject *tmp_args_element_name_137;
        PyObject *tmp_mvar_value_177;
        PyObject *tmp_args_element_name_138;
        PyObject *tmp_mvar_value_178;
        PyObject *tmp_args_element_name_139;
        PyObject *tmp_mvar_value_179;
        tmp_mvar_value_175 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_175 == NULL ))
        {
            tmp_mvar_value_175 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_175 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 351;

            goto frame_exception_exit_1;
        }

        tmp_called_name_123 = tmp_mvar_value_175;
        tmp_mvar_value_176 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RATE_MONEY );

        if (unlikely( tmp_mvar_value_176 == NULL ))
        {
            tmp_mvar_value_176 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATE_MONEY );
        }

        if ( tmp_mvar_value_176 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RATE_MONEY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 352;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_136 = tmp_mvar_value_176;
        tmp_mvar_value_177 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PER );

        if (unlikely( tmp_mvar_value_177 == NULL ))
        {
            tmp_mvar_value_177 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PER );
        }

        CHECK_OBJECT( tmp_mvar_value_177 );
        tmp_args_element_name_137 = tmp_mvar_value_177;
        tmp_mvar_value_178 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_PERIOD );

        if (unlikely( tmp_mvar_value_178 == NULL ))
        {
            tmp_mvar_value_178 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PERIOD );
        }

        if ( tmp_mvar_value_178 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PERIOD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 354;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_138 = tmp_mvar_value_178;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 351;
        {
            PyObject *call_args[] = { tmp_args_element_name_136, tmp_args_element_name_137, tmp_args_element_name_138 };
            tmp_source_name_45 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_123, call_args );
        }

        if ( tmp_source_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 351;

            goto frame_exception_exit_1;
        }
        tmp_called_name_122 = LOOKUP_ATTRIBUTE( tmp_source_name_45, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_45 );
        if ( tmp_called_name_122 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 351;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_179 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Rate );

        if (unlikely( tmp_mvar_value_179 == NULL ))
        {
            tmp_mvar_value_179 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rate );
        }

        if ( tmp_mvar_value_179 == NULL )
        {
            Py_DECREF( tmp_called_name_122 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Rate" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 356;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_139 = tmp_mvar_value_179;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 351;
        {
            PyObject *call_args[] = { tmp_args_element_name_139 };
            tmp_assign_source_89 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_122, call_args );
        }

        Py_DECREF( tmp_called_name_122 );
        if ( tmp_assign_source_89 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 351;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RATE, tmp_assign_source_89 );
    }
    {
        PyObject *tmp_assign_source_90;
        PyObject *tmp_called_name_124;
        PyObject *tmp_mvar_value_180;
        tmp_mvar_value_180 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_180 == NULL ))
        {
            tmp_mvar_value_180 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_180 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 367;

            goto frame_exception_exit_1;
        }

        tmp_called_name_124 = tmp_mvar_value_180;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 367;
        tmp_assign_source_90 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_124, &PyTuple_GET_ITEM( const_tuple_str_chr_45_tuple, 0 ) );

        if ( tmp_assign_source_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 367;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DASH, tmp_assign_source_90 );
    }
    {
        PyObject *tmp_assign_source_91;
        PyObject *tmp_called_name_125;
        PyObject *tmp_source_name_46;
        PyObject *tmp_called_name_126;
        PyObject *tmp_mvar_value_181;
        PyObject *tmp_args_element_name_140;
        PyObject *tmp_mvar_value_182;
        PyObject *tmp_args_element_name_141;
        PyObject *tmp_called_instance_15;
        PyObject *tmp_mvar_value_183;
        PyObject *tmp_args_element_name_142;
        PyObject *tmp_mvar_value_184;
        tmp_mvar_value_181 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_181 == NULL ))
        {
            tmp_mvar_value_181 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_181 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 369;

            goto frame_exception_exit_1;
        }

        tmp_called_name_126 = tmp_mvar_value_181;
        tmp_mvar_value_182 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_AMOUNT );

        if (unlikely( tmp_mvar_value_182 == NULL ))
        {
            tmp_mvar_value_182 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AMOUNT );
        }

        if ( tmp_mvar_value_182 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AMOUNT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 370;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_140 = tmp_mvar_value_182;
        tmp_mvar_value_183 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_CURRENCY );

        if (unlikely( tmp_mvar_value_183 == NULL ))
        {
            tmp_mvar_value_183 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CURRENCY );
        }

        if ( tmp_mvar_value_183 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CURRENCY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 371;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_15 = tmp_mvar_value_183;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 371;
        tmp_args_element_name_141 = CALL_METHOD_NO_ARGS( tmp_called_instance_15, const_str_plain_optional );
        if ( tmp_args_element_name_141 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 369;
        {
            PyObject *call_args[] = { tmp_args_element_name_140, tmp_args_element_name_141 };
            tmp_source_name_46 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_126, call_args );
        }

        Py_DECREF( tmp_args_element_name_141 );
        if ( tmp_source_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;

            goto frame_exception_exit_1;
        }
        tmp_called_name_125 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_46 );
        if ( tmp_called_name_125 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_184 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Money );

        if (unlikely( tmp_mvar_value_184 == NULL ))
        {
            tmp_mvar_value_184 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Money );
        }

        if ( tmp_mvar_value_184 == NULL )
        {
            Py_DECREF( tmp_called_name_125 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Money" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 373;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_142 = tmp_mvar_value_184;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 369;
        {
            PyObject *call_args[] = { tmp_args_element_name_142 };
            tmp_assign_source_91 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_125, call_args );
        }

        Py_DECREF( tmp_called_name_125 );
        if ( tmp_assign_source_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MONEY, tmp_assign_source_91 );
    }
    {
        PyObject *tmp_assign_source_92;
        PyObject *tmp_called_name_127;
        PyObject *tmp_mvar_value_185;
        PyObject *tmp_args_element_name_143;
        PyObject *tmp_called_instance_16;
        PyObject *tmp_called_name_128;
        PyObject *tmp_mvar_value_186;
        PyObject *tmp_args_element_name_144;
        PyObject *tmp_called_name_129;
        PyObject *tmp_source_name_47;
        PyObject *tmp_mvar_value_187;
        PyObject *tmp_args_element_name_145;
        PyObject *tmp_source_name_48;
        PyObject *tmp_mvar_value_188;
        tmp_mvar_value_185 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_185 == NULL ))
        {
            tmp_mvar_value_185 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_185 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 376;

            goto frame_exception_exit_1;
        }

        tmp_called_name_127 = tmp_mvar_value_185;
        tmp_mvar_value_186 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_186 == NULL ))
        {
            tmp_mvar_value_186 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_186 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 377;

            goto frame_exception_exit_1;
        }

        tmp_called_name_128 = tmp_mvar_value_186;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 377;
        tmp_called_instance_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_128, &PyTuple_GET_ITEM( const_tuple_str_digest_0005f1018b913d59f0698ba7ad8e7254_tuple, 0 ) );

        if ( tmp_called_instance_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 377;
        tmp_args_element_name_143 = CALL_METHOD_NO_ARGS( tmp_called_instance_16, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_16 );
        if ( tmp_args_element_name_143 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_187 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MONEY );

        if (unlikely( tmp_mvar_value_187 == NULL ))
        {
            tmp_mvar_value_187 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RANGE_MONEY );
        }

        if ( tmp_mvar_value_187 == NULL )
        {
            Py_DECREF( tmp_args_element_name_143 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RANGE_MONEY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 378;

            goto frame_exception_exit_1;
        }

        tmp_source_name_47 = tmp_mvar_value_187;
        tmp_called_name_129 = LOOKUP_ATTRIBUTE( tmp_source_name_47, const_str_plain_interpretation );
        if ( tmp_called_name_129 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_143 );

            exception_lineno = 378;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_188 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Range );

        if (unlikely( tmp_mvar_value_188 == NULL ))
        {
            tmp_mvar_value_188 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Range );
        }

        if ( tmp_mvar_value_188 == NULL )
        {
            Py_DECREF( tmp_args_element_name_143 );
            Py_DECREF( tmp_called_name_129 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Range" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 379;

            goto frame_exception_exit_1;
        }

        tmp_source_name_48 = tmp_mvar_value_188;
        tmp_args_element_name_145 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain_min );
        if ( tmp_args_element_name_145 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_143 );
            Py_DECREF( tmp_called_name_129 );

            exception_lineno = 379;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 378;
        {
            PyObject *call_args[] = { tmp_args_element_name_145 };
            tmp_args_element_name_144 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_129, call_args );
        }

        Py_DECREF( tmp_called_name_129 );
        Py_DECREF( tmp_args_element_name_145 );
        if ( tmp_args_element_name_144 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_143 );

            exception_lineno = 378;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 376;
        {
            PyObject *call_args[] = { tmp_args_element_name_143, tmp_args_element_name_144 };
            tmp_assign_source_92 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_127, call_args );
        }

        Py_DECREF( tmp_args_element_name_143 );
        Py_DECREF( tmp_args_element_name_144 );
        if ( tmp_assign_source_92 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 376;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MIN, tmp_assign_source_92 );
    }
    {
        PyObject *tmp_assign_source_93;
        PyObject *tmp_called_name_130;
        PyObject *tmp_mvar_value_189;
        PyObject *tmp_args_element_name_146;
        PyObject *tmp_called_instance_17;
        PyObject *tmp_called_name_131;
        PyObject *tmp_mvar_value_190;
        PyObject *tmp_args_element_name_147;
        PyObject *tmp_called_name_132;
        PyObject *tmp_source_name_49;
        PyObject *tmp_mvar_value_191;
        PyObject *tmp_args_element_name_148;
        PyObject *tmp_source_name_50;
        PyObject *tmp_mvar_value_192;
        tmp_mvar_value_189 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_189 == NULL ))
        {
            tmp_mvar_value_189 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_189 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 383;

            goto frame_exception_exit_1;
        }

        tmp_called_name_130 = tmp_mvar_value_189;
        tmp_mvar_value_190 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_eq );

        if (unlikely( tmp_mvar_value_190 == NULL ))
        {
            tmp_mvar_value_190 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eq );
        }

        if ( tmp_mvar_value_190 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 384;

            goto frame_exception_exit_1;
        }

        tmp_called_name_131 = tmp_mvar_value_190;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 384;
        tmp_called_instance_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_131, &PyTuple_GET_ITEM( const_tuple_str_digest_947f7a6ac444bb1ddd86a5723bc9579c_tuple, 0 ) );

        if ( tmp_called_instance_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 384;
        tmp_args_element_name_146 = CALL_METHOD_NO_ARGS( tmp_called_instance_17, const_str_plain_optional );
        Py_DECREF( tmp_called_instance_17 );
        if ( tmp_args_element_name_146 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 384;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_191 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MONEY );

        if (unlikely( tmp_mvar_value_191 == NULL ))
        {
            tmp_mvar_value_191 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RANGE_MONEY );
        }

        if ( tmp_mvar_value_191 == NULL )
        {
            Py_DECREF( tmp_args_element_name_146 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RANGE_MONEY" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 385;

            goto frame_exception_exit_1;
        }

        tmp_source_name_49 = tmp_mvar_value_191;
        tmp_called_name_132 = LOOKUP_ATTRIBUTE( tmp_source_name_49, const_str_plain_interpretation );
        if ( tmp_called_name_132 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_146 );

            exception_lineno = 385;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_192 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Range );

        if (unlikely( tmp_mvar_value_192 == NULL ))
        {
            tmp_mvar_value_192 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Range );
        }

        if ( tmp_mvar_value_192 == NULL )
        {
            Py_DECREF( tmp_args_element_name_146 );
            Py_DECREF( tmp_called_name_132 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Range" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 386;

            goto frame_exception_exit_1;
        }

        tmp_source_name_50 = tmp_mvar_value_192;
        tmp_args_element_name_148 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain_max );
        if ( tmp_args_element_name_148 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_146 );
            Py_DECREF( tmp_called_name_132 );

            exception_lineno = 386;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 385;
        {
            PyObject *call_args[] = { tmp_args_element_name_148 };
            tmp_args_element_name_147 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_132, call_args );
        }

        Py_DECREF( tmp_called_name_132 );
        Py_DECREF( tmp_args_element_name_148 );
        if ( tmp_args_element_name_147 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_146 );

            exception_lineno = 385;

            goto frame_exception_exit_1;
        }
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 383;
        {
            PyObject *call_args[] = { tmp_args_element_name_146, tmp_args_element_name_147 };
            tmp_assign_source_93 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_130, call_args );
        }

        Py_DECREF( tmp_args_element_name_146 );
        Py_DECREF( tmp_args_element_name_147 );
        if ( tmp_assign_source_93 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 383;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MAX, tmp_assign_source_93 );
    }
    {
        PyObject *tmp_assign_source_94;
        PyObject *tmp_called_name_133;
        PyObject *tmp_source_name_51;
        PyObject *tmp_called_name_134;
        PyObject *tmp_mvar_value_193;
        PyObject *tmp_args_element_name_149;
        PyObject *tmp_mvar_value_194;
        PyObject *tmp_args_element_name_150;
        PyObject *tmp_called_instance_18;
        PyObject *tmp_mvar_value_195;
        PyObject *tmp_args_element_name_151;
        PyObject *tmp_mvar_value_196;
        PyObject *tmp_args_element_name_152;
        PyObject *tmp_mvar_value_197;
        tmp_mvar_value_193 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_rule );

        if (unlikely( tmp_mvar_value_193 == NULL ))
        {
            tmp_mvar_value_193 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rule );
        }

        if ( tmp_mvar_value_193 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "rule" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 390;

            goto frame_exception_exit_1;
        }

        tmp_called_name_134 = tmp_mvar_value_193;
        tmp_mvar_value_194 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MIN );

        if (unlikely( tmp_mvar_value_194 == NULL ))
        {
            tmp_mvar_value_194 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RANGE_MIN );
        }

        if ( tmp_mvar_value_194 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RANGE_MIN" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 391;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_149 = tmp_mvar_value_194;
        tmp_mvar_value_195 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_DASH );

        if (unlikely( tmp_mvar_value_195 == NULL ))
        {
            tmp_mvar_value_195 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DASH );
        }

        if ( tmp_mvar_value_195 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DASH" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 392;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_18 = tmp_mvar_value_195;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 392;
        tmp_args_element_name_150 = CALL_METHOD_NO_ARGS( tmp_called_instance_18, const_str_plain_optional );
        if ( tmp_args_element_name_150 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 392;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_196 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE_MAX );

        if (unlikely( tmp_mvar_value_196 == NULL ))
        {
            tmp_mvar_value_196 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RANGE_MAX );
        }

        if ( tmp_mvar_value_196 == NULL )
        {
            Py_DECREF( tmp_args_element_name_150 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RANGE_MAX" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 393;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_151 = tmp_mvar_value_196;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 390;
        {
            PyObject *call_args[] = { tmp_args_element_name_149, tmp_args_element_name_150, tmp_args_element_name_151 };
            tmp_source_name_51 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_134, call_args );
        }

        Py_DECREF( tmp_args_element_name_150 );
        if ( tmp_source_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;

            goto frame_exception_exit_1;
        }
        tmp_called_name_133 = LOOKUP_ATTRIBUTE( tmp_source_name_51, const_str_plain_interpretation );
        Py_DECREF( tmp_source_name_51 );
        if ( tmp_called_name_133 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_197 = GET_STRING_DICT_VALUE( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_Range );

        if (unlikely( tmp_mvar_value_197 == NULL ))
        {
            tmp_mvar_value_197 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Range );
        }

        if ( tmp_mvar_value_197 == NULL )
        {
            Py_DECREF( tmp_called_name_133 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Range" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 395;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_152 = tmp_mvar_value_197;
        frame_61aadf8837dc934bae8e309d56e02ee2->m_frame.f_lineno = 390;
        {
            PyObject *call_args[] = { tmp_args_element_name_152 };
            tmp_assign_source_94 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_133, call_args );
        }

        Py_DECREF( tmp_called_name_133 );
        if ( tmp_assign_source_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_grammars$money, (Nuitka_StringObject *)const_str_plain_RANGE, tmp_assign_source_94 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_61aadf8837dc934bae8e309d56e02ee2 );
#endif
    popFrameStack();

    assertFrameObject( frame_61aadf8837dc934bae8e309d56e02ee2 );

    goto frame_no_exception_4;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_61aadf8837dc934bae8e309d56e02ee2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_61aadf8837dc934bae8e309d56e02ee2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_61aadf8837dc934bae8e309d56e02ee2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_61aadf8837dc934bae8e309d56e02ee2, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_4:;

    return MOD_RETURN_VALUE( module_grammars$money );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
