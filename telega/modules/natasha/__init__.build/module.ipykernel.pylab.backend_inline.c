/* Generated code for Python module 'ipykernel.pylab.backend_inline'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_ipykernel$pylab$backend_inline" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_ipykernel$pylab$backend_inline;
PyDictObject *moduledict_ipykernel$pylab$backend_inline;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_pyplot;
static PyObject *const_str_digest_3e11435b48ed74a30b12f3ad448aa3f2;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_colors;
extern PyObject *const_str_plain_xaxis;
extern PyObject *const_str_plain_draw_if_interactive;
static PyObject *const_str_plain__is_transparent;
extern PyObject *const_str_plain_yaxis;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_args;
static PyObject *const_str_digest_fe0d253449712347b13be09e4fa96105;
extern PyObject *const_tuple_str_plain_display_tuple;
extern PyObject *const_tuple_str_plain_get_backend_tuple;
extern PyObject *const_str_plain_a;
static PyObject *const_str_plain_needs_background;
static PyObject *const_str_plain_rgbaArr;
extern PyObject *const_str_plain_unregister;
extern PyObject *const_str_plain_Gcf;
extern PyObject *const_str_plain_size;
extern PyObject *const_str_plain_instance;
extern PyObject *const_str_plain_close_figures;
static PyObject *const_str_plain_configure_inline_support;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_7df8ceb231fc5d679d9a68226ed4dd13;
extern PyObject *const_float_0_5;
static PyObject *const_tuple_str_plain_InlineBackend_tuple;
extern PyObject *const_str_plain_canvas;
extern PyObject *const_tuple_str_plain_colors_tuple;
extern PyObject *const_str_plain_get_ipython;
static PyObject *const_tuple_slice_none_none_none_slice_none_int_pos_3_none_tuple;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_str_plain_get_backend;
extern PyObject *const_str_plain_get_ticklabels;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_ac05d3da45dca72358f6c1362aeffc5b;
extern PyObject *const_str_plain_block;
static PyObject *const_str_plain__is_light;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_to_rgba_array;
extern PyObject *const_str_plain_close;
extern PyObject *const_str_plain_matplotlib;
static PyObject *const_tuple_4998d76f630be0a26289ab6440e90627_tuple;
extern PyObject *const_str_plain_get_facecolor;
extern PyObject *const_str_plain_metadata;
static PyObject *const_str_digest_94fc2e309c5e6d3b60575e07c80f016e;
extern PyObject *const_str_plain_label;
static PyObject *const_tuple_str_plain_new_figure_manager_str_plain_FigureCanvasAgg_tuple;
static PyObject *const_float_0_587;
static PyObject *const_tuple_str_plain_fig_str_plain_ticksLight_tuple;
static PyObject *const_tuple_str_plain_color_str_plain_rgbaArr_tuple;
extern PyObject *const_str_digest_8090d9314ba82f55c86e2a009f824e92;
static PyObject *const_tuple_str_plain_close_str_plain_block_str_plain_figure_manager_tuple;
extern PyObject *const_str_plain_fm;
extern PyObject *const_str_digest_79d9bffedced0e4cee681f99603a2fbc;
static PyObject *const_str_plain_dark;
extern PyObject *const_str_plain_FigureCanvasAgg;
static PyObject *const_tuple_str_plain_fig_str_plain_active_tuple;
extern PyObject *const_str_plain_all;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_new_figure_manager;
static PyObject *const_str_plain_ticksLight;
extern PyObject *const_str_plain_display;
extern PyObject *const_str_plain_FigureCanvas;
static PyObject *const_str_plain_activate_matplotlib;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_InlineBackend;
extern PyObject *const_str_plain_append;
static PyObject *const_float_0_114;
extern PyObject *const_str_digest_f355ba503a244dec56e3d7454c1ba0a3;
extern PyObject *const_str_digest_6ef69ad9ba636370ece1f3f94ebd9ce4;
static PyObject *const_float_0_299;
extern PyObject *const_str_plain_remove;
extern PyObject *const_tuple_type_ImportError_type_AttributeError_tuple;
extern PyObject *const_str_plain_config;
extern PyObject *const_str_plain_ip;
extern PyObject *const_str_plain_show;
static PyObject *const_tuple_float_0_299_float_0_587_float_0_114_tuple;
static PyObject *const_str_digest_cb851dcc9e3cdb5498cc6c377fa59d18;
static PyObject *const_str_plain__fetch_figure_metadata;
extern PyObject *const_str_plain_showtraceback;
extern PyObject *const_str_plain_get_color;
static PyObject *const_str_plain__enable_matplotlib_integration;
static PyObject *const_str_plain__draw_called;
static PyObject *const_tuple_7f3e02428687640479746f2cf1c7deac_tuple;
extern PyObject *const_tuple_str_plain_Gcf_tuple;
extern PyObject *const_int_0;
static PyObject *const_tuple_str_plain_axes_str_plain_axis_str_plain_label_tuple;
extern PyObject *const_slice_none_int_pos_3_none;
static PyObject *const_str_digest_c3dc39718f5d43b2b8ec972adbedeb86;
extern PyObject *const_str_plain_register;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_39a578d8a7ebadf7d5393da7b1006c1a;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_tuple_tuple_float_0_299_float_0_587_float_0_114_tuple_tuple;
static PyObject *const_tuple_str_plain_manager_str_plain_fig_tuple;
extern PyObject *const_str_plain_is_interactive;
static PyObject *const_str_plain__to_draw;
static PyObject *const_tuple_str_plain_color_str_plain_rgba_tuple;
extern PyObject *const_tuple_str_plain_all_tuple;
static PyObject *const_str_digest_31e7051de072b8c6dc953a34b6008916;
static PyObject *const_str_digest_f8a1c9d016d0db16e3b8bcd23c2efa72;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_color;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_print_function;
static PyObject *const_tuple_str_plain_a_str_plain_fig_tuple;
extern PyObject *const_str_plain_dot;
static PyObject *const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple;
extern PyObject *const_str_plain_events;
static PyObject *const_str_plain_figure_manager;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_flush_figures;
extern PyObject *const_str_plain_backend;
extern PyObject *const_str_plain_manager;
extern PyObject *const_str_plain_axis;
static PyObject *const_str_digest_400a510d64c550097facdebb4cc806a3;
extern PyObject *const_str_plain_fig;
extern PyObject *const_str_plain_get_active;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_rgba;
static PyObject *const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple;
extern PyObject *const_slice_none_none_none;
extern PyObject *const_str_plain_active;
static PyObject *const_str_plain_configure_once;
extern PyObject *const_str_plain_axes;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_ee909890eb7563ac3618f0be14f8d328;
extern PyObject *const_str_plain_figure;
extern PyObject *const_str_plain_to_rgba;
static PyObject *const_tuple_str_plain_fm_tuple;
static PyObject *const_str_digest_b9f2a881cafefcbc0d48837561210923;
static PyObject *const_str_digest_4d8f574fc18cab1e4cf1aff5bfe9300d;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_tuple_str_plain_get_ipython_tuple;
extern PyObject *const_str_plain_light;
extern PyObject *const_str_plain_get_all_fig_managers;
static PyObject *const_str_digest_e6984948594e6a7de68fe3c0b8bf2b4d;
static PyObject *const_str_plain_post_run_cell;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_3e11435b48ed74a30b12f3ad448aa3f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 893944 ], 60, 0 );
    const_str_plain__is_transparent = UNSTREAM_STRING_ASCII( &constant_bin[ 894004 ], 15, 1 );
    const_str_digest_fe0d253449712347b13be09e4fa96105 = UNSTREAM_STRING_ASCII( &constant_bin[ 894019 ], 174, 0 );
    const_str_plain_needs_background = UNSTREAM_STRING_ASCII( &constant_bin[ 894193 ], 16, 1 );
    const_str_plain_rgbaArr = UNSTREAM_STRING_ASCII( &constant_bin[ 894209 ], 7, 1 );
    const_str_plain_configure_inline_support = UNSTREAM_STRING_ASCII( &constant_bin[ 894216 ], 24, 1 );
    const_str_digest_7df8ceb231fc5d679d9a68226ed4dd13 = UNSTREAM_STRING_ASCII( &constant_bin[ 857009 ], 20, 0 );
    const_tuple_str_plain_InlineBackend_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_InlineBackend_tuple, 0, const_str_plain_InlineBackend ); Py_INCREF( const_str_plain_InlineBackend );
    const_tuple_slice_none_none_none_slice_none_int_pos_3_none_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_slice_none_none_none_slice_none_int_pos_3_none_tuple, 0, const_slice_none_none_none ); Py_INCREF( const_slice_none_none_none );
    PyTuple_SET_ITEM( const_tuple_slice_none_none_none_slice_none_int_pos_3_none_tuple, 1, const_slice_none_int_pos_3_none ); Py_INCREF( const_slice_none_int_pos_3_none );
    const_str_digest_ac05d3da45dca72358f6c1362aeffc5b = UNSTREAM_STRING_ASCII( &constant_bin[ 894240 ], 454, 0 );
    const_str_plain__is_light = UNSTREAM_STRING_ASCII( &constant_bin[ 894694 ], 9, 1 );
    const_tuple_4998d76f630be0a26289ab6440e90627_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    const_str_plain_activate_matplotlib = UNSTREAM_STRING_ASCII( &constant_bin[ 894703 ], 19, 1 );
    PyTuple_SET_ITEM( const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 1, const_str_plain_activate_matplotlib ); Py_INCREF( const_str_plain_activate_matplotlib );
    PyTuple_SET_ITEM( const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 2, const_str_plain_backend ); Py_INCREF( const_str_plain_backend );
    PyTuple_SET_ITEM( const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 3, const_str_plain_configure_inline_support ); Py_INCREF( const_str_plain_configure_inline_support );
    PyTuple_SET_ITEM( const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 4, const_str_plain_ip ); Py_INCREF( const_str_plain_ip );
    const_str_plain_configure_once = UNSTREAM_STRING_ASCII( &constant_bin[ 894722 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 5, const_str_plain_configure_once ); Py_INCREF( const_str_plain_configure_once );
    const_str_digest_94fc2e309c5e6d3b60575e07c80f016e = UNSTREAM_STRING_ASCII( &constant_bin[ 894736 ], 34, 0 );
    const_tuple_str_plain_new_figure_manager_str_plain_FigureCanvasAgg_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_new_figure_manager_str_plain_FigureCanvasAgg_tuple, 0, const_str_plain_new_figure_manager ); Py_INCREF( const_str_plain_new_figure_manager );
    PyTuple_SET_ITEM( const_tuple_str_plain_new_figure_manager_str_plain_FigureCanvasAgg_tuple, 1, const_str_plain_FigureCanvasAgg ); Py_INCREF( const_str_plain_FigureCanvasAgg );
    const_float_0_587 = UNSTREAM_FLOAT( &constant_bin[ 894770 ] );
    const_tuple_str_plain_fig_str_plain_ticksLight_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fig_str_plain_ticksLight_tuple, 0, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    const_str_plain_ticksLight = UNSTREAM_STRING_ASCII( &constant_bin[ 894778 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fig_str_plain_ticksLight_tuple, 1, const_str_plain_ticksLight ); Py_INCREF( const_str_plain_ticksLight );
    const_tuple_str_plain_color_str_plain_rgbaArr_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_color_str_plain_rgbaArr_tuple, 0, const_str_plain_color ); Py_INCREF( const_str_plain_color );
    PyTuple_SET_ITEM( const_tuple_str_plain_color_str_plain_rgbaArr_tuple, 1, const_str_plain_rgbaArr ); Py_INCREF( const_str_plain_rgbaArr );
    const_tuple_str_plain_close_str_plain_block_str_plain_figure_manager_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_close_str_plain_block_str_plain_figure_manager_tuple, 0, const_str_plain_close ); Py_INCREF( const_str_plain_close );
    PyTuple_SET_ITEM( const_tuple_str_plain_close_str_plain_block_str_plain_figure_manager_tuple, 1, const_str_plain_block ); Py_INCREF( const_str_plain_block );
    const_str_plain_figure_manager = UNSTREAM_STRING_ASCII( &constant_bin[ 894788 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_close_str_plain_block_str_plain_figure_manager_tuple, 2, const_str_plain_figure_manager ); Py_INCREF( const_str_plain_figure_manager );
    const_str_plain_dark = UNSTREAM_STRING_ASCII( &constant_bin[ 53898 ], 4, 1 );
    const_tuple_str_plain_fig_str_plain_active_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fig_str_plain_active_tuple, 0, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    PyTuple_SET_ITEM( const_tuple_str_plain_fig_str_plain_active_tuple, 1, const_str_plain_active ); Py_INCREF( const_str_plain_active );
    const_float_0_114 = UNSTREAM_FLOAT( &constant_bin[ 894802 ] );
    const_float_0_299 = UNSTREAM_FLOAT( &constant_bin[ 894810 ] );
    const_tuple_float_0_299_float_0_587_float_0_114_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_float_0_299_float_0_587_float_0_114_tuple, 0, const_float_0_299 ); Py_INCREF( const_float_0_299 );
    PyTuple_SET_ITEM( const_tuple_float_0_299_float_0_587_float_0_114_tuple, 1, const_float_0_587 ); Py_INCREF( const_float_0_587 );
    PyTuple_SET_ITEM( const_tuple_float_0_299_float_0_587_float_0_114_tuple, 2, const_float_0_114 ); Py_INCREF( const_float_0_114 );
    const_str_digest_cb851dcc9e3cdb5498cc6c377fa59d18 = UNSTREAM_STRING_ASCII( &constant_bin[ 894818 ], 53, 0 );
    const_str_plain__fetch_figure_metadata = UNSTREAM_STRING_ASCII( &constant_bin[ 894871 ], 22, 1 );
    const_str_plain__enable_matplotlib_integration = UNSTREAM_STRING_ASCII( &constant_bin[ 894893 ], 30, 1 );
    const_str_plain__draw_called = UNSTREAM_STRING_ASCII( &constant_bin[ 894923 ], 12, 1 );
    const_tuple_7f3e02428687640479746f2cf1c7deac_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_7f3e02428687640479746f2cf1c7deac_tuple, 0, const_str_plain_configure_inline_support ); Py_INCREF( const_str_plain_configure_inline_support );
    PyTuple_SET_ITEM( const_tuple_7f3e02428687640479746f2cf1c7deac_tuple, 1, const_str_plain_activate_matplotlib ); Py_INCREF( const_str_plain_activate_matplotlib );
    const_tuple_str_plain_axes_str_plain_axis_str_plain_label_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_str_plain_axis_str_plain_label_tuple, 0, const_str_plain_axes ); Py_INCREF( const_str_plain_axes );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_str_plain_axis_str_plain_label_tuple, 1, const_str_plain_axis ); Py_INCREF( const_str_plain_axis );
    PyTuple_SET_ITEM( const_tuple_str_plain_axes_str_plain_axis_str_plain_label_tuple, 2, const_str_plain_label ); Py_INCREF( const_str_plain_label );
    const_str_digest_c3dc39718f5d43b2b8ec972adbedeb86 = UNSTREAM_STRING_ASCII( &constant_bin[ 894935 ], 39, 0 );
    const_str_digest_39a578d8a7ebadf7d5393da7b1006c1a = UNSTREAM_STRING_ASCII( &constant_bin[ 894943 ], 30, 0 );
    const_tuple_tuple_float_0_299_float_0_587_float_0_114_tuple_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_tuple_float_0_299_float_0_587_float_0_114_tuple_tuple, 0, const_tuple_float_0_299_float_0_587_float_0_114_tuple ); Py_INCREF( const_tuple_float_0_299_float_0_587_float_0_114_tuple );
    const_tuple_str_plain_manager_str_plain_fig_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_manager_str_plain_fig_tuple, 0, const_str_plain_manager ); Py_INCREF( const_str_plain_manager );
    PyTuple_SET_ITEM( const_tuple_str_plain_manager_str_plain_fig_tuple, 1, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    const_str_plain__to_draw = UNSTREAM_STRING_ASCII( &constant_bin[ 894974 ], 8, 1 );
    const_tuple_str_plain_color_str_plain_rgba_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_color_str_plain_rgba_tuple, 0, const_str_plain_color ); Py_INCREF( const_str_plain_color );
    PyTuple_SET_ITEM( const_tuple_str_plain_color_str_plain_rgba_tuple, 1, const_str_plain_rgba ); Py_INCREF( const_str_plain_rgba );
    const_str_digest_31e7051de072b8c6dc953a34b6008916 = UNSTREAM_STRING_ASCII( &constant_bin[ 894982 ], 89, 0 );
    const_str_digest_f8a1c9d016d0db16e3b8bcd23c2efa72 = UNSTREAM_STRING_ASCII( &constant_bin[ 895071 ], 23, 0 );
    const_tuple_str_plain_a_str_plain_fig_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_fig_tuple, 0, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_str_plain_a_str_plain_fig_tuple, 1, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 0, const_str_plain_get_backend ); Py_INCREF( const_str_plain_get_backend );
    PyTuple_SET_ITEM( const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 1, const_str_plain_ip ); Py_INCREF( const_str_plain_ip );
    PyTuple_SET_ITEM( const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 2, const_str_plain_backend ); Py_INCREF( const_str_plain_backend );
    PyTuple_SET_ITEM( const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 3, const_str_plain_configure_inline_support ); Py_INCREF( const_str_plain_configure_inline_support );
    PyTuple_SET_ITEM( const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 4, const_str_plain_activate_matplotlib ); Py_INCREF( const_str_plain_activate_matplotlib );
    PyTuple_SET_ITEM( const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 5, const_str_plain_configure_once ); Py_INCREF( const_str_plain_configure_once );
    const_str_plain_flush_figures = UNSTREAM_STRING_ASCII( &constant_bin[ 895094 ], 13, 1 );
    const_str_digest_400a510d64c550097facdebb4cc806a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 895107 ], 37, 0 );
    const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple, 0, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple, 1, const_str_plain_ip ); Py_INCREF( const_str_plain_ip );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple, 2, const_str_plain_active ); Py_INCREF( const_str_plain_active );
    PyTuple_SET_ITEM( const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple, 3, const_str_plain_fig ); Py_INCREF( const_str_plain_fig );
    const_str_digest_ee909890eb7563ac3618f0be14f8d328 = UNSTREAM_STRING_ASCII( &constant_bin[ 895144 ], 503, 0 );
    const_tuple_str_plain_fm_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_fm_tuple, 0, const_str_plain_fm ); Py_INCREF( const_str_plain_fm );
    const_str_digest_b9f2a881cafefcbc0d48837561210923 = UNSTREAM_STRING_ASCII( &constant_bin[ 895647 ], 51, 0 );
    const_str_digest_4d8f574fc18cab1e4cf1aff5bfe9300d = UNSTREAM_STRING_ASCII( &constant_bin[ 895698 ], 54, 0 );
    const_str_digest_e6984948594e6a7de68fe3c0b8bf2b4d = UNSTREAM_STRING_ASCII( &constant_bin[ 895752 ], 33, 0 );
    const_str_plain_post_run_cell = UNSTREAM_STRING_ASCII( &constant_bin[ 895785 ], 13, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_ipykernel$pylab$backend_inline( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_896fa765e9d33209e00f0e9dcb7c711f;
static PyCodeObject *codeobj_b7148923fec65e5f5d876b26b1e2cb93;
static PyCodeObject *codeobj_e9cc925c1e1e57471b89ffb4722ecec5;
static PyCodeObject *codeobj_3916db8562726766d5e1de040911726e;
static PyCodeObject *codeobj_1015e2ab33e2c9dd53b6aa2f0d45c6dd;
static PyCodeObject *codeobj_aae33955774a731e65f1ec252e16f0e1;
static PyCodeObject *codeobj_cb9f93bfea53ceb21f077042c146e4af;
static PyCodeObject *codeobj_5f5b6848cb7b160753646e30611d9bbe;
static PyCodeObject *codeobj_1c2391fce0fdc7c8adf34218454d4538;
static PyCodeObject *codeobj_7b42445b8010696950926459f86baab2;
static PyCodeObject *codeobj_351813ceb61fdcf1f824b495298d0d3b;
static PyCodeObject *codeobj_33ef6dc2045dd7b0fa4ccf457ec26325;
static PyCodeObject *codeobj_64226e3327ffd67626285bc5fbde2773;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_e6984948594e6a7de68fe3c0b8bf2b4d );
    codeobj_896fa765e9d33209e00f0e9dcb7c711f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 79, const_tuple_str_plain_a_str_plain_fig_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS );
    codeobj_b7148923fec65e5f5d876b26b1e2cb93 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 176, const_tuple_str_plain_axes_str_plain_axis_str_plain_label_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e9cc925c1e1e57471b89ffb4722ecec5 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 129, const_tuple_str_plain_fig_str_plain_active_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3916db8562726766d5e1de040911726e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 128, const_tuple_str_plain_fm_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1015e2ab33e2c9dd53b6aa2f0d45c6dd = MAKE_CODEOBJ( module_filename_obj, const_str_digest_c3dc39718f5d43b2b8ec972adbedeb86, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_aae33955774a731e65f1ec252e16f0e1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__enable_matplotlib_integration, 151, const_tuple_ae75dcd9be055c3079df250b9ca4d0aa_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cb9f93bfea53ceb21f077042c146e4af = MAKE_CODEOBJ( module_filename_obj, const_str_plain__fetch_figure_metadata, 171, const_tuple_str_plain_fig_str_plain_ticksLight_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5f5b6848cb7b160753646e30611d9bbe = MAKE_CODEOBJ( module_filename_obj, const_str_plain__is_light, 186, const_tuple_str_plain_color_str_plain_rgbaArr_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1c2391fce0fdc7c8adf34218454d4538 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__is_transparent, 193, const_tuple_str_plain_color_str_plain_rgba_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7b42445b8010696950926459f86baab2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configure_once, 163, const_tuple_4998d76f630be0a26289ab6440e90627_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS );
    codeobj_351813ceb61fdcf1f824b495298d0d3b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_draw_if_interactive, 55, const_tuple_str_plain_manager_str_plain_fig_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_33ef6dc2045dd7b0fa4ccf457ec26325 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_flush_figures, 100, const_tuple_str_plain_e_str_plain_ip_str_plain_active_str_plain_fig_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_64226e3327ffd67626285bc5fbde2773 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show, 19, const_tuple_str_plain_close_str_plain_block_str_plain_figure_manager_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_1_show( PyObject *defaults );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_3_flush_figures(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_6__is_light(  );


static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_7__is_transparent(  );


// The module function definitions.
static PyObject *impl_ipykernel$pylab$backend_inline$$$function_1_show( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_close = python_pars[ 0 ];
    PyObject *par_block = python_pars[ 1 ];
    PyObject *var_figure_manager = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_64226e3327ffd67626285bc5fbde2773;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_64226e3327ffd67626285bc5fbde2773 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_64226e3327ffd67626285bc5fbde2773, codeobj_64226e3327ffd67626285bc5fbde2773, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_64226e3327ffd67626285bc5fbde2773 = cache_frame_64226e3327ffd67626285bc5fbde2773;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_64226e3327ffd67626285bc5fbde2773 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_64226e3327ffd67626285bc5fbde2773 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_close );
        tmp_compexpr_left_1 = par_close;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_InlineBackend );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InlineBackend );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InlineBackend" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 34;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 34;
            tmp_source_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_instance );
            if ( tmp_source_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_close_figures );
            Py_DECREF( tmp_source_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_close;
                assert( old != NULL );
                par_close = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 36;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_all_fig_managers );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "ooo";
            goto try_except_handler_2;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                exception_lineno = 36;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_figure_manager;
            var_figure_manager = tmp_assign_source_4;
            Py_INCREF( var_figure_manager );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_display );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_display );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "display" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 37;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        CHECK_OBJECT( var_figure_manager );
        tmp_source_name_3 = var_figure_manager;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_canvas );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_figure );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_metadata;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_fetch_figure_metadata" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        CHECK_OBJECT( var_figure_manager );
        tmp_source_name_5 = var_figure_manager;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_canvas );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 39;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_figure );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 39;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 39;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 37;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "ooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 36;
        type_description_1 = "ooo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_64226e3327ffd67626285bc5fbde2773, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_64226e3327ffd67626285bc5fbde2773, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_5;
        tmp_assattr_name_1 = PyList_New( 0 );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_assattr_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }

        tmp_assattr_target_1 = tmp_mvar_value_5;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__to_draw, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_call_result_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_close );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_close );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }

        tmp_called_instance_3 = tmp_mvar_value_6;
        frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 45;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_all_fig_managers );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 45;
            type_description_1 = "ooo";
            goto try_except_handler_4;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_call_result_3;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_matplotlib );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_matplotlib );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "matplotlib" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 46;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }

            tmp_source_name_6 = tmp_mvar_value_7;
            tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_pyplot );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 46;
            tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_close, &PyTuple_GET_ITEM( const_tuple_str_plain_all_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto try_except_handler_4;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_2:;
    }
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 35;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_64226e3327ffd67626285bc5fbde2773->m_frame) frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooo";
    goto try_except_handler_4;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_1_show );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_8;
        tmp_assattr_name_2 = PyList_New( 0 );
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_assattr_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = tmp_mvar_value_8;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__to_draw, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        int tmp_truth_name_3;
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_result_4;
        int tmp_truth_name_4;
        CHECK_OBJECT( par_close );
        tmp_truth_name_3 = CHECK_IF_TRUE( par_close );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_2 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_5 = tmp_mvar_value_9;
        frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 45;
        tmp_call_result_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_get_all_fig_managers );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_call_result_4 );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_4 );

            exception_lineno = 45;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_4 );
        tmp_condition_result_3 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_condition_result_3 = tmp_and_left_value_2;
        and_end_2:;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_instance_6;
            PyObject *tmp_source_name_7;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_call_result_5;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_matplotlib );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_matplotlib );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "matplotlib" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_7 = tmp_mvar_value_10;
            tmp_called_instance_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_pyplot );
            if ( tmp_called_instance_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_64226e3327ffd67626285bc5fbde2773->m_frame.f_lineno = 46;
            tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_close, &PyTuple_GET_ITEM( const_tuple_str_plain_all_tuple, 0 ) );

            Py_DECREF( tmp_called_instance_6 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_3:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64226e3327ffd67626285bc5fbde2773 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64226e3327ffd67626285bc5fbde2773 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_64226e3327ffd67626285bc5fbde2773, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_64226e3327ffd67626285bc5fbde2773->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_64226e3327ffd67626285bc5fbde2773, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_64226e3327ffd67626285bc5fbde2773,
        type_description_1,
        par_close,
        par_block,
        var_figure_manager
    );


    // Release cached frame.
    if ( frame_64226e3327ffd67626285bc5fbde2773 == cache_frame_64226e3327ffd67626285bc5fbde2773 )
    {
        Py_DECREF( frame_64226e3327ffd67626285bc5fbde2773 );
    }
    cache_frame_64226e3327ffd67626285bc5fbde2773 = NULL;

    assertFrameObject( frame_64226e3327ffd67626285bc5fbde2773 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_1_show );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_close );
    Py_DECREF( par_close );
    par_close = NULL;

    CHECK_OBJECT( (PyObject *)par_block );
    Py_DECREF( par_block );
    par_block = NULL;

    Py_XDECREF( var_figure_manager );
    var_figure_manager = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_close );
    par_close = NULL;

    CHECK_OBJECT( (PyObject *)par_block );
    Py_DECREF( par_block );
    par_block = NULL;

    Py_XDECREF( var_figure_manager );
    var_figure_manager = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_1_show );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_manager = NULL;
    struct Nuitka_CellObject *var_fig = PyCell_EMPTY();
    struct Nuitka_FrameObject *frame_351813ceb61fdcf1f824b495298d0d3b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_351813ceb61fdcf1f824b495298d0d3b = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_351813ceb61fdcf1f824b495298d0d3b, codeobj_351813ceb61fdcf1f824b495298d0d3b, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *) );
    frame_351813ceb61fdcf1f824b495298d0d3b = cache_frame_351813ceb61fdcf1f824b495298d0d3b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_351813ceb61fdcf1f824b495298d0d3b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_351813ceb61fdcf1f824b495298d0d3b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_Gcf );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_351813ceb61fdcf1f824b495298d0d3b->m_frame.f_lineno = 63;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_active );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        assert( var_manager == NULL );
        var_manager = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_manager );
        tmp_compexpr_left_1 = var_manager;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_manager );
        tmp_source_name_2 = var_manager;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_canvas );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_figure );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_fig ) == NULL );
        PyCell_SET( var_fig, tmp_assign_source_2 );

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( PyCell_GET( var_fig ) );
        tmp_source_name_3 = PyCell_GET( var_fig );
        tmp_attribute_name_1 = const_str_plain_show;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_3, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            tmp_assattr_name_1 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda(  );

            ((struct Nuitka_FunctionObject *)tmp_assattr_name_1)->m_closure[0] = var_fig;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assattr_name_1)->m_closure[0] );


            CHECK_OBJECT( PyCell_GET( var_fig ) );
            tmp_assattr_target_1 = PyCell_GET( var_fig );
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_show, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "oc";
                goto frame_exception_exit_1;
            }
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_matplotlib );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_matplotlib );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "matplotlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        frame_351813ceb61fdcf1f824b495298d0d3b->m_frame.f_lineno = 84;
        tmp_operand_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_is_interactive );
        if ( tmp_operand_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        Py_DECREF( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_3:;
    }
    // Tried code:
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }

        tmp_source_name_4 = tmp_mvar_value_3;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__to_draw );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( PyCell_GET( var_fig ) );
        tmp_args_element_name_1 = PyCell_GET( var_fig );
        frame_351813ceb61fdcf1f824b495298d0d3b->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_remove, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_351813ceb61fdcf1f824b495298d0d3b, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_351813ceb61fdcf1f824b495298d0d3b, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_ValueError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oc";
            goto try_except_handler_3;
        }
        tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oc";
            goto try_except_handler_3;
        }
        tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 90;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_351813ceb61fdcf1f824b495298d0d3b->m_frame) frame_351813ceb61fdcf1f824b495298d0d3b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oc";
        goto try_except_handler_3;
        branch_no_4:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive );
    return NULL;
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_4;
        tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__to_draw );
        if ( tmp_called_instance_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( var_fig ) );
        tmp_args_element_name_2 = PyCell_GET( var_fig );
        frame_351813ceb61fdcf1f824b495298d0d3b->m_frame.f_lineno = 96;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, call_args );
        }

        Py_DECREF( tmp_called_instance_4 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_5;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = tmp_mvar_value_5;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__draw_called, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_351813ceb61fdcf1f824b495298d0d3b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_351813ceb61fdcf1f824b495298d0d3b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_351813ceb61fdcf1f824b495298d0d3b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_351813ceb61fdcf1f824b495298d0d3b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_351813ceb61fdcf1f824b495298d0d3b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_351813ceb61fdcf1f824b495298d0d3b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_351813ceb61fdcf1f824b495298d0d3b,
        type_description_1,
        var_manager,
        var_fig
    );


    // Release cached frame.
    if ( frame_351813ceb61fdcf1f824b495298d0d3b == cache_frame_351813ceb61fdcf1f824b495298d0d3b )
    {
        Py_DECREF( frame_351813ceb61fdcf1f824b495298d0d3b );
    }
    cache_frame_351813ceb61fdcf1f824b495298d0d3b = NULL;

    assertFrameObject( frame_351813ceb61fdcf1f824b495298d0d3b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_manager );
    Py_DECREF( var_manager );
    var_manager = NULL;

    CHECK_OBJECT( (PyObject *)var_fig );
    Py_DECREF( var_fig );
    var_fig = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_manager );
    var_manager = NULL;

    CHECK_OBJECT( (PyObject *)var_fig );
    Py_DECREF( var_fig );
    var_fig = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_a = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_896fa765e9d33209e00f0e9dcb7c711f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_896fa765e9d33209e00f0e9dcb7c711f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_896fa765e9d33209e00f0e9dcb7c711f, codeobj_896fa765e9d33209e00f0e9dcb7c711f, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *) );
    frame_896fa765e9d33209e00f0e9dcb7c711f = cache_frame_896fa765e9d33209e00f0e9dcb7c711f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_896fa765e9d33209e00f0e9dcb7c711f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_896fa765e9d33209e00f0e9dcb7c711f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_display );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_display );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "display" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = PyCell_GET( self->m_closure[0] );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_metadata;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_fetch_figure_metadata" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "fig" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        frame_896fa765e9d33209e00f0e9dcb7c711f->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_896fa765e9d33209e00f0e9dcb7c711f->m_frame.f_lineno = 79;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_896fa765e9d33209e00f0e9dcb7c711f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_896fa765e9d33209e00f0e9dcb7c711f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_896fa765e9d33209e00f0e9dcb7c711f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_896fa765e9d33209e00f0e9dcb7c711f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_896fa765e9d33209e00f0e9dcb7c711f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_896fa765e9d33209e00f0e9dcb7c711f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_896fa765e9d33209e00f0e9dcb7c711f,
        type_description_1,
        par_a,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_896fa765e9d33209e00f0e9dcb7c711f == cache_frame_896fa765e9d33209e00f0e9dcb7c711f )
    {
        Py_DECREF( frame_896fa765e9d33209e00f0e9dcb7c711f );
    }
    cache_frame_896fa765e9d33209e00f0e9dcb7c711f = NULL;

    assertFrameObject( frame_896fa765e9d33209e00f0e9dcb7c711f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_3_flush_figures( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_e = NULL;
    PyObject *var_ip = NULL;
    PyObject *var_active = NULL;
    PyObject *var_fig = NULL;
    PyObject *outline_0_var_fm = NULL;
    PyObject *outline_1_var_fig = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_33ef6dc2045dd7b0fa4ccf457ec26325;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    struct Nuitka_FrameObject *frame_3916db8562726766d5e1de040911726e_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_3916db8562726766d5e1de040911726e_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    struct Nuitka_FrameObject *frame_e9cc925c1e1e57471b89ffb4722ecec5_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_e9cc925c1e1e57471b89ffb4722ecec5_3 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    static struct Nuitka_FrameObject *cache_frame_33ef6dc2045dd7b0fa4ccf457ec26325 = NULL;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_33ef6dc2045dd7b0fa4ccf457ec26325, codeobj_33ef6dc2045dd7b0fa4ccf457ec26325, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_33ef6dc2045dd7b0fa4ccf457ec26325 = cache_frame_33ef6dc2045dd7b0fa4ccf457ec26325;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_33ef6dc2045dd7b0fa4ccf457ec26325 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_33ef6dc2045dd7b0fa4ccf457ec26325 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__draw_called );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_InlineBackend );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InlineBackend );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InlineBackend" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 114;
        tmp_source_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_instance );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_close_figures );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 114;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        // Tried code:
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 117;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }

            tmp_called_name_1 = tmp_mvar_value_3;
            frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 117;
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_true_tuple, 0 ) );

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 117;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            goto frame_return_exit_1;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_1 = PyExc_Exception;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_1;
                tmp_assign_source_1 = EXC_VALUE(PyThreadState_GET());
                assert( var_e == NULL );
                Py_INCREF( tmp_assign_source_1 );
                var_e = tmp_assign_source_1;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_name_2;
                PyObject *tmp_mvar_value_4;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_get_ipython );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_ipython );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_ipython" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 120;
                    type_description_1 = "oooo";
                    goto try_except_handler_4;
                }

                tmp_called_name_2 = tmp_mvar_value_4;
                frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 120;
                tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 120;
                    type_description_1 = "oooo";
                    goto try_except_handler_4;
                }
                assert( var_ip == NULL );
                var_ip = tmp_assign_source_2;
            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                CHECK_OBJECT( var_ip );
                tmp_compexpr_left_2 = var_ip;
                tmp_compexpr_right_2 = Py_None;
                tmp_condition_result_4 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_raise_type_1;
                    CHECK_OBJECT( var_e );
                    tmp_raise_type_1 = var_e;
                    exception_type = tmp_raise_type_1;
                    Py_INCREF( tmp_raise_type_1 );
                    exception_lineno = 122;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooo";
                    goto try_except_handler_4;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    PyObject *tmp_called_instance_2;
                    PyObject *tmp_call_result_1;
                    CHECK_OBJECT( var_ip );
                    tmp_called_instance_2 = var_ip;
                    frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 124;
                    tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_showtraceback );
                    if ( tmp_call_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 124;
                        type_description_1 = "oooo";
                        goto try_except_handler_4;
                    }
                    Py_DECREF( tmp_call_result_1 );
                }
                tmp_return_value = Py_None;
                Py_INCREF( tmp_return_value );
                goto try_return_handler_4;
                branch_end_4:;
            }
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
            return NULL;
            // Return handler code:
            try_return_handler_4:;
            Py_XDECREF( var_e );
            var_e = NULL;

            goto try_return_handler_3;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( var_e );
            var_e = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_3;
            // End of try:
            goto branch_end_3;
            branch_no_3:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 116;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame) frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooo";
            goto try_except_handler_3;
            branch_end_3:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        branch_no_2:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_set_arg_1;
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_Gcf );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Gcf );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Gcf" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 128;
                type_description_1 = "oooo";
                goto try_except_handler_6;
            }

            tmp_called_instance_3 = tmp_mvar_value_5;
            frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 128;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_all_fig_managers );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "oooo";
                goto try_except_handler_6;
            }
            tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "oooo";
                goto try_except_handler_6;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3916db8562726766d5e1de040911726e_2, codeobj_3916db8562726766d5e1de040911726e, module_ipykernel$pylab$backend_inline, sizeof(void *) );
        frame_3916db8562726766d5e1de040911726e_2 = cache_frame_3916db8562726766d5e1de040911726e_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3916db8562726766d5e1de040911726e_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3916db8562726766d5e1de040911726e_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 128;
                    goto try_except_handler_7;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_7 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_fm;
                outline_0_var_fm = tmp_assign_source_7;
                Py_INCREF( outline_0_var_fm );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_fm );
            tmp_source_name_4 = outline_0_var_fm;
            tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_canvas );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_2 = "o";
                goto try_except_handler_7;
            }
            tmp_append_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_figure );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_2 = "o";
                goto try_except_handler_7;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_2 = "o";
                goto try_except_handler_7;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_2 = "o";
            goto try_except_handler_7;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_set_arg_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_set_arg_1 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3916db8562726766d5e1de040911726e_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_3916db8562726766d5e1de040911726e_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_6;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3916db8562726766d5e1de040911726e_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3916db8562726766d5e1de040911726e_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3916db8562726766d5e1de040911726e_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3916db8562726766d5e1de040911726e_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3916db8562726766d5e1de040911726e_2,
            type_description_2,
            outline_0_var_fm
        );


        // Release cached frame.
        if ( frame_3916db8562726766d5e1de040911726e_2 == cache_frame_3916db8562726766d5e1de040911726e_2 )
        {
            Py_DECREF( frame_3916db8562726766d5e1de040911726e_2 );
        }
        cache_frame_3916db8562726766d5e1de040911726e_2 = NULL;

        assertFrameObject( frame_3916db8562726766d5e1de040911726e_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "oooo";
        goto try_except_handler_6;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        Py_XDECREF( outline_0_var_fm );
        outline_0_var_fm = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_fm );
        outline_0_var_fm = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        outline_exception_1:;
        exception_lineno = 128;
        goto try_except_handler_5;
        outline_result_1:;
        tmp_assign_source_3 = PySet_New( tmp_set_arg_1 );
        Py_DECREF( tmp_set_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "oooo";
            goto try_except_handler_5;
        }
        assert( var_active == NULL );
        var_active = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_iter_arg_2;
        // Tried code:
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 129;
                type_description_1 = "oooo";
                goto try_except_handler_8;
            }

            tmp_source_name_5 = tmp_mvar_value_6;
            tmp_iter_arg_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__to_draw );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooo";
                goto try_except_handler_8;
            }
            tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooo";
                goto try_except_handler_8;
            }
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_9;
        }
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_e9cc925c1e1e57471b89ffb4722ecec5_3, codeobj_e9cc925c1e1e57471b89ffb4722ecec5, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *) );
        frame_e9cc925c1e1e57471b89ffb4722ecec5_3 = cache_frame_e9cc925c1e1e57471b89ffb4722ecec5_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_11;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_2 = tmp_listcomp_2__$0;
            tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 129;
                    goto try_except_handler_9;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_12;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_12 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_fig;
                outline_1_var_fig = tmp_assign_source_12;
                Py_INCREF( outline_1_var_fig );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( outline_1_var_fig );
            tmp_compexpr_left_3 = outline_1_var_fig;
            CHECK_OBJECT( var_active );
            tmp_compexpr_right_3 = var_active;
            tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "oo";
                goto try_except_handler_9;
            }
            tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_append_list_2;
                PyObject *tmp_append_value_2;
                CHECK_OBJECT( tmp_listcomp_2__contraction );
                tmp_append_list_2 = tmp_listcomp_2__contraction;
                CHECK_OBJECT( outline_1_var_fig );
                tmp_append_value_2 = outline_1_var_fig;
                assert( PyList_Check( tmp_append_list_2 ) );
                tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 129;
                    type_description_2 = "oo";
                    goto try_except_handler_9;
                }
            }
            branch_no_5:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_2 = "oo";
            goto try_except_handler_9;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_iter_arg_2 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_iter_arg_2 );
        goto try_return_handler_9;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_8;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_e9cc925c1e1e57471b89ffb4722ecec5_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_e9cc925c1e1e57471b89ffb4722ecec5_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_e9cc925c1e1e57471b89ffb4722ecec5_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_e9cc925c1e1e57471b89ffb4722ecec5_3,
            type_description_2,
            outline_1_var_fig,
            var_active
        );


        // Release cached frame.
        if ( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 == cache_frame_e9cc925c1e1e57471b89ffb4722ecec5_3 )
        {
            Py_DECREF( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 );
        }
        cache_frame_e9cc925c1e1e57471b89ffb4722ecec5_3 = NULL;

        assertFrameObject( frame_e9cc925c1e1e57471b89ffb4722ecec5_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "oooo";
        goto try_except_handler_8;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Return handler code:
        try_return_handler_8:;
        Py_XDECREF( outline_1_var_fig );
        outline_1_var_fig = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_fig );
        outline_1_var_fig = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        outline_exception_2:;
        exception_lineno = 129;
        goto try_except_handler_5;
        outline_result_2:;
        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "oooo";
            goto try_except_handler_5;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_8;
    }
    // Tried code:
    loop_start_3:;
    {
        PyObject *tmp_next_source_3;
        PyObject *tmp_assign_source_13;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_3 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_13 = ITERATOR_NEXT( tmp_next_source_3 );
        if ( tmp_assign_source_13 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_3;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 129;
                goto try_except_handler_10;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_13;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_14;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_14 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_fig;
            var_fig = tmp_assign_source_14;
            Py_INCREF( var_fig );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_display );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_display );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "display" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "oooo";
            goto try_except_handler_11;
        }

        tmp_called_name_3 = tmp_mvar_value_7;
        CHECK_OBJECT( var_fig );
        tmp_tuple_element_1 = var_fig;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_metadata;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_fetch_figure_metadata" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "oooo";
            goto try_except_handler_11;
        }

        tmp_called_name_4 = tmp_mvar_value_8;
        CHECK_OBJECT( var_fig );
        tmp_args_element_name_1 = var_fig;
        frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 131;
            type_description_1 = "oooo";
            goto try_except_handler_11;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 131;
        tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooo";
            goto try_except_handler_11;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_8 == NULL )
    {
        exception_keeper_tb_8 = MAKE_TRACEBACK( frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_keeper_lineno_8 );
    }
    else if ( exception_keeper_lineno_8 != 0 )
    {
        exception_keeper_tb_8 = ADD_TRACEBACK( exception_keeper_tb_8, frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_keeper_lineno_8 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_8, &exception_keeper_value_8, &exception_keeper_tb_8 );
    PyException_SetTraceback( exception_keeper_value_8, (PyObject *)exception_keeper_tb_8 );
    PUBLISH_EXCEPTION( &exception_keeper_type_8, &exception_keeper_value_8, &exception_keeper_tb_8 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_4 = PyExc_Exception;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooo";
            goto try_except_handler_12;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = EXC_VALUE(PyThreadState_GET());
            {
                PyObject *old = var_e;
                var_e = tmp_assign_source_15;
                Py_INCREF( var_e );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_9;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_get_ipython );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_ipython );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_ipython" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "oooo";
                goto try_except_handler_13;
            }

            tmp_called_name_5 = tmp_mvar_value_9;
            frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 134;
            tmp_assign_source_16 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "oooo";
                goto try_except_handler_13;
            }
            {
                PyObject *old = var_ip;
                var_ip = tmp_assign_source_16;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( var_ip );
            tmp_compexpr_left_5 = var_ip;
            tmp_compexpr_right_5 = Py_None;
            tmp_condition_result_7 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                CHECK_OBJECT( var_e );
                tmp_raise_type_2 = var_e;
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_lineno = 136;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                goto try_except_handler_13;
            }
            goto branch_end_7;
            branch_no_7:;
            {
                PyObject *tmp_called_instance_4;
                PyObject *tmp_call_result_3;
                CHECK_OBJECT( var_ip );
                tmp_called_instance_4 = var_ip;
                frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = 138;
                tmp_call_result_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_showtraceback );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;
                    type_description_1 = "oooo";
                    goto try_except_handler_13;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_13;
            branch_end_7:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
        return NULL;
        // Return handler code:
        try_return_handler_13:;
        Py_XDECREF( var_e );
        var_e = NULL;

        goto try_return_handler_12;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_12;
        // End of try:
        goto branch_end_6;
        branch_no_6:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 130;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame) frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooo";
        goto try_except_handler_12;
        branch_end_6:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
    return NULL;
    // Return handler code:
    try_return_handler_12:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_return_handler_10;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto try_except_handler_10;
    // End of try:
    // End of try:
    try_end_1:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_10;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_2;
    // Return handler code:
    try_return_handler_10:;
    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
    Py_DECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    goto try_return_handler_5;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_5;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Return handler code:
    try_return_handler_5:;
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_10;
        tmp_assattr_name_1 = PyList_New( 0 );
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_assattr_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_1 = tmp_mvar_value_10;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__to_draw, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_11;
        tmp_assattr_name_2 = Py_False;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = tmp_mvar_value_11;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__draw_called, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_3 );
    exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_3 );
    exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_3 );

    if ( exception_keeper_tb_12 == NULL )
    {
        exception_keeper_tb_12 = MAKE_TRACEBACK( frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_keeper_lineno_12 );
    }
    else if ( exception_keeper_lineno_12 != 0 )
    {
        exception_keeper_tb_12 = ADD_TRACEBACK( exception_keeper_tb_12, frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_keeper_lineno_12 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_12, &exception_keeper_value_12, &exception_keeper_tb_12 );
    PyException_SetTraceback( exception_keeper_value_12, (PyObject *)exception_keeper_tb_12 );
    PUBLISH_EXCEPTION( &exception_keeper_type_12, &exception_keeper_value_12, &exception_keeper_tb_12 );
    // Tried code:
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_mvar_value_12;
        tmp_assattr_name_3 = PyList_New( 0 );
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_assattr_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "oooo";
            goto try_except_handler_14;
        }

        tmp_assattr_target_3 = tmp_mvar_value_12;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__to_draw, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooo";
            goto try_except_handler_14;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_mvar_value_13;
        tmp_assattr_name_4 = Py_False;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "oooo";
            goto try_except_handler_14;
        }

        tmp_assattr_target_4 = tmp_mvar_value_13;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__draw_called, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooo";
            goto try_except_handler_14;
        }
    }
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 126;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame) frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "oooo";
    goto try_except_handler_14;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
    return NULL;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        PyObject *tmp_mvar_value_14;
        tmp_assattr_name_5 = PyList_New( 0 );
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_assattr_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_5 = tmp_mvar_value_14;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__to_draw, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        PyObject *tmp_mvar_value_15;
        tmp_assattr_name_6 = Py_False;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_6 = tmp_mvar_value_15;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__draw_called, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_33ef6dc2045dd7b0fa4ccf457ec26325 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_33ef6dc2045dd7b0fa4ccf457ec26325 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_33ef6dc2045dd7b0fa4ccf457ec26325 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_33ef6dc2045dd7b0fa4ccf457ec26325->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_33ef6dc2045dd7b0fa4ccf457ec26325, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_33ef6dc2045dd7b0fa4ccf457ec26325,
        type_description_1,
        var_e,
        var_ip,
        var_active,
        var_fig
    );


    // Release cached frame.
    if ( frame_33ef6dc2045dd7b0fa4ccf457ec26325 == cache_frame_33ef6dc2045dd7b0fa4ccf457ec26325 )
    {
        Py_DECREF( frame_33ef6dc2045dd7b0fa4ccf457ec26325 );
    }
    cache_frame_33ef6dc2045dd7b0fa4ccf457ec26325 = NULL;

    assertFrameObject( frame_33ef6dc2045dd7b0fa4ccf457ec26325 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_ip );
    var_ip = NULL;

    Py_XDECREF( var_active );
    var_active = NULL;

    Py_XDECREF( var_fig );
    var_fig = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_ip );
    var_ip = NULL;

    Py_XDECREF( var_active );
    var_active = NULL;

    Py_XDECREF( var_fig );
    var_fig = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_3_flush_figures );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_get_backend = NULL;
    struct Nuitka_CellObject *var_ip = PyCell_EMPTY();
    struct Nuitka_CellObject *var_backend = PyCell_EMPTY();
    struct Nuitka_CellObject *var_configure_inline_support = PyCell_EMPTY();
    struct Nuitka_CellObject *var_activate_matplotlib = PyCell_EMPTY();
    struct Nuitka_CellObject *var_configure_once = PyCell_EMPTY();
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_aae33955774a731e65f1ec252e16f0e1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_aae33955774a731e65f1ec252e16f0e1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aae33955774a731e65f1ec252e16f0e1, codeobj_aae33955774a731e65f1ec252e16f0e1, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_aae33955774a731e65f1ec252e16f0e1 = cache_frame_aae33955774a731e65f1ec252e16f0e1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aae33955774a731e65f1ec252e16f0e1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aae33955774a731e65f1ec252e16f0e1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_matplotlib;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_get_backend_tuple;
        tmp_level_name_1 = const_int_0;
        frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 153;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_get_backend );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        assert( var_get_backend == NULL );
        var_get_backend = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_get_ipython );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_ipython );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_ipython" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 154;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 154;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_ip ) == NULL );
        PyCell_SET( var_ip, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        CHECK_OBJECT( var_get_backend );
        tmp_called_name_2 = var_get_backend;
        frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 155;
        tmp_assign_source_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_backend ) == NULL );
        PyCell_SET( var_backend, tmp_assign_source_3 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( PyCell_GET( var_ip ) );
        tmp_truth_name_1 = CHECK_IF_TRUE( PyCell_GET( var_ip ) );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( PyCell_GET( var_backend ) );
        tmp_compexpr_left_1 = PyCell_GET( var_backend );
        tmp_compexpr_right_1 = const_str_digest_f355ba503a244dec56e3d7454c1ba0a3;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_name_name_2;
            PyObject *tmp_globals_name_2;
            PyObject *tmp_locals_name_2;
            PyObject *tmp_fromlist_name_2;
            PyObject *tmp_level_name_2;
            tmp_name_name_2 = const_str_digest_6ef69ad9ba636370ece1f3f94ebd9ce4;
            tmp_globals_name_2 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
            tmp_locals_name_2 = Py_None;
            tmp_fromlist_name_2 = const_tuple_7f3e02428687640479746f2cf1c7deac_tuple;
            tmp_level_name_2 = const_int_0;
            frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 157;
            tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "occccc";
                goto frame_exception_exit_1;
            }
            assert( tmp_import_from_1__module == NULL );
            tmp_import_from_1__module = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_import_name_from_2;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_2 = tmp_import_from_1__module;
            tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_configure_inline_support );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "occccc";
                goto try_except_handler_2;
            }
            assert( PyCell_GET( var_configure_inline_support ) == NULL );
            PyCell_SET( var_configure_inline_support, tmp_assign_source_5 );

        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_import_name_from_3;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_3 = tmp_import_from_1__module;
            tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_activate_matplotlib );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "occccc";
                goto try_except_handler_2;
            }
            assert( PyCell_GET( var_activate_matplotlib ) == NULL );
            PyCell_SET( var_activate_matplotlib, tmp_assign_source_6 );

        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        // Tried code:
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( PyCell_GET( var_activate_matplotlib ) );
            tmp_called_name_3 = PyCell_GET( var_activate_matplotlib );
            CHECK_OBJECT( PyCell_GET( var_backend ) );
            tmp_args_element_name_1 = PyCell_GET( var_backend );
            frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 159;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 159;
                type_description_1 = "occccc";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( PyCell_GET( var_configure_inline_support ) );
            tmp_called_name_4 = PyCell_GET( var_configure_inline_support );
            CHECK_OBJECT( PyCell_GET( var_ip ) );
            tmp_args_element_name_2 = PyCell_GET( var_ip );
            CHECK_OBJECT( PyCell_GET( var_backend ) );
            tmp_args_element_name_3 = PyCell_GET( var_backend );
            frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 160;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
            }

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 160;
                type_description_1 = "occccc";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_2 == NULL )
        {
            exception_keeper_tb_2 = MAKE_TRACEBACK( frame_aae33955774a731e65f1ec252e16f0e1, exception_keeper_lineno_2 );
        }
        else if ( exception_keeper_lineno_2 != 0 )
        {
            exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_aae33955774a731e65f1ec252e16f0e1, exception_keeper_lineno_2 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
        PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
        PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = const_tuple_type_ImportError_type_AttributeError_tuple;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_1 = "occccc";
                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_7;
                tmp_assign_source_7 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once(  );

                ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] = var_activate_matplotlib;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[0] );
                ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[1] = var_backend;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[1] );
                ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[2] = var_configure_inline_support;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[2] );
                ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[3] = var_configure_once;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[3] );
                ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[4] = var_ip;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_7)->m_closure[4] );


                assert( PyCell_GET( var_configure_once ) == NULL );
                PyCell_SET( var_configure_once, tmp_assign_source_7 );

            }
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_1;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                CHECK_OBJECT( PyCell_GET( var_ip ) );
                tmp_source_name_1 = PyCell_GET( var_ip );
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_events );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 167;
                    type_description_1 = "occccc";
                    goto try_except_handler_4;
                }
                tmp_args_element_name_4 = const_str_plain_post_run_cell;
                CHECK_OBJECT( PyCell_GET( var_configure_once ) );
                tmp_args_element_name_5 = PyCell_GET( var_configure_once );
                frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = 167;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                    tmp_call_result_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_register, call_args );
                }

                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 167;
                    type_description_1 = "occccc";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 158;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_aae33955774a731e65f1ec252e16f0e1->m_frame) frame_aae33955774a731e65f1ec252e16f0e1->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "occccc";
            goto try_except_handler_4;
            branch_end_2:;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        try_end_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto try_end_2;
        // exception handler codes exits in all cases
        NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration );
        return NULL;
        // End of try:
        try_end_2:;
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aae33955774a731e65f1ec252e16f0e1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aae33955774a731e65f1ec252e16f0e1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aae33955774a731e65f1ec252e16f0e1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aae33955774a731e65f1ec252e16f0e1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aae33955774a731e65f1ec252e16f0e1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aae33955774a731e65f1ec252e16f0e1,
        type_description_1,
        var_get_backend,
        var_ip,
        var_backend,
        var_configure_inline_support,
        var_activate_matplotlib,
        var_configure_once
    );


    // Release cached frame.
    if ( frame_aae33955774a731e65f1ec252e16f0e1 == cache_frame_aae33955774a731e65f1ec252e16f0e1 )
    {
        Py_DECREF( frame_aae33955774a731e65f1ec252e16f0e1 );
    }
    cache_frame_aae33955774a731e65f1ec252e16f0e1 = NULL;

    assertFrameObject( frame_aae33955774a731e65f1ec252e16f0e1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_get_backend );
    Py_DECREF( var_get_backend );
    var_get_backend = NULL;

    CHECK_OBJECT( (PyObject *)var_ip );
    Py_DECREF( var_ip );
    var_ip = NULL;

    CHECK_OBJECT( (PyObject *)var_backend );
    Py_DECREF( var_backend );
    var_backend = NULL;

    CHECK_OBJECT( (PyObject *)var_configure_inline_support );
    Py_DECREF( var_configure_inline_support );
    var_configure_inline_support = NULL;

    CHECK_OBJECT( (PyObject *)var_activate_matplotlib );
    Py_DECREF( var_activate_matplotlib );
    var_activate_matplotlib = NULL;

    CHECK_OBJECT( (PyObject *)var_configure_once );
    Py_DECREF( var_configure_once );
    var_configure_once = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_get_backend );
    var_get_backend = NULL;

    CHECK_OBJECT( (PyObject *)var_ip );
    Py_DECREF( var_ip );
    var_ip = NULL;

    CHECK_OBJECT( (PyObject *)var_backend );
    Py_DECREF( var_backend );
    var_backend = NULL;

    CHECK_OBJECT( (PyObject *)var_configure_inline_support );
    Py_DECREF( var_configure_inline_support );
    var_configure_inline_support = NULL;

    CHECK_OBJECT( (PyObject *)var_activate_matplotlib );
    Py_DECREF( var_activate_matplotlib );
    var_activate_matplotlib = NULL;

    CHECK_OBJECT( (PyObject *)var_configure_once );
    Py_DECREF( var_configure_once );
    var_configure_once = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7b42445b8010696950926459f86baab2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7b42445b8010696950926459f86baab2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7b42445b8010696950926459f86baab2, codeobj_7b42445b8010696950926459f86baab2, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7b42445b8010696950926459f86baab2 = cache_frame_7b42445b8010696950926459f86baab2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7b42445b8010696950926459f86baab2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7b42445b8010696950926459f86baab2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "activate_matplotlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "backend" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 164;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
        frame_7b42445b8010696950926459f86baab2->m_frame.f_lineno = 164;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 164;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "configure_inline_support" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = PyCell_GET( self->m_closure[2] );
        if ( PyCell_GET( self->m_closure[4] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ip" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = PyCell_GET( self->m_closure[4] );
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "backend" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 165;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( self->m_closure[1] );
        frame_7b42445b8010696950926459f86baab2->m_frame.f_lineno = 165;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        if ( PyCell_GET( self->m_closure[4] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "ip" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( self->m_closure[4] );
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_events );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_unregister );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = const_str_plain_post_run_cell;
        if ( PyCell_GET( self->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "configure_once" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = PyCell_GET( self->m_closure[3] );
        frame_7b42445b8010696950926459f86baab2->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "occccc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b42445b8010696950926459f86baab2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b42445b8010696950926459f86baab2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7b42445b8010696950926459f86baab2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7b42445b8010696950926459f86baab2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7b42445b8010696950926459f86baab2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7b42445b8010696950926459f86baab2,
        type_description_1,
        par_args,
        self->m_closure[0],
        self->m_closure[1],
        self->m_closure[2],
        self->m_closure[4],
        self->m_closure[3]
    );


    // Release cached frame.
    if ( frame_7b42445b8010696950926459f86baab2 == cache_frame_7b42445b8010696950926459f86baab2 )
    {
        Py_DECREF( frame_7b42445b8010696950926459f86baab2 );
    }
    cache_frame_7b42445b8010696950926459f86baab2 = NULL;

    assertFrameObject( frame_7b42445b8010696950926459f86baab2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fig = python_pars[ 0 ];
    PyObject *var_ticksLight = NULL;
    PyObject *outline_0_var_axes = NULL;
    PyObject *outline_0_var_axis = NULL;
    PyObject *outline_0_var_label = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__contraction_iter_0 = NULL;
    PyObject *tmp_listcomp_1__contraction_iter_1 = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_1__iter_value_1 = NULL;
    PyObject *tmp_listcomp_1__iter_value_2 = NULL;
    struct Nuitka_FrameObject *frame_cb9f93bfea53ceb21f077042c146e4af;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_b7148923fec65e5f5d876b26b1e2cb93_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_b7148923fec65e5f5d876b26b1e2cb93_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_cb9f93bfea53ceb21f077042c146e4af = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cb9f93bfea53ceb21f077042c146e4af, codeobj_cb9f93bfea53ceb21f077042c146e4af, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *) );
    frame_cb9f93bfea53ceb21f077042c146e4af = cache_frame_cb9f93bfea53ceb21f077042c146e4af;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cb9f93bfea53ceb21f077042c146e4af );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cb9f93bfea53ceb21f077042c146e4af ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__is_transparent );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__is_transparent );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_is_transparent" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fig );
        tmp_called_instance_1 = par_fig;
        frame_cb9f93bfea53ceb21f077042c146e4af->m_frame.f_lineno = 174;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_facecolor );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_cb9f93bfea53ceb21f077042c146e4af->m_frame.f_lineno = 174;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 174;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__is_light );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__is_light );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_is_light" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 176;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            // Tried code:
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_source_name_1;
                CHECK_OBJECT( par_fig );
                tmp_source_name_1 = par_fig;
                tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_axes );
                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 177;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 176;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                assert( tmp_listcomp_1__$0 == NULL );
                tmp_listcomp_1__$0 = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                tmp_assign_source_3 = PyList_New( 0 );
                assert( tmp_listcomp_1__contraction == NULL );
                tmp_listcomp_1__contraction = tmp_assign_source_3;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_b7148923fec65e5f5d876b26b1e2cb93_2, codeobj_b7148923fec65e5f5d876b26b1e2cb93, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *)+sizeof(void *) );
            frame_b7148923fec65e5f5d876b26b1e2cb93_2 = cache_frame_b7148923fec65e5f5d876b26b1e2cb93_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_b7148923fec65e5f5d876b26b1e2cb93_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_b7148923fec65e5f5d876b26b1e2cb93_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( tmp_listcomp_1__$0 );
                tmp_next_source_1 = tmp_listcomp_1__$0;
                tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "ooo";
                        exception_lineno = 176;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_2;
                    tmp_listcomp_1__iter_value_2 = tmp_assign_source_4;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_5;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_2 );
                tmp_assign_source_5 = tmp_listcomp_1__iter_value_2;
                {
                    PyObject *old = outline_0_var_axes;
                    outline_0_var_axes = tmp_assign_source_5;
                    Py_INCREF( outline_0_var_axes );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_iter_arg_2;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_source_name_3;
                CHECK_OBJECT( outline_0_var_axes );
                tmp_source_name_2 = outline_0_var_axes;
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_xaxis );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 178;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                tmp_iter_arg_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_iter_arg_2, 0, tmp_tuple_element_1 );
                CHECK_OBJECT( outline_0_var_axes );
                tmp_source_name_3 = outline_0_var_axes;
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_yaxis );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_iter_arg_2 );

                    exception_lineno = 178;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_iter_arg_2, 1, tmp_tuple_element_1 );
                tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
                Py_DECREF( tmp_iter_arg_2 );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 176;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = tmp_listcomp_1__contraction_iter_1;
                    tmp_listcomp_1__contraction_iter_1 = tmp_assign_source_6;
                    Py_XDECREF( old );
                }

            }
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_7;
                CHECK_OBJECT( tmp_listcomp_1__contraction_iter_1 );
                tmp_next_source_2 = tmp_listcomp_1__contraction_iter_1;
                tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_7 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "ooo";
                        exception_lineno = 176;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_1;
                    tmp_listcomp_1__iter_value_1 = tmp_assign_source_7;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_8;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_1 );
                tmp_assign_source_8 = tmp_listcomp_1__iter_value_1;
                {
                    PyObject *old = outline_0_var_axis;
                    outline_0_var_axis = tmp_assign_source_8;
                    Py_INCREF( outline_0_var_axis );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_iter_arg_3;
                PyObject *tmp_called_instance_2;
                CHECK_OBJECT( outline_0_var_axis );
                tmp_called_instance_2 = outline_0_var_axis;
                frame_b7148923fec65e5f5d876b26b1e2cb93_2->m_frame.f_lineno = 179;
                tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_ticklabels );
                if ( tmp_iter_arg_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 179;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_3 );
                Py_DECREF( tmp_iter_arg_3 );
                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 176;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = tmp_listcomp_1__contraction_iter_0;
                    tmp_listcomp_1__contraction_iter_0 = tmp_assign_source_9;
                    Py_XDECREF( old );
                }

            }
            loop_start_3:;
            {
                PyObject *tmp_next_source_3;
                PyObject *tmp_assign_source_10;
                CHECK_OBJECT( tmp_listcomp_1__contraction_iter_0 );
                tmp_next_source_3 = tmp_listcomp_1__contraction_iter_0;
                tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_3 );
                if ( tmp_assign_source_10 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_3;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "ooo";
                        exception_lineno = 176;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_0;
                    tmp_listcomp_1__iter_value_0 = tmp_assign_source_10;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                tmp_assign_source_11 = tmp_listcomp_1__iter_value_0;
                {
                    PyObject *old = outline_0_var_label;
                    outline_0_var_label = tmp_assign_source_11;
                    Py_INCREF( outline_0_var_label );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_called_instance_3;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_label );
                tmp_called_instance_3 = outline_0_var_label;
                frame_b7148923fec65e5f5d876b26b1e2cb93_2->m_frame.f_lineno = 176;
                tmp_append_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_color );
                if ( tmp_append_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 176;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 176;
                    type_description_2 = "ooo";
                    goto try_except_handler_3;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            goto loop_start_3;
            loop_end_3:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction_iter_0 );
            Py_DECREF( tmp_listcomp_1__contraction_iter_0 );
            tmp_listcomp_1__contraction_iter_0 = NULL;

            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            goto loop_start_2;
            loop_end_2:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction_iter_1 );
            Py_DECREF( tmp_listcomp_1__contraction_iter_1 );
            tmp_listcomp_1__contraction_iter_1 = NULL;

            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_2 = "ooo";
                goto try_except_handler_3;
            }
            goto loop_start_1;
            loop_end_1:;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_args_element_name_2 = tmp_listcomp_1__contraction;
            Py_INCREF( tmp_args_element_name_2 );
            goto try_return_handler_3;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata );
            return NULL;
            // Return handler code:
            try_return_handler_3:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            Py_XDECREF( tmp_listcomp_1__contraction_iter_0 );
            tmp_listcomp_1__contraction_iter_0 = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_1 );
            tmp_listcomp_1__iter_value_1 = NULL;

            Py_XDECREF( tmp_listcomp_1__contraction_iter_1 );
            tmp_listcomp_1__contraction_iter_1 = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_2 );
            tmp_listcomp_1__iter_value_2 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            Py_XDECREF( tmp_listcomp_1__contraction_iter_0 );
            tmp_listcomp_1__contraction_iter_0 = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_1 );
            tmp_listcomp_1__iter_value_1 = NULL;

            Py_XDECREF( tmp_listcomp_1__contraction_iter_1 );
            tmp_listcomp_1__contraction_iter_1 = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_2 );
            tmp_listcomp_1__iter_value_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_b7148923fec65e5f5d876b26b1e2cb93_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_b7148923fec65e5f5d876b26b1e2cb93_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_2;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_b7148923fec65e5f5d876b26b1e2cb93_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_b7148923fec65e5f5d876b26b1e2cb93_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_b7148923fec65e5f5d876b26b1e2cb93_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_b7148923fec65e5f5d876b26b1e2cb93_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_b7148923fec65e5f5d876b26b1e2cb93_2,
                type_description_2,
                outline_0_var_axes,
                outline_0_var_axis,
                outline_0_var_label
            );


            // Release cached frame.
            if ( frame_b7148923fec65e5f5d876b26b1e2cb93_2 == cache_frame_b7148923fec65e5f5d876b26b1e2cb93_2 )
            {
                Py_DECREF( frame_b7148923fec65e5f5d876b26b1e2cb93_2 );
            }
            cache_frame_b7148923fec65e5f5d876b26b1e2cb93_2 = NULL;

            assertFrameObject( frame_b7148923fec65e5f5d876b26b1e2cb93_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            type_description_1 = "oo";
            goto try_except_handler_2;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata );
            return NULL;
            // Return handler code:
            try_return_handler_2:;
            Py_XDECREF( outline_0_var_axes );
            outline_0_var_axes = NULL;

            Py_XDECREF( outline_0_var_axis );
            outline_0_var_axis = NULL;

            Py_XDECREF( outline_0_var_label );
            outline_0_var_label = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_0_var_axes );
            outline_0_var_axes = NULL;

            Py_XDECREF( outline_0_var_axis );
            outline_0_var_axis = NULL;

            Py_XDECREF( outline_0_var_label );
            outline_0_var_label = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata );
            return NULL;
            outline_exception_1:;
            exception_lineno = 176;
            goto frame_exception_exit_1;
            outline_result_1:;
            frame_cb9f93bfea53ceb21f077042c146e4af->m_frame.f_lineno = 176;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_ticksLight == NULL );
            var_ticksLight = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_2;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_call_result_2;
            int tmp_truth_name_3;
            CHECK_OBJECT( var_ticksLight );
            tmp_source_name_4 = var_ticksLight;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_size );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 180;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( var_ticksLight );
            tmp_compexpr_left_1 = var_ticksLight;
            CHECK_OBJECT( var_ticksLight );
            tmp_subscribed_name_1 = var_ticksLight;
            tmp_subscript_name_1 = const_int_0;
            tmp_compexpr_right_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_compexpr_right_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_4 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_right_1 );
            if ( tmp_called_instance_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_cb9f93bfea53ceb21f077042c146e4af->m_frame.f_lineno = 180;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_all );
            Py_DECREF( tmp_called_instance_4 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_2 );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_2 );

                exception_lineno = 180;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_1 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_2 );
            tmp_condition_result_2 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_2 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_subscript_result_1;
                int tmp_truth_name_4;
                tmp_dict_key_1 = const_str_plain_needs_background;
                CHECK_OBJECT( var_ticksLight );
                tmp_subscribed_name_2 = var_ticksLight;
                tmp_subscript_name_2 = const_int_0;
                tmp_subscript_result_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
                if ( tmp_subscript_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 182;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_truth_name_4 = CHECK_IF_TRUE( tmp_subscript_result_1 );
                if ( tmp_truth_name_4 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_subscript_result_1 );

                    exception_lineno = 182;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_subscript_result_1 );
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                tmp_dict_value_1 = const_str_plain_dark;
                goto condexpr_end_1;
                condexpr_false_1:;
                tmp_dict_value_1 = const_str_plain_light;
                condexpr_end_1:;
                tmp_return_value = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                goto frame_return_exit_1;
            }
            branch_no_2:;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb9f93bfea53ceb21f077042c146e4af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb9f93bfea53ceb21f077042c146e4af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb9f93bfea53ceb21f077042c146e4af );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cb9f93bfea53ceb21f077042c146e4af, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cb9f93bfea53ceb21f077042c146e4af->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cb9f93bfea53ceb21f077042c146e4af, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cb9f93bfea53ceb21f077042c146e4af,
        type_description_1,
        par_fig,
        var_ticksLight
    );


    // Release cached frame.
    if ( frame_cb9f93bfea53ceb21f077042c146e4af == cache_frame_cb9f93bfea53ceb21f077042c146e4af )
    {
        Py_DECREF( frame_cb9f93bfea53ceb21f077042c146e4af );
    }
    cache_frame_cb9f93bfea53ceb21f077042c146e4af = NULL;

    assertFrameObject( frame_cb9f93bfea53ceb21f077042c146e4af );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    Py_XDECREF( var_ticksLight );
    var_ticksLight = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_fig );
    Py_DECREF( par_fig );
    par_fig = NULL;

    Py_XDECREF( var_ticksLight );
    var_ticksLight = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_6__is_light( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_color = python_pars[ 0 ];
    PyObject *var_rgbaArr = NULL;
    struct Nuitka_FrameObject *frame_5f5b6848cb7b160753646e30611d9bbe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5f5b6848cb7b160753646e30611d9bbe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5f5b6848cb7b160753646e30611d9bbe, codeobj_5f5b6848cb7b160753646e30611d9bbe, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *) );
    frame_5f5b6848cb7b160753646e30611d9bbe = cache_frame_5f5b6848cb7b160753646e30611d9bbe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5f5b6848cb7b160753646e30611d9bbe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5f5b6848cb7b160753646e30611d9bbe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_colors );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_colors );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "colors" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 190;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_color );
        tmp_args_element_name_1 = par_color;
        frame_5f5b6848cb7b160753646e30611d9bbe->m_frame.f_lineno = 190;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_to_rgba_array, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_rgbaArr == NULL );
        var_rgbaArr = tmp_assign_source_1;
    }
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( var_rgbaArr );
        tmp_subscribed_name_1 = var_rgbaArr;
        tmp_subscript_name_1 = const_tuple_slice_none_none_none_slice_none_int_pos_3_none_tuple;
        tmp_called_instance_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_5f5b6848cb7b160753646e30611d9bbe->m_frame.f_lineno = 191;
        tmp_compexpr_left_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_dot, &PyTuple_GET_ITEM( const_tuple_tuple_float_0_299_float_0_587_float_0_114_tuple_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_float_0_5;
        tmp_return_value = RICH_COMPARE_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f5b6848cb7b160753646e30611d9bbe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f5b6848cb7b160753646e30611d9bbe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f5b6848cb7b160753646e30611d9bbe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5f5b6848cb7b160753646e30611d9bbe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5f5b6848cb7b160753646e30611d9bbe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5f5b6848cb7b160753646e30611d9bbe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5f5b6848cb7b160753646e30611d9bbe,
        type_description_1,
        par_color,
        var_rgbaArr
    );


    // Release cached frame.
    if ( frame_5f5b6848cb7b160753646e30611d9bbe == cache_frame_5f5b6848cb7b160753646e30611d9bbe )
    {
        Py_DECREF( frame_5f5b6848cb7b160753646e30611d9bbe );
    }
    cache_frame_5f5b6848cb7b160753646e30611d9bbe = NULL;

    assertFrameObject( frame_5f5b6848cb7b160753646e30611d9bbe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_6__is_light );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_color );
    Py_DECREF( par_color );
    par_color = NULL;

    CHECK_OBJECT( (PyObject *)var_rgbaArr );
    Py_DECREF( var_rgbaArr );
    var_rgbaArr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_color );
    Py_DECREF( par_color );
    par_color = NULL;

    Py_XDECREF( var_rgbaArr );
    var_rgbaArr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_6__is_light );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_ipykernel$pylab$backend_inline$$$function_7__is_transparent( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_color = python_pars[ 0 ];
    PyObject *var_rgba = NULL;
    struct Nuitka_FrameObject *frame_1c2391fce0fdc7c8adf34218454d4538;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1c2391fce0fdc7c8adf34218454d4538 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1c2391fce0fdc7c8adf34218454d4538, codeobj_1c2391fce0fdc7c8adf34218454d4538, module_ipykernel$pylab$backend_inline, sizeof(void *)+sizeof(void *) );
    frame_1c2391fce0fdc7c8adf34218454d4538 = cache_frame_1c2391fce0fdc7c8adf34218454d4538;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1c2391fce0fdc7c8adf34218454d4538 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1c2391fce0fdc7c8adf34218454d4538 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_colors );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_colors );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "colors" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 195;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_color );
        tmp_args_element_name_1 = par_color;
        frame_1c2391fce0fdc7c8adf34218454d4538->m_frame.f_lineno = 195;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_to_rgba, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_rgba == NULL );
        var_rgba = tmp_assign_source_1;
    }
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( var_rgba );
        tmp_subscribed_name_1 = var_rgba;
        tmp_subscript_name_1 = const_int_pos_3;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 3 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_float_0_5;
        tmp_return_value = RICH_COMPARE_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1c2391fce0fdc7c8adf34218454d4538 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1c2391fce0fdc7c8adf34218454d4538 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1c2391fce0fdc7c8adf34218454d4538 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1c2391fce0fdc7c8adf34218454d4538, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1c2391fce0fdc7c8adf34218454d4538->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1c2391fce0fdc7c8adf34218454d4538, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1c2391fce0fdc7c8adf34218454d4538,
        type_description_1,
        par_color,
        var_rgba
    );


    // Release cached frame.
    if ( frame_1c2391fce0fdc7c8adf34218454d4538 == cache_frame_1c2391fce0fdc7c8adf34218454d4538 )
    {
        Py_DECREF( frame_1c2391fce0fdc7c8adf34218454d4538 );
    }
    cache_frame_1c2391fce0fdc7c8adf34218454d4538 = NULL;

    assertFrameObject( frame_1c2391fce0fdc7c8adf34218454d4538 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_7__is_transparent );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_color );
    Py_DECREF( par_color );
    par_color = NULL;

    CHECK_OBJECT( (PyObject *)var_rgba );
    Py_DECREF( var_rgba );
    var_rgba = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_color );
    Py_DECREF( par_color );
    par_color = NULL;

    Py_XDECREF( var_rgba );
    var_rgba = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( ipykernel$pylab$backend_inline$$$function_7__is_transparent );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_1_show( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_1_show,
        const_str_plain_show,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_64226e3327ffd67626285bc5fbde2773,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_ee909890eb7563ac3618f0be14f8d328,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive,
        const_str_plain_draw_if_interactive,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_351813ceb61fdcf1f824b495298d0d3b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_cb851dcc9e3cdb5498cc6c377fa59d18,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_400a510d64c550097facdebb4cc806a3,
#endif
        codeobj_896fa765e9d33209e00f0e9dcb7c711f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_3_flush_figures(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_3_flush_figures,
        const_str_plain_flush_figures,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_33ef6dc2045dd7b0fa4ccf457ec26325,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_ac05d3da45dca72358f6c1362aeffc5b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration,
        const_str_plain__enable_matplotlib_integration,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_aae33955774a731e65f1ec252e16f0e1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_31e7051de072b8c6dc953a34b6008916,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration$$$function_1_configure_once,
        const_str_plain_configure_once,
#if PYTHON_VERSION >= 300
        const_str_digest_4d8f574fc18cab1e4cf1aff5bfe9300d,
#endif
        codeobj_7b42445b8010696950926459f86baab2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        NULL,
        5
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata,
        const_str_plain__fetch_figure_metadata,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cb9f93bfea53ceb21f077042c146e4af,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_b9f2a881cafefcbc0d48837561210923,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_6__is_light(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_6__is_light,
        const_str_plain__is_light,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5f5b6848cb7b160753646e30611d9bbe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_fe0d253449712347b13be09e4fa96105,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_7__is_transparent(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_ipykernel$pylab$backend_inline$$$function_7__is_transparent,
        const_str_plain__is_transparent,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1c2391fce0fdc7c8adf34218454d4538,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_ipykernel$pylab$backend_inline,
        const_str_digest_94fc2e309c5e6d3b60575e07c80f016e,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_ipykernel$pylab$backend_inline =
{
    PyModuleDef_HEAD_INIT,
    "ipykernel.pylab.backend_inline",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(ipykernel$pylab$backend_inline)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(ipykernel$pylab$backend_inline)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_ipykernel$pylab$backend_inline );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("ipykernel.pylab.backend_inline: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("ipykernel.pylab.backend_inline: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("ipykernel.pylab.backend_inline: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initipykernel$pylab$backend_inline" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_ipykernel$pylab$backend_inline = Py_InitModule4(
        "ipykernel.pylab.backend_inline",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_ipykernel$pylab$backend_inline = PyModule_Create( &mdef_ipykernel$pylab$backend_inline );
#endif

    moduledict_ipykernel$pylab$backend_inline = MODULE_DICT( module_ipykernel$pylab$backend_inline );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_ipykernel$pylab$backend_inline,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_ipykernel$pylab$backend_inline,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_ipykernel$pylab$backend_inline,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_ipykernel$pylab$backend_inline,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_ipykernel$pylab$backend_inline );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_39a578d8a7ebadf7d5393da7b1006c1a, module_ipykernel$pylab$backend_inline );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_3e11435b48ed74a30b12f3ad448aa3f2;
        UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd = MAKE_MODULE_FRAME( codeobj_1015e2ab33e2c9dd53b6aa2f0d45c6dd, module_ipykernel$pylab$backend_inline );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd );
    assert( Py_REFCNT( frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 6;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_print_function );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_matplotlib;
        tmp_globals_name_1 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 8;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_matplotlib, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_8090d9314ba82f55c86e2a009f824e92;
        tmp_globals_name_2 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_new_figure_manager_str_plain_FigureCanvasAgg_tuple;
        tmp_level_name_2 = const_int_0;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 9;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_new_figure_manager );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_new_figure_manager, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_FigureCanvasAgg );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_FigureCanvasAgg, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_matplotlib;
        tmp_globals_name_3 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_colors_tuple;
        tmp_level_name_3 = const_int_0;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 10;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_colors );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_colors, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_79d9bffedced0e4cee681f99603a2fbc;
        tmp_globals_name_4 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Gcf_tuple;
        tmp_level_name_4 = const_int_0;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 11;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Gcf );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_Gcf, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_f8a1c9d016d0db16e3b8bcd23c2efa72;
        tmp_globals_name_5 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_get_ipython_tuple;
        tmp_level_name_5 = const_int_0;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 13;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_get_ipython );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_get_ipython, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_7df8ceb231fc5d679d9a68226ed4dd13;
        tmp_globals_name_6 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_display_tuple;
        tmp_level_name_6 = const_int_0;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 14;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_display );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_display, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_config;
        tmp_globals_name_7 = (PyObject *)moduledict_ipykernel$pylab$backend_inline;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_InlineBackend_tuple;
        tmp_level_name_7 = const_int_pos_1;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 16;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_ipykernel$pylab$backend_inline,
                const_str_plain_InlineBackend,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_InlineBackend );
        }

        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_InlineBackend, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_14 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_1_show( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        PyObject *tmp_mvar_value_3;
        tmp_assattr_name_3 = Py_False;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_assattr_target_3 = tmp_mvar_value_3;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__draw_called, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        PyObject *tmp_mvar_value_4;
        tmp_assattr_name_4 = PyList_New( 0 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_show );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_assattr_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;

            goto frame_exception_exit_1;
        }

        tmp_assattr_target_4 = tmp_mvar_value_4;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__to_draw, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_2_draw_if_interactive(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_draw_if_interactive, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_3_flush_figures(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_flush_figures, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_FigureCanvasAgg );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FigureCanvasAgg );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FigureCanvasAgg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_17 = tmp_mvar_value_5;
        UPDATE_STRING_DICT0( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain_FigureCanvas, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_4__enable_matplotlib_integration(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__enable_matplotlib_integration, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__enable_matplotlib_integration );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__enable_matplotlib_integration );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_called_name_1 = tmp_mvar_value_6;
        frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame.f_lineno = 169;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd );
#endif
    popFrameStack();

    assertFrameObject( frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1015e2ab33e2c9dd53b6aa2f0d45c6dd, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_5__fetch_figure_metadata(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__fetch_figure_metadata, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_6__is_light(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__is_light, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_ipykernel$pylab$backend_inline$$$function_7__is_transparent(  );



        UPDATE_STRING_DICT1( moduledict_ipykernel$pylab$backend_inline, (Nuitka_StringObject *)const_str_plain__is_transparent, tmp_assign_source_21 );
    }

    return MOD_RETURN_VALUE( module_ipykernel$pylab$backend_inline );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
