/* Generated code for Python module 'PIL.ImageChops'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_PIL$ImageChops" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_PIL$ImageChops;
PyDictObject *moduledict_PIL$ImageChops;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_blend;
static PyObject *const_str_digest_da0dca45318b0e88a977cc534db2c085;
extern PyObject *const_str_plain_image2;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_offset;
extern PyObject *const_str_plain_scale;
static PyObject *const_str_plain_chop_invert;
extern PyObject *const_str_plain_logical_xor;
static PyObject *const_str_digest_2366abc32a7a698ab8b1f922bf783013;
extern PyObject *const_str_plain_composite;
extern PyObject *const_str_plain_invert;
extern PyObject *const_str_plain_size;
extern PyObject *const_str_plain_subtract_modulo;
extern PyObject *const_str_plain_image;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_330931758f0ece41f45b97f63bd1d3cc;
static PyObject *const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple;
static PyObject *const_str_plain_chop_multiply;
extern PyObject *const_str_plain_subtract;
static PyObject *const_str_plain_chop_screen;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_9a5d03d9befb6e87d4d1ca27abfd50a8;
static PyObject *const_str_plain_duplicate;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_L;
extern PyObject *const_str_plain_add;
static PyObject *const_str_digest_18b038d615cf2881bb3b9e83332935d1;
extern PyObject *const_float_1_0;
static PyObject *const_str_digest_c2567550310528637a128acfb6c5b84a;
extern PyObject *const_str_plain_value;
static PyObject *const_str_digest_75d3ca04904dd8d83bb88c84b3351182;
extern PyObject *const_str_plain_screen;
static PyObject *const_str_digest_c4114e5a91c7c6fb76bf628af1bb6603;
extern PyObject *const_str_plain_alpha;
extern PyObject *const_str_plain_load;
static PyObject *const_str_digest_6990f30b1cd867bf667e4913f55b47ba;
extern PyObject *const_str_plain_mask;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_new;
static PyObject *const_str_digest_d5f99763a11d6c1c0de11ca998b46f2c;
static PyObject *const_str_digest_b42909581e7a9e471d384490ea7d49d8;
extern PyObject *const_str_plain__new;
static PyObject *const_tuple_float_1_0_int_0_tuple;
static PyObject *const_str_digest_f07efc180a7edc734c9cc6da7ea485e5;
static PyObject *const_tuple_str_plain_image_str_plain_value_tuple;
static PyObject *const_str_plain_add_modulo;
static PyObject *const_str_digest_b286e616f5af2c021e109847f78adb5b;
static PyObject *const_str_digest_3e0519114973f2ac39322526e664803d;
static PyObject *const_str_digest_fc9d3e28e9cdcadba0fba9dfa8980efc;
extern PyObject *const_tuple_str_plain_Image_tuple;
extern PyObject *const_str_plain_False;
static PyObject *const_tuple_str_plain_image_str_plain_xoffset_str_plain_yoffset_tuple;
static PyObject *const_str_digest_28d6326370ff3344dafef726a6138b01;
static PyObject *const_tuple_str_plain_image1_str_plain_image2_tuple;
extern PyObject *const_int_0;
static PyObject *const_str_plain_chop_darker;
static PyObject *const_str_plain_chop_difference;
extern PyObject *const_str_plain_logical_or;
extern PyObject *const_str_plain_logical_and;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_80ed5aa5d813a5989d5e2be433f0ecd8;
static PyObject *const_str_plain_chop_and;
static PyObject *const_str_plain_chop_xor;
static PyObject *const_str_plain_darker;
static PyObject *const_str_digest_ba4056a05a85932c26437bcda2fecef3;
extern PyObject *const_tuple_str_plain_image_tuple;
static PyObject *const_str_plain_chop_add_modulo;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_chop_or;
static PyObject *const_str_digest_37ac6f6e8d90b774baa26f6f389313fa;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_str_plain_chop_subtract_modulo;
static PyObject *const_str_plain_lighter;
extern PyObject *const_str_plain_difference;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_copy;
extern PyObject *const_str_plain_xoffset;
static PyObject *const_tuple_str_plain_image1_str_plain_image2_str_plain_mask_tuple;
static PyObject *const_str_plain_chop_subtract;
extern PyObject *const_str_plain_image1;
extern PyObject *const_str_plain_yoffset;
static PyObject *const_str_plain_chop_add;
static PyObject *const_str_digest_35d1aac12da8e690cf368dbb2e557b39;
extern PyObject *const_str_plain_im;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_e6c4a2040bc5430895d5761bc5985e97;
extern PyObject *const_str_plain_multiply;
extern PyObject *const_str_plain_Image;
extern PyObject *const_str_plain_constant;
extern PyObject *const_str_empty;
static PyObject *const_tuple_str_plain_image1_str_plain_image2_str_plain_alpha_tuple;
static PyObject *const_str_plain_chop_lighter;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_da0dca45318b0e88a977cc534db2c085 = UNSTREAM_STRING_ASCII( &constant_bin[ 51830 ], 382, 0 );
    const_str_plain_chop_invert = UNSTREAM_STRING_ASCII( &constant_bin[ 52212 ], 11, 1 );
    const_str_digest_2366abc32a7a698ab8b1f922bf783013 = UNSTREAM_STRING_ASCII( &constant_bin[ 52223 ], 14, 0 );
    const_str_digest_330931758f0ece41f45b97f63bd1d3cc = UNSTREAM_STRING_ASCII( &constant_bin[ 52237 ], 143, 0 );
    const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple, 0, const_str_plain_image1 ); Py_INCREF( const_str_plain_image1 );
    PyTuple_SET_ITEM( const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple, 1, const_str_plain_image2 ); Py_INCREF( const_str_plain_image2 );
    PyTuple_SET_ITEM( const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple, 2, const_str_plain_scale ); Py_INCREF( const_str_plain_scale );
    PyTuple_SET_ITEM( const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple, 3, const_str_plain_offset ); Py_INCREF( const_str_plain_offset );
    const_str_plain_chop_multiply = UNSTREAM_STRING_ASCII( &constant_bin[ 52380 ], 13, 1 );
    const_str_plain_chop_screen = UNSTREAM_STRING_ASCII( &constant_bin[ 52393 ], 11, 1 );
    const_str_digest_9a5d03d9befb6e87d4d1ca27abfd50a8 = UNSTREAM_STRING_ASCII( &constant_bin[ 52404 ], 261, 0 );
    const_str_plain_duplicate = UNSTREAM_STRING_ASCII( &constant_bin[ 52665 ], 9, 1 );
    const_str_digest_18b038d615cf2881bb3b9e83332935d1 = UNSTREAM_STRING_ASCII( &constant_bin[ 52674 ], 314, 0 );
    const_str_digest_c2567550310528637a128acfb6c5b84a = UNSTREAM_STRING_ASCII( &constant_bin[ 52988 ], 246, 0 );
    const_str_digest_75d3ca04904dd8d83bb88c84b3351182 = UNSTREAM_STRING_ASCII( &constant_bin[ 53234 ], 86, 0 );
    const_str_digest_c4114e5a91c7c6fb76bf628af1bb6603 = UNSTREAM_STRING_ASCII( &constant_bin[ 53320 ], 23, 0 );
    const_str_digest_6990f30b1cd867bf667e4913f55b47ba = UNSTREAM_STRING_ASCII( &constant_bin[ 53343 ], 17, 0 );
    const_str_digest_d5f99763a11d6c1c0de11ca998b46f2c = UNSTREAM_STRING_ASCII( &constant_bin[ 53360 ], 309, 0 );
    const_str_digest_b42909581e7a9e471d384490ea7d49d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 53669 ], 140, 0 );
    const_tuple_float_1_0_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_int_0_tuple, 0, const_float_1_0 ); Py_INCREF( const_float_1_0 );
    PyTuple_SET_ITEM( const_tuple_float_1_0_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_digest_f07efc180a7edc734c9cc6da7ea485e5 = UNSTREAM_STRING_ASCII( &constant_bin[ 53809 ], 260, 0 );
    const_tuple_str_plain_image_str_plain_value_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image_str_plain_value_tuple, 0, const_str_plain_image ); Py_INCREF( const_str_plain_image );
    PyTuple_SET_ITEM( const_tuple_str_plain_image_str_plain_value_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_plain_add_modulo = UNSTREAM_STRING_ASCII( &constant_bin[ 54069 ], 10, 1 );
    const_str_digest_b286e616f5af2c021e109847f78adb5b = UNSTREAM_STRING_ASCII( &constant_bin[ 54079 ], 250, 0 );
    const_str_digest_3e0519114973f2ac39322526e664803d = UNSTREAM_STRING_ASCII( &constant_bin[ 54329 ], 105, 0 );
    const_str_digest_fc9d3e28e9cdcadba0fba9dfa8980efc = UNSTREAM_STRING_ASCII( &constant_bin[ 54434 ], 198, 0 );
    const_tuple_str_plain_image_str_plain_xoffset_str_plain_yoffset_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image_str_plain_xoffset_str_plain_yoffset_tuple, 0, const_str_plain_image ); Py_INCREF( const_str_plain_image );
    PyTuple_SET_ITEM( const_tuple_str_plain_image_str_plain_xoffset_str_plain_yoffset_tuple, 1, const_str_plain_xoffset ); Py_INCREF( const_str_plain_xoffset );
    PyTuple_SET_ITEM( const_tuple_str_plain_image_str_plain_xoffset_str_plain_yoffset_tuple, 2, const_str_plain_yoffset ); Py_INCREF( const_str_plain_yoffset );
    const_str_digest_28d6326370ff3344dafef726a6138b01 = UNSTREAM_STRING_ASCII( &constant_bin[ 54632 ], 211, 0 );
    const_tuple_str_plain_image1_str_plain_image2_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_tuple, 0, const_str_plain_image1 ); Py_INCREF( const_str_plain_image1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_tuple, 1, const_str_plain_image2 ); Py_INCREF( const_str_plain_image2 );
    const_str_plain_chop_darker = UNSTREAM_STRING_ASCII( &constant_bin[ 54843 ], 11, 1 );
    const_str_plain_chop_difference = UNSTREAM_STRING_ASCII( &constant_bin[ 54854 ], 15, 1 );
    const_str_digest_80ed5aa5d813a5989d5e2be433f0ecd8 = UNSTREAM_STRING_ASCII( &constant_bin[ 54869 ], 133, 0 );
    const_str_plain_chop_and = UNSTREAM_STRING_ASCII( &constant_bin[ 55002 ], 8, 1 );
    const_str_plain_chop_xor = UNSTREAM_STRING_ASCII( &constant_bin[ 55010 ], 8, 1 );
    const_str_plain_darker = UNSTREAM_STRING_ASCII( &constant_bin[ 53898 ], 6, 1 );
    const_str_digest_ba4056a05a85932c26437bcda2fecef3 = UNSTREAM_STRING_ASCII( &constant_bin[ 55018 ], 211, 0 );
    const_str_plain_chop_add_modulo = UNSTREAM_STRING_ASCII( &constant_bin[ 55229 ], 15, 1 );
    const_str_plain_chop_or = UNSTREAM_STRING_ASCII( &constant_bin[ 55244 ], 7, 1 );
    const_str_digest_37ac6f6e8d90b774baa26f6f389313fa = UNSTREAM_STRING_ASCII( &constant_bin[ 55251 ], 200, 0 );
    const_str_plain_chop_subtract_modulo = UNSTREAM_STRING_ASCII( &constant_bin[ 55451 ], 20, 1 );
    const_str_plain_lighter = UNSTREAM_STRING_ASCII( &constant_bin[ 52493 ], 7, 1 );
    const_tuple_str_plain_image1_str_plain_image2_str_plain_mask_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_str_plain_mask_tuple, 0, const_str_plain_image1 ); Py_INCREF( const_str_plain_image1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_str_plain_mask_tuple, 1, const_str_plain_image2 ); Py_INCREF( const_str_plain_image2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_str_plain_mask_tuple, 2, const_str_plain_mask ); Py_INCREF( const_str_plain_mask );
    const_str_plain_chop_subtract = UNSTREAM_STRING_ASCII( &constant_bin[ 55451 ], 13, 1 );
    const_str_plain_chop_add = UNSTREAM_STRING_ASCII( &constant_bin[ 55229 ], 8, 1 );
    const_str_digest_35d1aac12da8e690cf368dbb2e557b39 = UNSTREAM_STRING_ASCII( &constant_bin[ 55471 ], 216, 0 );
    const_str_digest_e6c4a2040bc5430895d5761bc5985e97 = UNSTREAM_STRING_ASCII( &constant_bin[ 55687 ], 361, 0 );
    const_tuple_str_plain_image1_str_plain_image2_str_plain_alpha_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_str_plain_alpha_tuple, 0, const_str_plain_image1 ); Py_INCREF( const_str_plain_image1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_str_plain_alpha_tuple, 1, const_str_plain_image2 ); Py_INCREF( const_str_plain_image2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_image1_str_plain_image2_str_plain_alpha_tuple, 2, const_str_plain_alpha ); Py_INCREF( const_str_plain_alpha );
    const_str_plain_chop_lighter = UNSTREAM_STRING_ASCII( &constant_bin[ 56048 ], 12, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_PIL$ImageChops( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_2fbb7b8194454550904d54f37fd7ff4c;
static PyCodeObject *codeobj_d468b75ac324cdae9f1e7acbb7763d50;
static PyCodeObject *codeobj_c96336b8557e652460e94c445d5f2f0a;
static PyCodeObject *codeobj_53f15a8bf8c601274b1aaabd8fd561a3;
static PyCodeObject *codeobj_96e17a40fcf8a6142f9402ef589d7997;
static PyCodeObject *codeobj_a80fd99dfa48772b65f9d449174b0001;
static PyCodeObject *codeobj_cb5c75815ee5beecbd694aea081b1ea6;
static PyCodeObject *codeobj_b823efcd8375fab800c0308b574e7806;
static PyCodeObject *codeobj_a9b2b98cbd7f0ad29169a301089c4c5a;
static PyCodeObject *codeobj_02a62e08d6194bcf003d6a9939b2d26e;
static PyCodeObject *codeobj_865cd7eb6d982ee89b140a1729cc9d11;
static PyCodeObject *codeobj_27d39bc7971b735a8df8347637e03683;
static PyCodeObject *codeobj_13b8b49414d26667850034df9bf052bf;
static PyCodeObject *codeobj_f5a5303a25614db1752f1665d0393002;
static PyCodeObject *codeobj_c2bed1ce2928f93bd8748a341ce97c16;
static PyCodeObject *codeobj_af286cc727298ea3ea0c46a871429bda;
static PyCodeObject *codeobj_ec23a4fe1d8b2d7050fa355ea79f8045;
static PyCodeObject *codeobj_64ac24dd5ea3e59fb92fdcb3535baaaf;
static PyCodeObject *codeobj_99e7727443b496c937b5dcba4a3a7973;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_6990f30b1cd867bf667e4913f55b47ba );
    codeobj_2fbb7b8194454550904d54f37fd7ff4c = MAKE_CODEOBJ( module_filename_obj, const_str_digest_c4114e5a91c7c6fb76bf628af1bb6603, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_d468b75ac324cdae9f1e7acbb7763d50 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_add, 142, const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c96336b8557e652460e94c445d5f2f0a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_add_modulo, 178, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_53f15a8bf8c601274b1aaabd8fd561a3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_blend, 258, const_tuple_str_plain_image1_str_plain_image2_str_plain_alpha_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_96e17a40fcf8a6142f9402ef589d7997 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_composite, 268, const_tuple_str_plain_image1_str_plain_image2_str_plain_mask_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a80fd99dfa48772b65f9d449174b0001 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_constant, 21, const_tuple_str_plain_image_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cb5c75815ee5beecbd694aea081b1ea6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_darker, 71, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b823efcd8375fab800c0308b574e7806 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_difference, 88, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a9b2b98cbd7f0ad29169a301089c4c5a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_duplicate, 30, const_tuple_str_plain_image_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_02a62e08d6194bcf003d6a9939b2d26e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_invert, 39, const_tuple_str_plain_image_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_865cd7eb6d982ee89b140a1729cc9d11 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_lighter, 54, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27d39bc7971b735a8df8347637e03683 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_logical_and, 210, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_13b8b49414d26667850034df9bf052bf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_logical_or, 226, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f5a5303a25614db1752f1665d0393002 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_logical_xor, 242, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c2bed1ce2928f93bd8748a341ce97c16 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_multiply, 105, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_af286cc727298ea3ea0c46a871429bda = MAKE_CODEOBJ( module_filename_obj, const_str_plain_offset, 278, const_tuple_str_plain_image_str_plain_xoffset_str_plain_yoffset_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ec23a4fe1d8b2d7050fa355ea79f8045 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_screen, 125, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_64ac24dd5ea3e59fb92fdcb3535baaaf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_subtract, 160, const_tuple_0c6a3415d70b5aa1e1d80eb3983b6941_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_99e7727443b496c937b5dcba4a3a7973 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_subtract_modulo, 194, const_tuple_str_plain_image1_str_plain_image2_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_10_subtract( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_11_add_modulo(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_12_subtract_modulo(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_13_logical_and(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_14_logical_or(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_15_logical_xor(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_16_blend(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_17_composite(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_18_offset( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_1_constant(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_2_duplicate(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_3_invert(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_4_lighter(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_5_darker(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_6_difference(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_7_multiply(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_8_screen(  );


static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_9_add( PyObject *defaults );


// The module function definitions.
static PyObject *impl_PIL$ImageChops$$$function_1_constant( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_a80fd99dfa48772b65f9d449174b0001;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a80fd99dfa48772b65f9d449174b0001 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a80fd99dfa48772b65f9d449174b0001, codeobj_a80fd99dfa48772b65f9d449174b0001, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_a80fd99dfa48772b65f9d449174b0001 = cache_frame_a80fd99dfa48772b65f9d449174b0001;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a80fd99dfa48772b65f9d449174b0001 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a80fd99dfa48772b65f9d449174b0001 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_plain_L;
        CHECK_OBJECT( par_image );
        tmp_source_name_2 = par_image;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_size );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_value );
        tmp_args_element_name_3 = par_value;
        frame_a80fd99dfa48772b65f9d449174b0001->m_frame.f_lineno = 27;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a80fd99dfa48772b65f9d449174b0001 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a80fd99dfa48772b65f9d449174b0001 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a80fd99dfa48772b65f9d449174b0001 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a80fd99dfa48772b65f9d449174b0001, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a80fd99dfa48772b65f9d449174b0001->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a80fd99dfa48772b65f9d449174b0001, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a80fd99dfa48772b65f9d449174b0001,
        type_description_1,
        par_image,
        par_value
    );


    // Release cached frame.
    if ( frame_a80fd99dfa48772b65f9d449174b0001 == cache_frame_a80fd99dfa48772b65f9d449174b0001 )
    {
        Py_DECREF( frame_a80fd99dfa48772b65f9d449174b0001 );
    }
    cache_frame_a80fd99dfa48772b65f9d449174b0001 = NULL;

    assertFrameObject( frame_a80fd99dfa48772b65f9d449174b0001 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_1_constant );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_1_constant );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_2_duplicate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a9b2b98cbd7f0ad29169a301089c4c5a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a9b2b98cbd7f0ad29169a301089c4c5a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a9b2b98cbd7f0ad29169a301089c4c5a, codeobj_a9b2b98cbd7f0ad29169a301089c4c5a, module_PIL$ImageChops, sizeof(void *) );
    frame_a9b2b98cbd7f0ad29169a301089c4c5a = cache_frame_a9b2b98cbd7f0ad29169a301089c4c5a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a9b2b98cbd7f0ad29169a301089c4c5a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a9b2b98cbd7f0ad29169a301089c4c5a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_image );
        tmp_called_instance_1 = par_image;
        frame_a9b2b98cbd7f0ad29169a301089c4c5a->m_frame.f_lineno = 36;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_copy );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9b2b98cbd7f0ad29169a301089c4c5a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9b2b98cbd7f0ad29169a301089c4c5a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a9b2b98cbd7f0ad29169a301089c4c5a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a9b2b98cbd7f0ad29169a301089c4c5a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a9b2b98cbd7f0ad29169a301089c4c5a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a9b2b98cbd7f0ad29169a301089c4c5a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a9b2b98cbd7f0ad29169a301089c4c5a,
        type_description_1,
        par_image
    );


    // Release cached frame.
    if ( frame_a9b2b98cbd7f0ad29169a301089c4c5a == cache_frame_a9b2b98cbd7f0ad29169a301089c4c5a )
    {
        Py_DECREF( frame_a9b2b98cbd7f0ad29169a301089c4c5a );
    }
    cache_frame_a9b2b98cbd7f0ad29169a301089c4c5a = NULL;

    assertFrameObject( frame_a9b2b98cbd7f0ad29169a301089c4c5a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_2_duplicate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_2_duplicate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_3_invert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_02a62e08d6194bcf003d6a9939b2d26e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_02a62e08d6194bcf003d6a9939b2d26e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_02a62e08d6194bcf003d6a9939b2d26e, codeobj_02a62e08d6194bcf003d6a9939b2d26e, module_PIL$ImageChops, sizeof(void *) );
    frame_02a62e08d6194bcf003d6a9939b2d26e = cache_frame_02a62e08d6194bcf003d6a9939b2d26e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_02a62e08d6194bcf003d6a9939b2d26e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_02a62e08d6194bcf003d6a9939b2d26e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image );
        tmp_called_instance_1 = par_image;
        frame_02a62e08d6194bcf003d6a9939b2d26e->m_frame.f_lineno = 50;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_image );
        tmp_source_name_1 = par_image;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image );
        tmp_source_name_2 = par_image;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_im );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_02a62e08d6194bcf003d6a9939b2d26e->m_frame.f_lineno = 51;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_chop_invert );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_02a62e08d6194bcf003d6a9939b2d26e->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02a62e08d6194bcf003d6a9939b2d26e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_02a62e08d6194bcf003d6a9939b2d26e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02a62e08d6194bcf003d6a9939b2d26e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_02a62e08d6194bcf003d6a9939b2d26e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_02a62e08d6194bcf003d6a9939b2d26e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_02a62e08d6194bcf003d6a9939b2d26e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_02a62e08d6194bcf003d6a9939b2d26e,
        type_description_1,
        par_image
    );


    // Release cached frame.
    if ( frame_02a62e08d6194bcf003d6a9939b2d26e == cache_frame_02a62e08d6194bcf003d6a9939b2d26e )
    {
        Py_DECREF( frame_02a62e08d6194bcf003d6a9939b2d26e );
    }
    cache_frame_02a62e08d6194bcf003d6a9939b2d26e = NULL;

    assertFrameObject( frame_02a62e08d6194bcf003d6a9939b2d26e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_3_invert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_3_invert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_4_lighter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_865cd7eb6d982ee89b140a1729cc9d11;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_865cd7eb6d982ee89b140a1729cc9d11 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_865cd7eb6d982ee89b140a1729cc9d11, codeobj_865cd7eb6d982ee89b140a1729cc9d11, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_865cd7eb6d982ee89b140a1729cc9d11 = cache_frame_865cd7eb6d982ee89b140a1729cc9d11;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_865cd7eb6d982ee89b140a1729cc9d11 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_865cd7eb6d982ee89b140a1729cc9d11 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_865cd7eb6d982ee89b140a1729cc9d11->m_frame.f_lineno = 66;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_865cd7eb6d982ee89b140a1729cc9d11->m_frame.f_lineno = 67;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_lighter );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_865cd7eb6d982ee89b140a1729cc9d11->m_frame.f_lineno = 68;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_865cd7eb6d982ee89b140a1729cc9d11->m_frame.f_lineno = 68;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_865cd7eb6d982ee89b140a1729cc9d11 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_865cd7eb6d982ee89b140a1729cc9d11 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_865cd7eb6d982ee89b140a1729cc9d11 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_865cd7eb6d982ee89b140a1729cc9d11, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_865cd7eb6d982ee89b140a1729cc9d11->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_865cd7eb6d982ee89b140a1729cc9d11, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_865cd7eb6d982ee89b140a1729cc9d11,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_865cd7eb6d982ee89b140a1729cc9d11 == cache_frame_865cd7eb6d982ee89b140a1729cc9d11 )
    {
        Py_DECREF( frame_865cd7eb6d982ee89b140a1729cc9d11 );
    }
    cache_frame_865cd7eb6d982ee89b140a1729cc9d11 = NULL;

    assertFrameObject( frame_865cd7eb6d982ee89b140a1729cc9d11 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_4_lighter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_4_lighter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_5_darker( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_cb5c75815ee5beecbd694aea081b1ea6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_cb5c75815ee5beecbd694aea081b1ea6 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cb5c75815ee5beecbd694aea081b1ea6, codeobj_cb5c75815ee5beecbd694aea081b1ea6, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_cb5c75815ee5beecbd694aea081b1ea6 = cache_frame_cb5c75815ee5beecbd694aea081b1ea6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cb5c75815ee5beecbd694aea081b1ea6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cb5c75815ee5beecbd694aea081b1ea6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_cb5c75815ee5beecbd694aea081b1ea6->m_frame.f_lineno = 83;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_cb5c75815ee5beecbd694aea081b1ea6->m_frame.f_lineno = 84;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 85;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_darker );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 85;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 85;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_cb5c75815ee5beecbd694aea081b1ea6->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 85;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_cb5c75815ee5beecbd694aea081b1ea6->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb5c75815ee5beecbd694aea081b1ea6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb5c75815ee5beecbd694aea081b1ea6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb5c75815ee5beecbd694aea081b1ea6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cb5c75815ee5beecbd694aea081b1ea6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cb5c75815ee5beecbd694aea081b1ea6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cb5c75815ee5beecbd694aea081b1ea6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cb5c75815ee5beecbd694aea081b1ea6,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_cb5c75815ee5beecbd694aea081b1ea6 == cache_frame_cb5c75815ee5beecbd694aea081b1ea6 )
    {
        Py_DECREF( frame_cb5c75815ee5beecbd694aea081b1ea6 );
    }
    cache_frame_cb5c75815ee5beecbd694aea081b1ea6 = NULL;

    assertFrameObject( frame_cb5c75815ee5beecbd694aea081b1ea6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_5_darker );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_5_darker );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_6_difference( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_b823efcd8375fab800c0308b574e7806;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_b823efcd8375fab800c0308b574e7806 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b823efcd8375fab800c0308b574e7806, codeobj_b823efcd8375fab800c0308b574e7806, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_b823efcd8375fab800c0308b574e7806 = cache_frame_b823efcd8375fab800c0308b574e7806;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b823efcd8375fab800c0308b574e7806 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b823efcd8375fab800c0308b574e7806 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_b823efcd8375fab800c0308b574e7806->m_frame.f_lineno = 100;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_b823efcd8375fab800c0308b574e7806->m_frame.f_lineno = 101;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_difference );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_b823efcd8375fab800c0308b574e7806->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_b823efcd8375fab800c0308b574e7806->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b823efcd8375fab800c0308b574e7806 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b823efcd8375fab800c0308b574e7806 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b823efcd8375fab800c0308b574e7806 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b823efcd8375fab800c0308b574e7806, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b823efcd8375fab800c0308b574e7806->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b823efcd8375fab800c0308b574e7806, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b823efcd8375fab800c0308b574e7806,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_b823efcd8375fab800c0308b574e7806 == cache_frame_b823efcd8375fab800c0308b574e7806 )
    {
        Py_DECREF( frame_b823efcd8375fab800c0308b574e7806 );
    }
    cache_frame_b823efcd8375fab800c0308b574e7806 = NULL;

    assertFrameObject( frame_b823efcd8375fab800c0308b574e7806 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_6_difference );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_6_difference );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_7_multiply( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_c2bed1ce2928f93bd8748a341ce97c16;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_c2bed1ce2928f93bd8748a341ce97c16 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c2bed1ce2928f93bd8748a341ce97c16, codeobj_c2bed1ce2928f93bd8748a341ce97c16, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_c2bed1ce2928f93bd8748a341ce97c16 = cache_frame_c2bed1ce2928f93bd8748a341ce97c16;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c2bed1ce2928f93bd8748a341ce97c16 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c2bed1ce2928f93bd8748a341ce97c16 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_c2bed1ce2928f93bd8748a341ce97c16->m_frame.f_lineno = 120;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_c2bed1ce2928f93bd8748a341ce97c16->m_frame.f_lineno = 121;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 122;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_multiply );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 122;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 122;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_c2bed1ce2928f93bd8748a341ce97c16->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 122;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_c2bed1ce2928f93bd8748a341ce97c16->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c2bed1ce2928f93bd8748a341ce97c16 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c2bed1ce2928f93bd8748a341ce97c16 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c2bed1ce2928f93bd8748a341ce97c16 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c2bed1ce2928f93bd8748a341ce97c16, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c2bed1ce2928f93bd8748a341ce97c16->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c2bed1ce2928f93bd8748a341ce97c16, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c2bed1ce2928f93bd8748a341ce97c16,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_c2bed1ce2928f93bd8748a341ce97c16 == cache_frame_c2bed1ce2928f93bd8748a341ce97c16 )
    {
        Py_DECREF( frame_c2bed1ce2928f93bd8748a341ce97c16 );
    }
    cache_frame_c2bed1ce2928f93bd8748a341ce97c16 = NULL;

    assertFrameObject( frame_c2bed1ce2928f93bd8748a341ce97c16 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_7_multiply );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_7_multiply );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_8_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_ec23a4fe1d8b2d7050fa355ea79f8045;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_ec23a4fe1d8b2d7050fa355ea79f8045 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec23a4fe1d8b2d7050fa355ea79f8045, codeobj_ec23a4fe1d8b2d7050fa355ea79f8045, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_ec23a4fe1d8b2d7050fa355ea79f8045 = cache_frame_ec23a4fe1d8b2d7050fa355ea79f8045;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec23a4fe1d8b2d7050fa355ea79f8045 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec23a4fe1d8b2d7050fa355ea79f8045 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_ec23a4fe1d8b2d7050fa355ea79f8045->m_frame.f_lineno = 137;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_ec23a4fe1d8b2d7050fa355ea79f8045->m_frame.f_lineno = 138;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_screen );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_ec23a4fe1d8b2d7050fa355ea79f8045->m_frame.f_lineno = 139;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_ec23a4fe1d8b2d7050fa355ea79f8045->m_frame.f_lineno = 139;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec23a4fe1d8b2d7050fa355ea79f8045 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec23a4fe1d8b2d7050fa355ea79f8045 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec23a4fe1d8b2d7050fa355ea79f8045 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec23a4fe1d8b2d7050fa355ea79f8045, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec23a4fe1d8b2d7050fa355ea79f8045->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec23a4fe1d8b2d7050fa355ea79f8045, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec23a4fe1d8b2d7050fa355ea79f8045,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_ec23a4fe1d8b2d7050fa355ea79f8045 == cache_frame_ec23a4fe1d8b2d7050fa355ea79f8045 )
    {
        Py_DECREF( frame_ec23a4fe1d8b2d7050fa355ea79f8045 );
    }
    cache_frame_ec23a4fe1d8b2d7050fa355ea79f8045 = NULL;

    assertFrameObject( frame_ec23a4fe1d8b2d7050fa355ea79f8045 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_8_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_8_screen );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_9_add( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    PyObject *par_scale = python_pars[ 2 ];
    PyObject *par_offset = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_d468b75ac324cdae9f1e7acbb7763d50;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d468b75ac324cdae9f1e7acbb7763d50 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d468b75ac324cdae9f1e7acbb7763d50, codeobj_d468b75ac324cdae9f1e7acbb7763d50, module_PIL$ImageChops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d468b75ac324cdae9f1e7acbb7763d50 = cache_frame_d468b75ac324cdae9f1e7acbb7763d50;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d468b75ac324cdae9f1e7acbb7763d50 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d468b75ac324cdae9f1e7acbb7763d50 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_d468b75ac324cdae9f1e7acbb7763d50->m_frame.f_lineno = 155;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_d468b75ac324cdae9f1e7acbb7763d50->m_frame.f_lineno = 156;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_add );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_scale );
        tmp_args_element_name_3 = par_scale;
        CHECK_OBJECT( par_offset );
        tmp_args_element_name_4 = par_offset;
        frame_d468b75ac324cdae9f1e7acbb7763d50->m_frame.f_lineno = 157;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_d468b75ac324cdae9f1e7acbb7763d50->m_frame.f_lineno = 157;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d468b75ac324cdae9f1e7acbb7763d50 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d468b75ac324cdae9f1e7acbb7763d50 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d468b75ac324cdae9f1e7acbb7763d50 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d468b75ac324cdae9f1e7acbb7763d50, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d468b75ac324cdae9f1e7acbb7763d50->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d468b75ac324cdae9f1e7acbb7763d50, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d468b75ac324cdae9f1e7acbb7763d50,
        type_description_1,
        par_image1,
        par_image2,
        par_scale,
        par_offset
    );


    // Release cached frame.
    if ( frame_d468b75ac324cdae9f1e7acbb7763d50 == cache_frame_d468b75ac324cdae9f1e7acbb7763d50 )
    {
        Py_DECREF( frame_d468b75ac324cdae9f1e7acbb7763d50 );
    }
    cache_frame_d468b75ac324cdae9f1e7acbb7763d50 = NULL;

    assertFrameObject( frame_d468b75ac324cdae9f1e7acbb7763d50 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_9_add );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_scale );
    Py_DECREF( par_scale );
    par_scale = NULL;

    CHECK_OBJECT( (PyObject *)par_offset );
    Py_DECREF( par_offset );
    par_offset = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_scale );
    Py_DECREF( par_scale );
    par_scale = NULL;

    CHECK_OBJECT( (PyObject *)par_offset );
    Py_DECREF( par_offset );
    par_offset = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_9_add );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_10_subtract( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    PyObject *par_scale = python_pars[ 2 ];
    PyObject *par_offset = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_64ac24dd5ea3e59fb92fdcb3535baaaf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_64ac24dd5ea3e59fb92fdcb3535baaaf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_64ac24dd5ea3e59fb92fdcb3535baaaf, codeobj_64ac24dd5ea3e59fb92fdcb3535baaaf, module_PIL$ImageChops, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_64ac24dd5ea3e59fb92fdcb3535baaaf = cache_frame_64ac24dd5ea3e59fb92fdcb3535baaaf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_64ac24dd5ea3e59fb92fdcb3535baaaf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_64ac24dd5ea3e59fb92fdcb3535baaaf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_64ac24dd5ea3e59fb92fdcb3535baaaf->m_frame.f_lineno = 173;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_64ac24dd5ea3e59fb92fdcb3535baaaf->m_frame.f_lineno = 174;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_subtract );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_scale );
        tmp_args_element_name_3 = par_scale;
        CHECK_OBJECT( par_offset );
        tmp_args_element_name_4 = par_offset;
        frame_64ac24dd5ea3e59fb92fdcb3535baaaf->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_64ac24dd5ea3e59fb92fdcb3535baaaf->m_frame.f_lineno = 175;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64ac24dd5ea3e59fb92fdcb3535baaaf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_64ac24dd5ea3e59fb92fdcb3535baaaf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_64ac24dd5ea3e59fb92fdcb3535baaaf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_64ac24dd5ea3e59fb92fdcb3535baaaf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_64ac24dd5ea3e59fb92fdcb3535baaaf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_64ac24dd5ea3e59fb92fdcb3535baaaf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_64ac24dd5ea3e59fb92fdcb3535baaaf,
        type_description_1,
        par_image1,
        par_image2,
        par_scale,
        par_offset
    );


    // Release cached frame.
    if ( frame_64ac24dd5ea3e59fb92fdcb3535baaaf == cache_frame_64ac24dd5ea3e59fb92fdcb3535baaaf )
    {
        Py_DECREF( frame_64ac24dd5ea3e59fb92fdcb3535baaaf );
    }
    cache_frame_64ac24dd5ea3e59fb92fdcb3535baaaf = NULL;

    assertFrameObject( frame_64ac24dd5ea3e59fb92fdcb3535baaaf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_10_subtract );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_scale );
    Py_DECREF( par_scale );
    par_scale = NULL;

    CHECK_OBJECT( (PyObject *)par_offset );
    Py_DECREF( par_offset );
    par_offset = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_scale );
    Py_DECREF( par_scale );
    par_scale = NULL;

    CHECK_OBJECT( (PyObject *)par_offset );
    Py_DECREF( par_offset );
    par_offset = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_10_subtract );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_11_add_modulo( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_c96336b8557e652460e94c445d5f2f0a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_c96336b8557e652460e94c445d5f2f0a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c96336b8557e652460e94c445d5f2f0a, codeobj_c96336b8557e652460e94c445d5f2f0a, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_c96336b8557e652460e94c445d5f2f0a = cache_frame_c96336b8557e652460e94c445d5f2f0a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c96336b8557e652460e94c445d5f2f0a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c96336b8557e652460e94c445d5f2f0a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_c96336b8557e652460e94c445d5f2f0a->m_frame.f_lineno = 189;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_c96336b8557e652460e94c445d5f2f0a->m_frame.f_lineno = 190;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_add_modulo );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_c96336b8557e652460e94c445d5f2f0a->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_c96336b8557e652460e94c445d5f2f0a->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c96336b8557e652460e94c445d5f2f0a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c96336b8557e652460e94c445d5f2f0a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c96336b8557e652460e94c445d5f2f0a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c96336b8557e652460e94c445d5f2f0a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c96336b8557e652460e94c445d5f2f0a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c96336b8557e652460e94c445d5f2f0a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c96336b8557e652460e94c445d5f2f0a,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_c96336b8557e652460e94c445d5f2f0a == cache_frame_c96336b8557e652460e94c445d5f2f0a )
    {
        Py_DECREF( frame_c96336b8557e652460e94c445d5f2f0a );
    }
    cache_frame_c96336b8557e652460e94c445d5f2f0a = NULL;

    assertFrameObject( frame_c96336b8557e652460e94c445d5f2f0a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_11_add_modulo );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_11_add_modulo );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_12_subtract_modulo( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_99e7727443b496c937b5dcba4a3a7973;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_99e7727443b496c937b5dcba4a3a7973 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_99e7727443b496c937b5dcba4a3a7973, codeobj_99e7727443b496c937b5dcba4a3a7973, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_99e7727443b496c937b5dcba4a3a7973 = cache_frame_99e7727443b496c937b5dcba4a3a7973;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_99e7727443b496c937b5dcba4a3a7973 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_99e7727443b496c937b5dcba4a3a7973 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_99e7727443b496c937b5dcba4a3a7973->m_frame.f_lineno = 205;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_99e7727443b496c937b5dcba4a3a7973->m_frame.f_lineno = 206;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 206;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_subtract_modulo );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_99e7727443b496c937b5dcba4a3a7973->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_99e7727443b496c937b5dcba4a3a7973->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_99e7727443b496c937b5dcba4a3a7973 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_99e7727443b496c937b5dcba4a3a7973 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_99e7727443b496c937b5dcba4a3a7973 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_99e7727443b496c937b5dcba4a3a7973, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_99e7727443b496c937b5dcba4a3a7973->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_99e7727443b496c937b5dcba4a3a7973, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_99e7727443b496c937b5dcba4a3a7973,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_99e7727443b496c937b5dcba4a3a7973 == cache_frame_99e7727443b496c937b5dcba4a3a7973 )
    {
        Py_DECREF( frame_99e7727443b496c937b5dcba4a3a7973 );
    }
    cache_frame_99e7727443b496c937b5dcba4a3a7973 = NULL;

    assertFrameObject( frame_99e7727443b496c937b5dcba4a3a7973 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_12_subtract_modulo );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_12_subtract_modulo );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_13_logical_and( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_27d39bc7971b735a8df8347637e03683;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_27d39bc7971b735a8df8347637e03683 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_27d39bc7971b735a8df8347637e03683, codeobj_27d39bc7971b735a8df8347637e03683, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_27d39bc7971b735a8df8347637e03683 = cache_frame_27d39bc7971b735a8df8347637e03683;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_27d39bc7971b735a8df8347637e03683 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_27d39bc7971b735a8df8347637e03683 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_27d39bc7971b735a8df8347637e03683->m_frame.f_lineno = 221;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 221;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_27d39bc7971b735a8df8347637e03683->m_frame.f_lineno = 222;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 223;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_and );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 223;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 223;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_27d39bc7971b735a8df8347637e03683->m_frame.f_lineno = 223;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 223;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_27d39bc7971b735a8df8347637e03683->m_frame.f_lineno = 223;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 223;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27d39bc7971b735a8df8347637e03683 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_27d39bc7971b735a8df8347637e03683 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27d39bc7971b735a8df8347637e03683 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_27d39bc7971b735a8df8347637e03683, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_27d39bc7971b735a8df8347637e03683->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_27d39bc7971b735a8df8347637e03683, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_27d39bc7971b735a8df8347637e03683,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_27d39bc7971b735a8df8347637e03683 == cache_frame_27d39bc7971b735a8df8347637e03683 )
    {
        Py_DECREF( frame_27d39bc7971b735a8df8347637e03683 );
    }
    cache_frame_27d39bc7971b735a8df8347637e03683 = NULL;

    assertFrameObject( frame_27d39bc7971b735a8df8347637e03683 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_13_logical_and );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_13_logical_and );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_14_logical_or( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_13b8b49414d26667850034df9bf052bf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_13b8b49414d26667850034df9bf052bf = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_13b8b49414d26667850034df9bf052bf, codeobj_13b8b49414d26667850034df9bf052bf, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_13b8b49414d26667850034df9bf052bf = cache_frame_13b8b49414d26667850034df9bf052bf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_13b8b49414d26667850034df9bf052bf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_13b8b49414d26667850034df9bf052bf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_13b8b49414d26667850034df9bf052bf->m_frame.f_lineno = 237;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 237;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_13b8b49414d26667850034df9bf052bf->m_frame.f_lineno = 238;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_or );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_13b8b49414d26667850034df9bf052bf->m_frame.f_lineno = 239;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_13b8b49414d26667850034df9bf052bf->m_frame.f_lineno = 239;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_13b8b49414d26667850034df9bf052bf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_13b8b49414d26667850034df9bf052bf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_13b8b49414d26667850034df9bf052bf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_13b8b49414d26667850034df9bf052bf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_13b8b49414d26667850034df9bf052bf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_13b8b49414d26667850034df9bf052bf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_13b8b49414d26667850034df9bf052bf,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_13b8b49414d26667850034df9bf052bf == cache_frame_13b8b49414d26667850034df9bf052bf )
    {
        Py_DECREF( frame_13b8b49414d26667850034df9bf052bf );
    }
    cache_frame_13b8b49414d26667850034df9bf052bf = NULL;

    assertFrameObject( frame_13b8b49414d26667850034df9bf052bf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_14_logical_or );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_14_logical_or );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_15_logical_xor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_f5a5303a25614db1752f1665d0393002;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f5a5303a25614db1752f1665d0393002 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f5a5303a25614db1752f1665d0393002, codeobj_f5a5303a25614db1752f1665d0393002, module_PIL$ImageChops, sizeof(void *)+sizeof(void *) );
    frame_f5a5303a25614db1752f1665d0393002 = cache_frame_f5a5303a25614db1752f1665d0393002;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f5a5303a25614db1752f1665d0393002 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f5a5303a25614db1752f1665d0393002 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image1 );
        tmp_called_instance_1 = par_image1;
        frame_f5a5303a25614db1752f1665d0393002->m_frame.f_lineno = 253;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_2;
        CHECK_OBJECT( par_image2 );
        tmp_called_instance_2 = par_image2;
        frame_f5a5303a25614db1752f1665d0393002->m_frame.f_lineno = 254;
        tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_load );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_image1 );
        tmp_source_name_1 = par_image1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image1 );
        tmp_source_name_3 = par_image1;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_im );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_chop_xor );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image2 );
        tmp_source_name_4 = par_image2;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_im );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_f5a5303a25614db1752f1665d0393002->m_frame.f_lineno = 255;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_f5a5303a25614db1752f1665d0393002->m_frame.f_lineno = 255;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5a5303a25614db1752f1665d0393002 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5a5303a25614db1752f1665d0393002 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5a5303a25614db1752f1665d0393002 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f5a5303a25614db1752f1665d0393002, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f5a5303a25614db1752f1665d0393002->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f5a5303a25614db1752f1665d0393002, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f5a5303a25614db1752f1665d0393002,
        type_description_1,
        par_image1,
        par_image2
    );


    // Release cached frame.
    if ( frame_f5a5303a25614db1752f1665d0393002 == cache_frame_f5a5303a25614db1752f1665d0393002 )
    {
        Py_DECREF( frame_f5a5303a25614db1752f1665d0393002 );
    }
    cache_frame_f5a5303a25614db1752f1665d0393002 = NULL;

    assertFrameObject( frame_f5a5303a25614db1752f1665d0393002 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_15_logical_xor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_15_logical_xor );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_16_blend( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    PyObject *par_alpha = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_53f15a8bf8c601274b1aaabd8fd561a3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_53f15a8bf8c601274b1aaabd8fd561a3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_53f15a8bf8c601274b1aaabd8fd561a3, codeobj_53f15a8bf8c601274b1aaabd8fd561a3, module_PIL$ImageChops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_53f15a8bf8c601274b1aaabd8fd561a3 = cache_frame_53f15a8bf8c601274b1aaabd8fd561a3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_53f15a8bf8c601274b1aaabd8fd561a3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_53f15a8bf8c601274b1aaabd8fd561a3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 265;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_image1 );
        tmp_args_element_name_1 = par_image1;
        CHECK_OBJECT( par_image2 );
        tmp_args_element_name_2 = par_image2;
        CHECK_OBJECT( par_alpha );
        tmp_args_element_name_3 = par_alpha;
        frame_53f15a8bf8c601274b1aaabd8fd561a3->m_frame.f_lineno = 265;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_blend, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 265;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53f15a8bf8c601274b1aaabd8fd561a3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_53f15a8bf8c601274b1aaabd8fd561a3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_53f15a8bf8c601274b1aaabd8fd561a3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_53f15a8bf8c601274b1aaabd8fd561a3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_53f15a8bf8c601274b1aaabd8fd561a3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_53f15a8bf8c601274b1aaabd8fd561a3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_53f15a8bf8c601274b1aaabd8fd561a3,
        type_description_1,
        par_image1,
        par_image2,
        par_alpha
    );


    // Release cached frame.
    if ( frame_53f15a8bf8c601274b1aaabd8fd561a3 == cache_frame_53f15a8bf8c601274b1aaabd8fd561a3 )
    {
        Py_DECREF( frame_53f15a8bf8c601274b1aaabd8fd561a3 );
    }
    cache_frame_53f15a8bf8c601274b1aaabd8fd561a3 = NULL;

    assertFrameObject( frame_53f15a8bf8c601274b1aaabd8fd561a3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_16_blend );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_alpha );
    Py_DECREF( par_alpha );
    par_alpha = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_alpha );
    Py_DECREF( par_alpha );
    par_alpha = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_16_blend );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_17_composite( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image1 = python_pars[ 0 ];
    PyObject *par_image2 = python_pars[ 1 ];
    PyObject *par_mask = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_96e17a40fcf8a6142f9402ef589d7997;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_96e17a40fcf8a6142f9402ef589d7997 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_96e17a40fcf8a6142f9402ef589d7997, codeobj_96e17a40fcf8a6142f9402ef589d7997, module_PIL$ImageChops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_96e17a40fcf8a6142f9402ef589d7997 = cache_frame_96e17a40fcf8a6142f9402ef589d7997;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_96e17a40fcf8a6142f9402ef589d7997 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_96e17a40fcf8a6142f9402ef589d7997 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 275;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_image1 );
        tmp_args_element_name_1 = par_image1;
        CHECK_OBJECT( par_image2 );
        tmp_args_element_name_2 = par_image2;
        CHECK_OBJECT( par_mask );
        tmp_args_element_name_3 = par_mask;
        frame_96e17a40fcf8a6142f9402ef589d7997->m_frame.f_lineno = 275;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS3( tmp_called_instance_1, const_str_plain_composite, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96e17a40fcf8a6142f9402ef589d7997 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_96e17a40fcf8a6142f9402ef589d7997 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96e17a40fcf8a6142f9402ef589d7997 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_96e17a40fcf8a6142f9402ef589d7997, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_96e17a40fcf8a6142f9402ef589d7997->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_96e17a40fcf8a6142f9402ef589d7997, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_96e17a40fcf8a6142f9402ef589d7997,
        type_description_1,
        par_image1,
        par_image2,
        par_mask
    );


    // Release cached frame.
    if ( frame_96e17a40fcf8a6142f9402ef589d7997 == cache_frame_96e17a40fcf8a6142f9402ef589d7997 )
    {
        Py_DECREF( frame_96e17a40fcf8a6142f9402ef589d7997 );
    }
    cache_frame_96e17a40fcf8a6142f9402ef589d7997 = NULL;

    assertFrameObject( frame_96e17a40fcf8a6142f9402ef589d7997 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_17_composite );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_mask );
    Py_DECREF( par_mask );
    par_mask = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image1 );
    Py_DECREF( par_image1 );
    par_image1 = NULL;

    CHECK_OBJECT( (PyObject *)par_image2 );
    Py_DECREF( par_image2 );
    par_image2 = NULL;

    CHECK_OBJECT( (PyObject *)par_mask );
    Py_DECREF( par_mask );
    par_mask = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_17_composite );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$ImageChops$$$function_18_offset( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_image = python_pars[ 0 ];
    PyObject *par_xoffset = python_pars[ 1 ];
    PyObject *par_yoffset = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_af286cc727298ea3ea0c46a871429bda;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_af286cc727298ea3ea0c46a871429bda = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_yoffset );
        tmp_compexpr_left_1 = par_yoffset;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            CHECK_OBJECT( par_xoffset );
            tmp_assign_source_1 = par_xoffset;
            {
                PyObject *old = par_yoffset;
                assert( old != NULL );
                par_yoffset = tmp_assign_source_1;
                Py_INCREF( par_yoffset );
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_af286cc727298ea3ea0c46a871429bda, codeobj_af286cc727298ea3ea0c46a871429bda, module_PIL$ImageChops, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_af286cc727298ea3ea0c46a871429bda = cache_frame_af286cc727298ea3ea0c46a871429bda;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_af286cc727298ea3ea0c46a871429bda );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_af286cc727298ea3ea0c46a871429bda ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_image );
        tmp_called_instance_1 = par_image;
        frame_af286cc727298ea3ea0c46a871429bda->m_frame.f_lineno = 291;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_load );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 291;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_image );
        tmp_source_name_1 = par_image;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__new );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_image );
        tmp_source_name_2 = par_image;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_im );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 292;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_xoffset );
        tmp_args_element_name_2 = par_xoffset;
        CHECK_OBJECT( par_yoffset );
        tmp_args_element_name_3 = par_yoffset;
        frame_af286cc727298ea3ea0c46a871429bda->m_frame.f_lineno = 292;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_args_element_name_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_offset, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 292;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_af286cc727298ea3ea0c46a871429bda->m_frame.f_lineno = 292;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af286cc727298ea3ea0c46a871429bda );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_af286cc727298ea3ea0c46a871429bda );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af286cc727298ea3ea0c46a871429bda );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_af286cc727298ea3ea0c46a871429bda, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_af286cc727298ea3ea0c46a871429bda->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_af286cc727298ea3ea0c46a871429bda, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_af286cc727298ea3ea0c46a871429bda,
        type_description_1,
        par_image,
        par_xoffset,
        par_yoffset
    );


    // Release cached frame.
    if ( frame_af286cc727298ea3ea0c46a871429bda == cache_frame_af286cc727298ea3ea0c46a871429bda )
    {
        Py_DECREF( frame_af286cc727298ea3ea0c46a871429bda );
    }
    cache_frame_af286cc727298ea3ea0c46a871429bda = NULL;

    assertFrameObject( frame_af286cc727298ea3ea0c46a871429bda );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_18_offset );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_xoffset );
    Py_DECREF( par_xoffset );
    par_xoffset = NULL;

    CHECK_OBJECT( (PyObject *)par_yoffset );
    Py_DECREF( par_yoffset );
    par_yoffset = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_image );
    Py_DECREF( par_image );
    par_image = NULL;

    CHECK_OBJECT( (PyObject *)par_xoffset );
    Py_DECREF( par_xoffset );
    par_xoffset = NULL;

    CHECK_OBJECT( (PyObject *)par_yoffset );
    Py_DECREF( par_yoffset );
    par_yoffset = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$ImageChops$$$function_18_offset );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_10_subtract( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_10_subtract,
        const_str_plain_subtract,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_64ac24dd5ea3e59fb92fdcb3535baaaf,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_18b038d615cf2881bb3b9e83332935d1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_11_add_modulo(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_11_add_modulo,
        const_str_plain_add_modulo,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c96336b8557e652460e94c445d5f2f0a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_ba4056a05a85932c26437bcda2fecef3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_12_subtract_modulo(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_12_subtract_modulo,
        const_str_plain_subtract_modulo,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_99e7727443b496c937b5dcba4a3a7973,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_35d1aac12da8e690cf368dbb2e557b39,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_13_logical_and(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_13_logical_and,
        const_str_plain_logical_and,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_27d39bc7971b735a8df8347637e03683,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_37ac6f6e8d90b774baa26f6f389313fa,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_14_logical_or(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_14_logical_or,
        const_str_plain_logical_or,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_13b8b49414d26667850034df9bf052bf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_fc9d3e28e9cdcadba0fba9dfa8980efc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_15_logical_xor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_15_logical_xor,
        const_str_plain_logical_xor,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f5a5303a25614db1752f1665d0393002,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_28d6326370ff3344dafef726a6138b01,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_16_blend(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_16_blend,
        const_str_plain_blend,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_53f15a8bf8c601274b1aaabd8fd561a3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_330931758f0ece41f45b97f63bd1d3cc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_17_composite(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_17_composite,
        const_str_plain_composite,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_96e17a40fcf8a6142f9402ef589d7997,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_b42909581e7a9e471d384490ea7d49d8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_18_offset( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_18_offset,
        const_str_plain_offset,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_af286cc727298ea3ea0c46a871429bda,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_da0dca45318b0e88a977cc534db2c085,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_1_constant(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_1_constant,
        const_str_plain_constant,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a80fd99dfa48772b65f9d449174b0001,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_75d3ca04904dd8d83bb88c84b3351182,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_2_duplicate(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_2_duplicate,
        const_str_plain_duplicate,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a9b2b98cbd7f0ad29169a301089c4c5a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_3e0519114973f2ac39322526e664803d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_3_invert(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_3_invert,
        const_str_plain_invert,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_02a62e08d6194bcf003d6a9939b2d26e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_80ed5aa5d813a5989d5e2be433f0ecd8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_4_lighter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_4_lighter,
        const_str_plain_lighter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_865cd7eb6d982ee89b140a1729cc9d11,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_9a5d03d9befb6e87d4d1ca27abfd50a8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_5_darker(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_5_darker,
        const_str_plain_darker,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cb5c75815ee5beecbd694aea081b1ea6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_f07efc180a7edc734c9cc6da7ea485e5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_6_difference(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_6_difference,
        const_str_plain_difference,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b823efcd8375fab800c0308b574e7806,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_b286e616f5af2c021e109847f78adb5b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_7_multiply(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_7_multiply,
        const_str_plain_multiply,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c2bed1ce2928f93bd8748a341ce97c16,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_e6c4a2040bc5430895d5761bc5985e97,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_8_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_8_screen,
        const_str_plain_screen,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ec23a4fe1d8b2d7050fa355ea79f8045,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_c2567550310528637a128acfb6c5b84a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$ImageChops$$$function_9_add( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$ImageChops$$$function_9_add,
        const_str_plain_add,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d468b75ac324cdae9f1e7acbb7763d50,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$ImageChops,
        const_str_digest_d5f99763a11d6c1c0de11ca998b46f2c,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_PIL$ImageChops =
{
    PyModuleDef_HEAD_INIT,
    "PIL.ImageChops",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(PIL$ImageChops)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(PIL$ImageChops)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_PIL$ImageChops );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("PIL.ImageChops: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.ImageChops: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.ImageChops: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initPIL$ImageChops" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_PIL$ImageChops = Py_InitModule4(
        "PIL.ImageChops",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_PIL$ImageChops = PyModule_Create( &mdef_PIL$ImageChops );
#endif

    moduledict_PIL$ImageChops = MODULE_DICT( module_PIL$ImageChops );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_PIL$ImageChops,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_PIL$ImageChops,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$ImageChops,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$ImageChops,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_PIL$ImageChops );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_2366abc32a7a698ab8b1f922bf783013, module_PIL$ImageChops );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_2fbb7b8194454550904d54f37fd7ff4c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_2fbb7b8194454550904d54f37fd7ff4c = MAKE_MODULE_FRAME( codeobj_2fbb7b8194454550904d54f37fd7ff4c, module_PIL$ImageChops );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_2fbb7b8194454550904d54f37fd7ff4c );
    assert( Py_REFCNT( frame_2fbb7b8194454550904d54f37fd7ff4c ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_PIL$ImageChops;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Image_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_2fbb7b8194454550904d54f37fd7ff4c->m_frame.f_lineno = 18;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_4 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_PIL$ImageChops,
                const_str_plain_Image,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Image );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_Image, tmp_assign_source_4 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2fbb7b8194454550904d54f37fd7ff4c );
#endif
    popFrameStack();

    assertFrameObject( frame_2fbb7b8194454550904d54f37fd7ff4c );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2fbb7b8194454550904d54f37fd7ff4c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2fbb7b8194454550904d54f37fd7ff4c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2fbb7b8194454550904d54f37fd7ff4c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2fbb7b8194454550904d54f37fd7ff4c, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = MAKE_FUNCTION_PIL$ImageChops$$$function_1_constant(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_constant, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = MAKE_FUNCTION_PIL$ImageChops$$$function_2_duplicate(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_duplicate, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_PIL$ImageChops$$$function_3_invert(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_invert, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = MAKE_FUNCTION_PIL$ImageChops$$$function_4_lighter(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_lighter, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = MAKE_FUNCTION_PIL$ImageChops$$$function_5_darker(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_darker, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_PIL$ImageChops$$$function_6_difference(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_difference, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_PIL$ImageChops$$$function_7_multiply(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_multiply, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_PIL$ImageChops$$$function_8_screen(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_screen, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_float_1_0_int_0_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_13 = MAKE_FUNCTION_PIL$ImageChops$$$function_9_add( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_add, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_float_1_0_int_0_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_14 = MAKE_FUNCTION_PIL$ImageChops$$$function_10_subtract( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_subtract, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_PIL$ImageChops$$$function_11_add_modulo(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_add_modulo, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_PIL$ImageChops$$$function_12_subtract_modulo(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_subtract_modulo, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_PIL$ImageChops$$$function_13_logical_and(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_logical_and, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_PIL$ImageChops$$$function_14_logical_or(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_logical_or, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_PIL$ImageChops$$$function_15_logical_xor(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_logical_xor, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_PIL$ImageChops$$$function_16_blend(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_blend, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_PIL$ImageChops$$$function_17_composite(  );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_composite, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_22 = MAKE_FUNCTION_PIL$ImageChops$$$function_18_offset( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_PIL$ImageChops, (Nuitka_StringObject *)const_str_plain_offset, tmp_assign_source_22 );
    }

    return MOD_RETURN_VALUE( module_PIL$ImageChops );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
