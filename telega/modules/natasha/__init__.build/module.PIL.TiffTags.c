/* Generated code for Python module 'PIL.TiffTags'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_PIL$TiffTags" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_PIL$TiffTags;
PyDictObject *moduledict_PIL$TiffTags;

/* The declarations of module constants used, if any. */
extern PyObject *const_int_pos_12;
static PyObject *const_str_plain_MPIndividualNum;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_int_pos_271;
extern PyObject *const_str_plain___name__;
static PyObject *const_int_pos_45578;
extern PyObject *const_int_pos_9;
static PyObject *const_str_plain_ImageJMetaDataByteCounts;
extern PyObject *const_str_plain_LONG;
static PyObject *const_str_plain_AxisDistance_Y;
static PyObject *const_int_pos_332;
static PyObject *const_int_pos_531;
extern PyObject *const_str_plain_None;
static PyObject *const_str_plain_ConvergenceAngle;
extern PyObject *const_str_plain_Software;
static PyObject *const_str_plain_SMinSampleValue;
extern PyObject *const_int_pos_512;
extern PyObject *const_int_pos_5;
static PyObject *const_str_plain_PanOverlap_V;
static PyObject *const_str_plain_MPEntry;
static PyObject *const_int_pos_518;
static PyObject *const_str_plain_Orientation;
extern PyObject *const_int_pos_262;
static PyObject *const_str_plain_ImageWidth;
static PyObject *const_int_pos_45576;
static PyObject *const_str_plain_SMaxSampleValue;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_tuple_int_pos_323_tuple;
static PyObject *const_str_plain_TileOffsets;
extern PyObject *const_int_pos_258;
extern PyObject *const_str_plain_UNDEFINED;
static PyObject *const_str_plain_Uncompressed;
static PyObject *const_str_plain_PanOverlap_H;
extern PyObject *const_int_pos_278;
static PyObject *const_str_plain_TransferFunction;
static PyObject *const_str_plain_InkSet;
extern PyObject *const_str_plain_none;
static PyObject *const_str_plain_Separate;
static PyObject *const_int_pos_45581;
extern PyObject *const_str_plain_RATIONAL;
static PyObject *const_str_plain_NumberOfImages;
static PyObject *const_str_plain__populate;
extern PyObject *const_int_pos_34377;
static PyObject *const_str_plain_LZW;
static PyObject *const_str_plain_TransferRange;
extern PyObject *const_int_pos_8;
static PyObject *const_int_pos_45574;
static PyObject *const_int_pos_517;
static PyObject *const_int_pos_269;
extern PyObject *const_int_pos_33432;
static PyObject *const_str_digest_53d172ce5d71b7b445878c626394ea74;
extern PyObject *const_str_plain_TYPES;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_SHORT;
static PyObject *const_int_pos_45572;
extern PyObject *const_str_plain_SIGNED_RATIONAL;
extern PyObject *const_int_pos_270;
static PyObject *const_str_plain_inch;
extern PyObject *const_str_plain_False;
static PyObject *const_int_pos_293;
static PyObject *const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c;
static PyObject *const_int_pos_333;
extern PyObject *const_str_plain___new__;
static PyObject *const_int_pos_285;
static PyObject *const_str_plain_JPEGInterchangeFormatLength;
extern PyObject *const_int_0;
static PyObject *const_int_pos_340;
static PyObject *const_str_plain_ReferenceBlackWhite;
extern PyObject *const_int_pos_347;
extern PyObject *const_int_pos_254;
static PyObject *const_str_plain_T4Options;
static PyObject *const_str_plain_StripOffsets;
static PyObject *const_str_plain_Contiguous;
extern PyObject *const_int_pos_4;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_SampleFormat;
static PyObject *const_str_plain_PageName;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_int_pos_34665;
extern PyObject *const_int_pos_301;
extern PyObject *const_int_pos_323;
static PyObject *const_str_plain_JPEGRestartInterval;
static PyObject *const_str_plain_TargetPrinter;
extern PyObject *const_int_pos_50839;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_PhotometricInterpretation;
static PyObject *const_str_plain_ICCProfile;
static PyObject *const_str_plain_ImageJMetaData;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain_MinSampleValue;
static PyObject *const_str_plain_MPFVersion;
static PyObject *const_str_plain_ColorMap;
static PyObject *const_str_plain_TileWidth;
static PyObject *const_dict_d5fe7a7e78b4671757baf25612bf91d9;
extern PyObject *const_int_pos_274;
extern PyObject *const_int_pos_320;
static PyObject *const_str_plain_ExifIFD;
static PyObject *const_str_digest_c09b95005eedf3ad3be8a985f840859c;
static PyObject *const_dict_24f46532581234f3874adc3a050bd90e;
static PyObject *const_str_plain_TileLength;
static PyObject *const_str_plain_SubfileType;
extern PyObject *const_int_pos_45058;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_NumberOfInks;
extern PyObject *const_int_pos_11;
static PyObject *const_str_plain_ImageUIDList;
static PyObject *const_tuple_int_pos_320_tuple;
static PyObject *const_str_plain_GPSInfoIFD;
extern PyObject *const_int_pos_257;
extern PyObject *const_str_plain_JPEG;
static PyObject *const_str_plain_JPEGInterchangeFormat;
static PyObject *const_str_plain_SamplesPerPixel;
extern PyObject *const_str_plain_YResolution;
static PyObject *const_int_pos_318;
static PyObject *const_str_plain_JPEGLosslessPredictors;
extern PyObject *const_int_pos_324;
static PyObject *const_int_pos_321;
extern PyObject *const_int_pos_50838;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_int_pos_292;
extern PyObject *const_int_pos_259;
static PyObject *const_int_pos_290;
static PyObject *const_str_plain_FillOrder;
static PyObject *const_int_pos_32773;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_9072fa341e4af472c925a960d85624d2;
static PyObject *const_str_plain_TileByteCounts;
extern PyObject *const_str_plain_Artist;
extern PyObject *const_str_plain_value;
extern PyObject *const_int_pos_339;
extern PyObject *const_str_plain_collections;
static PyObject *const_tuple_int_pos_532_tuple;
extern PyObject *const_int_pos_34675;
static PyObject *const_tuple_int_pos_333_tuple;
static PyObject *const_tuple_int_pos_301_tuple;
extern PyObject *const_str_plain_sv;
extern PyObject *const_tuple_str_plain_self_str_plain_value_tuple;
extern PyObject *const_int_pos_34853;
static PyObject *const_str_plain_PrimaryChromaticities;
extern PyObject *const_str_plain_cm;
extern PyObject *const_str_plain_DOUBLE;
extern PyObject *const_int_pos_521;
static PyObject *const_int_pos_319;
extern PyObject *const_tuple_int_pos_322_tuple;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_plain_StripByteCounts;
extern PyObject *const_str_plain_k;
static PyObject *const_str_plain_PlanarConfiguration;
extern PyObject *const_int_pos_45057;
extern PyObject *const_int_pos_305;
static PyObject *const_int_pos_316;
extern PyObject *const_int_pos_317;
static PyObject *const_str_plain_TagInfo;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_length;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_int_pos_45580;
extern PyObject *const_str_plain_namedtuple;
static PyObject *const_str_digest_b6dc26ddce418bcacac57b367fb6f7e7;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_sk;
extern PyObject *const_int_pos_256;
static PyObject *const_int_pos_45570;
static PyObject *const_str_plain_AxisDistance_Z;
static PyObject *const_str_plain_DotRange;
static PyObject *const_str_plain_YCbCrCoefficients;
static PyObject *const_str_plain_YPosition;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_TAGS;
static PyObject *const_int_pos_337;
extern PyObject *const_str_plain_XResolution;
static PyObject *const_str_plain_GrayResponseCurve;
extern PyObject *const_int_pos_334;
static PyObject *const_str_digest_c2e7b38ef82ac1e80c8c83b5490b4226;
static PyObject *const_str_digest_6dad7c3e74a8f2791f9f9446a43b0c1f;
static PyObject *const_str_plain_AxisDistance_X;
static PyObject *const_int_pos_281;
static PyObject *const_str_plain_PageNumber;
extern PyObject *const_int_pos_288;
extern PyObject *const_str_plain_SIGNED_BYTE;
static PyObject *const_str_plain_JPEGDCTables;
static PyObject *const_str_plain_ImageDescription;
static PyObject *const_int_pos_341;
static PyObject *const_tuple_acfea2da227d5def1afbf4e120841093_tuple;
static PyObject *const_int_pos_514;
static PyObject *const_set_5e66dfb79f927e10330bafc843d6850e;
extern PyObject *const_int_pos_266;
extern PyObject *const_str_plain_FLOAT;
static PyObject *const_dict_4b92f8f39dec2a9ad82c82df72668389;
static PyObject *const_str_plain_RollAngle;
extern PyObject *const_str_plain_XMP;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_plain_namedtuple_tuple;
static PyObject *const_str_plain_JPEGProc;
static PyObject *const_str_plain_HostComputer;
static PyObject *const_str_digest_7361b02ca501fe7096ab484308be5957;
static PyObject *const_str_plain_TotalFrames;
static PyObject *const_str_plain_Predictor;
static PyObject *const_str_plain_JPEGACTables;
static PyObject *const_int_pos_336;
static PyObject *const_str_plain_ExtraSamples;
static PyObject *const_str_plain_PitchAngle;
extern PyObject *const_str_plain_SIGNED_SHORT;
static PyObject *const_dict_22dc888c657875abc535e5e13e9ac7c5;
extern PyObject *const_tuple_empty;
static PyObject *const_dict_d42350037eae70df4c65014dffbfed19;
extern PyObject *const_int_pos_279;
static PyObject *const_int_pos_513;
static PyObject *const_int_pos_50780;
extern PyObject *const_int_pos_296;
extern PyObject *const_int_pos_10;
extern PyObject *const_str_plain_remove;
static PyObject *const_str_plain_ResolutionUnit;
static PyObject *const_int_pos_45577;
extern PyObject *const_str_plain_SIGNED_LONG;
extern PyObject *const_int_pos_306;
static PyObject *const_str_plain_Make;
static PyObject *const_str_plain_PanOrientation;
static PyObject *const_int_pos_45056;
extern PyObject *const_str_plain___slots__;
static PyObject *const_int_pos_286;
static PyObject *const_dict_39749579f943d31b5ff968e26bc0ca5a;
extern PyObject *const_str_plain_ASCII;
static PyObject *const_int_pos_342;
static PyObject *const_str_plain_JPEGTables;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_Model;
static PyObject *const_str_plain_Compression;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain_YCbCrSubSampling;
extern PyObject *const_int_pos_283;
static PyObject *const_str_plain_PackBits;
static PyObject *const_str_plain_NewSubfileType;
extern PyObject *const_str_plain_enum;
static PyObject *const_int_pos_529;
static PyObject *const_int_pos_45575;
static PyObject *const_int_pos_280;
static PyObject *const_int_pos_297;
extern PyObject *const_str_plain_get;
extern PyObject *const_int_pos_284;
static PyObject *const_int_pos_272;
static PyObject *const_int_pos_45569;
static PyObject *const_str_plain_BestQualityScale;
extern PyObject *const_int_pos_700;
static PyObject *const_int_pos_287;
static PyObject *const_str_plain_MakerNoteSafety;
extern PyObject *const_int_pos_291;
static PyObject *const_str_digest_ffb260d0e0e4558ef75d2a99afc87803;
extern PyObject *const_str_plain_lookup;
extern PyObject *const_str_plain_tag;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_LIBTIFF_CORE;
extern PyObject *const_tuple_int_pos_255_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_unknown;
extern PyObject *const_int_pos_273;
static PyObject *const_int_pos_45313;
static PyObject *const_str_plain_BaselineLength;
static PyObject *const_str_plain_CellWidth;
static PyObject *const_str_plain_RowsPerStrip;
static PyObject *const_tuple_none_str_plain_unknown_none_none_none_tuple;
extern PyObject *const_str_plain_items;
extern PyObject *const_int_pos_519;
static PyObject *const_int_pos_45579;
extern PyObject *const_int_pos_6;
static PyObject *const_str_plain_Unsafe;
extern PyObject *const_str_plain_cls;
static PyObject *const_str_digest_ad8cd88bda0fa81543a675928fabf523;
extern PyObject *const_int_pos_322;
static PyObject *const_int_pos_263;
extern PyObject *const_str_plain_BYTE;
static PyObject *const_str_plain_BaseViewpointNum;
static PyObject *const_str_plain_DocumentName;
static PyObject *const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple;
static PyObject *const_str_plain_YawAngle;
static PyObject *const_str_plain_MaxSampleValue;
extern PyObject *const_int_pos_338;
static PyObject *const_str_plain_HalftoneHints;
static PyObject *const_str_digest_a0f7729a174dda1cefa8c44b6d04c7dc;
static PyObject *const_int_pos_45059;
extern PyObject *const_tuple_str_plain_tag_tuple;
static PyObject *const_str_plain_Threshholding;
static PyObject *const_int_pos_45573;
static PyObject *const_str_plain_BitsPerSample;
static PyObject *const_int_pos_45571;
static PyObject *const_str_plain_YCbCrPositioning;
static PyObject *const_str_plain_VerticalDivergence;
static PyObject *const_int_pos_515;
extern PyObject *const_int_pos_282;
extern PyObject *const_str_plain_cvt_enum;
static PyObject *const_str_plain__TagInfo;
static PyObject *const_str_plain_GrayResponseUnit;
static PyObject *const_str_plain_Safe;
static PyObject *const_int_pos_289;
extern PyObject *const_int_pos_520;
static PyObject *const_str_plain_InkNames;
extern PyObject *const_int_pos_530;
static PyObject *const_str_plain_PhotoshopInfo;
static PyObject *const_str_plain_JPEGQTables;
static PyObject *const_str_plain_JPEGPointTransforms;
static PyObject *const_str_plain_CellLength;
static PyObject *const_int_pos_264;
static PyObject *const_str_plain_Copyright;
static PyObject *const_str_plain_XPosition;
static PyObject *const_int_pos_45060;
static PyObject *const_str_digest_721f10328662a234e6901bd2fd070d78;
static PyObject *const_str_plain_FreeByteCounts;
static PyObject *const_int_pos_325;
extern PyObject *const_int_pos_265;
static PyObject *const_str_plain_ImageLength;
extern PyObject *const_int_pos_277;
static PyObject *const_str_plain_WhitePoint;
extern PyObject *const_int_pos_3;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_DateTime;
static PyObject *const_int_pos_532;
extern PyObject *const_int_pos_7;
static PyObject *const_str_plain_FreeOffsets;
static PyObject *const_str_plain_T6Options;
extern PyObject *const_int_pos_2;
static PyObject *const_int_pos_50741;
static PyObject *const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple;
extern PyObject *const_int_pos_315;
static PyObject *const_str_plain_TAGS_V2;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_MPIndividualNum = UNSTREAM_STRING_ASCII( &constant_bin[ 106525 ], 15, 1 );
    const_int_pos_45578 = PyLong_FromUnsignedLong( 45578ul );
    const_str_plain_ImageJMetaDataByteCounts = UNSTREAM_STRING_ASCII( &constant_bin[ 106540 ], 24, 1 );
    const_str_plain_AxisDistance_Y = UNSTREAM_STRING_ASCII( &constant_bin[ 106564 ], 14, 1 );
    const_int_pos_332 = PyLong_FromUnsignedLong( 332ul );
    const_int_pos_531 = PyLong_FromUnsignedLong( 531ul );
    const_str_plain_ConvergenceAngle = UNSTREAM_STRING_ASCII( &constant_bin[ 106578 ], 16, 1 );
    const_str_plain_SMinSampleValue = UNSTREAM_STRING_ASCII( &constant_bin[ 106594 ], 15, 1 );
    const_str_plain_PanOverlap_V = UNSTREAM_STRING_ASCII( &constant_bin[ 106609 ], 12, 1 );
    const_str_plain_MPEntry = UNSTREAM_STRING_ASCII( &constant_bin[ 106621 ], 7, 1 );
    const_int_pos_518 = PyLong_FromUnsignedLong( 518ul );
    const_str_plain_Orientation = UNSTREAM_STRING_ASCII( &constant_bin[ 106628 ], 11, 1 );
    const_str_plain_ImageWidth = UNSTREAM_STRING_ASCII( &constant_bin[ 106639 ], 10, 1 );
    const_int_pos_45576 = PyLong_FromUnsignedLong( 45576ul );
    const_str_plain_SMaxSampleValue = UNSTREAM_STRING_ASCII( &constant_bin[ 106649 ], 15, 1 );
    const_str_plain_TileOffsets = UNSTREAM_STRING_ASCII( &constant_bin[ 106664 ], 11, 1 );
    const_str_plain_Uncompressed = UNSTREAM_STRING_ASCII( &constant_bin[ 106675 ], 12, 1 );
    const_str_plain_PanOverlap_H = UNSTREAM_STRING_ASCII( &constant_bin[ 106687 ], 12, 1 );
    const_str_plain_TransferFunction = UNSTREAM_STRING_ASCII( &constant_bin[ 106699 ], 16, 1 );
    const_str_plain_InkSet = UNSTREAM_STRING_ASCII( &constant_bin[ 106715 ], 6, 1 );
    const_str_plain_Separate = UNSTREAM_STRING_ASCII( &constant_bin[ 106721 ], 8, 1 );
    const_int_pos_45581 = PyLong_FromUnsignedLong( 45581ul );
    const_str_plain_NumberOfImages = UNSTREAM_STRING_ASCII( &constant_bin[ 106729 ], 14, 1 );
    const_str_plain__populate = UNSTREAM_STRING_ASCII( &constant_bin[ 106743 ], 9, 1 );
    const_str_plain_LZW = UNSTREAM_STRING_ASCII( &constant_bin[ 106752 ], 3, 1 );
    const_str_plain_TransferRange = UNSTREAM_STRING_ASCII( &constant_bin[ 106755 ], 13, 1 );
    const_int_pos_45574 = PyLong_FromUnsignedLong( 45574ul );
    const_int_pos_517 = PyLong_FromUnsignedLong( 517ul );
    const_int_pos_269 = PyLong_FromUnsignedLong( 269ul );
    const_str_digest_53d172ce5d71b7b445878c626394ea74 = UNSTREAM_STRING_ASCII( &constant_bin[ 105642 ], 12, 0 );
    const_int_pos_45572 = PyLong_FromUnsignedLong( 45572ul );
    const_str_plain_inch = UNSTREAM_STRING_ASCII( &constant_bin[ 65285 ], 4, 1 );
    const_int_pos_293 = PyLong_FromUnsignedLong( 293ul );
    const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c = _PyDict_NewPresized( 7 );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_plain_Uncompressed, const_int_pos_1 );
    const_str_digest_7361b02ca501fe7096ab484308be5957 = UNSTREAM_STRING_ASCII( &constant_bin[ 106768 ], 8, 0 );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_digest_7361b02ca501fe7096ab484308be5957, const_int_pos_2 );
    const_str_digest_c2e7b38ef82ac1e80c8c83b5490b4226 = UNSTREAM_STRING_ASCII( &constant_bin[ 106776 ], 11, 0 );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_digest_c2e7b38ef82ac1e80c8c83b5490b4226, const_int_pos_3 );
    const_str_digest_a0f7729a174dda1cefa8c44b6d04c7dc = UNSTREAM_STRING_ASCII( &constant_bin[ 106787 ], 11, 0 );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_digest_a0f7729a174dda1cefa8c44b6d04c7dc, const_int_pos_4 );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_plain_LZW, const_int_pos_5 );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_plain_JPEG, const_int_pos_6 );
    const_str_plain_PackBits = UNSTREAM_STRING_ASCII( &constant_bin[ 106798 ], 8, 1 );
    const_int_pos_32773 = PyLong_FromUnsignedLong( 32773ul );
    PyDict_SetItem( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c, const_str_plain_PackBits, const_int_pos_32773 );
    assert( PyDict_Size( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c ) == 7 );
    const_int_pos_333 = PyLong_FromUnsignedLong( 333ul );
    const_int_pos_285 = PyLong_FromUnsignedLong( 285ul );
    const_str_plain_JPEGInterchangeFormatLength = UNSTREAM_STRING_ASCII( &constant_bin[ 106806 ], 27, 1 );
    const_int_pos_340 = PyLong_FromUnsignedLong( 340ul );
    const_str_plain_ReferenceBlackWhite = UNSTREAM_STRING_ASCII( &constant_bin[ 106833 ], 19, 1 );
    const_str_plain_T4Options = UNSTREAM_STRING_ASCII( &constant_bin[ 106852 ], 9, 1 );
    const_str_plain_StripOffsets = UNSTREAM_STRING_ASCII( &constant_bin[ 106861 ], 12, 1 );
    const_str_plain_Contiguous = UNSTREAM_STRING_ASCII( &constant_bin[ 106873 ], 10, 1 );
    const_str_plain_SampleFormat = UNSTREAM_STRING_ASCII( &constant_bin[ 106883 ], 12, 1 );
    const_str_plain_PageName = UNSTREAM_STRING_ASCII( &constant_bin[ 106895 ], 8, 1 );
    const_str_plain_JPEGRestartInterval = UNSTREAM_STRING_ASCII( &constant_bin[ 106903 ], 19, 1 );
    const_str_plain_TargetPrinter = UNSTREAM_STRING_ASCII( &constant_bin[ 106922 ], 13, 1 );
    const_str_plain_PhotometricInterpretation = UNSTREAM_STRING_ASCII( &constant_bin[ 106935 ], 25, 1 );
    const_str_plain_ICCProfile = UNSTREAM_STRING_ASCII( &constant_bin[ 106960 ], 10, 1 );
    const_str_plain_ImageJMetaData = UNSTREAM_STRING_ASCII( &constant_bin[ 106540 ], 14, 1 );
    const_str_plain_MinSampleValue = UNSTREAM_STRING_ASCII( &constant_bin[ 106595 ], 14, 1 );
    const_str_plain_MPFVersion = UNSTREAM_STRING_ASCII( &constant_bin[ 106970 ], 10, 1 );
    const_str_plain_ColorMap = UNSTREAM_STRING_ASCII( &constant_bin[ 106980 ], 8, 1 );
    const_str_plain_TileWidth = UNSTREAM_STRING_ASCII( &constant_bin[ 106988 ], 9, 1 );
    const_dict_d5fe7a7e78b4671757baf25612bf91d9 = _PyDict_NewPresized( 2 );
    const_str_plain_Unsafe = UNSTREAM_STRING_ASCII( &constant_bin[ 106997 ], 6, 1 );
    PyDict_SetItem( const_dict_d5fe7a7e78b4671757baf25612bf91d9, const_str_plain_Unsafe, const_int_0 );
    const_str_plain_Safe = UNSTREAM_STRING_ASCII( &constant_bin[ 107003 ], 4, 1 );
    PyDict_SetItem( const_dict_d5fe7a7e78b4671757baf25612bf91d9, const_str_plain_Safe, const_int_pos_1 );
    assert( PyDict_Size( const_dict_d5fe7a7e78b4671757baf25612bf91d9 ) == 2 );
    const_str_plain_ExifIFD = UNSTREAM_STRING_ASCII( &constant_bin[ 107007 ], 7, 1 );
    const_str_digest_c09b95005eedf3ad3be8a985f840859c = UNSTREAM_STRING_ASCII( &constant_bin[ 107014 ], 23, 0 );
    const_dict_24f46532581234f3874adc3a050bd90e = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_24f46532581234f3874adc3a050bd90e, const_str_plain_none, const_int_pos_1 );
    PyDict_SetItem( const_dict_24f46532581234f3874adc3a050bd90e, const_str_digest_c09b95005eedf3ad3be8a985f840859c, const_int_pos_2 );
    assert( PyDict_Size( const_dict_24f46532581234f3874adc3a050bd90e ) == 2 );
    const_str_plain_TileLength = UNSTREAM_STRING_ASCII( &constant_bin[ 107037 ], 10, 1 );
    const_str_plain_SubfileType = UNSTREAM_STRING_ASCII( &constant_bin[ 107047 ], 11, 1 );
    const_str_plain_NumberOfInks = UNSTREAM_STRING_ASCII( &constant_bin[ 107058 ], 12, 1 );
    const_str_plain_ImageUIDList = UNSTREAM_STRING_ASCII( &constant_bin[ 107070 ], 12, 1 );
    const_tuple_int_pos_320_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_320_tuple, 0, const_int_pos_320 ); Py_INCREF( const_int_pos_320 );
    const_str_plain_GPSInfoIFD = UNSTREAM_STRING_ASCII( &constant_bin[ 107082 ], 10, 1 );
    const_str_plain_JPEGInterchangeFormat = UNSTREAM_STRING_ASCII( &constant_bin[ 106806 ], 21, 1 );
    const_str_plain_SamplesPerPixel = UNSTREAM_STRING_ASCII( &constant_bin[ 107092 ], 15, 1 );
    const_int_pos_318 = PyLong_FromUnsignedLong( 318ul );
    const_str_plain_JPEGLosslessPredictors = UNSTREAM_STRING_ASCII( &constant_bin[ 107107 ], 22, 1 );
    const_int_pos_321 = PyLong_FromUnsignedLong( 321ul );
    const_int_pos_290 = PyLong_FromUnsignedLong( 290ul );
    const_str_plain_FillOrder = UNSTREAM_STRING_ASCII( &constant_bin[ 107129 ], 9, 1 );
    const_str_digest_9072fa341e4af472c925a960d85624d2 = UNSTREAM_STRING_ASCII( &constant_bin[ 107138 ], 27, 0 );
    const_str_plain_TileByteCounts = UNSTREAM_STRING_ASCII( &constant_bin[ 107165 ], 14, 1 );
    const_tuple_int_pos_532_tuple = PyTuple_New( 1 );
    const_int_pos_532 = PyLong_FromUnsignedLong( 532ul );
    PyTuple_SET_ITEM( const_tuple_int_pos_532_tuple, 0, const_int_pos_532 ); Py_INCREF( const_int_pos_532 );
    const_tuple_int_pos_333_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_333_tuple, 0, const_int_pos_333 ); Py_INCREF( const_int_pos_333 );
    const_tuple_int_pos_301_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_301_tuple, 0, const_int_pos_301 ); Py_INCREF( const_int_pos_301 );
    const_str_plain_PrimaryChromaticities = UNSTREAM_STRING_ASCII( &constant_bin[ 107179 ], 21, 1 );
    const_int_pos_319 = PyLong_FromUnsignedLong( 319ul );
    const_str_plain_StripByteCounts = UNSTREAM_STRING_ASCII( &constant_bin[ 107200 ], 15, 1 );
    const_str_plain_PlanarConfiguration = UNSTREAM_STRING_ASCII( &constant_bin[ 107215 ], 19, 1 );
    const_int_pos_316 = PyLong_FromUnsignedLong( 316ul );
    const_str_plain_TagInfo = UNSTREAM_STRING_ASCII( &constant_bin[ 107234 ], 7, 1 );
    const_int_pos_45580 = PyLong_FromUnsignedLong( 45580ul );
    const_str_digest_b6dc26ddce418bcacac57b367fb6f7e7 = UNSTREAM_STRING_ASCII( &constant_bin[ 107241 ], 15, 0 );
    const_int_pos_45570 = PyLong_FromUnsignedLong( 45570ul );
    const_str_plain_AxisDistance_Z = UNSTREAM_STRING_ASCII( &constant_bin[ 107256 ], 14, 1 );
    const_str_plain_DotRange = UNSTREAM_STRING_ASCII( &constant_bin[ 107270 ], 8, 1 );
    const_str_plain_YCbCrCoefficients = UNSTREAM_STRING_ASCII( &constant_bin[ 107278 ], 17, 1 );
    const_str_plain_YPosition = UNSTREAM_STRING_ASCII( &constant_bin[ 107295 ], 9, 1 );
    const_str_plain_TAGS = UNSTREAM_STRING_ASCII( &constant_bin[ 107304 ], 4, 1 );
    const_int_pos_337 = PyLong_FromUnsignedLong( 337ul );
    const_str_plain_GrayResponseCurve = UNSTREAM_STRING_ASCII( &constant_bin[ 107308 ], 17, 1 );
    const_str_digest_6dad7c3e74a8f2791f9f9446a43b0c1f = UNSTREAM_STRING_ASCII( &constant_bin[ 107325 ], 16, 0 );
    const_str_plain_AxisDistance_X = UNSTREAM_STRING_ASCII( &constant_bin[ 107341 ], 14, 1 );
    const_int_pos_281 = PyLong_FromUnsignedLong( 281ul );
    const_str_plain_PageNumber = UNSTREAM_STRING_ASCII( &constant_bin[ 107355 ], 10, 1 );
    const_str_plain_JPEGDCTables = UNSTREAM_STRING_ASCII( &constant_bin[ 107365 ], 12, 1 );
    const_str_plain_ImageDescription = UNSTREAM_STRING_ASCII( &constant_bin[ 107377 ], 16, 1 );
    const_int_pos_341 = PyLong_FromUnsignedLong( 341ul );
    const_tuple_acfea2da227d5def1afbf4e120841093_tuple = PyTuple_New( 2 );
    const_str_plain__TagInfo = UNSTREAM_STRING_ASCII( &constant_bin[ 107393 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_acfea2da227d5def1afbf4e120841093_tuple, 0, const_str_plain__TagInfo ); Py_INCREF( const_str_plain__TagInfo );
    PyTuple_SET_ITEM( const_tuple_acfea2da227d5def1afbf4e120841093_tuple, 1, const_str_digest_9072fa341e4af472c925a960d85624d2 ); Py_INCREF( const_str_digest_9072fa341e4af472c925a960d85624d2 );
    const_int_pos_514 = PyLong_FromUnsignedLong( 514ul );
    const_set_5e66dfb79f927e10330bafc843d6850e = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 107401 ], 200 );
    const_dict_4b92f8f39dec2a9ad82c82df72668389 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 107601 ], 3075 );
    const_str_plain_RollAngle = UNSTREAM_STRING_ASCII( &constant_bin[ 110676 ], 9, 1 );
    const_str_plain_JPEGProc = UNSTREAM_STRING_ASCII( &constant_bin[ 110685 ], 8, 1 );
    const_str_plain_HostComputer = UNSTREAM_STRING_ASCII( &constant_bin[ 110693 ], 12, 1 );
    const_str_plain_TotalFrames = UNSTREAM_STRING_ASCII( &constant_bin[ 110705 ], 11, 1 );
    const_str_plain_Predictor = UNSTREAM_STRING_ASCII( &constant_bin[ 107119 ], 9, 1 );
    const_str_plain_JPEGACTables = UNSTREAM_STRING_ASCII( &constant_bin[ 110716 ], 12, 1 );
    const_int_pos_336 = PyLong_FromUnsignedLong( 336ul );
    const_str_plain_ExtraSamples = UNSTREAM_STRING_ASCII( &constant_bin[ 110728 ], 12, 1 );
    const_str_plain_PitchAngle = UNSTREAM_STRING_ASCII( &constant_bin[ 110740 ], 10, 1 );
    const_dict_22dc888c657875abc535e5e13e9ac7c5 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_22dc888c657875abc535e5e13e9ac7c5, const_str_plain_none, const_int_pos_1 );
    PyDict_SetItem( const_dict_22dc888c657875abc535e5e13e9ac7c5, const_str_plain_inch, const_int_pos_2 );
    PyDict_SetItem( const_dict_22dc888c657875abc535e5e13e9ac7c5, const_str_plain_cm, const_int_pos_3 );
    assert( PyDict_Size( const_dict_22dc888c657875abc535e5e13e9ac7c5 ) == 3 );
    const_dict_d42350037eae70df4c65014dffbfed19 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_d42350037eae70df4c65014dffbfed19, const_str_plain_Contiguous, const_int_pos_1 );
    PyDict_SetItem( const_dict_d42350037eae70df4c65014dffbfed19, const_str_plain_Separate, const_int_pos_2 );
    assert( PyDict_Size( const_dict_d42350037eae70df4c65014dffbfed19 ) == 2 );
    const_int_pos_513 = PyLong_FromUnsignedLong( 513ul );
    const_int_pos_50780 = PyLong_FromUnsignedLong( 50780ul );
    const_str_plain_ResolutionUnit = UNSTREAM_STRING_ASCII( &constant_bin[ 109142 ], 14, 1 );
    const_int_pos_45577 = PyLong_FromUnsignedLong( 45577ul );
    const_str_plain_Make = UNSTREAM_STRING_ASCII( &constant_bin[ 21906 ], 4, 1 );
    const_str_plain_PanOrientation = UNSTREAM_STRING_ASCII( &constant_bin[ 110750 ], 14, 1 );
    const_int_pos_45056 = PyLong_FromUnsignedLong( 45056ul );
    const_int_pos_286 = PyLong_FromUnsignedLong( 286ul );
    const_dict_39749579f943d31b5ff968e26bc0ca5a = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 110764 ], 152 );
    const_int_pos_342 = PyLong_FromUnsignedLong( 342ul );
    const_str_plain_JPEGTables = UNSTREAM_STRING_ASCII( &constant_bin[ 107609 ], 10, 1 );
    const_str_plain_Compression = UNSTREAM_STRING_ASCII( &constant_bin[ 91789 ], 11, 1 );
    const_str_plain_YCbCrSubSampling = UNSTREAM_STRING_ASCII( &constant_bin[ 110916 ], 16, 1 );
    const_str_plain_NewSubfileType = UNSTREAM_STRING_ASCII( &constant_bin[ 110932 ], 14, 1 );
    const_int_pos_529 = PyLong_FromUnsignedLong( 529ul );
    const_int_pos_45575 = PyLong_FromUnsignedLong( 45575ul );
    const_int_pos_280 = PyLong_FromUnsignedLong( 280ul );
    const_int_pos_297 = PyLong_FromUnsignedLong( 297ul );
    const_int_pos_272 = PyLong_FromUnsignedLong( 272ul );
    const_int_pos_45569 = PyLong_FromUnsignedLong( 45569ul );
    const_str_plain_BestQualityScale = UNSTREAM_STRING_ASCII( &constant_bin[ 110946 ], 16, 1 );
    const_int_pos_287 = PyLong_FromUnsignedLong( 287ul );
    const_str_plain_MakerNoteSafety = UNSTREAM_STRING_ASCII( &constant_bin[ 110962 ], 15, 1 );
    const_str_digest_ffb260d0e0e4558ef75d2a99afc87803 = UNSTREAM_STRING_ASCII( &constant_bin[ 110977 ], 15, 0 );
    const_int_pos_45313 = PyLong_FromUnsignedLong( 45313ul );
    const_str_plain_BaselineLength = UNSTREAM_STRING_ASCII( &constant_bin[ 110992 ], 14, 1 );
    const_str_plain_CellWidth = UNSTREAM_STRING_ASCII( &constant_bin[ 111006 ], 9, 1 );
    const_str_plain_RowsPerStrip = UNSTREAM_STRING_ASCII( &constant_bin[ 111015 ], 12, 1 );
    const_tuple_none_str_plain_unknown_none_none_none_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_unknown_none_none_none_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_unknown_none_none_none_tuple, 1, const_str_plain_unknown ); Py_INCREF( const_str_plain_unknown );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_unknown_none_none_none_tuple, 2, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_unknown_none_none_none_tuple, 3, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_str_plain_unknown_none_none_none_tuple, 4, Py_None ); Py_INCREF( Py_None );
    const_int_pos_45579 = PyLong_FromUnsignedLong( 45579ul );
    const_str_digest_ad8cd88bda0fa81543a675928fabf523 = UNSTREAM_STRING_ASCII( &constant_bin[ 111027 ], 247, 0 );
    const_int_pos_263 = PyLong_FromUnsignedLong( 263ul );
    const_str_plain_BaseViewpointNum = UNSTREAM_STRING_ASCII( &constant_bin[ 111274 ], 16, 1 );
    const_str_plain_DocumentName = UNSTREAM_STRING_ASCII( &constant_bin[ 111290 ], 12, 1 );
    const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple, 0, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    PyTuple_SET_ITEM( const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple, 1, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple, 2, const_str_plain_sk ); Py_INCREF( const_str_plain_sk );
    PyTuple_SET_ITEM( const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple, 3, const_str_plain_sv ); Py_INCREF( const_str_plain_sv );
    const_str_plain_YawAngle = UNSTREAM_STRING_ASCII( &constant_bin[ 111302 ], 8, 1 );
    const_str_plain_MaxSampleValue = UNSTREAM_STRING_ASCII( &constant_bin[ 106650 ], 14, 1 );
    const_str_plain_HalftoneHints = UNSTREAM_STRING_ASCII( &constant_bin[ 111310 ], 13, 1 );
    const_int_pos_45059 = PyLong_FromUnsignedLong( 45059ul );
    const_str_plain_Threshholding = UNSTREAM_STRING_ASCII( &constant_bin[ 111323 ], 13, 1 );
    const_int_pos_45573 = PyLong_FromUnsignedLong( 45573ul );
    const_str_plain_BitsPerSample = UNSTREAM_STRING_ASCII( &constant_bin[ 111336 ], 13, 1 );
    const_int_pos_45571 = PyLong_FromUnsignedLong( 45571ul );
    const_str_plain_YCbCrPositioning = UNSTREAM_STRING_ASCII( &constant_bin[ 111349 ], 16, 1 );
    const_str_plain_VerticalDivergence = UNSTREAM_STRING_ASCII( &constant_bin[ 111365 ], 18, 1 );
    const_int_pos_515 = PyLong_FromUnsignedLong( 515ul );
    const_str_plain_GrayResponseUnit = UNSTREAM_STRING_ASCII( &constant_bin[ 111383 ], 16, 1 );
    const_int_pos_289 = PyLong_FromUnsignedLong( 289ul );
    const_str_plain_InkNames = UNSTREAM_STRING_ASCII( &constant_bin[ 111399 ], 8, 1 );
    const_str_plain_PhotoshopInfo = UNSTREAM_STRING_ASCII( &constant_bin[ 108022 ], 13, 1 );
    const_str_plain_JPEGQTables = UNSTREAM_STRING_ASCII( &constant_bin[ 111407 ], 11, 1 );
    const_str_plain_JPEGPointTransforms = UNSTREAM_STRING_ASCII( &constant_bin[ 111418 ], 19, 1 );
    const_str_plain_CellLength = UNSTREAM_STRING_ASCII( &constant_bin[ 111437 ], 10, 1 );
    const_int_pos_264 = PyLong_FromUnsignedLong( 264ul );
    const_str_plain_Copyright = UNSTREAM_STRING_ASCII( &constant_bin[ 1058 ], 9, 1 );
    const_str_plain_XPosition = UNSTREAM_STRING_ASCII( &constant_bin[ 111447 ], 9, 1 );
    const_int_pos_45060 = PyLong_FromUnsignedLong( 45060ul );
    const_str_digest_721f10328662a234e6901bd2fd070d78 = UNSTREAM_STRING_ASCII( &constant_bin[ 111456 ], 21, 0 );
    const_str_plain_FreeByteCounts = UNSTREAM_STRING_ASCII( &constant_bin[ 111477 ], 14, 1 );
    const_int_pos_325 = PyLong_FromUnsignedLong( 325ul );
    const_str_plain_ImageLength = UNSTREAM_STRING_ASCII( &constant_bin[ 111491 ], 11, 1 );
    const_str_plain_WhitePoint = UNSTREAM_STRING_ASCII( &constant_bin[ 111502 ], 10, 1 );
    const_str_plain_DateTime = UNSTREAM_STRING_ASCII( &constant_bin[ 108449 ], 8, 1 );
    const_str_plain_FreeOffsets = UNSTREAM_STRING_ASCII( &constant_bin[ 111512 ], 11, 1 );
    const_str_plain_T6Options = UNSTREAM_STRING_ASCII( &constant_bin[ 111523 ], 9, 1 );
    const_int_pos_50741 = PyLong_FromUnsignedLong( 50741ul );
    const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 3, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 4, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 5, const_str_plain_enum ); Py_INCREF( const_str_plain_enum );
    PyTuple_SET_ITEM( const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 6, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_plain_TAGS_V2 = UNSTREAM_STRING_ASCII( &constant_bin[ 111106 ], 7, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_PIL$TiffTags( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_e8b23506ab469c2d72bc10c38ef6aae5;
static PyCodeObject *codeobj_d29fc0804c1aaba60d7f980f77c77c96;
static PyCodeObject *codeobj_da346643a0ba3d8d5d1634fbaa203cd1;
static PyCodeObject *codeobj_3b49996fa0e4880fce8b7a0984bb2158;
static PyCodeObject *codeobj_d46e6593214c94bf6a7e8880aad44632;
static PyCodeObject *codeobj_4ddb20c1cc5e6b07c56f0704ee2f54b5;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_ffb260d0e0e4558ef75d2a99afc87803 );
    codeobj_e8b23506ab469c2d72bc10c38ef6aae5 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_721f10328662a234e6901bd2fd070d78, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_d29fc0804c1aaba60d7f980f77c77c96 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_TagInfo, 23, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_da346643a0ba3d8d5d1634fbaa203cd1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___new__, 26, const_tuple_b3f774eba0643da7255cb6f47a856fab_tuple, 6, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_3b49996fa0e4880fce8b7a0984bb2158 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__populate, 356, const_tuple_str_plain_k_str_plain_v_str_plain_sk_str_plain_sv_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d46e6593214c94bf6a7e8880aad44632 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cvt_enum, 31, const_tuple_str_plain_self_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4ddb20c1cc5e6b07c56f0704ee2f54b5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_lookup, 38, const_tuple_str_plain_tag_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_5_complex_call_helper_pos_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_1___new__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_2_cvt_enum(  );


static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_3_lookup(  );


static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_4__populate(  );


// The module function definitions.
static PyObject *impl_PIL$TiffTags$$$function_1___new__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_type = python_pars[ 3 ];
    PyObject *par_length = python_pars[ 4 ];
    PyObject *par_enum = python_pars[ 5 ];
    struct Nuitka_FrameObject *frame_da346643a0ba3d8d5d1634fbaa203cd1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_da346643a0ba3d8d5d1634fbaa203cd1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_da346643a0ba3d8d5d1634fbaa203cd1, codeobj_da346643a0ba3d8d5d1634fbaa203cd1, module_PIL$TiffTags, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_da346643a0ba3d8d5d1634fbaa203cd1 = cache_frame_da346643a0ba3d8d5d1634fbaa203cd1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_da346643a0ba3d8d5d1634fbaa203cd1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_da346643a0ba3d8d5d1634fbaa203cd1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TagInfo );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TagInfo );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TagInfo" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 28;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_cls );
        tmp_object_name_1 = par_cls;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___new__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_cls );
        tmp_args_element_name_1 = par_cls;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_2 = par_value;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_3 = par_name;
        CHECK_OBJECT( par_type );
        tmp_args_element_name_4 = par_type;
        CHECK_OBJECT( par_length );
        tmp_args_element_name_5 = par_length;
        CHECK_OBJECT( par_enum );
        tmp_or_left_value_1 = par_enum;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 29;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = PyDict_New();
        tmp_args_element_name_6 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_args_element_name_6 = tmp_or_left_value_1;
        or_end_1:;
        frame_da346643a0ba3d8d5d1634fbaa203cd1->m_frame.f_lineno = 28;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_da346643a0ba3d8d5d1634fbaa203cd1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_da346643a0ba3d8d5d1634fbaa203cd1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_da346643a0ba3d8d5d1634fbaa203cd1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_da346643a0ba3d8d5d1634fbaa203cd1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_da346643a0ba3d8d5d1634fbaa203cd1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_da346643a0ba3d8d5d1634fbaa203cd1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_da346643a0ba3d8d5d1634fbaa203cd1,
        type_description_1,
        par_cls,
        par_value,
        par_name,
        par_type,
        par_length,
        par_enum,
        NULL
    );


    // Release cached frame.
    if ( frame_da346643a0ba3d8d5d1634fbaa203cd1 == cache_frame_da346643a0ba3d8d5d1634fbaa203cd1 )
    {
        Py_DECREF( frame_da346643a0ba3d8d5d1634fbaa203cd1 );
    }
    cache_frame_da346643a0ba3d8d5d1634fbaa203cd1 = NULL;

    assertFrameObject( frame_da346643a0ba3d8d5d1634fbaa203cd1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_1___new__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_type );
    Py_DECREF( par_type );
    par_type = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_enum );
    Py_DECREF( par_enum );
    par_enum = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_type );
    Py_DECREF( par_type );
    par_type = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_enum );
    Py_DECREF( par_enum );
    par_enum = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_1___new__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$TiffTags$$$function_2_cvt_enum( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_d46e6593214c94bf6a7e8880aad44632;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d46e6593214c94bf6a7e8880aad44632 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d46e6593214c94bf6a7e8880aad44632, codeobj_d46e6593214c94bf6a7e8880aad44632, module_PIL$TiffTags, sizeof(void *)+sizeof(void *) );
    frame_d46e6593214c94bf6a7e8880aad44632 = cache_frame_d46e6593214c94bf6a7e8880aad44632;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d46e6593214c94bf6a7e8880aad44632 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d46e6593214c94bf6a7e8880aad44632 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_enum );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 35;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_enum );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_value );
        tmp_args_element_name_1 = par_value;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_2 = par_value;
        frame_d46e6593214c94bf6a7e8880aad44632->m_frame.f_lineno = 35;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( par_value );
        tmp_return_value = par_value;
        Py_INCREF( tmp_return_value );
        condexpr_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d46e6593214c94bf6a7e8880aad44632 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d46e6593214c94bf6a7e8880aad44632 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d46e6593214c94bf6a7e8880aad44632 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d46e6593214c94bf6a7e8880aad44632, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d46e6593214c94bf6a7e8880aad44632->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d46e6593214c94bf6a7e8880aad44632, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d46e6593214c94bf6a7e8880aad44632,
        type_description_1,
        par_self,
        par_value
    );


    // Release cached frame.
    if ( frame_d46e6593214c94bf6a7e8880aad44632 == cache_frame_d46e6593214c94bf6a7e8880aad44632 )
    {
        Py_DECREF( frame_d46e6593214c94bf6a7e8880aad44632 );
    }
    cache_frame_d46e6593214c94bf6a7e8880aad44632 = NULL;

    assertFrameObject( frame_d46e6593214c94bf6a7e8880aad44632 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_2_cvt_enum );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_2_cvt_enum );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$TiffTags$$$function_3_lookup( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tag = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4ddb20c1cc5e6b07c56f0704ee2f54b5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4ddb20c1cc5e6b07c56f0704ee2f54b5, codeobj_4ddb20c1cc5e6b07c56f0704ee2f54b5, module_PIL$TiffTags, sizeof(void *) );
    frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 = cache_frame_4ddb20c1cc5e6b07c56f0704ee2f54b5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS_V2 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAGS_V2 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAGS_V2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_1 = par_tag;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TagInfo );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TagInfo );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TagInfo" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_3 = par_tag;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAGS );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAGS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        CHECK_OBJECT( par_tag );
        tmp_args_element_name_5 = par_tag;
        tmp_args_element_name_6 = const_str_plain_unknown;
        frame_4ddb20c1cc5e6b07c56f0704ee2f54b5->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_args_element_name_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4ddb20c1cc5e6b07c56f0704ee2f54b5->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_4ddb20c1cc5e6b07c56f0704ee2f54b5->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4ddb20c1cc5e6b07c56f0704ee2f54b5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4ddb20c1cc5e6b07c56f0704ee2f54b5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4ddb20c1cc5e6b07c56f0704ee2f54b5,
        type_description_1,
        par_tag
    );


    // Release cached frame.
    if ( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 == cache_frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 )
    {
        Py_DECREF( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 );
    }
    cache_frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 = NULL;

    assertFrameObject( frame_4ddb20c1cc5e6b07c56f0704ee2f54b5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_3_lookup );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_tag );
    Py_DECREF( par_tag );
    par_tag = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_3_lookup );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$TiffTags$$$function_4__populate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_k = NULL;
    PyObject *var_v = NULL;
    PyObject *var_sk = NULL;
    PyObject *var_sv = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_3b49996fa0e4880fce8b7a0984bb2158;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_3b49996fa0e4880fce8b7a0984bb2158 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3b49996fa0e4880fce8b7a0984bb2158, codeobj_3b49996fa0e4880fce8b7a0984bb2158, module_PIL$TiffTags, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3b49996fa0e4880fce8b7a0984bb2158 = cache_frame_3b49996fa0e4880fce8b7a0984bb2158;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3b49996fa0e4880fce8b7a0984bb2158 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3b49996fa0e4880fce8b7a0984bb2158 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS_V2 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAGS_V2 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAGS_V2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_3b49996fa0e4880fce8b7a0984bb2158->m_frame.f_lineno = 357;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 357;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 357;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooo";
            exception_lineno = 357;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooo";
                    exception_lineno = 357;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooo";
            exception_lineno = 357;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_k;
            var_k = tmp_assign_source_6;
            Py_INCREF( var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_7;
            Py_INCREF( var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( var_v );
        tmp_subscribed_name_1 = var_v;
        tmp_subscript_name_1 = const_int_0;
        tmp_ass_subvalue_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_ass_subvalue_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAGS );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAGS" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 359;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_2;
        CHECK_OBJECT( var_k );
        tmp_ass_subscript_1 = var_k;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 359;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( var_v );
        tmp_len_arg_1 = var_v;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = const_int_pos_4;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_iter_arg_3;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_v );
            tmp_subscribed_name_2 = var_v;
            tmp_subscript_name_2 = const_int_pos_3;
            tmp_called_instance_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 3 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            frame_3b49996fa0e4880fce8b7a0984bb2158->m_frame.f_lineno = 361;
            tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_items );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_iter_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_3 );
            Py_DECREF( tmp_iter_arg_3 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            {
                PyObject *old = tmp_for_loop_2__for_iterator;
                tmp_for_loop_2__for_iterator = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_for_loop_2__for_iterator );
            tmp_next_source_2 = tmp_for_loop_2__for_iterator;
            tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooo";
                    exception_lineno = 361;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_for_loop_2__iter_value;
                tmp_for_loop_2__iter_value = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_iter_arg_4;
            CHECK_OBJECT( tmp_for_loop_2__iter_value );
            tmp_iter_arg_4 = tmp_for_loop_2__iter_value;
            tmp_assign_source_10 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 361;
                type_description_1 = "oooo";
                goto try_except_handler_6;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__source_iter;
                tmp_tuple_unpack_2__source_iter = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
            if ( tmp_assign_source_11 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooo";
                exception_lineno = 361;
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_1;
                tmp_tuple_unpack_2__element_1 = tmp_assign_source_11;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
            tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
            if ( tmp_assign_source_12 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooo";
                exception_lineno = 361;
                goto try_except_handler_7;
            }
            {
                PyObject *old = tmp_tuple_unpack_2__element_2;
                tmp_tuple_unpack_2__element_2 = tmp_assign_source_12;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_2;
            CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
            tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooo";
                        exception_lineno = 361;
                        goto try_except_handler_7;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooo";
                exception_lineno = 361;
                goto try_except_handler_7;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_6;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
        Py_DECREF( tmp_tuple_unpack_2__source_iter );
        tmp_tuple_unpack_2__source_iter = NULL;

        {
            PyObject *tmp_assign_source_13;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
            tmp_assign_source_13 = tmp_tuple_unpack_2__element_1;
            {
                PyObject *old = var_sk;
                var_sk = tmp_assign_source_13;
                Py_INCREF( var_sk );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_1 );
        tmp_tuple_unpack_2__element_1 = NULL;

        {
            PyObject *tmp_assign_source_14;
            CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
            tmp_assign_source_14 = tmp_tuple_unpack_2__element_2;
            {
                PyObject *old = var_sv;
                var_sv = tmp_assign_source_14;
                Py_INCREF( var_sv );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_2__element_2 );
        tmp_tuple_unpack_2__element_2 = NULL;

        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_ass_subscript_2;
            PyObject *tmp_tuple_element_1;
            CHECK_OBJECT( var_sk );
            tmp_ass_subvalue_2 = var_sk;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAGS );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAGS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 362;
                type_description_1 = "oooo";
                goto try_except_handler_5;
            }

            tmp_ass_subscribed_2 = tmp_mvar_value_3;
            CHECK_OBJECT( var_k );
            tmp_tuple_element_1 = var_k;
            tmp_ass_subscript_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_ass_subscript_2, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( var_sv );
            tmp_tuple_element_1 = var_sv;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_ass_subscript_2, 1, tmp_tuple_element_1 );
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            Py_DECREF( tmp_ass_subscript_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 362;
                type_description_1 = "oooo";
                goto try_except_handler_5;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 361;
            type_description_1 = "oooo";
            goto try_except_handler_5;
        }
        goto loop_start_2;
        loop_end_2:;
        goto try_end_5;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_2__iter_value );
        tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
        Py_DECREF( tmp_for_loop_2__for_iterator );
        tmp_for_loop_2__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_2;
        // End of try:
        try_end_5:;
        Py_XDECREF( tmp_for_loop_2__iter_value );
        tmp_for_loop_2__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
        Py_DECREF( tmp_for_loop_2__for_iterator );
        tmp_for_loop_2__for_iterator = NULL;

        branch_no_1:;
    }
    {
        PyObject *tmp_ass_subvalue_3;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_ass_subscribed_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_ass_subscript_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TagInfo );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TagInfo );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TagInfo" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 364;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_dircall_arg1_1 = tmp_mvar_value_4;
        CHECK_OBJECT( var_k );
        tmp_tuple_element_2 = var_k;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var_v );
        tmp_dircall_arg3_1 = var_v;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_ass_subvalue_3 = impl___internal__$$$function_5_complex_call_helper_pos_star_list( dir_call_args );
        }
        if ( tmp_ass_subvalue_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS_V2 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAGS_V2 );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAGS_V2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 364;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_ass_subscribed_3 = tmp_mvar_value_5;
        CHECK_OBJECT( var_k );
        tmp_ass_subscript_3 = var_k;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
        Py_DECREF( tmp_ass_subvalue_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 357;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3b49996fa0e4880fce8b7a0984bb2158 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3b49996fa0e4880fce8b7a0984bb2158 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3b49996fa0e4880fce8b7a0984bb2158, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3b49996fa0e4880fce8b7a0984bb2158->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3b49996fa0e4880fce8b7a0984bb2158, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3b49996fa0e4880fce8b7a0984bb2158,
        type_description_1,
        var_k,
        var_v,
        var_sk,
        var_sv
    );


    // Release cached frame.
    if ( frame_3b49996fa0e4880fce8b7a0984bb2158 == cache_frame_3b49996fa0e4880fce8b7a0984bb2158 )
    {
        Py_DECREF( frame_3b49996fa0e4880fce8b7a0984bb2158 );
    }
    cache_frame_3b49996fa0e4880fce8b7a0984bb2158 = NULL;

    assertFrameObject( frame_3b49996fa0e4880fce8b7a0984bb2158 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_4__populate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_sk );
    var_sk = NULL;

    Py_XDECREF( var_sv );
    var_sv = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_sk );
    var_sk = NULL;

    Py_XDECREF( var_sv );
    var_sv = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$TiffTags$$$function_4__populate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_1___new__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$TiffTags$$$function_1___new__,
        const_str_plain___new__,
#if PYTHON_VERSION >= 300
        const_str_digest_b6dc26ddce418bcacac57b367fb6f7e7,
#endif
        codeobj_da346643a0ba3d8d5d1634fbaa203cd1,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$TiffTags,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_2_cvt_enum(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$TiffTags$$$function_2_cvt_enum,
        const_str_plain_cvt_enum,
#if PYTHON_VERSION >= 300
        const_str_digest_6dad7c3e74a8f2791f9f9446a43b0c1f,
#endif
        codeobj_d46e6593214c94bf6a7e8880aad44632,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$TiffTags,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_3_lookup(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$TiffTags$$$function_3_lookup,
        const_str_plain_lookup,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4ddb20c1cc5e6b07c56f0704ee2f54b5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$TiffTags,
        const_str_digest_ad8cd88bda0fa81543a675928fabf523,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$TiffTags$$$function_4__populate(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$TiffTags$$$function_4__populate,
        const_str_plain__populate,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3b49996fa0e4880fce8b7a0984bb2158,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$TiffTags,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_PIL$TiffTags =
{
    PyModuleDef_HEAD_INIT,
    "PIL.TiffTags",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(PIL$TiffTags)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(PIL$TiffTags)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_PIL$TiffTags );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("PIL.TiffTags: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.TiffTags: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.TiffTags: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initPIL$TiffTags" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_PIL$TiffTags = Py_InitModule4(
        "PIL.TiffTags",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_PIL$TiffTags = PyModule_Create( &mdef_PIL$TiffTags );
#endif

    moduledict_PIL$TiffTags = MODULE_DICT( module_PIL$TiffTags );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_PIL$TiffTags,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_PIL$TiffTags,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$TiffTags,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$TiffTags,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_PIL$TiffTags );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_53d172ce5d71b7b445878c626394ea74, module_PIL$TiffTags );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    struct Nuitka_FrameObject *frame_e8b23506ab469c2d72bc10c38ef6aae5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_PIL$TiffTags_23 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_d29fc0804c1aaba60d7f980f77c77c96_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_d29fc0804c1aaba60d7f980f77c77c96_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_e8b23506ab469c2d72bc10c38ef6aae5 = MAKE_MODULE_FRAME( codeobj_e8b23506ab469c2d72bc10c38ef6aae5, module_PIL$TiffTags );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_e8b23506ab469c2d72bc10c38ef6aae5 );
    assert( Py_REFCNT( frame_e8b23506ab469c2d72bc10c38ef6aae5 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_collections;
        tmp_globals_name_1 = (PyObject *)moduledict_PIL$TiffTags;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_namedtuple_tuple;
        tmp_level_name_1 = const_int_0;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 20;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_namedtuple );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_namedtuple, tmp_assign_source_4 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_namedtuple );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_namedtuple );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 23;
        tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_acfea2da227d5def1afbf4e120841093_tuple, 0 ) );

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        tmp_assign_source_5 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_5, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_6 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_7;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_8 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_8;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_1;
            }
            tmp_tuple_element_2 = const_str_plain_TagInfo;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 23;
            tmp_assign_source_9 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_1;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_9;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_1;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 23;

                    goto try_except_handler_1;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 23;

                    goto try_except_handler_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 23;

                    goto try_except_handler_1;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 23;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_1;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_10;
            tmp_assign_source_10 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_10;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_11;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_PIL$TiffTags_23 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_53d172ce5d71b7b445878c626394ea74;
        tmp_res = PyObject_SetItem( locals_PIL$TiffTags_23, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_plain_TagInfo;
        tmp_res = PyObject_SetItem( locals_PIL$TiffTags_23, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;

            goto try_except_handler_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_d29fc0804c1aaba60d7f980f77c77c96_2, codeobj_d29fc0804c1aaba60d7f980f77c77c96, module_PIL$TiffTags, sizeof(void *) );
        frame_d29fc0804c1aaba60d7f980f77c77c96_2 = cache_frame_d29fc0804c1aaba60d7f980f77c77c96_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_d29fc0804c1aaba60d7f980f77c77c96_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_d29fc0804c1aaba60d7f980f77c77c96_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = PyList_New( 0 );
        tmp_res = PyObject_SetItem( locals_PIL$TiffTags_23, const_str_plain___slots__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_str_plain_unknown_none_none_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_PIL$TiffTags$$$function_1___new__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_PIL$TiffTags_23, const_str_plain___new__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 26;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$TiffTags$$$function_2_cvt_enum(  );



        tmp_res = PyObject_SetItem( locals_PIL$TiffTags_23, const_str_plain_cvt_enum, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d29fc0804c1aaba60d7f980f77c77c96_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d29fc0804c1aaba60d7f980f77c77c96_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_d29fc0804c1aaba60d7f980f77c77c96_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_d29fc0804c1aaba60d7f980f77c77c96_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_d29fc0804c1aaba60d7f980f77c77c96_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_d29fc0804c1aaba60d7f980f77c77c96_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_d29fc0804c1aaba60d7f980f77c77c96_2 == cache_frame_d29fc0804c1aaba60d7f980f77c77c96_2 )
        {
            Py_DECREF( frame_d29fc0804c1aaba60d7f980f77c77c96_2 );
        }
        cache_frame_d29fc0804c1aaba60d7f980f77c77c96_2 = NULL;

        assertFrameObject( frame_d29fc0804c1aaba60d7f980f77c77c96_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_3;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_PIL$TiffTags_23, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_3;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_3 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_TagInfo;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_PIL$TiffTags_23;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 23;
            tmp_assign_source_12 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_3;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_12;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_11 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_11 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$TiffTags );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        Py_DECREF( locals_PIL$TiffTags_23 );
        locals_PIL$TiffTags_23 = NULL;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_PIL$TiffTags_23 );
        locals_PIL$TiffTags_23 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$TiffTags );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( PIL$TiffTags );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 23;
        goto try_except_handler_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TagInfo, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_PIL$TiffTags$$$function_3_lookup(  );



        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_lookup, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = const_int_pos_1;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_BYTE, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = const_int_pos_2;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = const_int_pos_3;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = const_int_pos_4;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = const_int_pos_5;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = const_int_pos_6;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_BYTE, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = const_int_pos_7;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = const_int_pos_8;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_SHORT, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = const_int_pos_9;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_LONG, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = const_int_pos_10;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = const_int_pos_11;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_FLOAT, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = const_int_pos_12;
        UPDATE_STRING_DICT0( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_DOUBLE, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_tuple_element_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_tuple_element_7;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_tuple_element_8;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_tuple_element_10;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_tuple_element_11;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_tuple_element_12;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_tuple_element_13;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_tuple_element_14;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_tuple_element_15;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_tuple_element_16;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_tuple_element_17;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_tuple_element_18;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_tuple_element_19;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_tuple_element_20;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_tuple_element_21;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_tuple_element_22;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_tuple_element_23;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_tuple_element_24;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_tuple_element_25;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_tuple_element_26;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_tuple_element_27;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        PyObject *tmp_tuple_element_28;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_tuple_element_29;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_tuple_element_30;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_tuple_element_31;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_tuple_element_32;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_tuple_element_33;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        PyObject *tmp_tuple_element_34;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_tuple_element_35;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_tuple_element_36;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_tuple_element_37;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_tuple_element_38;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_tuple_element_39;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        PyObject *tmp_tuple_element_40;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_dict_key_37;
        PyObject *tmp_dict_value_37;
        PyObject *tmp_tuple_element_41;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_dict_key_38;
        PyObject *tmp_dict_value_38;
        PyObject *tmp_tuple_element_42;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_dict_key_39;
        PyObject *tmp_dict_value_39;
        PyObject *tmp_tuple_element_43;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_dict_key_40;
        PyObject *tmp_dict_value_40;
        PyObject *tmp_tuple_element_44;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_dict_key_41;
        PyObject *tmp_dict_value_41;
        PyObject *tmp_tuple_element_45;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_dict_key_42;
        PyObject *tmp_dict_value_42;
        PyObject *tmp_tuple_element_46;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_dict_key_43;
        PyObject *tmp_dict_value_43;
        PyObject *tmp_tuple_element_47;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_dict_key_44;
        PyObject *tmp_dict_value_44;
        PyObject *tmp_tuple_element_48;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_dict_key_45;
        PyObject *tmp_dict_value_45;
        PyObject *tmp_tuple_element_49;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_dict_key_46;
        PyObject *tmp_dict_value_46;
        PyObject *tmp_tuple_element_50;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_dict_key_47;
        PyObject *tmp_dict_value_47;
        PyObject *tmp_tuple_element_51;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_dict_key_48;
        PyObject *tmp_dict_value_48;
        PyObject *tmp_tuple_element_52;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_dict_key_49;
        PyObject *tmp_dict_value_49;
        PyObject *tmp_tuple_element_53;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_dict_key_50;
        PyObject *tmp_dict_value_50;
        PyObject *tmp_tuple_element_54;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_dict_key_51;
        PyObject *tmp_dict_value_51;
        PyObject *tmp_tuple_element_55;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_dict_key_52;
        PyObject *tmp_dict_value_52;
        PyObject *tmp_tuple_element_56;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_dict_key_53;
        PyObject *tmp_dict_value_53;
        PyObject *tmp_tuple_element_57;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_dict_key_54;
        PyObject *tmp_dict_value_54;
        PyObject *tmp_tuple_element_58;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_dict_key_55;
        PyObject *tmp_dict_value_55;
        PyObject *tmp_tuple_element_59;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_dict_key_56;
        PyObject *tmp_dict_value_56;
        PyObject *tmp_tuple_element_60;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_dict_key_57;
        PyObject *tmp_dict_value_57;
        PyObject *tmp_tuple_element_61;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_dict_key_58;
        PyObject *tmp_dict_value_58;
        PyObject *tmp_tuple_element_62;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_dict_key_59;
        PyObject *tmp_dict_value_59;
        PyObject *tmp_tuple_element_63;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_dict_key_60;
        PyObject *tmp_dict_value_60;
        PyObject *tmp_tuple_element_64;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_dict_key_61;
        PyObject *tmp_dict_value_61;
        PyObject *tmp_tuple_element_65;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_dict_key_62;
        PyObject *tmp_dict_value_62;
        PyObject *tmp_tuple_element_66;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_dict_key_63;
        PyObject *tmp_dict_value_63;
        PyObject *tmp_tuple_element_67;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_dict_key_64;
        PyObject *tmp_dict_value_64;
        PyObject *tmp_tuple_element_68;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_dict_key_65;
        PyObject *tmp_dict_value_65;
        PyObject *tmp_tuple_element_69;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_dict_key_66;
        PyObject *tmp_dict_value_66;
        PyObject *tmp_tuple_element_70;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_dict_key_67;
        PyObject *tmp_dict_value_67;
        PyObject *tmp_tuple_element_71;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_dict_key_68;
        PyObject *tmp_dict_value_68;
        PyObject *tmp_tuple_element_72;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_dict_key_69;
        PyObject *tmp_dict_value_69;
        PyObject *tmp_tuple_element_73;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_dict_key_70;
        PyObject *tmp_dict_value_70;
        PyObject *tmp_tuple_element_74;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_dict_key_71;
        PyObject *tmp_dict_value_71;
        PyObject *tmp_tuple_element_75;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_dict_key_72;
        PyObject *tmp_dict_value_72;
        PyObject *tmp_tuple_element_76;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_dict_key_73;
        PyObject *tmp_dict_value_73;
        PyObject *tmp_tuple_element_77;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_dict_key_74;
        PyObject *tmp_dict_value_74;
        PyObject *tmp_tuple_element_78;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_dict_key_75;
        PyObject *tmp_dict_value_75;
        PyObject *tmp_tuple_element_79;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_dict_key_76;
        PyObject *tmp_dict_value_76;
        PyObject *tmp_tuple_element_80;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_dict_key_77;
        PyObject *tmp_dict_value_77;
        PyObject *tmp_tuple_element_81;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_dict_key_78;
        PyObject *tmp_dict_value_78;
        PyObject *tmp_tuple_element_82;
        PyObject *tmp_mvar_value_81;
        PyObject *tmp_dict_key_79;
        PyObject *tmp_dict_value_79;
        PyObject *tmp_tuple_element_83;
        PyObject *tmp_mvar_value_82;
        PyObject *tmp_dict_key_80;
        PyObject *tmp_dict_value_80;
        PyObject *tmp_tuple_element_84;
        PyObject *tmp_mvar_value_83;
        PyObject *tmp_dict_key_81;
        PyObject *tmp_dict_value_81;
        PyObject *tmp_tuple_element_85;
        PyObject *tmp_mvar_value_84;
        PyObject *tmp_dict_key_82;
        PyObject *tmp_dict_value_82;
        PyObject *tmp_tuple_element_86;
        PyObject *tmp_mvar_value_85;
        PyObject *tmp_dict_key_83;
        PyObject *tmp_dict_value_83;
        PyObject *tmp_tuple_element_87;
        PyObject *tmp_mvar_value_86;
        PyObject *tmp_dict_key_84;
        PyObject *tmp_dict_value_84;
        PyObject *tmp_tuple_element_88;
        PyObject *tmp_mvar_value_87;
        PyObject *tmp_dict_key_85;
        PyObject *tmp_dict_value_85;
        PyObject *tmp_tuple_element_89;
        PyObject *tmp_mvar_value_88;
        PyObject *tmp_dict_key_86;
        PyObject *tmp_dict_value_86;
        PyObject *tmp_tuple_element_90;
        PyObject *tmp_mvar_value_89;
        PyObject *tmp_dict_key_87;
        PyObject *tmp_dict_value_87;
        PyObject *tmp_tuple_element_91;
        PyObject *tmp_mvar_value_90;
        PyObject *tmp_dict_key_88;
        PyObject *tmp_dict_value_88;
        PyObject *tmp_tuple_element_92;
        PyObject *tmp_mvar_value_91;
        PyObject *tmp_dict_key_89;
        PyObject *tmp_dict_value_89;
        PyObject *tmp_tuple_element_93;
        PyObject *tmp_mvar_value_92;
        PyObject *tmp_dict_key_90;
        PyObject *tmp_dict_value_90;
        PyObject *tmp_tuple_element_94;
        PyObject *tmp_mvar_value_93;
        PyObject *tmp_dict_key_91;
        PyObject *tmp_dict_value_91;
        PyObject *tmp_tuple_element_95;
        PyObject *tmp_mvar_value_94;
        PyObject *tmp_dict_key_92;
        PyObject *tmp_dict_value_92;
        PyObject *tmp_tuple_element_96;
        PyObject *tmp_mvar_value_95;
        PyObject *tmp_dict_key_93;
        PyObject *tmp_dict_value_93;
        PyObject *tmp_tuple_element_97;
        PyObject *tmp_mvar_value_96;
        PyObject *tmp_dict_key_94;
        PyObject *tmp_dict_value_94;
        PyObject *tmp_tuple_element_98;
        PyObject *tmp_mvar_value_97;
        PyObject *tmp_dict_key_95;
        PyObject *tmp_dict_value_95;
        PyObject *tmp_tuple_element_99;
        PyObject *tmp_mvar_value_98;
        PyObject *tmp_dict_key_96;
        PyObject *tmp_dict_value_96;
        PyObject *tmp_tuple_element_100;
        PyObject *tmp_mvar_value_99;
        PyObject *tmp_dict_key_97;
        PyObject *tmp_dict_value_97;
        PyObject *tmp_tuple_element_101;
        PyObject *tmp_mvar_value_100;
        PyObject *tmp_dict_key_98;
        PyObject *tmp_dict_value_98;
        PyObject *tmp_tuple_element_102;
        PyObject *tmp_mvar_value_101;
        PyObject *tmp_dict_key_99;
        PyObject *tmp_dict_value_99;
        PyObject *tmp_tuple_element_103;
        PyObject *tmp_mvar_value_102;
        PyObject *tmp_dict_key_100;
        PyObject *tmp_dict_value_100;
        PyObject *tmp_tuple_element_104;
        PyObject *tmp_mvar_value_103;
        PyObject *tmp_dict_key_101;
        PyObject *tmp_dict_value_101;
        PyObject *tmp_tuple_element_105;
        PyObject *tmp_mvar_value_104;
        PyObject *tmp_dict_key_102;
        PyObject *tmp_dict_value_102;
        PyObject *tmp_tuple_element_106;
        PyObject *tmp_mvar_value_105;
        PyObject *tmp_dict_key_103;
        PyObject *tmp_dict_value_103;
        PyObject *tmp_tuple_element_107;
        PyObject *tmp_mvar_value_106;
        tmp_dict_key_1 = const_int_pos_254;
        tmp_tuple_element_5 = const_str_plain_NewSubfileType;
        tmp_dict_value_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 0, tmp_tuple_element_5 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_tuple_element_5 = tmp_mvar_value_4;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 1, tmp_tuple_element_5 );
        tmp_tuple_element_5 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_dict_value_1, 2, tmp_tuple_element_5 );
        tmp_assign_source_26 = _PyDict_NewPresized( 103 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_int_pos_255;
        tmp_tuple_element_6 = const_str_plain_SubfileType;
        tmp_dict_value_2 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_dict_value_2, 0, tmp_tuple_element_6 );
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_tuple_element_6 = tmp_mvar_value_5;
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_dict_value_2, 1, tmp_tuple_element_6 );
        tmp_tuple_element_6 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_dict_value_2, 2, tmp_tuple_element_6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_int_pos_256;
        tmp_tuple_element_7 = const_str_plain_ImageWidth;
        tmp_dict_value_3 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_dict_value_3, 0, tmp_tuple_element_7 );
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_tuple_element_7 = tmp_mvar_value_6;
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_dict_value_3, 1, tmp_tuple_element_7 );
        tmp_tuple_element_7 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_dict_value_3, 2, tmp_tuple_element_7 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_int_pos_257;
        tmp_tuple_element_8 = const_str_plain_ImageLength;
        tmp_dict_value_4 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_dict_value_4, 0, tmp_tuple_element_8 );
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_7 );
        tmp_tuple_element_8 = tmp_mvar_value_7;
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_dict_value_4, 1, tmp_tuple_element_8 );
        tmp_tuple_element_8 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_dict_value_4, 2, tmp_tuple_element_8 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_int_pos_258;
        tmp_tuple_element_9 = const_str_plain_BitsPerSample;
        tmp_dict_value_5 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_dict_value_5, 0, tmp_tuple_element_9 );
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_8 );
        tmp_tuple_element_9 = tmp_mvar_value_8;
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_dict_value_5, 1, tmp_tuple_element_9 );
        tmp_tuple_element_9 = const_int_0;
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_dict_value_5, 2, tmp_tuple_element_9 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_5, tmp_dict_value_5 );
        Py_DECREF( tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_int_pos_259;
        tmp_tuple_element_10 = const_str_plain_Compression;
        tmp_dict_value_6 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_10 );
        PyTuple_SET_ITEM( tmp_dict_value_6, 0, tmp_tuple_element_10 );
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_tuple_element_10 = tmp_mvar_value_9;
        Py_INCREF( tmp_tuple_element_10 );
        PyTuple_SET_ITEM( tmp_dict_value_6, 1, tmp_tuple_element_10 );
        tmp_tuple_element_10 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_10 );
        PyTuple_SET_ITEM( tmp_dict_value_6, 2, tmp_tuple_element_10 );
        tmp_tuple_element_10 = PyDict_Copy( const_dict_5dc73ef09b7ff8a9b0d93d7297f9537c );
        PyTuple_SET_ITEM( tmp_dict_value_6, 3, tmp_tuple_element_10 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_6, tmp_dict_value_6 );
        Py_DECREF( tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_int_pos_262;
        tmp_tuple_element_11 = const_str_plain_PhotometricInterpretation;
        tmp_dict_value_7 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_11 );
        PyTuple_SET_ITEM( tmp_dict_value_7, 0, tmp_tuple_element_11 );
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_tuple_element_11 = tmp_mvar_value_10;
        Py_INCREF( tmp_tuple_element_11 );
        PyTuple_SET_ITEM( tmp_dict_value_7, 1, tmp_tuple_element_11 );
        tmp_tuple_element_11 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_11 );
        PyTuple_SET_ITEM( tmp_dict_value_7, 2, tmp_tuple_element_11 );
        tmp_tuple_element_11 = PyDict_Copy( const_dict_39749579f943d31b5ff968e26bc0ca5a );
        PyTuple_SET_ITEM( tmp_dict_value_7, 3, tmp_tuple_element_11 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_7, tmp_dict_value_7 );
        Py_DECREF( tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_int_pos_263;
        tmp_tuple_element_12 = const_str_plain_Threshholding;
        tmp_dict_value_8 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_12 );
        PyTuple_SET_ITEM( tmp_dict_value_8, 0, tmp_tuple_element_12 );
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_11 );
        tmp_tuple_element_12 = tmp_mvar_value_11;
        Py_INCREF( tmp_tuple_element_12 );
        PyTuple_SET_ITEM( tmp_dict_value_8, 1, tmp_tuple_element_12 );
        tmp_tuple_element_12 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_12 );
        PyTuple_SET_ITEM( tmp_dict_value_8, 2, tmp_tuple_element_12 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_8, tmp_dict_value_8 );
        Py_DECREF( tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_int_pos_264;
        tmp_tuple_element_13 = const_str_plain_CellWidth;
        tmp_dict_value_9 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_dict_value_9, 0, tmp_tuple_element_13 );
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_12 );
        tmp_tuple_element_13 = tmp_mvar_value_12;
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_dict_value_9, 1, tmp_tuple_element_13 );
        tmp_tuple_element_13 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_dict_value_9, 2, tmp_tuple_element_13 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_9, tmp_dict_value_9 );
        Py_DECREF( tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_int_pos_265;
        tmp_tuple_element_14 = const_str_plain_CellLength;
        tmp_dict_value_10 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_14 );
        PyTuple_SET_ITEM( tmp_dict_value_10, 0, tmp_tuple_element_14 );
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_13 );
        tmp_tuple_element_14 = tmp_mvar_value_13;
        Py_INCREF( tmp_tuple_element_14 );
        PyTuple_SET_ITEM( tmp_dict_value_10, 1, tmp_tuple_element_14 );
        tmp_tuple_element_14 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_14 );
        PyTuple_SET_ITEM( tmp_dict_value_10, 2, tmp_tuple_element_14 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_10, tmp_dict_value_10 );
        Py_DECREF( tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_int_pos_266;
        tmp_tuple_element_15 = const_str_plain_FillOrder;
        tmp_dict_value_11 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_15 );
        PyTuple_SET_ITEM( tmp_dict_value_11, 0, tmp_tuple_element_15 );
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_14 );
        tmp_tuple_element_15 = tmp_mvar_value_14;
        Py_INCREF( tmp_tuple_element_15 );
        PyTuple_SET_ITEM( tmp_dict_value_11, 1, tmp_tuple_element_15 );
        tmp_tuple_element_15 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_15 );
        PyTuple_SET_ITEM( tmp_dict_value_11, 2, tmp_tuple_element_15 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_11, tmp_dict_value_11 );
        Py_DECREF( tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_int_pos_269;
        tmp_tuple_element_16 = const_str_plain_DocumentName;
        tmp_dict_value_12 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_16 );
        PyTuple_SET_ITEM( tmp_dict_value_12, 0, tmp_tuple_element_16 );
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_15 );
        tmp_tuple_element_16 = tmp_mvar_value_15;
        Py_INCREF( tmp_tuple_element_16 );
        PyTuple_SET_ITEM( tmp_dict_value_12, 1, tmp_tuple_element_16 );
        tmp_tuple_element_16 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_16 );
        PyTuple_SET_ITEM( tmp_dict_value_12, 2, tmp_tuple_element_16 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_12, tmp_dict_value_12 );
        Py_DECREF( tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_13 = const_int_pos_270;
        tmp_tuple_element_17 = const_str_plain_ImageDescription;
        tmp_dict_value_13 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_17 );
        PyTuple_SET_ITEM( tmp_dict_value_13, 0, tmp_tuple_element_17 );
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_16 );
        tmp_tuple_element_17 = tmp_mvar_value_16;
        Py_INCREF( tmp_tuple_element_17 );
        PyTuple_SET_ITEM( tmp_dict_value_13, 1, tmp_tuple_element_17 );
        tmp_tuple_element_17 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_17 );
        PyTuple_SET_ITEM( tmp_dict_value_13, 2, tmp_tuple_element_17 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_13, tmp_dict_value_13 );
        Py_DECREF( tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_int_pos_271;
        tmp_tuple_element_18 = const_str_plain_Make;
        tmp_dict_value_14 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_18 );
        PyTuple_SET_ITEM( tmp_dict_value_14, 0, tmp_tuple_element_18 );
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_17 );
        tmp_tuple_element_18 = tmp_mvar_value_17;
        Py_INCREF( tmp_tuple_element_18 );
        PyTuple_SET_ITEM( tmp_dict_value_14, 1, tmp_tuple_element_18 );
        tmp_tuple_element_18 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_18 );
        PyTuple_SET_ITEM( tmp_dict_value_14, 2, tmp_tuple_element_18 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_14, tmp_dict_value_14 );
        Py_DECREF( tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_int_pos_272;
        tmp_tuple_element_19 = const_str_plain_Model;
        tmp_dict_value_15 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_19 );
        PyTuple_SET_ITEM( tmp_dict_value_15, 0, tmp_tuple_element_19 );
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_18 );
        tmp_tuple_element_19 = tmp_mvar_value_18;
        Py_INCREF( tmp_tuple_element_19 );
        PyTuple_SET_ITEM( tmp_dict_value_15, 1, tmp_tuple_element_19 );
        tmp_tuple_element_19 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_19 );
        PyTuple_SET_ITEM( tmp_dict_value_15, 2, tmp_tuple_element_19 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_16 = const_int_pos_273;
        tmp_tuple_element_20 = const_str_plain_StripOffsets;
        tmp_dict_value_16 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_20 );
        PyTuple_SET_ITEM( tmp_dict_value_16, 0, tmp_tuple_element_20 );
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_19 );
        tmp_tuple_element_20 = tmp_mvar_value_19;
        Py_INCREF( tmp_tuple_element_20 );
        PyTuple_SET_ITEM( tmp_dict_value_16, 1, tmp_tuple_element_20 );
        tmp_tuple_element_20 = const_int_0;
        Py_INCREF( tmp_tuple_element_20 );
        PyTuple_SET_ITEM( tmp_dict_value_16, 2, tmp_tuple_element_20 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_int_pos_274;
        tmp_tuple_element_21 = const_str_plain_Orientation;
        tmp_dict_value_17 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_21 );
        PyTuple_SET_ITEM( tmp_dict_value_17, 0, tmp_tuple_element_21 );
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_20 );
        tmp_tuple_element_21 = tmp_mvar_value_20;
        Py_INCREF( tmp_tuple_element_21 );
        PyTuple_SET_ITEM( tmp_dict_value_17, 1, tmp_tuple_element_21 );
        tmp_tuple_element_21 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_21 );
        PyTuple_SET_ITEM( tmp_dict_value_17, 2, tmp_tuple_element_21 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_17, tmp_dict_value_17 );
        Py_DECREF( tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_int_pos_277;
        tmp_tuple_element_22 = const_str_plain_SamplesPerPixel;
        tmp_dict_value_18 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_22 );
        PyTuple_SET_ITEM( tmp_dict_value_18, 0, tmp_tuple_element_22 );
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_21 );
        tmp_tuple_element_22 = tmp_mvar_value_21;
        Py_INCREF( tmp_tuple_element_22 );
        PyTuple_SET_ITEM( tmp_dict_value_18, 1, tmp_tuple_element_22 );
        tmp_tuple_element_22 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_22 );
        PyTuple_SET_ITEM( tmp_dict_value_18, 2, tmp_tuple_element_22 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_18, tmp_dict_value_18 );
        Py_DECREF( tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_19 = const_int_pos_278;
        tmp_tuple_element_23 = const_str_plain_RowsPerStrip;
        tmp_dict_value_19 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_23 );
        PyTuple_SET_ITEM( tmp_dict_value_19, 0, tmp_tuple_element_23 );
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_22 );
        tmp_tuple_element_23 = tmp_mvar_value_22;
        Py_INCREF( tmp_tuple_element_23 );
        PyTuple_SET_ITEM( tmp_dict_value_19, 1, tmp_tuple_element_23 );
        tmp_tuple_element_23 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_23 );
        PyTuple_SET_ITEM( tmp_dict_value_19, 2, tmp_tuple_element_23 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_19, tmp_dict_value_19 );
        Py_DECREF( tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_20 = const_int_pos_279;
        tmp_tuple_element_24 = const_str_plain_StripByteCounts;
        tmp_dict_value_20 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_24 );
        PyTuple_SET_ITEM( tmp_dict_value_20, 0, tmp_tuple_element_24 );
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_23 );
        tmp_tuple_element_24 = tmp_mvar_value_23;
        Py_INCREF( tmp_tuple_element_24 );
        PyTuple_SET_ITEM( tmp_dict_value_20, 1, tmp_tuple_element_24 );
        tmp_tuple_element_24 = const_int_0;
        Py_INCREF( tmp_tuple_element_24 );
        PyTuple_SET_ITEM( tmp_dict_value_20, 2, tmp_tuple_element_24 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_int_pos_280;
        tmp_tuple_element_25 = const_str_plain_MinSampleValue;
        tmp_dict_value_21 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_25 );
        PyTuple_SET_ITEM( tmp_dict_value_21, 0, tmp_tuple_element_25 );
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_24 );
        tmp_tuple_element_25 = tmp_mvar_value_24;
        Py_INCREF( tmp_tuple_element_25 );
        PyTuple_SET_ITEM( tmp_dict_value_21, 1, tmp_tuple_element_25 );
        tmp_tuple_element_25 = const_int_0;
        Py_INCREF( tmp_tuple_element_25 );
        PyTuple_SET_ITEM( tmp_dict_value_21, 2, tmp_tuple_element_25 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_21, tmp_dict_value_21 );
        Py_DECREF( tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_22 = const_int_pos_281;
        tmp_tuple_element_26 = const_str_plain_MaxSampleValue;
        tmp_dict_value_22 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_26 );
        PyTuple_SET_ITEM( tmp_dict_value_22, 0, tmp_tuple_element_26 );
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_25 );
        tmp_tuple_element_26 = tmp_mvar_value_25;
        Py_INCREF( tmp_tuple_element_26 );
        PyTuple_SET_ITEM( tmp_dict_value_22, 1, tmp_tuple_element_26 );
        tmp_tuple_element_26 = const_int_0;
        Py_INCREF( tmp_tuple_element_26 );
        PyTuple_SET_ITEM( tmp_dict_value_22, 2, tmp_tuple_element_26 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_22, tmp_dict_value_22 );
        Py_DECREF( tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_int_pos_282;
        tmp_tuple_element_27 = const_str_plain_XResolution;
        tmp_dict_value_23 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_27 );
        PyTuple_SET_ITEM( tmp_dict_value_23, 0, tmp_tuple_element_27 );
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_26 );
        tmp_tuple_element_27 = tmp_mvar_value_26;
        Py_INCREF( tmp_tuple_element_27 );
        PyTuple_SET_ITEM( tmp_dict_value_23, 1, tmp_tuple_element_27 );
        tmp_tuple_element_27 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_27 );
        PyTuple_SET_ITEM( tmp_dict_value_23, 2, tmp_tuple_element_27 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_23, tmp_dict_value_23 );
        Py_DECREF( tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_int_pos_283;
        tmp_tuple_element_28 = const_str_plain_YResolution;
        tmp_dict_value_24 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_28 );
        PyTuple_SET_ITEM( tmp_dict_value_24, 0, tmp_tuple_element_28 );
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_27 );
        tmp_tuple_element_28 = tmp_mvar_value_27;
        Py_INCREF( tmp_tuple_element_28 );
        PyTuple_SET_ITEM( tmp_dict_value_24, 1, tmp_tuple_element_28 );
        tmp_tuple_element_28 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_28 );
        PyTuple_SET_ITEM( tmp_dict_value_24, 2, tmp_tuple_element_28 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_24, tmp_dict_value_24 );
        Py_DECREF( tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_25 = const_int_pos_284;
        tmp_tuple_element_29 = const_str_plain_PlanarConfiguration;
        tmp_dict_value_25 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_29 );
        PyTuple_SET_ITEM( tmp_dict_value_25, 0, tmp_tuple_element_29 );
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_28 );
        tmp_tuple_element_29 = tmp_mvar_value_28;
        Py_INCREF( tmp_tuple_element_29 );
        PyTuple_SET_ITEM( tmp_dict_value_25, 1, tmp_tuple_element_29 );
        tmp_tuple_element_29 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_29 );
        PyTuple_SET_ITEM( tmp_dict_value_25, 2, tmp_tuple_element_29 );
        tmp_tuple_element_29 = PyDict_Copy( const_dict_d42350037eae70df4c65014dffbfed19 );
        PyTuple_SET_ITEM( tmp_dict_value_25, 3, tmp_tuple_element_29 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_25, tmp_dict_value_25 );
        Py_DECREF( tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_int_pos_285;
        tmp_tuple_element_30 = const_str_plain_PageName;
        tmp_dict_value_26 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_30 );
        PyTuple_SET_ITEM( tmp_dict_value_26, 0, tmp_tuple_element_30 );
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_29 );
        tmp_tuple_element_30 = tmp_mvar_value_29;
        Py_INCREF( tmp_tuple_element_30 );
        PyTuple_SET_ITEM( tmp_dict_value_26, 1, tmp_tuple_element_30 );
        tmp_tuple_element_30 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_30 );
        PyTuple_SET_ITEM( tmp_dict_value_26, 2, tmp_tuple_element_30 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_26, tmp_dict_value_26 );
        Py_DECREF( tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_int_pos_286;
        tmp_tuple_element_31 = const_str_plain_XPosition;
        tmp_dict_value_27 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_31 );
        PyTuple_SET_ITEM( tmp_dict_value_27, 0, tmp_tuple_element_31 );
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_30 );
        tmp_tuple_element_31 = tmp_mvar_value_30;
        Py_INCREF( tmp_tuple_element_31 );
        PyTuple_SET_ITEM( tmp_dict_value_27, 1, tmp_tuple_element_31 );
        tmp_tuple_element_31 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_31 );
        PyTuple_SET_ITEM( tmp_dict_value_27, 2, tmp_tuple_element_31 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_27, tmp_dict_value_27 );
        Py_DECREF( tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_28 = const_int_pos_287;
        tmp_tuple_element_32 = const_str_plain_YPosition;
        tmp_dict_value_28 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_32 );
        PyTuple_SET_ITEM( tmp_dict_value_28, 0, tmp_tuple_element_32 );
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_31 );
        tmp_tuple_element_32 = tmp_mvar_value_31;
        Py_INCREF( tmp_tuple_element_32 );
        PyTuple_SET_ITEM( tmp_dict_value_28, 1, tmp_tuple_element_32 );
        tmp_tuple_element_32 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_32 );
        PyTuple_SET_ITEM( tmp_dict_value_28, 2, tmp_tuple_element_32 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_28, tmp_dict_value_28 );
        Py_DECREF( tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_int_pos_288;
        tmp_tuple_element_33 = const_str_plain_FreeOffsets;
        tmp_dict_value_29 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_33 );
        PyTuple_SET_ITEM( tmp_dict_value_29, 0, tmp_tuple_element_33 );
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_32 );
        tmp_tuple_element_33 = tmp_mvar_value_32;
        Py_INCREF( tmp_tuple_element_33 );
        PyTuple_SET_ITEM( tmp_dict_value_29, 1, tmp_tuple_element_33 );
        tmp_tuple_element_33 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_33 );
        PyTuple_SET_ITEM( tmp_dict_value_29, 2, tmp_tuple_element_33 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_29, tmp_dict_value_29 );
        Py_DECREF( tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_30 = const_int_pos_289;
        tmp_tuple_element_34 = const_str_plain_FreeByteCounts;
        tmp_dict_value_30 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_34 );
        PyTuple_SET_ITEM( tmp_dict_value_30, 0, tmp_tuple_element_34 );
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_33 );
        tmp_tuple_element_34 = tmp_mvar_value_33;
        Py_INCREF( tmp_tuple_element_34 );
        PyTuple_SET_ITEM( tmp_dict_value_30, 1, tmp_tuple_element_34 );
        tmp_tuple_element_34 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_34 );
        PyTuple_SET_ITEM( tmp_dict_value_30, 2, tmp_tuple_element_34 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_30, tmp_dict_value_30 );
        Py_DECREF( tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_31 = const_int_pos_290;
        tmp_tuple_element_35 = const_str_plain_GrayResponseUnit;
        tmp_dict_value_31 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_35 );
        PyTuple_SET_ITEM( tmp_dict_value_31, 0, tmp_tuple_element_35 );
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_34 );
        tmp_tuple_element_35 = tmp_mvar_value_34;
        Py_INCREF( tmp_tuple_element_35 );
        PyTuple_SET_ITEM( tmp_dict_value_31, 1, tmp_tuple_element_35 );
        tmp_tuple_element_35 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_35 );
        PyTuple_SET_ITEM( tmp_dict_value_31, 2, tmp_tuple_element_35 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_31, tmp_dict_value_31 );
        Py_DECREF( tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_32 = const_int_pos_291;
        tmp_tuple_element_36 = const_str_plain_GrayResponseCurve;
        tmp_dict_value_32 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_36 );
        PyTuple_SET_ITEM( tmp_dict_value_32, 0, tmp_tuple_element_36 );
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_35 );
        tmp_tuple_element_36 = tmp_mvar_value_35;
        Py_INCREF( tmp_tuple_element_36 );
        PyTuple_SET_ITEM( tmp_dict_value_32, 1, tmp_tuple_element_36 );
        tmp_tuple_element_36 = const_int_0;
        Py_INCREF( tmp_tuple_element_36 );
        PyTuple_SET_ITEM( tmp_dict_value_32, 2, tmp_tuple_element_36 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_32, tmp_dict_value_32 );
        Py_DECREF( tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_33 = const_int_pos_292;
        tmp_tuple_element_37 = const_str_plain_T4Options;
        tmp_dict_value_33 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_37 );
        PyTuple_SET_ITEM( tmp_dict_value_33, 0, tmp_tuple_element_37 );
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_36 );
        tmp_tuple_element_37 = tmp_mvar_value_36;
        Py_INCREF( tmp_tuple_element_37 );
        PyTuple_SET_ITEM( tmp_dict_value_33, 1, tmp_tuple_element_37 );
        tmp_tuple_element_37 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_37 );
        PyTuple_SET_ITEM( tmp_dict_value_33, 2, tmp_tuple_element_37 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_33, tmp_dict_value_33 );
        Py_DECREF( tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_34 = const_int_pos_293;
        tmp_tuple_element_38 = const_str_plain_T6Options;
        tmp_dict_value_34 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_38 );
        PyTuple_SET_ITEM( tmp_dict_value_34, 0, tmp_tuple_element_38 );
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_37 );
        tmp_tuple_element_38 = tmp_mvar_value_37;
        Py_INCREF( tmp_tuple_element_38 );
        PyTuple_SET_ITEM( tmp_dict_value_34, 1, tmp_tuple_element_38 );
        tmp_tuple_element_38 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_38 );
        PyTuple_SET_ITEM( tmp_dict_value_34, 2, tmp_tuple_element_38 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_34, tmp_dict_value_34 );
        Py_DECREF( tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_35 = const_int_pos_296;
        tmp_tuple_element_39 = const_str_plain_ResolutionUnit;
        tmp_dict_value_35 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_39 );
        PyTuple_SET_ITEM( tmp_dict_value_35, 0, tmp_tuple_element_39 );
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_38 );
        tmp_tuple_element_39 = tmp_mvar_value_38;
        Py_INCREF( tmp_tuple_element_39 );
        PyTuple_SET_ITEM( tmp_dict_value_35, 1, tmp_tuple_element_39 );
        tmp_tuple_element_39 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_39 );
        PyTuple_SET_ITEM( tmp_dict_value_35, 2, tmp_tuple_element_39 );
        tmp_tuple_element_39 = PyDict_Copy( const_dict_22dc888c657875abc535e5e13e9ac7c5 );
        PyTuple_SET_ITEM( tmp_dict_value_35, 3, tmp_tuple_element_39 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_35, tmp_dict_value_35 );
        Py_DECREF( tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_36 = const_int_pos_297;
        tmp_tuple_element_40 = const_str_plain_PageNumber;
        tmp_dict_value_36 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_40 );
        PyTuple_SET_ITEM( tmp_dict_value_36, 0, tmp_tuple_element_40 );
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_39 );
        tmp_tuple_element_40 = tmp_mvar_value_39;
        Py_INCREF( tmp_tuple_element_40 );
        PyTuple_SET_ITEM( tmp_dict_value_36, 1, tmp_tuple_element_40 );
        tmp_tuple_element_40 = const_int_pos_2;
        Py_INCREF( tmp_tuple_element_40 );
        PyTuple_SET_ITEM( tmp_dict_value_36, 2, tmp_tuple_element_40 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_36, tmp_dict_value_36 );
        Py_DECREF( tmp_dict_value_36 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_37 = const_int_pos_301;
        tmp_tuple_element_41 = const_str_plain_TransferFunction;
        tmp_dict_value_37 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_41 );
        PyTuple_SET_ITEM( tmp_dict_value_37, 0, tmp_tuple_element_41 );
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_40 );
        tmp_tuple_element_41 = tmp_mvar_value_40;
        Py_INCREF( tmp_tuple_element_41 );
        PyTuple_SET_ITEM( tmp_dict_value_37, 1, tmp_tuple_element_41 );
        tmp_tuple_element_41 = const_int_0;
        Py_INCREF( tmp_tuple_element_41 );
        PyTuple_SET_ITEM( tmp_dict_value_37, 2, tmp_tuple_element_41 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_37, tmp_dict_value_37 );
        Py_DECREF( tmp_dict_value_37 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_38 = const_int_pos_305;
        tmp_tuple_element_42 = const_str_plain_Software;
        tmp_dict_value_38 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_42 );
        PyTuple_SET_ITEM( tmp_dict_value_38, 0, tmp_tuple_element_42 );
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_41 );
        tmp_tuple_element_42 = tmp_mvar_value_41;
        Py_INCREF( tmp_tuple_element_42 );
        PyTuple_SET_ITEM( tmp_dict_value_38, 1, tmp_tuple_element_42 );
        tmp_tuple_element_42 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_42 );
        PyTuple_SET_ITEM( tmp_dict_value_38, 2, tmp_tuple_element_42 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_38, tmp_dict_value_38 );
        Py_DECREF( tmp_dict_value_38 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_39 = const_int_pos_306;
        tmp_tuple_element_43 = const_str_plain_DateTime;
        tmp_dict_value_39 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_43 );
        PyTuple_SET_ITEM( tmp_dict_value_39, 0, tmp_tuple_element_43 );
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_42 );
        tmp_tuple_element_43 = tmp_mvar_value_42;
        Py_INCREF( tmp_tuple_element_43 );
        PyTuple_SET_ITEM( tmp_dict_value_39, 1, tmp_tuple_element_43 );
        tmp_tuple_element_43 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_43 );
        PyTuple_SET_ITEM( tmp_dict_value_39, 2, tmp_tuple_element_43 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_39, tmp_dict_value_39 );
        Py_DECREF( tmp_dict_value_39 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_40 = const_int_pos_315;
        tmp_tuple_element_44 = const_str_plain_Artist;
        tmp_dict_value_40 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_44 );
        PyTuple_SET_ITEM( tmp_dict_value_40, 0, tmp_tuple_element_44 );
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_43 );
        tmp_tuple_element_44 = tmp_mvar_value_43;
        Py_INCREF( tmp_tuple_element_44 );
        PyTuple_SET_ITEM( tmp_dict_value_40, 1, tmp_tuple_element_44 );
        tmp_tuple_element_44 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_44 );
        PyTuple_SET_ITEM( tmp_dict_value_40, 2, tmp_tuple_element_44 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_40, tmp_dict_value_40 );
        Py_DECREF( tmp_dict_value_40 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_41 = const_int_pos_316;
        tmp_tuple_element_45 = const_str_plain_HostComputer;
        tmp_dict_value_41 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_45 );
        PyTuple_SET_ITEM( tmp_dict_value_41, 0, tmp_tuple_element_45 );
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_44 );
        tmp_tuple_element_45 = tmp_mvar_value_44;
        Py_INCREF( tmp_tuple_element_45 );
        PyTuple_SET_ITEM( tmp_dict_value_41, 1, tmp_tuple_element_45 );
        tmp_tuple_element_45 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_45 );
        PyTuple_SET_ITEM( tmp_dict_value_41, 2, tmp_tuple_element_45 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_41, tmp_dict_value_41 );
        Py_DECREF( tmp_dict_value_41 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_42 = const_int_pos_317;
        tmp_tuple_element_46 = const_str_plain_Predictor;
        tmp_dict_value_42 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_46 );
        PyTuple_SET_ITEM( tmp_dict_value_42, 0, tmp_tuple_element_46 );
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_45 );
        tmp_tuple_element_46 = tmp_mvar_value_45;
        Py_INCREF( tmp_tuple_element_46 );
        PyTuple_SET_ITEM( tmp_dict_value_42, 1, tmp_tuple_element_46 );
        tmp_tuple_element_46 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_46 );
        PyTuple_SET_ITEM( tmp_dict_value_42, 2, tmp_tuple_element_46 );
        tmp_tuple_element_46 = PyDict_Copy( const_dict_24f46532581234f3874adc3a050bd90e );
        PyTuple_SET_ITEM( tmp_dict_value_42, 3, tmp_tuple_element_46 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_42, tmp_dict_value_42 );
        Py_DECREF( tmp_dict_value_42 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_43 = const_int_pos_318;
        tmp_tuple_element_47 = const_str_plain_WhitePoint;
        tmp_dict_value_43 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_47 );
        PyTuple_SET_ITEM( tmp_dict_value_43, 0, tmp_tuple_element_47 );
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_46 );
        tmp_tuple_element_47 = tmp_mvar_value_46;
        Py_INCREF( tmp_tuple_element_47 );
        PyTuple_SET_ITEM( tmp_dict_value_43, 1, tmp_tuple_element_47 );
        tmp_tuple_element_47 = const_int_pos_2;
        Py_INCREF( tmp_tuple_element_47 );
        PyTuple_SET_ITEM( tmp_dict_value_43, 2, tmp_tuple_element_47 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_43, tmp_dict_value_43 );
        Py_DECREF( tmp_dict_value_43 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_44 = const_int_pos_319;
        tmp_tuple_element_48 = const_str_plain_PrimaryChromaticities;
        tmp_dict_value_44 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_48 );
        PyTuple_SET_ITEM( tmp_dict_value_44, 0, tmp_tuple_element_48 );
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_47 );
        tmp_tuple_element_48 = tmp_mvar_value_47;
        Py_INCREF( tmp_tuple_element_48 );
        PyTuple_SET_ITEM( tmp_dict_value_44, 1, tmp_tuple_element_48 );
        tmp_tuple_element_48 = const_int_pos_6;
        Py_INCREF( tmp_tuple_element_48 );
        PyTuple_SET_ITEM( tmp_dict_value_44, 2, tmp_tuple_element_48 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_44, tmp_dict_value_44 );
        Py_DECREF( tmp_dict_value_44 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_45 = const_int_pos_320;
        tmp_tuple_element_49 = const_str_plain_ColorMap;
        tmp_dict_value_45 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_49 );
        PyTuple_SET_ITEM( tmp_dict_value_45, 0, tmp_tuple_element_49 );
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_48 );
        tmp_tuple_element_49 = tmp_mvar_value_48;
        Py_INCREF( tmp_tuple_element_49 );
        PyTuple_SET_ITEM( tmp_dict_value_45, 1, tmp_tuple_element_49 );
        tmp_tuple_element_49 = const_int_0;
        Py_INCREF( tmp_tuple_element_49 );
        PyTuple_SET_ITEM( tmp_dict_value_45, 2, tmp_tuple_element_49 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_45, tmp_dict_value_45 );
        Py_DECREF( tmp_dict_value_45 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_46 = const_int_pos_321;
        tmp_tuple_element_50 = const_str_plain_HalftoneHints;
        tmp_dict_value_46 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_50 );
        PyTuple_SET_ITEM( tmp_dict_value_46, 0, tmp_tuple_element_50 );
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_49 );
        tmp_tuple_element_50 = tmp_mvar_value_49;
        Py_INCREF( tmp_tuple_element_50 );
        PyTuple_SET_ITEM( tmp_dict_value_46, 1, tmp_tuple_element_50 );
        tmp_tuple_element_50 = const_int_pos_2;
        Py_INCREF( tmp_tuple_element_50 );
        PyTuple_SET_ITEM( tmp_dict_value_46, 2, tmp_tuple_element_50 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_46, tmp_dict_value_46 );
        Py_DECREF( tmp_dict_value_46 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_47 = const_int_pos_322;
        tmp_tuple_element_51 = const_str_plain_TileWidth;
        tmp_dict_value_47 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_51 );
        PyTuple_SET_ITEM( tmp_dict_value_47, 0, tmp_tuple_element_51 );
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_50 );
        tmp_tuple_element_51 = tmp_mvar_value_50;
        Py_INCREF( tmp_tuple_element_51 );
        PyTuple_SET_ITEM( tmp_dict_value_47, 1, tmp_tuple_element_51 );
        tmp_tuple_element_51 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_51 );
        PyTuple_SET_ITEM( tmp_dict_value_47, 2, tmp_tuple_element_51 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_47, tmp_dict_value_47 );
        Py_DECREF( tmp_dict_value_47 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_48 = const_int_pos_323;
        tmp_tuple_element_52 = const_str_plain_TileLength;
        tmp_dict_value_48 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_52 );
        PyTuple_SET_ITEM( tmp_dict_value_48, 0, tmp_tuple_element_52 );
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_51 );
        tmp_tuple_element_52 = tmp_mvar_value_51;
        Py_INCREF( tmp_tuple_element_52 );
        PyTuple_SET_ITEM( tmp_dict_value_48, 1, tmp_tuple_element_52 );
        tmp_tuple_element_52 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_52 );
        PyTuple_SET_ITEM( tmp_dict_value_48, 2, tmp_tuple_element_52 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_48, tmp_dict_value_48 );
        Py_DECREF( tmp_dict_value_48 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_49 = const_int_pos_324;
        tmp_tuple_element_53 = const_str_plain_TileOffsets;
        tmp_dict_value_49 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_53 );
        PyTuple_SET_ITEM( tmp_dict_value_49, 0, tmp_tuple_element_53 );
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_52 );
        tmp_tuple_element_53 = tmp_mvar_value_52;
        Py_INCREF( tmp_tuple_element_53 );
        PyTuple_SET_ITEM( tmp_dict_value_49, 1, tmp_tuple_element_53 );
        tmp_tuple_element_53 = const_int_0;
        Py_INCREF( tmp_tuple_element_53 );
        PyTuple_SET_ITEM( tmp_dict_value_49, 2, tmp_tuple_element_53 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_49, tmp_dict_value_49 );
        Py_DECREF( tmp_dict_value_49 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_50 = const_int_pos_325;
        tmp_tuple_element_54 = const_str_plain_TileByteCounts;
        tmp_dict_value_50 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_54 );
        PyTuple_SET_ITEM( tmp_dict_value_50, 0, tmp_tuple_element_54 );
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_53 );
        tmp_tuple_element_54 = tmp_mvar_value_53;
        Py_INCREF( tmp_tuple_element_54 );
        PyTuple_SET_ITEM( tmp_dict_value_50, 1, tmp_tuple_element_54 );
        tmp_tuple_element_54 = const_int_0;
        Py_INCREF( tmp_tuple_element_54 );
        PyTuple_SET_ITEM( tmp_dict_value_50, 2, tmp_tuple_element_54 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_50, tmp_dict_value_50 );
        Py_DECREF( tmp_dict_value_50 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_51 = const_int_pos_332;
        tmp_tuple_element_55 = const_str_plain_InkSet;
        tmp_dict_value_51 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_55 );
        PyTuple_SET_ITEM( tmp_dict_value_51, 0, tmp_tuple_element_55 );
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_54 );
        tmp_tuple_element_55 = tmp_mvar_value_54;
        Py_INCREF( tmp_tuple_element_55 );
        PyTuple_SET_ITEM( tmp_dict_value_51, 1, tmp_tuple_element_55 );
        tmp_tuple_element_55 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_55 );
        PyTuple_SET_ITEM( tmp_dict_value_51, 2, tmp_tuple_element_55 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_51, tmp_dict_value_51 );
        Py_DECREF( tmp_dict_value_51 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_52 = const_int_pos_333;
        tmp_tuple_element_56 = const_str_plain_InkNames;
        tmp_dict_value_52 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_56 );
        PyTuple_SET_ITEM( tmp_dict_value_52, 0, tmp_tuple_element_56 );
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_55 );
        tmp_tuple_element_56 = tmp_mvar_value_55;
        Py_INCREF( tmp_tuple_element_56 );
        PyTuple_SET_ITEM( tmp_dict_value_52, 1, tmp_tuple_element_56 );
        tmp_tuple_element_56 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_56 );
        PyTuple_SET_ITEM( tmp_dict_value_52, 2, tmp_tuple_element_56 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_52, tmp_dict_value_52 );
        Py_DECREF( tmp_dict_value_52 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_53 = const_int_pos_334;
        tmp_tuple_element_57 = const_str_plain_NumberOfInks;
        tmp_dict_value_53 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_57 );
        PyTuple_SET_ITEM( tmp_dict_value_53, 0, tmp_tuple_element_57 );
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_56 );
        tmp_tuple_element_57 = tmp_mvar_value_56;
        Py_INCREF( tmp_tuple_element_57 );
        PyTuple_SET_ITEM( tmp_dict_value_53, 1, tmp_tuple_element_57 );
        tmp_tuple_element_57 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_57 );
        PyTuple_SET_ITEM( tmp_dict_value_53, 2, tmp_tuple_element_57 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_53, tmp_dict_value_53 );
        Py_DECREF( tmp_dict_value_53 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_54 = const_int_pos_336;
        tmp_tuple_element_58 = const_str_plain_DotRange;
        tmp_dict_value_54 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_58 );
        PyTuple_SET_ITEM( tmp_dict_value_54, 0, tmp_tuple_element_58 );
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_57 );
        tmp_tuple_element_58 = tmp_mvar_value_57;
        Py_INCREF( tmp_tuple_element_58 );
        PyTuple_SET_ITEM( tmp_dict_value_54, 1, tmp_tuple_element_58 );
        tmp_tuple_element_58 = const_int_0;
        Py_INCREF( tmp_tuple_element_58 );
        PyTuple_SET_ITEM( tmp_dict_value_54, 2, tmp_tuple_element_58 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_54, tmp_dict_value_54 );
        Py_DECREF( tmp_dict_value_54 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_55 = const_int_pos_337;
        tmp_tuple_element_59 = const_str_plain_TargetPrinter;
        tmp_dict_value_55 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_59 );
        PyTuple_SET_ITEM( tmp_dict_value_55, 0, tmp_tuple_element_59 );
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_58 );
        tmp_tuple_element_59 = tmp_mvar_value_58;
        Py_INCREF( tmp_tuple_element_59 );
        PyTuple_SET_ITEM( tmp_dict_value_55, 1, tmp_tuple_element_59 );
        tmp_tuple_element_59 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_59 );
        PyTuple_SET_ITEM( tmp_dict_value_55, 2, tmp_tuple_element_59 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_55, tmp_dict_value_55 );
        Py_DECREF( tmp_dict_value_55 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_56 = const_int_pos_338;
        tmp_tuple_element_60 = const_str_plain_ExtraSamples;
        tmp_dict_value_56 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_60 );
        PyTuple_SET_ITEM( tmp_dict_value_56, 0, tmp_tuple_element_60 );
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_59 );
        tmp_tuple_element_60 = tmp_mvar_value_59;
        Py_INCREF( tmp_tuple_element_60 );
        PyTuple_SET_ITEM( tmp_dict_value_56, 1, tmp_tuple_element_60 );
        tmp_tuple_element_60 = const_int_0;
        Py_INCREF( tmp_tuple_element_60 );
        PyTuple_SET_ITEM( tmp_dict_value_56, 2, tmp_tuple_element_60 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_56, tmp_dict_value_56 );
        Py_DECREF( tmp_dict_value_56 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_57 = const_int_pos_339;
        tmp_tuple_element_61 = const_str_plain_SampleFormat;
        tmp_dict_value_57 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_61 );
        PyTuple_SET_ITEM( tmp_dict_value_57, 0, tmp_tuple_element_61 );
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_60 );
        tmp_tuple_element_61 = tmp_mvar_value_60;
        Py_INCREF( tmp_tuple_element_61 );
        PyTuple_SET_ITEM( tmp_dict_value_57, 1, tmp_tuple_element_61 );
        tmp_tuple_element_61 = const_int_0;
        Py_INCREF( tmp_tuple_element_61 );
        PyTuple_SET_ITEM( tmp_dict_value_57, 2, tmp_tuple_element_61 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_57, tmp_dict_value_57 );
        Py_DECREF( tmp_dict_value_57 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_58 = const_int_pos_340;
        tmp_tuple_element_62 = const_str_plain_SMinSampleValue;
        tmp_dict_value_58 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_62 );
        PyTuple_SET_ITEM( tmp_dict_value_58, 0, tmp_tuple_element_62 );
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_DOUBLE );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOUBLE );
        }

        CHECK_OBJECT( tmp_mvar_value_61 );
        tmp_tuple_element_62 = tmp_mvar_value_61;
        Py_INCREF( tmp_tuple_element_62 );
        PyTuple_SET_ITEM( tmp_dict_value_58, 1, tmp_tuple_element_62 );
        tmp_tuple_element_62 = const_int_0;
        Py_INCREF( tmp_tuple_element_62 );
        PyTuple_SET_ITEM( tmp_dict_value_58, 2, tmp_tuple_element_62 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_58, tmp_dict_value_58 );
        Py_DECREF( tmp_dict_value_58 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_59 = const_int_pos_341;
        tmp_tuple_element_63 = const_str_plain_SMaxSampleValue;
        tmp_dict_value_59 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_63 );
        PyTuple_SET_ITEM( tmp_dict_value_59, 0, tmp_tuple_element_63 );
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_DOUBLE );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DOUBLE );
        }

        CHECK_OBJECT( tmp_mvar_value_62 );
        tmp_tuple_element_63 = tmp_mvar_value_62;
        Py_INCREF( tmp_tuple_element_63 );
        PyTuple_SET_ITEM( tmp_dict_value_59, 1, tmp_tuple_element_63 );
        tmp_tuple_element_63 = const_int_0;
        Py_INCREF( tmp_tuple_element_63 );
        PyTuple_SET_ITEM( tmp_dict_value_59, 2, tmp_tuple_element_63 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_59, tmp_dict_value_59 );
        Py_DECREF( tmp_dict_value_59 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_60 = const_int_pos_342;
        tmp_tuple_element_64 = const_str_plain_TransferRange;
        tmp_dict_value_60 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_64 );
        PyTuple_SET_ITEM( tmp_dict_value_60, 0, tmp_tuple_element_64 );
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_63 );
        tmp_tuple_element_64 = tmp_mvar_value_63;
        Py_INCREF( tmp_tuple_element_64 );
        PyTuple_SET_ITEM( tmp_dict_value_60, 1, tmp_tuple_element_64 );
        tmp_tuple_element_64 = const_int_pos_6;
        Py_INCREF( tmp_tuple_element_64 );
        PyTuple_SET_ITEM( tmp_dict_value_60, 2, tmp_tuple_element_64 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_60, tmp_dict_value_60 );
        Py_DECREF( tmp_dict_value_60 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_61 = const_int_pos_347;
        tmp_tuple_element_65 = const_str_plain_JPEGTables;
        tmp_dict_value_61 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_65 );
        PyTuple_SET_ITEM( tmp_dict_value_61, 0, tmp_tuple_element_65 );
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNDEFINED );
        }

        CHECK_OBJECT( tmp_mvar_value_64 );
        tmp_tuple_element_65 = tmp_mvar_value_64;
        Py_INCREF( tmp_tuple_element_65 );
        PyTuple_SET_ITEM( tmp_dict_value_61, 1, tmp_tuple_element_65 );
        tmp_tuple_element_65 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_65 );
        PyTuple_SET_ITEM( tmp_dict_value_61, 2, tmp_tuple_element_65 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_61, tmp_dict_value_61 );
        Py_DECREF( tmp_dict_value_61 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_62 = const_int_pos_512;
        tmp_tuple_element_66 = const_str_plain_JPEGProc;
        tmp_dict_value_62 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_66 );
        PyTuple_SET_ITEM( tmp_dict_value_62, 0, tmp_tuple_element_66 );
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_65 );
        tmp_tuple_element_66 = tmp_mvar_value_65;
        Py_INCREF( tmp_tuple_element_66 );
        PyTuple_SET_ITEM( tmp_dict_value_62, 1, tmp_tuple_element_66 );
        tmp_tuple_element_66 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_66 );
        PyTuple_SET_ITEM( tmp_dict_value_62, 2, tmp_tuple_element_66 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_62, tmp_dict_value_62 );
        Py_DECREF( tmp_dict_value_62 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_63 = const_int_pos_513;
        tmp_tuple_element_67 = const_str_plain_JPEGInterchangeFormat;
        tmp_dict_value_63 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_67 );
        PyTuple_SET_ITEM( tmp_dict_value_63, 0, tmp_tuple_element_67 );
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_66 );
        tmp_tuple_element_67 = tmp_mvar_value_66;
        Py_INCREF( tmp_tuple_element_67 );
        PyTuple_SET_ITEM( tmp_dict_value_63, 1, tmp_tuple_element_67 );
        tmp_tuple_element_67 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_67 );
        PyTuple_SET_ITEM( tmp_dict_value_63, 2, tmp_tuple_element_67 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_63, tmp_dict_value_63 );
        Py_DECREF( tmp_dict_value_63 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_64 = const_int_pos_514;
        tmp_tuple_element_68 = const_str_plain_JPEGInterchangeFormatLength;
        tmp_dict_value_64 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_68 );
        PyTuple_SET_ITEM( tmp_dict_value_64, 0, tmp_tuple_element_68 );
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_67 );
        tmp_tuple_element_68 = tmp_mvar_value_67;
        Py_INCREF( tmp_tuple_element_68 );
        PyTuple_SET_ITEM( tmp_dict_value_64, 1, tmp_tuple_element_68 );
        tmp_tuple_element_68 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_68 );
        PyTuple_SET_ITEM( tmp_dict_value_64, 2, tmp_tuple_element_68 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_64, tmp_dict_value_64 );
        Py_DECREF( tmp_dict_value_64 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_65 = const_int_pos_515;
        tmp_tuple_element_69 = const_str_plain_JPEGRestartInterval;
        tmp_dict_value_65 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_69 );
        PyTuple_SET_ITEM( tmp_dict_value_65, 0, tmp_tuple_element_69 );
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_68 );
        tmp_tuple_element_69 = tmp_mvar_value_68;
        Py_INCREF( tmp_tuple_element_69 );
        PyTuple_SET_ITEM( tmp_dict_value_65, 1, tmp_tuple_element_69 );
        tmp_tuple_element_69 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_69 );
        PyTuple_SET_ITEM( tmp_dict_value_65, 2, tmp_tuple_element_69 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_65, tmp_dict_value_65 );
        Py_DECREF( tmp_dict_value_65 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_66 = const_int_pos_517;
        tmp_tuple_element_70 = const_str_plain_JPEGLosslessPredictors;
        tmp_dict_value_66 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_70 );
        PyTuple_SET_ITEM( tmp_dict_value_66, 0, tmp_tuple_element_70 );
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_69 );
        tmp_tuple_element_70 = tmp_mvar_value_69;
        Py_INCREF( tmp_tuple_element_70 );
        PyTuple_SET_ITEM( tmp_dict_value_66, 1, tmp_tuple_element_70 );
        tmp_tuple_element_70 = const_int_0;
        Py_INCREF( tmp_tuple_element_70 );
        PyTuple_SET_ITEM( tmp_dict_value_66, 2, tmp_tuple_element_70 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_66, tmp_dict_value_66 );
        Py_DECREF( tmp_dict_value_66 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_67 = const_int_pos_518;
        tmp_tuple_element_71 = const_str_plain_JPEGPointTransforms;
        tmp_dict_value_67 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_71 );
        PyTuple_SET_ITEM( tmp_dict_value_67, 0, tmp_tuple_element_71 );
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_70 );
        tmp_tuple_element_71 = tmp_mvar_value_70;
        Py_INCREF( tmp_tuple_element_71 );
        PyTuple_SET_ITEM( tmp_dict_value_67, 1, tmp_tuple_element_71 );
        tmp_tuple_element_71 = const_int_0;
        Py_INCREF( tmp_tuple_element_71 );
        PyTuple_SET_ITEM( tmp_dict_value_67, 2, tmp_tuple_element_71 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_67, tmp_dict_value_67 );
        Py_DECREF( tmp_dict_value_67 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_68 = const_int_pos_519;
        tmp_tuple_element_72 = const_str_plain_JPEGQTables;
        tmp_dict_value_68 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_72 );
        PyTuple_SET_ITEM( tmp_dict_value_68, 0, tmp_tuple_element_72 );
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_71 );
        tmp_tuple_element_72 = tmp_mvar_value_71;
        Py_INCREF( tmp_tuple_element_72 );
        PyTuple_SET_ITEM( tmp_dict_value_68, 1, tmp_tuple_element_72 );
        tmp_tuple_element_72 = const_int_0;
        Py_INCREF( tmp_tuple_element_72 );
        PyTuple_SET_ITEM( tmp_dict_value_68, 2, tmp_tuple_element_72 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_68, tmp_dict_value_68 );
        Py_DECREF( tmp_dict_value_68 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_69 = const_int_pos_520;
        tmp_tuple_element_73 = const_str_plain_JPEGDCTables;
        tmp_dict_value_69 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_73 );
        PyTuple_SET_ITEM( tmp_dict_value_69, 0, tmp_tuple_element_73 );
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_72 );
        tmp_tuple_element_73 = tmp_mvar_value_72;
        Py_INCREF( tmp_tuple_element_73 );
        PyTuple_SET_ITEM( tmp_dict_value_69, 1, tmp_tuple_element_73 );
        tmp_tuple_element_73 = const_int_0;
        Py_INCREF( tmp_tuple_element_73 );
        PyTuple_SET_ITEM( tmp_dict_value_69, 2, tmp_tuple_element_73 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_69, tmp_dict_value_69 );
        Py_DECREF( tmp_dict_value_69 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_70 = const_int_pos_521;
        tmp_tuple_element_74 = const_str_plain_JPEGACTables;
        tmp_dict_value_70 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_74 );
        PyTuple_SET_ITEM( tmp_dict_value_70, 0, tmp_tuple_element_74 );
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_73 );
        tmp_tuple_element_74 = tmp_mvar_value_73;
        Py_INCREF( tmp_tuple_element_74 );
        PyTuple_SET_ITEM( tmp_dict_value_70, 1, tmp_tuple_element_74 );
        tmp_tuple_element_74 = const_int_0;
        Py_INCREF( tmp_tuple_element_74 );
        PyTuple_SET_ITEM( tmp_dict_value_70, 2, tmp_tuple_element_74 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_70, tmp_dict_value_70 );
        Py_DECREF( tmp_dict_value_70 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_71 = const_int_pos_529;
        tmp_tuple_element_75 = const_str_plain_YCbCrCoefficients;
        tmp_dict_value_71 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_75 );
        PyTuple_SET_ITEM( tmp_dict_value_71, 0, tmp_tuple_element_75 );
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_74 );
        tmp_tuple_element_75 = tmp_mvar_value_74;
        Py_INCREF( tmp_tuple_element_75 );
        PyTuple_SET_ITEM( tmp_dict_value_71, 1, tmp_tuple_element_75 );
        tmp_tuple_element_75 = const_int_pos_3;
        Py_INCREF( tmp_tuple_element_75 );
        PyTuple_SET_ITEM( tmp_dict_value_71, 2, tmp_tuple_element_75 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_71, tmp_dict_value_71 );
        Py_DECREF( tmp_dict_value_71 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_72 = const_int_pos_530;
        tmp_tuple_element_76 = const_str_plain_YCbCrSubSampling;
        tmp_dict_value_72 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_76 );
        PyTuple_SET_ITEM( tmp_dict_value_72, 0, tmp_tuple_element_76 );
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_75 );
        tmp_tuple_element_76 = tmp_mvar_value_75;
        Py_INCREF( tmp_tuple_element_76 );
        PyTuple_SET_ITEM( tmp_dict_value_72, 1, tmp_tuple_element_76 );
        tmp_tuple_element_76 = const_int_pos_2;
        Py_INCREF( tmp_tuple_element_76 );
        PyTuple_SET_ITEM( tmp_dict_value_72, 2, tmp_tuple_element_76 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_72, tmp_dict_value_72 );
        Py_DECREF( tmp_dict_value_72 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_73 = const_int_pos_531;
        tmp_tuple_element_77 = const_str_plain_YCbCrPositioning;
        tmp_dict_value_73 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_77 );
        PyTuple_SET_ITEM( tmp_dict_value_73, 0, tmp_tuple_element_77 );
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_76 );
        tmp_tuple_element_77 = tmp_mvar_value_76;
        Py_INCREF( tmp_tuple_element_77 );
        PyTuple_SET_ITEM( tmp_dict_value_73, 1, tmp_tuple_element_77 );
        tmp_tuple_element_77 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_77 );
        PyTuple_SET_ITEM( tmp_dict_value_73, 2, tmp_tuple_element_77 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_73, tmp_dict_value_73 );
        Py_DECREF( tmp_dict_value_73 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_74 = const_int_pos_532;
        tmp_tuple_element_78 = const_str_plain_ReferenceBlackWhite;
        tmp_dict_value_74 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_78 );
        PyTuple_SET_ITEM( tmp_dict_value_74, 0, tmp_tuple_element_78 );
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_77 );
        tmp_tuple_element_78 = tmp_mvar_value_77;
        Py_INCREF( tmp_tuple_element_78 );
        PyTuple_SET_ITEM( tmp_dict_value_74, 1, tmp_tuple_element_78 );
        tmp_tuple_element_78 = const_int_pos_6;
        Py_INCREF( tmp_tuple_element_78 );
        PyTuple_SET_ITEM( tmp_dict_value_74, 2, tmp_tuple_element_78 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_74, tmp_dict_value_74 );
        Py_DECREF( tmp_dict_value_74 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_75 = const_int_pos_700;
        tmp_tuple_element_79 = const_str_plain_XMP;
        tmp_dict_value_75 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_79 );
        PyTuple_SET_ITEM( tmp_dict_value_75, 0, tmp_tuple_element_79 );
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_BYTE );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTE );
        }

        CHECK_OBJECT( tmp_mvar_value_78 );
        tmp_tuple_element_79 = tmp_mvar_value_78;
        Py_INCREF( tmp_tuple_element_79 );
        PyTuple_SET_ITEM( tmp_dict_value_75, 1, tmp_tuple_element_79 );
        tmp_tuple_element_79 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_79 );
        PyTuple_SET_ITEM( tmp_dict_value_75, 2, tmp_tuple_element_79 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_75, tmp_dict_value_75 );
        Py_DECREF( tmp_dict_value_75 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_76 = const_int_pos_33432;
        tmp_tuple_element_80 = const_str_plain_Copyright;
        tmp_dict_value_76 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_80 );
        PyTuple_SET_ITEM( tmp_dict_value_76, 0, tmp_tuple_element_80 );
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_ASCII );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ASCII );
        }

        CHECK_OBJECT( tmp_mvar_value_79 );
        tmp_tuple_element_80 = tmp_mvar_value_79;
        Py_INCREF( tmp_tuple_element_80 );
        PyTuple_SET_ITEM( tmp_dict_value_76, 1, tmp_tuple_element_80 );
        tmp_tuple_element_80 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_80 );
        PyTuple_SET_ITEM( tmp_dict_value_76, 2, tmp_tuple_element_80 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_76, tmp_dict_value_76 );
        Py_DECREF( tmp_dict_value_76 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_77 = const_int_pos_34377;
        tmp_tuple_element_81 = const_str_plain_PhotoshopInfo;
        tmp_dict_value_77 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_81 );
        PyTuple_SET_ITEM( tmp_dict_value_77, 0, tmp_tuple_element_81 );
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_BYTE );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTE );
        }

        CHECK_OBJECT( tmp_mvar_value_80 );
        tmp_tuple_element_81 = tmp_mvar_value_80;
        Py_INCREF( tmp_tuple_element_81 );
        PyTuple_SET_ITEM( tmp_dict_value_77, 1, tmp_tuple_element_81 );
        tmp_tuple_element_81 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_81 );
        PyTuple_SET_ITEM( tmp_dict_value_77, 2, tmp_tuple_element_81 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_77, tmp_dict_value_77 );
        Py_DECREF( tmp_dict_value_77 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_78 = const_int_pos_34665;
        tmp_tuple_element_82 = const_str_plain_ExifIFD;
        tmp_dict_value_78 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_82 );
        PyTuple_SET_ITEM( tmp_dict_value_78, 0, tmp_tuple_element_82 );
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_81 );
        tmp_tuple_element_82 = tmp_mvar_value_81;
        Py_INCREF( tmp_tuple_element_82 );
        PyTuple_SET_ITEM( tmp_dict_value_78, 1, tmp_tuple_element_82 );
        tmp_tuple_element_82 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_82 );
        PyTuple_SET_ITEM( tmp_dict_value_78, 2, tmp_tuple_element_82 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_78, tmp_dict_value_78 );
        Py_DECREF( tmp_dict_value_78 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_79 = const_int_pos_34675;
        tmp_tuple_element_83 = const_str_plain_ICCProfile;
        tmp_dict_value_79 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_83 );
        PyTuple_SET_ITEM( tmp_dict_value_79, 0, tmp_tuple_element_83 );
        tmp_mvar_value_82 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED );

        if (unlikely( tmp_mvar_value_82 == NULL ))
        {
            tmp_mvar_value_82 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNDEFINED );
        }

        CHECK_OBJECT( tmp_mvar_value_82 );
        tmp_tuple_element_83 = tmp_mvar_value_82;
        Py_INCREF( tmp_tuple_element_83 );
        PyTuple_SET_ITEM( tmp_dict_value_79, 1, tmp_tuple_element_83 );
        tmp_tuple_element_83 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_83 );
        PyTuple_SET_ITEM( tmp_dict_value_79, 2, tmp_tuple_element_83 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_79, tmp_dict_value_79 );
        Py_DECREF( tmp_dict_value_79 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_80 = const_int_pos_34853;
        tmp_tuple_element_84 = const_str_plain_GPSInfoIFD;
        tmp_dict_value_80 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_84 );
        PyTuple_SET_ITEM( tmp_dict_value_80, 0, tmp_tuple_element_84 );
        tmp_mvar_value_83 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_BYTE );

        if (unlikely( tmp_mvar_value_83 == NULL ))
        {
            tmp_mvar_value_83 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTE );
        }

        CHECK_OBJECT( tmp_mvar_value_83 );
        tmp_tuple_element_84 = tmp_mvar_value_83;
        Py_INCREF( tmp_tuple_element_84 );
        PyTuple_SET_ITEM( tmp_dict_value_80, 1, tmp_tuple_element_84 );
        tmp_tuple_element_84 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_84 );
        PyTuple_SET_ITEM( tmp_dict_value_80, 2, tmp_tuple_element_84 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_80, tmp_dict_value_80 );
        Py_DECREF( tmp_dict_value_80 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_81 = const_int_pos_45056;
        tmp_tuple_element_85 = const_str_plain_MPFVersion;
        tmp_dict_value_81 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_85 );
        PyTuple_SET_ITEM( tmp_dict_value_81, 0, tmp_tuple_element_85 );
        tmp_mvar_value_84 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED );

        if (unlikely( tmp_mvar_value_84 == NULL ))
        {
            tmp_mvar_value_84 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNDEFINED );
        }

        CHECK_OBJECT( tmp_mvar_value_84 );
        tmp_tuple_element_85 = tmp_mvar_value_84;
        Py_INCREF( tmp_tuple_element_85 );
        PyTuple_SET_ITEM( tmp_dict_value_81, 1, tmp_tuple_element_85 );
        tmp_tuple_element_85 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_85 );
        PyTuple_SET_ITEM( tmp_dict_value_81, 2, tmp_tuple_element_85 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_81, tmp_dict_value_81 );
        Py_DECREF( tmp_dict_value_81 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_82 = const_int_pos_45057;
        tmp_tuple_element_86 = const_str_plain_NumberOfImages;
        tmp_dict_value_82 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_86 );
        PyTuple_SET_ITEM( tmp_dict_value_82, 0, tmp_tuple_element_86 );
        tmp_mvar_value_85 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_85 == NULL ))
        {
            tmp_mvar_value_85 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_85 );
        tmp_tuple_element_86 = tmp_mvar_value_85;
        Py_INCREF( tmp_tuple_element_86 );
        PyTuple_SET_ITEM( tmp_dict_value_82, 1, tmp_tuple_element_86 );
        tmp_tuple_element_86 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_86 );
        PyTuple_SET_ITEM( tmp_dict_value_82, 2, tmp_tuple_element_86 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_82, tmp_dict_value_82 );
        Py_DECREF( tmp_dict_value_82 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_83 = const_int_pos_45058;
        tmp_tuple_element_87 = const_str_plain_MPEntry;
        tmp_dict_value_83 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_87 );
        PyTuple_SET_ITEM( tmp_dict_value_83, 0, tmp_tuple_element_87 );
        tmp_mvar_value_86 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED );

        if (unlikely( tmp_mvar_value_86 == NULL ))
        {
            tmp_mvar_value_86 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNDEFINED );
        }

        CHECK_OBJECT( tmp_mvar_value_86 );
        tmp_tuple_element_87 = tmp_mvar_value_86;
        Py_INCREF( tmp_tuple_element_87 );
        PyTuple_SET_ITEM( tmp_dict_value_83, 1, tmp_tuple_element_87 );
        tmp_tuple_element_87 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_87 );
        PyTuple_SET_ITEM( tmp_dict_value_83, 2, tmp_tuple_element_87 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_83, tmp_dict_value_83 );
        Py_DECREF( tmp_dict_value_83 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_84 = const_int_pos_45059;
        tmp_tuple_element_88 = const_str_plain_ImageUIDList;
        tmp_dict_value_84 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_88 );
        PyTuple_SET_ITEM( tmp_dict_value_84, 0, tmp_tuple_element_88 );
        tmp_mvar_value_87 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED );

        if (unlikely( tmp_mvar_value_87 == NULL ))
        {
            tmp_mvar_value_87 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNDEFINED );
        }

        CHECK_OBJECT( tmp_mvar_value_87 );
        tmp_tuple_element_88 = tmp_mvar_value_87;
        Py_INCREF( tmp_tuple_element_88 );
        PyTuple_SET_ITEM( tmp_dict_value_84, 1, tmp_tuple_element_88 );
        tmp_tuple_element_88 = const_int_0;
        Py_INCREF( tmp_tuple_element_88 );
        PyTuple_SET_ITEM( tmp_dict_value_84, 2, tmp_tuple_element_88 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_84, tmp_dict_value_84 );
        Py_DECREF( tmp_dict_value_84 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_85 = const_int_pos_45060;
        tmp_tuple_element_89 = const_str_plain_TotalFrames;
        tmp_dict_value_85 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_89 );
        PyTuple_SET_ITEM( tmp_dict_value_85, 0, tmp_tuple_element_89 );
        tmp_mvar_value_88 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_88 == NULL ))
        {
            tmp_mvar_value_88 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_88 );
        tmp_tuple_element_89 = tmp_mvar_value_88;
        Py_INCREF( tmp_tuple_element_89 );
        PyTuple_SET_ITEM( tmp_dict_value_85, 1, tmp_tuple_element_89 );
        tmp_tuple_element_89 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_89 );
        PyTuple_SET_ITEM( tmp_dict_value_85, 2, tmp_tuple_element_89 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_85, tmp_dict_value_85 );
        Py_DECREF( tmp_dict_value_85 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_86 = const_int_pos_45313;
        tmp_tuple_element_90 = const_str_plain_MPIndividualNum;
        tmp_dict_value_86 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_90 );
        PyTuple_SET_ITEM( tmp_dict_value_86, 0, tmp_tuple_element_90 );
        tmp_mvar_value_89 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_89 == NULL ))
        {
            tmp_mvar_value_89 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_89 );
        tmp_tuple_element_90 = tmp_mvar_value_89;
        Py_INCREF( tmp_tuple_element_90 );
        PyTuple_SET_ITEM( tmp_dict_value_86, 1, tmp_tuple_element_90 );
        tmp_tuple_element_90 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_90 );
        PyTuple_SET_ITEM( tmp_dict_value_86, 2, tmp_tuple_element_90 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_86, tmp_dict_value_86 );
        Py_DECREF( tmp_dict_value_86 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_87 = const_int_pos_45569;
        tmp_tuple_element_91 = const_str_plain_PanOrientation;
        tmp_dict_value_87 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_91 );
        PyTuple_SET_ITEM( tmp_dict_value_87, 0, tmp_tuple_element_91 );
        tmp_mvar_value_90 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_90 == NULL ))
        {
            tmp_mvar_value_90 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_90 );
        tmp_tuple_element_91 = tmp_mvar_value_90;
        Py_INCREF( tmp_tuple_element_91 );
        PyTuple_SET_ITEM( tmp_dict_value_87, 1, tmp_tuple_element_91 );
        tmp_tuple_element_91 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_91 );
        PyTuple_SET_ITEM( tmp_dict_value_87, 2, tmp_tuple_element_91 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_87, tmp_dict_value_87 );
        Py_DECREF( tmp_dict_value_87 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_88 = const_int_pos_45570;
        tmp_tuple_element_92 = const_str_plain_PanOverlap_H;
        tmp_dict_value_88 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_92 );
        PyTuple_SET_ITEM( tmp_dict_value_88, 0, tmp_tuple_element_92 );
        tmp_mvar_value_91 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_91 == NULL ))
        {
            tmp_mvar_value_91 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_91 );
        tmp_tuple_element_92 = tmp_mvar_value_91;
        Py_INCREF( tmp_tuple_element_92 );
        PyTuple_SET_ITEM( tmp_dict_value_88, 1, tmp_tuple_element_92 );
        tmp_tuple_element_92 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_92 );
        PyTuple_SET_ITEM( tmp_dict_value_88, 2, tmp_tuple_element_92 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_88, tmp_dict_value_88 );
        Py_DECREF( tmp_dict_value_88 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_89 = const_int_pos_45571;
        tmp_tuple_element_93 = const_str_plain_PanOverlap_V;
        tmp_dict_value_89 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_93 );
        PyTuple_SET_ITEM( tmp_dict_value_89, 0, tmp_tuple_element_93 );
        tmp_mvar_value_92 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_92 == NULL ))
        {
            tmp_mvar_value_92 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_92 );
        tmp_tuple_element_93 = tmp_mvar_value_92;
        Py_INCREF( tmp_tuple_element_93 );
        PyTuple_SET_ITEM( tmp_dict_value_89, 1, tmp_tuple_element_93 );
        tmp_tuple_element_93 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_93 );
        PyTuple_SET_ITEM( tmp_dict_value_89, 2, tmp_tuple_element_93 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_89, tmp_dict_value_89 );
        Py_DECREF( tmp_dict_value_89 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_90 = const_int_pos_45572;
        tmp_tuple_element_94 = const_str_plain_BaseViewpointNum;
        tmp_dict_value_90 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_94 );
        PyTuple_SET_ITEM( tmp_dict_value_90, 0, tmp_tuple_element_94 );
        tmp_mvar_value_93 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_93 == NULL ))
        {
            tmp_mvar_value_93 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_93 );
        tmp_tuple_element_94 = tmp_mvar_value_93;
        Py_INCREF( tmp_tuple_element_94 );
        PyTuple_SET_ITEM( tmp_dict_value_90, 1, tmp_tuple_element_94 );
        tmp_tuple_element_94 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_94 );
        PyTuple_SET_ITEM( tmp_dict_value_90, 2, tmp_tuple_element_94 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_90, tmp_dict_value_90 );
        Py_DECREF( tmp_dict_value_90 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_91 = const_int_pos_45573;
        tmp_tuple_element_95 = const_str_plain_ConvergenceAngle;
        tmp_dict_value_91 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_95 );
        PyTuple_SET_ITEM( tmp_dict_value_91, 0, tmp_tuple_element_95 );
        tmp_mvar_value_94 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_94 == NULL ))
        {
            tmp_mvar_value_94 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_94 );
        tmp_tuple_element_95 = tmp_mvar_value_94;
        Py_INCREF( tmp_tuple_element_95 );
        PyTuple_SET_ITEM( tmp_dict_value_91, 1, tmp_tuple_element_95 );
        tmp_tuple_element_95 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_95 );
        PyTuple_SET_ITEM( tmp_dict_value_91, 2, tmp_tuple_element_95 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_91, tmp_dict_value_91 );
        Py_DECREF( tmp_dict_value_91 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_92 = const_int_pos_45574;
        tmp_tuple_element_96 = const_str_plain_BaselineLength;
        tmp_dict_value_92 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_96 );
        PyTuple_SET_ITEM( tmp_dict_value_92, 0, tmp_tuple_element_96 );
        tmp_mvar_value_95 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_95 == NULL ))
        {
            tmp_mvar_value_95 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_95 );
        tmp_tuple_element_96 = tmp_mvar_value_95;
        Py_INCREF( tmp_tuple_element_96 );
        PyTuple_SET_ITEM( tmp_dict_value_92, 1, tmp_tuple_element_96 );
        tmp_tuple_element_96 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_96 );
        PyTuple_SET_ITEM( tmp_dict_value_92, 2, tmp_tuple_element_96 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_92, tmp_dict_value_92 );
        Py_DECREF( tmp_dict_value_92 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_93 = const_int_pos_45575;
        tmp_tuple_element_97 = const_str_plain_VerticalDivergence;
        tmp_dict_value_93 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_97 );
        PyTuple_SET_ITEM( tmp_dict_value_93, 0, tmp_tuple_element_97 );
        tmp_mvar_value_96 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_96 == NULL ))
        {
            tmp_mvar_value_96 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_96 );
        tmp_tuple_element_97 = tmp_mvar_value_96;
        Py_INCREF( tmp_tuple_element_97 );
        PyTuple_SET_ITEM( tmp_dict_value_93, 1, tmp_tuple_element_97 );
        tmp_tuple_element_97 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_97 );
        PyTuple_SET_ITEM( tmp_dict_value_93, 2, tmp_tuple_element_97 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_93, tmp_dict_value_93 );
        Py_DECREF( tmp_dict_value_93 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_94 = const_int_pos_45576;
        tmp_tuple_element_98 = const_str_plain_AxisDistance_X;
        tmp_dict_value_94 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_98 );
        PyTuple_SET_ITEM( tmp_dict_value_94, 0, tmp_tuple_element_98 );
        tmp_mvar_value_97 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_97 == NULL ))
        {
            tmp_mvar_value_97 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_97 );
        tmp_tuple_element_98 = tmp_mvar_value_97;
        Py_INCREF( tmp_tuple_element_98 );
        PyTuple_SET_ITEM( tmp_dict_value_94, 1, tmp_tuple_element_98 );
        tmp_tuple_element_98 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_98 );
        PyTuple_SET_ITEM( tmp_dict_value_94, 2, tmp_tuple_element_98 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_94, tmp_dict_value_94 );
        Py_DECREF( tmp_dict_value_94 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_95 = const_int_pos_45577;
        tmp_tuple_element_99 = const_str_plain_AxisDistance_Y;
        tmp_dict_value_95 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_99 );
        PyTuple_SET_ITEM( tmp_dict_value_95, 0, tmp_tuple_element_99 );
        tmp_mvar_value_98 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_98 == NULL ))
        {
            tmp_mvar_value_98 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_98 );
        tmp_tuple_element_99 = tmp_mvar_value_98;
        Py_INCREF( tmp_tuple_element_99 );
        PyTuple_SET_ITEM( tmp_dict_value_95, 1, tmp_tuple_element_99 );
        tmp_tuple_element_99 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_99 );
        PyTuple_SET_ITEM( tmp_dict_value_95, 2, tmp_tuple_element_99 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_95, tmp_dict_value_95 );
        Py_DECREF( tmp_dict_value_95 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_96 = const_int_pos_45578;
        tmp_tuple_element_100 = const_str_plain_AxisDistance_Z;
        tmp_dict_value_96 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_100 );
        PyTuple_SET_ITEM( tmp_dict_value_96, 0, tmp_tuple_element_100 );
        tmp_mvar_value_99 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_99 == NULL ))
        {
            tmp_mvar_value_99 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_99 );
        tmp_tuple_element_100 = tmp_mvar_value_99;
        Py_INCREF( tmp_tuple_element_100 );
        PyTuple_SET_ITEM( tmp_dict_value_96, 1, tmp_tuple_element_100 );
        tmp_tuple_element_100 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_100 );
        PyTuple_SET_ITEM( tmp_dict_value_96, 2, tmp_tuple_element_100 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_96, tmp_dict_value_96 );
        Py_DECREF( tmp_dict_value_96 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_97 = const_int_pos_45579;
        tmp_tuple_element_101 = const_str_plain_YawAngle;
        tmp_dict_value_97 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_101 );
        PyTuple_SET_ITEM( tmp_dict_value_97, 0, tmp_tuple_element_101 );
        tmp_mvar_value_100 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_100 == NULL ))
        {
            tmp_mvar_value_100 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_100 );
        tmp_tuple_element_101 = tmp_mvar_value_100;
        Py_INCREF( tmp_tuple_element_101 );
        PyTuple_SET_ITEM( tmp_dict_value_97, 1, tmp_tuple_element_101 );
        tmp_tuple_element_101 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_101 );
        PyTuple_SET_ITEM( tmp_dict_value_97, 2, tmp_tuple_element_101 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_97, tmp_dict_value_97 );
        Py_DECREF( tmp_dict_value_97 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_98 = const_int_pos_45580;
        tmp_tuple_element_102 = const_str_plain_PitchAngle;
        tmp_dict_value_98 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_102 );
        PyTuple_SET_ITEM( tmp_dict_value_98, 0, tmp_tuple_element_102 );
        tmp_mvar_value_101 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_101 == NULL ))
        {
            tmp_mvar_value_101 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_101 );
        tmp_tuple_element_102 = tmp_mvar_value_101;
        Py_INCREF( tmp_tuple_element_102 );
        PyTuple_SET_ITEM( tmp_dict_value_98, 1, tmp_tuple_element_102 );
        tmp_tuple_element_102 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_102 );
        PyTuple_SET_ITEM( tmp_dict_value_98, 2, tmp_tuple_element_102 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_98, tmp_dict_value_98 );
        Py_DECREF( tmp_dict_value_98 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_99 = const_int_pos_45581;
        tmp_tuple_element_103 = const_str_plain_RollAngle;
        tmp_dict_value_99 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_103 );
        PyTuple_SET_ITEM( tmp_dict_value_99, 0, tmp_tuple_element_103 );
        tmp_mvar_value_102 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );

        if (unlikely( tmp_mvar_value_102 == NULL ))
        {
            tmp_mvar_value_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SIGNED_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_102 );
        tmp_tuple_element_103 = tmp_mvar_value_102;
        Py_INCREF( tmp_tuple_element_103 );
        PyTuple_SET_ITEM( tmp_dict_value_99, 1, tmp_tuple_element_103 );
        tmp_tuple_element_103 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_103 );
        PyTuple_SET_ITEM( tmp_dict_value_99, 2, tmp_tuple_element_103 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_99, tmp_dict_value_99 );
        Py_DECREF( tmp_dict_value_99 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_100 = const_int_pos_50741;
        tmp_tuple_element_104 = const_str_plain_MakerNoteSafety;
        tmp_dict_value_100 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_104 );
        PyTuple_SET_ITEM( tmp_dict_value_100, 0, tmp_tuple_element_104 );
        tmp_mvar_value_103 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_SHORT );

        if (unlikely( tmp_mvar_value_103 == NULL ))
        {
            tmp_mvar_value_103 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SHORT );
        }

        CHECK_OBJECT( tmp_mvar_value_103 );
        tmp_tuple_element_104 = tmp_mvar_value_103;
        Py_INCREF( tmp_tuple_element_104 );
        PyTuple_SET_ITEM( tmp_dict_value_100, 1, tmp_tuple_element_104 );
        tmp_tuple_element_104 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_104 );
        PyTuple_SET_ITEM( tmp_dict_value_100, 2, tmp_tuple_element_104 );
        tmp_tuple_element_104 = PyDict_Copy( const_dict_d5fe7a7e78b4671757baf25612bf91d9 );
        PyTuple_SET_ITEM( tmp_dict_value_100, 3, tmp_tuple_element_104 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_100, tmp_dict_value_100 );
        Py_DECREF( tmp_dict_value_100 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_101 = const_int_pos_50780;
        tmp_tuple_element_105 = const_str_plain_BestQualityScale;
        tmp_dict_value_101 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_105 );
        PyTuple_SET_ITEM( tmp_dict_value_101, 0, tmp_tuple_element_105 );
        tmp_mvar_value_104 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_RATIONAL );

        if (unlikely( tmp_mvar_value_104 == NULL ))
        {
            tmp_mvar_value_104 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RATIONAL );
        }

        CHECK_OBJECT( tmp_mvar_value_104 );
        tmp_tuple_element_105 = tmp_mvar_value_104;
        Py_INCREF( tmp_tuple_element_105 );
        PyTuple_SET_ITEM( tmp_dict_value_101, 1, tmp_tuple_element_105 );
        tmp_tuple_element_105 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_105 );
        PyTuple_SET_ITEM( tmp_dict_value_101, 2, tmp_tuple_element_105 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_101, tmp_dict_value_101 );
        Py_DECREF( tmp_dict_value_101 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_102 = const_int_pos_50838;
        tmp_tuple_element_106 = const_str_plain_ImageJMetaDataByteCounts;
        tmp_dict_value_102 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_106 );
        PyTuple_SET_ITEM( tmp_dict_value_102, 0, tmp_tuple_element_106 );
        tmp_mvar_value_105 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LONG );

        if (unlikely( tmp_mvar_value_105 == NULL ))
        {
            tmp_mvar_value_105 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LONG );
        }

        CHECK_OBJECT( tmp_mvar_value_105 );
        tmp_tuple_element_106 = tmp_mvar_value_105;
        Py_INCREF( tmp_tuple_element_106 );
        PyTuple_SET_ITEM( tmp_dict_value_102, 1, tmp_tuple_element_106 );
        tmp_tuple_element_106 = const_int_0;
        Py_INCREF( tmp_tuple_element_106 );
        PyTuple_SET_ITEM( tmp_dict_value_102, 2, tmp_tuple_element_106 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_102, tmp_dict_value_102 );
        Py_DECREF( tmp_dict_value_102 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_103 = const_int_pos_50839;
        tmp_tuple_element_107 = const_str_plain_ImageJMetaData;
        tmp_dict_value_103 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_107 );
        PyTuple_SET_ITEM( tmp_dict_value_103, 0, tmp_tuple_element_107 );
        tmp_mvar_value_106 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_UNDEFINED );

        if (unlikely( tmp_mvar_value_106 == NULL ))
        {
            tmp_mvar_value_106 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNDEFINED );
        }

        CHECK_OBJECT( tmp_mvar_value_106 );
        tmp_tuple_element_107 = tmp_mvar_value_106;
        Py_INCREF( tmp_tuple_element_107 );
        PyTuple_SET_ITEM( tmp_dict_value_103, 1, tmp_tuple_element_107 );
        tmp_tuple_element_107 = const_int_pos_1;
        Py_INCREF( tmp_tuple_element_107 );
        PyTuple_SET_ITEM( tmp_dict_value_103, 2, tmp_tuple_element_107 );
        tmp_res = PyDict_SetItem( tmp_assign_source_26, tmp_dict_key_103, tmp_dict_value_103 );
        Py_DECREF( tmp_dict_value_103 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS_V2, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = PyDict_Copy( const_dict_4b92f8f39dec2a9ad82c82df72668389 );
        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TAGS, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_PIL$TiffTags$$$function_4__populate(  );



        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain__populate, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_107;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_107 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain__populate );

        if (unlikely( tmp_mvar_value_107 == NULL ))
        {
            tmp_mvar_value_107 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__populate );
        }

        CHECK_OBJECT( tmp_mvar_value_107 );
        tmp_called_name_4 = tmp_mvar_value_107;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 367;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 367;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_TYPES, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = PySet_New( const_set_5e66dfb79f927e10330bafc843d6850e );
        UPDATE_STRING_DICT1( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_108;
        PyObject *tmp_call_result_2;
        tmp_mvar_value_108 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_108 == NULL ))
        {
            tmp_mvar_value_108 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        CHECK_OBJECT( tmp_mvar_value_108 );
        tmp_called_instance_1 = tmp_mvar_value_108;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 444;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_320_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 444;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_109;
        PyObject *tmp_call_result_3;
        tmp_mvar_value_109 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_109 == NULL ))
        {
            tmp_mvar_value_109 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        if ( tmp_mvar_value_109 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LIBTIFF_CORE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 445;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_109;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 445;
        tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_301_tuple, 0 ) );

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 445;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_110;
        PyObject *tmp_call_result_4;
        tmp_mvar_value_110 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_110 == NULL ))
        {
            tmp_mvar_value_110 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        if ( tmp_mvar_value_110 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LIBTIFF_CORE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 446;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_110;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 446;
        tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_532_tuple, 0 ) );

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 446;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_111;
        PyObject *tmp_call_result_5;
        tmp_mvar_value_111 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_111 == NULL ))
        {
            tmp_mvar_value_111 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        if ( tmp_mvar_value_111 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LIBTIFF_CORE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 448;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_111;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 448;
        tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_255_tuple, 0 ) );

        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 448;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_mvar_value_112;
        PyObject *tmp_call_result_6;
        tmp_mvar_value_112 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_112 == NULL ))
        {
            tmp_mvar_value_112 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        if ( tmp_mvar_value_112 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LIBTIFF_CORE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 449;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_5 = tmp_mvar_value_112;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 449;
        tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_322_tuple, 0 ) );

        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 449;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_called_instance_6;
        PyObject *tmp_mvar_value_113;
        PyObject *tmp_call_result_7;
        tmp_mvar_value_113 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_113 == NULL ))
        {
            tmp_mvar_value_113 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        if ( tmp_mvar_value_113 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LIBTIFF_CORE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 450;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_6 = tmp_mvar_value_113;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 450;
        tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_323_tuple, 0 ) );

        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 450;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_mvar_value_114;
        PyObject *tmp_call_result_8;
        tmp_mvar_value_114 = GET_STRING_DICT_VALUE( moduledict_PIL$TiffTags, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );

        if (unlikely( tmp_mvar_value_114 == NULL ))
        {
            tmp_mvar_value_114 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LIBTIFF_CORE );
        }

        if ( tmp_mvar_value_114 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LIBTIFF_CORE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 451;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_7 = tmp_mvar_value_114;
        frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame.f_lineno = 451;
        tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_remove, &PyTuple_GET_ITEM( const_tuple_int_pos_333_tuple, 0 ) );

        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 451;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e8b23506ab469c2d72bc10c38ef6aae5 );
#endif
    popFrameStack();

    assertFrameObject( frame_e8b23506ab469c2d72bc10c38ef6aae5 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e8b23506ab469c2d72bc10c38ef6aae5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e8b23506ab469c2d72bc10c38ef6aae5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e8b23506ab469c2d72bc10c38ef6aae5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e8b23506ab469c2d72bc10c38ef6aae5, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_PIL$TiffTags );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
