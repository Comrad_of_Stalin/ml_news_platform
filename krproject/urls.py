"""krproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from kourseproject import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'main', views.index),
    path(r'news_reader', views.fill_db_with_raw_news),
    path(r'news_reader_2', views.news_reader_2),
    path(r'lang_aggregator', views.lang_aggregator),
    path(r'ajax_scroll_handler', views.DBScrollHandler('ru')),
    path(r'tape', views.tape),
    path(r'request', views.request_page),
    path(r'inner_request', views.handle_request),
    path(r'login', views.login_page),
    path(r'handle_login', views.handle_authentication),
    path(r'quit', views.handle_quit),
    path(r'profile', views.profile_page),
]
