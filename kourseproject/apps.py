from django.apps import AppConfig


class KourseprojectConfig(AppConfig):
    name = 'kourseproject'
