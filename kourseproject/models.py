from django.db import models

# Create your models here.


class Article(models.Model):
    text = models.TextField(null=True)
    title = models.TextField(null=True)
    datetime = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=255, default='', null=True)
    url = models.CharField(default='', max_length=255)
    site_name = models.CharField(max_length=255, default='')
    description = models.TextField(null=True)
    filename = models.CharField(max_length=100, default='')
    domain = models.CharField(max_length=100, default='')
    domain_suffix = models.CharField(max_length=100, default='')


class LanguageClassifiedArticle(models.Model):
    text = models.TextField(null=True)
    title = models.TextField(null=True)
    datetime = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=255, default='', null=True)
    url = models.CharField(default='', max_length=255)
    site_name = models.CharField(max_length=255, default='')
    description = models.TextField(null=True)
    lang = models.CharField(max_length=10, default='other')
    filename = models.CharField(max_length=100, default='')
    domain = models.CharField(max_length=100, default='')
    domain_suffix = models.CharField(max_length=100, default='')


class TopicClassifiedArticle(models.Model):
    text = models.TextField(null=True)
    title = models.TextField(null=True)
    datetime = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=255, default='', null=True)
    url = models.CharField(default='', max_length=255)
    site_name = models.CharField(max_length=255, default='')
    description = models.TextField(null=True)
    lang = models.CharField(max_length=10, default='other')
    topic = models.CharField(max_length=50, default='OTHER')
    filename = models.CharField(max_length=100, default='')
    domain = models.CharField(max_length=100, default='')
    domain_suffix = models.CharField(max_length=100, default='')


class ThreadClassifiedArticle(models.Model):
    text = models.TextField(null=True)
    title = models.TextField(null=True)
    datetime = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=255, default='', null=True)
    url = models.CharField(default='', max_length=255)
    site_name = models.CharField(max_length=255, default='')
    description = models.TextField(null=True)
    lang = models.CharField(max_length=10, default='other')
    topic = models.CharField(max_length=50, default='OTHER')
    thread_id = models.IntegerField(default=-1)
    filename = models.CharField(max_length=100, default='')
    domain = models.CharField(max_length=100, default='')
    domain_suffix = models.CharField(max_length=100, default='')


class TBaseArticle(models.Model):
    text = models.TextField(null=True)
    title = models.TextField(null=True)
    datetime = models.DateTimeField(auto_now=True)
    url = models.CharField(default='', max_length=255)
    description = models.TextField(null=True)
    filename = models.CharField(max_length=100, default='')
    domain_suffix = models.CharField(max_length=100, default='')
    domain = models.CharField(max_length=100, default='')
    author = models.CharField(max_length=255, default='', null=True)


class TDomain(models.Model):
    price = models.IntegerField(default=0)
    site_name = models.CharField(max_length=255, default='')
    name = models.CharField(max_length=100, default='', unique=True)
    news = models.ManyToManyField(TBaseArticle)


class TAuthor(models.Model):
    name = models.CharField(max_length=100, default='') #, unique=True)
    news = models.ManyToManyField(TBaseArticle)


class TLanguageClassifiedArticle(models.Model):
    base_article = models.OneToOneField(TBaseArticle, on_delete=models.CASCADE)
    lang = models.CharField(max_length=10, default='other')


class TTopicClassifiedArticle(models.Model):
    lang_classified_article = models.OneToOneField(TLanguageClassifiedArticle, on_delete=models.CASCADE)
    topic = models.CharField(max_length=50, default='OTHER')


class TThreadClassifiedArticle(models.Model):
    topic_classified_article = models.OneToOneField(TTopicClassifiedArticle, on_delete=models.CASCADE)
    thread_id = models.IntegerField(default=-1)


class TThread(models.Model):
    name = models.TextField(null=True)
    news = models.ManyToManyField(TThreadClassifiedArticle)


class TRequest(models.Model):
    url = models.TextField(default='', null=True)
    user_profile = models.ForeignKey('kourseproject.TUserProfile', on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now=True)


class TUserProfile(models.Model):
    cash = models.IntegerField(default=0)
    auth_id = models.IntegerField(default=-1, unique=True)
    domains = models.ManyToManyField('TDomain')
    registration_datetime = models.DateTimeField(auto_now=True)
