from django.shortcuts import render , HttpResponse, Http404, redirect
from django.template import RequestContext
from telega.runner import stage_1, stage_3
import json
import os
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

import pandas as pd
from kourseproject.models import Article, LanguageClassifiedArticle, TopicClassifiedArticle, TUserProfile, TRequest
from kourseproject.df_tools import fill_db_with_raw_news, DBScrollHandler, store_request
# Create your views here.


def index(request):
    return render(request, 'index.html', {})


def dt_from_str(s):
    if s is not None and not isinstance(s, str):
        return s
    return pd.datetime.strptime(s[:-6], '%Y-%m-%dT%H:%M:%S') if s else pd.datetime.now()


def news_reader(request):
    # LanguageClassifiedArticle.objects.all().delete()
    inp_dir = '/Users/deadcode/PycharmProjects/kaggle/shad/telegram/data/20191101'
    df = stage_1(inp_dir, fast=False)
    df['published_time'] = df['published_time'].apply(dt_from_str)
    print('\nINSERT\n')
    for i in range(df.shape[0]):
        print(i)
        n = df.iloc[i]
        obj = LanguageClassifiedArticle.objects.create(
            text=n.article,
            description=n.description,
            lang=n.title_estimated_lang,
            title=n.title,
            author=n.author,
            site_name=n.site_name,
            domain=n.domain,
            domain_suffix=n.domain_suffix,
            datetime=dt_from_str(n.published_time),
            filename=n.filename,
            url=n.url)
        obj.save()
    print('\nFINISH\n')
    return render(request, 'news_reader.html', {})


def news_reader_2(request):
    TopicClassifiedArticle.objects.all().delete()
    inp_dir = '/Users/deadcode/PycharmProjects/kaggle/shad/telegram/data/20191101'
    df = stage_1(inp_dir, fast=True)
    ru_df = df[df['title_estimated_lang'] == 'ru']
    en_df = df[df['title_estimated_lang'] == 'en']
    # ru_df['filename'] = ru_df['filename'].apply(lambda x: os.path.basename(x))
    # en_df['filename'] = en_df['filename'].apply(lambda x: os.path.basename(x))
    df = pd.concat([ru_df, en_df])
    df = stage_3(df, ru_dump='./models/ru_dump_without_nn', en_dump='./models/en_dump_without_nn')
    df['published_time'] = df['published_time'].apply(dt_from_str)
    print('\nINSERT\n')
    for i in range(df.shape[0]):
        print(i)
        n = df.iloc[i]
        obj = TopicClassifiedArticle.objects.create(
            text=n.article,
            description=n.description,
            lang=n.title_estimated_lang,
            title=n.title,
            author=n.author,
            site_name=n.site_name,
            domain=n.domain,
            domain_suffix=n.domain_suffix,
            datetime=dt_from_str(n.published_time),
            filename=n.filename,
            topic=n.category,
            url=n.url)
        obj.save()
    print('\nFINISH\n')
    return render(request, 'news_reader.html', {})


def lang_aggregator(request):
    lang = request.GET.get('lang')
    # print('LANGUAGE:', lang)
    lang_articles = LanguageClassifiedArticle.objects.filter(lang=lang)
    for o in lang_articles:
        print(o.lang)
    print(len(lang_articles))
    return render(request, 'news_viewer.html', {})


def news_generator(params):
    lang_articles = LanguageClassifiedArticle.objects.filter(**params)
    for l in lang_articles:
        yield l


def news_generator_2(params):
    lang_articles = TopicClassifiedArticle.objects.filter(**params)
    for l in lang_articles:
        yield l


class ScrollHandler:
    def __init__(self, default_lang='ru'):
        print('CALL CONSTRUCTOR')
        self.default_lang = default_lang
        self.default_params = {'lang': self.default_lang}
        self.news_generator = news_generator_2(self.default_params)

    def _extract_params(self, ajax_request):
        res = {}
        for k, v in ajax_request.GET.items():
            print('KV', k, v)
            if k == 'lang':
                res[k] = v.rstrip('#')
            elif k == 'author':
                res[k] = v.rstrip('#')
            elif k == 'topic':
                res[k] = v.rstrip('#')
            elif k == 'domain':
                res[k] = v.rstrip('#')
            elif k == 'domain_suffix':
                res[k] = v.rstrip('#')
            elif k == 'thread':
                res[k] = int(v.rstrip('#'))
        return res

    def __call__(self, ajax_request, *args, **kwargs):
        if not ajax_request.is_ajax():
            return Http404()
        batch_size = int(ajax_request.GET.get('batch_size', 10).rstrip('#'))
        news = []
        params = self._extract_params(ajax_request)
        print('PARAMS: ', params)
        if params != self.default_params:
            self.default_params = params
            self.news_generator = news_generator_2(params)
        for i in range(batch_size):
            try:
                o = next(self.news_generator)
            except StopIteration:
                self.news_generator = news_generator_2(params)
                o = next(self.news_generator)
            item = {
                'article': o.text,
                'title': o.title,
                'description': o.description,
                'author': o.author,
                'url': o.url,
                'domain': o.domain,
                'domain_suffix': o.domain_suffix,
                'lang': o.lang,
                'pub_datetime': o.datetime.strftime('%Y-%m-%dT%H:%M:%S'),
                'topic': o.topic
            }
            news.append(item)
        print([x['url'] for x in news])
        # print('AJAX REQUEST: ', ajax_request, 'PARAMS: ', dir(ajax_request), batch_size)
        # print('NEWS:', json.dumps(news))
        return HttpResponse(json.dumps(news), content_type='application/json')


@login_required
def tape(request):
    store_request(request)
    print('URI: ', request.get_raw_uri())
    return render(request, 'tape.html', {})


def handle_authentication(request):
    name = request.POST.get("name")
    password = request.POST.get("password")
    email = request.POST.get("email")
    user = authenticate(username=name, email=email, password=password)
    print('AUTH REQUEST: ', name, email, password)
    if user is None:
        user = User.objects.create_user(name, email, password)
        user.save()
        user = authenticate(username=name, email=email, password=password)
        assert user is not None
    print('USER IS AUTH: ', user.is_authenticated)

    uprofiles = TUserProfile.objects.filter(auth_id=user.id)
    if len(uprofiles) == 0:
        user_profile = TUserProfile.objects.create(auth_id=user.id)
        user_profile.save()

    login(request, user)
    print('REQUEST USER:', request.user)
    return redirect('/request')


@login_required
def handle_request(request):
    print('POST:', list(request.POST.items()), list(request.GET.items()), request, request.user)
    req = []
    # req2 = {}
    for k, v in request.GET.items():
        if k in ['topic', 'from_date', 'to_date', 'lang', 'author', 'domain']:
            if k == 'lang':
                v = v.lower()
            req.append(k + '=' + str(v))
            # req2[k] = v

    return redirect('/tape?' + '&'.join(req))


@csrf_exempt
@login_required
def request_page(request):
    print('REQUEST ')
    return render(request, 'request.html', {})


def login_page(request):
    return render(request, 'login.html', {})


def handle_quit(request):
    logout(request)
    return redirect('/login')


@login_required
def profile_page(request):
    user_profile = TUserProfile.objects.get(auth_id=request.user.id)
    print('PROFILE CASH: ', user_profile.cash)
    request_set = TRequest.objects.filter(user_profile=user_profile)
    return render(request, 'profile.html', {'user': request.user,
                                            'profile': user_profile,
                                            'requests': request_set,
                                            'n_requests': len(request_set)})
