# Generated by Django 3.0 on 2019-12-14 20:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kourseproject', '0005_auto_20191214_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='url',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='threadclassifiedarticle',
            name='url',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='topicclassifiedarticle',
            name='url',
            field=models.CharField(default='', max_length=255),
        ),
    ]
