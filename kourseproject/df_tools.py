from django.shortcuts import render , HttpResponse, Http404, redirect
from django.template import RequestContext
from telega.runner import stage_1, stage_3, stage_4
import json
import os
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

import pandas as pd
from kourseproject.models import (TBaseArticle,
                                  TLanguageClassifiedArticle,
                                  TTopicClassifiedArticle,
                                  TAuthor,
                                  TDomain,
                                  TThread,
                                  TThreadClassifiedArticle,
                                  TUserProfile,
                                  TRequest)


def dt_from_str(s):
    if s is not None and not isinstance(s, str):
        return s
    return pd.datetime.strptime(s[:-6], '%Y-%m-%dT%H:%M:%S') if s else pd.datetime.now()


def fill_db_with_raw_news(request):
    TBaseArticle.objects.all().delete()
    TLanguageClassifiedArticle.objects.all().delete()
    TTopicClassifiedArticle.objects.all().delete()
    TThreadClassifiedArticle.objects.all().delete()
    TDomain.objects.all().delete()
    TAuthor.objects.all().delete()
    TThread.objects.all().delete()
    inp_dir = '/Users/deadcode/PycharmProjects/kaggle/shad/telegram/data/20191101'
    df = stage_1(inp_dir, fast=True)
    ru_df = df[df['title_estimated_lang'] == 'ru']
    en_df = df[df['title_estimated_lang'] == 'en']
    df = pd.concat([ru_df, en_df])
    df = stage_3(df, ru_dump='./models/ru_dump_without_nn', en_dump='./models/en_dump_without_nn')
    df['published_time'] = df['published_time'].apply(dt_from_str)
    print('\nINSERT\n')
    passed_domains = set()
    passed_authors = set()

    base_articles_ids = []
    lang_articles_ids = []
    topic_articles_ids = []
    thread_classified_articles_ids = []

    for i in range(df.shape[0]):
        print(i)
        n = df.iloc[i]
        print('DOMAIN: ', n.domain)
        obj = TBaseArticle.objects.create(
            text=n.article,
            description=n.description,
            # lang=n.title_estimated_lang,
            title=n.title,
            author=n.author,
            domain=n.domain,
            # site_name=n.site_name,
            domain_suffix=n.domain_suffix,
            datetime=dt_from_str(n.published_time),
            filename=n.filename,
            url=n.url)

        lang_estimated_obj = TLanguageClassifiedArticle.objects.create(lang=n.title_estimated_lang, base_article=obj)
        topic_estimated_obj = TTopicClassifiedArticle.objects.create(topic=n.category, lang_classified_article=lang_estimated_obj)
        thread_estimated_obj = TThreadClassifiedArticle.objects.create(thread_id=-1, topic_classified_article=topic_estimated_obj)

        obj.save()
        if n.author:
            if n.author not in passed_authors:
                passed_authors.add(n.author)
                author_obj = TAuthor.objects.create(name=n.author)
            else:
                author_obj = TAuthor.objects.get(name=n.author)
            author_obj.news.add(obj)
            author_obj.save()

        if n.domain:
            if n.domain not in passed_domains:
                passed_domains.add(n.domain)
                domain_obj = TDomain.objects.create(name=n.domain, site_name='' if not n.site_name else n.site_name, price=0)
            else:
                domain_obj = TDomain.objects.get(name=n.domain)
            domain_obj.news.add(obj)
            domain_obj.save()
        obj.save()
        lang_estimated_obj.save()
        topic_estimated_obj.save()
        thread_estimated_obj.save()

        base_articles_ids.append(obj.id)
        lang_articles_ids.append(lang_estimated_obj.id)
        topic_articles_ids.append(topic_estimated_obj.id)
        thread_classified_articles_ids.append(thread_estimated_obj.id)

    df['base_article_id'] = base_articles_ids
    df['lang_article_id'] = lang_articles_ids
    df['topic_article_id'] = topic_articles_ids
    df['thread_estimated_article_id'] = thread_classified_articles_ids

    df = stage_4(df)

    groups = df.groupby('thread')
    for i, g in groups:
        title = g.iloc[0]['thread_title']
        print('THREAD TITLE: ', title)
        thread_estimated_articles_ids = g['thread_estimated_article_id']
        thread_estimated_articles_articles = TThreadClassifiedArticle.objects.filter(id__in=thread_estimated_articles_ids)
        thread_obj = TThread.objects.create(name='' if not title else title)
        thread_obj.news.set(thread_estimated_articles_articles)
        thread_obj.save()
        for o in thread_estimated_articles_articles:
            o.thread_id = thread_obj.id
            o.save()
        thread_obj.save()


    print('\nFINISH\n')
    return render(request, 'news_reader.html', {})


def batch_news_generator(params):
    res_query = {}
    for k, v in params.items():
        if k == 'from_date':
            k = 'topic_classified_article__lang_classified_article__base_article__datetime_le'
            v = pd.datetime.strptime(v.strip(), '%Y-%m-%d')
        elif k == 'to_date':
            k = 'topic_classified_article__lang_classified_article__base_article__datetime_ge'
            v = pd.datetime.strptime(v.strip(), '%Y-%m-%d')
        elif k == 'domain':
            if not v.strip():
                continue
            k = 'topic_classified_article__lang_classified_article__base_article__domain'
            v = v.strip()
        elif k == 'author':
            if not v.strip():
                continue
            k = 'topic_classified_article__lang_classified_article__base_article__author'
            v = v.strip()
        elif k == 'lang':
            k = 'topic_classified_article__lang_classified_article__lang'
            v = v.strip()
        elif k == 'topic':
            k = 'topic_classified_article__topic'
            v = v.strip()
        elif k == 'thread_id':
            k = 'thread_id'
            v = v
        else:
            continue
        res_query[k] = v

    print('QUERY PARAMS: ', res_query)


    lang_articles = TThreadClassifiedArticle.objects.filter(**res_query)
    for l in lang_articles:
        yield l


class DBScrollHandler:
    def __init__(self, default_lang='ru'):
        print('CALL CONSTRUCTOR')
        self.default_lang = default_lang
        self.default_params = {'lang': self.default_lang}
        self.news_generator = batch_news_generator(self.default_params)

    def _extract_params(self, ajax_request):
        res = {}
        for k, v in ajax_request.GET.items():
            print('KV', k, v)
            if k == 'lang':
                res[k] = v.rstrip('#')
            elif k == 'author':
                res[k] = v.rstrip('#')
            elif k == 'topic':
                res[k] = v.rstrip('#')
            elif k == 'domain':
                res[k] = v.rstrip('#')
            elif k == 'domain_suffix':
                res[k] = v.rstrip('#')
            elif k == 'thread_id':
                res[k] = int(v.rstrip('#'))
        return res

    def __call__(self, ajax_request, *args, **kwargs):
        if not ajax_request.is_ajax():
            return Http404()
        batch_size = int(ajax_request.GET.get('batch_size', 10).rstrip('#'))
        news = []
        params = self._extract_params(ajax_request)
        print('PARAMS: ', params)
        if params != self.default_params:
            self.default_params = params
            self.news_generator = batch_news_generator(params)
        for i in range(batch_size):
            try:
                o = next(self.news_generator)
            except StopIteration:
                self.news_generator = batch_news_generator(params)
                # o = next(self.news_generator)
                break
            item = {
                'article': o.topic_classified_article.lang_classified_article.base_article.text,
                'title': o.topic_classified_article.lang_classified_article.base_article.title,
                'description': o.topic_classified_article.lang_classified_article.base_article.description,
                'author': o.topic_classified_article.lang_classified_article.base_article.author,
                'url': o.topic_classified_article.lang_classified_article.base_article.url,
                'domain': o.topic_classified_article.lang_classified_article.base_article.domain,
                'domain_suffix': o.topic_classified_article.lang_classified_article.base_article.domain_suffix,
                'lang': o.topic_classified_article.lang_classified_article.lang,
                'pub_datetime': o.topic_classified_article.lang_classified_article.base_article.datetime.strftime('%Y-%m-%dT%H:%M:%S'),
                'topic': o.topic_classified_article.topic,
                'thread_id': o.thread_id
            }
            news.append(item)
        print([x['url'] for x in news])
        return HttpResponse(json.dumps(news), content_type='application/json')


def store_request(request):
    user_profile = TUserProfile.objects.get(auth_id=request.user.id)
    req_obj = TRequest.objects.create(user_profile=user_profile, url=request.get_raw_uri())
    req_obj.save()
    user_profile.save()
